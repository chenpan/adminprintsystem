<?php

/* *
 * 功能：支付宝服务器异步通知页面
 * 版本：3.3
 * 日期：2012-07-23
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。


 * ************************页面功能说明*************************
 * 创建该页面文件时，请留心该页面文件中无任何HTML代码及空格。
 * 该页面不能在本机电脑测试，请到服务器上做测试。请确保外部可以访问该页面。
 * 该页面调试工具请使用写文本函数logResult，该函数已被默认关闭，见alipay_notify_class.php中的函数verifyNotify
 * 如果没有收到该页面返回的 success 信息，支付宝会在24小时内按一定的时间策略重发通知
 */

require_once("alipay.config.php");
require_once("lib/alipay_notify.class.php");

//计算得出通知验证结果
$alipayNotify = new AlipayNotify($alipay_config);
$verify_result = $alipayNotify->verifyNotify();

if ($verify_result) {//验证成功
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //请在这里加上商户的业务逻辑程序代
    //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
    //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
    //批次号
    $batch_no = $_POST['batch_no'];

    //批量退款数据中转账成功的笔数

    $success_num = $_POST['success_num'];

    //批量退款数据中的详细信息
    $result_details = $_POST['result_details']; //2010031906272929^80^SUCCESS$jax_chuanhang@alipay.com^2088101003147483^0.01^SUCCESS

    $conn = mysql_connect("rds355y2nh13a6a5f420.mysql.rds.aliyuncs.com:3306", "admin_rw", "Kc-ADwoAsCv3J4");
    mysql_select_db("db_home", $conn);
//mysql_query("set names gb2312");
//    $sql1 = mysql_query("update tbl_refund set statue = 1 where tborderId ='" . $tborderId . "'", $conn);

    $result_detailss = explode('#', $result_details); //ID 1,2,3,4
    foreach ($result_detailss as $k => $o) {
        $result_detailsd = explode('^', $o); //ID 1,2,3,4
        $refund_info = mysql_query("update tbl_refund set info = '" . $result_details . "' where tborderId ='" . $result_detailsd[0] . "' AND money = $result_detailsd[1] AND statue != 1 limit 1", $conn);
        $str2 = substr($result_detailsd[2], 0, 7);
        if ($str2 == "SUCCESS") {
            mysql_query("update tbl_refund set refundTime = '" . date('Y-m-d H:i:s') . "' where tborderId ='" . $result_detailsd[0] . "' AND money = $result_detailsd[1] AND statue != 1 limit 1", $conn);
            $sql = mysql_query("select _sessionId,subbusinessId,consumptionIntegral,_userId,money,_orderId from tbl_refund where tborderId ='" . $result_detailsd[0] . "' AND money = $result_detailsd[1] AND statue != 1", $conn);
            $info = mysql_fetch_array($sql);
            if ($info["_sessionId"] == "") {
                mysql_query("update tbl_subbusiness set isrefund =1 where subbusinessId ='" . $info["subbusinessId"] . "'", $conn);
                mysql_query("update tbl_subbusiness set status =3 where subbusinessId ='" . $info["subbusinessId"] . "'", $conn);
                $sqls = mysql_query("select _businessId from tbl_subbusiness where subbusinessId ='" . $info["subbusinessId"] . "'", $conn);
                $infos = mysql_fetch_array($sqls);
                //查找此订单下所有子订单是否都不退款的？              
                $sqlx = mysql_query("select count(*) as totalcount from tbl_subbusiness where _businessId ='" . $infos["_businessId"] . "' and isrefund = 0");
                $row = mysql_fetch_array($sqlx);

                if ($row["totalcount"] == 0) {
                    mysql_query("update tbl_business set isrefund = 1 where businessid ='" . $infos["_businessId"] . "'", $conn);
                    mysql_query("update tbl_business set status = 3 where businessid ='" . $infos["_businessId"] . "'", $conn);
                }

//                $sql2 = mysql_query("select points from tbl_record where userid ='" . $info["_userId"] . "'", $conn);
//                $info2 = mysql_fetch_array($sql2);
//                $Integral = 0;
//                if ($info["consumptionIntegral"] == 0) {
//                    $Integral = $info2["points"] - $info["money"];
//                    $reduceIntegral = $info['money'] * 10;
//                    $userid = $info["_userId"];
//                    $_orderId = "订单号" . $info["_orderId"] . "退款y";
//                    mysql_query("insert into tbl_integralDetails(addIntegral,reduceIntegral,_userid,happentime,happenInfo) values('0','" . $reduceIntegral . "','" . $userid . "','" . date('Y-m-d H:i:s') . "','" . $_orderId . "')", $conn);
//                } else {
//                    $Integral = $info2["points"] - $info["money"] * 10 + $info["consumptionIntegral"];
//                    $addIntegral = $info["consumptionIntegral"];
//                    $reduceIntegral = $info['money'] * 10;
//                    $userid = $info["_userId"];
//                    $_orderId = "订单号" . $info["_orderId"] . "退款x";
//                    mysql_query("insert into tbl_integralDetails(addIntegral,reduceIntegral,_userid,happentime,happenInfo) values('" . $addIntegral . "','" . $reduceIntegral . "','" . $userid . "','" . date('Y-m-d H:i:s') . "','" . $_orderId . "')", $conn);
//                }
//                mysql_query("update tbl_record set points = " . intval(round(floatval($Integral))) . " where userid ='" . $info["subbusinessId"] . "'", $conn);
            }
            mysql_query("update tbl_refund set statue = 1 where tborderId ='" . $result_detailsd[0] . "' AND money = $result_detailsd[1] AND statue != 1 limit 1", $conn);
        } else {
            mysql_query("update tbl_refund set statue = 2 where tborderId ='" . $result_detailsd[0] . "' AND money = $result_detailsd[1] AND statue != 1", $conn);
        }
        mysql_close();
    };


//    $post_data = array('batch_no' => $batch_no, 'result_details' => $result_details);
//    $url = "http://123.56.94.175/index.php?r=alipayPrint/refund";
//    $data = request_post($url, $post_data);
    //判断是否在商户网站中已经做过了这次通知返回的处理
    //如果没有做过处理，那么执行商户的业务程序
    //如果有做过处理，那么不执行商户的业务程序

    echo "success";  //请不要修改或删除
    //调试用，写文本函数记录程序运行情况是否正常
    //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
    //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} else {
    //验证失败
    echo "fail";

    //调试用，写文本函数记录程序运行情况是否正常
    //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
}
?>