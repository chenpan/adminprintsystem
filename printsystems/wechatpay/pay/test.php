<?php

/**
  终端扫码支付 回传
 */
include_once("./log_.php");
include_once("../WxPayPubHelper/WxPayPubHelper.php");

//使用通用通知接口
$notify = new Notify_pub();

//存储微信的回调
//$notify->saveData($xml);

$xmls = "<xml><appid><![CDATA[wx64f496d57609eec4]]></appid><attach><![CDATA[test]]></attach><bank_type><![CDATA[CMB_DEBIT]]></bank_type><cash_fee><![CDATA[20]]></cash_fee><fee_type><![CDATA[CNY]]></fee_type><is_subscribe><![CDATA[Y]]></is_subscribe><mch_id><![CDATA[1292670201]]></mch_id><nonce_str><![CDATA[cb31fa5dcb074567b4eaeae02455e954]]></nonce_str><openid><![CDATA[osW7cjtIgikSiIzpmn98y9Mtukr0]]></openid><out_trade_no><![CDATA[12926702012015120311203191]]></out_trade_no><result_code><![CDATA[SUCCESS]]></result_code><return_code><![CDATA[SUCCESS]]></return_code><sign><![CDATA[A579F3225F2DB35DA1B0443C9D66D8F7]]></sign><time_end><![CDATA[20151203112051]]></time_end><total_fee>20</total_fee><trade_type><![CDATA[NATIVE]]></trade_type><transaction_id><![CDATA[1003180000201512031879143577]]></transaction_id></xml>";
$p = xml_parser_create();
xml_parse_into_struct($p, $xmls, $vals, $index);
xml_parser_free($p);

//echo "Index array\n";
//var_dump($index);
//echo "\nVals array\n";
//var_dump($vals);


$result_code = "";
$err_code = "";
$err_code_des = "";
$trade_type = "";
$transaction_id = "";
$out_trade_no = "";
$time_end = "";

foreach ($vals as $k => $l) {
    if ($vals[$k]["tag"] == "RESULT_CODE") {
        $result_code = $vals[$k]["value"];
    }
    if ($vals[$k]["tag"] == "ERR_CODE") {
        $err_code = $vals[$k]["value"];
    }
    if ($vals[$k]["tag"] == "ERR_CODE_DES") {
        $err_code_des = $vals[$k]["value"];
    }
    if ($vals[$k]["tag"] == "TRADE_TYPE") {
        $trade_type = $vals[$k]["value"];
    }
    if ($vals[$k]["tag"] == "TRANSACTION_ID") {
        $transaction_id = $vals[$k]["value"];
    }
    if ($vals[$k]["tag"] == "OUT_TRADE_NO") {
        $out_trade_no = $vals[$k]["value"];
    }
    if ($vals[$k]["tag"] == "TIME_END") {
        $time_end = $vals[$k]["value"];
    }
}
echo $result_code . "+" . $err_code . "+" . $err_code_des . "+" . $trade_type . "+" . $transaction_id . "+" . $out_trade_no . "+" . $time_end;
?>