<?php

function request_post($url = '', $post_data = array()) { //post Curl
    if (empty($url) || empty($post_data)) {
        return false;
    }
    $o = "";
    foreach ($post_data as $k => $v) {
        $o.= "$k=" . urlencode($v) . "&";
    }
    $post_data = substr($o, 0, -1);
    $postUrl = $url;
    $curlPost = $post_data;
    $ch = curl_init(); //初始化curl
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
    curl_setopt($ch, CURLOPT_URL, $postUrl); //抓取指定网页
    curl_setopt($ch, CURLOPT_HEADER, 0); //设置header
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_POST, 1); //post提交方式
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
    $data = curl_exec($ch); //运行curl
    curl_close($ch);
    return $data;
}

/**
  终端扫码支付 回传
 */
include_once("./log_.php");
include_once("../WxPayPubHelper/WxPayPubHelper.php");

//使用通用通知接口
$notify = new Notify_pub();

//存储微信的回调
$xml = $GLOBALS['HTTP_RAW_POST_DATA']; //返回的内容具体值见:https://pay.weixin.qq.com/wiki/doc/api/native.php?chapter=9_7
$notify->saveData($xml);
//验证签名，并回应微信。
//对后台通知交互时，如果微信收到商户的应答不是成功或超时，微信认为通知失败，
//微信会通过一定的策略（如30分钟共8次）定期重新发起通知，
//尽可能提高通知的成功率，但微信不保证通知最终能成功。

if ($notify->checkSign() == FALSE) { //返回微信服务器 接收状态
    $notify->setReturnParameter("return_code", "FAIL"); //返回状态码
    $notify->setReturnParameter("return_msg", "签名失败"); //返回信息
} else {
    $notify->setReturnParameter("return_code", "SUCCESS"); //设置返回码
    $p = xml_parser_create();
    xml_parse_into_struct($p, $xml, $vals, $index);
    xml_parser_free($p);

    $total_fee = 0;
    $result_code = "";
    $err_code = "";
    $err_code_des = "";
    $trade_type = "";
    $transaction_id = "";
    $out_trade_no = "";
    $time_end = "";

    foreach ($vals as $k => $l) {
        if ($vals[$k]["tag"] == "RESULT_CODE") {
            $result_code = $vals[$k]["value"];
        }
        if ($vals[$k]["tag"] == "ERR_CODE") {
            $err_code = $vals[$k]["value"];
        }
        if ($vals[$k]["tag"] == "ERR_CODE_DES") {
            $err_code_des = $vals[$k]["value"];
        }
        if ($vals[$k]["tag"] == "TRADE_TYPE") {
            $trade_type = $vals[$k]["value"];
        }
        if ($vals[$k]["tag"] == "TRANSACTION_ID") {
            $transaction_id = $vals[$k]["value"];
        }
        if ($vals[$k]["tag"] == "OUT_TRADE_NO") {
            $out_trade_no = $vals[$k]["value"];
        }
        if ($vals[$k]["tag"] == "TIME_END") {
            $time_end = $vals[$k]["value"];
        }
        if ($vals[$k]["tag"] == "TOTAL_FEE") {
            $total_fee = $vals[$k]["value"];
        }
    }

    $post_data = array('result_code' => $result_code, 'err_code' => $err_code, 'err_code_des' => $err_code_des, 'trade_type' => $trade_type, 'transaction_id' => $transaction_id, 'out_trade_no' => $out_trade_no, 'time_end' => $time_end, 'total_fee' => $total_fee);
    $url = "http://admin.cqutprint.com/index.php?r=alipayPrint/wechatpayment";
    $data = request_post($url, $post_data);
}
$returnXml = $notify->returnXml();
echo $returnXml;


//if ($notify->checkSign() == TRUE) //扫码支付成功 执行的操作
//{
//    $myfile = fopen("./newfile.txt", "w") or die("Unable to open file!");
//    date_default_timezone_set('PRC');
//    $date = date('Y-m-d H:i:s');
//    $txt = $date." success pay!";
//    fwrite($myfile, $txt);
//    fclose($myfile);
//}
//else //扫码支付失败
//{
//    $myfile = fopen("./newfile.txt", "w") or die("Unable to open file!");
//    date_default_timezone_set('PRC');
//    $date = date('Y-m-d H:i:s');
//    $txt = $date." false pay!";
//    fwrite($myfile, $txt);
//    fclose($myfile);
//}
?>