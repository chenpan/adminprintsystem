<?php

include 'BaseController.php';

class priceAdminController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function filters() {
        return array(
            'price + price',
            'editprice + editprice',
            'delprice + delprice',
        );
    }

    public function filterprice($filterChain) {
        $this->checkAccess("价格", $filterChain);
    }

    public function actionprice() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $storeId = Yii::app()->session['storeid'];
            $price_mode = price::model();
            if (Yii::app()->session['storeid'] == 0) {
                $price_info = $price_mode->findAll(array('order' => "storeid DESC"));
            } else {
                $price_info = $price_mode->findAllBySql("select * from tbl_price where storeid in (" . Yii::app()->session["storeid"] . ")", array('order' => "storeid DESC"));
            }
            //添加价格策略权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagAdd = "hidden";
            $flagEdit = "hidden";
            $flagDel = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "编辑价格") {
                    $flagEdit = "";
                }
                if ($itemName == "删除价格") {
                    $flagDel = "";
                }
            }
            $this->renderPartial('price', array('price_info' => $price_info, 'flagEdit' => $flagEdit, 'flagDel' => $flagDel, "leftContent" => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /* ---------------------修改价格------------------------------- */

    public function actioneditprices() {
        if (isset($_POST['priceId'])) {
            $priceId = $_POST["priceId"];
            $A4 = $_POST["A4"];
            $A42b = $_POST["A42b"];
            $A3 = $_POST["A3"];
            $scan = $_POST["scan"];
            $color = $_POST["color"];
            $copy = $_POST["copy"];
            $price_model = price::model();
            $price_info = $price_model->find(array('condition' => "priceid='$priceId'"));
            if ($price_info) {
                $price_info->one_side_A4 = $A4;
                $price_info->two_side_A4 = $A42b;
                $price_info->one_side_A3 = $A3;
                $price_info->scan_price = $scan;
                $price_info->color_price = $color;
                $price_info->copy_price = $copy;
                if ($price_info->save()) {
                    $json = '{"data":"200"}';
                    echo $json;
                } else {
                    $json = '{"data":"401"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"401"}';
                echo $json;
            }
        } else {
            $json = '{"data":"401"}';
            echo $json;
        }
    }

    /* ----------------------删除学校---------------------------------- */

    public function actiondelSchool() {
        if (isset($_POST['priceId'])) {
            $priceId = $_POST["priceId"];
            $price_model = price::model();
            $price_info = $price_model->find(array('condition' => "priceid='$priceId'"));
            if ($price_info) {
                $counts = $price_model->deleteAll(array('condition' => "priceid='$priceId'"));
                if ($counts > 0) {
                    $json = '{"data":"200"}';
                    echo $json;
                } else {
                    $json = '{"data":"401"}';
                    echo $json;
                }
            }
        }
    }

    /*
      类型与学校级联ajax数据
     */

    public function actionaddSchoolAjax() {
        if (isset($_POST)) {
            $id = $_POST["value"];
            $store_model = price::model();
            $store_info = $store_model->findAllBySql("SELECT * FROM  tbl_store WHERE storeid NOT IN (SELECT storeid FROM tbl_price where type=$id)");
            $str = "";
            if (count($store_info) > 0) {
                foreach ($store_info as $value) {
                    $storeId = $value->storeid;
                    $storeName = $value->storename;
                    $str .= "{'storeId':'$storeId','storeName':'$storeName'},";
                }
                $str = substr($str, 0, -1);
                $json = "["
                        . $str
                        . "]";
                $json = str_replace("'", '"', $json);
                echo $json;
            } else {
                $str = "{'storeId':0,'storeName':' '}";
                echo $str;
            }
        }
    }

}
