<?php

include 'BaseController.php';

class messageController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function filters() {
        return array(
            'sendmessage + sendmessage'
        );
    }

    public function filtersendmessage($filterChain) {
        $this->checkAccess("短信", $filterChain);
    }

    public function actionsendmessage() {//发送短信界面
        if (isset(Yii::app()->session['adminuser'])) {
            if (isset($_POST['phone'])) {
                $phone = $_POST['phone'];
                $content = $_POST['text'];

                $user = 'cqutprint'; //短信接口用户名 $user
                $pwd = '112233'; //短信接口密码 $pwd
                $mobiles = $phone; //说明：取用户输入的手号
                $contents = $content; //说明：取用户输入的短信内容
                $chid = 0; //通道ID
                $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
                file_get_contents($sendMessage);
                echo "<script>parent.alert('发送成功！');parent.window.location.href = './index.php?r=message/sendmessage';</script>";
            } else {
                $leftContent = $this->getLeftContent();
                $recommend = $this->getrecommend();
                $this->renderPartial("sendmessage", array("leftContent" => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

}
