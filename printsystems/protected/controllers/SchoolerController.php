<?php

include 'BaseController.php';

class schoolerController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'school + school',
            'addschool + addschool',
            'deleteschool + deleteschool',
            'editInfo + editInfo',
        );
    }

    public function filterschool($filterChain) {
        $this->checkAccess("学校", $filterChain);
    }

    public function filteraddschool($filterChain) {
        $this->checkAccess("添加学校", $filterChain);
    }

    public function filterdeleteschool($filterChain) {
        $this->checkAccess("删除学校", $filterChain);
    }

    public function filtereditInfo($filterChain) {
        $this->checkAccess("编辑信息", $filterChain);
    }

    public function filtereditactive($filterChain) {
        $this->checkAccess("活动开关", $filterChain);
    }

    /*     * ************** 学校列表界面 start ************** */

    public function actionschool() {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $deleteSchool = "hidden";
            $addSchool = "hidden";
            $editSchool = "hidden";
            $active = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除学校") {
                    $deleteSchool = "";
                }
                if ($itemName == "添加学校") {
                    $addSchool = "";
                }
                if ($itemName == "编辑信息") {
                    $editSchool = "";
                }
                if ($itemName == "活动开关") {
                    $active = "";
                }
            }
            $store_model = store::model();
            if (Yii::app()->session['storeid'] == 0) {
                $store_info = $store_model->findAll(array('order' => "storeid DESC"));
            } else {
                $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . Yii::app()->session["storeid"] . ")", array('order' => "storeid DESC"));
            }
            $this->renderPartial('school', array('store_info' => $store_info, 'deleteSchool' => $deleteSchool, 'addSchool' => $addSchool, 'editSchool' => $editSchool, 'active' => $active, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 学校列表界面 end ************** */

    /*     * ************** 添加学校 start ************** */

    public function actionaddschool() {//平台添加院校同时设置其管理员账号        
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();


            $store_model = new store();
            $administrator_model = new administrator();
            if (isset($_POST['schoolname'])) {
                $usernamee = $_POST['schoolusername'];
                $user = $administrator_model->find(array('condition' => "username='$usernamee'"));
                if (count($user) == 0) {
                    $province = $_POST['province'];
                    $store_model->storename = $_POST['schoolname'];
                    $store_model->storedescript = $_POST['schooldescript'];
                    $store_model->addtime = date('Y-m-d H:i:s', time());
                    $store_model->province = $province;
                    $store_info = $store_model->find(array("condition" => "province= '$province'"));
                    if (count($store_info) != 0) {
                        $store_model->pcode = $store_info->pcode;
                        $criteria = new CDbCriteria;
                        $criteria->select = 'storecode';
                        $criteria->addCondition("province = '$province'");
                        $criteria->order = 'storecode DESC';
                        $store_infos = $store_model->find($criteria);
                        header("Content-type: text/html; charset=utf-8");
                        $store_model->pcode = $store_info->pcode;
                        $store_model->storecode = $store_infos->storecode + 1;
                    } else {
                        $criteria = new CDbCriteria;
                        $criteria->select = 'pcode';
                        $criteria->order = 'pcode DESC';
                        $store_infos = $store_model->find($criteria);
                        header("Content-type: text/html; charset=utf-8");
                        $store_model->pcode = $store_infos->pcode + 1;
                        $store_model->storecode = $store_infos->pcode + 1 . (100 + count($store_info) + 1);
                        $store_model->pcode = $store_infos->pcode + 1;
                        $store_model->storecode = $store_infos->pcode + 1 . (100 + count($store_info) + 1);
                    }

                    $tempFile = $_FILES['schoollogo']['tmp_name'];

                    if ($store_model->save()) {
                        $newId = $store_model->attributes['storeid'];
                        $store_info = $store_model->find("storeid=$newId");
                        $attachment_after = substr(strrchr($_FILES['schoollogo']['name'], '.'), 1);  //文件的后缀名 docx

                        $fileName = $newId . '.' . $attachment_after;

                        $targetPath = './images/storelogo';
                        $targetFile = rtrim($targetPath, '/') . '/' . $fileName;

                        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));

//                        $uploadFile->saveAs('./images/storelogo/' . $fileName);
                        $store_info->storelogo = $fileName;
                        if ($store_info->save()) {
                            $administrator_model->username = $usernamee;
                            $administrator_model->_storeid = $newId;
                            $administrator_model->_roleid = 2; //学院管理
                            $password = $_POST['schoolpassword'];
                            $administrator_model->password = md5($password);
                            $administrator_model->phone = $_POST['phone'];
                            $administrator_model->QQ = $_POST['QQ'];
                            $administrator_model->addtime = date('Y-m-d H:i:s', time());
                            //默认价格
                            $priceFlag = false;
                            for ($i = 1; $i <= 2; $i++) {
                                $price_model_new = new price();
                                $price_model_new->storeid = $newId;
                                $price_model_new->storename = $_POST['schoolname'];
                                $price_model_new->one_side_A4 = 0.2;
                                $price_model_new->two_side_A4 = 0.2;
                                $price_model_new->one_side_A3 = 0.2;
                                $price_model_new->scan_price = 0.2;
                                $price_model_new->color_price = 0.2;
                                $price_model_new->copy_price = 0.2;
                                $price_model_new->type = $i;
                                if (!$price_model_new->save()) {
                                    $priceFlag = true;
                                }
                            }

                            if ($administrator_model->save() && $priceFlag == false) {
                                $this->redirect('./index.php?r=schooler/school');
                            }
                        }
                    }
                } else {
                    header("Content-type: text/html; charset=utf-8");
                    echo "<script>alert('用户名已存在,请重新输入！');</script>";
                }
            } else {
                $this->renderPartial('addschool', array('leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else
            $this->redirect('./index.php?r=default/index');
    }

    /*     * ************** 添加学校 end ************** */

    public function actiontest() {
        $province = "北京";
        $store_model = store::model();
        $store_info = $store_model->find(array("condition" => "province= '$province'"));
        if (count($store_info) != 0) {
            $store_model->pcode = $store_info->pcode;
            $criteria = new CDbCriteria;
            $criteria->select = 'storecode';
            $criteria->addCondition("province = '$province'");
            $criteria->order = 'storecode DESC';
            $store_infos = $store_model->find($criteria);
            header("Content-type: text/html; charset=utf-8");
            $store_model->pcode = $store_info->pcode;
            $store_model->storecode = $store_infos->storecode + 1;
        } else {

            $criteria = new CDbCriteria;
            $criteria->select = 'pcode';
            $criteria->order = 'pcode DESC';
            $store_infos = $store_model->find($criteria);
            header("Content-type: text/html; charset=utf-8");
            $store_model->pcode = $store_infos->pcode + 1;
            $store_model->storecode = $store_infos->pcode + 1 . (100 + count($store_info) + 1);
            echo "下";
            echo $store_infos->pcode + 1 . (100 + count($store_info) + 1);
        }
    }

    /*     * ************** 删除院校 start************** */

    public function actiondeleteschool() {
        $storeid = $_POST["storeid"];
        $store_model = store::model();

        $store_info = $store_model->findByPk($storeid);
        $filename = './images/storelogo/' . $store_info->storelogo;

        $count = $store_model->deleteAll(array('condition' => "storeid = $storeid"));

        $administrator_model = administrator::model();
        $counts = $administrator_model->deleteAll(array('condition' => "_storeid = $storeid"));

        if (is_file($filename)) {
            if (unlink($filename) && $count > 0 && $counts > 0) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

    /*     * ************** 删除院校 end************** */


    /*     * ************** 编辑院校信息 start************** */

    public function actioneditInfo($storeid) {

        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $administrator_model = administrator::model();
            $store_model = store::model();
            if (isset($_POST['schoolname'])) {
                $schoolId = $_POST["schoolId"];
                $store_info = $store_model->find("storeid = $schoolId");
                if (count($store_info) != 0) {
                    $store_info->storename = $_POST['schoolname'];
                    $store_info->storedescript = $_POST['schooldescript'];
                    if (!empty($_FILES['schoollogo']['tmp_name'])) {
                        $tempFile = $_FILES['schoollogo']['tmp_name'];
                        $fileName = $schoolId . '.jpg';
                        $targetPath = './images/storelogo';
                        $targetFile = rtrim($targetPath, '/') . '/' . $fileName;
                        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));
                        $store_info->storelogo = $fileName;
                    }
                }
                $administrator_info = $administrator_model->find("_storeid = $schoolId");
                if ($_POST['schoolusername'] != "") {
                    $username = $_POST['schoolusername'];
                    $users = $administrator_model->find(array('condition' => "username='$username' AND _storeid !=$storeid"));

                    if (count($users) != 0) {
                        echo "<script>alert('用户名已存在,请重新输入！');window.location.href='./index.php?r=schooler/school&storeid='" . $schoolId . ";</script>";
                    } else {
                        if (count($administrator_info) != 0) {
                            $administrator_info->username = $username;
                            if ($_POST['schoolpassword'] != "") {
                                $password = $_POST['schoolpassword'];
                                $administrator_info->password = md5($password);
                            }
                            if ($_POST['phone'] != "") {
                                $administrator_info->phone = $_POST['phone'];
                            }
                            if ($_POST['QQ'] != "") {
                                $administrator_info->QQ = $_POST['QQ'];
                            }
                        }
                    }
                } else {
                    if ($_POST['schoolpassword'] != "") {
                        $password = $_POST['schoolpassword'];
                        $administrator_info->password = md5($password);
                    }
                    if ($_POST['phone'] != "") {
                        $administrator_info->phone = $_POST['phone'];
                    }
                    if ($_POST['QQ'] != "") {
                        $administrator_info->QQ = $_POST['QQ'];
                    }
                }

                if ($store_info->save() && $administrator_info->save()) {
                    echo "<script>parent.$('.save_lable').text('保存成功！').css('color','red');</script>";
                } else {
                    echo "<script>parent.$('.save_lable').text('保存失败！').css('color','red');</script>";
                }
            } else {
                $administrator_info = administrator::model()->find("_storeid=$storeid");
                $store_info = $store_model->find("storeid = $storeid");
                $this->renderPartial('editInfo', array('administrator_info' => $administrator_info, 'store_info' => $store_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 编辑院校信息 end************** */
    /*     * ************** 编辑学校是否开启活动 start************** */

    public function actioneditactive() {
        $storeid = $_POST["storeid"];
        $type = $_POST["type"];
        $activedescript = $_POST["activedescript"];

        $store_model = store::model();
        $store_info = $store_model->findByPk($storeid);

        if (count($store_info) != 0) {
            if ($type == "开启") {
                $store_info->active = 1; //开启
                $store_info->activedescript = $activedescript;
            } else {
                $store_info->active = 0; //关闭
                $store_info->activedescript = null;
            }
            if ($store_info->save()) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"保存失败"}';
                echo $json;
            }
        } else {
            $json = '{"data":"未找到此学校"}';
            echo $json;
        }
    }

    /*     * ************** 编辑学校是否开启活动 end************** */
}
