<?php

require_once './oss/samples/Common.php';

class uploadsController extends Controller {

    public function actionupload_version() {
//        file_put_contents('C:/data.txt', $_POST);
        $group_model = group::model();

        $printVesion_model = new printVersion();
        $filename = $_POST['filename'];

        $bucket = "porunoss";
        $timeout = 3600;
        $ossClient = Common::getOssClient();
        $object = $filename;

        $filenames = $ossClient->signUrl($bucket, $object, $timeout);

        $md5file = md5_file($filenames);
        $printVesion_model->versionCode = $_POST['vesioncode'];
        $printVesion_model->versionFile = $_POST['versionname'];
        $printVesion_model->updateTime = date('Y-m-d H:i:s', time());
        $printVesion_model->versiondescript = $_POST['versiondescript'];
        $printVesion_model->FileMD5 = $md5file;
        $printVesion_model->type = $_POST['type'];
        $printVesion_model->Filepath = $filename;
        $printVesion_model->_groupID = $_POST['vesiongroup'];
        $versionGroups = $_POST['vesiongroup'];
        $versionGroup = explode(',', $versionGroups); //ID 1,2,3,4		
        $printor_model = printor::model();
        if ($printVesion_model->save()) {
            if ($versionGroup[0] == "multiselect-all") {
                for ($i = 1; $i < count($versionGroup); $i++) {
                    $group_info = $group_model->find("groupID = $versionGroup[$i]");
                    $group_info->_versionId = $printVesion_model->versionId;
                    $group_info->save();
                    $print_info = $printor_model->findAll("_groupID = $versionGroup[$i]");
                    foreach ($print_info as $k => $l) {
                        $l->updatestate = "";
                        $l->save();
                    }
                }
            } else {
                for ($i = 0; $i < count($versionGroup); $i++) {
                    $group_info = $group_model->find("groupID = $versionGroup[$i]");
                    $group_info->_versionId = $printVesion_model->versionId;
                    $group_info->save();
                    $print_info = $printor_model->findAll("_groupID = $versionGroup[$i]");
                    foreach ($print_info as $k => $l) {
                        $l->updatestate = "";
                        $l->save();
                    }
                }
            }
            if (count($group_info->save()) > 0) {
//                file_put_contents('C:/data.txt', '123');
                echo '{"status": "ok"}';
            }
        }
    }

    public function actionupload_ad() {
        $advertisementname = $_POST["advertisementname"];
        $advertisementLength = $_POST["advertisementlength"];
        $startdate = $_POST["startdate"];
        $enddate = $_POST["enddate"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $groups = $_POST["group"];
        $group = explode(',', $groups); //ID 1,2,3,4		
        $company = $_POST["company"];
        $times = $_POST["times"];

        $filename = $_POST['filename'];

        $attachment_after = substr(strrchr($filename, '.'), 1);  //文件的后缀名
        $bucket = "porunoss";
        $timeout = 3600;
        $ossClient = Common::getOssClient();
        $object = $filename;

        $filenames = $ossClient->signUrl($bucket, $object, $timeout);
        $md5file = md5_file($filenames);


        $advertisement_model = new advertisement();
        $advertisement_model->_companyID = $company;
        $advertisement_model->advertisementName = $advertisementname;
        $advertisement_model->advertisementUrl = $filename;
        $advertisement_model->advertisementFormat = $attachment_after;
        $advertisement_model->saveName = $filename;
        $advertisement_model->advertisementLength = $advertisementLength;
        $advertisement_model->FileMD5 = $md5file;
        $advertisement_model->addTime = date('Y-m-d H:i:s', time());
        $advertisement_model->save();
        if ($group[0] == "multiselect-all") {
            $i = 1;
        } else {
            $i = 0;
        }
        for ($i; $i < count($group); $i++) {

            $adsdelivery_model = new adsdelivery();

//            $adsdelivery_info = $adsdelivery_model->findAll("startDate <= $startdate and endDate >= $startdate and starttime <= $starttime and endtime >=$endtime and _advertisementID = $advertisement_model->advertisementID and _groupID = $group[$i]");

            $adsdelivery_model->_advertisementID = $advertisement_model->advertisementID;
            $adsdelivery_model->_groupID = $group[$i];
            $adsdelivery_model->starttime = $starttime;
            $adsdelivery_model->endtime = $endtime;
            $adsdelivery_model->startDate = $startdate;
            $adsdelivery_model->endDate = $enddate;
            $adsdelivery_model->times = $times;
            $adsdelivery_model->addTime = date('Y-m-d H:i:s', time());
            $adsdelivery_model->save();
        }
        echo '{"status": "ok"}';
    }

    public function actionupload_problem() {
        if ($_POST['id'] == "" || $_POST['id'] == null) {
            $problem_model = new problem();
            $fileName = $_POST['filename'];
            $arrachment_name = substr($fileName, 28);
            $problem_model->problem_name = $_POST['name'];
            $problem_model->problem_describe = $_POST['descrp'];
            $problem_model->problem_type = $_POST['type'];
            $problem_model->solution = $_POST['solution'];
            $problem_model->author = $_POST['author'];
            $problem_model->attachment_name = $arrachment_name;
            $problem_model->filepath = $fileName;

            $addTime = date('y-m-d H:i:s', time());
            $problem_model->addtime = $addTime;
            $problem_model->save();
            echo '{"status": "ok"}';
        } else {
            $Id = $_POST['id'];
            $name = $_POST['name'];
            $descrp = $_POST['descrp'];
            $type = $_POST['type'];
            $solution = $_POST['solution'];
            $author = $_POST['author'];
            $fileName = $_POST['filename'];
            $arrachment_name = substr($fileName, 28);
            $problem_info = problem::model()->find("problemid=$Id");
            $problem_info->problem_name = $name;
            $problem_info->problem_describe = $descrp;
            $problem_info->problem_type = $type;
            $problem_info->solution = $solution;
            $problem_info->author = $author;
            $problem_info->attachment_name = $arrachment_name;
            $problem_info->filepath = $fileName;
            $addTime = date('y-m-d H:i:s', time());
            $problem_info->addtime = $addTime;
            $problem_info->save();
            echo '{"status": "ok"}';
        }
    }

}
