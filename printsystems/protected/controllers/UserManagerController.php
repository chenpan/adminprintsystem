<?php

include 'BaseController.php';

class userManagerController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'serInfo + serInfo',
            'userstoreDetail + userstoreDetail',
            'searchFile + searchFile',
            'searchBusiness + searchBusiness',
            'editUsers + editUsers',
            'deleteUsers + deleteUsers',
        );
    }

    public function filterserInfo($filterChain) {
        $this->checkAccess("用户", $filterChain);
    }

    public function filteruserstoreDetail($filterChain) {
        $this->checkAccess("查看积分", $filterChain);
    }

    public function filtersearchFile($filterChain) {
        $this->checkAccess("查看文件", $filterChain);
    }

    public function filtersearchBusiness($filterChain) {
        $this->checkAccess("查看订单", $filterChain);
    }

    public function filtereditUsers($filterChain) {
        $this->checkAccess("编辑用户", $filterChain);
    }

    public function filterdeleteUsers($filterChain) {
        $this->checkAccess("删除用户", $filterChain);
    }

    public function actionuserInfo() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $editUser = "hidden";
            $deleteUser = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除用户") {
                    $deleteUser = "";
                }
                if ($itemName == "编辑用户") {
                    $editUser = "";
                }
            }
            $this->renderPartial('userInfo', array('editUser' => $editUser, 'deleteUser' => $deleteUser, "leftContent" => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actionInfoToServerSideAjax() {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=default/index');
        }
        $storeId = Yii::app()->session['storeid'];
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 2:
                    $orderSql = " order by username " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by phone " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by email " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by registertime " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by _storeid " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by integration " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }
        if ($storeId == 0) {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(userid) as sum FROM tbl_user a join tbl_store b on a._storeid = b.storeid";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $business_sum_info = $result->queryAll();
            foreach ($business_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " where (a.username LIKE '%" . $search . "%' or a.phone LIKE '%" . $search . "%' or a.email LIKE '%" . $search . "%' or b.storename LIKE '%" . $search . "%' or a.integration LIKE '%" . $search . "%' or a.registertime LIKE binary '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $attachment_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($attachment_sum_filter_info[0]['sum'] > 0) {
                    foreach ($attachment_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.userid,a.username,a.phone,a.email,a.registertime,b.storename,a.integration FROM tbl_user a join tbl_store b on a._storeid = b.storeid";
        } else {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(userid) as sum FROM tbl_user a join tbl_store b on a._storeid = b.storeid where a._storeid in($storeId)";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $business_sum_info = $result->queryAll();
            foreach ($business_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.username LIKE '%" . $search . "%' or a.phone LIKE '%" . $search . "%' or a.email LIKE '%" . $search . "%' or b.storename LIKE '%" . $search . "%' or a.integration LIKE '%" . $search . "%' or a.registertime LIKE binary '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $attachment_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($attachment_sum_filter_info) > 0) {
                    foreach ($attachment_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.userid,a.username,a.phone,a.email,a.registertime,b.storename,a.integration FROM tbl_user a join tbl_store b on a._storeid = b.storeid where a._storeid in ($storeId)";
        }
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $user_info_filter = $dataResult->queryAll();
            if (count($user_info_filter) > 0) {
                $i = 0;
                foreach ($user_info_filter as $v) {
                    $i++;
                    $userid = $v['userid'];
                    $username = $v['username'];
                    $phone = $v['phone'];
                    $email = $v['email'];
                    $registertime = $v['registertime'];
                    $schoolname = $v["storename"];
                    $record_info = $v['integration'] . "点";
                    $databuf[] = array(
                        'userid' => $userid,
                        'id' => $i,
                        'username' => $username,
                        'phone' => $phone,
                        'email' => $email,
                        'registertime' => $registertime,
                        'schoolname' => $schoolname,
                        'record_info' => $record_info,
                    );
                }
                $data1 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $databuf,
                );
                echo json_encode($data1);
            } else {
                $databuf[] = array(
                    'userid' => null,
                    'id' => null,
                    'username' => null,
                    'phone' => null,
                    'email' => null,
                    'registertime' => null,
                    'schoolname' => null,
                    'record_info' => null,
                );
                $data1 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $databuf,
                );
                echo json_encode($data1);
            }
        } else {
            //直接查询所有记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $user_info = $dataResult->queryAll();
            $i = 0;
            foreach ($user_info as $v) {
                $i++;
                $userid = $v['userid'];
                $username = $v['username'];
                $phone = $v['phone'];
                $email = $v['email'];
                $registertime = $v['registertime'];
                $schoolname = $v["storename"];
                $record_info = $v['integration'] . "点";
                $databuf[] = array(
                    'userid' => $userid,
                    'id' => $i,
                    'username' => $username,
                    'phone' => $phone,
                    'email' => $email,
                    'registertime' => $registertime,
                    'schoolname' => $schoolname,
                    'record_info' => $record_info,
                );
            }
            $data1 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $databuf,
            );
            echo json_encode($data1);
        }
    }

    /*     * ************** 查找文件 ************** */

    public function actionsearchFile($userName, $phone) {
        $attachment_model = attachment::model();
        $user_model = user::model();
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset($phone) || isset($userName)) {//如果存在userName $fileName
            if ($phone == "") {
                $attachment_info = $attachment_model->findAll(array('condition' => "isdelete = 1", 'order' => 'attachmentid DESC'));
                $this->redirect('./index.php?r=userManager/files', array('attachment_info' => $attachment_info, "phone" => " ", "userName" => " ", "leftContent" => $leftContent, 'recommend' => $recommend));
            } else if ($phone != "") {
                $userName = $user_model->find(array('condition' => "phone = '$phone'"))->username;
                $userId = $user_model->find(array('condition' => "phone = '$phone'"))->userid;
                $attachment_info = $attachment_model->findAll(array('condition' => "isdelete = 1 and _userid='$userId' and storeid !=0", 'order' => 'attachmentid DESC'));
                $this->renderPartial('files', array('attachment_info' => $attachment_info, "phone" => "$phone", "userName" => " ", "leftContent" => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $attachment_info = $attachment_model->findAll(array('order' => 'attachmentid DESC'));
            $this->redirect('./index.php?r=userManager/files', array('attachment_info' => $attachment_info, "userName" => $userName, "leftContent" => $leftContent, 'recommend' => $recommend));
        }
    }

    public function actionfiles() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $attachment_model = attachment::model();
        $attachment_info = $attachment_model->findAll(array('condition' => "isdelete = 1", 'order' => 'attachmentid DESC'));
        $this->renderPartial('files', array('attachment_info' => $attachment_info, "leftContent" => $leftContent, 'recommend' => $recommend));
    }

//查询订单列表
    public function actionsearchBusiness($userName, $phone) {
        $business_model = business::model();
        $user_model = user::model();
        $recommend = $this->getrecommend();
        $leftContent = $this->getLeftContent();
        if (isset($phone)) {//如果存在$orderId $userName
            if ($phone == "" && $userName == "") {
                $business_info = $business_model->findAll(array('condition' => "isdelete = 0", 'order' => 'businessid DESC'));
                $this->redirect('./index.php?r=userManager/business', array('business_info' => $business_info, 'orderId' => " ", 'userName' => "", 'leftContent' => $leftContent, 'recommend' => $recommend));
            } else if ($phone != "" && $userName == "") {
                $username = "";
                $userId = $user_model->find(array('condition' => "phone = '$phone'"))->userid;
                $business_info = $business_model->findAll(array('condition' => "isdelete = 0 and _userid=$userId", 'order' => 'businessid DESC'));
                if (count($business_info) > 0) {
                    $this->renderPartial('business', array('business_info' => $business_info, 'userName' => "$username", 'leftContent' => $leftContent, 'recommend' => $recommend));
                } else {
                    $business_info = "";
                    $this->renderPartial('business', array('business_info' => $business_info, 'userName' => "", 'leftContent' => $leftContent, 'recommend' => $recommend));
                }
            } else if ($phone == "" && $userName != "") {
                $userId = $user_model->find(array('condition' => "username = '$userName'"))->userid;
                $business_info = $business_model->findAll(array('condition' => "isdelete = 0 and _userid=$userId", 'order' => 'businessid DESC'));
                if (count($business_info) > 0) {
                    $this->renderPartial('business', array('business_info' => $business_info, 'userName' => $userName, 'leftContent' => $leftContent, 'recommend' => $recommend));
                } else {
                    $business_info = "";
                    $this->renderPartial('business', array('business_info' => $business_info, 'userName' => "", 'leftContent' => $leftContent, 'recommend' => $recommend));
                }
            } else if ($phone != "" && $userName != "" && $userName != "null") {
                $userId = $user_model->find(array('condition' => "phone = '$phone'"))->userid;
                $business_info = $business_model->findAll(array('condition' => "isdelete = 0 and _userid=$userId", 'order' => 'businessid DESC'));
                if (count($business_info) > 0) {
                    $this->renderPartial('business', array('business_info' => $business_info, 'userName' => $userName, 'leftContent' => $leftContent, 'recommend' => $recommend));
                } else {
                    $business_info = "";
                    $this->renderPartial('business', array('business_info' => $business_info, 'userName' => "", 'leftContent' => $leftContent, 'recommend' => $recommend));
                }
            } else if ($phone != "" && $userName = "null") {
                $userId = $user_model->find(array('condition' => "phone = '$phone'"))->userid;
                $business_info = $business_model->findAll(array('condition' => "isdelete = 0 and _userid=$userId", 'order' => 'businessid DESC'));
                if (count($business_info) > 0) {
                    $this->renderPartial('business', array('business_info' => $business_info, 'userName' => "", 'leftContent' => $leftContent, 'recommend' => $recommend));
                } else {
                    $business_info = "";
                    $this->renderPartial('business', array('business_info' => $business_info, 'userName' => "", 'leftContent' => $leftContent, 'recommend' => $recommend));
                }
            }
        } else {
            $business_info = $business_model->findAll(array('condition' => "isdelete = 0", 'order' => 'businessid DESC'));
            $this->redirect('./index.php?r=userManager/business', array('business_info' => $business_info, 'userName' => $userName, 'leftContent' => $leftContent, 'recommend' => $recommend));
        }
    }

    //订单列表
    public function actionbusiness() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $business_model = business::model();
        $business_info = $business_model->findAll(array('condition' => "isdelete = 0", 'order' => 'businessid DESC'));
        $this->renderPartial('business', array('business_info' => $business_info, 'orderId' => "", 'userName' => "", 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    //订单详情
    public function actionorderDetail($businessid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $username = Yii::app()->session['adminuser'];

        $businessidd = base64_decode($businessid);

        $attachment_model = attachment::model();
        $user_model = user::model();
        $businssid_model = business::model();
        $subbusiness_model = subbusiness::model();
        $businessid_info = $businssid_model->find(array("condition" => "businessid=$businessidd"));


        $integration = (int) $user_model->find(array("condition" => "userid = '$businessid_info->_userid'"))->integration;


        $attachmentArray = array();
        $printNumber = 0; //打印

        $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId=$businessidd"));
        if ($subbusiness_info) {
            foreach ($subbusiness_info as $l => $y) {
                $attachmentId = $y->_attachmentId;
                $attachinfo = $attachment_model->find(array('condition' => "attachmentid = $attachmentId"));
                array_push($attachmentArray, array("attachmentname" => $attachinfo->attachmentname, "printNumber" => $y->printNumbers, "paidMoney" => $y->paidMoney, "printSet" => $y->printSet, "payType" => $y->payType, "isPay" => $y->isPay, "status" => $y->status, "marchineId" => $y->marchineId, "isrefund" => $y->isrefund));
            }
        }
        $this->renderPartial('orderDetail', array("businessid_info" => $businessid_info, "attachmentArray" => $attachmentArray, "username" => $username, "integration" => $integration, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    /*     * ************** 订单列表 ************** */

    /*     * ************** 编辑用户 ************** */

    public function actioneditUsers($userid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset(Yii::app()->session['adminuser'])) {
            $user_model = user::model();
            if (isset($_POST['username'])) {
                $usernamee = $_POST['username'];
                $userpsww = md5($_POST['password']);
                $phone = $_POST['phone'];
                $email = $_POST['email'];

                $user_info = $user_model->find(array('condition' => "username='$usernamee'"));
                if ($user_info) {
                    $user_info->userpsw = $userpsww;
                    $user_info->phone = $phone;
                    $user_info->email = $email;
                    if ($user_info->save()) {
                        echo "<script>parent.$('#add_success').text('保存成功！').css('color','red');</script>";
                    } else {
                        echo "<script>parent.$('#add_success').text('保存失败！').css('color','red');</script>";
                    }
                }
            } else {
                $user_model = user::model();
                $user_info = $user_model->find(array('condition' => "userid='$userid'"));
                $this->renderPartial('editUsersInfo', array('user_info' => $user_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else
            $this->redirect('./index.php?r=default/index');
    }

    /*     * ************** 编辑用户 ************** */

    //用户积分流水
    public function actionuserstoreDetail($userid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $integralDetails_models = integralDetails::model();
        $useridx = $userid;
        $integralDetails_infos = $integralDetails_models->findAll(array('condition' => "_userid = '$useridx'", 'order' => 'integralDetailsId DESC'));
        $this->renderPartial('userstoreDetail', array('integralDetails_infos' => $integralDetails_infos, "leftContent" => $leftContent, 'recommend' => $recommend));
    }

    public function actiondeleteUsers() {//删除用户
        $userid = $_POST['userid'];
        $user_model = user::model();
        $user_info = $user_model->find(array('condition' => "userid='$userid'"));
        if ($user_info) {
            $counts = $user_model->deleteAll(array('condition' => "userid='$userid'"));
            if ($counts > 0) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        }
    }

//用户文件下载
    public function actiondownload($attachmentid) {

        $attachment_model = attachment::model();
        $attachmentids = $attachmentid;
        $attachment_info = $attachment_model->find(array('condition' => "attachmentid = '$attachmentids'"));
        if (count($attachment_info) > 0) {
            $name = $attachment_info->attachmentname;
            $truename = $attachment_info->attachmentfile . "-" . $attachment_info->attachmentname;

            require_once './oss/samples/Common.php';
            $bucket = "porunoss";

            $timeout = 3600;
            $ossClient = Common::getOssClient();
            $object = $attachment_info->savepath . $truename;
            $filePath = $ossClient->signUrl($bucket, $object, $timeout);

            header('Content-Type:application/octet-stream'); //文件的类型
            Header("Accept-Ranges: bytes");
            header('Content-Disposition:attachment;filename = "' . $name . '"'); //下载显示的名字
            ob_clean();
            flush();
            readfile($filePath);
            exit();
        } else {
            $this->redirect('./index.php?r=userManager/userInfo');
        }
    }

}
