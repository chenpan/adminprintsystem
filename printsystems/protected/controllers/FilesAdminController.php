<?php

include 'BaseController.php';

class filesAdminController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function filters() {
        return array(
            'files + files',
            'download + download',
            'deleteFile + deleteFile',
        );
    }

    public function filterfiles($filterChain) {
        $this->checkAccess("文件", $filterChain);
    }

    public function filterdownload($filterChain) {

        $this->checkAccess("文件下载", $filterChain);
    }

    public function filterdeleteFile($filterChain) {
        //增加终端
        $this->checkAccess("文件删除", $filterChain);
    }

    public function actionfiles() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $flagFiles = "hidden";
            $flagDown = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "文件删除") {
                    $flagFiles = "";
                }
                if ($itemName == "文件下载") {
                    $flagDown = "";
                }
            }
            //文库分类
            $classify_model = classify::model();
            $recordsClassify1_info = $classify_model->findAll();
            foreach ($recordsClassify1_info as $value) {
                $classify_code = $value['code'];
                $classify_name = $value['name'];
                $classify[] = array(
                    'code' => $classify_code,
                    'name' => $classify_name
                );
            }
            $classify = json_encode($classify);

            $this->renderPartial('files', array("flagFiles" => $flagFiles, "flagDown" => $flagDown, "classify" => $classify, "leftContent" => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actionfilesToserverSideAjax() {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=default/index');
        }
        $storeId = Yii::app()->session['storeid'];
        $attachment_model = attachment::model();
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 4:
                    $orderSql = " order by b.username " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by b.phone " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by a.attachmentname " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by a.filenumber " . $order_dir;
                    break;
                case 8:
                    $orderSql = " order by c.storename " . $order_dir;
                    break;
                case 9:
                    $orderSql = " order by a.uploadtime " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }
        if (Yii::app()->session['storeid'] == 0) {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.attachmentid) as sum FROM tbl_attachment a join tbl_user b on a._userid = b.userid join tbl_store c on b._storeid = c.storeid where a.isdelete= 1";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $attachment_sum_info = $result->queryAll();
            foreach ($attachment_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.attachmentname LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or b.phone LIKE '%" . $search . "%'  or a.filenumber LIKE '%" . $search . "%' or a.uploadtime LIKE binary '%" . $search . "%' or c.storename LIKE binary '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $attachment_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($attachment_sum_filter_info[0]['sum'] > 0) {
                    foreach ($attachment_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.attachmentid,a.share,a._userid,a.attachmentname,a.filenumber,a.uploadtime,a._userid,b.username,b.phone,c.storename FROM tbl_attachment a join tbl_user b on a._userid = b.userid join tbl_store c on b._storeid = c.storeid where a.isdelete= 1";
        } else {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.attachmentid) as sum FROM tbl_attachment a join tbl_user b on a._userid = b.userid join tbl_store c on b._storeid = c.storeid where a.isdelete= 1 and a.storeid in ($storeId)";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $attachment_sum_info = $result->queryAll();
            foreach ($attachment_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.attachmentname LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or b.phone LIKE '%" . $search . "%' or a.filenumber LIKE '%" . $search . "%' or a.uploadtime LIKE binary '%" . $search . "%' or c.storename LIKE binary '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $attachment_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($attachment_sum_filter_info) > 0) {
                    foreach ($attachment_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.attachmentid,a.share,a._userid,a.attachmentname,a.filenumber,a.uploadtime,a._userid,b.username,b.phone,c.storename FROM tbl_attachment a join tbl_user b on a._userid = b.userid join tbl_store c on b._storeid = c.storeid where a.isdelete= 1 and a.storeid in ($storeId)";
        }
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $attachment_info_filter = $dataResult->queryAll();
            if (count($attachment_info_filter) > 0) {
                $i = 0;
                foreach ($attachment_info_filter as $v) {
                    $i++;
                    $name = $v['username'];
                    $phone = $v['phone'];
                    $userId = $v['_userid'];
                    $isShare = $v['share'];
                    $attachmentId = $v['attachmentid'];
                    $attachment = $v['attachmentname'];
                    $filenumber = $v['filenumber'] . "页";
                    $school = $v['storename'];
                    $uploadtime = $v['uploadtime'];
                    $data1[] = array(
                        'attachmentid' => $attachmentId,
                        'userId' => $userId,
                        'isShare' => $isShare,
                        'id' => $i,
                        'name' => $name,
                        'phone' => $phone,
                        'attachment' => $attachment,
                        'filenumber' => $filenumber,
                        'school' => $school,
                        'uploadtime' => $uploadtime,
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            } else {
                $data1[] = array(
                    'attachmentid' => null,
                    'userId' => NULL,
                    'id' => null,
                    'name' => null,
                    'phone' => null,
                    'attachment' => null,
                    'filenumber' => null,
                    'filenumber' => null,
                    'uploadtime' => null,
                );
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
            //直接查询所有记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $attachment_info = $dataResult->queryAll();
            $i = 0;
            foreach ($attachment_info as $v) {
                $i++;
                $name = $v['username'];
                $phone = $v['phone'];
                $userId = $v['_userid'];
                $isShare = $v['share'];
                $attachmentId = $v['attachmentid'];
                $attachment = $v['attachmentname'];
                $filenumber = $v['filenumber'] . "页";
                $school = $v['storename'];
                $uploadtime = $v['uploadtime'];
                $data1[] = array(
                    'attachmentid' => $attachmentId,
                    'userId' => $userId,
                    'isShare' => $isShare,
                    'id' => $i,
                    'name' => $name,
                    'phone' => $phone,
                    'attachment' => $attachment,
                    'filenumber' => $filenumber,
                    'school' => $school,
                    'uploadtime' => $uploadtime,
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

    /*     * ************** 查找文件 ************** */

    public function actiondownload($attachmentid) {

        $attachment_model = attachment::model();
        $attachmentids = $attachmentid;
        $attachment_info = $attachment_model->find(array('condition' => "attachmentid = '$attachmentids'"));
        if (count($attachment_info) > 0) {
            $name = $attachment_info->attachmentname;
            $truename = $attachment_info->attachmentfile . "-" . $attachment_info->attachmentname;

            require_once './oss/samples/Common.php';
            $bucket = "porunoss";

            $timeout = 3600;
            $ossClient = Common::getOssClient();
            $object = $attachment_info->savepath . $truename;
            $filePath = $ossClient->signUrl($bucket, $object, $timeout);

            header('Content-Type:application/octet-stream'); //文件的类型
            Header("Accept-Ranges: bytes");
            header('Content-Disposition:attachment;filename = "' . $name . '"'); //下载显示的名字
            ob_clean();
            flush();
            readfile($filePath);
            exit();
        } else {
            $this->redirect('./index.php?r=filesAdmin/files');
        }
    }

//删除文件
    public function actiondeleteFile() {
        $attachmentid = $_POST['attachmentid']; //文件ID
        $attachment_model = attachment::model();
        $attachment_info = $attachment_model->find(array('condition' => "attachmentid = $attachmentid"));
        $attachment_info->isdelete = 0;

        if ($attachment_info->save()) {
            echo"success";
        } else {
            echo 'false';
        }
    }

    //分享文档
    public function actionshareFile() {
        header('Access-Control-Allow-Origin: *');
//        header("Access-Control-Allow-Origin:http://dev.cqutprint.com");
        $attachmentid = intval($_POST['attachmentid']); //文档ID
        $userId = intval($_POST['userId']);
        $filename = $_POST['filename']; //文档名
        $classify = $_POST['classify']; //文档分类
        $credit = intval($_POST['credit']); //文档分享所需积分 
        $keyword = $_POST['keyword']; //文档标签
        $describe = $_POST['describe']; //文档描述

        $url = "http://www.cqutprint.com/Library/shareFile";
        $post_data = array('attachmentid' => $attachmentid, 'userid' => 1027, 'filename' => $filename, 'classify' => $classify, 'credit' => $credit, 'keyword' => $keyword, 'describe' => $describe);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
// 返回结果 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
// 使用POST提交 
        curl_setopt($ch, CURLOPT_POST, 1);
// POST参数 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
// 结果 
        $res = curl_exec($ch);
        curl_close($ch);
        echo $res;
    }

    /*     * ************** 文件列表 ************** */
}
