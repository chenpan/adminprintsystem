<?php

include 'BaseController.php';

class adminController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function actionIndex() {
        $storeSessionId = Yii::app()->session['storeid'];
        $administrator_id = Yii::app()->session['administrator_id'];
        $connection = Yii::app()->db; //admin
        $connection2 = Yii::app()->db2; //home
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            if ($storeSessionId != 0) {
                //覆盖校园
                $total_store = "select count(storeid) as total_store from tbl_store where storeid in ($storeSessionId)";
                foreach ($connection2->createCommand($total_store)->query() as $k => $v) {
                    $store_all = $v['total_store'];
                }
//会员量
                $total_user = "select count(userid) as total_user from tbl_user where _storeid in ($storeSessionId)";
                foreach ($connection2->createCommand($total_user)->query() as $k => $v) {
                    $user_all = $v['total_user'];
                }
//在外总积分
                $integration = "select sum(integration) as totalPoints from tbl_user where _storeid in ($storeSessionId)";
                foreach ($connection2->createCommand($integration)->query() as $k => $v) {
                    $totalPoints = $v['totalPoints'];
                }
//终端数量
                if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                    $machine_count = "select count(printorId) as total_machine_count from tbl_printor where _storeid in ($storeSessionId)";
                    foreach ($connection2->createCommand($machine_count)->query() as $k => $v) {
                        $total_machine_count = $v['total_machine_count'];
                    }
                } else {
                    $machine_count = "select count(*) as total_machine_count from tbl_agent_printor where administrator_id = $administrator_id";
                    foreach ($connection2->createCommand($machine_count)->query() as $k => $v) {
                        $total_machine_count = $v['total_machine_count'];
                    }
                }


                //转化率
                $conversions = "SELECT count(userid) as conversions FROM tbl_user WHERE userid IN (SELECT _userid FROM tbl_business) and _storeid in ($storeSessionId)";
                foreach ($connection2->createCommand($conversions)->query() as $k => $v) {
                    $conversionx = $v['conversions'];
                }
                if ($user_all != 0) {
                    $conversion = sprintf("%.2f", ($conversionx / $user_all) * 100);
                } else {
                    $conversion = 0.00;
                }
            } else {
                //覆盖校园
                $total_store = "select count(storeid) as total_store from tbl_store";
                foreach ($connection2->createCommand($total_store)->query() as $k => $v) {
                    $store_all = $v['total_store'];
                }
                //会员量

                $total_user = "select count(userid) as total_user from tbl_user";
                foreach ($connection2->createCommand($total_user)->query() as $k => $v) {
                    $user_all = $v['total_user'];
                }

//在外总积分
                $integration = "select sum(integration) as totalPoints from tbl_user";
                foreach ($connection2->createCommand($integration)->query() as $k => $v) {
                    $totalPoints = $v['totalPoints'];
                }
//终端数量
                if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                    $machine_count = "select count(printorId) as total_machine_count from tbl_printor";
                } else {
                    $machine_count = "select count(printor_id) as total_machine_count from tbl_agent_printor where administrator_id = $administrator_id";
                }
                foreach ($connection2->createCommand($machine_count)->query() as $k => $v) {
                    $total_machine_count = $v['total_machine_count'];
                }
                //转化率
                $conversions = "SELECT count(userid) as conversions FROM tbl_user WHERE userid IN (SELECT _userid FROM tbl_business)";
                foreach ($connection2->createCommand($conversions)->query() as $k => $v) {
                    $conversionx = $v['conversions'];
                }
                if ($user_all != 0) {
                    $conversion = sprintf("%.2f", ($conversionx / $user_all) * 100);
                } else {
                    $conversion = 0.00;
                }
            }
            //  一周收入统计

            $sqlx = "select _sessionId from tbl_refund WHERE _sessionId is not null";
            $rowx = $connection2->createCommand($sqlx)->query();
            $str = "";
            foreach ($rowx as $k => $v) {
                $str .= $v['_sessionId'] . ",";
            }
            $str = substr($str, 0, -1);
            if (strlen($str) == 0) {
                $str = 0;
            }

            $dayArray = array();
            $dayOrder = array();
            $dayReg = array();
            $dayRefund = array();
            $totalIncome_dayss = 0; //收入统计
            $totalCount_dayss = 0; //订单统计
            $regCount_dayss = 0; //注册人数统计
            $totalrefund_dayss = 0; //退款统计
            //统计当天的退款订单量和退款金额
            $refundrateCount_terminal = 0; //终端订单退款率
            $refundrateCount_web = 0; //web端订单退款率
            $refundrateCount_total = 0; //订单总退款
            $refundrateCount_terminal_total = 0; //终端订单退款率
            $refundrateCount_web_total = 0; //web端订单退款率
            $refundrateCount_total_total = 0; //订单总退款率

            for ($i = 6; $i >= 0; $i--) {
                $time = date('Y-m-d', strtotime('-' . $i . 'day'));
                $stime = $time . " 00:00:00";
                $etime = $time . " 23:59:59";
                //*****************退款统计***************************//
                $totalrefund_days = 0;
                if ($storeSessionId == 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //钱                
                        $refund_infox = "select sum(money) as total_refund_money from tbl_refund where applyTime between '$stime' and '$etime' and tborderId != '' ";
                        //积分                        
                        $refund_infos = "select sum(Integral) as total_refund_integral from tbl_refund where applyTime between '$stime' and '$etime' and refundType = 5 and Integral != 0 and tborderId != ''";

                        foreach ($connection2->createCommand($refund_infox)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_money'];
                        }
                        foreach ($connection2->createCommand($refund_infos)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_integral'] / 100;
                        }
                    } else {
                        //终端订单退的钱
                        $refund_info_pm = "select sum(a.money) as total_refund_money from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        //线上订单退的钱
                        $refund_info_sm = "select sum(a.money) as total_refund_money from tbl_refund a join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        //终端订单退的积分
                        $refund_info_pp = "select sum(a.Integral) as total_refund_integral from tbl_refund a"
                                . " join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        //线上订单退的积分
                        $refund_info_sp = "select sum(a.Integral) as total_refund_integral from tbl_refund a"
                                . " join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        foreach ($connection2->createCommand($refund_info_pm)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_money'];
                        }
                        foreach ($connection2->createCommand($refund_info_sm)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_money'];
                        }
                        foreach ($connection2->createCommand($refund_info_pp)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_integral'] / 100;
                        }
                        foreach ($connection2->createCommand($refund_info_sp)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_integral'] / 100;
                        }
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //钱                
                        $refund_infox = "select sum(money) as total_refund_money from tbl_refund where applyTime between '$stime' and '$etime' and tborderId != '' and _storeid in ($storeSessionId)";
                        //积分                        
                        $refund_infos = "select sum(Integral) as total_refund_integral from tbl_refund where applyTime between '$stime' and '$etime' and refundType = 5 and Integral != 0 and tborderId != '' and _storeid in ($storeSessionId)";

                        foreach ($connection2->createCommand($refund_infox)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_money'];
                        }
                        foreach ($connection2->createCommand($refund_infos)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_integral'] / 100;
                        }
                    } else {
                        //终端订单退的钱
                        $refund_info_pm = "select sum(a.money) as total_refund_money from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        //线上订单退的钱
                        $refund_info_sm = "select sum(a.money) as total_refund_money from tbl_refund a join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        //终端订单退的积分
                        $refund_info_pp = "select sum(a.Integral) as total_refund_integral from tbl_refund a"
                                . " join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and a._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        //线上订单退的积分
                        $refund_info_sp = "select sum(a.Integral) as total_refund_integral from tbl_refund a"
                                . " join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and a._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        foreach ($connection2->createCommand($refund_info_pm)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_money'];
                        }
                        foreach ($connection2->createCommand($refund_info_sm)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_money'];
                        }
                        foreach ($connection2->createCommand($refund_info_pp)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_integral'] / 100;
                        }
                        foreach ($connection2->createCommand($refund_info_sp)->query() as $k => $v) {
                            $totalrefund_days += $v['total_refund_integral'] / 100;
                        }
                    }
                }

                $totalrefund_dayss += sprintf("%.2f", $totalrefund_days);
                array_push($dayRefund, array('totalrefund_days' => sprintf("%.2f", $totalrefund_days), 'day' => $time = date('m-d', strtotime('-' . $i . 'day'))));
                //****************注册人数***********************//
                $regCount_days = 0;
                if ($storeSessionId == 0) {
                    $reg_infos = "select count(userid) as total_reg from tbl_user where registertime between '$stime' and '$etime'";
                    foreach ($connection2->createCommand($reg_infos)->query() as $k => $v) {
                        $regCount_days = $v['total_reg'];
                    }
                } else {
                    $reg_infos = "select count(userid) as total_reg from tbl_user where registertime between '$stime' and '$etime' and _storeid in ($storeSessionId)";
                    foreach ($connection2->createCommand($reg_infos)->query() as $k => $v) {
                        $regCount_days = $v['total_reg'];
                    }
                }

                $regCount_dayss += $regCount_days;
                array_push($dayReg, array('regCount_days' => $regCount_days, 'day' => $time = date('m-d', strtotime('-' . $i . 'day'))));
                //****************订单统计******************//
                $totalCount_days_terminal = 0; //终端订单量
                $totalCount_days_web = 0; //web端订单量
                $totalCount_refund_terminal = 0; //终端退款订单量
                $totalCount_refund_web = 0; //web端退款订单量

                if ($storeSessionId == 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $business_infos = "select count(businessid) as total_business from tbl_business where placeOrdertime between '$stime' and '$etime'";
                        $prbusiness_info = "select count(prbusinessid) as total_prbusiness from tbl_prbusiness where starttime between '$stime' and '$etime' and _storeid !='' and trade_no !=''";
                        //web订单退款量
                        $refund_infoq = "select count(distinct a.businessid) as total_refund_order from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " where c.applyTime between '$stime' and '$etime' and c.subbusinessId is not null";
//                        $refund_infoq = "select count(a.refundId) as total_refund_order from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is not null";
                        //终端退款量
                        $refund_infox = "select count(refundId) as total_refund_order from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null";
                    } else {
                        $business_infos = "SELECT count(distinct a.businessid) as total_business FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $prbusiness_info = "select count(a.prbusinessid) as total_prbusiness from tbl_prbusiness a"
                                . " join db_home.tbl_printor b on a.machineId = b.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a._storeid !='' and a.trade_no !=''"
                                . " and b.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        //web订单退款量
                        $refund_infoq = "select count(distinct a.businessid) as total_refund_order from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " join tbl_printor d on b.marchineId = d.machineId"
                                . " where c.applyTime between '$stime' and '$etime' and c.tborderId != ''"
                                . " and d._storeid in ($storeSessionId) and d.printorId in (" . Yii::app()->session['printor_id'] . ")";
//                        $refund_infoq = "select count(a.refundId) as total_refund_order from tbl_refund a join tbl_subbusiness b on a.tborderId = b.trade_no"
//                                . " join tbl_printor c on b.marchineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
//                                . " and a._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
//                        $refund_infoq = "select count(*) as total_refund_order from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is not null";
                        //终端退款量
//                        $refund_infox = "select count(*) as total_refund_order from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null";
                        $refund_infox = "select count(a.refundId) as total_refund_order from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $business_infos = "select count(businessid) as total_business from tbl_business where placeOrdertime between '$stime' and '$etime' and _storeid in ($storeSessionId)";
                        $prbusiness_info = "select count(prbusinessid) as total_prbusiness from tbl_prbusiness where starttime between '$stime' and '$etime' and _storeid in ($storeSessionId) and _storeid !='' and trade_no !=''";
                        //web订单退款量
                        $refund_infoq = "select count(distinct a.businessid) as total_refund_order from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " where c.applyTime between '$stime' and '$etime' and c.subbusinessId is not null and a._storeid in ($storeSessionId)";
//                        $refund_infoq = "select count(*) as total_refund_order from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is not null and _storeid in ($storeSessionId)";
                        //终端退款量
                        $refund_infox = "select count(refundId) as total_refund_order from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null and _storeid in ($storeSessionId)";
                    } else {
                        $business_infos = "SELECT count(distinct a.businessid) as total_business FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId"
                                . " WHERE a.placeOrdertime between '$stime' and '$etime' and c._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $prbusiness_info = "select count(a.prbusinessid) as total_prbusiness from tbl_prbusiness a"
                                . " join db_home.tbl_printor b on a.machineId = b.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a._storeid !='' and a.trade_no !=''"
                                . " and a._storeid in ($storeSessionId) and b.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        //web订单退款量

                        $refund_infoq = "select count(distinct a.businessid) as total_refund_order from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " join tbl_printor d on b.marchineId = d.machineId"
                                . " where c.applyTime between '$stime' and '$etime' and c.tborderId != ''"
                                . " and d._storeid in ($storeSessionId) and c._storeid in ($storeSessionId) and d.printorId in (" . Yii::app()->session['printor_id'] . ")";
//                        $refund_infoq = "select count(a.refundId) as total_refund_order from tbl_refund a join tbl_subbusiness b on a.tborderId = b.trade_no"
//                                . " join tbl_printor c on b.marchineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
//                                . " and c._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
//                        $refund_infoq = "select count(*) as total_refund_order from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is not null";
                        //终端退款量
//                        $refund_infox = "select count(*) as total_refund_order from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null";
                        $refund_infox = "select count(a.refundId) as total_refund_order from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($business_infos)->query() as $k => $v) {
                    $totalCount_days_web = $v['total_business'];
                }
                foreach ($connection->createCommand($prbusiness_info)->query() as $k => $v) {
                    $totalCount_days_terminal = $v['total_prbusiness'];
                }
                foreach ($connection2->createCommand($refund_infoq)->query() as $k => $v) {
                    $totalCount_refund_web = $v['total_refund_order'];
                }
                foreach ($connection2->createCommand($refund_infox)->query() as $k => $v) {
                    $totalCount_refund_terminal = $v['total_refund_order'];
                }
                $totalCount_dayss += $totalCount_days_web + $totalCount_days_terminal;
                if ($i == 1) {
                    //终端订单退款率
                    $refundrateCount_terminal = $totalCount_refund_terminal;
                    $refundrateCount_terminal_total = $totalCount_days_terminal;
                    //WEB端订单退款率
                    $refundrateCount_web = $totalCount_refund_web;
                    $refundrateCount_web_total = $totalCount_days_web;
                    //总的退款率
                    $refundrateCount_total = $totalCount_refund_terminal + $totalCount_refund_web;
                    $refundrateCount_total_total = $totalCount_days_web + $totalCount_days_terminal;
                }
                array_push($dayOrder, array('totalCount_days_web' => $totalCount_days_web, 'totalCount_days_terminal' => $totalCount_days_terminal, 'day' => $time = date('m-d', strtotime('-' . $i . 'day')), 'totalCount_refund_terminal' => $totalCount_refund_terminal, 'totalCount_refund_web' => $totalCount_refund_web));
                //************收入统计************//
                $totalIncome_day = 0;
                $totalIncome_points_day = 0;
                if (Yii::app()->session['storeid'] == 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $subbusiness_infox = "select sum(paidMoney) as total_money from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0";
                        $subbusiness_points_infox = "select sum(paidMoney) as total_money from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and payType = 5 and isrefund = 0";
                        $prbusiness_infox = "select sum(paidmoney) as total_money from tbl_prbusiness where sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''";
                        $prbusiness_points_infox = "select sum(paidmoney) as total_money from tbl_prbusiness where sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $subbusiness_infox = "select sum(a.paidMoney) as total_money from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $subbusiness_points_infox = "select sum(a.paidMoney) as total_money from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5 and a.isrefund = 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $prbusiness_infox = "select sum(a.paidmoney) as total_money from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $prbusiness_points_infox = "select sum(a.paidmoney) as total_money from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $subbusiness_infox = "select sum(paidMoney) as total_money from tbl_subbusiness where _businessId in (select businessid from tbl_business where _storeid in (" . $storeSessionId . ")) and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0";
                        $subbusiness_points_infox = "select sum(paidMoney) as total_money from tbl_subbusiness where _businessId in (select businessid from tbl_business where _storeid in (" . $storeSessionId . ")) and payTime between '$stime' and '$etime' and ispay = 1 and payType = 5 and isrefund = 0";
                        $prbusiness_infox = "select sum(paidmoney) as total_money from tbl_prbusiness where sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and _storeid in ($storeSessionId)";
                        $prbusiness_points_infox = "select sum(paidmoney) as total_money from tbl_prbusiness where sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5 and _storeid in ($storeSessionId)";
                    } else {
                        $subbusiness_infox = "select sum(a.paidMoney) as total_money from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in (" . $storeSessionId . "))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $subbusiness_points_infox = "select sum(a.paidMoney) as total_money from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5 and a.isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in (" . $storeSessionId . "))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $prbusiness_infox = "select sum(a.paidmoney) as total_money from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and a._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $prbusiness_points_infox = "select sum(a.paidmoney) as total_money from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and a._storeid in ($storeSessionId) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($subbusiness_points_infox)->query() as $k => $v) {
                    $totalIncome_points_day += $v['total_money'] * 100;
                }
                foreach ($connection->createCommand($prbusiness_points_infox)->query() as $k => $v) {
                    $totalIncome_points_day += $v['total_money'] * 100;
                }
                foreach ($connection2->createCommand($subbusiness_infox)->query() as $k => $v) {
                    $totalIncome_day += $v['total_money'];
                }
                foreach ($connection->createCommand($prbusiness_infox)->query() as $k => $v) {
                    $totalIncome_day += $v['total_money'];
                }
                $totalIncome_dayss += sprintf("%.2f", $totalIncome_day);
                array_push($dayArray, array('totalIncome_day' => sprintf("%.2f", $totalIncome_day), 'totalIncome_points_day' => sprintf("%.2f", $totalIncome_points_day), 'day' => $time = date('m-d', strtotime('-' . $i . 'day'))));
            }
            $this->renderPartial('index', array('leftContent' => $leftContent, 'recommend' => $recommend, 'store_all' => $store_all, 'user_all' => $user_all, 'totalPoints' => $totalPoints, 'total_machine_count' => $total_machine_count, 'dayRefund' => $dayRefund, 'dayOrder' => $dayOrder, 'dayReg' => $dayReg, 'dayArray' => $dayArray, 'regCount_dayss' => $regCount_dayss, 'totalrefund_dayss' => $totalrefund_dayss, 'totalCount_dayss' => $totalCount_dayss, 'totalIncome_dayss' => $totalIncome_dayss, 'refundrateCount_terminal' => $refundrateCount_terminal, 'refundrateCount_terminal_total' => $refundrateCount_terminal_total, 'refundrateCount_web' => $refundrateCount_web, 'refundrateCount_web_total' => $refundrateCount_web_total, 'refundrateCount_total' => $refundrateCount_total, 'refundrateCount_total_total' => $refundrateCount_total_total, 'storeSessionId' => $storeSessionId, "conversion" => $conversion));
        } else {
            $this->redirect('./index.php?r = default/index');
        }
    }

//实时刷新数据
    public function actiongetData() {
        $storeSessionId = Yii::app()->session['storeid'];
        $prbusiness_model = prbusiness::model();
        $subbusiness_model = subbusiness::model();
        $business_model = business::model();
        $refund_model = refund::model();
        $register_model = user::model();

        $connection = Yii::app()->db2;
        $sqlx = "select _sessionId from tbl_refund WHERE _sessionId is not null";
        $rowx = $connection->createCommand($sqlx)->query();
        $str = "";
        foreach ($rowx as $k => $v) {
            $str .= $v['_sessionId'] . ",";
        }
        $str = substr($str, 0, -1);


        $dayRefund = array();
        $totalIncome_dayss = 0; //收入统计
        $totalCount_dayss = 0; //订单统计
        $regCount_dayss = 0; //注册人数统计
        $totalrefund_dayss = 0; //退款统计       
        for ($i = 6; $i >= 0; $i--) {
            $time = date('Y-m-d', strtotime('-' . $i . 'day'));
            $stime = $time . " 00:00:00";
            $etime = $time . " 23:59:59";
            //*****************退款统计***************************//
            $totalrefund_days = 0;
            $regCount_days = 0;
            $totalCount_days = 0;
            $totalIncome_day = 0;
            $totalIncome_points_day = 0;
            $totalCount_days_terminal = 0; //终端订单量
            $totalCount_refund_terminal = 0; //终端退款订单量
            $totalCount_refund_web = 0; //web端退款订单量

            if ($storeSessionId == 0) {
//钱
                $refund_infox = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime", array(':stime' => $stime, ':etime' => $etime));
//积分
                $refund_infos = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime and refundType = 5", array(':stime' => $stime, ':etime' => $etime));
                $reg_infos = $register_model->findAllBySql("select * from tbl_user where registertime between :stime and :etime", array(':stime' => $stime, ':etime' => $etime));
                $regCount_days += count($reg_infos);

                $business_infos = $business_model->findAllBySql("select * from tbl_business where placeOrdertime between :stime and :etime", array(':stime' => $stime, ':etime' => $etime));
                $totalCount_days += count($business_infos);

                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "_storeid !='' AND starttime >='$stime' AND starttime <='$etime'"));
                $totalCount_days_terminal = count($prbusiness_info);

                //web订单退款量
                $refund_infoq = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime and subbusinessId is not null", array(':stime' => $stime, ':etime' => $etime));
                $totalCount_refund_web = count($refund_infoq);

                //终端退款量
                $refund_infow = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime and subbusinessId is null", array(':stime' => $stime, ':etime' => $etime));
                $totalCount_refund_terminal = count($refund_infow);

                $subbusiness_infox = $subbusiness_model->findAllBySql("select paidMoney from tbl_subbusiness where payTime between :stime and :etime and ispay = 1 and isrefund != 1", array(':stime' => $stime, ':etime' => $etime));
                //积分支付
                $subbusiness_points_infox = $subbusiness_model->findAllBySql("select paidMoney from tbl_subbusiness where payTime between :stime and :etime and ispay = 1 and payType =5 and isrefund != 1", array(':stime' => $stime, ':etime' => $etime));
                $prbusiness_infox = $prbusiness_model->findAllBySql("select paidmoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and payType = 5 and starttime between :stime and :etime and _storeid is not null", array(':stime' => $stime, ':etime' => $etime));
            } else {
//钱
                $refund_infox = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime and _storeid = " . $storeSessionId, array(':stime' => $stime, ':etime' => $etime));
//积分
                $refund_infos = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime and refundType = 5 and _storeid = " . $storeSessionId, array(':stime' => $stime, ':etime' => $etime));
                $reg_infos = $register_model->findAllBySql("select * from tbl_user where registertime between :stime and :etime and _storeid = :storeid", array(':stime' => $stime, ':etime' => $etime, ':storeid' => $storeSessionId));
                $regCount_days += count($reg_infos);
                $business_infos = $business_model->findAllBySql("select * from tbl_business where placeOrdertime between :stime and :etime and _storeid = :storeid", array(':stime' => $stime, ':etime' => $etime, ':storeid' => $storeSessionId));
                $totalCount_days += count($business_infos);

                //终端订单量
                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "_storeid !='' AND starttime >='$stime' AND starttime <='$etime' AND _storeid in ($storeSessionId)"));
                $totalCount_days_terminal = count($prbusiness_info);

                //终端退款量
                $refund_infoq = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime and subbusinessId is not null and _storeid in ($storeSessionId)", array(':stime' => $stime, ':etime' => $etime));
                $totalCount_refund_terminal = count($refund_infoq);
                //web订单退款量
                $refund_infow = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime and subbusinessId is null and _storeid in ($storeSessionId)", array(':stime' => $stime, ':etime' => $etime));
                $totalCount_refund_web = count($refund_infow);

                $subbusiness_infox = $subbusiness_model->findAllBySql("select paidMoney from tbl_subbusiness where tbl_subbusiness._businessId in (select businessid from tbl_business where _storeid = " . $storeSessionId . ") and payTime between :stime and :etime and ispay = 1 and isrefund != 1", array(':stime' => $stime, ':etime' => $etime));
                $prbusiness_infox = $prbusiness_model->findAllBySql("select paidmoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and starttime between :stime and :etime and _storeid =" . $storeSessionId, array(':stime' => $stime, ':etime' => $etime));
            }
            foreach ($refund_infox as $k => $l) {
                $totalrefund_days += $l->money;
            }
            foreach ($refund_infos as $k => $l) {
                $totalrefund_days += $l->consumptionIntegral / 100;
            }
            foreach ($subbusiness_points_infox as $k => $v) {
                $totalIncome_points_day += $v['paidMoney'] * 100;
            }
            foreach ($subbusiness_infox as $k => $v) {
                $totalIncome_day += $v['paidMoney'];
            }
            foreach ($prbusiness_infox as $k => $v) {
                $totalIncome_day += $v['paidmoney'];
            }
            $totalIncome_dayss += $totalIncome_day;
            $regCount_dayss += $regCount_days;
            $totalrefund_dayss += $totalrefund_days;
            $totalCount_dayss += $totalCount_days + $totalCount_days_terminal;
            array_push($dayRefund, array('totalrefund_days' => $totalrefund_days, 'totalIncome_day' => $totalIncome_day, 'totalIncome_points_day' => $totalIncome_points_day, 'totalCount_days' => $totalCount_days, 'totalCount_days_terminal' => $totalCount_days_terminal, 'regCount_days' => $regCount_days, 'totalCount_refund_terminal' => $totalCount_refund_terminal, 'totalCount_refund_web' => $totalCount_refund_web, 'day' => $time = date('m-d', strtotime('-' . $i . 'day'))));
        }
        array_push($dayRefund, array('totalrefund_dayss' => $totalrefund_dayss, 'totalCount_dayss' => $totalCount_dayss, 'totalIncome_dayss' => $totalIncome_dayss, 'regCount_dayss' => $regCount_dayss, 'regCount_dayss' => $regCount_dayss));
        echo json_encode($dayRefund);
    }

//注销
    public function actionLogout() {
        unset(Yii::app()->session['adminuser']);
        if (!isset(Yii::app()->session['adminuser'])) {
            $this->redirect('./index.php?r = default/index');
        }
    }

//下载APP
    public function actiondownloadApp() {
        $filename = 'http://android.cqutprint.com/appVersion/cqutprintAdmin.apk';
        header('Content-Type:application/octet-stream'); //文件的类型
        Header("Accept-Ranges: bytes");
        header('Content-Disposition:attachment;
                filename = "cqutprintAdmin.apk"'); //下载显示的名字
        ob_clean();
        flush();
        readfile($filename);
        exit();
    }

    public function actionModify() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $issetadmin = Yii::app()->session['adminuser']; //用户名
        if (isset($issetadmin)) {
            $administrator_model = administrator::model();
            if (isset($_POST['password'])) {

                $adminInfo = $administrator_model->find("username = '$issetadmin'");
                $adminInfo->password = md5($_POST['password']);

                if ($adminInfo->save()) {
                    $json = ' {
                    "data":"success"
                }';
                    echo $json;
                } else {
                    $json = ' {
                    "data":"false"
                }';
                    echo $json;
                }
            } else {
                $this->renderPartial('modifyInfo', array('administrator_model' => $administrator_model, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        }
    }

    public function actiontestiframe() {
        for ($i = 6; $i >= 0; $i--) {
            $stime = date('Y-m-d', strtotime('-' . $i . 'day'));
            echo $stime . "*";
        }
//        echo date('Y-m-d H:i:s',strtotime('-18 day'));
//        $recommend = $this->getrecommend();
//        $this->renderPartial('testiframe', array('recommend' => $recommend));
    }

//    public function actionstatic() {
//        $business_model = business::model();
//        $subbusiness_model = subbusiness::model();
//        $refund_model = refund::model();
//        $user_model = user::model();
//        $prbusiness_model = prbusiness::model();
//        $printor_model = printor::model();
//        $store_model = store::model();
//        $store_info = $store_model->findAll();
////订单
//
//        $user_info = $user_model->find(array("order" => "userid asc", "limit" => 1));
//        //$starttime = $user_info->registertime; //2015-9-28
//        $starttime = "2015-11-01 00:00:00";
//        $syear = date("Y", strtotime($starttime));
//        $smonth = date("m", strtotime($starttime));
//        $day = date("d", strtotime($starttime));
//        $days = date("t", strtotime($starttime)); //当月有多少天
////        for ($x = $smonth; $x <= 12; $x++) {
//        for ($i = $day; $i <= $days; $i++) {
//            if ($i != $days) {
//                $stime = $syear . "-" . $smonth . "-" . $i . " " . "00:00:00";
//                $etime = $syear . "-" . $smonth . "-" . ($i + 1) . " " . "00:00:00";
//            } else {
//                $stime = $syear . "-" . $smonth . "-" . $i . " " . "00:00:00";
//                $etime = $syear . "-" . $smonth . "-" . $i . " " . "23:59:59";
//            }
//            $moneyincome_day = 0; //一天金钱收入
//            $pointincome_day = 0; //一天积分收入
//            $refuncmoney_day = 0; //一天金钱退款
//            $refundpoint_day = 0; //一天积分退款
//            foreach ($store_info as $K => $L) {
////学校
////当天订单量
//                $business_info = $business_model->findAll(array('condition' => "placeOrdertime >='$stime' AND placeOrdertime <='$etime' AND _storeid = $L->storeid"));
////U盘打印量
//                $prbusiness_info = $prbusiness_model->findAll(array('condition' => "starttime >='$stime' AND starttime <='$etime' AND exit_phrase >=5 AND _storeid = $L->storeid"));
////金钱收入  积分收入
//                $subbusiness_info = $subbusiness_model->findAll(array('condition' => "payTime >='$stime' AND payTime <='$etime' AND isPay =1 AND isrefund!=1"));
//
//                foreach ($subbusiness_info as $O => $P) {
//                    if ($P->payType != 5)
//                        $moneyincome_day += $P->paidMoney;
//                    else if ($P->payType == 5)
//                        $pointincome_day += $P->paidMoney * 100;
//                }
//                foreach ($prbusiness_info as $pr) {
//
//                    $refund_info = $refund_model->find("_sessionId=$pr->sessionId");
//                    if (count($refund_info) == 0) {
//                        $moneyincome_day += $pr->paidmoney;
//                    } else if (count($refund_info) != 0 && $refund_info->statue != 1) {
//                        $moneyincome_day += $pr->paidmoney;
//                    }
//                }
//
////金钱退款  积分退款  statue 1 已退款
//                $refund_info = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType != 5 AND _storeid = $L->storeid"));
//                foreach ($refund_info as $k => $l) {
//                    $refuncmoney_day += $l->money;
//                }
//                $refund_infox = $refund_model->findAll(array('condition' => "refundTime >='$stime' AND refundTime <='$etime' AND statue = 1 AND refundType = 5 AND _storeid = $L->storeid"));
//                foreach ($refund_infox as $k => $l) {
//                    $refundpoint_day += $l->consumptionIntegral;
//                }
//
////注册用户
//                $user_infos = $user_model->findAll(array('condition' => "registertime >='$stime' AND registertime <='$etime' AND _storeid = $L->storeid"));
//
//                $services_day = 0;
//                $page_day = 0;
//                $printor_info = $printor_model->findAll(array('condition' => "_storeid = $L->storeid"));
//                foreach ($printor_info as $l => $y) {
//
////服务人数
//                    $totalservice = 0;
//                    $machineId = $y->machineId;
//                    $subbusiness_infoz = $subbusiness_model->findAll(array('condition' => "printTime >='$stime' AND printTime <='$etime' AND status = 1 AND marchineId = '$machineId'"));
//
//                    $prbusiness_info = $prbusiness_model->findAll(array('condition' => "starttime >='$stime' AND starttime <='$etime' AND exit_phrase >=5 AND machineId = '$machineId' AND printset != ''"));
//                    $totalservice = count($subbusiness_infoz) + count($prbusiness_info);
//                    $services_day +=$totalservice;
//
//                    $page_day_one = 0;
////打印纸张数
//                    foreach ($subbusiness_infoz as $q => $w) {
//                        $filePage = explode(', ', $w->printSet);
//                        foreach ($filePage as $z => $x) {
//                            if (is_numeric($x)) {
//                                $page_day_one += 1;
//                                $page_day += 1;
//                            } else {
//                                $filePag = explode('-', $x);
//                                $filePa = $filePag[1] - $filePag[0] + 1;
//                                $page_day_one += (int) $filePa;
//                                $page_day +=(int) $filePa;
//                            }
//                        }
//                    }
//                    foreach ($prbusiness_info as $q => $w) {
//                        $filePage = explode(', ', $w->printset);
//                        foreach ($filePage as $z => $x) {
//                            if (is_numeric($x)) {
//                                $page_day_one += 1;
//                                $page_day += 1;
//                            } else {
//                                $filePag = explode('-', $x);
//                                $filePa = $filePag[1] - $filePag[0] + 1;
//                                $page_day_one += (int) $filePa;
//                                $page_day +=(int) $filePa;
//                            }
//                        }
//                    }
////U盘打印量
//                    $prbusiness_infoq = $prbusiness_model->findAll(array('condition' => "starttime >='$stime' AND starttime <='$etime' AND exit_phrase >=5 AND machineId = '$machineId'"));
//
//                    $printorstatistics_model = new printorstatistics();
//                    if ($stime == "2015-10-01 00:00:00") {
//                        $printorstatistics_info = $printorstatistics_model->find(array('condition' => "printorid = $y->printorId AND daytime = '2015-09-30'"));
//                    } else if ($stime == "2015-11-01 00:00:00") {
//                        $printorstatistics_info = $printorstatistics_model->find(array('condition' => "printorid = $y->printorId AND daytime = '2015-10-31'"));
//                    } else if ($stime == "2015-12-01 00:00:00") {
//                        $printorstatistics_info = $printorstatistics_model->find(array('condition' => "printorid = $y->printorId AND daytime = '2015-11-30'"));
//                    } else if ($stime == "2016-01-01 00:00:00") {
//                        $printorstatistics_info = $printorstatistics_model->find(array('condition' => "printorid = $y->printorId AND daytime = '2015-12-31'"));
//                    } else {
//                        $printorstatistics_info = $printorstatistics_model->find(array('condition' => "printorid = $y->printorId AND daytime = '" . $syear . "-" . $smonth . "-" . ($i - 1) . "'"));
//                    }
//
//                    $printorstatistics_model->printorid = $y->printorId;
//                    $printorstatistics_model->Udisk_day = count($prbusiness_infoq); //一天U盘打印量
//                    $printorstatistics_model->paper_day = $page_day_one; //一天总共打印纸张
//                    $printorstatistics_model->service_day = $totalservice; //一天总共服务人数
//                    $printorstatistics_model->daytime = $syear . "-" . $smonth . "-" . $i; //当天
//                    $printorstatistics_model->addtime = date('Y-m-d H:i:s', time()); //加入时间
//                    if (count($printorstatistics_info) == 0) {
//                        $printorstatistics_model->Udisk_total = count($prbusiness_infoq); //总共U盘打印量
//                        $printorstatistics_model->paper_total = $page_day_one; //总共打印纸张数
//                        $printorstatistics_model->service_total = $totalservice; //总共服务人数
//                    } else {
////                            $printorstatistics_infos = $printorstatistics_model->findAll(array('condition' => "printorid = $y->printorId AND daytime = " . $syear . " - " . $x . " - " . ($day - 1)));
//
//                        $printorstatistics_model->Udisk_total = $printorstatistics_info->Udisk_total + count($prbusiness_infoq); //总共U盘打印量
//                        $printorstatistics_model->paper_total = $printorstatistics_info->paper_total + $page_day_one; //总共打印纸张数
//                        $printorstatistics_model->service_total = $printorstatistics_info->service_total + $totalservice; //总共服务人数
//                    }
//                    $printorstatistics_model->save();
//                }
//                $schoolstatistics_model = new schoolstatistics();
//                if ($stime == "2015-10-01 00:00:00") {
//                    $schoolstatistics_info = $schoolstatistics_model->find(array('condition' => "_storeid = $L->storeid AND daytime = '2015-09-30'"));
//                } else if ($stime == "2015-11-01 00:00:00") {
//                    $schoolstatistics_info = $schoolstatistics_model->find(array('condition' => "_storeid = $L->storeid AND daytime = '2015-10-31'"));
//                } else if ($stime == "2015-12-01 00:00:00") {
//                    $schoolstatistics_info = $schoolstatistics_model->find(array('condition' => "_storeid = $L->storeid AND daytime = '2015-11-30'"));
//                } else if ($stime == "2016-01-01 00:00:00") {
//                    $schoolstatistics_info = $schoolstatistics_model->find(array('condition' => "_storeid = $L->storeid AND daytime = '2015-12-31'"));
//                } else {
//                    $schoolstatistics_info = $schoolstatistics_model->find(array('condition' => "_storeid = $L->storeid AND daytime = '" . $syear . "-" . $smonth . "-" . ($i - 1) . "'"));
//                }
//
//                $schoolstatistics_model->_storeid = $L->storeid; //storeid
//                $schoolstatistics_model->Udisk_day = count($prbusiness_info); //U盘打印量
//                $schoolstatistics_model->order_day = count($business_info); //一天订单量
//                $schoolstatistics_model->moneyincome_day = $moneyincome_day; //一天的金钱收入
//                $schoolstatistics_model->pointincome_day = $pointincome_day; //一天积分收入
//                $schoolstatistics_model->refundmoney_day = $refuncmoney_day; //一天金钱退款
//                $schoolstatistics_model->refundpoint_day = $refundpoint_day; //一天积分退款
//                $schoolstatistics_model->register_day = count($user_infos); //一天注册人数
//                $schoolstatistics_model->paper_day = $page_day; //一天打印纸张数
//                $schoolstatistics_model->service_day = $services_day; //一天打印纸张数
//                $schoolstatistics_model->daytime = $syear . "-" . $smonth . "-" . $i; //当天
//                $schoolstatistics_model->addtime = date('Y-m-d H:i:s', time()); //加入时间
////如果没有数据，总的数据就是当天的数据
//                if (count($schoolstatistics_info) == 0) {
//                    $schoolstatistics_model->Udisk_total = count($prbusiness_info); //U盘总打印量
//                    $schoolstatistics_model->order_total = count($business_info); //一天订单量
//                    $schoolstatistics_model->moneyincome_total = $moneyincome_day; //一天的金钱收入
//                    $schoolstatistics_model->pointincome_total = $pointincome_day; //一天积分收入
//                    $schoolstatistics_model->refundmoney_total = $refuncmoney_day; //一天金钱退款
//                    $schoolstatistics_model->refundpoint_total = $refundpoint_day; //一天积分退款
//                    $schoolstatistics_model->register_total = count($user_infos); //一天注册人数
//                    $schoolstatistics_model->paper_total = $page_day; //一天打印纸张数
//                    $schoolstatistics_model->service_total = $services_day; //一天打印纸张数
//                } else {
////                        $schoolstatistics_infos = $schoolstatistics_model->findAll(array('condition' => "_storeid = $L->storeid AND daytime = " . $syear . " - " . $x . " - " . ($day - 1)));
//                    $schoolstatistics_model->Udisk_total = $schoolstatistics_info->Udisk_total + count($prbusiness_info); //U盘总打印量
//                    $schoolstatistics_model->order_total = $schoolstatistics_info->order_total + count($business_info); //一天订单量
//                    $schoolstatistics_model->moneyincome_total = $schoolstatistics_info->moneyincome_total + $moneyincome_day; //一天的金钱收入
//                    $schoolstatistics_model->pointincome_total = $schoolstatistics_info->pointincome_total + $pointincome_day; //一天积分收入
//                    $schoolstatistics_model->refundmoney_total = $schoolstatistics_info->refundmoney_total + $refuncmoney_day; //一天金钱退款
//                    $schoolstatistics_model->refundpoint_total = $schoolstatistics_info->refundpoint_total + $refundpoint_day; //一天积分退款
//                    $schoolstatistics_model->register_total = $schoolstatistics_info->register_total + count($user_infos); //一天注册人数
//                    $schoolstatistics_model->paper_total = $schoolstatistics_info->paper_total + $page_day; //一天打印纸张数
//                    $schoolstatistics_model->service_total = $schoolstatistics_info->service_total + $services_day; //一天打印纸张数
//                }
//                $schoolstatistics_model->save();
//            }
//        }
////        }
//    }
}
