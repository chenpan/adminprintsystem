<?php

include 'BaseController.php';

class advertisementsController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'companyInfo + companyInfo', //公司管理
            'addcompany + addcompany',
            'addCompanys + addCompanys',
            'editcompany + editcompany',
            'editCompanys + editCompanys',
            'deleteCompany + deleteCompany',
            'advertisementInfo + advertisementInfo', //广告管理
            'addadvertisement + addadvertisement',
            'editAds + editAds',
            'DeleteAds + DeleteAds',
            'saveads + saveads',
        );
    }

    public function filtercompanyInfo($filterChain) {
        $this->checkAccess("公司管理", $filterChain);
    }

    public function filteraddcompany($filterChain) {
        $this->checkAccess("添加公司", $filterChain);
    }

    public function filteraddCompanys($filterChain) {
        $this->checkAccess("添加公司", $filterChain);
    }

    public function filtereditCompanys($filterChain) {
        $this->checkAccess("编辑公司", $filterChain);
    }

    public function filtereditcompany($filterChain) {
        $this->checkAccess("编辑公司", $filterChain);
    }

    public function filteradvertisementInfo($filterChain) {
        $this->checkAccess("广告管理", $filterChain);
    }

    public function filteraddadvertisement($filterChain) {
        $this->checkAccess("添加广告", $filterChain);
    }

    public function filtereditAds($filterChain) {
        $this->checkAccess("编辑广告", $filterChain);
    }

    public function filtereDeleteAds($filterChain) {
        $this->checkAccess("删除广告", $filterChain);
    }

    /*     * ************** 公司信息 start************** */

    public function actioncompanyInfo() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $deleteCompany = "hidden";
            $addCompany = "hidden";
            $editCompany = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除公司") {
                    $deleteCompany = "";
                }
                if ($itemName == "添加公司") {
                    $addCompany = "";
                }
                if ($itemName == "编辑公司") {
                    $editCompany = "";
                }
            }
            $company_model = company::model();
            $company_info = $company_model->findAll(array('order' => "companyID DESC"));
            $this->renderPartial('companyInfo', array('company_info' => $company_info, 'deleteCompany' => $deleteCompany, 'addCompany' => $addCompany, 'editCompany' => $editCompany, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 公司信息 end************** */

    public function actionaddcompany() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $this->renderPartial('addcompany', array('leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actionaddCompanys() {
        $companyName = $_POST["name"];
        $companyPhone = $_POST["phone"];
        $companyAddress = $_POST["address"];
        $linkMan = $_POST["man"];


        if (isset(Yii::app()->session['adminuser'])) {
            $company_model = new company();
            $company_info = $company_model->find("companyName='$companyName'");
            if (count($company_info) != 0) {
                $json = '{"data":"exist"}';
                echo $json;
            } else {
                $company_model->companyName = $companyName;
                $company_model->companyPhone = $companyPhone;
                $company_model->companyAddress = $companyAddress;
                $company_model->linkMan = $linkMan;
                date_default_timezone_set('PRC');
                $company_model->addTime = date('Y-m-d H:i:s', time());

                if ($company_model->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actiondeleteCompany() {
        if (isset(Yii::app()->session['adminuser'])) {
            $companyID = $_POST["ID"];

            $company_model = company::model();
            $company_info = $company_model->find("companyID='$companyID'");

            if (count($company_info) != 0) {
                if ($company_info->delete()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"no"}';
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actioneditcompany($companyID) {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $company_model = new company();
            $company_info = $company_model->find("companyID='$companyID'");

            $this->renderPartial('editCompany', array('company_info' => $company_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actioneditCompanys() {
        $companyID = $_POST["companyID"];
        $companyName = $_POST["name"];
        $companyPhone = $_POST["phone"];
        $companyAddress = $_POST["address"];
        $linkMan = $_POST["man"];

        if (isset(Yii::app()->session['adminuser'])) {
            $company_model = company::model();
            $company_info = $company_model->find("companyID='$companyID'");

            if (count($company_info) != 0) {
                $company_info->companyName = $companyName;
                $company_info->companyPhone = $companyPhone;
                $company_info->linkMan = $linkMan;
                $company_info->companyAddress = $companyAddress;


                if ($company_info->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"no"}';
                echo $json;
            }
        } else
            $this->redirect('./index.php?r=default/index');
    }

    /*     * *********************** 公司管理  *************************** */

    /*     * *********************** 广告管理  *************************** */

    public function actionadvertisementInfo() {

        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();

        if (isset(Yii::app()->session['adminuser'])) {
            $advertisement_model = advertisement::model();
            $advertisement_info = $advertisement_model->findAll(array('order' => "advertisementID DESC"));
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $deleteAds = "hidden";
            $addAds = "hidden";
            $editAds = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "添加广告") {
                    $addAds = "";
                }
                if ($itemName == "编辑广告") {
                    $editAds = "";
                }
                if ($itemName == "删除广告") {
                    $deleteAds = "";
                }
            }

            $this->renderPartial('advertisementInfo', array('advertisement_info' => $advertisement_info, 'deleteAds' => $deleteAds, 'addAds' => $addAds, 'editAds' => $editAds, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else
            $this->redirect('./index.php?r=default/index');
    }

    public function actionaddadvertisement($groupID = 0) {
        $group_model = group::model();
        $group_info = $group_model->findAll();

        $company_model = new company();
        $company_info = $company_model->findAll();

        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();

        if (isset(Yii::app()->session['adminuser'])) {
            $this->renderPartial('addadvertisement', array('group_info' => $group_info, 'groupID' => $groupID, 'company_info' => $company_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actionaddadvertisements() {
        if (isset(Yii::app()->session['adminuser'])) {

            $startdate = $_POST["startdate"];
            $advertisementLength = $_POST["advertisementLength"];
            $enddate = $_POST["enddate"];
            $starttime = $_POST["starttime"];
            $endtime = $_POST["endtime"];
            $group = $_POST["group"];
            $company = $_POST["company"];
            $times = $_POST["times"];

            $advertisement_model = new advertisement();

            $adsdelivery_model = new adsdelivery();

            if ($_FILES["file"]["error"] > 0) {
                echo '<script>$("#file_error").text("上传文件错误！");</script>';
            } else {
                $fileName = $_FILES["file"]["name"];
                $advertisement_info = $advertisement_model->find(array("condition" => "_companyID=$company AND advertisementName = '$fileName'"));
                if (count($advertisement_info) != 0) {
                    echo '<script>alert("文件已存在！");window.location.href="./index.php?r=advertisements/addadvertisement&companyID=' . $company . '"</script>';
                } else {
                    $attachment_after = substr(strrchr($fileName, '.'), 1);  //文件的后缀名

                    $saveName = $company . '-' . date('YmdHis');

                    $targetPath = './assets/advertisements';
                    $targetFile = rtrim($targetPath, '/') . '/' . $saveName . "." . $attachment_after;


                    $url = dirname(Yii::app()->BasePath); //C:\xampp\htdocs\printsystem\printsystem
                    $advertisement_model->_companyID = $company;
                    $advertisement_model->advertisementName = $fileName;
                    $advertisement_model->advertisementUrl = $url . "\\assets\\advertisements\\";
                    $advertisement_model->advertisementFormat = $attachment_after;
                    $advertisement_model->saveName = $saveName . "." . $attachment_after;
                    $advertisement_model->advertisementLength = $advertisementLength;
                    $advertisement_model->FileMD5 = md5_file($_FILES["file"]["tmp_name"]);

                    date_default_timezone_set('PRC');
                    $advertisement_model->addTime = date('Y-m-d H:i:s', time());

                    move_uploaded_file($_FILES["file"]["tmp_name"], iconv("UTF-8", "gb2312", $targetFile));

//                $vtime = exec("ffmpeg -i " . $_FILES["file"]["name"] . " 2>&1 | grep Duration | cut -d -f 4 | sed s/,//"); //CuPlayer.com提示总长度 
                    //$duration = explode(":",$time); 
                    // $duration_in_seconds = $duration[0]*3600 + $duration[1]*60+ round($duration[2]);//CuPlayer.com提示转化为秒 
//                return array(vtime => $vtime,
//                    ctime => $ctime
//                );
                    if ($advertisement_model->save()) {
                        $adsdelivery_model->_advertisementID = $advertisement_model->advertisementID;
                        $adsdelivery_model->_groupID = $group;
                        $adsdelivery_model->starttime = $starttime;
                        $adsdelivery_model->endtime = $endtime;
                        $adsdelivery_model->startDate = $startdate;
                        $adsdelivery_model->endDate = $enddate;
                        $adsdelivery_model->times = $times;
                        date_default_timezone_set('PRC');
                        $adsdelivery_model->addTime = date('Y-m-d H:i:s', time());

                        if ($adsdelivery_model->save()) {
                            echo "<script>parent.$('#add_success').text('保存成功！').css('color','red');</script>";
                        } else {
                            echo "<script>parent.$('#add_success').text('保存失败！').css('color','red');</script>";
                        }
                    }
                }
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*
     * 编辑广告
     */

    public function actioneditAds($advertisementID, $id = 0) {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $group_model = group::model();
            $group_info = $group_model->findAll();

            $company_model = new company();
            $company_info = $company_model->findAll();

            $advertisement_model = advertisement::model();
            $delivery = adsdelivery::model();

            $advertisement_info = $advertisement_model->find("advertisementID=" . $advertisementID);
            $delivery_info = $delivery->find("_advertisementID=" . $advertisementID);
            $delivery_infox = $delivery->findAll("_advertisementID=" . $advertisementID);
            $group = "";
            foreach ($delivery_infox as $k => $l) {
                $group .= "'" . $l->_groupID . "'" . ",";
            }
            $group = substr($group, 0, -1);
            $this->renderPartial("editAds", array('leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info, 'company_info' => $company_info, 'advertisement_info' => $advertisement_info, 'group' => $group, 'delivery_info' => $delivery_info, 'id' => $id));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*
     * 编辑广告并保存
     */

    public function actioneditad() {
        $advertisementID = $_POST["advertisementID"];
        $advertisementLength = $_POST["advertisementLength"];
        $advertisementName = $_POST["advertisementName"];
        $group = $_POST["group"];
        $company = $_POST["company"];

        $advertisement_model = advertisement::model();
        $adsdelivery_model = adsdelivery::model();

        $advertisement_info = $advertisement_model->find("advertisementID=$advertisementID");

        if (count($advertisement_info) == 0) {
            echo '<script>alert("广告不存在！");"</script>';
        } else {
            $advertisement_info->_companyID = $company;
            $advertisement_info->advertisementName = $advertisementName;
            $advertisement_info->advertisementLength = $advertisementLength;
            foreach ($group as $k => $l) {//先循环组，看播放列表里面没有这个组的就加上
                $adsdelivery_info = $adsdelivery_model->find("_advertisementID = $advertisementID and _groupID = $l");
                if (count($adsdelivery_info) == 0) {
                    $adsdelivery_infos = $adsdelivery_model->find("_advertisementID = $advertisementID");
                    $adsdelivery_models = new adsdelivery();
                    $adsdelivery_models->_advertisementID = $advertisementID;
                    $adsdelivery_models->_groupID = $l;
                    $adsdelivery_models->starttime = $adsdelivery_infos->starttime;
                    $adsdelivery_models->endtime = $adsdelivery_infos->endtime;
                    $adsdelivery_models->startDate = $adsdelivery_infos->startDate;
                    $adsdelivery_models->endDate = $adsdelivery_infos->endDate;
                    $adsdelivery_models->times = $adsdelivery_infos->times;
                    $adsdelivery_models->addTime = date('Y-m-d H:i:s', time());
                    $adsdelivery_models->save();
                }
            }
            $adsdelivery_infox = $adsdelivery_model->findAll("_advertisementID = $advertisementID");
            foreach ($adsdelivery_infox as $k => $l) {//再循环播放策略，有多的就删除掉
                if (!in_array($l->_groupID, $group)) {
                    $adsdelivery_model->deleteByPk($l->adsDeliveryID);
                }
            }

            if ($advertisement_info->save()) {
                echo "<script>parent.$('#add_success').text('保存成功！').css('color','red');</script>";
            } else {
                echo "<script>parent.$('#add_success').text('保存失败！').css('color','red');</script>";
            }
        }
    }

    public function actiondelads() {
        $advertisement_model = advertisement::model();
        $advertisementID = $_POST['advertisementID'];

        $adsdelivery_model = adsdelivery::model();
        $adsdelivery_model->deleteAll("_advertisementID = $advertisementID");

        if (count($advertisement_model->deleteByPk($advertisementID)) > 0) {
            $json = '{"data":"success"}';
        } else {
            $json = '{"data":"false"}';
        }
        echo $json;
    }

    //下载广告
    public function actiondownloadadvertisements($advertisementID) {

        $advertisement_model = advertisement::model();
        $advertisement_info = $advertisement_model->find(array('condition' => "advertisementID = '$advertisementID'"));

        if (count($advertisement_info) != 0) {

            require_once './oss/samples/Common.php';
            $bucket = "porunoss";

            $timeout = 3600;
            $ossClient = Common::getOssClient();
            $object = $advertisement_info->saveName;
            $filePath = $ossClient->signUrl($bucket, $object, $timeout);
            $name = $advertisement_info->advertisementName . '.' . $advertisement_info->advertisementFormat;

            header('Content-Type:application/octet-stream'); //文件的类型
            Header("Accept-Ranges: bytes");
            header('Content-Disposition:attachment;filename = "' . $name . '"'); //下载显示的名字
            ob_clean();
            flush();
            readfile($filePath);
            exit();
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//查看分组
    public function actionlookgroup($advertisementID) {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $adsdelivery_model = adsdelivery::model();
            $advertisement_model = advertisement::model();
            $advertisement_info = $advertisement_model->findByPk($advertisementID);
            $advertisementname = $advertisement_info->advertisementName . '.' . $advertisement_info->advertisementFormat;
            $group_model = group::model();
            $adsdelivery_info = $adsdelivery_model->findAll("_advertisementID = $advertisementID");
            $groupidarray = array();
            if (count($adsdelivery_info) != 0) {
                foreach ($adsdelivery_info as $key => $value) {
                    array_push($groupidarray, $value->_groupID);
                }
            }
            $criteria = new CDbCriteria;
            $criteria->addInCondition('groupID', $groupidarray); //代表where id IN (1,2,3,4,5,);  
            $group_info = $group_model->findAll($criteria);
            $this->renderPartial('lookgroup', array('group_info' => $group_info, 'advertisementID' => $advertisementID, 'advertisementname' => $advertisementname, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//编辑播放策略页面
    public function actioneditadsdelivery($groupID, $advertisementID, $id = 0) {
        if (isset(Yii::app()->session['adminuser'])) {
            $adsdelivery_model = adsdelivery::model();
            $group_model = group::model();
            $advertisement_model = advertisement::model();


            $adsdelivery_info = $adsdelivery_model->find("_advertisementID = $advertisementID and _groupID = $groupID");

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            if (count($adsdelivery_info) != 0) {

                $group_info = $group_model->findAll();

                $advertisement_info = $advertisement_model->find("advertisementID=" . $advertisementID);

                $this->renderPartial("editadsdelivery", array('leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info, 'advertisement_info' => $advertisement_info, 'groupID' => $groupID, 'adsdelivery_info' => $adsdelivery_info, 'id' => $id));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//保存播放策略
    public function actioneditadsdeliverys() {
        $advertisementID = $_POST["advertisementID"];
        $startdate = $_POST["startdate"];
        $enddate = $_POST["enddate"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $group = $_POST["group"];
        $times = $_POST["times"];

        $statue = false;

        $adsdelivery_model = adsdelivery::model();
        if ($group[0] == "multiselect-all") {
            for ($i = 1; $i < count($group); $i++) {
                $adsdelivery_info = $adsdelivery_model->find("_advertisementID=$advertisementID and _groupID = $group[$i]");
                if (count($adsdelivery_info) > 0) {
                    $adsdelivery_info->starttime = $starttime;
                    $adsdelivery_info->endtime = $endtime;
                    $adsdelivery_info->startDate = $startdate;
                    $adsdelivery_info->endDate = $enddate;
                    $adsdelivery_info->times = $times;
                    if ($adsdelivery_info->save()) {
                        $statue = true;
                    }
                } else {
                    $adsdelivery_model = new adsdelivery();
                    $adsdelivery_model->_advertisementID = $advertisementID;
                    $adsdelivery_model->_groupID = $group[$i];
                    $adsdelivery_model->starttime = $starttime;
                    $adsdelivery_model->endtime = $endtime;
                    $adsdelivery_model->startDate = $startdate;
                    $adsdelivery_model->endDate = $enddate;
                    $adsdelivery_model->times = $times;
                    $adsdelivery_model->addTime = date('Y-m-d H:i:s', time());
                    if ($adsdelivery_model->save()) {
                        $statue = true;
                    }
                }
            }
        } else {
            for ($i = 0; $i < count($group); $i++) {
                $adsdelivery_info = $adsdelivery_model->find("_advertisementID=$advertisementID and _groupID = $group[$i]");
                if (count($adsdelivery_info) > 0) {
                    $adsdelivery_info->starttime = $starttime;
                    $adsdelivery_info->endtime = $endtime;
                    $adsdelivery_info->startDate = $startdate;
                    $adsdelivery_info->endDate = $enddate;
                    $adsdelivery_info->times = $times;
                    if ($adsdelivery_info->save()) {
                        $statue = true;
                    }
                } else {
                    $adsdelivery_model = new adsdelivery();
                    $adsdelivery_model->_advertisementID = $advertisement_info->advertisementID;
                    $adsdelivery_model->_groupID = $group[$i];
                    $adsdelivery_model->starttime = $starttime;
                    $adsdelivery_model->endtime = $endtime;
                    $adsdelivery_model->startDate = $startdate;
                    $adsdelivery_model->endDate = $enddate;
                    $adsdelivery_model->times = $times;
                    $adsdelivery_model->addTime = date('Y-m-d H:i:s', time());
                    if ($adsdelivery_model->save()) {
                        $statue = true;
                    }
                }
            }
        }
        if ($statue) {
            echo "<script>parent.$('#add_success').text('保存成功！').css('color','red');</script>";
        } else {
            echo "<script>parent.$('#add_success').text('保存失败！').css('color','red');</script>";
        }
    }

    //删除播放策略
    public function actiondeladsdelivery() {

        $advertisementID = $_POST['advertisementID'];
        $groupID = $_POST['groupID'];

        $adsdelivery_model = adsdelivery::model();


        if (count($adsdelivery_model->deleteAll("_advertisementID = $advertisementID and _groupID = $groupID")) > 0) {
            $json = '{"data":"success"}';
        } else {
            $json = '{"data":"false"}';
        }
        echo $json;
    }

    /*     * *********************** 广告管理  *************************** */
}
