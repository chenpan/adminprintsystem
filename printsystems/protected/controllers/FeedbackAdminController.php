
<?php

include 'BaseController.php';

class feedbackAdminController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*     * ************** 反馈 ************** */

    public function filters() {
        return array(
            'userfeedback + userfeedback'
        );
    }

    public function filteruserfeedback($filterChain) {
        $this->checkAccess("反馈", $filterChain);
    }

    public function actionuserfeedback() {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $this->renderPartial('userfeedback', array('leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actionuserfeedbackAjax() {//后台分页
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=default/index');
        }
        $storeId = Yii::app()->session['storeid'];
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 0:
                    $orderSql = " order by a.feedbackid " . $order_dir;
                    break;
                case 1:
                    $orderSql = " order by a.phone " . $order_dir;
                    break;
                case 2:
                    $orderSql = " order by c.storename " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by a.content " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by a.email " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by a.url " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by a.time " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by a.statue " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页 
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }

        if ($storeId == 0) {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.feedbackid) as sum FROM tbl_feedback a left join tbl_user b ON a.phone = b.phone left join tbl_store c ON b._storeid = c.storeid";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $message_sum_info = $result->queryAll();
            foreach ($message_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            $sumSqlWhere = " where (a.phone LIKE '%" . $search . "%' or c.storename LIKE '%" . $search . "%' or a.content LIKE '%" . $search . "%' or a.url LIKE '%" . $search . "%' or a.email LIKE '%" . $search . "%' or a.time LIKE binary '%" . $search . "%' or a.statue LIKE '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $message_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($message_sum_filter_info[0]['sum'] > 0) {
                    foreach ($message_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.feedbackid,a.phone,a.content,a.time,b.username,c.storename,a.email,a.url,a.statue FROM tbl_feedback a"
                    . " left join tbl_user b ON a.phone = b.phone left join tbl_store c ON b._storeid = c.storeid";
        } else {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.feedbackid) as sum FROM tbl_feedback a left join tbl_user b ON a.phone = b.phone left join tbl_store c ON b._storeid = c.storeid where c.storeid in ($storeId)";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $message_sum_info = $result->queryAll();
            foreach ($message_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.phone LIKE '%" . $search . "%' or c.storename LIKE '%" . $search . "%' or a.email LIKE '%" . $search . "%'  or a.url LIKE '%" . $search . "%' or a.content LIKE '%" . $search . "%' or a.url LIKE '%" . $search . "%' or a.email LIKE '%" . $search . "%' or a.time LIKE binary '%" . $search . "%' or a.statue LIKE '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $message_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($message_sum_filter_info) > 0) {
                    foreach ($message_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.feedbackid,a.phone,a.content,a.time,b.username,c.storename,a.email,a.url,a.statue FROM tbl_feedback a"
                    . " left join tbl_user b ON a.phone = b.phone left join tbl_store c ON b._storeid = c.storeid and c.storeid in ($storeId)";
        }
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $adataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $message_info_filter = $adataResult->queryAll();
            if (count($message_info_filter) > 0) {
                $i = 0;
                foreach ($message_info_filter as $v) {
                    $i ++;
                    $id = $i;
                    $feedbackid = $v['feedbackid'];
                    $phone = $v['phone'];
                    $storename = $v['storename'];
                    $addtime = $v['time'];
                    $content = $v['content'];
                    $email = $v["email"];
                    $url = $v["url"];
                    $statue = $v["statue"];
                    $adata1[] = array(
                        'id' => $id,
                        'phone' => $phone,
                        'storename' => $storename,
                        'content' => $content,
                        'email' => $email,
                        'url' => $url,
                        'addtime' => $addtime,
                        'statue' => $statue,
                        'feedbackid' => $feedbackid
                    );
                }
                $adata2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $adata1
                );
                echo $data = json_encode($adata2);
            }
        } else {
            //直接查询所有记录
            $adataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $message_info_filter = $adataResult->queryAll();
            $i = 0;
            foreach ($message_info_filter as $v) {
                $i ++;
                $id = $i;
                $feedbackid = $v['feedbackid'];
                $phone = $v['phone'];
                $storename = $v['storename'];
                $addtime = $v['time'];
                $content = $v['content'];
                $email = $v["email"];
                $url = $v["url"];
                $statue = $v["statue"];
                $adata1[] = array(
                    'id' => $id,
                    'feedbackid' => $feedbackid,
                    'phone' => $phone,
                    'storename' => $storename,
                    'content' => $content,
                    'email' => $email,
                    'url' => $url,
                    'addtime' => $addtime,
                    'statue' => $statue
                );
            }
            $adata2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $adata1
            );
            echo $data = json_encode($adata2);
        }
    }

    public function actionedit() {
        $feedbackid = $_POST["feedbackid"];
        $feedback_model = feedback::model();
        $feedback_info = $feedback_model->findByPk($feedbackid);

        if (count($feedback_info) != 0) {
            $feedback_info->statue = "已处理";
            if ($feedback_info->save()) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"fail"}';
                echo $json;
            }
        }
    }

}
