<?php

include 'BaseController.php';

class printorController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'printor + printor', //终端信息
            'addprintor + addprintor',
            'deleteterninal + deleteterninal',
            'editprintor + editprintor',
            'printormonitor + printormonitor', //终端监控
            'group + group', //终端分组
            'addgroup + addgroup',
            'editgroup + editgroup',
            'deleteGroup + deleteGroup',
            'printversion + printversion', //终端版本
            'addversion + addversion', //终端监控
            'deleteversion + deleteversion', //终端监控
            'editversion + editversion', //终端监控
            'printerror + printerror', //打印错误信息
        );
    }

    public function filterprintor($filterChain) {
        $this->checkAccess("终端信息", $filterChain);
    }

    public function filteraddprintor($filterChain) {
        $this->checkAccess("添加终端", $filterChain);
    }

    public function filterdeleteterninal($filterChain) {
        $this->checkAccess("删除终端", $filterChain);
    }

    public function filtereditprintor($filterChain) {
        $this->checkAccess("编辑终端", $filterChain);
    }

    public function filterprintormonitor($filterChain) {
        $this->checkAccess("终端监控", $filterChain);
    }

    public function filtergroup($filterChain) {
        $this->checkAccess("终端分组", $filterChain);
    }

    public function filteraddgroup($filterChain) {
        $this->checkAccess("添加分组", $filterChain);
    }

    public function filtereditgroup($filterChain) {
        $this->checkAccess("编辑分组", $filterChain);
    }

    public function filterdeleteGroup($filterChain) {
        $this->checkAccess("删除分组", $filterChain);
    }

    public function filterprintversion($filterChain) {
        $this->checkAccess("终端版本", $filterChain);
    }

    public function filteraddversion($filterChain) {
        $this->checkAccess("添加版本", $filterChain);
    }

    public function filterdeleteversion($filterChain) {
        $this->checkAccess("删除版本", $filterChain);
    }

    public function filtereditversion($filterChain) {
        $this->checkAccess("编辑版本", $filterChain);
    }

    public function filterprinterror($filterChain) {
        $this->checkAccess("错误信息", $filterChain);
    }

    /*     * ************** 终端信息 start************** */

    public function actionprintor() {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $addPrintor = "hidden";
            $editPrintor = "hidden";
            $deletePrintor = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "添加终端") {
                    $addPrintor = "";
                }
                if ($itemName == "编辑终端") {
                    $editPrintor = "";
                }
                if ($itemName == "删除终端") {
                    $deletePrintor = "";
                }
            }


            $this->renderPartial('printor', array('leftContent' => $leftContent, 'addPrintor' => $addPrintor, 'editPrintor' => $editPrintor, 'deletePrintor' => $deletePrintor, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 终端信息分页 start************** */

    public function actionprintortopage() {
        $storeId = Yii::app()->session['storeid'];
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 2:
                    $orderSql = " order by a.machineId " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by a.printorName " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by a.address " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by a.picture " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by c.storename " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by b.groupName " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页 
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }
        if ($storeId == 0) {
            //定义查询数据总记录数sql
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $sumSql = "SELECT count(a.machineId) as sum FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID join tbl_store c on a._storeId=c.storeid where 1";
            } else {
                $sumSql = "SELECT count(a.machineId) as sum FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID "
                        . " join tbl_store c on a._storeId=c.storeid "
                        . " join db_admin.tbl_agent_printor d on a.printorId = d.printor_id where d.administrator_id = " . Yii::app()->session['administrator_id'];
            }
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $printor_sum_info = $result->queryAll();
            foreach ($printor_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.machineId LIKE '%" . $search . "%' "
                    . "or a.printorName LIKE '%" . $search . "%' "
                    . "or c.storename LIKE '%" . $search . "%'"
                    . "or a.address LIKE '%" . $search . "%' "
                    . "or b.groupName LIKE '%" . $search . "%' "
                    . "or a.picture LIKE '%" . $search . "%' )";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $printor_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($printor_sum_filter_info[0]['sum'] > 0) {
                    foreach ($printor_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $totalResultSql = "SELECT a.printorId,a.machineId,a.printorName,a.address,c.storename,b.groupName,a.picture"
                        . " FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID join tbl_store c on a._storeId=c.storeid where 1";
            } else {
                $totalResultSql = "SELECT a.printorId,a.machineId,a.printorName,a.address,c.storename,b.groupName,a.picture"
                        . " FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID join tbl_store c on a._storeId=c.storeid"
                        . " join db_admin.tbl_agent_printor d on a.printorId = d.printor_id where d.administrator_id = " . Yii::app()->session['administrator_id'];
            }
        } else {
            //定义查询数据总记录数sql
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $sumSql = "SELECT count(a.machineId) as sum FROM tbl_printor a  join db_admin.tbl_group b on a._groupID = b.groupID"
                        . "  join tbl_store c on a._storeId=c.storeid where a._storeId in ($storeId)";
            } else {
                $sumSql = "SELECT count(a.machineId) as sum FROM tbl_printor a  join db_admin.tbl_group b on a._groupID = b.groupID"
                        . "  join tbl_store c on a._storeId=c.storeid"
                        . "  join db_admin.tbl_agent_printor d on a.printorId = d.printor_id"
                        . " where d.administrator_id = " . Yii::app()->session['administrator_id']
                        . " and a._storeId in ($storeId)";
            }
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $printor_sum_info = $result->queryAll();
            foreach ($printor_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.machineId LIKE '%" . $search . "%' "
                    . "or a.printorName LIKE '%" . $search . "%' "
                    . "or c.storename LIKE '%" . $search . "%'"
                    . "or a.address LIKE '%" . $search . "%' "
                    . "or b.groupName LIKE '%" . $search . "%' "
                    . "or a.picture LIKE '%" . $search . "%' )";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $printor_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($printor_sum_filter_info) > 0) {
                    foreach ($printor_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $totalResultSql = "SELECT a.printorId,a.machineId,a.printorName,a.address,c.storename,b.groupName,a.picture"
                        . " FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID"
                        . " join tbl_store c on a._storeId=c.storeid"
                        . " where a._storeId in ($storeId)";
            } else {
                $totalResultSql = "SELECT a.printorId,a.machineId,a.printorName,a.address,c.storename,b.groupName,a.picture"
                        . " FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID"
                        . " join tbl_store c on a._storeId=c.storeid"
                        . " join db_admin.tbl_agent_printor d on a.printorId = d.printor_id"
                        . " where d.administrator_id = " . Yii::app()->session['administrator_id']
                        . " and a._storeId in ($storeId)";
            }
        }
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $printor_info_filter = $dataResult->queryAll();
            if (count($printor_info_filter) > 0) {
                $i = 0;
                foreach ($printor_info_filter as $v) {
                    $i++;
                    $printorId = $v['printorId'];
                    $machineId = $v['machineId'];
                    $printorName = $v['printorName'];
                    $address = $v['address'];
                    $picture = $v['picture'];
                    $storename = $v['storename'];
                    $groupName = $v['groupName'];
                    $data1[] = array(
                        'id' => $i,
                        'printorId' => $printorId,
                        'machineId' => $machineId,
                        'printorName' => $printorName,
                        'address' => $address,
                        'picture' => $picture,
                        'storename' => $storename,
                        'groupName' => $groupName,
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
            //直接查询所有记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $printor_info_filter = $dataResult->queryAll();
            $i = 0;
            foreach ($printor_info_filter as $v) {
                $i++;
                $printorId = $v['printorId'];
                $machineId = $v['machineId'];
                $printorName = $v['printorName'];
                $address = $v['address'];
                $picture = $v['picture'];
                $storename = $v['storename'];
                $groupName = $v['groupName'];
                $data1[] = array(
                    'id' => $i,
                    'printorId' => $printorId,
                    'machineId' => $machineId,
                    'printorName' => $printorName,
                    'address' => $address,
                    'picture' => $picture,
                    'storename' => $storename,
                    'groupName' => $groupName,
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

    /*     * ************** 终端信息分页 end************** */
    /*     * ************** 终端信息 end************** */
    /*     * ************** 删除终端 start************** */

    public function actiondeleteterninal() {//删除终端
        $printorId = $_POST['printorId'];
        $printor_model = printor::model();
        $printor_info = $printor_model->find(array('condition' => "printorId='$printorId'"));
        if ($printor_info) {
            $counts = $printor_model->deleteAll(array('condition' => "printorId='$printorId'"));
            agent_printor::model()->deleteAll(array('condition' => "printor_id=$printorId"));
            if ($counts > 0) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        }
    }

    /*     * ************** 删除终端 end************** */
    /*     * ************** 添加终端 start ************** */

    public function actionaddprintor($groupID = 0) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            header("Content-type: text/html; charset=utf-8");

            if (isset($_POST['machineId'])) {
                $newId = $_POST['machineId'];
                $printorname = $_POST["printorname"];
                $printoraddress = $_POST['printoraddress'];
                $printorlng = $_POST['printorlng'];
                $printorlat = $_POST['printorlat'];
                $printorstoreId = $_POST['school'];
                $printorGroup = $_POST['printorgroup'];

                $lines_id = explode(',', $newId);
                $lines_name = explode(',', $printorname);
                $lines_add = explode(',', $printoraddress);
                $lines_lng = explode(',', $printorlng);
                $lines_lat = explode(',', $printorlat);

                $flagSuc = false;
                $flagEx = true;
                for ($i = 0; $i < count($lines_id); $i++) {
                    $printor_model = new printor();
                    $machine_id = $lines_id[$i];
                    $machine_name = $lines_name[$i];
                    $printor_add = $lines_add[$i];
                    $printor_lng = $lines_lng[$i];
                    $printor_lat = $lines_lat[$i];
                    $printor_info = $printor_model->find(array('condition' => "machineId='$machine_id'"));
                    if (count($printor_info) == 0) {
                        $printor_model->machineId = $machine_id;
                        $printor_model->printorName = $machine_name;
                        $printor_model->address = $printor_add;
                        $printor_model->lng = $printor_lng;
                        $printor_model->lat = $printor_lat;
                        $printor_model->_storeId = $printorstoreId;
                        $printor_model->_groupID = $printorGroup;
                        if ($printor_model->save()) {
                            if ($i == count($lines_id) - 1) {
                                $flagSuc = true;
                            }
                            echo "<script>alert('$machine_id 终端保存成功！');</script>";
                        }
                    } else {
                        echo "<script>alert('$machine_id 终端已存在,请重新输入！');</script>";
                        $flagEx = false;
                    }
                }

                if ($flagSuc && $flagEx) {
                    $this->redirect('./index.php?r=printor/printor');
                }
                if (!$flagEx) {
                    echo "<script>alert('终端已存在,请重新输入！');</script>";
                    $this->redirect('./index.php?r=printor/addprintor');
                }
            } else {
                if (Yii::app()->session['storeid'] == 0) {
                    $group_info = group::model()->findAll();
                    $store_info = store::model()->findAll();
                } else {
                    $group_info = group::model()->findAllBySql("select * from tbl_group where _storeid in (" . Yii::app()->session["storeid"] . ")");
                    $store_info = store::model()->findAllBySql("select * from tbl_store where storeid in (" . Yii::app()->session["storeid"] . ")");
                }
                $this->renderPartial('addprintor', array('leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info, 'store_info' => $store_info, 'groupID' => $groupID));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*
      学校与分组级联ajax数据
     */

    public function actionaddprintorAjax() {
        if (isset($_POST)) {
            $id = $_POST["value"];
            $group = group::model()->findAll("_storeid='$id'");
            $str = "";
            if (count($group) > 0) {
                $i = 0;
                foreach ($group as $value) {
                    $i++;
                    $groupId = $value->groupID;
                    $groupName = $value->groupName;
                    $str .= "{'id':'$i','groupid':'$groupId','groupname':'$groupName'},";
                }
                $str = substr($str, 0, -1);
                $json = "["
                        . $str
                        . "]";
                $json = str_replace("'", '"', $json);
            }
            echo $json;
        }
    }

    /*     * ************** 添加终端 end ************** */
    /*     * ************** 编辑终端 start ************** */

    public function actioneditprintor($printorId) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $printor_model = new printor();
            if (isset($_POST['oldmachineId'])) {
                $machineId = $_POST['oldmachineId'];
                $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));
                if (count($printor_info) != 0) {
                    $newId = $_POST['machineId'];
                    $printorname = $_POST["printorname"];
                    $printoraddress = $_POST['printoraddress'];
                    $printorlng = $_POST['printorlng'];
                    $printorlat = $_POST['printorlat'];
                    $printorstoreId = $_POST['school'];
                    $printorGroup = $_POST['printorgroup'];

                    $printor_info->machineId = $newId;
                    $printor_info->printorName = $printorname;
                    $printor_info->address = $printoraddress;
                    $printor_info->lng = $printorlng;
                    $printor_info->lat = $printorlat;
                    $printor_info->_storeId = $printorstoreId;
                    $printor_info->_groupID = $printorGroup;
                    $machineIds = $newId . ";";

                    if (!empty($_FILES['showpicture']['tmp_name'])) {
                        $tempFile = $_FILES['showpicture']['tmp_name'];
                        $attachment_after = substr(strrchr($_FILES['showpicture']['name'], '.'), 1);  //文件的后缀名 docx
                        $fileName = $newId . '.' . $attachment_after;
                        $targetPath = './images/terninalPictures';
                        $targetFile = rtrim($targetPath, '/') . '/' . $fileName;
                        move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));
                        $printor_info->picture = $fileName;
                    }
                    if ($printor_info->save()) {
                        echo "<script>parent.$('.save_lable').text('保存成功！').css('color','red');</script>";
//                        $this->redirect('./index.php?r=printor/printor');
                    } else {
                        echo "<script>parent.$('.save_lable').text('保存失败！').css('color','red');</script>";
                    }
                }
            } else {
                if (Yii::app()->session['storeid'] == 0) {
                    $group_info = group::model()->findAll();
                    $store_info = store::model()->findAll();
                } else {
                    $group_info = group::model()->findAllBySql("select * from tbl_group where _storeid in (" . Yii::app()->session["storeid"] . ")");
                    $store_info = store::model()->findAllBySql("select * from tbl_store where storeid in (" . Yii::app()->session["storeid"] . ")");
                }
                $printor_info = $printor_model->find("printorId = '$printorId'");
                $this->renderPartial('editprintor', array('leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info, 'store_info' => $store_info, 'printor_info' => $printor_info));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actioneditprintorAjax() {
        if (isset($_POST)) {
            $id = $_POST["value"];
            $group = group::model()->findAll("_storeid='$id'");
            $str = "";
            if (count($group) > 0) {
                $i = 0;
                foreach ($group as $value) {
                    $i++;
                    $groupId = $value->groupID;
                    $groupName = $value->groupName;
                    $str .= "{'id':'$i','groupid':'$groupId','groupname':'$groupName'},";
                }
                $str = substr($str, 0, -1);
                $json = "["
                        . $str
                        . "]";
                $json = str_replace("'", '"', $json);
            }
            echo $json;
        }
    }

    /*     * ************** 编辑终端 end ************** */
    /*     * ************** 终端监控 start ************** */

    public function actionprintormonitor() {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $this->renderPartial('printormonitor', array('leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 终端监控后台分页 start************* */

    public function actionprintormonitortopage() {
        $storeId = Yii::app()->session['storeid'];
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if ($order_column) {
            $i = intval($order_column);
            switch ($i) {
                case 1:
                    $orderSql = " order by a.machineId " . $order_dir;
                    break;
                case 2:
                    $orderSql = " order by a.printorName " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by a.last_time " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by a.address " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by a.paper_remain " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by c.storename " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by b.groupName " . $order_dir;
                    break;
                case 10:
                    $orderSql = " order by a.printer_error_time " . $order_dir;
                    break;
                case 12:
                    $orderSql = " order by a.updatetime " . $order_dir;
                    break;
                case 13:
                    $orderSql = " order by a.printer_errorinfo " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页 
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }
        if ($storeId == 0) {
            //定义查询数据总记录数sql
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $sumSql = "SELECT count(a.machineId) as sum FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID join tbl_store c on a._storeId=c.storeid where 1";
            } else {
                $sumSql = "SELECT count(a.machineId) as sum FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID"
                        . " join tbl_store c on a._storeId=c.storeid"
                        . " join db_admin.tbl_agent_printor d on a.printorId = d.printor_id where d.administrator_id = " . Yii::app()->session['administrator_id'];
            }
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $printor_sum_info = $result->queryAll();
            foreach ($printor_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.machineId LIKE '%" . $search . "%' "
                    . "or a.printorName LIKE '%" . $search . "%' "
                    . "or a.last_time LIKE  '%" . $search . "%' "
                    . "or c.storename LIKE '%" . $search . "%' "
                    . "or a.address LIKE '%" . $search . "%' "
                    . "or a.paper_remain LIKE '%" . $search . "%' "
                    . "or b.groupName LIKE '%" . $search . "%' "
                    . "or a.version LIKE '%" . $search . "%' "
                    . "or a.printer_errorcode LIKE '%" . $search . "%' "
                    . "or a.printer_error_time LIKE binary '%" . $search . "%' "
                    . "or a.updatetime LIKE binary '%" . $search . "%' "
                    . "or a.printer_errorinfo LIKE '%" . $search . "%' )";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $printor_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($printor_sum_filter_info[0]['sum'] > 0) {
                    foreach ($printor_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $totalResultSql = "SELECT a.machineId,a.printorName,a.last_time,a.address,c.storename,a.paper_remain,b.groupName,a.version,"
                        . "a.printer_errorcode,a.printer_error_time,a.updatestate,a.updatetime,a.printer_status,a.printer_errorinfo"
                        . " FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID join tbl_store c on a._storeId=c.storeid where 1";
            } else {
                $totalResultSql = "SELECT a.machineId,a.printorName,a.last_time,a.address,c.storename,a.paper_remain,b.groupName,a.version,"
                        . "a.printer_errorcode,a.printer_error_time,a.updatestate,a.updatetime,a.printer_status,a.printer_errorinfo"
                        . " FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID join tbl_store c on a._storeId=c.storeid"
                        . " join db_admin.tbl_agent_printor d on a.printorId = d.printor_id where d.administrator_id = " . Yii::app()->session['administrator_id'];
            }
        } else {
            //定义查询数据总记录数sql
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $sumSql = "SELECT count(a.machineId) as sum FROM tbl_printor a  join db_admin.tbl_group b on a._groupID = b.groupID"
                        . "  join tbl_store c on a._storeId=c.storeid where a._storeId in ($storeId)";
            } else {
                $sumSql = "SELECT count(a.machineId) as sum FROM tbl_printor a  join db_admin.tbl_group b on a._groupID = b.groupID"
                        . "  join tbl_store c on a._storeId=c.storeid"
                        . "  join db_admin.tbl_agent_printor d on a.printorId = d.printor_id"
                        . " where d.administrator_id = " . Yii::app()->session['administrator_id']
                        . " and a._storeId in ($storeId)";
            }
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $printor_sum_info = $result->queryAll();
            foreach ($printor_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.machineId LIKE '%" . $search . "%' or a.printorName LIKE '%" . $search . "%' or a.last_time LIKE  '%" . $search . "%' "
                    . "or c.storename LIKE '%" . $search . "%' or a.printer_error_time LIKE binary '%" . $search . "%' "
                    . " or a.address LIKE '%" . $search . "%' or a.paper_remain LIKE binary '%" . $search . "%' or b.groupName LIKE '%" . $search . "%' or "
                    . "a.version LIKE '%" . $search . "%' or a.printer_errorcode LIKE '%" . $search . "%' or a.updatetime LIKE binary '%" . $search . "%' "
                    . " or a.printer_errorinfo LIKE '%" . $search . "%' )";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $printor_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($printor_sum_filter_info) > 0) {
                    foreach ($printor_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $totalResultSql = "SELECT a.machineId,a.printorName,a.last_time,a.address,c.storename,a.paper_remain,b.groupName,a.version,"
                        . "a.printer_errorcode,a.printer_error_time,a.updatestate,a.updatetime,a.printer_status,a.printer_errorinfo"
                        . " FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID"
                        . " join tbl_store c on a._storeId=c.storeid"
                        . " where a._storeId in ($storeId)";
            } else {
                $totalResultSql = "SELECT a.machineId,a.printorName,a.last_time,a.address,c.storename,a.paper_remain,b.groupName,a.version,"
                        . "a.printer_errorcode,a.printer_error_time,a.updatestate,a.updatetime,a.printer_status,a.printer_errorinfo "
                        . " FROM tbl_printor a join db_admin.tbl_group b on a._groupID = b.groupID"
                        . " join tbl_store c on a._storeId=c.storeid"
                        . " join db_admin.tbl_agent_printor d on a.printorId = d.printor_id"
                        . " where d.administrator_id = " . Yii::app()->session['administrator_id']
                        . " and a._storeId in ($storeId)";
            }
        }
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $printor_info_filter = $dataResult->queryAll();
            if (count($printor_info_filter) > 0) {
                $i = 0;
                foreach ($printor_info_filter as $v) {
                    $i++;
                    $machineId = $v['machineId'];
                    $printorName = $v['printorName'];
                    $data = time();
                    $last_time = ($data - $v['last_time']) > 120 ? "关机" : "开机";
                    $address = $v['address'];
                    $paper_remain = $v['paper_remain'];
                    $storename = $v['storename'];
                    $groupName = $v['groupName'];
                    $version = $v['version'];
                    $printer_errorcode = $v['printer_errorcode'];
                    if ($printer_errorcode == 30017) {
                        $printer_errorcode = "21打印超限或21打印数据溢出丢失或21页面太复杂";
                    } else if ($printer_errorcode == 0) {
                        $printer_errorcode = "暂无";
                    }
                    $printer_error_time = $v['printer_error_time'];
                    $updatestate = $v['updatestate'];
                    $printer_errorinfo = $v['printer_errorinfo'];
                    if ($updatestate == 200) {
                        $updatestate = "升级成功";
                    } else if ($updatestate == 201) {
                        $updatestate = "无需升级";
                    } else if ($updatestate == 400) {
                        $updatestate = "升级失败，其他未知错误";
                    } else if ($updatestate == 401) {
                        $updatestate = "获取服务器版本失败";
                    } else if ($updatestate == 402) {
                        $updatestate = "下载主程序文件失败";
                    } else if ($updatestate == 403) {
                        $updatestate = "下载的主程序文件MD5校验不通过";
                    } else if ($updatestate == 404) {
                        $updatestate = "解压失败";
                    } else if ($updatestate == 405) {
                        $updatestate = "覆盖原程序失败";
                    }
                    $updatetime = $v['updatetime'];
                    $printer_status = $v['printer_status'];
                    if ($printer_status == 0 || $printer_status == 1) {
                        $printer_status = "运行正常";
                    } else if ($printer_status != 0 && $printer_status != 1) {
                        $printer_status = $printer_errorinfo;
                    }
                    $data1[] = array(
                        'id' => $i,
                        'machineId' => $machineId,
                        'printorName' => $printorName,
                        'last_time' => $last_time,
                        'address' => $address,
                        'paper_remain' => $paper_remain,
                        'storename' => $storename,
                        'groupName' => $groupName,
                        'version' => $version,
                        'printer_errorcode' => $printer_errorcode,
                        'printer_error_time' => $printer_error_time,
                        'updatestate' => $updatestate,
                        'updatetime' => $updatetime,
                        'printer_status' => $printer_status,
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
            //直接查询所有记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $printor_info_filter = $dataResult->queryAll();
            $i = 0;
            foreach ($printor_info_filter as $v) {
                $i++;
                $machineId = $v['machineId'];
                $printorName = $v['printorName'];
                $data = time();
                $last_time = ($data - $v['last_time']) > 120 ? "关机" : "开机";
                $address = $v['address'];
                $paper_remain = $v['paper_remain'];
                $storename = $v['storename'];
                $groupName = $v['groupName'];
                $version = $v['version'];
                $printer_errorcode = $v['printer_errorcode'];
                $printer_error_time = $v['printer_error_time'];
                if ($printer_errorcode == 30017) {
                    $printer_errorcode = "21打印超限或21打印数据溢出丢失或21页面太复杂";
                } else if ($printer_errorcode == 0) {
                    $printer_errorcode = "暂无";
                }
                $updatestate = $v['updatestate'];
                $printer_errorinfo = $v['printer_errorinfo'];
                if ($updatestate == 200) {
                    $updatestate = "升级成功";
                } else if ($updatestate == 201) {
                    $updatestate = "无需升级";
                } else if ($updatestate == 400) {
                    $updatestate = "升级失败，其他未知错误";
                } else if ($updatestate == 401) {
                    $updatestate = "获取服务器版本失败";
                } else if ($updatestate == 402) {
                    $updatestate = "下载主程序文件失败";
                } else if ($updatestate == 403) {
                    $updatestate = "下载的主程序文件MD5校验不通过";
                } else if ($updatestate == 404) {
                    $updatestate = "解压失败";
                } else if ($updatestate == 405) {
                    $updatestate = "覆盖原程序失败";
                }
                $updatetime = $v['updatetime'];
                $printer_status = $v['printer_status'];
                if ($printer_status == 0 || $printer_status == 1) {
                    $printer_status = "运行正常";
                } else if ($printer_status != 0 && $printer_status != 1) {
                    $printer_status = $printer_errorinfo;
                }
                $data1[] = array(
                    'id' => $i,
                    'machineId' => $machineId,
                    'printorName' => $printorName,
                    'last_time' => $last_time,
                    'address' => $address,
                    'paper_remain' => $paper_remain,
                    'storename' => $storename,
                    'groupName' => $groupName,
                    'version' => $version,
                    'printer_errorcode' => $printer_errorcode,
                    'printer_error_time' => $printer_error_time,
                    'updatestate' => $updatestate,
                    'updatetime' => $updatetime,
                    'printer_status' => $printer_status,
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

    /*     * ************** 终端监控后台分页 end*************** */
    /*     * ************** 终端监控 end ************** */
    /*     * ************** 终端分组 start ************** */

    public function actiongroup() {//分组首页
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $group_model = group::model();
            if (Yii::app()->session['storeid'] == 0) {
                $group_info = $group_model->findAll(array('order' => "groupID DESC"));
            } else {
                $group_info = group::model()->findAllBySql("select * from tbl_group where _storeid in (" . Yii::app()->session["storeid"] . ")");
            }
//删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $addGroup = "hidden";
            $editGroup = "hidden";
            $deleteGroup = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除分组") {
                    $deleteGroup = "";
                }
                if ($itemName == "添加分组") {
                    $addGroup = "";
                }
                if ($itemName == "编辑分组") {
                    $editGroup = "";
                }
            }
            $this->renderPartial('group', array('group_info' => $group_info, 'addGroup' => $addGroup, 'editGroup' => $editGroup, 'deleteGroup' => $deleteGroup, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 终端监控 end ************** */
    /*     * ************** 删除分组 start ************** */

    public function actiondeleteGroup() {//删除分组功能
        if (isset(Yii::app()->session['adminuser'])) {
            $groupID = $_POST["groupID"];

            $group_model = group::model();
            $group_info = $group_model->find("groupID='$groupID'");

            $printor_model = printor::model();
            $printor_info = $printor_model->find("_groupID='$groupID'");
            if (count($group_info) != 0) {

                if ($group_info->delete()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else if (count($group_info) != 0) {
                $json = '{"data":"no_printor"}';
                echo $json;
            } else {
                $json = '{"data":"no"}';
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 删除分组 end ************** */
    /*     * ************** 增加分组 start ************** */

    public function actionaddgroup() {//增加分组功能
        if (isset(Yii::app()->session['adminuser'])) {
            if (isset($_POST["groupName"])) {
                $groupName = $_POST["groupName"];
                $groupDetail = $_POST["groupDetail"];
                $storeid = $_POST["schoolName"];
                $group_model = new group();
                $group_info = $group_model->find("groupName='$groupName'");
                if (count($group_info) != 0) {
                    $json = '{"data":"exist"}';
                    echo $json;
                } else {
                    $group_model->groupName = $groupName;
                    $group_model->groupDetail = $groupDetail;
                    $group_model->_storeid = $storeid;

                    if ($group_model->save()) {
                        $json = '{"data":"success"}';
                        echo $json;
                    } else {
                        $json = '{"data":"false"}';
                        echo $json;
                    }
                }
            } else {
                $leftContent = $this->getLeftContent();
                $recommend = $this->getrecommend();
                $store_model = store::model();
                if (Yii::app()->session['storeid'] == 0) {
                    $store_info = store::model()->findAll();
                } else {
                    $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . Yii::app()->session["storeid"] . ")", array('order' => "storeid DESC"));
                }
                $this->renderPartial('addgroup', array('leftContent' => $leftContent, 'recommend' => $recommend, 'store_info' => $store_info));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 增加分组 end ************** */
    /*     * ************** 编辑分组 start ************** */

    public function actioneditgroup($groupID) {//编辑分组页面
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $group_model = group::model();
            $group_info = $group_model->find("groupID='$groupID'");
            $store_model = store::model();
            $store_info = $store_model->findAll();
            $this->renderPartial('editgroup', array('group_info' => $group_info, 'leftContent' => $leftContent, 'recommend' => $recommend, 'store_info' => $store_info));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actioneditgroups() {//编辑分组
        if (isset(Yii::app()->session['adminuser'])) {
            $groupID = $_POST["groupID"];
            $groupName = $_POST["groupName"];
            $groupDetail = $_POST["groupDetail"];
            $storeid = $_POST["schoolName"];

            $group_model = group::model();
            $group_info = $group_model->find("groupID='$groupID'");

            if (count($group_info) != 0) {
                $group_info->groupName = $groupName;
                $group_info->groupDetail = $groupDetail;
                $group_info->_storeid = $storeid;

                if ($group_info->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"no"}';
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 编辑分组 end ************** */
    /*     * ************** 查看分组终端 start ************** */

    public function actionlookterminal($groupID) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $printor_model = printor::model();

            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $printor_info = $printor_model->findAll("_groupID = $groupID");
            } else {
                $printor_info = $printor_model->findAll("_groupID = $groupID and printorId in (" . Yii::app()->session['printor_id'] . ")");
            }


            $group_model = group::model();
            $group_info = $group_model->findByPk($groupID);

            $this->renderPartial('lookterminal', array('printor_info' => $printor_info, 'leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 查看分组终端 end ************** */
    /*     * ************** 查看分组广告 start ************** */

    public function actionlookadvertisment($groupID) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $advertisement_model = advertisement::model();

            $adsdelivery_model = adsdelivery::model();
            $adsdelivery_info = $adsdelivery_model->findAll("_groupID = $groupID");
            $advertisementarr = array();
            foreach ($adsdelivery_info as $k => $l) {
                array_push($advertisementarr, $l->_advertisementID);
            }
            $criteria = new CDbCriteria;
            $criteria->addInCondition('advertisementID', $advertisementarr); //代表where id IN (1,2,3,4,5,); 
            $advertisement_info = $advertisement_model->findAll($criteria);
            $this->renderPartial('lookadvertisment', array('advertisement_info' => $advertisement_info, 'groupID' => $groupID, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 查看分组广告 end ************** */
    /*     * ************** 查看分组版本 start ************** */

    public function actionlookversion($groupID) {

        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $group_model = group::model();

            $printVersion_model = printVersion::model();
            $group_info = $group_model->findByPk($groupID);
            if (count($group_info) != 0) {
                $version = $group_info->_versionId;

                $versions = explode(",", $version);
                $versionarr = array();
                foreach ($versions as $k => $l) {
                    array_push($versionarr, $l);
                }
                $criteria = new CDbCriteria;
                $criteria->addInCondition('versionId', $versionarr); //代表where id IN (1,2,3,4,5,); 
                $printVesion_info = $printVersion_model->findAll($criteria);
            } else {
                $printVesion_info = $printVersion_model->findAll("versionId=0");
            }
            $this->renderPartial('lookversion', array('printVesion_info' => $printVesion_info, 'groupID' => $groupID, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 查看分组版本 end ************** */

    /*     * ************** 终端版本 start ************** */

    public function actionprintversion() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
//删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $deleteVersion = "hidden";
            $addVersion = "hidden";
            $editVersion = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除版本")
                    $deleteVersion = "";
                if ($itemName == "添加版本") {
                    $addVersion = "";
                }if ($itemName == "编辑版本") {
                    $editVersion = "";
                }
            }
            $printVesion_model = printVersion::model();
            $printVesion_info = $printVesion_model->findAll(array('order' => "versionId DESC"));
            $this->renderPartial('printvesion', array('printVesion_info' => $printVesion_info, 'deleteVersion' => $deleteVersion, 'addVersion' => $addVersion, 'editVersion' => $editVersion, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 终端版本 end ************** */
    /*     * ************** 删除终端版本 start ************** */

    public function actiondeleteversion() {
        if (isset(Yii::app()->session['adminuser'])) {
            $versionId = $_POST["versionId"];
            $printVesion_model = printVersion::model();
            $printVesion_info = $printVesion_model->findByPk($versionId);
            if ($printVesion_info->delete()) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 删除终端版本 end ************** */

    /*     * ************** 新增终端版本 start ************** */

    public function actionaddversion() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $group_info = group::model()->findAll();

            if (!empty($_FILES['versionfile']['tmp_name'])) {
                if ($_FILES["versionfile"]["error"] > 0) {
                    echo "Error: ";
                } else {
                    $tempFile = $_FILES["versionfile"]["tmp_name"];
                    $fileName = $_FILES["versionfile"]["name"];

                    $targetPath = './printVersion';
                    $targetFile = rtrim($targetPath, '/') . '/' . $fileName;


                    $printVesion_model = new printVersion();
                    $printVesion_model->versionCode = $_POST['vesionCode'];
                    $printVesion_model->versionFile = $fileName;
                    $printVesion_model->updateTime = date('Y-m-d H:i:s', time());
                    $printVesion_model->versiondescript = $_POST['versiondescript'];
                    $printVesion_model->type = $_POST['type'];
                    $printVesion_model->FileMD5 = md5_file($tempFile);
                    move_uploaded_file($tempFile, iconv("UTF-8", "gb2312", $targetFile));
                    $versionGroup = $_POST['vesionGroup'];
                    $group_model = group::model();
                    $printor_model = printor::model();
                    if ($printVesion_model->save()) {
                        if ($versionGroup[0] == "multiselect-all") {
                            for ($i = 1; $i < count($versionGroup); $i++) {
                                $group_info = $group_model->find("groupId = $versionGroup[$i]");
                                $group_info->_versionId = $printVesion_model->versionId;
                                $group_info->save();
                                $print_info = $printor_model->findAll("_groupID = $versionGroup[$i]");
                                foreach ($print_info as $k => $l) {
                                    $l->updatestate = "";
                                    $l->save();
                                }
                            }
                        } else {
                            for ($i = 0; $i < count($versionGroup); $i++) {
                                $group_info = $group_model->find("groupId = $versionGroup[$i]");
                                $group_info->_versionId = $printVesion_model->versionId;
                                $group_info->save();
                                $print_info = $printor_model->findAll("_groupID = $versionGroup[$i]");
                                foreach ($print_info as $k => $l) {
                                    $l->updatestate = "";
                                    $l->save();
                                }
                            }
                        }
                        if (count($group_info->save()) > 0) {
                            $this->redirect('./index.php?r=printor/printversion');
                        }
                    }
                }
            } else {
                $this->renderPartial('addversion', array('leftContent' => $leftContent, 'recommend' => $recommend, 'group_info' => $group_info));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 新增终端版本 end ************** */
    /*     * ************** 编辑终端版本 start ************** */

    public function actioneditVersion($versionId) {//编辑版本页面
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $version_model = printVersion::model();
            $group_info = group::model()->findAll();



            $group_group = group::model()->findAll("_versionId='$versionId'");
            $group_groupId = "";
            if (count($group_group) > 0) {
                foreach ($group_group as $k => $l) {
                    $group_groupId .= "'" . $l->groupID . "'" . ",";
                }
            } else {
                $group_groupId = 0;
            }
            $version_info = $version_model->find("versionId='$versionId'");
//            $group_groupId = "";
//            $versionGroup = explode(',', $version_info->_groupID); //ID 1,2,3,4
//            for ($i = 0; $i < count($versionGroup); $i++) {
//                $group_groupId .= "'" . $versionGroup[$i] . "'" . ",";
//            }
//            $group_groupId = substr($group_groupId, 0, -1);


            $store_model = store::model();
            $store_info = $store_model->findAll();
            $this->renderPartial('editversion', array('version_info' => $version_info, 'group_groupId' => $group_groupId, 'leftContent' => $leftContent, 'recommend' => $recommend, 'store_info' => $store_info, 'group_info' => $group_info));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actioneditVersions() {//编辑版本
        if (isset(Yii::app()->session['adminuser'])) {
            $group_info = group::model()->findAll();
            $printVersion_model = printVersion::model();

            $versionId = $_POST['versionID'];
            $printVesion_info = $printVersion_model->find("versionId = '$versionId'");
            $printVesion_info->versionCode = $_POST['vesionCode'];
            $printVesion_info->versionFile = $_POST['versionname'];
            $printVesion_info->updateTime = date('Y-m-d H:i:s', time());
            $printVesion_info->versiondescript = $_POST['versiondescript'];
            $printVesion_info->type = $_POST['type'];
            $versionGroup = $_POST['vesionGroup'];

            $group_model = group::model();

            $group_infox = $group_model->findAll("_versionId = $versionId");
            if (count($group_infox != 0)) {
                foreach ($group_infox as $k => $l) {
                    $l->_versionId = "";
                    $l->save();
                }
            }

            $flag = false;
            if ($printVesion_info->save()) {
                if (count($versionGroup) != 0) {
                    if ($versionGroup[0] == "multiselect-all") {
                        for ($i = 1; $i < count($versionGroup); $i++) {
                            $group_info = $group_model->find("groupId = $versionGroup[$i]");
                            $group_info->_versionId = $versionId;
                            $group_info->save();
                        }
                    } else {
                        for ($i = 0; $i < count($versionGroup); $i++) {
                            $group_info = $group_model->find("groupId = $versionGroup[$i]");
                            $group_info->_versionId = $versionId;
                            $group_info->save();
                        }
                    }
                    if (count($group_info->save()) > 0) {
                        $flag = true;
                    }
                } else {
                    $flag = true;
                }
            }
            if ($flag) {
                echo "<script>parent.$('.save_lable').text('保存成功！').css('color','red');</script>";
            } else {
                echo "<script>parent.$('.save_lable').text('保存失败！').css('color','red');</script>";
            }
        }
    }

    /*     * ************** 编辑终端版本 end ************** */
    /*     * ************** 下载终端版本 start ************** */

    //下载版本
    public function actiondownloadprintorversion($versionId) {
        $printVersion_model = printVersion::model();
        $printVersion_info = $printVersion_model->findByPk($versionId);

        if (count($printVersion_info) != 0) {

            require_once './oss/samples/Common.php';
            $bucket = "porunoss";

            $timeout = 3600;
            $ossClient = Common::getOssClient();
            $object = $printVersion_info->Filepath;
            $filePath = $ossClient->signUrl($bucket, $object, $timeout);

            $names = explode("/", $printVersion_info->Filepath);
            $y = count($names);
            $name = substr($names[count($names) - 1], 14);

            header('Content-Type:application/octet-stream'); //文件的类型
            Header("Accept-Ranges: bytes");
            header('Content-Disposition:attachment;filename = "' . $name . '"'); //下载显示的名字
            ob_clean();
            flush();
            readfile($filePath);
            exit();
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 下载终端版本 end ************** */
    /*     * ************** 打印错误信息 start ************** */

    public function actionprinterror() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $delErrInfo = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除错误信息") {
                    $delErrInfo = "true";
                }
            }
            $this->renderPartial('printerror', array('delErrInfo' => $delErrInfo, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    //错误信息后台分页
    public function actionprintorerrAjax() {

        $storeId = Yii::app()->session['storeid'];
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 2:
                    $orderSql = " order by a.machineid " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by b.printorName " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by b.address " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by c.storename " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by a.error_type " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by a.error_informaition " . $order_dir;
                    break;
                case 8:
                    $orderSql = " order by a.occur_time " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页 
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }
        if ($storeId == 0) {
            //定义查询数据总记录数sql
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $sumSql = "SELECT count(a.errorid) as sum FROM tbl_printorerror a join db_home.tbl_printor b on a.machineid = b.machineId join db_home.tbl_store c on a._storeid=c.storeid where 1";
            } else {
                $sumSql = "SELECT count(a.errorid) as sum FROM tbl_printorerror a join db_home.tbl_printor b on a.machineid = b.machineId join db_home.tbl_store c on a._storeid=c.storeid where b.printorId in(" . Yii::app()->session['printor_id'] . ")";
            }
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db->createCommand($sumSql);
            $printor_sum_info = $result->queryAll();
            foreach ($printor_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.machineid LIKE '%" . $search . "%' or b.printorName LIKE '%" . $search . "%' or b.address LIKE '%" . $search . "%' or c.storename LIKE '%" . $search . "%' or a.error_type LIKE '%" . $search . "%'"
                    . " or a.error_informaition LIKE '%" . $search . "%' or a.occur_time LIKE binary '%" . $search . "%' )";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db->createCommand($sumSql . $sumSqlWhere);
                $printor_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($printor_sum_filter_info[0]['sum'] > 0) {
                    foreach ($printor_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商、
                $totalResultSql = "SELECT a.errorid,a.machineid,b.printorName,b.address,c.storename,a.error_type,a.error_informaition,a.occur_time FROM"
                        . " tbl_printorerror a join db_home.tbl_printor b on a.machineid = b.machineId join db_home.tbl_store c on a._storeid=c.storeid where 1";
            } else {
                $totalResultSql = "SELECT a.errorid,a.machineid,b.printorName,b.address,c.storename,a.error_type,a.error_informaition,a.occur_time FROM"
                        . " tbl_printorerror a join db_home.tbl_printor b on a.machineid = b.machineId join db_home.tbl_store c on a._storeid=c.storeid where b.printorId in(" . Yii::app()->session['printor_id'] . ")";
            }
        } else {
            //定义查询数据总记录数sql
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $sumSql = "SELECT count(a.errorid) as sum FROM tbl_printorerror a join db_home.tbl_printor b"
                        . " on a.machineid = b.machineId join db_home.tbl_store c on a._storeid=c.storeid where a._storeid in ($storeId)";
            } else {
                $sumSql = "SELECT count(a.errorid) as sum FROM tbl_printorerror a join db_home.tbl_printor b"
                        . " on a.machineid = b.machineId join db_home.tbl_store c on a._storeid=c.storeid where a._storeid in ($storeId) and b.printorId in(" . Yii::app()->session['printor_id'] . ")";
            }
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db->createCommand($sumSql);
            $printor_sum_info = $result->queryAll();
            foreach ($printor_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.machineid LIKE '%" . $search . "%' or b.printorName LIKE '%" . $search . "%' or b.address LIKE '%" . $search . "%' or c.storename LIKE '%" . $search . "%' or a.error_type LIKE '%" . $search . "%'"
                    . " or a.error_informaition LIKE '%" . $search . "%' or a.occur_time LIKE binary '%" . $search . "%' ";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db->createCommand($sumSql . $sumSqlWhere);
                $printor_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($printor_sum_filter_info) > 0) {
                    foreach ($printor_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $totalResultSql = "SELECT a.errorid,a.machineid,b.printorName,b.address,c.storename,a.error_type,a.error_informaition,a.occur_time FROM"
                        . " tbl_printorerror a join db_home.tbl_printor b on a.machineid = b.machineId join db_home.tbl_store c on a._storeid=c.storeid where a._storeid in ($storeId)";
            } else {
                $totalResultSql = "SELECT a.errorid,a.machineid,b.printorName,b.address,c.storename,a.error_type,a.error_informaition,a.occur_time FROM"
                        . " tbl_printorerror a join db_home.tbl_printor b on a.machineid = b.machineId join db_home.tbl_store c on a._storeid=c.storeid where a._storeid in ($storeId) and b.printorId in(" . Yii::app()->session['printor_id'] . ")";
            }
        }
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $printor_info_filter = $dataResult->queryAll();
            if (count($printor_info_filter) > 0) {
                $i = 0;
                foreach ($printor_info_filter as $v) {
                    $i++;
                    $errorid = $v['errorid'];
                    $machineId = $v['machineid'];
                    $printorName = $v['printorName'];
                    $address = $v['address'];
                    $storename = $v['storename'];
                    $error_type = $v['error_type'];
                    $error_informaition = $v['error_informaition'];
                    $occur_time = $v['occur_time'];
                    $data1[] = array(
                        'errorid' => $errorid,
                        'id' => $i,
                        'machineId' => $machineId,
                        'printorName' => $printorName,
                        'address' => $address,
                        'storename' => $storename,
                        'error_type' => $error_type,
                        'error_informaition' => $error_informaition,
                        'occur_time' => $occur_time,
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
            //直接查询所有记录
            $dataResult = Yii::app()->db->createCommand($totalResultSql . $orderSql . $limitSql);
            $printor_info_filter = $dataResult->queryAll();
            $i = 0;
            foreach ($printor_info_filter as $v) {
                $i++;
                $errorid = $v['errorid'];
                $machineId = $v['machineid'];
                $printorName = $v['printorName'];
                $address = $v['address'];
                $storename = $v['storename'];
                $error_type = $v['error_type'];
                $error_informaition = $v['error_informaition'];
                $occur_time = $v['occur_time'];
                $data1[] = array(
                    'errorid' => $errorid,
                    'id' => $i,
                    'machineId' => $machineId,
                    'printorName' => $printorName,
                    'address' => $address,
                    'storename' => $storename,
                    'error_type' => $error_type,
                    'error_informaition' => $error_informaition,
                    'occur_time' => $occur_time,
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

    //删除错误信息
    public function actiondeleteerrorinfo() {
        if (isset(Yii::app()->session['adminuser'])) {
            $errorid = $_POST['errorid'];

            $printorerror_model = printorerror::model();
            if (count($printorerror_model->deleteByPk($errorid)) > 0) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 打印错误信息 end ************** */
}
