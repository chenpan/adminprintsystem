<?php

class baseController extends Controller {

    public $menu = array(
        //0代表不显示 1代表显示
        "学校" => 0, //院校管理
        "文件" => 0, //文件管理
        "订单" => 0, //订单管理
        "用户" => 0, //用户管理
        "终端" => 0, //终端管理
        "终端信息" => 0, //终端信息
        "终端监控" => 0,
        "终端分组" => 0,
        "终端版本" => 0,
        "错误信息" => 0,
        "广告" => 0, //广告管理
        "公司管理" => 0,
        "广告管理" => 0,
        "退款" => 0, //退款管理
        "退款列表" => 0,
        "已退款列表" => 0,
        "文库" => 0, //文库
        "分享文档" => 0,
        "删除文档" => 0,
        "审核文档" => 0,
        "分享终端文档" => 0,
        "统计" => 0, //统计管理
        "订单统计" => 0,
        "收入统计" => 0,
        "退款统计" => 0,
        "注册人数统计" => 0,
        "打印纸张统计" => 0,
        "上传文件统计" => 0,
        "统计信息" => 0,
        "兼职" => 0, //兼职管理
        "兼职管理" => 0,
        "兼职审核" => 0,
        "兼职冲账" => 0,
        "权限" => 0, //权限管理
        "角色与权限" => 0,
        "权限分配" => 0,
        "积分" => 0, //积分
        "增加积分" => 0,
        "扣除积分" => 0,
        "价格"=>0,//价格
        "问题"=>0,//问题
        "反馈"=>0,//反馈
        "短信"=>0,//反馈
    );

    public function getLeftContent() {
        //获取左则导航栏
        $common = new commonController();
        $roleID = Yii::app()->session['_roleid'];
        $auth_model = assignment::model();
        $item_model = item::model();
        foreach ($this->menu as $k => &$v) {
            $item_info = $item_model->find("itemName='$k'");
            $item_id = $item_info->itemId;
            if (count($auth_model->find("_roleId='$roleID'and _itemId ='$item_id'")) > 0) {
                $v = 1;
            }
        }
        $date = Yii::app()->session['logintime'];
        $url = $common->webUrl . "?r=recommend/leftmenu&username=" . Yii::app()->session['adminuser'] . "&menu=" . json_encode($this->menu) . "&logintime=" . urlencode($date);
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
     * 检查权限
     * 输入权限名称
     */

    protected function checkAccess($authname, $filterChain) {
        if (isset(Yii::app()->session['_roleid'])) {
            $_roleID = Yii::app()->session['_roleid'];
            $auth_model = assignment::model();
            $auth_info = $auth_model->findAll("_roleId=" . $_roleID);
            $item_model = item::model();
            $item_id = $item_model->find("itemName = '$authname'")->itemId;
            if (count($auth_info) > 0) {
                $flag = false;
                foreach ($auth_info as $K => $V) {
                    if ($V->_itemId == $item_id) {
                        $filterChain->run();
                        $flag = true;
                        break;
                    }
                }
                if (!$flag) {
//                    echo '<script> alert("无权访问");history.go(-1);</script>';
                    $this->redirect('./index.php?r=nonPrivilege/index');
                }
            } else {
                //无权访问
//                echo '<script> alert("无权访问");history.go(-1);</script>';
                $this->redirect('./index.php?r=nonPrivilege/index');
            }
        } else {
            //无权访问
//            echo '<script> alert("无权访问");history.go(-1);</script>';
            $this->redirect('./index.php?r=nonPrivilege/index');
        }
    }

}
