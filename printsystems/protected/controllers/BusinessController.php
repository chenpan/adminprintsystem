<?php

include 'BaseController.php';

class businessController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*     * ************** 订单列表 ************** */

    public function filters() {

        return array(
            'business + business',
            'orderDetail + orderDetail',
        );
    }

    public function filterbusiness($filterChain) {
        $this->checkAccess("订单", $filterChain);
    }

    public function filterorderDetail($filterChain) {
        $this->checkAccess("订单详情", $filterChain);
    }

//订单列表
    public function actionbusiness() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $this->renderPartial('business', array('leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actionbusinessAjax() {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=default/index');
        }
        $business_model = business::model();
        $storeId = Yii::app()->session['storeid'];
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
        //拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 2:
                    $orderSql = " order by a.orderId " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by b.username " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by c.storename " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by a.placeOrdertime " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by a.paidMoney " . $order_dir;
                    break;
                case 8:
                    $orderSql = " order by a.consumptionIntegral " . $order_dir;
                    break;
                case 9:
                    $orderSql = " order by a.verificationCode " . $order_dir;
                    break;
                default;
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }
        if ($storeId == 0) {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.businessid) as sum FROM tbl_business a join tbl_user b on a._userid = b.userid join tbl_store c on b._storeid = c.storeid where a.isdelete= 0";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $attachment_sum_info = $result->queryAll();
            foreach ($attachment_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.paidMoney LIKE '%" . $search . "%' or a.consumptionIntegral LIKE '%" . $search . "%' or a.verificationCode LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or a.orderId LIKE '%" . $search . "%' or a.placeOrdertime LIKE binary '%" . $search . "%' or c.storename LIKE binary '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $attachment_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($attachment_sum_filter_info[0]['sum'] > 0) {
                    foreach ($attachment_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.businessid,a.orderId,a.placeOrdertime,a.paidMoney,a.consumptionIntegral,a.verificationCode,a._userid,a.medium,b.username,c.storename FROM tbl_business a join tbl_user b on a._userid = b.userid join tbl_store c on b._storeid = c.storeid where a.isdelete= 0";
        } else {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.businessid) as sum FROM tbl_business a join tbl_user b on a._userid = b.userid join tbl_store c on b._storeid = c.storeid where a.isdelete= 0 and a._storeid in ($storeId)";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $attachment_sum_info = $result->queryAll();
            foreach ($attachment_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.paidMoney LIKE '%" . $search . "%' or a.consumptionIntegral LIKE '%" . $search . "%' or a.verificationCode LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or a.orderId LIKE '%" . $search . "%' or a.placeOrdertime LIKE binary '%" . $search . "%' or c.storename LIKE binary '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $attachment_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($attachment_sum_filter_info) > 0) {
                    foreach ($attachment_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.businessid,a.orderId,a.placeOrdertime,a.paidMoney,a.consumptionIntegral,a.verificationCode,a._userid,a.medium,b.username,c.storename FROM tbl_business a join tbl_user b on a._userid = b.userid join tbl_store c on b._storeid = c.storeid where a.isdelete= 0 and a._storeid in ($storeId)";
        }
        //query data
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $attachment_info_filter = $dataResult->queryAll();
            if (count($attachment_info_filter) > 0) {
                $i = 0;
                foreach ($attachment_info_filter as $v) {
                    $i++;
                    $name = $v['username'];
                    $businessId = $v['businessid'];
                    $orderId = $v['orderId'];
                    $school = $v['storename'];
                    if ($v['medium'] == 1) {
                        $type = "PC端";
                    } else if ($v['medium'] == 2) {
                        $type = "APP端";
                    } else if ($v['medium'] == 3) {
                        $type = "微信";
                    }
                    $placeOrdertime = $v['placeOrdertime'];
                    $paidMoney = $v['paidMoney'];
                    $consumptionIntegral = $v['consumptionIntegral'];
                    $verificationCode = $v['verificationCode'];
                    $data1[] = array(
                        'id' => $i,
                        'businessId' => $businessId,
                        'orderId' => $orderId,
                        'name' => $name,
                        'school' => $school,
                        'type' => $type,
                        'placeOrdertime' => $placeOrdertime,
                        'paidMoney' => $paidMoney,
                        'consumptionIntegral' => $consumptionIntegral,
                        'verificationCode' => $verificationCode
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            } else {
                $data1[] = array(
                    'id' => null,
                    'businessId' => null,
                    'orderId' => null,
                    'name' => null,
                    'type' => null,
                    'school' => null,
                    'placeOrdertime' => null,
                    'paidMoney' => null,
                    'consumptionIntegral' => null,
                    'verificationCode' => null
                );
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
            //直接查询所有记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $attachment_info = $dataResult->queryAll();
            $i = 0;
            foreach ($attachment_info as $v) {
                $i++;
                $name = $v['username'];
                $school = $v['storename'];
                if ($v['medium'] == 1) {
                    $type = "PC端";
                } else if ($v['medium'] == 2) {
                    $type = "APP端";
                } else if ($v['medium'] == 3) {
                    $type = "微信";
                }
                $businessId = $v['businessid'];
                $orderId = $v['orderId'];
                $placeOrdertime = $v['placeOrdertime'];
                $paidMoney = $v['paidMoney'];
                $consumptionIntegral = $v['consumptionIntegral'];
                $verificationCode = $v['verificationCode'];
                $data1[] = array(
                    'id' => $i,
                    'businessId' => $businessId,
                    'orderId' => $orderId,
                    'name' => $name,
                    'school' => $school,
                    'type' => $type,
                    'placeOrdertime' => $placeOrdertime,
                    'paidMoney' => $paidMoney,
                    'consumptionIntegral' => $consumptionIntegral,
                    'verificationCode' => $verificationCode
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

    /*     * ************** 订单列表 ************** */
    /*     * ************** 订单详情 ************** */

//订单详情
    public function actionorderDetail($businessid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $username = Yii::app()->session['adminuser'];

        $businessidd = $businessid;

        $attachment_model = attachment::model();
        $user_model = user::model();
        $businssid_model = business::model();
        $subbusiness_model = subbusiness::model();
        $businessid_info = $businssid_model->find(array("condition" => "businessid=$businessidd"));


        $integration = (int) $user_model->find(array("condition" => "userid = '$businessid_info->_userid'"))->integration;


        $attachmentArray = array();
        $printNumber = 0; //打印

        $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId=$businessidd"));
        if ($subbusiness_info) {
            foreach ($subbusiness_info as $l => $y) {
                $attachmentId = $y->_attachmentId;
                $attachinfo = $attachment_model->find(array('condition' => "attachmentid = $attachmentId"));
                array_push($attachmentArray, array("attachmentname" => $attachinfo->attachmentname, "printNumber" => $y->printNumbers, "paidMoney" => $y->paidMoney, "printSet" => $y->printSet, "payType" => $y->payType, "isPay" => $y->isPay, "status" => $y->status, "marchineId" => $y->marchineId, "isrefund" => $y->isrefund, "double_sided" => $y->double_sided));
            }
        }
        $this->renderPartial('orderDetail', array("businessid_info" => $businessid_info, "attachmentArray" => $attachmentArray, "username" => $username, "integration" => $integration, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    /*     * ************** 订单列表 ************** */
}
