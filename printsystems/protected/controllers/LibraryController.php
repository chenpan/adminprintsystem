<?php

include 'BaseController.php';

class libraryController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      文库
     */

    public function filters() {
        return array(
            'sharedocument + sharedocument', //分享文档
            'deldocument + deldocument', //删除文档
            'auditdocument + auditdocument', //审核文档
            'shareterminaldocument + shareterminaldocument', //分享终端文档
        );
    }

    public function filterdeldocument($filterChain) {
        $this->checkAccess("删除文档", $filterChain);
    }

    public function filterauditdocument($filterChain) {
        $this->checkAccess("审核文档", $filterChain);
    }

    public function filtershareterminaldocument($filterChain) {
        $this->checkAccess("分享终端文档", $filterChain);
    }

//删除文档
    public function actiondeldocument() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $this->renderPartial("deldocument", array("leftContent" => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//审核文档
    public function actionauditdocument() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $this->renderPartial("auditdocument", array("leftContent" => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//分享终端文档
    public function actionshareterminaldocument() {

        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
//文库分类
            $classify_model = classify::model();
            $recordsClassify1_info = $classify_model->findAll();
            foreach ($recordsClassify1_info as $value) {
                $classify_code = $value['code'];
                $classify_name = $value['name'];
                $classify[] = array(
                    'code' => $classify_code,
                    'name' => $classify_name
                );
            }
            $classify = json_encode($classify);
            $this->renderPartial("shareterminaldocument", array('leftContent' => $leftContent, 'recommend' => $recommend, "classify" => $classify));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//分享终端文档后台分页
    public function actionshareterminaldocumentAjax() {//后台分页
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=default/index');
        }
        $storeId = Yii::app()->session['storeid'];
        $store_model = store::model();
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 1:
                    $orderSql = " order by a.filename " . $order_dir;
                    break;
                case 2:
                    $orderSql = " order by b.storename " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by c.printorName " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by a.uploadtime " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页 
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }

        //查询打印终端
        //第一步，先在printor表里面找到machineId
        if (Yii::app()->session['storeid'] == 0) {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.shareid) as sum FROM tbl_terminalshare a join db_home.tbl_store b on a.storeId = b.storeid join db_home.tbl_printor c on a.printorId = c.printorId ";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db->createCommand($sumSql);
            $terminalshare_info = $result->queryAll();
            foreach ($terminalshare_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql


            $sumSqlWhere = " where (a.filename LIKE '%" . $search . "%' or b.storename LIKE '%" . $search . "%' or c.printorName LIKE '%" . $search . "%' or a.uploadtime LIKE binary '%" . $search . "%' )";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db->createCommand($sumSql . $sumSqlWhere);
                $terminalshare_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($terminalshare_sum_filter_info[0]['sum'] > 0) {
                    foreach ($terminalshare_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.shareid,a.filename,b.storename,c.printorName,a.uploadtime FROM tbl_terminalshare a join db_home.tbl_store b on a.storeId = b.storeid  join db_home.tbl_printor c on a.printorId = c.printorId ";
        } else {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.shareid) as sum FROM tbl_terminalshare a join db_home.tbl_store b on a.storeId = b.storeid join db_home.tbl_printor c on a.printorId = c.printorId where a.storeId in ($storeId)";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db->createCommand($sumSql);
            $terminalshare_info = $result->queryAll();
            foreach ($terminalshare_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.filename LIKE '%" . $search . "%' or b.storename LIKE '%" . $search . "%' or c.printorname LIKE '%" . $search . "%' or a.uploadtime LIKE binary '%" . $search . "%' )";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db->createCommand($sumSql . $sumSqlWhere);
                $terminalshare_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($terminalshare_sum_filter_info) > 0) {
                    foreach ($terminalshare_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.shareid,a.filename,b.storename,c.printorName,a.uploadtime FROM tbl_terminalshare a join db_home.tbl_store b on a.storeId = b.storeid  join db_home.tbl_printor c on a.printorId = c.printorId where a.storeId in ($storeId)";
        }
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $terminalshare_info_filter = $dataResult->queryAll();
            if (count($terminalshare_info_filter) > 0) {
                foreach ($terminalshare_info_filter as $v) {
                    $shareId = $v['shareid'];
                    $name = $v['filename'];
                    $storeName = $v['storename'];
                    $printorName = $v['printorName'];
                    $uploadtime = $v['uploadtime'];
                    $data1[] = array(
                        'shareId' => $shareId,
                        'filename' => $name,
                        'storeName' => $storeName,
                        'printorName' => $printorName,
                        'uploadtime' => $uploadtime,
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'data' => $data1
                );
                echo $data = json_encode($data2);
            } else {
                $data1[] = array(
                    'shareId' => null,
                    'filename' => NULL,
                    'storeName' => null,
                    'printorName' => null,
                    'uploadtime' => null,
                );
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'data' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
            //直接查询所有记录
            $dataResult = Yii::app()->db->createCommand($totalResultSql . $orderSql . $limitSql);
            $terminalshare_info = $dataResult->queryAll();
            foreach ($terminalshare_info as $v) {
                $shareId = $v['shareid'];
                $name = $v['filename'];
                $storeName = $v['storename'];
                $printorName = $v['printorName'];
                $uploadtime = $v['uploadtime'];
                $data1[] = array(
                    'shareId' => $shareId,
                    'filename' => $name,
                    'storeName' => $storeName,
                    'printorName' => $printorName,
                    'uploadtime' => $uploadtime,
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'data' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

//删除分享的文档
    public function actiondeleteshares() {
        if (isset(Yii::app()->session['adminuser'])) {
            if (isset($_POST['shareIdd'])) {
                $terminalshare_model = terminalshare::model();
                $shareIdd = $_POST['shareIdd'];
                $shareId = explode(",", $shareIdd);

                $statues = 0;

                foreach ($shareId as $k => $l) {
                    $terminalshare_info = $terminalshare_model->findByPk($l);

                    if (count($terminalshare_info) != 0) {
                        $terminalshare_model->deleteByPk($l);
                    }
                }
                $json = '{"data":"success"}';
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//分享终端上传文件
    public function actionshareterminal() {
        if (isset(Yii::app()->session['adminuser'])) {
            if (isset($_POST['shareIdd'])) {
                $terminalshare_model = terminalshare::model();
                $shareIdd = $_POST['shareIdd'];
                $shareId = explode(",", $shareIdd);
                $classify = $_POST['classify'];
                $credit = $_POST['credit'];
                $keyword = $_POST['keyword'];
                $describe = $_POST['describe'];

                foreach ($shareId as $k => $l) {
                    $terminalshare_info = $terminalshare_model->findByPk($l);
                    $attachment_after = substr(strrchr($terminalshare_info->filename, '.'), 1);  //文件的后缀名 docx

                    $attachment_model = new attachment();
                    $attachment_model->attachmentname = $terminalshare_info->filename;
                    $attachment_model->attachmentfile = $terminalshare_info->truename;
                    $attachment_model->_userid = 1027;
                    $attachment_model->filetype = $attachment_after;
                    $attachment_model->filenumber = $terminalshare_info->filenumber;
                    $attachment_model->uploadtime = $terminalshare_info->uploadtime;
                    $attachment_model->isdelete = 1;
                    $attachment_model->storeid = $terminalshare_info->storeId;
                    $attachment_model->savepath = $terminalshare_info->savepath;
                    $attachment_model->status = 0; //未打印
                    $attachment_model->share = 0; //未分享
                    $attachment_model->medium = 4; //终端

                    if ($attachment_model->save()) {
                        header('Access-Control-Allow-Origin: *');
//        header("Access-Control-Allow-Origin:http://dev.cqutprint.com");
                        $attachmentid = intval($attachment_model->attachmentid); //文档ID
                        $filename = $terminalshare_info->filename; //文档名
                        $classify = $_POST['classify']; //文档分类
                        $credit = intval($_POST['credit']); //文档分享所需积分 
                        $keyword = $_POST['keyword']; //文档标签
                        $describe = $_POST['describe']; //文档描述

                        $url = "http://www.cqutprint.com/Library/shareFile";
                        $post_data = array('attachmentid' => $attachmentid, 'userid' => 1027, 'filename' => $filename, 'classify' => $classify, 'credit' => $credit, 'keyword' => $keyword, 'describe' => $describe);
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
// 返回结果 
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
// 使用POST提交 
                        curl_setopt($ch, CURLOPT_POST, 1);
// POST参数 
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
// 结果 
                        $res = curl_exec($ch);
                        curl_close($ch);

                        $jsons = json_decode($res);
                        if ($jsons->code == 200) {
                            $terminalshare_model->deleteByPk($l);
                            echo $res;
                        }
                    } else {
                        $json = '{"code":"400","msg":"保存失败"}';
                        echo $json;
                    }
                }
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//终端文件下载
    public function actiondownload($shareid) {
        if (isset(Yii::app()->session['adminuser'])) {
            $shareid = intval($shareid);
            $terminalshare_model = terminalshare::model();
            $terminalshare_info = $terminalshare_model->find(array('condition' => "shareid = '$shareid'"));


            if (count($terminalshare_info) > 0) {

                require_once './oss/samples/Common.php';
                $bucket = "porunoss";

                $timeout = 3600;
                $ossClient = Common::getOssClient();
                $object = $terminalshare_info->savepath . $terminalshare_info->truename . "-" . $terminalshare_info->filename;
                $filePath = $ossClient->signUrl($bucket, $object, $timeout);
                $name = $terminalshare_info->filename;

                header('Content-Type:application/octet-stream'); //文件的类型
                Header("Accept-Ranges: bytes");
                header('Content-Disposition:attachment;filename = "' . $name . '"'); //下载显示的名字
                ob_clean();
                flush();
                readfile($filePath);
                exit();
            } else {
                $this->redirect('./index.php?r=filesAdmin/files');
            }
        } else {
            header("HTTP/1.0 403");
        }
    }

    /*     * **********************文库文档后台分页**************************** */

    public function actiondelLibrarySerPage() {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=default/index');
        }
        $storeId = Yii::app()->session['storeid'];
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 2:
                    $orderSql = " order by c.username " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by c.phone " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by a.filename " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by a.credit " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by a.sharetime " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by d.storename " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
//搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
//分页
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }
        if (Yii::app()->session['storeid'] == 0) {
//定义查询数据总记录数sql
            $sumSql = "SELECT count(a.shareid) as sum "
                    . "FROM tbl_library a JOIN tbl_attachment b ON a.attachmentid=b.attachmentid JOIN tbl_user c ON a.userid=c.userid JOIN tbl_store d ON c._storeid=d.storeid "
                    . "where a.shareid >=0";
//条件过滤后记录数 必要
            $recordsFiltered = 0;
//表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $library_sum_info = $result->queryAll();
            foreach ($library_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
//定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.filename LIKE '%" . $search . "%' or c.username LIKE '%" . $search . "%' or c.phone LIKE '%" . $search . "%'  or d.storename LIKE '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $library_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($library_sum_filter_info[0]['sum'] > 0) {
                    foreach ($library_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.filename,c.phone,a.attachmentid,a.credit,a.sharetime,c._storeid,c.username,d.storename "
                    . "FROM tbl_library a JOIN tbl_attachment b ON a.attachmentid=b.attachmentid JOIN tbl_user c ON a.userid=c.userid JOIN tbl_store d ON c._storeid=d.storeid";
        } else {
//定义查询数据总记录数sql
            $sumSql = "SELECT count(a.shareid) as sum "
                    . "FROM tbl_library a JOIN tbl_attachment b ON a.attachmentid=b.attachmentid JOIN tbl_user c ON a.userid=c.userid JOIN tbl_store d ON c._storeid=d.storeid"
                    . " where c._storeid in ($storeId)";
//条件过滤后记录数 必要
            $recordsFiltered = 0;
//表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $terminalshare_info = $result->queryAll();
            foreach ($terminalshare_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
//定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.filename LIKE '%" . $search . "%' or c.username LIKE '%" . $search . "%' or c.phone LIKE '%" . $search . "%' or d.storename LIKE '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $library_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($library_sum_filter_info) > 0) {
                    foreach ($library_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.filename,c.phone,a.attachmentid,a.credit,a.sharetime,c.username,c._storeid,d.storename "
                    . "FROM tbl_library a JOIN tbl_attachment b ON a.attachmentid=b.attachmentid JOIN tbl_user c ON a.userid=c.userid JOIN tbl_store d ON c._storeid=d.storeid"
                    . " where c._storeid in ($storeId)";
        }
        if (strlen($search) > 0) {
//如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $library_info_filter = $dataResult->queryAll();
            if (count($library_info_filter) > 0) {
                $i = 0;
                foreach ($library_info_filter as $v) {
                    $i++;
                    $name = $v['username'];
                    $filename = $v['filename'];
                    $phone = $v['phone'];
                    $attachmentid = $v['attachmentid'];
                    $credit = $v['credit'];
                    $sharetime = $v['sharetime'];
                    $storename = $v['storename'];
                    $data1[] = array(
                        'attachmentid' => $attachmentid,
                        'id' => $i,
                        'name' => $name,
                        'phone' => $phone,
                        'filename' => $filename,
                        'credit' => $credit,
                        'sharetime' => $sharetime,
                        'storename' => $storename,
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            } else {
                $data1[] = array(
                    'attachmentid' => null,
                    'id' => null,
                    'name' => null,
                    'phone' => null,
                    'filename' => null,
                    'credit' => null,
                    'sharetime' => null,
                    'storename' => null,
                );
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
//直接查询所有记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $terminalshare_info = $dataResult->queryAll();
            $i = 0;
            foreach ($terminalshare_info as $v) {
                $i++;
                $name = $v['username'];
                $phone = $v['phone'];
                $filename = $v['filename'];
                $attachmentid = $v['attachmentid'];
                $credit = $v['credit'];
                $sharetime = $v['sharetime'];
                $storename = $v['storename'];
                $data1[] = array(
                    'attachmentid' => $attachmentid,
                    'id' => $i,
                    'name' => $name,
                    'phone' => $phone,
                    'filename' => $filename,
                    'credit' => $credit,
                    'sharetime' => $sharetime,
                    'storename' => $storename,
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

//------------------删除分享文档----------------------//            
    public function actiondeleteLib() {
        $attachmentId = intval($_POST["attachmentid"]);
        $library_model = library::model();
        $attachment_model = attachment::model();
        $isDel = false;
        $library_model->deleteAll("attachmentid='$attachmentId'");
        $attachmentId_info = $attachment_model->find("attachmentid='$attachmentId'");
        $attachmentId_info->share = 0;
        if ($attachmentId_info->update()) {
            $isDel = true;
        }
        if ($isDel) {
            $json = '{"data":"success"}';
            echo $json;
        } else {
            $json = '{"data":"false"}';
            echo $json;
        }
    }

//+++++++++++++++++++++++文档预览（文库审核auditdocment）+++++++++++++++++++++++++//
    public function actionauditLibpreview() {

        $attachmentId = intval($_POST["attachmentid"]);
        $attachment_model = attachment::model();
        $attachmentId_info = $attachment_model->find("attachmentid='$attachmentId'"); //id=6672
        $savepath = $attachmentId_info->savepath; //home/uploads/2016-02-19/
        $attachmentfile = $attachmentId_info->attachmentfile; //105-2016021913393337
        $ext = ($attachmentId_info->filenumber) > 1 ? 'swf' : 'png';
        $object = $savepath . $attachmentfile . '.' . $ext;

        require_once './oss/samples/Common.php';
        $bucket = "porunoss";

        $timeout = 3600;
        $ossClient = Common::getOssClient();

        $signedUrl = $ossClient->signUrl($bucket, $object, $timeout);
        $arr = get_headers($signedUrl, 1);
        $exist = false;
        if (preg_match('/200/', $arr[0])) {
            $exist = true;
        }
        $swf = $exist ? $signedUrl : '';
        if ($swf != '' && $ext == 'swf') {
            while (strpos($swf, "%2B") > -1) {
                $swf = $ossClient->signUrl($bucket, $object, 300);
            }
            $swf = str_replace('%', '----', $swf);
            $swf = $this->escape($swf);
            $swf = str_replace('----', '%', $swf);
        }
        $data = array(
            'swf' => $swf,
            'ext' => $ext,
        );
        echo json_encode($data);
    }

    public function escape($str) {
        $reString = '';
        preg_match_all("/[\x80-\xff].|[\x01-\x7f]+/", $str, $newstr);
        $ar = $newstr[0];
        foreach ($ar as $k => $v) {
            if (ord($ar[$k]) >= 127) {
                $tmpString = bin2hex($v);
                if (!eregi("WIN", PHP_OS)) {
                    $tmpString = substr($tmpString, 2, 2) . substr($tmpString, 0, 2);
                }
                $reString.="%u" . $tmpString;
            } else {
                $reString.= rawurlencode($v);
            }
        }
        return $reString;
    }

//-----------------------文档预览（文库审核auditdocment）-------------------------//
    /*     * **********************文库审核后台分页**************************** */
    public function actionauditLibrarySerPage() {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=default/index');
        }
        $storeId = Yii::app()->session['storeid'];
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 2:
                    $orderSql = " order by b.username " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by a.filename " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by a.credit " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by a.sharetime " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by c.storename " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
//搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
//分页
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }
        if (Yii::app()->session['storeid'] == 0) {
//定义查询数据总记录数sql
            $sumSql = "SELECT count(a.shareid) as sum FROM tbl_library a JOIN tbl_user b ON a.userid=b.userid JOIN tbl_store c ON b._storeid= c.storeid WHERE a.`status` in (1,2)";
//条件过滤后记录数 必要
            $recordsFiltered = 0;
//表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $library_sum_info = $result->queryAll();
            foreach ($library_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
//定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.filename LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or a.sharetime LIKE binary '%" . $search . "%' or c.storename LIKE '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $library_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($library_sum_filter_info[0]['sum'] > 0) {
                    foreach ($library_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.filename,a.shareid,a.attachmentid,a.credit,a.sharetime,b._storeid,b.username,c.storename "
                    . "FROM tbl_library a JOIN tbl_user b ON a.userid=b.userid JOIN tbl_store c ON b._storeid= c.storeid WHERE a.`status` in (1,2)";
        } else {
//定义查询数据总记录数sql
            $sumSql = "SELECT count(a.shareid) as sum FROM tbl_library a JOIN tbl_user b ON a.userid=b.userid JOIN tbl_store c ON b._storeid= c.storeid"
                    . " WHERE b._storeid in ($storeId) and a.`status` in (1,2)";
//条件过滤后记录数 必要
            $recordsFiltered = 0;
//表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $terminalshare_info = $result->queryAll();
            foreach ($terminalshare_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
//定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a.filename LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or a.sharetime LIKE binary '%" . $search . "%' or c.storename LIKE '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $library_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($library_sum_filter_info) > 0) {
                    foreach ($library_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.filename,a.shareid,a.attachmentid,a.credit,a.sharetime,b._storeid,b.username,c.storename "
                    . "FROM tbl_library a JOIN tbl_user b ON a.userid=b.userid JOIN tbl_store c ON b._storeid= c.storeid "
                    . "WHERE b._storeid in ($storeId) and a.`status` in (1,2)";
        }
        if (strlen($search) > 0) {
//如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $library_info_filter = $dataResult->queryAll();
            if (count($library_info_filter) > 0) {
                $i = 0;
                foreach ($library_info_filter as $v) {
                    $i++;
                    $name = $v['username'];
                    $filename = $v['filename'];
                    $attachmentid = $v['attachmentid'];
                    $credit = $v['credit'];
                    $sharetime = $v['sharetime'];
                    $storename = $v['storename'];
                    $data1[] = array(
                        'attachmentid' => $attachmentid,
                        'id' => $i,
                        'name' => $name,
                        'filename' => $filename,
                        'credit' => $credit,
                        'sharetime' => $sharetime,
                        'storename' => $storename,
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            } else {
                $data1[] = array(
                    'attachmentid' => null,
                    'id' => null,
                    'name' => null,
                    'filename' => null,
                    'credit' => null,
                    'sharetime' => null,
                    'storename' => null,
                );
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
//直接查询所有记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $terminalshare_info = $dataResult->queryAll();
            $i = 0;
            foreach ($terminalshare_info as $v) {
                $i++;
                $name = $v['username'];
                $filename = $v['filename'];
                $attachmentid = $v['attachmentid'];
                $credit = $v['credit'];
                $sharetime = $v['sharetime'];
                $storename = $v['storename'];
                $data1[] = array(
                    'attachmentid' => $attachmentid,
                    'id' => $i,
                    'name' => $name,
                    'filename' => $filename,
                    'credit' => $credit,
                    'sharetime' => $sharetime,
                    'storename' => $storename,
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

//+++++++++++++++++++++++通过分享（文库审核auditdocment）+++++++++++++++++++++++++//
    public function actionacceptdelShare() {
        $attachmentId = intval($_POST["attachmentid"]);
        $url = "http://dev.cqutprint.com/Library/shareDetect/attachmentid/" . $attachmentId . "/status/1";
        echo file_get_contents($url);
    }

//-----------------------文档预览（文库审核auditdocment）-------------------------//


    private function setCredit($businessid) {

        $subbusiness_model = subbusiness::model();
        $ids = $subbusiness_model->findAll(array("condition" => "_businessId = $businessid"));

        $Subbusiness = D('Subbusiness');
        $Attachment = D('Attachment');
        $Library = D('Library');
        $User = D('User');
        $ids = $Subbusiness->where(array('_string' => '_businessId=' . $businessid))->field('_attachmentId')->select();
        foreach ($ids as $id) {
            if ($Attachment->where(array('attachmentid' => $id['_attachmentid'], '_string' => '_userid=' . $this->userid))->count() == 0) {
                $list = $Library->where(array('attachmentid' => $id['_attachmentid']))->field('credit,userid')->find();
                if ($list['credit'] > 0) {
                    $User->where(array('userid' => $list['userid']))->setInc('integration', $list['credit']);
                }
            }
        }
    }

}
