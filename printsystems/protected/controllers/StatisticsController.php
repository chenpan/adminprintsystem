<?php

include 'BaseController.php';

class statisticsController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'school + school', //
            'terninalStatistics + terninalStatistics',
            'order + downloadorderexcel',
            'income + downloadincomeexcel',
            'refund + downloadrefundexcel',
            'register + downloadregisterexcel',
            'file + downloadfileexcel',
            'printpaper + downloadprintpaperexcel',
        );
    }

    public function filterschool($filterChain) {
        $this->checkAccess("统计", $filterChain);
    }

    public function filterterninalStatistics($filterChain) {
        $this->checkAccess("统计", $filterChain);
    }

    public function filterorder($filterChain) {
        $this->checkAccess("下载订单", $filterChain);
    }

    public function filterincome($filterChain) {
        $this->checkAccess("收入下载", $filterChain);
    }

    public function filterrefund($filterChain) {
        $this->checkAccess("退款下载", $filterChain);
    }

    public function filterregister($filterChain) {
        $this->checkAccess("注册人数下载", $filterChain);
    }

    public function filterfile($filterChain) {
        $this->checkAccess("上传文件下载", $filterChain);
    }

    public function filterprintpaper($filterChain) {
        $this->checkAccess("打印纸张下载", $filterChain);
    }

    /*     * ************** 统计选择学校 start ************** */

    public function actionschool($type) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset(Yii::app()->session['adminuser'])) {
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $store_model = store::model();
            if (Yii::app()->session['storeid'] == 0) {
                $store_info = $store_model->findAll(array('order' => "storeid DESC"));
            } else {
                $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . Yii::app()->session["storeid"] . ")", array('order' => "storeid DESC"));
            }
            if ($type == "order") {
                $statisticssorder = new statisticssorder();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $orderarr = $statisticssorder->statsticssorder_day($storeid = Yii::app()->session['storeid'], $starttime, $endtime, $types);
                $flagorder = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "下载订单") {
                        $flagorder = "";
                    }
                }
                $this->renderPartial('orderschool', array('store_info' => $store_info, 'flagorder' => $flagorder, 'orderarr' => $orderarr, 'leftContent' => $leftContent, 'recommend' => $recommend, 'type' => $type));
            } else if ($type == "income") {
                $statisticssincome = new statisticsincome();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $incomearr = $statisticssincome->statistics_income($storeid = Yii::app()->session['storeid'], $starttime, $endtime, $types);
                $flagincome = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "收入下载") {
                        $flagincome = "";
                    }
                }
                $this->renderPartial('incomeschool', array('store_info' => $store_info, 'flagincome' => $flagincome, 'incomearr' => $incomearr, 'leftContent' => $leftContent, 'recommend' => $recommend, 'type' => $type));
            } else if ($type == "refund") {
                $statisticssrefund = new statisticsrefund();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $refundarr = $statisticssrefund->statistics_refund($storeid = Yii::app()->session['storeid'], $starttime, $endtime, $types);
                $flagrefund = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "退款下载") {
                        $flagrefund = "";
                    }
                }
                $this->renderPartial('refundschool', array('store_info' => $store_info, 'flagrefund' => $flagrefund, 'refundarr' => $refundarr, 'leftContent' => $leftContent, 'recommend' => $recommend, 'type' => $type));
            } else if ($type == "register") {
                $statisticssregister = new statisticsregister();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $registerarr = $statisticssregister->statistics_register($storeid = Yii::app()->session['storeid'], $starttime, $endtime, $types);
                $flagregister = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "注册人数下载") {
                        $flagregister = "";
                    }
                }
                $this->renderPartial('registerschool', array('store_info' => $store_info, 'flagregister' => $flagregister, 'registerarr' => $registerarr, 'leftContent' => $leftContent, 'recommend' => $recommend, 'type' => $type));
            } else if ($type == "printpaper") {
                $statisticssregister = new statisticsprintpaper();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $totalarr = $statisticssregister->statistics_printpaper_total($storeid = Yii::app()->session['storeid'], $starttime, $endtime, $types);
                $flagprintpaper = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "打印纸张下载") {
                        $flagprintpaper = "";
                    }
                }
                $this->renderPartial('printpaperschool', array('store_info' => $store_info, 'flagprintpaper' => $flagprintpaper, 'totalarr' => $totalarr, 'leftContent' => $leftContent, 'recommend' => $recommend, 'type' => $type));
            } else if ($type == "file") {
                $statisticsfile = new statisticsfile();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $filearr = $statisticsfile->statisticsfile_day($storeid = Yii::app()->session['storeid'], $starttime, $endtime, $types);
                $flagfile = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "上传文件下载") {
                        $flagfile = "";
                    }
                }
                $this->renderPartial('fileschool', array('store_info' => $store_info, 'flagfile' => $flagfile, 'filearr' => $filearr, 'leftContent' => $leftContent, 'recommend' => $recommend, 'type' => $type));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 统计选择学校 end ************** */

    public function actionterninalStatistics($storeid, $type) {//根据type的不同，跳转到不同的界面去统计
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset(Yii::app()->session['adminuser'])) {
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            if ($type == "order") {//订单统计
                $statisticssorder = new statisticssorder();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $orderarr = $statisticssorder->statsticssorder_day($storeid, $starttime, $endtime, $types);
                $flagorder = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "下载订单") {
                        $flagorder = "";
                    }
                }
                $this->renderPartial('statisticOrderone', array("orderarr" => $orderarr, "storeid" => $storeid, "flagorder" => $flagorder, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
            if ($type == "income") {//收入统计
                $statisticssincome = new statisticsincome();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $incomearr = $statisticssincome->statistics_income($storeid, $starttime, $endtime, $types);
                $flagincome = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "收入下载") {
                        $flagincome = "";
                    }
                }
                $this->renderPartial('statisticIncomeone', array("incomearr" => $incomearr, "storeid" => $storeid, "flagincome" => $flagincome, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
            if ($type == "refund") {//退款统计
                $statisticssrefund = new statisticsrefund();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $refundarr = $statisticssrefund->statistics_refund($storeid, $starttime, $endtime, $types);
                $flagrefund = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "退款下载") {
                        $flagrefund = "";
                    }
                }
                $this->renderPartial('statisticRefundone', array('refundarr' => $refundarr, "storeid" => $storeid, 'flagrefund' => $flagrefund, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
            if ($type == "register") {
                $statisticssregister = new statisticsregister();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $registerarr = $statisticssregister->statistics_register($storeid, $starttime, $endtime, $types);
                $flagregister = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "注册人数下载") {
                        $flagregister = "";
                    }
                }
                $this->renderPartial('statisticregisterone', array('registerarr' => $registerarr, "storeid" => $storeid, 'flagregister' => $flagregister, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
            if ($type == "printpaper") {//打印纸张统计
                $statisticsprintpaper = new statisticsprintpaper();
                $starttime = date("Y-m") . "-1";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";

                $printpaperarr = $statisticsprintpaper->statistics_printpaper($storeid, $starttime, $endtime);
                $flagprintpaper = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "打印纸张下载") {
                        $flagprintpaper = "";
                    }
                }
                $this->renderPartial('statisticsPaper', array('printpaperarr' => $printpaperarr, "storeid" => $storeid, 'flagprintpaper' => $flagprintpaper, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }if ($type == "file") {
                $statisticsfile = new statisticsfile();
                $starttime = date("Y-m") . "-01";
                $fate = date("d"); //当天是多少号
                $endtime = date("Y-m") . "-$fate";
                $types = "day";
                $filearr = $statisticsfile->statisticsfile_day($storeid, $starttime, $endtime, $types);
                $flagfile = "hidden";
                foreach ($assign as $value) {
                    $id = $value->_itemId;
                    $assign_info = $item_model->find("itemId ='$id'");
                    $itemName = $assign_info->itemName;
                    if ($itemName == "上传文件下载") {
                        $flagfile = "";
                    }
                }
                $this->renderPartial('statisticsfile', array('filearr' => $filearr, "storeid" => $storeid, 'flagfile' => $flagfile, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * *************************订单统计***************************** */

//按时间段查询
    public function actionsearch_statisticOrderone() {
        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $types = $_POST["types"];

        $statisticssorder = new statisticssorder();
        $orderarr_month = $statisticssorder->statsticssorder_day($storeid, $starttime, $endtime, $types);
        $json_string = json_encode($orderarr_month);
        echo $json_string;
    }

//下载订单统计excel
    public function actiondownloadorderexcel($storeid, $dstarttime, $dendtime) {
        $store_model = store::model();
        header('Content-Type: application/vnd.ms-excel');
        if ($storeid != 0) {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '订单统计.xls');
        } else {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '订单总量统计.xls');
        }
        header('Pragma: no-cache');
        header('Expires: 0');

        if ($storeid != 0) {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 订单统计');
        } else {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 订单总量统计');
        }

        $statisticssorder = new statisticssorder();

        $starttime = $dstarttime;
        $endtimes = $dendtime;
        $types = "day";
//        $orderarray = array();

        $timearray = array(); //第一行时间
        $dayArray_total = array(); //总订单量
        $dayArray = array(); //web订单量
        $dayArray_pro = array(); //终端订单量
        $dayArray_PC = array(); //webPC订单量
        $dayArray_APP = array(); //webAPP订单量
        $dayArray_WX = array(); //webWX订单量
        $dayArray_refund_total = array(); //总订单退款量
        $dayArray_refund_web = array(); //web订单退款量
        $dayArray_refund_terminal = array(); //终端订单退款量
        $dayArray_refundrate_total = array(); //订单总退款率
        $dayArray_refundrate_web = array(); //web订单退款率
        $dayArray_refundrate_terminal = array(); //终端订单退款率

        array_push($timearray, "时间");
        array_push($dayArray_total, "订单总量");
        array_push($dayArray, "WEB订单量");
        array_push($dayArray_pro, "终端订单量");
        array_push($dayArray_PC, "PC端订单量");
        array_push($dayArray_APP, "APP端订单量");
        array_push($dayArray_WX, "微信端订单量");
        array_push($dayArray_refund_total, "退款订单总量");
        array_push($dayArray_refund_web, "WEB端退款订单量");
        array_push($dayArray_refund_terminal, "终端订单退款量");
        array_push($dayArray_refundrate_total, "退款订单率");
        array_push($dayArray_refundrate_web, "WEB端退款率");
        array_push($dayArray_refundrate_terminal, "终端退款率");


        $orderarr = $statisticssorder->statsticssorder_day($storeid, $starttime, $endtimes, $types);


        array_push($timearray, "总量"); //订单总量
        array_push($dayArray_total, $orderarr["totalCount_dattotals"] . "单"); //订单总量
        array_push($dayArray, $orderarr["totalCount_dayss"] . "单"); //WEB订单量
        array_push($dayArray_pro, $orderarr["totalCount_dayspros"] . "单"); //终端订单量

        array_push($dayArray_PC, $orderarr["totalCount_dayss_PC"] . "单"); //PC端订单量
        array_push($dayArray_APP, $orderarr["totalCount_dayss_APP"] . "单"); //APP端订单量
        array_push($dayArray_WX, $orderarr["totalCount_dayss_WX"] . "单"); //微信端订单量

        array_push($dayArray_refund_total, $orderarr["totalCount_days_refund"] . "单"); //退款订单总量
        array_push($dayArray_refund_web, $orderarr["totalCount_days_refund_web"] . "单"); //WEB端退款订单量
        array_push($dayArray_refund_terminal, $orderarr["totalCount_days_refund_terminal"] . "单"); //终端退款订单量
        if ($orderarr["totalCount_dattotals"] != 0) {
            array_push($dayArray_refundrate_total, sprintf("%.2f", $orderarr["totalCount_days_refund"] / $orderarr["totalCount_dattotals"] * 100) . "%"); //退款订单率
        } else {
            array_push($dayArray_refundrate_total, "0%"); //退款订单率
        }
        if ($orderarr["totalCount_dayss"] != 0) {
            array_push($dayArray_refundrate_web, sprintf("%.2f", $orderarr["totalCount_days_refund_web"] / $orderarr["totalCount_dayss"] * 100) . "%"); //WEB端退款率
        } else {
            array_push($dayArray_refundrate_web, "0%"); //退款订单率
        }
        if ($orderarr["totalCount_dayspros"] != 0) {
            array_push($dayArray_refundrate_terminal, sprintf("%.2f", $orderarr["totalCount_days_refund_terminal"] / $orderarr["totalCount_dayspros"] * 100) . "%"); //终端退款率
        } else {
            array_push($dayArray_refundrate_terminal, "0%"); //退款订单率
        }

        while ($starttime <= $dendtime) {

            $orderarrx = $statisticssorder->statsticssorder_day($storeid, $starttime, $starttime, $types);
            array_push($timearray, $starttime); //时间
            array_push($dayArray_total, $orderarrx["totalCount_dattotals"] . "单"); //订单总量
            array_push($dayArray, $orderarrx["totalCount_dayss"] . "单"); //WEB订单量
            array_push($dayArray_pro, $orderarrx["totalCount_dayspros"] . "单"); //终端订单量
            array_push($dayArray_PC, $orderarrx["totalCount_dayss_PC"] . "单"); //PC端订单量
            array_push($dayArray_APP, $orderarrx["totalCount_dayss_APP"] . "单"); //APP端订单量
            array_push($dayArray_WX, $orderarrx["totalCount_dayss_WX"] . "单"); //微信端订单量
            array_push($dayArray_refund_total, $orderarrx["totalCount_days_refund"] . "单"); //退款订单总量
            array_push($dayArray_refund_web, $orderarrx["totalCount_days_refund_web"] . "单"); //WEB端退款订单量
            array_push($dayArray_refund_terminal, $orderarrx["totalCount_days_refund_terminal"] . "单"); //终端退款订单量
            if ($orderarrx["totalCount_dattotals"] != 0) {
                array_push($dayArray_refundrate_total, sprintf("%.2f", $orderarrx["totalCount_days_refund"] / $orderarrx["totalCount_dattotals"] * 100) . "%"); //退款订单率
            } else {
                array_push($dayArray_refundrate_total, "0%"); //退款订单率
            }
            if ($orderarrx["totalCount_dayss"] != 0) {
                array_push($dayArray_refundrate_web, sprintf("%.2f", $orderarrx["totalCount_days_refund_web"] / $orderarrx["totalCount_dayss"] * 100) . "%"); //WEB端退款率
            } else {
                array_push($dayArray_refundrate_web, "0%"); //退款订单率
            }
            if ($orderarrx["totalCount_dayspros"] != 0) {
                array_push($dayArray_refundrate_terminal, sprintf("%.2f", $orderarrx["totalCount_days_refund_terminal"] / $orderarrx["totalCount_dayspros"] * 100) . "%"); //终端退款率
            } else {
                array_push($dayArray_refundrate_terminal, "0%"); //退款订单率
            }

            $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
        }
        $orderarray = array(
            $timearray,
            $dayArray_total,
            $dayArray,
            $dayArray_pro,
            $dayArray_PC,
            $dayArray_APP,
            $dayArray_WX,
            $dayArray_refund_total,
            $dayArray_refund_web,
            $dayArray_refund_terminal,
            $dayArray_refundrate_total,
            $dayArray_refundrate_web,
            $dayArray_refundrate_terminal
        );

        if ($storeid == 0) {
            $store_infos = $store_model->findAll(array("order" => "storeid asc"));
        } else {
            $store_infos = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }
        foreach ($store_infos as $k => $l) {

            $titles = array($l->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 订单统计');

            $statisticssorder = new statisticssorder();

            $starttimes = $dstarttime;
            $endtimes = $dendtime;
            $types = "day";

            $timearray = array(); //第一行时间
            $dayArray_total = array(); //总订单量
            $dayArray = array(); //web订单量
            $dayArray_pro = array(); //终端订单量
            $dayArray_PC = array(); //webPC订单量
            $dayArray_APP = array(); //webAPP订单量
            $dayArray_WX = array(); //webWX订单量
            $dayArray_refund_total = array(); //总订单退款量
            $dayArray_refund_web = array(); //web订单退款量
            $dayArray_refund_terminal = array(); //终端订单退款量
            $dayArray_refundrate_total = array(); //订单总退款率
            $dayArray_refundrate_web = array(); //web订单退款率
            $dayArray_refundrate_terminal = array(); //终端订单退款率

            array_push($timearray, "时间");
            array_push($dayArray_total, "订单总量");
            array_push($dayArray, "WEB订单量");
            array_push($dayArray_pro, "终端订单量");
            array_push($dayArray_PC, "PC端订单量");
            array_push($dayArray_APP, "APP端订单量");
            array_push($dayArray_WX, "微信端订单量");
            array_push($dayArray_refund_total, "退款订单总量");
            array_push($dayArray_refund_web, "WEB端退款订单量");
            array_push($dayArray_refund_terminal, "终端订单退款量");
            array_push($dayArray_refundrate_total, "退款订单率");
            array_push($dayArray_refundrate_web, "WEB端退款率");
            array_push($dayArray_refundrate_terminal, "终端退款率");

            $orderarr = $statisticssorder->statsticssorder_day($l->storeid, $starttimes, $endtimes, $types);

            array_push($timearray, "总量"); //订单总量
            array_push($dayArray_total, $orderarr["totalCount_dattotals"] . "单"); //订单总量
            array_push($dayArray, $orderarr["totalCount_dayss"] . "单"); //WEB订单量
            array_push($dayArray_pro, $orderarr["totalCount_dayspros"] . "单"); //终端订单量

            array_push($dayArray_PC, $orderarr["totalCount_dayss_PC"] . "单"); //PC端订单量
            array_push($dayArray_APP, $orderarr["totalCount_dayss_APP"] . "单"); //APP端订单量
            array_push($dayArray_WX, $orderarr["totalCount_dayss_WX"] . "单"); //微信端订单量

            array_push($dayArray_refund_total, $orderarr["totalCount_days_refund"] . "单"); //退款订单总量
            array_push($dayArray_refund_web, $orderarr["totalCount_days_refund_web"] . "单"); //WEB端退款订单量
            array_push($dayArray_refund_terminal, $orderarr["totalCount_days_refund_terminal"] . "单"); //终端退款订单量
            if ($orderarr["totalCount_dattotals"] != 0) {
                array_push($dayArray_refundrate_total, sprintf("%.2f", $orderarr["totalCount_days_refund"] / $orderarr["totalCount_dattotals"] * 100) . "%"); //退款订单率
            } else {
                array_push($dayArray_refundrate_total, "0%"); //退款订单率
            }
            if ($orderarr["totalCount_dayss"] != 0) {
                array_push($dayArray_refundrate_web, sprintf("%.2f", $orderarr["totalCount_days_refund_web"] / $orderarr["totalCount_dayss"] * 100) . "%"); //WEB端退款率
            } else {
                array_push($dayArray_refundrate_web, "0%"); //退款订单率
            }
            if ($orderarr["totalCount_dayspros"] != 0) {
                array_push($dayArray_refundrate_terminal, sprintf("%.2f", $orderarr["totalCount_days_refund_terminal"] / $orderarr["totalCount_dayspros"] * 100) . "%"); //终端退款率
            } else {
                array_push($dayArray_refundrate_terminal, "0%"); //退款订单率
            }

            while ($starttimes <= $dendtime) {

                $orderarrx = $statisticssorder->statsticssorder_day($l->storeid, $starttimes, $starttimes, $types);
                array_push($timearray, $starttimes); //时间
                array_push($dayArray_total, $orderarrx["totalCount_dattotals"] . "单"); //订单总量
                array_push($dayArray, $orderarrx["totalCount_dayss"] . "单"); //WEB订单量
                array_push($dayArray_pro, $orderarrx["totalCount_dayspros"] . "单"); //终端订单量
                array_push($dayArray_PC, $orderarrx["totalCount_dayss_PC"] . "单"); //PC端订单量
                array_push($dayArray_APP, $orderarrx["totalCount_dayss_APP"] . "单"); //APP端订单量
                array_push($dayArray_WX, $orderarrx["totalCount_dayss_WX"] . "单"); //微信端订单量
                array_push($dayArray_refund_total, $orderarrx["totalCount_days_refund"] . "单"); //退款订单总量
                array_push($dayArray_refund_web, $orderarrx["totalCount_days_refund_web"] . "单"); //WEB端退款订单量
                array_push($dayArray_refund_terminal, $orderarrx["totalCount_days_refund_terminal"] . "单"); //终端退款订单量
                if ($orderarrx["totalCount_dattotals"] != 0) {
                    array_push($dayArray_refundrate_total, sprintf("%.2f", $orderarrx["totalCount_days_refund"] / $orderarrx["totalCount_dattotals"] * 100) . "%"); //退款订单率
                } else {
                    array_push($dayArray_refundrate_total, "0%"); //退款订单率
                }
                if ($orderarrx["totalCount_dayss"] != 0) {
                    array_push($dayArray_refundrate_web, sprintf("%.2f", $orderarrx["totalCount_days_refund_web"] / $orderarrx["totalCount_dayss"] * 100) . "%"); //WEB端退款率
                } else {
                    array_push($dayArray_refundrate_web, "0%"); //退款订单率
                }
                if ($orderarrx["totalCount_dayspros"] != 0) {
                    array_push($dayArray_refundrate_terminal, sprintf("%.2f", $orderarrx["totalCount_days_refund_terminal"] / $orderarrx["totalCount_dayspros"] * 100) . "%"); //终端退款率
                } else {
                    array_push($dayArray_refundrate_terminal, "0%"); //退款订单率
                }

                $starttimes = date('Y-m-d', strtotime('+1 day', strtotime($starttimes)));
            }
            array_push($orderarray, $titles, $timearray, $dayArray_total, $dayArray, $dayArray_pro, $dayArray_PC, $dayArray_APP, $dayArray_WX, $dayArray_refund_total, $dayArray_refund_web, $dayArray_refund_terminal, $dayArray_refundrate_total, $dayArray_refundrate_web, $dayArray_refundrate_terminal);
        }
        echo iconv('utf-8', 'gbk', implode("\t", $title)), "\n";
        foreach ($orderarray as $value) {
            echo iconv('utf-8', 'gbk', implode("\t", $value)), "\n";
        }
    }

    /*     * *************************订单统计***************************** */
    /*     * *************************收入统计***************************** */

    public function actionsearch_statisticIncomeone() {

        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $types = $_POST["types"];

        $statisticssincome = new statisticsincome();
        $incomearr_month = $statisticssincome->statistics_income($storeid, $starttime, $endtime, $types);
        $json_string = json_encode($incomearr_month);
        echo $json_string;
    }

//下载收入统计excel
    public function actiondownloadincomeexcel($storeid, $dstarttime, $dendtime) {
        $store_model = store::model();
        header('Content-Type: application/vnd.ms-excel');
        if ($storeid != 0) {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '收入统计.xls');
        } else {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '总收入统计.xls');
        }
        header('Pragma: no-cache');
        header('Expires: 0');
        if ($storeid != 0) {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 收入统计');
        } else {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 总收入统计');
        }

        $statisticssincome = new statisticsincome();

        $starttime = $dstarttime;
        $endtimes = $dendtime;
        $types = "day";

        $timearray = array(); //第一行时间
        $array1 = array(); //不除去退款总收入
        $array2 = array(); //除去退款总收入
        $array3 = array(); //不除去退款积分总收入
        $array4 = array(); //除去退款积分总收入
        $array5 = array(); //不除去退款金额总收入
        $array6 = array(); //除去退款金额总收入
        $array7 = array(); //积分退款率
        $array8 = array(); //金额退款率
        $array9 = array(); //收入退款率
        $array10 = array(); //除去退款支付宝总收入
        $array11 = array(); //不除去退款支付宝总收入
        $array12 = array(); //除去退款微信总收入
        $array13 = array(); //不除去退款微信总收入

        array_push($timearray, "时间");
        array_push($array1, "不除去退款总收入");
        array_push($array2, "除去退款总收入");
        array_push($array3, "不除去退款积分总收入");
        array_push($array4, "除去退款积分总收入");
        array_push($array5, "不除去退款金额总收入");
        array_push($array6, "除去退款金额总收入");
        array_push($array7, "积分退款率");
        array_push($array8, "金额退款率");
        array_push($array9, "收入退款率");
        array_push($array10, "除去退款支付宝总收入");
        array_push($array11, "不除去退款支付宝总收入");
        array_push($array12, "除去退款微信总收入");
        array_push($array13, "不除去退款微信总收入");

        $incomearr = $statisticssincome->statistics_income($storeid, $starttime, $endtimes, $types);

        array_push($timearray, "总量"); //订单总量
        array_push($array1, $incomearr["totalIncome_dayss"] . "元");
        array_push($array2, $incomearr["totalIncome_removerefund_dayss"] . "元");
        array_push($array3, $incomearr["total_integrals"] . "分");
        array_push($array4, $incomearr["totalIncome_removerefund_integrals"] . "分");
        array_push($array5, $incomearr["total_moneys"] . "元");
        array_push($array6, $incomearr["total_removerefund_money"] . "元");
        array_push($array7, $incomearr["total_refundrate_integral"] . "%");
        array_push($array8, $incomearr["total_refundrate_money"] . "%");
        array_push($array9, $incomearr["total_refundrate"] . "%");
        array_push($array10, $incomearr["total_removerefund_money_zfbs"] . "元");
        array_push($array11, $incomearr["total_money_zfbs"] . "元");
        array_push($array12, $incomearr["total_removerefund_money_wxs"] . "元");
        array_push($array13, $incomearr["total_money_wxs"] . "元");

        while ($starttime <= $dendtime) {

            $incomearrx = $statisticssincome->statistics_income($storeid, $starttime, $starttime, $types);
            array_push($timearray, $starttime); //时间
            array_push($array1, $incomearrx["totalIncome_dayss"] . "元");
            array_push($array2, $incomearrx["totalIncome_removerefund_dayss"] . "元");
            array_push($array3, $incomearrx["total_integrals"] . "分");
            array_push($array4, $incomearrx["totalIncome_removerefund_integrals"] . "分");
            array_push($array5, $incomearrx["total_moneys"] . "元");
            array_push($array6, $incomearrx["total_removerefund_money"] . "元");
            array_push($array7, $incomearrx["total_refundrate_integral"] . "%");
            array_push($array8, $incomearrx["total_refundrate_money"] . "%");
            array_push($array9, $incomearrx["total_refundrate"] . "%");
            array_push($array10, $incomearrx["total_removerefund_money_zfbs"] . "元");
            array_push($array11, $incomearrx["total_money_zfbs"] . "元");
            array_push($array12, $incomearrx["total_removerefund_money_wxs"] . "元");
            array_push($array13, $incomearrx["total_money_wxs"] . "元");

            $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
        }
        $incomearray = array(
            $timearray,
            $array1,
            $array2,
            $array3,
            $array4,
            $array5,
            $array6,
            $array7,
            $array8,
            $array9,
            $array10,
            $array11,
            $array12,
            $array13
        );
        if ($storeid == 0) {
            $store_infos = $store_model->findAll(array("order" => "storeid asc"));
        } else {
            $store_infos = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }
        foreach ($store_infos as $k => $l) {

            $titles = array($l->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 收入统计');

            $statisticssincome = new statisticsincome();

            $starttimes = $dstarttime;
            $endtimes = $dendtime;
            $types = "day";

            $timearray = array(); //第一行时间
            $array1 = array(); //不除去退款总收入
            $array2 = array(); //除去退款总收入
            $array3 = array(); //不除去退款积分总收入
            $array4 = array(); //除去退款积分总收入
            $array5 = array(); //不除去退款金额总收入
            $array6 = array(); //除去退款金额总收入
            $array7 = array(); //积分退款率
            $array8 = array(); //金额退款率
            $array9 = array(); //收入退款率
            $array10 = array(); //除去退款支付宝总收入
            $array11 = array(); //不除去退款支付宝总收入
            $array12 = array(); //除去退款微信总收入
            $array13 = array(); //不除去退款微信总收入

            array_push($timearray, "时间");
            array_push($array1, "不除去退款总收入");
            array_push($array2, "除去退款总收入");
            array_push($array3, "不除去退款积分总收入");
            array_push($array4, "除去退款积分总收入");
            array_push($array5, "不除去退款金额总收入");
            array_push($array6, "除去退款金额总收入");
            array_push($array7, "积分退款率");
            array_push($array8, "金额退款率");
            array_push($array9, "收入退款率");
            array_push($array10, "除去退款支付宝总收入");
            array_push($array11, "不除去退款支付宝总收入");
            array_push($array12, "除去退款微信总收入");
            array_push($array13, "不除去退款微信总收入");

            $incomearr = $statisticssincome->statistics_income($l->storeid, $starttimes, $endtimes, $types);

            array_push($timearray, "总量"); //订单总量
            array_push($array1, $incomearr["totalIncome_dayss"] . "元");
            array_push($array2, $incomearr["totalIncome_removerefund_dayss"] . "元");
            array_push($array3, $incomearr["total_integrals"] . "分");
            array_push($array4, $incomearr["totalIncome_removerefund_integrals"] . "分");
            array_push($array5, $incomearr["total_moneys"] . "元");
            array_push($array6, $incomearr["total_removerefund_money"] . "元");
            array_push($array7, $incomearr["total_refundrate_integral"] . "%");
            array_push($array8, $incomearr["total_refundrate_money"] . "%");
            array_push($array9, $incomearr["total_refundrate"] . "%");
            array_push($array10, $incomearr["total_removerefund_money_zfbs"] . "元");
            array_push($array11, $incomearr["total_money_zfbs"] . "元");
            array_push($array12, $incomearr["total_removerefund_money_wxs"] . "元");
            array_push($array13, $incomearr["total_money_wxs"] . "元");

            while ($starttimes <= $dendtime) {

                $incomearrx = $statisticssincome->statistics_income($l->storeid, $starttimes, $starttime, $types);
                array_push($timearray, $starttimes); //时间
                array_push($array1, $incomearrx["totalIncome_dayss"] . "元");
                array_push($array2, $incomearrx["totalIncome_removerefund_dayss"] . "元");
                array_push($array3, $incomearrx["total_integrals"] . "分");
                array_push($array4, $incomearrx["totalIncome_removerefund_integrals"] . "分");
                array_push($array5, $incomearrx["total_moneys"] . "元");
                array_push($array6, $incomearrx["total_removerefund_money"] . "元");
                array_push($array7, $incomearrx["total_refundrate_integral"] . "%");
                array_push($array8, $incomearrx["total_refundrate_money"] . "%");
                array_push($array9, $incomearrx["total_refundrate"] . "%");
                array_push($array10, $incomearrx["total_removerefund_money_zfbs"] . "元");
                array_push($array11, $incomearrx["total_money_zfbs"] . "元");
                array_push($array12, $incomearrx["total_removerefund_money_wxs"] . "元");
                array_push($array13, $incomearrx["total_money_wxs"] . "元");

                $starttimes = date('Y-m-d', strtotime('+1 day', strtotime($starttimes)));
            }
            array_push($incomearray, $titles, $timearray, $array1, $array2, $array3, $array4, $array5, $array6, $array7, $array8, $array9, $array10, $array11, $array12, $array13);
        }

        echo iconv('utf-8', 'gbk', implode("\t", $title)), "\n";
        foreach ($incomearray as $value) {
            echo iconv('utf-8', 'gbk', implode("\t", $value)), "\n";
        }
    }

    /*     * *************************收入统计***************************** */
    /*     * *************************退款统计***************************** */

    public function actionsearch_statisticRefundone() {

        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $types = $_POST["types"];

        $statisticsrefund = new statisticsrefund();
        $refundarr_month = $statisticsrefund->statistics_refund($storeid, $starttime, $endtime, $types);
        $json_string = json_encode($refundarr_month);
        echo $json_string;
    }

//下载退款统计excel
    public function actiondownloadrefundexcel($storeid, $dstarttime, $dendtime) {
        $store_model = store::model();
        header('Content-Type: application/vnd.ms-excel');
        if ($storeid != 0) {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '退款统计.xls');
        } else {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '总退款统计.xls');
        }
        header('Pragma: no-cache');
        header('Expires: 0');
        if ($storeid != 0) {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 退款统计');
        } else {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 总退款统计');
        }

        $statisticssrefund = new statisticsrefund();

        $starttime = $dstarttime;
        $endtimes = $dendtime;
        $types = "day";

        $timearray = array(); //第一行时间
        $array1 = array(); //总退款金额
        $array2 = array(); //退款积分
        $array3 = array(); //退款金额

        array_push($timearray, "时间");
        array_push($array1, "总退款金额");
        array_push($array2, "退款积分");
        array_push($array3, "退款金额");

        $refundarr = $statisticssrefund->statistics_refund($storeid, $starttime, $endtimes, $types);

        array_push($timearray, "总量"); //订单总量
        array_push($array1, $refundarr["refund_totals"] . "元");
        array_push($array2, $refundarr["refund_integral_totals"] . "分");
        array_push($array3, $refundarr["refund_money_totals"] . "元");

        while ($starttime <= $dendtime) {

            $refundarrx = $statisticssrefund->statistics_refund($storeid, $starttime, $starttime, $types);
            array_push($timearray, $starttime); //时间
            array_push($array1, $refundarrx["refund_totals"] . "元");
            array_push($array2, $refundarrx["refund_integral_totals"] . "分");
            array_push($array3, $refundarrx["refund_money_totals"] . "元");

            $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
        }
        $refundarray = array(
            $timearray,
            $array1,
            $array2,
            $array3
        );
        if ($storeid == 0) {
            $store_infos = $store_model->findAll(array("order" => "storeid asc"));
        } else {
            $store_infos = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }
        foreach ($store_infos as $k => $l) {

            $titles = array($l->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 退款统计');

            $statisticssrefund = new statisticsrefund();

            $starttimes = $dstarttime;
            $endtimes = $dendtime;
            $types = "day";

            $timearray = array(); //第一行时间
            $array1 = array(); //总退款金额
            $array2 = array(); //退款积分
            $array3 = array(); //退款金额

            array_push($timearray, "时间");
            array_push($array1, "总退款金额");
            array_push($array2, "退款积分");
            array_push($array3, "退款金额");

            $refundarr = $statisticssrefund->statistics_refund($l->storeid, $starttimes, $endtimes, $types);

            array_push($timearray, "总量"); //
            array_push($array1, $refundarr["refund_totals"] . "元");
            array_push($array2, $refundarr["refund_integral_totals"] . "分");
            array_push($array3, $refundarr["refund_money_totals"] . "元");

            while ($starttimes <= $dendtime) {

                $refundarrx = $statisticssrefund->statistics_refund($l->storeid, $starttimes, $starttimes, $types);
                array_push($timearray, $starttimes); //时间
                array_push($array1, $refundarrx["refund_totals"] . "元");
                array_push($array2, $refundarrx["refund_integral_totals"] . "分");
                array_push($array3, $refundarrx["refund_money_totals"] . "元");

                $starttimes = date('Y-m-d', strtotime('+1 day', strtotime($starttimes)));
            }
            array_push($refundarray, $titles, $timearray, $array1, $array2, $array3);
        }

        echo iconv('utf-8', 'gbk', implode("\t", $title)), "\n";
        foreach ($refundarray as $value) {
            echo iconv('utf-8', 'gbk', implode("\t", $value)), "\n";
        }
    }

    /*     * *************************退款统计***************************** */
    /*     * *************************注册人数统计***************************** */

    public function actionsearch_statisticregisterone() {

        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $types = $_POST["types"];

        $statisticsregister = new statisticsregister();
        $registerarr_month = $statisticsregister->statistics_register($storeid, $starttime, $endtime, $types);
        $json_string = json_encode($registerarr_month);
        echo $json_string;
    }

//下载注册人数EXCEL
    public function actiondownloadregisterexcel($storeid, $dstarttime, $dendtime) {
        $store_model = store::model();
        header('Content-Type: application/vnd.ms-excel');
        if ($storeid != 0) {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '注册人数统计.xls');
        } else {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '总注册人数统计.xls');
        }
        header('Pragma: no-cache');
        header('Expires: 0');
        if ($storeid != 0) {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 注册人数统计');
        } else {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 总注册人数统计');
        }
        $statisticssregister = new statisticsregister();

        $starttime = $dstarttime;
        $endtimes = $dendtime;
        $types = "day";

        $timearray = array(); //第一行时间
        $array1 = array(); //总退款金额

        array_push($timearray, "时间");
        array_push($array1, "注册人数");

        $registerarr = $statisticssregister->statistics_register($storeid, $starttime, $endtimes, $types);

        array_push($timearray, "总量"); //订单总量
        array_push($array1, $registerarr["register_total"] . "人");

        while ($starttime <= $dendtime) {

            $registerarrx = $statisticssregister->statistics_register($storeid, $starttime, $starttime, $types);
            array_push($timearray, $starttime); //时间
            array_push($array1, $registerarrx["register_total"] . "人");

            $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
        }
        $registerarray = array(
            $timearray,
            $array1
        );
        if ($storeid == 0) {
            $store_infos = $store_model->findAll(array("order" => "storeid asc"));
        } else {
            $store_infos = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }
        foreach ($store_infos as $k => $l) {

            $titles = array($l->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 注册人数统计');

            $statisticssregister = new statisticsregister();

            $starttimes = $dstarttime;
            $endtimes = $dendtime;
            $types = "day";

            $timearray = array(); //第一行时间
            $array1 = array(); //总退款金额

            array_push($timearray, "时间");
            array_push($array1, "注册人数");

            $registerarr = $statisticssregister->statistics_register($l->storeid, $starttimes, $endtimes, $types);

            array_push($timearray, "总量"); //订单总量
            array_push($array1, $registerarr["register_total"] . "人");

            while ($starttimes <= $dendtime) {

                $registerarrx = $statisticssregister->statistics_register($l->storeid, $starttimes, $starttimes, $types);
                array_push($timearray, $starttimes); //时间
                array_push($array1, $registerarrx["register_total"] . "人");

                $starttimes = date('Y-m-d', strtotime('+1 day', strtotime($starttimes)));
            }
            array_push($registerarray, $titles, $timearray, $array1);
        }

        echo iconv('utf-8', 'gbk', implode("\t", $title)), "\n";
        foreach ($registerarray as $value) {
            echo iconv('utf-8', 'gbk', implode("\t", $value)), "\n";
        }
    }

    /*     * *************************注册人数统计***************************** */
    /*     * *************************文件上传量统计*************************** */

    public function actionsearch_statisticsfile() {

        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $types = $_POST["types"];

        $statisticsfile = new statisticsfile();
        $filerarr_month = $statisticsfile->statisticsfile_day($storeid, $starttime, $endtime, $types);
        $json_string = json_encode($filerarr_month);
        echo $json_string;
    }

    public function actiondownloadfileexcel($storeid, $dstarttime, $dendtime) {
        $store_model = store::model();
        header('Content-Type: application/vnd.ms-excel');
        if ($storeid != 0) {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '文件上传统计.xls');
        } else {
            header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '文件上传统计.xls');
        }
        header('Pragma: no-cache');
        header('Expires: 0');
        if ($storeid != 0) {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 文件上传统计');
        } else {
            $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 文件上传统计');
        }
        $statisticsfile = new statisticsfile();

        $starttime = $dstarttime;
        $endtimes = $dendtime;
        $types = "day";

        $timearray = array(); //第一行时间
        $array1 = array(); //总
        $array3 = array(); //pr
        $array4 = array(); //web
        $array5 = array(); //app
        $array6 = array(); //微信



        array_push($timearray, "时间");
        array_push($array1, "总上传量");
        array_push($array3, "终端上传量");
        array_push($array4, "web上传量");
        array_push($array5, "app上传量");
        array_push($array6, "微信上传量");


        $filerarr = $statisticsfile->statisticsfile_day($storeid, $starttime, $endtimes, $types);

        array_push($timearray, "总量"); //订单总量
        array_push($array1, $filerarr["totalCount_dayssAll"]);
        array_push($array3, $filerarr["totalCount_dayss_PR"]);
        array_push($array4, $filerarr["totalCount_dayss_PC"]);
        array_push($array5, $filerarr["totalCount_dayss_APP"]);
        array_push($array6, $filerarr["totalCount_dayss_WX"]);

        while ($starttime <= $dendtime) {

            $filerarrx = $statisticsfile->statisticsfile_day($storeid, $starttime, $starttime, $types);
            array_push($timearray, $starttime); //时间
            array_push($array1, $filerarrx["totalCount_days_total"]);
            array_push($array3, $filerarrx["totalCount_days_PR"]);
            array_push($array4, $filerarrx["totalCount_days_PC"]);
            array_push($array5, $filerarrx["totalCount_days_APP"]);
            array_push($array6, $filerarrx["totalCount_days_WX"]);


            $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
        }
        $filearray = array(
            $timearray,
            $array1,
            $array3,
            $array4,
            $array5,
            $array6
        );

        if ($storeid == 0) {
            $store_infos = $store_model->findAll(array("order" => "storeid asc"));
        } else {
            $store_infos = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }
        foreach ($store_infos as $k => $l) {

            $titles = array($l->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 文件上传统计');

            $statisticsfile = new statisticsfile();

            $starttimes = $dstarttime;
            $endtimes = $dendtime;
            $types = "day";

            $timearray = array(); //第一行时间
            $array1 = array(); //总
            $array3 = array(); //pr
            $array4 = array(); //web
            $array5 = array(); //app
            $array6 = array(); //微信



            array_push($timearray, "时间");
            array_push($array1, "总上传量");
            array_push($array3, "终端上传量");
            array_push($array4, "web上传量");
            array_push($array5, "app上传量");
            array_push($array6, "微信上传量");


            $filerarr = $statisticsfile->statisticsfile_day($l->storeid, $starttimes, $endtimes, $types);

            array_push($timearray, "总量"); //订单总量
            array_push($array1, $filerarr["totalCount_dayssAll"]);
            array_push($array3, $filerarr["totalCount_dayss_PR"]);
            array_push($array4, $filerarr["totalCount_dayss_PC"]);
            array_push($array5, $filerarr["totalCount_dayss_APP"]);
            array_push($array6, $filerarr["totalCount_dayss_WX"]);

            while ($starttimes <= $dendtime) {

                $filerarrx = $statisticsfile->statisticsfile_day($l->storeid, $starttimes, $starttimes, $types);
                array_push($timearray, $starttimes); //时间
                array_push($array1, $filerarrx["totalCount_days_total"]);
                array_push($array3, $filerarrx["totalCount_days_PR"]);
                array_push($array4, $filerarrx["totalCount_days_PC"]);
                array_push($array5, $filerarrx["totalCount_days_APP"]);
                array_push($array6, $filerarrx["totalCount_days_WX"]);

                $starttimes = date('Y-m-d', strtotime('+1 day', strtotime($starttimes)));
            }
            array_push($filearray, $titles, $timearray, $array1, $array3, $array4, $array5, $array6);
        }


        echo iconv('utf-8', 'gbk', implode("\t", $title)), "\n";
        foreach ($filearray as $value) {
            echo iconv('utf-8', 'gbk', implode("\t", $value)), "\n";
        }
    }

    /*     * *************************文件上传量统计*************************** */
    /*     * *************************打印纸张统计***************************** */

//查询今天用纸量
    public function actionsearch_statisticprintpaper() {
        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $statisticsprintpaper = new statisticsprintpaper();
        $printpaperarr = $statisticsprintpaper->statistics_printpaper($storeid, $starttime, $endtime);
        $json_string = json_encode($printpaperarr);
        echo $json_string;
    }

    //查询今年打印纸张量
    public function actionsearch_statisticprintpapers() {

        $storeid = $_POST["storeid"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $types = $_POST["types"];

        $statisticsprintpaper = new statisticsprintpaper();
        $printpaperarr_month = $statisticsprintpaper->statistics_printpaper_total($storeid, $starttime, $endtime, $types);
        $json_string = json_encode($printpaperarr_month);
        echo $json_string;
    }

//单个终端查看详情
    public function actionstatisticPapersone($machineId) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset(Yii::app()->session['adminuser'])) {
            $monthArray = array();
            $dayArray = array();

            $subbusiness_model = subbusiness::model();
            $prbusiness_model = prbusiness::model();
            $printor_model = printor::model();
            $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));

//按月统计
            $year = date("Y"); //2015-11    2015-10-26 10:32:09
            $monthtotalPages = 0;

            for ($i = 1; $i <= 12; $i++) {
                if ($i != 12) {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                } else {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . $i . "-31 23:59:59";
                }
                $totalPage = 0;

                $subbusiness_info = $subbusiness_model->findAllBySql("select printSet,printNumbers from tbl_subbusiness where status = 1 and marchineId = :machineId and printTime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));
                $prbusiness_info = $prbusiness_model->findAllBySql("select * from tbl_prbusiness where exit_phrase >= 5 and machineId = :machineId and printset is not null and printset != '' and starttime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));

                foreach ($subbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printSet);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $totalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $totalPage +=(int) $filePa * $w->printNumbers;
                        }
                    }
                }

                foreach ($prbusiness_info as $q => $w) {
                    if ($w->exit_phrase == 6) {
                        $filePage = explode(',', $w->printset);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $totalPagex = 0;
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPagex +=(int) $filePa * $w->copies;
                                if ($w->is_duplex_page == 0) {//单面
                                    $totalPage += $totalPagex;
                                } else {
                                    $totalPage += ceil($totalPagex / 2);
                                }
                            }
                        }
                    } else {
                        if ($w->is_duplex_page == 0) {//单面
                            if ($w->printedpages != "") {
                                $totalPage +=$w->printedpages;
                            } else {
                                $totalPagex = 0;
                                $filePage = explode(',', $w->printset);
                                foreach ($filePage as $z => $x) {
                                    if (is_numeric($x)) {
                                        $totalPage += 1;
                                    } else {
                                        $filePag = explode('-', $x);
                                        $filePa = $filePag[1] - $filePag[0] + 1;
                                        $totalPagex +=(int) $filePa * $w->copies;
                                        $totalPage += $totalPagex;
                                    }
                                }
                            }
                        } else {
                            if ($w->printedpages != "") {
                                $totalPage +=ceil($w->printedpages / 2);
                            } else {
                                $totalPagex = 0;
                                $filePage = explode(',', $w->printset);
                                foreach ($filePage as $z => $x) {
                                    if (is_numeric($x)) {
                                        $totalPage += 1;
                                    } else {
                                        $filePag = explode('-', $x);
                                        $filePa = $filePag[1] - $filePag[0] + 1;
                                        $totalPagex +=(int) $filePa * $w->copies;
                                        $totalPage += ceil($totalPagex / 2);
                                    }
                                }
                            }
                        }
                    }
                }
                $monthtotalPages += $totalPage;
                array_push($monthArray, array('totalPage' => $totalPage));
            }

//按日统计
            $day = date("Y-m"); //2015-11
            $fate = date("t"); //本月有多少天
            $month = date("m"); //11月
            $daytotalPages = 0;

            for ($i = 1; $i <= $fate; $i++) {

                $stime = $day . "-" . $i . " 00:00:00";
                $etime = $day . "-" . ($i + 1) . " 00:00:00";

                $daytotalPage = 0;
                $subbusiness_info = $subbusiness_model->findAllBySql("select printSet,printNumbers from tbl_subbusiness where status = 1 and marchineId = :machineId and printTime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));
                $prbusiness_info = $prbusiness_model->findAllBySql("select * from tbl_prbusiness where exit_phrase >= 5 and machineId = :machineId and printset is not null and printset != '' and starttime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));

                foreach ($subbusiness_info as $q => $w) {
                    $filePage = explode(',', $w->printSet);
                    foreach ($filePage as $z => $x) {
                        if (is_numeric($x)) {
                            $daytotalPage += 1;
                        } else {
                            $filePag = explode('-', $x);
                            $filePa = $filePag[1] - $filePag[0] + 1;
                            $daytotalPage +=(int) $filePa * $w->printNumbers;
                        }
                    }
                }

                foreach ($prbusiness_info as $q => $w) {
                    if ($w->exit_phrase == 6) {
                        $filePage = explode(',', $w->printset);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $daytotalPage += 1;
                            } else {
                                $totalPagex = 0;
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPagex +=(int) $filePa * $w->copies;
                                if ($w->is_duplex_page == 0) {//单面
                                    $daytotalPage += $totalPagex;
                                } else {
                                    $daytotalPage += ceil($totalPagex / 2);
                                }
                            }
                        }
                    } else {
                        if ($w->is_duplex_page == 0) {//单面
                            if ($w->printedpages != "") {
                                $totalPage +=$w->printedpages;
                            } else {
                                $totalPagex = 0;
                                $filePage = explode(',', $w->printset);
                                foreach ($filePage as $z => $x) {
                                    if (is_numeric($x)) {
                                        $daytotalPage += 1;
                                    } else {
                                        $filePag = explode('-', $x);
                                        $filePa = $filePag[1] - $filePag[0] + 1;
                                        $totalPagex +=(int) $filePa * $w->copies;
                                        $daytotalPage += $totalPagex;
                                    }
                                }
                            }
                        } else {
                            if ($w->printedpages != "") {
                                $totalPage +=ceil($w->printedpages / 2);
                            } else {
                                $totalPagex = 0;
                                $filePage = explode(',', $w->printset);
                                foreach ($filePage as $z => $x) {
                                    if (is_numeric($x)) {
                                        $daytotalPage += 1;
                                    } else {
                                        $filePag = explode('-', $x);
                                        $filePa = $filePag[1] - $filePag[0] + 1;
                                        $totalPagex +=(int) $filePa * $w->copies;
                                        $daytotalPage += ceil($totalPagex / 2);
                                    }
                                }
                            }
                        }
                    }
                }
                $daytotalPages += $daytotalPage;
                array_push($dayArray, array('totalPage' => $daytotalPage, 'day' => $i));
            }

            $this->renderPartial('statisticPapersone', array('monthArray' => $monthArray, 'schoolname' => $printor_info->printorName, "year" => $year, "dayArray" => $dayArray, "machineId" => $machineId, 'leftContent' => $leftContent, 'recommend' => $recommend, "monthtotalPages" => $monthtotalPages, "daytotalPages" => $daytotalPages));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//按时间段查询
    public function actionsearch_terninalStatisticsone() {

        $machineId = $_POST["machineId"];
        $starttime = $_POST["starttime"];
        $endtime = $_POST["endtime"];
        $time = $_POST["time"];
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $totalPages = 0;
//按月统计
        if ($time == "month") {
            $monthArray = array();
            $smonth = date("m", strtotime($starttime));
            $syear = date("Y", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $eyear = date("Y", strtotime($endtime));

            if ($smonth <= $emonth)
                $day = $emonth - $smonth;
            if ($smonth > $emonth)
                $day = 12 - $smonth + $emonth;
            if ($syear == $eyear) {
                for ($smonth; $smonth <= $emonth; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalPage = 0;
                    $subbusiness_info = $subbusiness_model->findAllBySql("select printSet,printNumbers from tbl_subbusiness where status = 1 and marchineId = :machineId and printTime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));
                    $prbusiness_info = $prbusiness_model->findAllBySql("select * from tbl_prbusiness where exit_phrase >= 5 and machineId = :machineId and printset is not null and printset != '' and starttime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));

                    foreach ($subbusiness_info as $q => $w) {
                        $filePage = explode(',', $w->printSet);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPage +=(int) $filePa * $w->printNumbers;
                            }
                        }
                    }

                    foreach ($prbusiness_info as $q => $w) {
                        if ($w->exit_phrase == 6) {
                            $filePage = explode(',', $w->printset);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $totalPagex = 0;
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPagex +=(int) $filePa * $w->copies;
                                    if ($w->is_duplex_page == 0) {//单面
                                        $totalPage += $totalPagex;
                                    } else {
                                        $totalPage += ceil($totalPagex / 2);
                                    }
                                }
                            }
                        } else {
                            if ($w->is_duplex_page == 0) {//单面
                                if ($w->printedpages != "") {
                                    $totalPage +=$w->printedpages;
                                } else {
                                    $totalPagex = 0;
                                    $filePage = explode(',', $w->printset);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w->copies;
                                            $totalPage += $totalPagex;
                                        }
                                    }
                                }
                            } else {
                                if ($w->printedpages != "") {
                                    $totalPage +=ceil($w->printedpages / 2);
                                } else {
                                    $totalPagex = 0;
                                    $filePage = explode(',', $w->printset);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w->copies;
                                            $totalPage += ceil($totalPagex / 2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $totalPages += $totalPage;
                    array_push($monthArray, array('totalPage' => $totalPage, 'num' => $smonth . "月"));
                }
            } else if ($syear < $eyear) {
                for ($smonth; $smonth <= 12; $smonth++) {
                    if ($smonth != 12) {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . ($smonth + 1) . "-1 00:00:00";
                    } else {
                        $stime = $syear . "-" . $smonth . "-1 00:00:00";
                        $etime = $syear . "-" . $smonth . "-31 23:59:59";
                    }
                    $totalPage = 0;
                    $subbusiness_info = $subbusiness_model->findAllBySql("select printSet,printNumbers from tbl_subbusiness where status = 1 and marchineId = :machineId and printTime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));
                    $prbusiness_info = $prbusiness_model->findAllBySql("select * from tbl_prbusiness where exit_phrase >= 5 and machineId = :machineId and printset is not null and printset != '' and starttime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));

                    foreach ($subbusiness_info as $q => $w) {
                        $filePage = explode(',', $w->printSet);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPage +=(int) $filePa * $w->printNumbers;
                            }
                        }
                    }

                    foreach ($prbusiness_info as $q => $w) {
                        if ($w->exit_phrase == 6) {
                            $filePage = explode(',', $w->printset);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $totalPagex = 0;
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPagex +=(int) $filePa * $w->copies;
                                    if ($w->is_duplex_page == 0) {//单面
                                        $totalPage += $totalPagex;
                                    } else {
                                        $totalPage += ceil($totalPagex / 2);
                                    }
                                }
                            }
                        } else {
                            if ($w->is_duplex_page == 0) {//单面
                                if ($w->printedpages != "") {
                                    $totalPage +=$w->printedpages;
                                } else {
                                    $totalPagex = 0;
                                    $filePage = explode(',', $w->printset);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w->copies;
                                            $totalPage += $totalPagex;
                                        }
                                    }
                                }
                            } else {
                                if ($w->printedpages != "") {
                                    $totalPage +=ceil($w->printedpages / 2);
                                } else {
                                    $totalPagex = 0;
                                    $filePage = explode(',', $w->printset);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w->copies;
                                            $totalPage += ceil($totalPagex / 2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $totalPages += $totalPage;
                    array_push($monthArray, array('totalPage' => $totalPage, 'num' => $smonth . "月"));
                }
                $syear = $syear + 1;
                if ($syear == $eyear) {
                    for ($p = 1; $p <= $emonth; $p++) {
                        if ($p != 12) {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . ($p + 1) . "-1 00:00:00";
                        } else {
                            $stime = $eyear . "-" . $p . "-1 00:00:00";
                            $etime = $eyear . "-" . $p . "-31 23:59:59";
                        }
                        $totalPage = 0;
                        $subbusiness_info = $subbusiness_model->findAllBySql("select printSet,printNumbers from tbl_subbusiness where status = 1 and marchineId = :machineId and printTime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));
                        $prbusiness_info = $prbusiness_model->findAllBySql("select * from tbl_prbusiness where exit_phrase >= 5 and machineId = :machineId and printset is not null and printset != '' and starttime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));

                        foreach ($subbusiness_info as $q => $w) {
                            $filePage = explode(',', $w->printSet);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPage +=(int) $filePa * $w->printNumbers;
                                }
                            }
                        }

                        foreach ($prbusiness_info as $q => $w) {
                            if ($w->exit_phrase == 6) {
                                $filePage = explode(',', $w->printset);
                                foreach ($filePage as $z => $x) {
                                    if (is_numeric($x)) {
                                        $totalPage += 1;
                                    } else {
                                        $totalPagex = 0;
                                        $filePag = explode('-', $x);
                                        $filePa = $filePag[1] - $filePag[0] + 1;
                                        $totalPagex +=(int) $filePa * $w->copies;
                                        if ($w->is_duplex_page == 0) {//单面
                                            $totalPage += $totalPagex;
                                        } else {
                                            $totalPage += ceil($totalPagex / 2);
                                        }
                                    }
                                }
                            } else {
                                if ($w->is_duplex_page == 0) {//单面
                                    if ($w->printedpages != "") {
                                        $totalPage +=$w->printedpages;
                                    } else {
                                        $totalPagex = 0;
                                        $filePage = explode(',', $w->printset);
                                        foreach ($filePage as $z => $x) {
                                            if (is_numeric($x)) {
                                                $totalPage += 1;
                                            } else {
                                                $filePag = explode('-', $x);
                                                $filePa = $filePag[1] - $filePag[0] + 1;
                                                $totalPagex +=(int) $filePa * $w->copies;
                                                $totalPage += $totalPagex;
                                            }
                                        }
                                    }
                                } else {
                                    if ($w->printedpages != "") {
                                        $totalPage +=ceil($w->printedpages / 2);
                                    } else {
                                        $totalPagex = 0;
                                        $filePage = explode(',', $w->printset);
                                        foreach ($filePage as $z => $x) {
                                            if (is_numeric($x)) {
                                                $totalPage += 1;
                                            } else {
                                                $filePag = explode('-', $x);
                                                $filePa = $filePag[1] - $filePag[0] + 1;
                                                $totalPagex +=(int) $filePa * $w->copies;
                                                $totalPage += ceil($totalPagex / 2);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $totalPages += $totalPage;
                        array_push($monthArray, array('totalPage' => $totalPage, 'num' => $p . "月"));
                    }
                } else if ($syear < $eyear) {
                    $totalPages = 0;
                    array_push($monthArray, array('totalPage' => 0, 'num' => ""));
                }
            }
            array_push($monthArray, $totalPages);
            $json_string = json_encode($monthArray);
            echo $json_string;
        }
//按天统计
        if ($time == "day") {
            $dayArray = array();
            $year = date("Y", strtotime($starttime));
            $smonth = date("m", strtotime($starttime));
            $emonth = date("m", strtotime($endtime));
            $sday = date("d", strtotime($starttime));
            $eday = date("d", strtotime($endtime));



//按日统计
//同一个月
            if ($smonth == $emonth) {
                for ($i = $sday; $i <= $eday; $i++) {
                    $stime = $year . "-" . $smonth . "-" . $i . " 00:00:00";
                    $etime = $year . "-" . $smonth . "-" . ($i + 1) . " 00:00:00";
                    $daytotalPage = 0;
                    $subbusiness_info = $subbusiness_model->findAllBySql("select printSet,printNumbers from tbl_subbusiness where status = 1 and marchineId = :machineId and printTime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));
                    $prbusiness_info = $prbusiness_model->findAllBySql("select * from tbl_prbusiness where exit_phrase >= 5 and machineId = :machineId and printset is not null and printset != '' and starttime between :stime and :etime", array(":machineId" => $machineId, ":stime" => $stime, ":etime" => $etime));

                    foreach ($subbusiness_info as $q => $w) {
                        $filePage = explode(',', $w->printSet);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $daytotalPage += 1;
                            } else {
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $daytotalPage +=(int) $filePa * $w->printNumbers;
                            }
                        }
                    }

                    foreach ($prbusiness_info as $q => $w) {
                        if ($w->exit_phrase == 6) {
                            $filePage = explode(',', $w->printset);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $daytotalPage += 1;
                                } else {
                                    $totalPagex = 0;
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPagex +=(int) $filePa * $w->copies;
                                    if ($w->is_duplex_page == 0) {//单面
                                        $daytotalPage += $totalPagex;
                                    } else {
                                        $daytotalPage += ceil($totalPagex / 2);
                                    }
                                }
                            }
                        } else {
                            if ($w->is_duplex_page == 0) {//单面
                                if ($w->printedpages != "") {
                                    $daytotalPage +=$w->printedpages;
                                } else {
                                    $totalPagex = 0;
                                    $filePage = explode(',', $w->printset);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $daytotalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w->copies;
                                            $daytotalPage += $totalPagex;
                                        }
                                    }
                                }
                            } else {
                                if ($w->printedpages != "") {
                                    $daytotalPage +=ceil($w->printedpages / 2);
                                } else {
                                    $totalPagex = 0;
                                    $filePage = explode(',', $w->printset);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $daytotalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w->copies;
                                            $daytotalPage += ceil($totalPagex / 2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $totalPages += $daytotalPage;
                    array_push($dayArray, array('totalPage' => $daytotalPage, 'num' => (int) $i));
                }
            }//不是同一个月 
            else if ($smonth + 1 == $emonth) {
                $totalPages += 0;
                array_push($dayArray, array('totalPage' => 0, 'num' => ""));
            }
            array_push($dayArray, $totalPages);
            $json_string = json_encode($dayArray);
            echo $json_string;
        }
    }

//学校下载打印纸张EXCEL  里面分为此学校每个终端的
    public function actiondownloadprintpaperexcel($storeid, $dstarttime, $dendtime) {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=从' . $dstarttime . '到' . $dendtime . '打印纸张统计.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        $title = array('从 ' . $dstarttime . ' 到 ' . $dendtime . ' 打印纸张统计 单位（张)');

        $statisticsprintpaper = new statisticsprintpaper();

        $starttime = $dstarttime;
        $endtimes = $dendtime;

        $array1 = array();
        $printor_model = printor::model();
        $printor_info = $printor_model->findAllBySql("select * from tbl_printor where _storeId in (" . $storeid . ")");
        $timearray = array(); //第一行时间
        $totalarray = array(); //打印纸张总量
        array_push($timearray, "时间");
        array_push($totalarray, "总量"); //打印纸张总量

        if (count($printor_info) != 0) {
            foreach ($printor_info as $l => $y) {
                $machineId = $y->machineId;
                $machinenName = $y->printorName;
                $list = 'list_' . $machineId;
                $$list = array();
                array_push($$list, $machinenName);
                array_push($array1, $$list);
            }

            while ($starttime <= $endtimes) {
                $printpaperarrx = $statisticsprintpaper->statistics_printpaper_day_total($storeid, $starttime, $starttime);
                array_push($timearray, $starttime); //时间
                array_push($totalarray, $printpaperarrx["totalPages"]);
                for ($j = 0; $j < count($array1); $j ++) {
                    array_push($array1[$j], $printpaperarrx["dayArray_pages"][0][$j]["totalPage"]);
                }
                $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
            }

            $printpaperarray = array(
                $timearray,
                $totalarray,
                $array1
            );

            echo iconv('utf-8', 'gbk', implode("\t", $title)), "\n";
            foreach ($printpaperarray as $key => $value) {
                if ($key != 2) {
                    echo iconv('utf-8', 'gbk', implode("\t", $value)), "\n";
                } else {
                    foreach ($value as $v) {
                        echo iconv('utf-8', 'gbk', implode("\t", $v)), "\n";
                    }
                }
            }
        }
    }

    /*     * *************************打印纸张统计***************************** */
    /*     * *************************统计信息start***************************** */

    public function actionexcelToEmail($storeid, $dstarttime, $dendtime) {
        header("Content-type:text/html;charset=utf-8");
        $store_model = store::model();
        $print_model = printor::model();
        $store_info = $store_model->findByPk($storeid);
        $print_info = $print_model->findAll(array('condition' => "_storeId = " . $storeid));

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename=' . $store_info->storename . '从' . $starttime . '到' . $dendtime . '统计.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        $oderTitle = array($store_info->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 订单统计');
        $incomeTitle = array($store_info->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 收入统计');
        $refundTitle = array($store_info->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 退款统计');
        $registerTitle = array($store_info->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 注册人数统计');
        $printPaperTitle = array($store_info->storename . '从 ' . $dstarttime . ' 到 ' . $dendtime . ' 打印纸张统计');

        $statisticssorder = new statisticssorder();
        $statisticssincome = new statisticsincome();
        $statisticssrefund = new statisticsrefund();
        $statisticsregister = new statisticsregister();
        $statisticsprintpaper = new statisticsprintpaper();

        $starttime = $dstarttime;
        $endtimes = $dendtime;
        $types = "day";
        $orderarray = array();
        $incomearray = array();
        $refundarray = array();
        $registerarray = array();
        $printpaperarray = array();
//订单数组数据
        $odertimearray = array(); //第一行时间
        $dayArray_total = array(); //总订单量
        $dayArray = array(); //web订单量
        $dayArray_pro = array(); //终端订单量
        $dayArray_PC = array(); //webPC订单量
        $dayArray_APP = array(); //webAPP订单量
        $dayArray_WX = array(); //webWX订单量
        $dayArray_refund_total = array(); //总订单退款量
        $dayArray_refund_web = array(); //web订单退款量
        $dayArray_refund_terminal = array(); //终端订单退款量
        $dayArray_refundrate_total = array(); //订单总退款率
        $dayArray_refundrate_web = array(); //web订单退款率
        $dayArray_refundrate_terminal = array(); //终端订单退款率
        array_push($odertimearray, "时间");
        array_push($dayArray_total, "订单总量");
        array_push($dayArray, "WEB订单量");
        array_push($dayArray_pro, "终端订单量");
        array_push($dayArray_PC, "PC端订单量");
        array_push($dayArray_APP, "APP端订单量");
        array_push($dayArray_WX, "微信端订单量");
        array_push($dayArray_refund_total, "退款订单总量");
        array_push($dayArray_refund_web, "WEB端退款订单量");
        array_push($dayArray_refund_terminal, "终端订单退款量");
        array_push($dayArray_refundrate_total, "退款订单率");
        array_push($dayArray_refundrate_web, "WEB端退款率");
        array_push($dayArray_refundrate_terminal, "终端退款率");
//收入数组数据
        $incometimearray = array(); //第一行时间
        $array1 = array(); //不除去退款总收入
        $array2 = array(); //除去退款总收入
        $array3 = array(); //不除去退款积分总收入
        $array4 = array(); //除去退款积分总收入
        $array5 = array(); //不除去退款金额总收入
        $array6 = array(); //除去退款金额总收入
        $array7 = array(); //积分退款率
        $array8 = array(); //金额退款率
        $array9 = array(); //收入退款率
        $array10 = array(); //除去退款支付宝总收入
        $array11 = array(); //不除去退款支付宝总收入
        $array12 = array(); //除去退款微信总收入
        $array13 = array(); //不除去退款微信总收入
        array_push($incometimearray, "时间");
        array_push($array1, "不除去退款总收入");
        array_push($array2, "除去退款总收入");
        array_push($array3, "不除去退款积分总收入");
        array_push($array4, "除去退款积分总收入");
        array_push($array5, "不除去退款金额总收入");
        array_push($array6, "除去退款金额总收入");
        array_push($array7, "积分退款率");
        array_push($array8, "金额退款率");
        array_push($array9, "收入退款率");
        array_push($array10, "除去退款支付宝总收入");
        array_push($array11, "不除去退款支付宝总收入");
        array_push($array12, "除去退款微信总收入");
        array_push($array13, "不除去退款微信总收入");
//退款数组数据
        $refundtimearray = array(); //第一行时间
        $array111 = array(); //总退款金额
        $array211 = array(); //退款积分
        $array311 = array(); //退款金额
        array_push($refundtimearray, "时间");
        array_push($array111, "总退款金额");
        array_push($array211, "退款积分");
        array_push($array311, "退款金额");
//注册人数数组数据
        $registertimearray = array(); //第一行时间
        $arrayrg = array(); //总退款金额
        array_push($registertimearray, "时间");
        array_push($arrayrg, "注册人数");
//打印纸张数组数据
        array_push($refundtimearray, "时间");
        $arraypp = array();
        $printpapertimearray = array();
        $totalarray = array(); //第二行时间
        array_push($printpapertimearray, "时间");
        array_push($totalarray, "总量"); //订单总量
        $i = 0;
        if (count($print_info) != 0) {
            foreach ($print_info as $l => $y) {
                $i ++;
                $machineId = $y->machineId;
                $machinenName = $y->printorName;
                $list = 'list_' . $machineId;
                $$list = array();
                array_push($$list, $machinenName);
                array_push($arraypp, $$list);
            }
        }
//获得各个情况各个时间的数据
        $orderarr = $statisticssorder->statsticssorder_day($storeid, $starttime, $endtimes, $types);
        $incomearr = $statisticssorder->statsticssorder_day($storeid, $starttime, $endtimes, $types);
        $refundarr = $statisticssrefund->statistics_refund($storeid, $starttime, $endtimes, $types);
        $registerarray = $statisticssregister->statistics_register($storeid, $starttime, $endtimes, $types);
//第二列数据_订单
        array_push($timearray, "总量"); //订单总量
        array_push($dayArray_total, $orderarr["totalCount_dattotals"] . "单"); //订单总量
        array_push($dayArray, $orderarr["totalCount_dayss"] . "单"); //WEB订单量
        array_push($dayArray_pro, $orderarr["totalCount_dayspros"] . "单"); //终端订单量

        array_push($dayArray_PC, $orderarr["totalCount_dayss_PC"] . "单"); //PC端订单量
        array_push($dayArray_APP, $orderarr["totalCount_dayss_APP"] . "单"); //APP端订单量
        array_push($dayArray_WX, $orderarr["totalCount_dayss_WX"] . "单"); //微信端订单量

        array_push($dayArray_refund_total, $orderarr["totalCount_days_refund"] . "单"); //退款订单总量
        array_push($dayArray_refund_web, $orderarr["totalCount_days_refund_web"] . "单"); //WEB端退款订单量
        array_push($dayArray_refund_terminal, $orderarr["totalCount_days_refund_terminal"] . "单"); //终端退款订单量
        if ($orderarr["totalCount_dattotals"] != 0) {
            array_push($dayArray_refundrate_total, sprintf("%.2f", $orderarr["totalCount_days_refund"] / $orderarr["totalCount_dattotals"] * 100) . "%"); //退款订单率
        } else {
            array_push($dayArray_refundrate_total, "0%"); //退款订单率
        }
        if ($orderarr["totalCount_dayss"] != 0) {
            array_push($dayArray_refundrate_web, sprintf("%.2f", $orderarr["totalCount_days_refund_web"] / $orderarr["totalCount_dayss"] * 100) . "%"); //WEB端退款率
        } else {
            array_push($dayArray_refundrate_web, "0%"); //退款订单率
        }
        if ($orderarr["totalCount_dayspros"] != 0) {
            array_push($dayArray_refundrate_terminal, sprintf("%.2f", $orderarr["totalCount_days_refund_terminal"] / $orderarr["totalCount_dayspros"] * 100) . "%"); //终端退款率
        } else {
            array_push($dayArray_refundrate_terminal, "0%"); //退款订单率
        }
//第二列数据_收入
        array_push($timearray, "总量"); //订单总量
        array_push($array1, $incomearr["totalIncome_dayss"] . "元");
        array_push($array2, $incomearr["totalIncome_removerefund_dayss"] . "元");
        array_push($array3, $incomearr["total_integrals"] . "分");
        array_push($array4, $incomearr["totalIncome_removerefund_integrals"] . "分");
        array_push($array5, $incomearr["total_moneys"] . "元");
        array_push($array6, $incomearr["total_removerefund_money"] . "元");
        array_push($array7, $incomearr["total_refundrate_integral"] . "%");
        array_push($array8, $incomearr["total_refundrate_money"] . "%");
        array_push($array9, $incomearr["total_refundrate"] . "%");
        array_push($array10, $incomearr["total_removerefund_money_zfbs"] . "元");
        array_push($array11, $incomearr["total_money_zfbs"] . "元");
        array_push($array12, $incomearr["total_removerefund_money_wxs"] . "元");
        array_push($array13, $incomearr["total_money_wxs"] . "元");
//第二列数据_退款
        array_push($timearray, "总量"); //订单总量
        array_push($array1, $refundarr["refund_totals"] . "元");
        array_push($array2, $refundarr["refund_integral_totals"] . "分");
        array_push($array3, $refundarr["refund_money_totals"] . "元");
//第二列数据_注册人数
        array_push($timearray, "总量"); //订单总量
        array_push($array1, $registerarray["register_total"] . "人");

//按日期循环得到各列的数据
        while ($starttime <= $dendtime) {
//订单列表
            $orderarrx = $statisticssorder->statsticssorder_day($storeid, $starttime, $starttime, $types);
            array_push($timearray, $starttime); //时间
            array_push($dayArray_total, $orderarrx["totalCount_dattotals"] . "单"); //订单总量
            array_push($dayArray, $orderarrx["totalCount_dayss"] . "单"); //WEB订单量
            array_push($dayArray_pro, $orderarrx["totalCount_dayspros"] . "单"); //终端订单量
            array_push($dayArray_PC, $orderarrx["totalCount_dayss_PC"] . "单"); //PC端订单量
            array_push($dayArray_APP, $orderarrx["totalCount_dayss_APP"] . "单"); //APP端订单量
            array_push($dayArray_WX, $orderarrx["totalCount_dayss_WX"] . "单"); //微信端订单量
            array_push($dayArray_refund_total, $orderarrx["totalCount_days_refund"] . "单"); //退款订单总量
            array_push($dayArray_refund_web, $orderarrx["totalCount_days_refund_web"] . "单"); //WEB端退款订单量
            array_push($dayArray_refund_terminal, $orderarrx["totalCount_days_refund_terminal"] . "单"); //终端退款订单量
            if ($orderarrx["totalCount_dattotals"] != 0) {
                array_push($dayArray_refundrate_total, sprintf("%.2f", $orderarrx["totalCount_days_refund"] / $orderarrx["totalCount_dattotals"] * 100) . "%"); //退款订单率
            } else {
                array_push($dayArray_refundrate_total, "0%"); //退款订单率
            }
            if ($orderarrx["totalCount_dayss"] != 0) {
                array_push($dayArray_refundrate_web, sprintf("%.2f", $orderarrx["totalCount_days_refund_web"] / $orderarrx["totalCount_dayss"] * 100) . "%"); //WEB端退款率
            } else {
                array_push($dayArray_refundrate_web, "0%"); //退款订单率
            }
            if ($orderarrx["totalCount_dayspros"] != 0) {
                array_push($dayArray_refundrate_terminal, sprintf("%.2f", $orderarrx["totalCount_days_refund_terminal"] / $orderarrx["totalCount_dayspros"] * 100) . "%"); //终端退款率
            } else {
                array_push($dayArray_refundrate_terminal, "0%"); //退款订单率
            }
            $orderarray = array(
                $timearray,
                $dayArray_total,
                $dayArray,
                $dayArray_pro,
                $dayArray_PC,
                $dayArray_APP,
                $dayArray_WX,
                $dayArray_refund_total,
                $dayArray_refund_web,
                $dayArray_refund_terminal,
                $dayArray_refundrate_total,
                $dayArray_refundrate_web,
                $dayArray_refundrate_terminal
            );
            $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
        }


        echo iconv('utf-8', 'gbk', implode("\t", $title)), "\n";
        foreach ($orderarray as $value) {
            echo iconv('utf-8', 'gbk', implode("\t", $value)), "\n";
        }
    }

    public function actionstatisticsinfo() {
        header("Content-type:text/html;charset=utf-8");
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        //获得昨天时间
        if (isset(Yii::app()->session['adminuser'])) {
            $store_model = store::model();
            $store_info = $store_model->findAll();

            $statisticssincome = new statisticsincome();
            $statisticssorder = new statisticssorder();
            $statisticssrefund = new statisticsrefund();
            $statisticsregister = new statisticsregister();

            $yestday = date("Y-m-d", strtotime("-1 day"));
            $dstarttime = $yestday;
            $dendtime = $yestday;
            $income_total_yestday = ""; // 昨日净总收入
            $refund_total_yestday = ""; //昨日退款
            foreach ($store_info as $value) {
                $storeid = $value->storeid;
                $storename = $value->storename;
                $types = "day";
                //收入统计
                $incomearr = $statisticssincome->statistics_income($storeid, $dstarttime, $dendtime, $types);
                $income_data = $incomearr["totalIncome_removerefund_dayss"];
                //$incomearray_yestday -> 各个学校除去退款除去积分收入后的昨日净收入
                $incomearray_yestday[] = array(
                    "storeid" => $storeid,
                    "storename" => $storename,
                    "data" => $income_data,
                );
                $income_total_yestday += $income_data;
                //退款统计
                $refundarr = $statisticssrefund->statistics_refund($storeid, $dstarttime, $dendtime, $types);
                $refund_data = $refundarr["refund_money_totals"];
                $refundarray_yestday[] = array(
                    "storeid" => $storeid,
                    "storename" => $storename,
                    "data" => $refund_data,
                );
                $refund_total_yestday += $refund_data;
            }
//        print_r($refund_total_yestday);
            //订单统计
            $orderarr = $statisticssorder->statsticssorder_day($storeid = 0, $dstarttime, $dendtime, $types);
            $total_order = $orderarr["totalCount_dattotals"]; //总订单量
            $printor_order = $orderarr["totalCount_dayspros"]; //终端订单量
            $web_order = $orderarr["totalCount_dayss"]; //web订单量
            $total_refund_order = $orderarr["totalCount_days_refund"]; //总退款订单
            $printor_refund_order = $orderarr["totalCount_days_refund_terminal"]; //终端退款订单数
            $web_refund_order = $orderarr["totalCount_days_refund_web"]; //web退款订单数
            //退款统计
//            $refundarr = $statisticssrefund->statistics_refund($storeid, $dstarttime, $dendtime, $types);
//            $total_refund = $refundarr["refund_money_totals"];
//        print_r($total_refund . "--");
            //注册人数统计
            $registerarray = $statisticsregister->statistics_register($storeid = 0, $dstarttime, $dendtime, $types);
            $total_register = $registerarray["register_total"];

            //信息整合
            $income_info_yest = "纯收入金额" . $income_total_yestday . "元("; //收入信息
            $order_info_yest = "订单量" . $total_order . "（终端" . $printor_order . "单,web端" . $web_order . "单);"; //订单信息
            $refund_info_yest = "退款金额" . $refund_total_yestday . ",退款订单" . $total_refund_order . "单(终端" . $printor_refund_order . "单,web端" . $web_refund_order . "单)(";
            $register_info_yest = "注册人数" . $total_register . "人";
            for ($i = 0; $i < count($store_info); $i++) {
                $storename = $incomearray_yestday[$i]["storename"];
                $storeoder = $incomearray_yestday[$i]["data"];
                if ($storename == "重庆理工大学-花溪校区") {
                    $storename = "重理工-花溪";
                } else if ($storename == "重庆理工大学-两江校区") {
                    $storename = "重理工-两江";
                }
//                if ($storeoder == "0元,") {
//                    $storename = "";
//                    $storeoder = "";
//                }
                $storename_refund = $refundarray_yestday[$i]["storename"];
                $store_refund = $refundarray_yestday[$i]["data"];
                if ($storename_refund == "重庆理工大学-花溪校区") {
                    $storename_refund = "重理工-花溪";
                } else if ($storename_refund == "重庆理工大学-两江校区") {
                    $storename_refund = "重理工-两江";
                }
//                if ($store_refund == "0元,") {
//                    $storename_refund = "";
//                    $store_refund = "";
//                }
//            array_push($oder_info,"$storename" , $storeoder);
//            array_push($refund_info,"$storename_refund", $store_refund);
                $oder_info[] = array(
                    "$storename" => $storeoder,
                );
                $refund_info[] = array(
                    "$storename_refund" => $store_refund,
                );

//            $income_info_yest .= $storename . $storeoder; //收入信息
//            $refund_info_yest .= $storename_refund . $store_refund; //退款信息
            }
            $dat_order = [];
            $dat_refund = [];
            foreach ($oder_info as $b) {
                $dat_order[key($b)] = $b[key($b)];
            }
            foreach ($refund_info as $b) {
                $dat_refund[key($b)] = $b[key($b)];
            }
            arsort($dat_order);
            arsort($dat_refund);
            foreach ($dat_order as $key => $value) {

                $income_info_yest.=$key . $value . "元"; //收入信息
            }
            foreach ($dat_refund as $k => $v) {
                $refund_info_yest.=$k . $v . "元"; //退款信息
            }
            $yestday_info_statists = $yestday . "后台管理系统显示:" . $income_info_yest . ");" . $order_info_yest . $refund_info_yest . ");" . $register_info_yest;
            if (isset($_POST['email'])) {
                $email = $_POST['email'];
                $text = $_POST["text"];
                $emailArray = explode(";", $email);
                foreach ($emailArray as $value) {
                    $email_uni = $value;
                    //******************** 配置信息 ********************************
                    $smtpserver = "smtp.163.com"; //SMTP服务器
                    $smtpserverport = 25; //SMTP服务器端口
                    $smtpusermail = "13330290051@163.com"; //SMTP服务器的用户邮箱
                    $smtpemailto = $email_uni; //发送给谁
                    $smtpuser = "13330290051@163.com"; //SMTP服务器的用户帐号
                    $smtppass = "guaxixiya"; //SMTP服务器的用户密码
                    $mailtitle = "颇闰科技昨日统计信息"; //邮件主题
                    $mailcontent = "<p>您好：</p> $text <p>重庆颇闰科技有限公司</p></h1>"; //邮件内容
                    $mailtype = "HTML"; //邮件格式（HTML/TXT）,TXT为文本邮件
                    //************************ 配置信息 ****************************
                    $email_class = new email();
                    $smtp = $email_class->smtp($smtpserver, $smtpserverport, true, $smtpuser, $smtppass); //这里面的一个true是表示使用身份验证,否则不使用身份验证.
                    $email_class->debug = false; //是否显示发送的调试信息
                    $state = $email_class->sendmail($smtpemailto, $smtpusermail, $mailtitle, $mailcontent, $mailtype);
                    echo "<div style='width:300px; margin:36px auto;'>";
                    if ($state == "") {
                        echo "<script>parent.$('#add_success').text('邮件发送失败！').css('color','red');</script>";
                        echo "对不起，邮件发送失败！请检查邮箱填写是否有误。";
                    }
                    echo "<script>parent.$('#add_success').text('邮件发送成功！').css('color','red');</script>";
                }
            }
            $this->renderPartial('statisticsinfo', array('yestday_info_statists' => $yestday_info_statists, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actionstatisticsinfo_oneday() {
//        $date = "2016-05-14";
        $date = $_POST["date"];
        $dstarttime = $date;
        $dendtime = $date;
        $store_model = store::model();
        $store_info = $store_model->findAll();

        $statisticssincome = new statisticsincome();
        $statisticssorder = new statisticssorder();
        $statisticssrefund = new statisticsrefund();
        $statisticsregister = new statisticsregister();

        $income_total_yestday = ""; // 昨日净总收入
        $refund_total_yestday = ""; //昨日退款
        foreach ($store_info as $value) {
            $storeid = $value->storeid;
            $storename = $value->storename;
            $types = "day";
            //收入统计
            $incomearr = $statisticssincome->statistics_income($storeid, $dstarttime, $dendtime, $types);
            $income_data = $incomearr["totalIncome_removerefund_dayss"];
            //$incomearray_yestday -> 各个学校除去退款除去积分收入后的昨日净收入
            $incomearray_yestday[] = array(
                "storeid" => $storeid,
                "storename" => $storename,
                "data" => $income_data,
            );
            $income_total_yestday += $income_data;
            //退款统计
            $refundarr = $statisticssrefund->statistics_refund($storeid, $dstarttime, $dendtime, $types);
            $refund_data = $refundarr["refund_money_totals"];
            $refundarray_yestday[] = array(
                "storeid" => $storeid,
                "storename" => $storename,
                "data" => $refund_data,
            );
            $refund_total_yestday += $refund_data;
        }
//        print_r($refund_total_yestday);
        //订单统计
        $orderarr = $statisticssorder->statsticssorder_day($storeid = 0, $dstarttime, $dendtime, $types);
        $total_order = $orderarr["totalCount_dattotals"]; //总订单量
        $printor_order = $orderarr["totalCount_dayspros"]; //终端订单量
        $web_order = $orderarr["totalCount_dayss"]; //web订单量
        $total_refund_order = $orderarr["totalCount_days_refund"]; //总退款订单
        $printor_refund_order = $orderarr["totalCount_days_refund_terminal"]; //终端退款订单数
        $web_refund_order = $orderarr["totalCount_days_refund_web"]; //web退款订单数
        //退款统计
//            $refundarr = $statisticssrefund->statistics_refund($storeid, $dstarttime, $dendtime, $types);
//            $total_refund = $refundarr["refund_money_totals"];
//        print_r($total_refund . "--");
        //注册人数统计
        $registerarray = $statisticsregister->statistics_register($storeid = 0, $dstarttime, $dendtime, $types);
        $total_register = $registerarray["register_total"];

        //信息整合
        $income_info_yest = "纯收入金额" . $income_total_yestday . "元("; //收入信息
        $order_info_yest = "订单量" . $total_order . "（终端" . $printor_order . "单,web端" . $web_order . "单);"; //订单信息
        $refund_info_yest = "退款金额" . $refund_total_yestday . ",退款订单" . $total_refund_order . "单(终端" . $printor_refund_order . "单,web端" . $web_refund_order . "单)(";
        $register_info_yest = "注册人数" . $total_register . "人";

//        $oder_info = array(0=>0);
//        $refund_info = array(0=>0);
        for ($i = 0; $i < count($store_info); $i++) {
            $storename = $incomearray_yestday[$i]["storename"];
            $storeoder = $incomearray_yestday[$i]["data"];
            if ($storename == "重庆理工大学-花溪校区") {
                $storename = "重理工-花溪";
            } else if ($storename == "重庆理工大学-两江校区") {
                $storename = "重理工-两江";
            }
//                if ($storeoder == "0元,") {
//                    $storename = "";
//                    $storeoder = "";
//                }
            $storename_refund = $refundarray_yestday[$i]["storename"];
            $store_refund = $refundarray_yestday[$i]["data"];
            if ($storename_refund == "重庆理工大学-花溪校区") {
                $storename_refund = "重理工-花溪";
            } else if ($storename_refund == "重庆理工大学-两江校区") {
                $storename_refund = "重理工-两江";
            }
//                if ($store_refund == "0元,") {
//                    $storename_refund = "";
//                    $store_refund = "";
//                }
//            array_push($oder_info,"$storename" , $storeoder);
//            array_push($refund_info,"$storename_refund", $store_refund);
            $oder_info[] = array(
                "$storename" => $storeoder,
            );
            $refund_info[] = array(
                "$storename_refund" => $store_refund,
            );

//            $income_info_yest .= $storename . $storeoder; //收入信息
//            $refund_info_yest .= $storename_refund . $store_refund; //退款信息
        }
        $dat_order = [];
        $dat_refund = [];
        foreach ($oder_info as $b) {
            $dat_order[key($b)] = $b[key($b)];
        }
        foreach ($refund_info as $b) {
            $dat_refund[key($b)] = $b[key($b)];
        }
        arsort($dat_order);
        arsort($dat_refund);
        foreach ($dat_order as $key => $value) {

            $income_info_yest.=$key . $value . "元"; //收入信息
        }
        foreach ($dat_refund as $k => $v) {
            $refund_info_yest.=$k . $v . "元"; //退款信息
        }
        $yestday_info_statists = $date . "后台管理系统显示:" . $income_info_yest . ");" . $order_info_yest . $refund_info_yest . ");" . $register_info_yest;

        $data[] = array(
            'code' => 200,
            'msg' => $yestday_info_statists,
        );
        echo $data = json_encode($data);
    }

    /*     * *************************统计信息end***************************** */
}
