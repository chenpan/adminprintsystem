<?php

include 'BaseController.php';

class parttimeController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'partTimeInfo + partTimeInfo', //兼职管理
            'addPlurality + addPlurality',
            'delPlurality + delPlurality',
            'editPlurality + editPlurality',
            'audite + audite', //审核管理
            'reverse + reverse'
        );
    }

    public function filterpartTimeInfo($filterChain) {
        $this->checkAccess("兼职管理", $filterChain);
    }

    public function filteraddPlurality($filterChain) {
        $this->checkAccess("添加兼职人员", $filterChain);
    }

    public function filterdelPlurality($filterChain) {
        $this->checkAccess("删除兼职人员", $filterChain);
    }

    public function filtereditPlurality($filterChain) {
        $this->checkAccess("编辑兼职人员", $filterChain);
    }

    public function filteraudite($filterChain) {
        $this->checkAccess("兼职审核", $filterChain);
    }
    
    public function filterreverse($filterChain) {
        $this->checkAccess("兼职冲账", $filterChain);
    }

    /*     * ************** 兼职人员管理 start ************** */

    public function actionpartTimeInfo() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $plurality_model = plurality::model();
//已经通过审核的人员信息
            if (Yii::app()->session['storeid'] == 0) {
                $plurality_info = $plurality_model->findAll("audite = 1");
            } else {
                $plurality_info = $plurality_model->findAllBySql("select * from tbl_plurality where _storeid in (" . Yii::app()->session["storeid"] . ") AND audite = 1");
            }

            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $deleteParttime = "hidden";
            $addParttime = "hidden";
            $editParttime = "hidden";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "添加兼职人员") {
                    $addParttime = "";
                }
                if ($itemName == "删除兼职人员") {
                    $deleteParttime = "";
                }
                if ($itemName == "编辑兼职人员") {
                    $editParttime = "";
                }
            }
            $this->renderPartial('partTimeInfo', array('plurality_info' => $plurality_info, 'deleteParttime' => $deleteParttime, 'addParttime' => $addParttime, 'editParttime' => $editParttime, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 兼职人员管理 start ************** */
    /*     * ************** 新增兼职人员 start ************** */

    public function actionaddPlurality() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            if (isset($_POST["addName"])) {
                $name = $_POST["addName"];
                $phone = $_POST["addPhone"];
                $type = $_POST["addType"];
                $school = $_POST["school"];
                $addTime = $_POST["addTime"];
                $plurality_model = new plurality();

                $plurality_model->pluralityName = $name;
                $plurality_model->phone = $phone;
                $plurality_model->pluralityType = $type;
                $plurality_model->_storeid = $school;
                if (isset($addTime)) {
                    $plurality_model->addTime = $addTime;
                }
                $plurality_model->audite = 1;
                if ($plurality_model->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            } else {
                $store_model = store::model();
                if (Yii::app()->session['storeid'] == 0) {
                    $store_info = $store_model->findAll(array('order' => "storeid DESC"));
                } else {
                    $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . Yii::app()->session["storeid"] . ")", array('order' => "storeid DESC"));
                }
                $this->renderPartial('addPlurality', array('store_info' => $store_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 新增兼职人员 end ************** */
    /*     * ************** 删除兼职人员 end ************** */

    public function actiondelPlurality() {
        if (isset(Yii::app()->session['adminuser'])) {
            if (isset($_POST['pluralityId'])) {
                $plurality_model = plurality::model();
                $pluralityId = $_POST['pluralityId'];
                if ($plurality_model->deleteAll("pluralityId=$pluralityId")) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 删除兼职人员 end ************** */
    /*     * ************** 修改兼职人员 start ************** */

    public function actioneditPlurality($pluralityId) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $plurality_mode = plurality::model();

            $store_model = store::model();
            if (Yii::app()->session['storeid'] == 0) {
                $store_info = $store_model->findAll(array('order' => "storeid DESC"));
            } else {
                $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . Yii::app()->session["storeid"] . ")", array('order' => "storeid DESC"));
            }

            $plurality_info = $plurality_mode->find("pluralityId=$pluralityId");
            $this->renderPartial('editPlurality', array('plurality_info' => $plurality_info, 'store_info' => $store_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actioneditPluralitys() {
        if (isset(Yii::app()->session['adminuser'])) {

            $plurality_mode = plurality::model();
            $pluralityId = $_POST['pluralityId'];
            $pluralityName = $_POST['pluralityName'];
            $pluralityPhone = $_POST['phone'];
            $pluralityType = $_POST['type'];
            $addTime = $_POST['addTime'];
            $leaveTime = $_POST['leaveTime'];
            $plurality_info = $plurality_mode->find("pluralityId=$pluralityId");
            if (count($plurality_info) > 0) {
                $plurality_info->pluralityName = $pluralityName;
                $plurality_info->phone = $pluralityPhone;
                $plurality_info->pluralityType = $pluralityType;
                if (isset($_POST['addTime'])) {
                    $plurality_info->addTime = $addTime;
                }
                if (isset($_POST['leaveTime'])) {
                    $plurality_info->leaveTime = $leaveTime;
                }
                if ($plurality_info->save()) {
                    $json = '{"data":"success"}';
                } else {
                    $json = '{"data":"false"}';
                }
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 修改兼职人员 end ************** */

    /*     * ************** 审核兼职人员 start ************** */

    public function actionaudite() {
        if (isset(Yii::app()->session['adminuser'])) {
            $plurality_model = plurality::model();
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            //删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $parttimePass = "false";
            $parttimeDeny = "false";
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "兼职通过") {
                    $parttimePass = "true";
                }
                if ($itemName == "兼职淘汰") {
                    $parttimeDeny = "true";
                }
            }
//未通过审核的人员信息
            $plurality_info = $plurality_model->findAll("audite=0");
            $this->renderPartial('auditePlurality', array('plurality_info' => $plurality_info, 'parttimePass' => $parttimePass, 'parttimeDeny' => $parttimeDeny, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//通过
    public function actionpassSelected() {
        if (isset(Yii::app()->session['adminuser'])) {

            if (isset($_POST['ids'])) {
                $plurality_mode = plurality::model();
                $ids = $_POST['ids'];
                $num = $plurality_mode->updateAll(array("audite" => 1), "pluralityId in ($ids)");
                if ($num > 0) {
                    $json = '{"data":"success"}';
                } else {
                    $json = '{"data":"false"}';
                }
                echo $json;
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    //淘汰
    public function actionoutSelected() {
        if (isset(Yii::app()->session['adminuser'])) {
            if (isset($_POST['ids'])) {
                $plurality_mode = plurality::model();
                $ids = $_POST['ids'];
                $num = $plurality_mode->updateAll(array("audite" => 2), "pluralityId in ($ids)");
                if ($num > 0) {
                    $json = ' {
                "data":"success"
            }';
                    echo $json;
                } else {
                    $json = ' {
                "data":"false"
            }';
                    echo $json;
                }
            }
        } else
            $this->redirect('./index.php?r=default/index');
    }

    //兼职冲账
    public function actionreverse() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $integralDetails_model = integralDetails::model();
            $integralDetails_info = $integralDetails_model->findAll("statue != 0");

            $this->renderPartial('reverse', array('integralDetails_info' => $integralDetails_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    //兼职冲账状态更改
    public function actioneditsatue() {
        $integralDetailsId = $_POST["integralDetailsId"];
        $statue = $_POST["statue"];
        $rechargedescript = $_POST["rechargedescript"];

        $integralDetails_model = integralDetails::model();
        $integralDetails_info = $integralDetails_model->findByPk($integralDetailsId);
        if (count($integralDetails_info) != 0) {
            if ($statue == "section") {//部分
                $integralDetails_info->statue = 1;
            } else if ($statue == "all") {
                $integralDetails_info->statue = 2;
            }
            $integralDetails_info->rechargedescript = $rechargedescript;
            if ($integralDetails_info->save()) {
                $json = '{"data":"success"}';
                echo $json;
            } else {
                $json = '{"data":"fail"}';
                echo $json;
            }
        } else {
            $json = '{"data":"fail"}';
            echo $json;
        }
    }

}
