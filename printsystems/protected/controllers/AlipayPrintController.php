<?php

class alipayPrintController extends Controller {

    public function actionPayInfo() {
        if (!empty($_POST)) {
            $type = $_POST['type'];
            $out_trade_no = $_POST['out_trade_no'];
            $trade_no = $_POST['trade_no'];
            $total_amount = $_POST['total_amount'];
            $buyer_logon_id = $_POST['buyer_logon_id'];
            $trade_status = $_POST['trade_status'];
            $buyer_pay_amount = $_POST['buyer_pay_amount'];

            $printpaytemp = new printpaytemp();
            $printpaytemp->out_trade_no = $out_trade_no;
            $printpaytemp->trade_no = $trade_no;
            $printpaytemp->total_amount = $total_amount;
            $printpaytemp->buyer_logon_id = $buyer_logon_id;
            $printpaytemp->trade_status = $trade_status;
            $printpaytemp->buyer_pay_amount = $buyer_pay_amount;

            $printpaytemp->addTime = date('Y-m-d H:i:s', time());
//            if ($type = 'success') {
//                $printpaytemp->trade_status = 1;
//            } else {
//                $printpaytemp->trade_status = 0;
//            }
            $printpaytemp->save();
            echo 'success';
        } else {
            echo 'error';
        }
    }

    public function actionPayment() {
        if (!empty($_POST)) {
            $out_trade_no = $_POST['out_trade_no'];
            $total_fee = $_POST['total_fee'];
            $buyer_email = $_POST['buyer_email'];
            $trade_no = $_POST['trade_no']; //支付宝交易号

            $user_model = user::model();
            $business_model = business::model();
            $subbusiness_model = subbusiness::model();
            $record_model = record::model();
            $integralDetails_model = new integralDetails();

            $business_info = $business_model->find(array('condition' => "orderId=$out_trade_no"));
            $user_infoss = $user_model->find(array('condition' => "userid = '$business_info->_userid'"));

            $record_info = $record_model->find(array('condition' => "userid = '$business_info->_userid'"));
            $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessid = $business_info->businessid"));
            foreach ($subbusiness_info as $subbus) {
                if ($subbus->sellerAccounter != NULL) {
                    $status = false;
                } else {
                    $status = true;
                }
            }
            if ($status) {
                $addintegration = $total_fee * 10; //需要增加的积分
//需要减去的积分
                $integration = ($business_info->paidMoney - $total_fee) * 100;
                if ($integration != 0) {
                    $integralDetails_info = new integralDetails();
                    $record_info->points -= $integration;
                    $integralDetails_info->_userid = $business_info->_userid;
                    $integralDetails_info->addIntegral = 0;
                    $integralDetails_info->reduceIntegral = $integration;
                    date_default_timezone_set('PRC');
                    $integralDetails_info->happentime = date('Y-m-d H:i:s');
                    $integralDetails_info->happenInfo = "支付订单号" . $out_trade_no . "扣积分";
                    $integralDetails_info->save();
                }

                $record_info->points +=$addintegration;
                $integralDetails_model->_userid = $business_info->_userid;
                $integralDetails_model->addIntegral = $addintegration;
                $integralDetails_model->reduceIntegral = 0;
                date_default_timezone_set('PRC');
                $integralDetails_model->happentime = date('Y-m-d H:i:s');
                $integralDetails_model->happenInfo = "支付订单号" . $out_trade_no . "送积分";
                $integralDetails_model->save();


                foreach ($subbusiness_info as $subbus) {
                    $subbus->sellerAccounter = $buyer_email;
                    $subbus->isPay = 1;
                    if ($integration != 0)
                        $subbus->payType = 6; //6积分+支付宝支付
                    else
                        $subbus->payType = 1; //支付宝支付

                    date_default_timezone_set('PRC');

                    $subbus->payTime = date('Y-m-d H:i:s');
                    $subbus->trade_no = $trade_no;
                    $subbus->save();
                }

                if (!isset($business_info->verificationCode)) {//为空
                    $verificationCode = mt_rand(100000, 999999); //验证码
                    $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一

                    while ($verificationCodes) {
                        $verificationCode = mt_rand(100000, 999999);
                        $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一
                    }
                    $user = 'cqutprint'; //短信接口用户名 $user
                    $pwd = '112233'; //短信接口密码 $pwd
                    $mobiles = $user_infoss->phone; //说明：取用户输入的手号
                    $contents = "亲爱的用户，您此次打印的验证码为" . $verificationCode . ",您已支付成功，请您于终端机打印,谢谢！【颇闰科技】"; //说明：取用户输入的短信内容
                    $chid = 0; //通道ID
                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";

                    $business_info->verificationCode = $verificationCode;
                    $business_info->consumptionIntegral = $integration;
                    file_get_contents($sendMessage);
                } else
                    $verificationCode = $business_info->verificationCode;

                if ($business_info->save() && $record_info->save()) {
                    echo "success";
                } else {
                    echo '未知的错误！';
                }
            } else {
                echo "success";
            }
        } else {
            echo 'error';
        }
    }

//退款完成处理函数
    public function actionrefund() {
        if (!empty($_POST)) {
            $batch_no = $_POST['batch_no'];
            $result_details = $_POST['result_details'];
//2015111321001004640024011251^0.20^SUCCESS#2015111321001004640024450965^0.40^SUCCESS
//            $result_details = "2015111321001004640024011251^0.20^SUCCESS#2015111321001004640024450965^0.40^SUCCESS";
//            2015112713504588722015112321001004070262545184^0.20^TRADE_HAS_CLOSED   已经退款

            $refund_model = refund::model();
            $subbusiness_model = subbusiness::model();
            $record_model = record::model();
            $business_model = business::model();
//            file_put_contents('D:/data.txt', $batch_no . "," . $result_details);

            $result_detailss = explode('#', $result_details); //ID 1,2,3,4
            foreach ($result_detailss as $k => $o) {
                $result_detailsd = explode('^', $o); //ID 1,2,3,4
                $refund_info = $refund_model->find(array('condition' => "tborderId = '$result_detailsd[0]' AND money = $result_detailsd[1] AND statue != 1"));

                $refund_info->info = $_POST['result_details'];
                $str2 = substr($result_detailsd[2], 0, 7);
                if ($str2 == "SUCCESS") {
                    $refund_info->statue = 1;
                    date_default_timezone_set('PRC');
                    $refund_info->refundTime = date('Y-m-d H:i:s');


                    if ($refund_info->_sessionId == "") {
                        $subbusiness_info = $subbusiness_model->find(array('condition' => "subbusinessId = $refund_info->subbusinessId"));
                        $subbusiness_info->isrefund = 1;

                        $business_info = $business_model->findByPk($subbusiness_info->_businessId);
                        $business_info->isrefund = 1;


//减去支付宝支付送的积分和加上付款扣掉的积分
                        $record_info = $record_model->find(array('condition' => "userid = $refund_info->_userId"));
                        $integralDetails_model = new integralDetails();

                        if ($refund_info->consumptionIntegral == 0) {
                            $Integral = $record_info->points - $refund_info->money * 10;
                            $integralDetails_model->addIntegral = 0;
                            $integralDetails_model->reduceIntegral = $refund_info->money * 10;
                        } else {
                            $Integral = $record_info->points - $refund_info->money * 10 + $refund_info->consumptionIntegral;
                            $integralDetails_model->addIntegral = $refund_info->consumptionIntegral;
                            $integralDetails_model->reduceIntegral = $refund_info->money * 10;
                        }
                        $record_info->points = intval(round(floatval($Integral)));
                        $integralDetails_model->_userid = $refund_info->_userId;
                        date_default_timezone_set('PRC');
                        $integralDetails_model->happentime = date('Y-m-d H:i:s');
                        $integralDetails_model->happenInfo = "订单号" . $refund_info->_orderId . "退款";

                        $record_info->save();
                        $subbusiness_info->save();
                        $business_info->save();
                        $integralDetails_model->save();
                    }
                } else {
                    $refund_info->statue = 2;
                }
                $refund_info->save();
            }
        } else {
            echo 'error';
        }
    }

//微信支付成功处理函数
    public function actionwechatpayment() {
        $result_code = $_POST["result_code"];
        $err_code = $_POST["err_code"];
        $err_code_des = $_POST["err_code_des"];
        $trade_type = $_POST["trade_type"];
        $transaction_id = $_POST["transaction_id"];
        $out_trade_no = $_POST["out_trade_no"];
        $time_end = $_POST["time_end"];
        $total_fee = $_POST["total_fee"];

        $wechatpay_model = new wechatpay();
        $wechatpay_info = $wechatpay_model->find("transaction_id = '$transaction_id'");
        if (count($wechatpay_info) == 0) {
            $wechatpay_model->result_code = $result_code;
            $wechatpay_model->err_code = $err_code;
            $wechatpay_model->err_code_des = $err_code_des;
            $wechatpay_model->trade_type = $trade_type;
            $wechatpay_model->transaction_id = $transaction_id;
            $wechatpay_model->out_trade_no = $out_trade_no;
            $wechatpay_model->time_end = $time_end;
            $wechatpay_model->total_fee = $total_fee;
            $wechatpay_model->save();
        }
        header("Content-type:text/xml");
        $str = '<?xml version="1.0" encoding="utf-8"?><xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
        echo $str;
    }

}
