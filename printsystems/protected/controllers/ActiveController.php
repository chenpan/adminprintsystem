<?php

include 'BaseController.php';

class activeController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function filters() {
        return array(
            'activepoints + activepoints',
            'parttimepoints + parttimepoints',
        );
    }

    public function filteractivepoints($filterChain) {
        $this->checkAccess("增加积分", $filterChain);
    }

    public function filterparttimepoints($filterChain) {
        $this->checkAccess("扣除积分", $filterChain);
    }

    public function actionparttimepoints() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();

        //扣除积分页面
        if (isset(Yii::app()->session['adminuser'])) {
            $this->renderPartial('parttimepoints', array('leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

// 扣除积分
    public function actiondivpoints() {
        $user_model = user::model();
        if (isset($_POST['phone'])) {
            $phone = $_POST['phone'];
            $phoneArray = explode(",", $phone);
            $flag = true;
            $msg = "保存失败";
            foreach ($phoneArray as $value) {
                if (preg_match("/1[3578]{1}\d{9}$/", $value)) {
                    $phone_uni = $value;
                    $points = $_POST['points'];
                    $content = $_POST['text'];
                    $addTime = date('y-m-d H:i:s', time());
                    $user_info = $user_model->find(array('condition' => "phone='$phone_uni'"));
                    if (count($user_info) > 0) {
                        $userId = $user_info->userid;
                        $olderpoints = $user_info->integration;
                        $pointss = $olderpoints - $points;
                        if ($pointss >= 0) {
                            $user_info->integration = $pointss;
                            if ($user_info->save()) {
                                $points_model = new integralDetails();
                                $points_model->_userid = $userId;
                                $points_model->reduceIntegral = $points;
                                $points_model->happentime = $addTime;
                                $points_model->happenInfo = $content;
                                if ($points_model->save()) {
                                    
                                } else {
                                    $flag = false;
                                }
                            }
                        } else {
                            $flag = false;
                            $msg = "积分不够扣";
                        }
                    } else {
                        $flag = false;
                        $msg = "无此用户！";
                    }
                } else {
                    $flag = false;
                    $msg = "手机号码不正确！";
                }
            }
            if (!$flag) {
                $json = '{"data":"' . $msg . '"}';
                echo $json;
            } else {
                $user = 'cqutprint'; //短信接口用户名 $user
                $pwd = '112233'; //短信接口密码 $pwd
                $mobiles = $phone; //说明：取用户输入的手号
                $chid = 0; //通道ID
                $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$content}&chid={$chid}&sendtime=";
                file_get_contents($sendMessage);
                $json = '{"data":"sucess"}';
                echo $json;
            }
        }
    }

    //增加积分页面
    public function actionactivepoints() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $user_model = user::model();
        //增加积分功能
        if (isset(Yii::app()->session['adminuser'])) {
            if (isset($_POST['phone'])) {
                $phone = $_POST['phone'];
                $points = $_POST['points'];
                $content = $_POST['text'];
                $checked = $_POST['check']; //判断是否为运营
                $addTime = date('y-m-d H:i:s', time());
                $phoneArray = explode(",", $phone);
                $flag = true;
                $msg = "增加失败！";
                foreach ($phoneArray as $value) {
                    if (preg_match("/1[3578]{1}\d{9}$/", $value)) {
                        $user_info = $user_model->find(array('condition' => "phone= '$value'"));
                        if (count($user_info) > 0) {
                            $user_info->integration += $points;
                            if ($user_info->save()) {
                                $points_model = new integralDetails();
                                $points_model->_userid = $user_info->userid;
                                $points_model->addIntegral = $points;
                                $points_model->happentime = $addTime;
                                $points_model->happenInfo = $content;
                                if ($checked == "yes") {
                                    $points_model->statue = 1; //充值了未冲账完
                                }
                                if ($points_model->save()) {
                                    
                                } else {
                                    $flag = false;
                                }
                            }
                        } else {
                            $flag = false;
                            $msg = "无此用户！";
                        }
                    } else {
                        $flag = false;
                        $msg = "手机号码不正确！";
                    }
                }
                if ($flag) {
                    $user = 'cqutprint'; //短信接口用户名 $user
                    $pwd = '112233'; //短信接口密码 $pwd
                    $mobiles = $phone; //说明：取用户输入的手号
                    $contents = $content; //说明：取用户输入的短信内容
                    $chid = 0; //通道ID
                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
                    file_get_contents($sendMessage);
                    echo "<script>parent.$('#add_success').text('增加成功！').css('color','red');parent.window.location.href = './index.php?r=active/activepoints';</script>";
                } else {
                    echo "<script>parent.$('#add_success').text('" . $msg . "').css('color','red');</script>";
                }
            } else {
                $this->renderPartial('activepoints', array('leftContent' => $leftContent, 'recommend' => $recommend));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//查询手机号剩余积分
    public function actionoldpoints() {
        $user_model = user::model();
        if (isset($_POST["phone"])) {
            $phone = $_POST["phone"];
            $olderpoints = -1;
            $user_info = $user_model->find(array('condition' => "phone='$phone'"));
            if ($user_info) {
                $olderpoints = $user_info->integration;
            }
            if ($olderpoints >= 0) {
                $data[] = array(
                    'code' => 200,
                    'msg' => $olderpoints,
                );
                echo $data = json_encode($data);
            } else {
                $data[] = array(
                    'code' => 401,
                    'msg' => "获取失败！",
                );
                echo $data = json_encode($data);
            }
        }
    }

}
