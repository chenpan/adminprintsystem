<?php
include 'BaseController.php';
class nonPrivilegeController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }
    public function  actionindex(){
        $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
        $this->renderPartial('index', array("leftContent" => $leftContent, 'recommend' => $recommend));
    }
}