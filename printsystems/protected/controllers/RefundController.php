<?php

include 'BaseController.php';

class refundController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    /*
      权限管理
     */

    public function filters() {
        return array(
            'refund + refund', //退款管理
            'deleterefund + deleterefund',
            'prbdetails + prbdetails',
            'alreadyRefund + alreadyRefund', //已退款列表
            'orderDetail + orderDetail',
            'refund_zfb + refund_zfb',
            'refund_wx + refund_wx',
            'refund_points + refund_points',
        );
    }

    public function filterrefund($filterChain) {
        $this->checkAccess("退款列表", $filterChain);
    }

    public function filterdeleterefund($filterChain) {
        $this->checkAccess("删除退款", $filterChain);
    }

    public function filterprbdetails($filterChain) {
        $this->checkAccess("退款列表", $filterChain);
    }

    public function filteralreadyRefund($filterChain) {
        $this->checkAccess("已退款列表", $filterChain);
    }

    public function filterorderDetail($filterChain) {
        $this->checkAccess("退款列表", $filterChain);
    }

    public function filterrefund_wx($filterChain) {
        $this->checkAccess("微信退款", $filterChain);
    }

    public function filterrefund_zfb($filterChain) {
        $this->checkAccess("支付宝退款", $filterChain);
    }

    public function filterrefund_points($filterChain) {
        $this->checkAccess("积分退款", $filterChain);
    }

    /*     * ************** 退款 start ************** */

    public function actionrefund() {
        if (isset(Yii::app()->session['adminuser'])) {

            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

//删除功能权限判断
            $admin = Yii::app()->session['adminuser'];
            $admin_model = administrator::model();
            $assign_model = assignment::model();
            $item_model = item::model();
            $roleId = $admin_model->find("username='$admin'")->_roleid;
            $assign = $assign_model->findAll("_roleId='$roleId'");
            $deleteRefund = "false"; //删除退款
            $zfbrefund = "false"; //支付宝退款
            $wxrefund = "false"; //微信退款
            $jfrefund = "false"; //积分退款
            foreach ($assign as $value) {
                $id = $value->_itemId;
                $assign_info = $item_model->find("itemId ='$id'");
                $itemName = $assign_info->itemName;
                if ($itemName == "删除退款") {
                    $deleteRefund = "true";
                }
                if ($itemName == "支付宝退款") {
                    $zfbrefund = "true";
                }
                if ($itemName == "微信退款") {
                    $wxrefund = "true";
                }
                if ($itemName == "积分退款") {
                    $jfrefund = "true";
                }
            }

            $this->renderPartial('refund', array('deleteRefund' => $deleteRefund, 'zfbrefund' => $zfbrefund, 'wxrefund' => $wxrefund, 'jfrefund' => $jfrefund, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    public function actionrefundAjax() {//后台分页
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=default/index');
        }
        $storeId = Yii::app()->session['storeid'];
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 1:
                    $orderSql = " order by a._sessionId " . $order_dir;
                    break;
                case 2:
                    $orderSql = " order by b.username " . $order_dir;
                    break;
                case 3:
                    $orderSql = " order by a._orderId " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by a.money " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by a.Integral " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by a.refundType " . $order_dir;
                    break;
                case 8:
                    $orderSql = " order by a.tborderId " . $order_dir;
                    break;
                case 9:
                    $orderSql = " order by a.sellerAccounter " . $order_dir;
                    break;
                case 10:
                    $orderSql = " order by a.applyTime " . $order_dir;
                    break;
                case 12:
                    $orderSql = " order by c.storename " . $order_dir;
                    break;
                case 13:
                    $orderSql = " order by a.statue " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页 
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }

//        //查询打印终端
//        //第一步，先在printor表里面找到machineId
//        $sqld = "select machineId from tbl_printor where printorName like '%" . $search . "%'";
//        $rowd = Yii::app()->db2->createCommand($sqld)->query();
//        $strd = "";
//        foreach ($rowd as $k => $v) {
//            $strd .= "'" . $v['machineId'] . "',";
//        }
//        if (strlen($strd) == 0) {
//            $strd = "'" . 0 . "',";
//        }
//        $strd = substr($strd, 0, -1);
//
//        //第二步，在prbusiness表里面找到machineId等于$strd的_sessionId
//        $sqlx = "select sessionId from tbl_prbusiness where machineId in ($strd)";
//        $rowx = Yii::app()->db->createCommand($sqlx)->query();
//        $strx = "";
//        foreach ($rowx as $k => $v) {
//            $strx .= "'" . $v['sessionId'] . "',";
//        }
//        if (strlen($strx) == 0) {
//            $strx = "'" . 0 . "',";
//        }
//        $strx = substr($strx, 0, -1);



        if ($storeId == 0) {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.refundId) as sum FROM tbl_refund a join tbl_user b on a._userId = b.userid join tbl_store c on a._storeid=c.storeid where a.statue != 1 and a.statue!=3";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $refund_sum_info = $result->queryAll();
            foreach ($refund_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            $sumSqlWhere = " and (a._sessionId LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or a._orderId LIKE '%" . $search . "%' or a.money LIKE '%" . $search . "%'"
                    . " or a.tborderId LIKE '%" . $search . "%'  or a.Integral LIKE '%" . $search . "%' or a.applyTime LIKE binary '%" . $search . "%' or"
                    . " c.storename like '%" . $search . "%')";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $refund_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($refund_sum_filter_info[0]['sum'] > 0) {
                    foreach ($refund_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a._sessionId,a.refundId,a.subbusinessId,b.username,a._orderId,a.money,a.Integral,a.payType,a.tborderId,a.sellerAccounter,a.applyTime,a.refundType,a.statue,c.storename "
                    . "FROM tbl_refund a join tbl_user b on a._userid = b.userid join tbl_store c on a._storeid=c.storeid where a.statue != 1 and a.statue!=3";
        } else {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.refundId) as sum FROM tbl_refund a join tbl_user b on a._userId = b.userid join tbl_store c on a._storeid=c.storeid where a.statue!= 1 and a.statue!=3 and a._storeid in ($storeId)";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $refund_sum_info = $result->queryAll();
            foreach ($refund_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a._sessionId LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or a._orderId LIKE '%" . $search . "%' or a.money LIKE '%" . $search . "%'  or a._orderId LIKE '%" . $search . "%'"
                    . " or a.tborderId LIKE '%" . $search . "%'  or a.Integral LIKE '%" . $search . "%' or a.applyTime LIKE binary '%" . $search . "%' or"
                    . " c.storename like '%" . $search . "%')";
//                    . " or a._sessionId in ($strx)))";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $refund_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($refund_sum_filter_info) > 0) {
                    foreach ($refund_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a._sessionId,a.refundId,a.subbusinessId,b.username,a._orderId,a.money,a.Integral,a.payType,a.tborderId,a.sellerAccounter,a.applyTime,a.refundType,a.statue,c.storename "
                    . "FROM tbl_refund a join tbl_user b on a._userId = b.userid join tbl_store c on a._storeid=c.storeid where a.statue!= 1 and a.statue!=3 and a._storeid in ($storeId)";
        }
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $refund_info_filter = $dataResult->queryAll();
            if (count($refund_info_filter) > 0) {
                foreach ($refund_info_filter as $v) {
                    $subbusinessId = $v['subbusinessId'];
                    $storeName = $v['storename'];
                    $refundId = $v['refundId'];
                    $sessionId = $v['_sessionId'];
                    $name = $v['username'];
                    $orderId = $v['_orderId'];
                    $busines = business::model()->find("orderId = '$orderId'");
                    if ($orderId != "") {
                        $businessId = base64_encode($busines->businessid);
                    } else {
                        $businessId = "";
                    }
                    $money = $v['money'];
                    $Integral = $v['Integral'];
                    $payType = $v['payType'];
                    if ($payType == 1 || $payType == 4)
                        $payTypeName = "支付宝";
                    else if ($payType == 5)
                        $payTypeName = "积分";
                    else if ($payType == 7) {
                        $payTypeName = "微信";
                    }
                    $tborderId = $v['tborderId'];
                    if ($tborderId == "") {
                        if (isset($busines->trade_no)) {
                            $tborderId = $busines->trade_no;
                        }
                    }
                    $sellerAccounter = $v['sellerAccounter'];
                    $applyTime = $v['applyTime'];
                    $refundType = $v['refundType'];
                    if ($refundType == 1) {
                        $refundTypeName = '支付宝';
                    } else if ($refundType == 5) {
                        $refundTypeName = '积分';
                    } else if ($refundType == 7) {
                        $refundTypeName = '微信';
                    }
                    $statue = $v['statue'];
                    if ($statue == 0)
                        $statueName = '未退款';
                    else if ($statue == 1)
                        $statueName = '已退款';
                    else if ($statue == 2) {
                        $statueName = '退款失败';
                    } else if ($statue == 3) {
                        $statueName = '已退款';
                    }
                    $data1[] = array(
                        'refundId' => $refundId,
                        'sessionId' => $sessionId,
                        'name' => $name,
                        'orderId' => $orderId,
                        'businessId' => $businessId,
                        'money' => $money,
                        'Integral' => $Integral,
                        'payTypeName' => $payTypeName,
                        'tborderId' => $tborderId,
                        'sellerAccounter' => $sellerAccounter,
                        'applyTime' => $applyTime,
                        'refundTypeName' => $refundTypeName,
                        'storeName' => $storeName,
                        'statueName' => $statueName
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
            //直接查询所有记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $refund_info_filter = $dataResult->queryAll();
            foreach ($refund_info_filter as $v) {
                $subbusinessId = $v['subbusinessId'];
                $storeName = $v['storename'];
                $refundId = $v['refundId'];
                $sessionId = $v['_sessionId'];
                $name = $v['username'];
                $orderId = $v['_orderId'];
                $busines = business::model()->find("orderId = '$orderId'");
                if ($orderId != "") {
                    $businessId = base64_encode($busines->businessid);
                } else {
                    $businessId = "";
                }
                $money = $v['money'];
                $Integral = $v['Integral'];
                $payType = $v['payType'];
                if ($payType == 1 || $payType == 4)
                    $payTypeName = "支付宝";
                else if ($payType == 5)
                    $payTypeName = "积分";
                else if ($payType == 7) {
                    $payTypeName = "微信";
                } else {
                    $payTypeName = " ";
                }
                $tborderId = $v['tborderId'];
                if ($tborderId == "") {
                    if (isset($busines->trade_no)) {
                        $tborderId = $busines->trade_no;
                    }
                }
                $sellerAccounter = $v['sellerAccounter'];
                $applyTime = $v['applyTime'];
                $refundType = $v['refundType'];
                if ($refundType == 1) {
                    $refundTypeName = '支付宝';
                } else if ($refundType == 5) {
                    $refundTypeName = '积分';
                } else if ($refundType == 7) {
                    $refundTypeName = '微信';
                } else {
                    $refundTypeName = ' ';
                }
                $statue = $v['statue'];
                if ($statue == 0)
                    $statueName = '未退款';
                else if ($statue == 1)
                    $statueName = '已退款';
                else if ($statue == 2) {
                    $statueName = '退款失败';
                } else if ($statue == 3) {
                    $statueName = '退款中';
                }
                $data1[] = array(
                    'refundId' => $refundId,
                    'sessionId' => $sessionId,
                    'name' => $name,
                    'orderId' => $orderId,
                    'businessId' => $businessId,
                    'money' => $money,
                    'Integral' => $Integral,
                    'payTypeName' => $payTypeName,
                    'tborderId' => $tborderId,
                    'sellerAccounter' => $sellerAccounter,
                    'applyTime' => $applyTime,
                    'refundTypeName' => $refundTypeName,
                    'storeName' => $storeName,
                    'statueName' => $statueName
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

    /*     * ************** 查找文件 ************** */

    public function actiondeleterefund() {//删除退款记录
        $refundIdd = $_POST["refundIdd"];
        $refundId = explode(",", $refundIdd);

        $refund_model = refund::model();
        $subbusiness_model = subbusiness::model();
        $business_model = business::model();
        foreach ($refundId as $k => $l) {
            $refund_info = $refund_model->findByPk($l);
            //删除前先判断是否是订单 ，改变其退款状态为未退款           
            if (count($refund_info) != 0) {
                if (isset($refund_info->subbusinessId)) {
                    $subbusiness_info = $subbusiness_model->findByPk($refund_info->subbusinessId);
                    if (count($subbusiness_info) != 0) {
                        $subbusiness_info->isrefund = 0;
                        if ($subbusiness_info->save()) {
                            $business_info = $business_model->findByPk($subbusiness_info->_businessId);
                            if (count($business_info) != 0) {
                                $business_info->isrefund = 0;
                                $business_info->save();
                            }
                        }
                    }
                }
                $refund_model->deleteByPk($l);
            }
        }
        $json = '{"data":"success"}';
        echo $json;
    }

    public function actionprbdetails($_sessionId, $trade_no) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $prbusiness_model = prbusiness::model();
            $prbusiness_info = $prbusiness_model->findAll("sessionId = '$_sessionId'AND trade_no='$trade_no'");
            $this->renderPartial('prbdetails', array('prbusiness_info' => $prbusiness_info, '_sessionId' => $_sessionId, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 退款end ************** */
    /*     * ************** 已退款列表start ************** */

    public function actionalreadyRefund() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $this->renderPartial('alreadyRefund', array('leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 已退款列表end ************** */

    //alreadyRefund后台分页处理
    public function actionalreadyRefundAjax() {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
            $this->redirect('./index.php?r=default/index');
        }
        $storeId = Yii::app()->session['storeid'];
        $subbusiness_model = subbusiness::model();
        $prbusiness_model = prbusiness::model();
        $store_model = store::model();
        $draw = $_GET['draw'];
        $order_column = $_GET['order']['0']['column']; //那一列排序，从0开始
        $order_dir = $_GET['order']['0']['dir']; //ase desc 升序或者降序
//拼接排序sql
        $orderSql = "";
        if (isset($order_column)) {
            $i = intval($order_column);
            switch ($i) {
                case 0:
                    $orderSql = " order by a._sessionId " . $order_dir;
                    break;
                case 1:
                    $orderSql = " order by b.username " . $order_dir;
                    break;
                case 2:
                    $orderSql = " order by a._orderId " . $order_dir;
                    break;
                case 4:
                    $orderSql = " order by a.money " . $order_dir;
                    break;
                case 5:
                    $orderSql = " order by a.Integral " . $order_dir;
                    break;
                case 6:
                    $orderSql = " order by a.payType " . $order_dir;
                    break;
                case 7:
                    $orderSql = " order by a.tborderId " . $order_dir;
                    break;
                case 8:
                    $orderSql = " order by a.applyTime " . $order_dir;
                    break;
                case 9:
                    $orderSql = " order by a.refundTime " . $order_dir;
                    break;
                case 10:
                    $orderSql = " order by a.refundType " . $order_dir;
                    break;
                case 10:
                    $orderSql = " order by a._storeid " . $order_dir;
                    break;
                default:
                    $orderSql = '';
            }
        }
        //搜索
        $search = $_GET['search']['value']; //获取前台传过来的过滤条件
        //分页 
        $start = $_GET['start']; //从多少开始
        $length = $_GET['length']; //数据长度
        $limitSql = '';
        $limitFlag = isset($_GET['start']) && $length != -1;
        if ($limitFlag) {
            $limitSql = " LIMIT " . intval($start) . ", " . intval($length);
        }
        if ($storeId == 0) {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.refundId) as sum FROM tbl_refund a join tbl_user b on a._userId = b.userid where a.statue = 1";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $refund_sum_info = $result->queryAll();
            foreach ($refund_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a._sessionId LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or a._orderId LIKE '%" . $search . "%' or a.money LIKE '%" . $search . "%' or a.Integral LIKE '%" . $search . "%'"
                    . " or a.tborderId LIKE '%" . $search . "%' or a.applyTime LIKE binary '%" . $search . "%' or a.refundTime LIKE binary '%" . $search . "%' or a._storeid in (select storeid from tbl_store where storename like '%" . $search . "%'))";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $refund_sum_filter_info = $recordsFilteredResult->queryAll();
                if ($refund_sum_filter_info[0]['sum'] > 0) {
                    foreach ($refund_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT b.username,a.* FROM tbl_refund a join tbl_user b on a._userid = b.userid where a.statue= 1";
        } else {
            //定义查询数据总记录数sql
            $sumSql = "SELECT count(a.refundId) as sum FROM tbl_refund a join tbl_user b on a._userId = b.userid where a.statue= 1 and a._storeid in ($storeId)";
            //条件过滤后记录数 必要
            $recordsFiltered = 0;
            //表的总记录数 必要
            $recordsTotal = 0;
            $result = Yii::app()->db2->createCommand($sumSql);
            $refund_sum_info = $result->queryAll();
            foreach ($refund_sum_info as $k => $l) {
                $recordsTotal = $l["sum"];
            }
            //定义过滤条件查询过滤后的记录数sql
            $sumSqlWhere = " and (a._sessionId LIKE '%" . $search . "%' or b.username LIKE '%" . $search . "%' or a._orderId LIKE '%" . $search . "%' or a.money LIKE '%" . $search . "%' or a.Integral LIKE '%" . $search . "%'"
                    . " or a.tborderId LIKE '%" . $search . "%' or a.applyTime LIKE binary '%" . $search . "%' or a.refundTime LIKE binary '%" . $search . "%' or a._storeid in (select storeid from tbl_store where storename like '%" . $search . "%'))";
            if (strlen($search) > 0) {
                $recordsFilteredResult = Yii::app()->db2->createCommand($sumSql . $sumSqlWhere);
                $refund_sum_filter_info = $recordsFilteredResult->queryAll();
                if (count($refund_sum_filter_info) > 0) {
                    foreach ($refund_sum_filter_info as $k1 => $l1) {
                        $recordsFiltered = $l1["sum"];
                    }
                } else {
                    $recordsFiltered = 0;
                }
            } else {
                $recordsFiltered = $recordsTotal;
            }
            $totalResultSql = "SELECT a.*,b.username FROM tbl_refund a join tbl_user b on a._userId = b.userid where a.statue= 1 and a._storeid in($storeId)";
        }
        if (strlen($search) > 0) {
            //如果有搜索条件，按条件过滤找出记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $sumSqlWhere . $orderSql . $limitSql);
            $refund_info_filter = $dataResult->queryAll();
            if (count($refund_info_filter) > 0) {
                foreach ($refund_info_filter as $v) {
                    $subbusinessId = $v['subbusinessId'];
                    $userStoreId = $v['_storeid'];
                    $refundId = $v['refundId'];
                    $sessionId = $v['_sessionId'];
                    $name = $v['username'];
                    $orderId = $v['_orderId'];
                    $busines = business::model()->find("orderId = '$orderId'");
                    if ($orderId != "") {
                        $businessId = base64_encode($busines->businessid);
                    } else {
                        $businessId = "";
                    }
                    $money = $v['money'];
                    $Integral = $v['Integral'];
                    $payType = $v['payType'];
                    if ($payType == 1)
                        $payTypeName = "支付宝";
                    else if ($payType == 5)
                        $payTypeName = "积分";
                    else if ($payType == 7) {
                        $payTypeName = "终端微信";
                    }
                    $tborderId = $v['tborderId'];
                    $applyTime = $v['applyTime'];
                    $refundTime = $v['refundTime'];
                    $refundType = $v['refundType'];
                    if ($refundType == 1) {
                        $refundTypeName = '支付宝';
                    } else if ($refundType == 5) {
                        $refundTypeName = '积分';
                    } else if ($payType == 4)
                        $payTypeName = "终端扫码支付";
                    else if ($refundType == 7) {
                        $refundTypeName = '微信';
                    }
                    if ($userStoreId != NULL)
                        $storeName = $store_model->find("storeid=$userStoreId")->storename;
                    else {
                        $storeName = "无";
                    }
                    $statue = $v['statue'];
                    $data1[] = array(
                        'sessionId' => $sessionId,
                        'name' => $name,
                        'orderId' => $orderId,
                        'businessId' => $businessId,
                        'money' => $money,
                        'Integral' => $Integral,
                        'payTypeName' => $payTypeName,
                        'tborderId' => $tborderId,
                        'applyTime' => $applyTime,
                        'refundTime' => $refundTime,
                        'refundTypeName' => $refundTypeName,
                        'storeName' => $storeName,
                    );
                }
                $data2 = array(
                    'draw' => intval($draw),
                    'recordsTotal' => intval($recordsTotal),
                    'recordsFiltered' => intval($recordsFiltered),
                    'aaData' => $data1
                );
                echo $data = json_encode($data2);
            }
        } else {
            //直接查询所有记录
            $dataResult = Yii::app()->db2->createCommand($totalResultSql . $orderSql . $limitSql);
            $refund_info_filter = $dataResult->queryAll();
            foreach ($refund_info_filter as $v) {
                $subbusinessId = $v['subbusinessId'];
                $userStoreId = $v['_storeid'];
                $refundId = $v['refundId'];
                $sessionId = $v['_sessionId'];
                $name = $v['username'];
                $orderId = $v['_orderId'];
                $busines = business::model()->find("orderId = '$orderId'");
                if ($orderId != "") {
                    $businessId = base64_encode($busines->businessid);
                } else {
                    $businessId = "";
                }
                $money = $v['money'];
                $Integral = $v['Integral'];
                $payType = $v['payType'];
                if ($payType == 1)
                    $payTypeName = "支付宝";
                else if ($payType == 5)
                    $payTypeName = "积分";
                else if ($payType == 4)
                    $payTypeName = "终端扫码支付";
                else if ($payType == 7) {
                    $payTypeName = "终端微信";
                }
                $tborderId = $v['tborderId'];
                $applyTime = $v['applyTime'];
                $refundTime = $v['refundTime'];
                $refundType = $v['refundType'];
                if ($refundType == 1) {
                    $refundTypeName = '支付宝';
                } else if ($refundType == 5) {
                    $refundTypeName = '积分';
                } else if ($refundType == 7) {
                    $refundTypeName = '微信';
                }
                if ($userStoreId != NULL)
                    $storeName = $store_model->find("storeid=$userStoreId")->storename;
                else {
                    $storeName = "无";
                }
                $data1[] = array(
                    'sessionId' => $sessionId,
                    'name' => $name,
                    'orderId' => $orderId,
                    'businessId' => $businessId,
                    'money' => $money,
                    'Integral' => $Integral,
                    'payTypeName' => $payTypeName,
                    'tborderId' => $tborderId,
                    'applyTime' => $applyTime,
                    'refundTime' => $refundTime,
                    'refundTypeName' => $refundTypeName,
                    'storeName' => $storeName,
                );
            }
            $data2 = array(
                'draw' => intval($draw),
                'recordsTotal' => intval($recordsTotal),
                'recordsFiltered' => intval($recordsFiltered),
                'aaData' => $data1
            );
            echo $data = json_encode($data2);
        }
    }

    /*     * ************** 订单详情 start************** */

//订单详情
    public function actionorderDetail($businessid) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $username = Yii::app()->session['adminuser'];

            $businessidd = base64_decode($businessid);

            $attachment_model = attachment::model();
            $user_model = user::model();
            $businssid_model = business::model();
            $subbusiness_model = subbusiness::model();

            $businessid_info = $businssid_model->find(array("condition" => "businessid=$businessidd"));

            $integration = (int) $user_model->find(array("condition" => "userid = '$businessid_info->_userid'"))->integration;

            $attachmentArray = array();

            $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId=$businessidd"));
            if ($subbusiness_info) {
                foreach ($subbusiness_info as $l => $y) {
                    $attachmentId = $y->_attachmentId;
                    $attachinfo = $attachment_model->find(array('condition' => "attachmentid = $attachmentId"));
                    array_push($attachmentArray, array("subbusinessId" => $y->subbusinessId, "attachmentname" => $attachinfo->attachmentname, "printNumber" => $y->printNumbers, "paidMoney" => $y->paidMoney, "printSet" => $y->printSet, "payType" => $y->payType, "isPay" => $y->isPay, "status" => $y->status, "marchineId" => $y->marchineId, "isrefund" => $y->isrefund));
                }
            }
            $this->renderPartial('orderDetail', array("businessid_info" => $businessid_info, "attachmentArray" => $attachmentArray, "username" => $username, "integration" => $integration, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    /*     * ************** 订单列表 end************** */
    /*     * ************** 支付宝退款 start************** */

    public function actionrefund_zfb($refundIdd) {
//        $refundIdd = $_POST['refundIdd'];
        $refundIdd = explode(',', $refundIdd); //ID 1,2,3,4

        if (isset($refundIdd)) {
            $refund_model = refund::model();
            $subbusiness_model = subbusiness::model();
            $business_model = business::model();
            $detail_datas = "";
            require_once("refundlib/alipay.config.php");
            require_once("refundlib/alipay_submit.class.php");
            foreach ($refundIdd as $k => $l) {
                $refund_info = $refund_model->find("refundId = $l");
                if (isset($refund_info->subbusinessId)) {
                    $subbusiness_info = $subbusiness_model->find("subbusinessId = $refund_info->subbusinessId");
                    $business_info = $business_model->find("orderId = $refund_info->_orderId");
                    if ($subbusiness_info->status != 1) {
                        if ($refund_info->tborderId == "" && $business_info->trade_no != "") {
//                            $tborderId = $business_info->trade_no;
                            $refund_info->tborderId = $business_info->trade_no;
                            $refund_info->save();
                        }
                        $tborderId = $refund_info->tborderId;
                        $detail_datas .= $tborderId . "^" . $refund_info->money . "^" . "协商退款#";
                    } else {
                        //$refund_model->deleteByPk($l);
                    }
                } else {
                    $detail_datas .= $refund_info->tborderId . "^" . $refund_info->money . "^" . "协商退款#";
                }
            }
            /*             * ************************请求参数************************* */

//服务器异步通知页面路径
            $notify_url = "http://admin.cqutprint.com/refundalipay/notify_url.php";
//需http://格式的完整路径，商户网关地址不允许加?id=123这类自定义参数
//卖家支付宝帐户
            $seller_email = "cqutprint@aliyun.com";
//必填
//退款当天日期
            date_default_timezone_set('PRC');
            $refund_date = date('Y-m-d H:i:s');
//必填，格式：年[4位]-月[2位]-日[2位] 小时[2位 24小时制]:分[2位]:秒[2位]，如：2007-10-01 13:13:13
//批次号
            $verificationCode = mt_rand(1000000000, 9999999999); //验证码
            $batch_no = date('Ymd') . $verificationCode;
//必填，格式：当天日期[8位]+序列号[3至24位]，如：201008010000001
//退款笔数
            $batch_num = count($refundIdd);
//必填，参数detail_data的值中，“#”字符出现的数量加1，最大支持1000笔（即“#”字符出现的数量999个）
//退款详细数据
            $detail_data = substr($detail_datas, 0, strlen($detail_datas) - 1);
//必填，具体格式请参见接口技术文档


            /*             * ********************************************************* */

//构造要请求的参数数组，无需改动
            $parameter = array(
                "service" => "refund_fastpay_by_platform_pwd",
                "partner" => trim($alipay_config['partner']),
                "notify_url" => $notify_url,
                "seller_email" => $seller_email,
                "refund_date" => $refund_date,
                "batch_no" => $batch_no,
                "batch_num" => $batch_num,
                "detail_data" => $detail_data,
                "_input_charset" => trim(strtolower($alipay_config['input_charset']))
            );

//建立请求
            $alipaySubmit = new AlipaySubmit($alipay_config);
            $html_text = $alipaySubmit->buildRequestForm($parameter, "get", "确认");
//            echo $html_text;
            $this->renderPartial('alipay', array("html_text" => $html_text));
        } else
            echo '请返回';
    }

    /*     * ************** 支付宝退款end ************** */

    /*     * ************** 微信退款 start ******************* */

    public function actionrefund_wx() {
        header("Content-type:text/html;charset=utf-8");
        $refundIdd = $_POST['refundIdd'];

        if (isset($refundIdd)) {
            $refund_model = refund::model();
            $subbusiness_model = subbusiness::model();
            $business_model = business::model();

            $refundIdd = explode(',', $refundIdd); //ID 1,2,3,4
            $error_info = "";
            foreach ($refundIdd as $k => $l) {
                $addTime = date('y-m-d H:i:s', time());
                $refund_info = $refund_model->find("refundId = $l");
                if (isset($refund_info->subbusinessId)) {
                    $subbusinessId = $refund_info->subbusinessId;
                    $subbusiness_info = $subbusiness_model->find("subbusinessId = $subbusinessId");
                    if ($subbusiness_info->status != 1) {
                        $business_info = $business_model->find("orderId = $refund_info->_orderId");
                        if (count($business_info) > 0) {
                            $medium = $business_info->medium;
                            $out_refund_no = $refund_info->_orderId;
                            $out_trade_no = $out_refund_no;
                            $out_refund_no_rand = md5(time() . mt_rand(0, 1000));
                            if ($refund_info->tborderId == "" && $business_info->trade_no != "") {
                                $refund_info->tborderId = $business_info->trade_no;
                                $refund_info->save();
                            }
                            $tbtborderId = $refund_info->tborderId;
                            $trade_no_info = $refund_model->findAll("tborderId=$tbtborderId and statue=1");
                            if (count($trade_no_info) > 0) {
                                $out_refund_no = $out_refund_no_rand;
                            }
                            $transaction_id = $business_info->trade_no;
                            $totalPay = ($business_info->paidMoney) * 100;
                            $money = ($refund_info->money) * 100;
                            if ($tbtborderId != "" || $transaction_id != "") {
                                if (isset($medium)) {
                                    if ($medium == 2) {
                                        include_once("WxPayAppHelper/WxPayPubHelper.php");
//使用退款接口
                                        $refund_pub = new Refund_App();
//设置必填参数
//appid已填,商户无需重复填写
//mch_id已填,商户无需重复填写
//noncestr已填,商户无需重复填写
//sign已填,商户无需重复填写
                                        $refund_pub->setParameter("transaction_id", "$transaction_id"); //商户订单号
                                        $refund_pub->setParameter("out_refund_no", "$out_refund_no"); //商户退款单号
                                        $refund_pub->setParameter("total_fee", "$totalPay"); //总金额
                                        $refund_pub->setParameter("refund_fee", "$money"); //退款金额
                                        $refund_pub->setParameter("op_user_id", WxPayConf_pub::MCHID); //操作员
//调用结果
                                        $return = $refund_pub->getResult();
                                        if (isset($return["err_code_des"])) {
                                            $error_code = $return["err_code_des"];
                                        } else {
                                            $error_code = "\u65e0\u0065\u0072\u0072\u005f\u0063\u006f\u0064\u0065\u005f\u0064\u0065\u0073"; //无err_code_des
                                        }
                                        if (isset($return["result_code"])) {
                                            if ($return["result_code"] == "FAIL") {
                                                file_put_contents('C:\refundS.txt', "[" . $addTime . "]: APP端" . json_encode($return) . "--unicode转UTF-8 -->" . $this->unicode2utf8($error_code) . PHP_EOL, FILE_APPEND);
                                            } else if ($return['result_code'] == "FAIL") {
                                                file_put_contents('C:\refundS.txt', "[" . $addTime . "]: APP端" . json_encode($return) . "--unicode转UTF-8 -->" . $this->unicode2utf8($error_code) . PHP_EOL, FILE_APPEND);
                                            }
                                        }
                                    } else if ($medium == 1) {
                                        $return = $this->wxintf($out_trade_no, $out_refund_no, $totalPay, $money);
                                        if (isset($return["err_code_des"])) {
                                            $error_code = $return["err_code_des"];
                                        } else {
                                            $error_code = "\u65e0\u0065\u0072\u0072\u005f\u0063\u006f\u0064\u0065\u005f\u0064\u0065\u0073"; //无err_code_des
                                        }
                                        if ($return['result_code'] == "FAIL") {
                                            file_put_contents('C:\refundS.txt', "[" . $addTime . "]: WEB端" . json_encode($return) . "--unicode转UTF-8 -->" . $this->unicode2utf8($error_code) . PHP_EOL, FILE_APPEND);
                                        }
//                                        if ($return['result_code'] != 'SUCCESS') {
//                                            $errorInfo = $this->unicode2utf8($return["err_code_des"]);
//                                            file_put_contents('C:\refundS.txt', "[" . $addTime . "]: " . json_encode($return) . "--unicode转UTF-8 -->" . $errorInfo . PHP_EOL, FILE_APPEND);
//                                            if ($return['err_code_des'] == '\u8ba2\u5355\u72b6\u6001\u9519\u8bef'|| '\u540c\u4e00\u4e2aout_refund_no\u9000\u6b3e\u91d1\u989d\u8981\u4e00\u81f4') {
//                                                $out_refund_no_que = md5(time() . mt_rand(0, 1000));
//                                                $return = $this->wxintf($out_trade_no, $out_refund_no_que, $totalPay, $money);
//                                            }
//                                        }
                                    } else if ($medium == 3) {
                                        $return = $this->wxintf($out_trade_no, $out_refund_no, $totalPay, $money);
                                        if (isset($return["err_code_des"])) {
                                            $error_code = $return["err_code_des"];
                                        } else {
                                            $error_code = "\u65e0\u0065\u0072\u0072\u005f\u0063\u006f\u0064\u0065\u005f\u0064\u0065\u0073"; //无err_code_des
                                        }
                                        if ($return['result_code'] == "FAIL") {
                                            file_put_contents('C:\refundS.txt', "[" . $addTime . "]:  微信公众号" . json_encode($return) . "--unicode转UTF-8 -->" . $this->unicode2utf8($error_code) . PHP_EOL, FILE_APPEND);
                                        }
                                    }
                                    if ($return['return_code'] == 'SUCCESS' && $return['result_code'] == 'SUCCESS') {
                                        $refund_info->statue = 1;
                                        $refund_info->refundTime = $addTime;
//                                        $refund_info->info = json_encode($return);
                                        $subbusiness_info->isrefund = 1;
                                        $subbusiness_info->status = 3;
                                        $businessId = $subbusiness_info->_businessId;
                                        $subbusiness_same_info = $subbusiness_model->findAll("_businessId=$businessId and subbusinessId!=$subbusinessId");
                                        $sub_flag = false;
                                        $ref_flag = false;
                                        if (count($subbusiness_same_info) > 0) {
                                            foreach ($subbusiness_same_info as $value) {
                                                if ($value->isrefund == 1 || $value->isrefund == 0) {
                                                    $sub_flag = true;
                                                } else {
                                                    $sub_flag = false;
                                                    break;
                                                }
                                                if ($value->status == 1) {
                                                    $ref_flag = false;
                                                    break;
                                                } else {
                                                    $ref_flag = true;
                                                }
                                            }
                                            if ($sub_flag) {
                                                $business_info->isrefund = 1;
                                                if ($ref_flag) {
                                                    $business_info->status = 3;
                                                }
                                                $business_info->update();
                                            }
                                        }
                                        if ($refund_info->update() && $subbusiness_info->update()) {
                                            $error_info .= "ID为" . $l . ":退款成功！";
                                        } else {
                                            $error_info.= "*ID为" . $l . "-退款失败*";
                                        }
                                    } else {
                                        $error_info.= "*退款ID为" . $l . "退款发生错误-原因为" . $this->unicode2utf8($error_code) . "*"; //.$this->unicode2utf8($error_code)
                                    }
                                }
                            } else {
                                $error_info.= "*未找到退款ID为" . $l . "的微信支付订单号*";
                            }
                        } else {
                            $error_info.= "*未找到退款ID为" . $l . "的订单*";
                        }
                    } else {
                        //$refund_info->delete();
                        $error_info.= "*退款ID为" . $l . "的子订单已打印成功*";
                    }
                } else {
                    $error_info.= "*未找到退款ID为" . $l . "的子订单*";
                }
            }
            $json = '{"info":"' . $error_info . '"}';
            echo $json;
        }
    }

    // web,公众号refund 
    public function wxintf($out_trade_no, $out_refund_no, $totalPay, $money) {
        include_once("WxPayPubHelper/WxPayPubHelper.php");
        //使用退款接口
        $refund_pub = new Refund_pub();
        //设置必填参数
        //appid已填,商户无需重复填写
        //mch_id已填,商户无需重复填写
        //noncestr已填,商户无需重复填写
        //sign已填,商户无需重复填写
        $refund_pub->setParameter("out_trade_no", "$out_trade_no"); //商户订单号
        $refund_pub->setParameter("out_refund_no", "$out_refund_no"); //商户退款单号
        $refund_pub->setParameter("total_fee", "$totalPay"); //总金额
        $refund_pub->setParameter("refund_fee", "$money"); //退款金额
        $refund_pub->setParameter("op_user_id", WxPayConf_public::MCHID); //操作员
        //调用结果
        $return = $refund_pub->getResult();
        return $return;
    }

    /*     * ************** 微信退款 ******************* */

    /*     * ************** 积分退款 start ******************* */

    public function actionrefund_points() {
        $refundIdd = $_POST['refundIdd'];
        if (isset($refundIdd)) {
            $refund_model = refund::model();
            $subbusiness_model = subbusiness::model();
            $business_model = business::model();
            $user_model = user::model();
            $points_model = new integralDetails();
            $addTime = date('y-m-d H:i:s', time());
            $refundIdd = explode(',', $refundIdd); //ID 1,2,3,4
            $flag = true;
            $print_flag = true;
            foreach ($refundIdd as $k => $l) {
                $refund_info = $refund_model->find("refundId = $l");
                if ($refund_info->statue != 1) {
                    if (isset($refund_info->subbusinessId)) {
                        $subbusinessId = $refund_info->subbusinessId;
                        $subbusiness_info = $subbusiness_model->find("subbusinessId =$subbusinessId");
                        if ($subbusiness_info->status != 1) {
                            $userid = $refund_info->_userId;
                            $points = $refund_info->Integral;
                            $points_model->_userid = $userid;
                            $points_model->addIntegral = $points;
                            $points_model->happentime = $addTime;
                            $points_model->happenInfo = "积分退款";

                            $user_info = $user_model->findByPk($userid);
                            if (count($user_info) > 0) {
                                $user_info->integration += $refund_info->Integral;
                            }
                            $refund_info->statue = 1;
                            $refund_info->refundTime = $addTime;
                            $subbusiness_info->isrefund = 1;
                            $subbusiness_info->status = 3;
                            $business_info = $business_model->find("orderId = '$refund_info->_orderId'");
                            if (isset($business_info)) {
                                $businessId = $subbusiness_info->_businessId;
                                $subbusiness_same_info = $subbusiness_model->findAll("_businessId=$businessId and subbusinessId!=$subbusinessId");
                                $sub_flag = false;
                                if (count($subbusiness_same_info) > 0) {
                                    foreach ($subbusiness_same_info as $value) {
                                        if ($value->status == 1) {
                                            $sub_flag = true;
                                        } else {
                                            $sub_flag = false;
                                            break;
                                        }
                                    }
                                    if ($sub_flag) {
                                        $business_info->isrefund = 1;
                                        $business_info->status = 3;
                                        $business_info->update();
                                    }
                                }
                            }
                            if ($refund_info->save() && $subbusiness_info->save() && $points_model->save() && $user_info->save()) {
                                
                            } else {
                                $flag = FALSE;
                            }
                        } else if ($subbusiness_info->status == 1) {
                            //$count = $refund_info->delete();
                            //if ($count > 0) {
                            //} else {
                            $print_flag = FALSE;
                            $flag = FALSE;
                            //}
                        }
                    } else if (isset($refund_info->tborderId)) {
                        $tran_no = $refund_info->tborderId;
                        $printpaytemp_model = printpaytemp::model();
                        $user_model = user::model();
                        $points_model = new integralDetails();
                        $printpaytemp_info = $printpaytemp_model->find("trade_no='$tran_no'");
                        $phone = $printpaytemp_info->buyer_logon_id;
                        $user_info = $user_model->find("phone='$phone'");
                        if (count($user_info) > 0) {
                            $userid = $user_info->userid;
                            $points = $refund_info->Integral;
                            $points_model->_userid = $userid;
                            $points_model->addIntegral = $points;
                            $points_model->happentime = $addTime;
                            $points_model->happenInfo = "积分退款";
                            $user_info1 = $user_model->findByPk($userid);
                            if (count($user_info1) > 0) {
                                $user_info1->integration += $refund_info->Integral;
                            }
                            $refund_info->statue = 1;
                            $refund_info->refundTime = $addTime;
                        }
                        if ($refund_info->save() && $points_model->save() && $user_info1->save()) {
                            
                        } else {
                            $flag = FALSE;
                        }
                    } else {
                        $sessionId = $refund_info->_sessionId;
                        $prbusiness_info = prbusiness::model()->find("sessionId='$sessionId");
                        $tran_no = $prbusiness_info->trade_no;
                        $user_model = user::model();
                        $points_model = new integralDetails();
                        $printpaytemp_info = $printpaytemp_model->find("trade_no='$tran_no'");
                        if (isset($printpaytemp_info)) {
                            $phone = $printpaytemp_info->buyer_logon_id;
                            $user_info = $user_model->find("phone='$phone'");
                            if (count($user_info) > 0) {
                                $userid = $user_info->userid;
                                $points = $refund_info->Integral;
                                $points_model->_userid = $userid;
                                $points_model->addIntegral = $points;
                                $points_model->happentime = $addTime;
                                $points_model->happenInfo = "积分退款";
                                $user_info1 = $user_model->findByPk($userid);
                                if (count($user_info1) > 0) {
                                    $user_info1->integration += $refund_info->Integral;
                                }
                                $refund_info->statue = 1;
                                $refund_info->refundTime = $addTime;
                            }
                            if ($refund_info->save() && $points_model->save() && $user_info1->save()) {
                                
                            } else {
                                $flag = FALSE;
                            }
                        } else {
                            $flag = FALSE;
                        }
                    }
                }
            }
            if ($flag) {
                $json = '{"data":"success"}';
                echo $json;
            } else if (!$print_flag) {
                $json = '{"data":"printed"}';
                echo $json;
            } else {
                $json = '{"data":"false"}';
                echo $json;
            }
        }
    }

    // unicode 转UTF-8 
    public function unicode2utf8($str) {
        if (!$str)
            return $str;
        $decode = json_decode($str);
        if ($decode)
            return $decode;
        $str = '["' . $str . '"]';
        $decode = json_decode($str);
        if (count($decode) == 1) {
            return $decode[0];
        }
        return $str;
    }

    /*     * ************** 积分退款 ******************* */
}
