<?php

include 'BaseController.php';

class problemAdminController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function filters() {
        return array(
            'problem + problem'
        );
    }

    public function filterproblem($filterChain) {
        $this->checkAccess("问题", $filterChain);
    }

    public function actionproblem() {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            $problem_mode = problem::model();
            $problemtype_mode = problemtype::model();
            $problem_info = $problem_mode->findAll(array('order' => "addtime DESC"));
            $problemtype_info = $problemtype_mode->findAll();
            $this->renderPartial('problem', array('problem_info' => $problem_info, 'problemtype_mode' => $problemtype_mode, 'problemtype_info' => $problemtype_info, "leftContent" => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

// 新增问题
    public function actionaddprblem() {
        if (isset(Yii::app()->session['adminuser'])) {
            //header("Content-type: text/html; charset=utf-8");
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();
            if (isset($_POST['name'])) {
                $problem_model = new problem();
                $problem_model->problem_name = $_POST['name'];
                $problem_model->problem_describe = $_POST['descrp'];
                $problem_model->problem_type = $_POST['type'];
                $problem_model->solution = $_POST['solution'];
                $problem_model->author = $_POST['author'];
                $addTime = date('y-m-d H:i:s', time());
                $problem_model->addtime = $addTime;
                if ($problem_model->save()) {
                    echo "<script>parent.$('#save_info').text('保存成功！').css('color','red');</script>";
                }
            } else {
                $problem_mode = problem::model();
                $problemtype_mode = problemtype::model();
                $problem_info = $problem_mode->findAll(array('order' => "addtime DESC"));
                $problemtype_info = $problemtype_mode->findAll();
                $this->renderPartial('addproblem', array('leftContent' => $leftContent, 'recommend' => $recommend, 'problem_info' => $problem_info, 'problemtype_info' => $problemtype_info));
            }
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

    //不上传OSS的问题编辑
    public function actioneditproblem_nofile() {
        if (isset($_POST['name'])) {
            $Id = $_POST['id'];
            $name = $_POST['name'];
            $descrp = $_POST['descrp'];
            $type = $_POST['type'];
            $solution = $_POST['solution'];
            $author = $_POST['author'];
            $problem_model = problem::model()->find("problemid=$Id");
            $problem_model->problem_name = $name;
            $problem_model->problem_describe = $descrp;
            $problem_model->problem_type = $type;
            $problem_model->solution = $solution;
            $problem_model->author = $author;
            $addTime = date('y-m-d H:i:s', time());
            $problem_model->addtime = $addTime;
            if ($problem_model->save()) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        }
    }

//问题详情
    public function actionprobleminfo($Id) {
        if (isset(Yii::app()->session['adminuser'])) {
            $leftContent = $this->getLeftContent();
            $recommend = $this->getrecommend();

            $problemId = $Id;
            $problem_model = problem::model();
            $problemtype_model = problemtype::model();

            $problem_info = $problem_model->find(array("condition" => "problemid=$problemId"));
            $type = $problem_info->problem_type;
            $typename = $problemtype_model->find("problemtype_id=$type")->problemtype_name;
            $this->renderPartial('problemDetail', array("problem_info" => $problem_info, "typename" => $typename, 'leftContent' => $leftContent, 'recommend' => $recommend));
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//删除问题
    public function actiondelproblem() {
        if (isset(Yii::app()->session['adminuser'])) {
            $problemId = $_POST['problemId'];

            $problem_model = problem::model();
            if (count($problem_model->deleteByPk($problemId)) > 0) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        } else {
            $this->redirect('./index.php?r=default/index');
        }
    }

//下载附件
    public function actiondownload($id) {
        $problem_model = problem::model();
        $problemId = $id;
        $problem_info = $problem_model->find(array('condition' => "problemid = '$problemId'"));
        if (count($problem_info) > 0) {
            $name = $problem_info->attachment_name;
            $truename = $problem_info->filepath;

            require_once './oss/samples/Common.php';
            $bucket = "porunoss";

            $timeout = 3600;
            $ossClient = Common::getOssClient();
            $object = $truename;
            $filePath = $ossClient->signUrl($bucket, $object, $timeout);

            header('Content-Type:application/octet-stream'); //文件的类型
            Header("Accept-Ranges: bytes");
            header('Content-Disposition:attachment;filename = "' . $name . '"'); //下载显示的名字
            ob_clean();
            flush();
            readfile($filePath);
            exit();
        } else {
            $this->redirect('./index.php?r=problemAdmin/problem');
        }
    }

}
