<?php

include 'BaseController.php';

class authController extends baseController {

    public function getrecommend() {//获取CSS JS引用
        $common = new commonController();
        $url = $common->webUrl . "?r=recommend/recommend";
        $structContent = file_get_contents($url);
        return $structContent;
    }

    public function filters() {
        return array(
            'Role + Role',
            'deleteRole + deleteRole',
            'assignRole + assignRole',
            'addAuth + addAuth',
            'addRole + addRole',
            'editRole + editRole',
            'addAdmin + addAdmin',
            'editAdmin + modify',
        );
    }

    public function filterRole($filterChain) {
        $this->checkAccess("角色与权限", $filterChain);
    }

    public function filterassignRole($filterChain) {
        $this->checkAccess("权限分配", $filterChain);
    }

    public function filteraddAuth($filterChain) {
        $this->checkAccess("创建权限", $filterChain);
    }

    public function filteraddRole($filterChain) {
        $this->checkAccess("创建角色", $filterChain);
    }

    public function filterdeleteRole($filterChain) {
        $this->checkAccess("删除角色", $filterChain);
    }

    public function filtereditRole($filterChain) {
        $this->checkAccess("编辑角色", $filterChain);
    }

    public function filteraddAdmin($filterChain) {
        $this->checkAccess("添加管理员", $filterChain);
    }

    public function filtereditAdmin($filterChain) {
        $this->checkAccess("编辑管理员", $filterChain);
    }

    public function actionRole() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $role_mode = role :: model();
        $role_info = $role_mode->findAll();
        //删除功能权限判断
        $admin = Yii::app()->session['adminuser'];
        $admin_model = administrator::model();
        $assign_model = assignment::model();
        $item_model = item::model();
        $roleId = $admin_model->find("username='$admin'")->_roleid;
        $assign = $assign_model->findAll("_roleId='$roleId'");
        $flagRole = "false";
        foreach ($assign as $value) {
            $id = $value->_itemId;
            $assign_info = $item_model->find("itemId ='$id'");
            $itemName = $assign_info->itemName;
            if ($itemName == "删除角色") {
                $flagRole = "true";
                break;
            }
        }

        $this->renderPartial('role', array('role_info' => $role_info, 'flagRole' => $flagRole, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    /*
     * 删除角色
     */

    public function actiondeleteRole() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $role_mode = role :: model();
            $iteminfo_model = assignment::model();
            if ((count($role_mode->deleteAll("roleId=" . $id)) && count($iteminfo_model->deleteAll("_roleId=" . $id)) ) > 0) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        }
    }

    /*
     * 分配角色
     */

    public function actionassignRole() {
        //管理员
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $admin_model = administrator::model();
        $admin_info = $admin_model->findAll("username!='admin'");
        $store_model = store::model();
        $store_info = $store_model->findAll();
        //角色名称
        $role = role::model();
        $role_info = $role->findAll("rolename!='超级管理员'");

        $this->renderPartial('assignRole', array('admin_info' => $admin_info, 'role_info' => $role_info, 'store_info' => $store_info, 'leftContent' => $leftContent, 'recommend' => $recommend)); //'admin_info' => $admin_info, 'role_info' => $role_info, 'page_list' => $page_list,
    }

    public function actionassignData() {
//        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' : false)) {
//            $this->redirect('./index.php?r=default/index');
//        }
        $admin_model = administrator::model();
        $role = role::model();
        $admin_info = $admin_model->findAll();
        $role_info = $role->findAll();
        $i = 0;
        foreach ($role_info as $v) {
            $roleName = $v->rolename;
            $data1[] = $roleName;
        }
        foreach ($admin_info as $value) {
            $i++;
            $name = $value->username;
            $data2[] = array(
                "id" => $i,
                "name" => $name,
                "role" => $data1
            );
        }
        $data3 = array(
            "draw" => 0,
            "recordsTotal" => $i,
            "recordsFiltered" => $i,
            'data' => $data2,
        );
        echo $data = json_encode($data3);
    }

    public function actionaddAuth() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $item_model = item::model();
        $item_info = $item_model->findAll("parentItem =0");
        $this->renderPartial('addAuth', array('item_info' => $item_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    public function actionsaveAuth() {//保存新增权限
        $item_model = new item;
        $authitem_model = item::model();
        if (isset($_POST['parentItem1'])) {
            $auth = $_POST['parentItem1']; //新增权限
            $authchild = $_POST['childTtem1'];
            $itemName1 = $authitem_model->find("itemName = '$auth'");
            if (!$itemName1) {
                $item_model = new item;
                $item_model->itemName = $auth;
                $item_model->parentItem = 0;
                if ($item_model->save()) {
                    if ($authchild != NULL) {
                        $itemInfo = $authitem_model->find("itemName = '$auth'");
                        $itemId = $itemInfo->itemId;
                        $itemchild_model = new item;
                        $itemchild_model->itemName = $authchild;
                        $itemchild_model->parentItem = $itemId;
                        if ($itemchild_model->save()) {
                            $json = '{"data":"success","datachild":"success"}';
                            echo $json;
                        } else {
                            $json = '{"data":"success","datachild":"false"}';
                            echo $json;
                        }
                    } else {
                        $json = '{"data":"success","datachild":"none"}';
                        echo $json;
                    }
                } else {
                    $json = '{"data":"false","datachild":"false"}';
                    echo $json;
                }
            } else {
                $json = '{"data":"namehad"}';
                echo $json;
            }
        }
        if (isset($_POST['childTtem'])) {  //已有权限添加子权限
            $parentauth = $_POST['parentItem'];
            $childauth = $_POST['childTtem'];
            $count = count($authitem_model->find("itemName='$childauth'"));
            if ($count > 0) {
                $json = '{"data":"namehad"}';
                echo $json;
            } else {
                $item_model1 = new item;
                $itemName = $authitem_model->find("itemName = '$parentauth'");
                $parentId = $itemName->itemId;
                $item_model1->itemName = $childauth;
                $item_model1->parentItem = $parentId;
                if ($item_model1->save()) {
                    $json = '{"data":"success"}';
                    echo $json;
                } else {
                    $json = '{"data":"false"}';
                    echo $json;
                }
            }
        }
    }

//
//    /*
//     * 为管理员分配角色
//     */
//
    public function actionassign() {
        if (isset($_POST['adminID']) && isset($_POST['roleID'])) {
            $administratorid = $_POST['adminID'];
            $_roleID = $_POST['roleID'];
            $admin_model = administrator::model();
            $admin_info = $admin_model->find("administratorid=" . $administratorid);
            $admin_info->_roleid = $_roleID;
            if ($admin_info->save()) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        }
    }

    /*
     * 删除管理员
     */

    public function actionassignDel() {
        if (isset($_POST['adminId'])) {
            $administratorid = $_POST['adminId'];
            $admin_model = administrator::model();
            $admin_info = $admin_model->find("administratorid=" . $administratorid);
            $admin_info->delete();
            agent_printor::model()->deleteAll(array('condition' => "administrator_id = $administratorid"));
            if (count($admin_info->delete()) > 0) {
                $json = '{"data":"success"}';
            } else {
                $json = '{"data":"false"}';
            }
            echo $json;
        }
    }

    /*
     * 创建角色
     */

    public function actionaddRole() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $role_model = role::model();
        $item_model = item::model();
        $authitem_info = $item_model->findAll("parentItem = 0");
        $this->renderPartial('addRole', array('authitem_info' => $authitem_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    /*
     * 
     */

    public function actionsaveRole() { //保存创建的角色
        $authrole_model2 = new role();
        if (isset($_POST)) {
            $authrole = $_POST['role'];
            $authrole_model2->rolename = $authrole;
            $authrole_model2->description = $authrole;
            $authrole_model2->save();
            $authrole_model = role :: model();
            $authItem_model = item::model();
            $role_info = $authrole_model->find("rolename='$authrole'");
            $flag = TRUE;
            $chidflag = true;
            if (isset($_POST['authparent'])) {  //父权限
                foreach ($_POST['authparent'] as $value) {
                    $authassignment_model = new assignment;
                    $role_id = $role_info->roleId;
                    $authassignment_model->_roleId = $role_id;
                    $item_info = $authItem_model->find("itemName='$value'");
                    $authassignment_Pid = $item_info->itemId;
                    $authassignment_model->_itemId = $authassignment_Pid;
                    if (!$authassignment_model->save() > 0) {
                        $flag = FALSE;
                    }
                }
            }
            if (isset($_POST['authchild'])) { // 子权限
                foreach ($_POST['authchild'] as $value) {
                    $authassignment_model1 = new assignment;
                    $role_id = $role_info->roleId;
                    $authassignment_model1->_roleId = $role_id;
                    $item_info = $authItem_model->find("itemName='$value'");
                    $authassignment_Cid = $item_info->itemId;
                    $authassignment_model1->_itemId = $authassignment_Cid;
                    if (!$authassignment_model1->save()) {
                        $chidflag = FALSE;
                    }
                }
            }
            if ($flag || $chidflag) {
                echo "<script>window.location.href='./index.php?r=auth/role';</script>";
            } else {
                echo "<script>alert('创建失败！');window.location.href='./index.php?r=auth/role';</script>";
            }
        }
    }

    /*
     * 编辑角色
     */

    public function actioneditRole($roleid) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $role_mode = role :: model();
        $authitem_model = item::model();
        $assignment = assignment::model();
        $authrole_info = $role_mode->find("roleId='$roleid'");
        $assignment_info = $assignment->findAll("_roleId='$roleid'");
        $assign_info = $authitem_model->findAll("parentItem=0");
        $auth_name = array();
        foreach ($assign_info as $value) {
            $id = $value->itemId;
            $name = $value->itemName;
            foreach ($assignment_info as $auth_info) {
                $auth_id = $auth_info->_itemId;
                $assig_id = $authitem_model->find("itemId='$auth_id'");
                $parent_id = $assig_id->parentItem;
                $parent_name = $assig_id->itemName;
                if ($parent_id == 0 && $parent_name == $name) {
                    $auth_name[$id][0] = $assig_id->itemName;
                } else if ($parent_id == $id) {
                    $auth_name[$id][$auth_id] = $assig_id->itemName;
                }
            }
        }
        $this->renderPartial('editRole', array('authrole_info' => $authrole_info, 'auth_name' => $auth_name, 'assign_info' => $assign_info, 'roleid' => $roleid, 'leftContent' => $leftContent, 'recommend' => $recommend));
    }

    public function actioneditupdateRole() {//编辑角色数据保存
        $roleid = $_POST['roleid'];
        $authrole_model2 = role::model();
        $authitem_model = new item;
        $authroleid = $roleid;
        $authassignment_model = assignment::model();
        $item_model = item::model();
        $count = $authassignment_model->deleteAll("_roleId = '$authroleid'");
        if (isset($_POST)) {
            $roleId = $_POST["role"];
            $auth_model_info = $authrole_model2->find("roleId = '$authroleid'");
            $roleName = $_POST["role"];
            $auth_model_info->roleId = $_POST["roleid"];
            $auth_model_info->rolename = $roleName;
            $auth_model_info->description = $roleName;
            $count = $auth_model_info->update(array('roleId', 'rolename', 'description'));
            $authrole_model = role :: model();
            $flag = TRUE;
            if (isset($_POST['authparent'])) { //父权限
                foreach ($_POST['authparent'] as $value) {
                    $authupdate_model = new assignment;
                    $parent_info = $item_model->find("itemName='$value'");
                    $id = $parent_info->itemId;
                    $authupdate_model->_roleId = $authroleid;
                    $authupdate_model->_itemId = $id;
                    if (!$authupdate_model->save()) {
                        $flag = FALSE;
                    }
                }
            }
            if (isset($_POST['authchild'])) {//子权限
                foreach ($_POST['authchild'] as $value) {
                    $authupdate_model1 = new assignment;
                    $parent_info = $item_model->find("itemName='$value'");
                    $id = $parent_info->itemId;
                    $authupdate_model1->_roleId = $authroleid;
                    $authupdate_model1->_itemId = $id;
                    if (!$authupdate_model1->save()) {
                        $flag = FALSE;
                    }
                }
            }
            if ($flag) {
                echo "<script>parent.$('.save_lable').text('保存成功！').css('color','red');</script>";
            } else {
                echo "<script>parent.$('.save_lable').text('保存失败！').css('color','red');</script>";
            }
        }
    }

    /*
     * 添加管理员 
     */

    public function actionaddAdmin() {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        if (isset($_POST["username"])) {
            $username = addslashes($_POST["username"]);
            $password = md5($_POST["password"]);
            $phone = $_POST["phone"];
            $QQ = $_POST["QQ"];
            $roleId = $_POST["role"];
            $storeId = $_POST["schoolName"];
            $check = $_POST["check"];
            $machinename = $_POST["machinename"];
            $administrator_model = new administrator();
            $administrator_info = $administrator_model->find("username = '$username'");
            if (count($administrator_info) == 0) {
                $administrator_model->username = $username;
                $administrator_model->password = $password;
                $administrator_model->_roleid = $roleId;
                if ($storeId[0] == "multiselect-all") {
                    $administrator_model->_storeid = 0;
                } else {
                    $str = "";
                    for ($i = 0; $i < count($storeId); $i++) {
                        $str .= $storeId[$i] . ",";
                    }
                    $str = substr($str, 0, -1);
                    $administrator_model->_storeid = $str;
                }
                $administrator_model->phone = $phone;
                $administrator_model->QQ = $QQ;
                $administrator_model->addtime = date('Y-m-d H:i:s', time());
                if ($check == "yes") {
                    $administrator_model->is_agent = 1;
                } else {
                    $administrator_model->is_agent = 0;
                }
                if ($administrator_model->save()) {
                    if ($check == "yes") {
                        if ($machinename[0] == "multiselect-all") {
                            $i = 1;
                        } else {
                            $i = 0;
                        }
                        for ($i; $i < count($machinename); $i++) {
                            $agent_printor_model = new agent_printor();
                            $agent_printor_model->administrator_id = $administrator_model->administratorid;
                            $agent_printor_model->printor_id = $machinename[$i];
                            $agent_printor_model->save();
                        }
                    }
                    echo "<script>parent.$('#add_success').text('保存成功！');</script>";
                } else {
                    echo "<script>parent.$('#add_success').text('保存失败！');</script>";
                }
            } else {
                echo "<script>parent.$('#username_error').text('用户名已存在！');</script>";
            }
        } else {
//        if (isset($_POST["username"])) {
//            $administrator_info = $administrator_model->find(array('condition' => "username='$username' AND password='$password'"));
            $store_model = store::model();
            $role_model = role::model();
            $store_info = $store_model->findAll();
            $role_info = $role_model->findAll("rolename!='超级管理员'");
            $this->renderPartial('addAdmin', array('role_info' => $role_info, 'store_info' => $store_info, 'leftContent' => $leftContent, 'recommend' => $recommend));
        }
    }

    /*
     * 新增管理员时选择终端
     */

    public function actionsearchmachine() {
        $store_ids = $_POST["store_id"];
        if ($store_ids[0] == "multiselect-all") {
            $printor_info = printor::model()->findAll();
        } else {
            $strt = "";
            foreach ($store_ids as $k => $l) {
                $strt .= $l . ",";
            }
            $strt = substr($strt, 0, -1);
            $printor_info = printor::model()->findAll(array("condition" => "_storeId in ($strt)"));
        }

        $printor_infox = array(); //返回的数组
        foreach ($printor_info as $k => $v) {
            array_push($printor_infox, array("printorId" => $v->printorId, "printorName" => $v->printorName));
        }
        $printor_infox = json_encode($printor_infox);
        echo $printor_infox;
    }

    /*
     * 编辑管理员
     */

    public function actionmodify($adminID) {
        $leftContent = $this->getLeftContent();
        $recommend = $this->getrecommend();
        $administrator_model = administrator::model();
        if (isset($_POST["administratorid"])) {
            $administratorid = $_POST["administratorid"];
            $username = addslashes($_POST["username"]);
            $password = md5($_POST["password"]);
            $roleId = $_POST["role"];
            $storeId = $_POST["schoolName"];
            $phone = $_POST["phone"];
            $QQ = $_POST["QQ"];
            $check = $_POST["check"];
            $machinename = $_POST["machinename"];

            $administrator_model = administrator::model();

            $administrator_infos = $administrator_model->findAll("username = '$username' and administratorid != $administratorid");
            if (count($administrator_infos) == 0) {
                $administrator_info = $administrator_model->findByPk($administratorid);
                if ($username != "") {
                    $administrator_info->username = $username;
                }
                if ($_POST["password"] != "") {
                    $administrator_info->password = $password;
                }
                $administrator_info->_roleid = $roleId;
                if ($storeId[0] == "multiselect-all") {
                    $administrator_info->_storeid = 0;
                } else {
                    $str = "";
                    for ($i = 0; $i < count($storeId); $i++) {
                        $str .= $storeId[$i] . ",";
                    }
                    $str = substr($str, 0, -1);
                    $administrator_info->_storeid = $str;
                }
                if ($phone != "") {
                    $administrator_info->phone = $phone;
                }
                if ($QQ != "") {
                    $administrator_info->QQ = $QQ;
                }
                if ($check == "yes") {
                    $administrator_info->is_agent = 1;
                } else {
                    $administrator_info->is_agent = 0;
                }
                if ($administrator_info->save()) {
                    agent_printor::model()->deleteAll(array('condition' => "administrator_id = $administratorid"));
                    if ($check == "yes") {
                        if ($machinename[0] == "multiselect-all") {
                            $i = 1;
                        } else {
                            $i = 0;
                        }
                        for ($i; $i < count($machinename); $i++) {
                            $agent_printor_model = new agent_printor();

                            $agent_printor_model->administrator_id = $administratorid;
                            $agent_printor_model->printor_id = $machinename[$i];
                            $agent_printor_model->save();
                        }
                    }
                    echo "<script>parent.$('#save_success').text('保存成功！');</script>";
                } else {
                    echo "<script>parent.$('#save_success').text('保存失败！');</script>";
                }
            } else {
                echo "<script>parent.$('#username_error').text('用户名已存在！');</script>";
            }
        } else {
            $store_model = store::model();
            $role_model = role::model();
            $store_info = $store_model->findAll();
            $role_info = $role_model->findAll("rolename!='超级管理员'");
            $admin_info = $administrator_model->find("administratorid='$adminID'");
            $roleId = $admin_info->_roleid;
            $storeId = $admin_info->_storeid;
            $storeIds = explode(',', $storeId); //ID 1,2,3,4
            $str = "";
            foreach ($storeIds as $k => $l) {
                $str .= "'" . $l . "',";
            }
            $str = substr($str, 0, -1);
            $machineid = "";
            if ($admin_info->is_agent == 0) {//不是代理商
                $machineid = "0";
            } else {//是代理商
                $agent_printor_info = agent_printor::model()->findAll(array("condition" => "administrator_id =" . $adminID));
                foreach ($agent_printor_info as $k => $l) {
                    $machineid .= "'" . $l->printor_id . "',";
                }
                $machineid = substr($machineid, 0, -1);
            }
            $this->renderPartial('modify', array('is_agent' => $admin_info->is_agent, 'admin_info' => $admin_info, 'roleId' => $roleId, 'storeId' => $str, 'role_info' => $role_info, 'store_info' => $store_info, 'machineid' => $machineid, 'leftContent' => $leftContent, 'recommend' => $recommend));
        }
    }

}
