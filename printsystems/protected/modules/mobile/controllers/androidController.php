<?php

header('content-type:application/json;charset=utf8');

class androidController extends Controller {

    public function __construct() {
        date_default_timezone_set('PRC');
        ini_set('session.gc_maxlifetime', 180);
        ini_set("session.gc_divisor", 1);
        session_start();
        if (!isset($_SESSION['user'])) {
            $return = array('resultCode' => 400, 'resultDescription' => '您未登陆');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            exit();
        }
    }

    public function actionInitPayConfig() {
        $config_mode = androidConfig :: model();
        $config_info = $config_mode->find("partner=2088711636157511");
        if (count($config_info) > 0) {
            $partner = $config_info->partner;
            $privatekey = $config_info->privatekey;
            $accountant = $config_info->accountant;
            $config = array(
                'partner' => $partner,
                'accountant' => $accountant,
                'privatekey' => $privatekey
            );
            $return = array(
                'resultCode' => 200,
                'resultDescription' => '获取成功',
                'config' => $config
            );
        } else {
            $return = array(
                'resultCode' => 400,
                'resultDescription' => '获取失败',
            );
        }
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function actionIndex() {
        return;
    }

    public function actionLogout() {
        $sessionID = new CHttpCookie('PHPSESSID', session_id());
        Yii::app()->request->cookies['PHPSESSID'] = $sessionID;
        $return = array('resultCode' => 200, 'resultDescription' => '退出成功', 'user' => $_SESSION['user']);
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        session_destroy();
        return;
    }

    public function actionEditPwd() {
        if (!isset($_POST) || empty($_POST)) {
            $return = array('resultCode' => 401, 'resultDescription' => '密码不能为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $user_model = new user();
        $username = $_SESSION['user']['username'];
        $pwdold = isset($_POST['pwdold']) ? trim($_POST['pwdold']) : '';
        $pwd = isset($_POST['pwd']) ? trim($_POST['pwd']) : '';
        $pwd2 = isset($_POST['pwd2']) ? trim($_POST['pwd2']) : '';

        if ($pwd != $pwd2) {
            $return = array('resultCode' => 404, 'resultDescription' => '两次密码不一致');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $usernameR = $user_model->find(array('condition' => "username='$username'"));
        if ($usernameR) {
            if (md5($pwdold) != $usernameR->userpsw) {
                $return = array('resultCode' => 402, 'resultDescription' => '用户名或密码错误');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        } else {
            $return = array('resultCode' => 403, 'resultDescription' => '用户名不存在');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $usernameR->userpsw = md5($pwd);
        if ($usernameR->save()) {
            $return = array('resultCode' => 200, 'resultDescription' => '修改成功');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $return = array('resultCode' => 405, 'resultDescription' => '修改失败');
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    public function actionEditPhone() {
        if (!isset($_POST) || empty($_POST)) {
            $return = array('resultCode' => 401, 'resultDescription' => '手机号或验证码不能为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $username = $_SESSION['user']['username'];
        $pwdold = isset($_POST['pwdold']) ? trim($_POST['pwdold']) : '';
        $phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
        $code = isset($_POST['code']) ? trim($_POST['code']) : '';
        if (!isset($_SESSION['register']['phone']) || $phone == '' || $phone != $_SESSION['register']['phone']) {
            $return = array('resultCode' => 405, 'resultDescription' => '手机号不一致');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        if (!isset($_SESSION['register']['code']) || $code == '' || $code != $_SESSION['register']['code']) {
            $return = array('resultCode' => 406, 'resultDescription' => '验证码错误');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $returnjson = $this->checkphone($phone);
        if ($returnjson != null) {
            echo $returnjson;
            return;
        }
        $user_model = new user();
        $usernameR = $user_model->find(array('condition' => "username='$username'"));
        if ($usernameR) {
            if (md5($pwdold) != $usernameR->userpsw) {
                $return = array('resultCode' => 402, 'resultDescription' => '用户名或密码错误');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        } else {
            $return = array('resultCode' => 403, 'resultDescription' => '用户名不存在', 'username' => $username);
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }

        $usernameR->phone = $phone;
        if ($usernameR->save()) {
            Yii::app()->request->cookies['phone'] = new CHttpCookie('phone', $usernameR->phone);
            $return = array('resultCode' => 200, 'resultDescription' => '绑定成功');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $return = array('resultCode' => 407, 'resultDescription' => '修改失败');
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    public function actionUploadList() {
        $userid = $_SESSION['user']['userid'];
        $page = isset($_POST['page']) ? trim($_POST['page']) - 1 : 0;
        $pageSize = 10;
        $pageStart = $page * $pageSize;
        $pageEnd = $pageStart + $pageSize - 1;
        $attachment_model = attachment::model();
        $fileList = $attachment_model->findAll(array('condition' => "_userid=$userid and isdelete=1", 'order' => 'uploadtime DESC'));
        $pages = ceil(count($fileList) / $pageSize);
        $pageEnd = $pageEnd > count($fileList) - 1 ? count($fileList) - 1 : $pageEnd;
        if ($page > $pages) {
            $return = array('resultCode' => 401, 'resultDescription' => '超出总页数');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        if ($fileList != null) {
            $files = array();
            for ($i = $pageStart; $i <= $pageEnd; $i++) {
                $file = $fileList[$i];
                array_push($files, array('_attachmentId' => $file->attachmentid, 'filename' => $file->attachmentname,
                    'pages' => $file->filenumber, 'money' => $this->getMoney($file->filenumber), 'uploadtime' => $file->uploadtime));
            }
            $return = array('resultCode' => 200, 'resultDescription' => '获取文件列表成功', 'filelist' => $files, 'pages' => $pages);
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        } else {
            $return = array('resultCode' => 402, 'resultDescription' => '文件列表为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

    public function actionDelete() {//删除上传文件
        $attachmentid = isset($_POST['_attachmentid']) ? trim($_POST['_attachmentid']) : '';
        if ($attachmentid == '' || intval($attachmentid) <= 0) {
            $return = array('resultCode' => 401, 'resultDescription' => '文件ID非法');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $userid = $_SESSION['user']['userid'];
        $attachment_model = attachment::model();
        $attachment_info = $attachment_model->find("attachmentid='$attachmentid' and _userid='$userid'");
        if ($attachment_info == null) {
            $return = array('resultCode' => 402, 'resultDescription' => '文件不存在');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $attachment_info->isdelete = 0;
        if ($attachment_info->save()) {
            $return = array('resultCode' => 200, 'resultDescription' => '删除文件成功');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        } else {
            $return = array('resultCode' => 403, 'resultDescription' => '删除文件失败');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

    public function actionUpload() {
        if (!isset($_SESSION['user']['userid'])) {
            $return = array('resultCode' => 403, 'resultDescription' => '您未登录');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        if (!empty($_FILES)) {
            $fileName = $_FILES['file']['name'];
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);
            $file = $_FILES['file']['tmp_name'];
            $fileNewname = $_SESSION['user']['userid'] . '_' . date('YmdHis') . '.' . $fileType;
            $r = move_uploaded_file($file, './assets/userfile/' . $fileNewname);
            $road = dirname(Yii::app()->basePath) . "/assets/userfile/" . $fileNewname;
            if ($r) {
//                require Yii::app()->basePath . '/controllers/' . 'PageInfo.php';
//                $filenumber = PageInfo::getPages($road, $fileType);
                $filenumber = $this->getPages(str_replace('\\', '/', $road) . "\n");
                if ($filenumber <= 0) {
                    $return = array('resultCode' => 401, 'resultDescription' => '上传文件失败');
                    echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                    return;
                }
                $attachment_model = new attachment();
                $userid = $_SESSION['user']['userid'];
                $user_model = user::model();
                $user_info = $user_model->find("userid = " . $userid);
                //文件中添加学校Id
                $attachment_model->_storeid = $user_info->_storeid;
                $attachment_model->attachmentname = $fileName;
                $attachment_model->attachmentfile = $fileNewname;
                $attachment_model->_userid = $userid;
                $attachment_model->filetype = $fileType;
                $attachment_model->filenumber = $filenumber;
                $attachment_model->uploadtime = date('Y-m-d H:i:s');
				$attachment_model->ip = GetHostByName("");//gethostbyname($_SERVER['SERVER_NAME']);
				$attachment_model->isdelete = 1;
                if ($attachment_model->save()) {
                    $return = array('resultCode' => 200, 'resultDescription' => '上传文件成功');
                    echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                    return;
                } else {
                    $return = array('resultCode' => 401, 'resultDescription' => '上传文件失败');
                    echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                    return;
                }
            }
        } else {
            $return = array('resultCode' => 402, 'resultDescription' => '无上传信息');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

    private function getPages($filePath) {
        $pages = 0;
        $host = "127.0.0.1";
        $port = 8090;
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array("sec" => 10, "usec" => 0));  // 发送超时10秒
        socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, array("sec" => 10, "usec" => 0));  // 接收超时10秒
        if (socket_connect($socket, $host, $port)) {
            if (socket_write($socket, $filePath)) {
                $result = socket_read($socket, 1024, PHP_NORMAL_READ);
                $pages = $result ? $result : 0;
            }
            socket_close($socket);
        }
        return $pages;
    }

    public function actionAddOrder() {
        if (!isset($_POST) || empty($_POST)) {
            $return = array('resultCode' => 401, 'resultDescription' => '订单信息不完整');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $userid = $_SESSION['user']['userid'];
        $filesjson = $_POST['files'];
        $filesjson = substr($filesjson, strpos($filesjson, '{'), strrpos($filesjson, '}') - strpos($filesjson, '{') + 1);

        $files = json_decode($filesjson);
        $pages = 0;
        for ($i = 0; $i < sizeof($files->files); $i++) {
            $file = $files->files[$i];
            $page = $this->chkPrintSet($file->filenumber, $file->printSet);
            if ($page == 0) {
                $return = array('resultCode' => 402, 'resultDescription' => '打印设置格式有错', 'attachmentId' => $file->_attachmentId, 'num' => intval($file->filenumber));
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
            $files->files[$i]->paidMoney = $page * 0.2 * $file->printNumbers;
            $pages = $pages + $page * $file->printNumbers;
        }

        $business_model = new business();
        while (true) {
            $orderid = date('YmdHis') . $this->genCode(9);
            if (!$business_model->find("orderId='$orderid'")) {
                break;
            }
        }
        $user_model = user::model();
        $user_info = $user_model->find("userid = " . $userid);
        if ($user_info) {
            $business_model->_storeid = $user_info->_storeid;
        }
        $business_model->_userid = $userid;
        $business_model->orderId = $orderid;
        $business_model->paidMoney = $this->getMoney($pages);
        $business_model->placeOrdertime = date('Y-m-d H:i:s');

        if (!$business_model->save()) {
            $return = array('resultCode' => 402, 'resultDescription' => '订单生成失败');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $business_info = $business_model->find("orderId='$orderid'");
        $_businessId = $business_info->businessid;
        foreach ($files->files as $file) {
            $subbusiness_model = new subbusiness();
            $subbusiness_model->_businessId = $_businessId;
            $subbusiness_model->_attachmentId = $file->_attachmentId;
            $subbusiness_model->printNumbers = $file->printNumbers;
            $subbusiness_model->printSet = $file->printSet;
            $subbusiness_model->paidMoney = $file->paidMoney;

            if (!$subbusiness_model->save()) {
                $return = array('resultCode' => 402, 'resultDescription' => '订单生成失败');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        }
        $businessid = $business_model->find("orderId='$orderid'")->businessid;
        //$this->actionsendCode($_SESSION['user']['phone']);
        $return = array('resultCode' => 200, 'resultDescription' => '订单生成成功', 'businessid' => $businessid, 'orderid' => $orderid, 'paidMoney' => $business_model->paidMoney);
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    private function getMoney($pages) {
        return $pages * ($pages <= 20 ? 0.2 : ($pages <= 50 ? 0.18 : 0.15));
    }

    public function actionGetMoney() {
        $pages = $_POST['pages'];
        $money = 0;
        if ($pages > 0) {
            $money = $this->getMoney($pages);
        }
        echo $money;
    }

    public function actionEditOrder() {
        $businessid = isset($_POST['businessid']) ? trim($_POST['businessid']) : '';
        if ($businessid == '') {
            $return = array('resultCode' => 401, 'resultDescription' => '订单号不能为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $userid = $_SESSION['user']['userid'];
        $filesjson = $_POST['files'];
        $filesjson = substr($filesjson, strpos($filesjson, '{'), strrpos($filesjson, '}') - strpos($filesjson, '{') + 1);

        $files = json_decode($filesjson);
        $pages = 0;
        for ($i = 0; $i < sizeof($files->files); $i++) {
            $file = $files->files[$i];
            $page = $this->chkPrintSet($file->filenumber, $file->printSet);
            if ($page == 0) {
                $return = array('resultCode' => 402, 'resultDescription' => '打印设置格式有错', 'attachmentId' => $file->_attachmentId, 'num' => intval($file->filenumber));
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
            $files->files[$i]->paidMoney = $page * 0.2 * $file->printNumbers;
            $pages = $pages + $page * $file->printNumbers;
        }
        $business_model = new business();
        $business_info = $business_model->find("businessid=$businessid and _userid = $userid");
        $business_info->paidMoney = $this->getMoney($pages);
        $business_info->placeOrdertime = date('Y-m-d H:i:s');

        if (!$business_info->save()) {
            $return = array('resultCode' => 402, 'resultDescription' => '订单修改失败');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $subbusiness_model = new subbusiness();
        $subbusiness_model->deleteAll("_businessId=$businessid");
        foreach ($files->files as $file) {
            $subbusiness_model->_businessId = $businessid;
            $subbusiness_model->_attachmentId = $file->_attachmentId;
            $subbusiness_model->printNumbers = $file->printNumbers;
            $subbusiness_model->printSet = $file->printSet;
            $subbusiness_model->paidMoney = $file->paidMoney;

            if (!$subbusiness_model->save()) {
                $return = array('resultCode' => 402, 'resultDescription' => '订单修改失败');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        }
        //$this->actionsendCode($_SESSION['user']['phone']);
        $return = array('resultCode' => 200, 'resultDescription' => '订单修改成功', 'paidMoney' => $business_info->paidMoney);
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    private function chkPrintSet($filenum, $printset) {
        $exp = '/^[1-9]([0-9])*([,\-][1-9]([0-9])*)*$/';
        $exp2 = '/\-\d+\-/';
        $num = 0;
        if (preg_match($exp, $printset)) {
            if (!preg_match($exp2, $printset)) {
                $arr = explode(',', $printset);
                foreach ($arr as $val) {
                    if (preg_match('/^\d+$/', $val) && intval($val) > 0 && intval($val) <= $filenum) {
                        $num = $num + 1;
                    } else if (preg_match('/^(\d+)\-(\d+)$/', $val, $matches)) {
                        $n1 = intval($matches[1]);
                        $n2 = intval($matches[2]);
                        if ($n1 <= $filenum && $n2 <= $filenum) {
                            $num = $num + abs($n2 - $n1) + 1;
                        } else {
                            return 0;
                        }
                    } else {
                        return 0;
                    }
                }
                return $num;
            }
        }
        return 0;
    }

    public function actionOrderList() {
        if (!isset($_POST) || empty($_POST)) {
            $return = array('resultCode' => 401, 'resultDescription' => '订单信息不完整');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $userid = $_SESSION['user']['userid'];
        $status0 = isset($_POST['ispay']) ? trim($_POST['ispay']) : 0;

        $business_model = new business();
        $subbusiness_model = new subbusiness();
        $business_info = $business_model->findAll(array('condition' => "_userid=$userid and isdelete=0", 'order' => 'placeOrdertime DESC'));
        if ($business_info != null) {
            $businessList = array();
            if ($status0 == 0) {
                $paytype = -1;
                foreach ($business_info as $business) {
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessId=" . $business->businessid));
                    $s = 0;
                    foreach ($subbusiness_info as $subbusiness) {
                        $ispay = $subbusiness['isPay'];
                        $paytype = $subbusiness['payType'] == null ? -1 : $subbusiness['payType'];
                        if ($ispay != 0) {
                            $s = 1;
                            break;
                        }
                    }
                    if ($s == 0) {
                        array_push($businessList, array('businessid' => $business->businessid, 'orderId' => $business->orderId, 'paidMoney' => $business->paidMoney,
                            'paytype' => $paytype, 'placeOrdertime' => $business->placeOrdertime, 'code' => $business->verificationCode));
                    }
                }
            } elseif ($status0 == 1) {
                foreach ($business_info as $business) {
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessId=" . $business->businessid));
                    $s = 0;
                    $n = 0;
                    foreach ($subbusiness_info as $subbusiness) {
                        $ispay = $subbusiness['isPay'];
                        $status = $subbusiness['status'];
                        if ($ispay != 1) {
                            $s = 1;
                            break;
                        }
                        if ($status == 1) {
                            $n++;
                        }
                    }
                    if ($s == 0 && $n < count($subbusiness_info)) {
                        array_push($businessList, array('businessid' => $business->businessid, 'orderId' => $business->orderId, 'paidMoney' => $business->paidMoney,
                            'placeOrdertime' => $business->placeOrdertime, 'code' => $business->verificationCode));
                    }
                }
            } elseif ($status0 == 2) {
                foreach ($business_info as $business) {
                    $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessId=" . $business->businessid));
                    $s = 0;
                    $n = 0;
                    foreach ($subbusiness_info as $subbusiness) {
                        $ispay = $subbusiness['isPay'];
                        $status = $subbusiness['status'];
                        if ($ispay != 1) {
                            $s = 1;
                            break;
                        }
                        if ($status == 1) {
                            $n++;
                        }
                    }
                    if ($s == 0 && $n == count($subbusiness_info)) {
                        array_push($businessList, array('businessid' => $business->businessid, 'orderId' => $business->orderId, 'paidMoney' => $business->paidMoney,
                            'placeOrdertime' => $business->placeOrdertime));
                    }
                }
            }
            if (count($businessList) == 0) {
                $return = array('resultCode' => 402, 'resultDescription' => '订单列表为空');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            } else {
                $return = array('resultCode' => 200, 'resultDescription' => '获取文件列表成功', 'orderList' => $businessList);
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        } else {
            $return = array('resultCode' => 402, 'resultDescription' => '订单列表为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

    public function actionDelOrder() {
        $businessid = isset($_POST['businessid']) ? trim($_POST['businessid']) : '';

        $exp = '/^[1-9]([0-9])*(,[1-9]([0-9])*)*$/';
        if (!preg_match($exp, $businessid)) {
            $return = array('resultCode' => 401, 'resultDescription' => '订单ID非法');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $userid = $_SESSION['user']['userid'];
        $business_model = business::model();

        $n = $business_model->updateAll(array('isdelete' => 1), "businessid in ($businessid) and _userid=$userid");
//        foreach ($businessid as $id) {
//            $attachment_info = $business_model->find("businessid = $id and _userid=$userid");
//            if ($attachment_info == null) {
//                $return = array('resultCode' => 402, 'resultDescription' => '订单不存在');
//                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
//                return;
//            }
//            $attachment_info->isdelete = 1;
//            if (!$attachment_info->save()) {
//                $return = array('resultCode' => 403, 'resultDescription' => '删除订单失败');
//                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
//                return;
//            }
//        }
        if ($n > 0) {
            $return = array('resultCode' => 200, 'resultDescription' => '删除订单成功');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        } else {
            $return = array('resultCode' => 403, 'resultDescription' => '删除订单失败');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

    public function actionOrderDetail() {
        if (!isset($_POST) || empty($_POST)) {
            $return = array('resultCode' => 401, 'resultDescription' => '订单信息不完整');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $userid = $_SESSION['user']['userid'];
        $_businessId = isset($_POST['businessid']) ? trim($_POST['businessid']) : '';

        $subbusiness_model = new subbusiness();
        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessId=$_businessId"));

        $attachment_model = new attachment();
        if ($subbusiness_info != null) {
            foreach ($subbusiness_info as $business) {
                $file = $attachment_model->find(array('condition' => "attachmentid=" . $business['_attachmentId']));
                $fileList[] = array('fileId' => $business['_attachmentId'], 'copy' => $business['printNumbers'],
                    'printSet' => $business['printSet'], 'paidMoney' => $business['paidMoney'], 'filename' => $file->attachmentname, 'filenum' => $file->filenumber);
            }
            $return = array('resultCode' => 200, 'resultDescription' => '获取文件列表成功', 'fileList' => $fileList);
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        } else {
            $return = array('resultCode' => 402, 'resultDescription' => '订单详情为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

    public function actionPayOrder() {
        if (!isset($_POST) || empty($_POST)) {
            $return = array('resultCode' => 401, 'resultDescription' => '订单信息不完整');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $userid = $_SESSION['user']['userid'];
        $orderid = isset($_POST['orderid']) ? trim($_POST['orderid']) : '';
        $business_model = new business();
        $business_info = $business_model->find("orderId='$orderid' and _userid='$userid'");

        $business_info->payType = 1;
        $business_info->payTime = date('Y-m-d H:i:s');

        $business_model->_userid = $userid;
        $business_model->orderId = $orderid;
        $business_model->placeOrdertime = date('Y-m-d H:i:s');

        if (!$business_model->save()) {
            $return = array('resultCode' => 402, 'resultDescription' => '订单生成失败');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $filesjson = $_POST['files'];
        $filesjson = substr($filesjson, strpos($filesjson, '{'), strrpos($filesjson, '}') - strpos($filesjson, '{') + 1);

        $files = json_decode($filesjson);

        foreach ($files->files as $file) {
            $subbusiness_model = subbusiness::model();
            $subbusiness_model->_businessId = $orderid;
            $subbusiness_model->_attachmentId = $file->_attachmentId;
            $subbusiness_model->printNumbers = $file->printNumbers;
            $subbusiness_model->printSet = $file->printSet;
            if (!$subbusiness_model->save()) {
                $business_model->delete("orderId=$orderid");
                $subbusiness_model->delete("_businessId=$orderid");
                $return = array('resultCode' => 402, 'resultDescription' => '订单生成失败');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        }
        //$this->actionsendCode($_SESSION['user']['phone']);
        $return = array('resultCode' => 200, 'resultDescription' => '订单生成成功', 'orderid' => $orderid);
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    public function actionSign() {
        $userid = $_SESSION['user']['userid'];
        $record_model = new record();
        $record = $record_model->find(array('condition' => "userid='$userid'"));
        if (!$record) {
            $record_model->userid = $userid;
            $record_model->points = 500;
            if (!$record_model->save()) {
                $return = array('resultCode' => 404, 'resultDescription' => '未记录');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
            $record = $record_model->find(array('condition' => "userid='$userid'"));
        }
        $lasttime = $record->signtime;
        $lastday = strtotime(date('y-m-d', $lasttime));
        $today = strtotime(date('y-m-d', time()));

        $diftime = $today - $lastday;

        if ($diftime <= 0) {
            $return = array('resultCode' => 401, 'resultDescription' => '今天已签到');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        } elseif ($diftime <= 86400) {
            if ($record->seridays < 6) {
                $record->seridays = $record->seridays + 1;
            }
            $record->toldays = $record->toldays + 1;
            $record->points = 5 * $record->seridays + $record->points;
            $record->signtime = time();
            if ($record->save()) {
                $indegral_model = new integraldetails();
                $indegral_model->_userid = $userid;
                $indegral_model->addIntegral = 5 * $record->seridays;
                $indegral_model->happentime = date('Y-m-d H:i:s');
                $indegral_model->happenInfo = "签到送积分";
                $indegral_model->save();
                $return = array('resultCode' => 200, 'resultDescription' => '连续签到成功', 'seridays' => $record->seridays, 'points' => $record->points, 'signtime' => $record->signtime);
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        } else {
            $record->seridays = 1;
            $record->toldays = $record->toldays + 1;
            $record->points = 5 * $record->seridays + $record->points;
            $record->signtime = time();
            if ($record->save()) {
                $indegral_model = new integraldetails();
                $indegral_model->_userid = $userid;
                $indegral_model->addIntegral = 5 * $record->seridays;
                $indegral_model->happentime = date('Y-m-d H:i:s');
                $indegral_model->happenInfo = "签到送积分";
                $indegral_model->save();
                $return = array('resultCode' => 200, 'resultDescription' => '签到成功', 'seridays' => $record->seridays, 'points' => $record->points);
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        }
        $return = array('resultCode' => 402, 'resultDescription' => '签到失败');
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    public function actionWallet() {
        $userid = $_SESSION['user']['userid'];
        $record_model = new record();
        $record = $record_model->find(array('condition' => "userid='$userid'"));
        if ($record) {
            $return = array('resultCode' => 200, 'resultDescription' => '获取成功', 'coin' => $record->coin, 'points' => $record->points);
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $return = array('resultCode' => 200, 'resultDescription' => '获取失败', 'coin' => 0, 'points' => 0);
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    private function genCode($len = 6) {
        if (intval($len) <= 0) {
            return '';
        }
        $chars = '0123456789';
        $char = substr($chars, rand(0, 9), 1);
        $code = $char;
        $len2 = 1;
        while ($len2 < $len) {
            $chars2 = substr($chars, 0, intval($char)) . substr($chars, intval($char) + 1);
            $char = substr($chars2, rand(0, 8), 1);
            $code.=$char;
            $len2 = $len2 + 1;
        }
        return $code;
    }

    public function actionsendCode() {
        if (isset($_POST)) {
            $mobiles = isset($_POST['mobiles']) ? trim($_POST['mobiles']) : '';
            $type = isset($_POST['type']) ? trim($_POST['type']) : '';
        }
        $returnjson = $this->checkphone($mobiles);
        if ($type == '' && $returnjson != NULL) {
            echo $returnjson;
            return;
        } elseif ($type != '') {
            $return = json_decode($returnjson);
            if ($return->resultCode != 412) {
                $return = array('resultCode' => 421, 'resultDescription' => '手机号未注册');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        }

        $code = $this->genCode();
        $_SESSION['register'] = array('code' => $code, 'phone' => $mobiles, 'time' => time());
        $sessionID = new CHttpCookie('PHPSESSID', session_id());
        Yii::app()->request->cookies['PHPSESSID'] = $sessionID;
        $contents = '【颇闰科技】欢迎使用颇闰产品，您的验证码为' . $code;
        $url = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user=cqutprint&pwd=112233&mobiles=$mobiles&contents=$contents&chid=0&sendtime=";
        file_get_contents($url);
        $return = array('resultCode' => 200, 'resultDescription' => '验证码发送成功', 'code' => $_SESSION['register']['code'],
            'phone' => $_SESSION['register']['phone'], 'time' => $_SESSION['register']['time']);
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    public function checkphone($phone) {
        $phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
        if ($phone == '') {
            $return = array('resultCode' => 402, 'resultDescription' => '手机号不能为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $user_model = new user();
        $user_info = $user_model->find("phone = '$phone'");
        if ($user_info) {
            $return = array('resultCode' => 412, 'resultDescription' => '手机号已注册');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        return NULL;
    }

    public function actionMessage() {
        $userid = $_SESSION['user']['userid'];
        $content = isset($_POST['content']) ? trim($_POST['content']) : '';
        $message_model = new message();
        $message_model->user_id = $userid;
        $message_model->content = $content;
        $message_model->addtime = time();

        if ($message_model->save()) {
            $return = array('resultCode' => 200, 'resultDescription' => '提交成功');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        } else {
            $return = array('resultCode' => 401, 'resultDescription' => '提交失败');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
    }

    public function actionPay() {
        $userid = $_SESSION['user']['userid'];
        $businessid = isset($_POST['businessid']) ? trim($_POST['businessid']) : '';
        $type = isset($_POST['type']) ? trim($_POST['type']) : '';
        $business_model = new business();
        $business_info = $business_model->find("businessid=$businessid and _userid=$userid");
        $vericode = $business_info['verificationCode'];
        $subbusiness_model = subbusiness::model();
        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessId=" . $businessid));
        if ($type == 2) {//积分支付
            $money = $business_info->paidMoney;
            $point = $money * 100;
            $record_model = new record();
            $record_info = $record_model->find(array('condition' => "userid='$userid'"));
            $points = $record_info->points;
            if ($point > $points) {
                $return = array('resultCode' => 401, 'resultDescription' => '积分不足');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
            $business_info->status = 1;
            $business_info->paytype = 5;
            $business_info->consumptionIntegral = $point;
            if ($vericode == NULL) {
                while (true) {
                    $code = $this->genCode(9);
                    if (!$business_model->find("verificationCode=$code")) {
                        break;
                    }
                }
                $business_info->verificationCode = $code;
            }
            $record_info->points = $points - $point;
            if ($record_info->save() && $business_info->save()) {
                foreach ($subbusiness_info as $subbusiness) {
                    $subbusiness_info2 = $subbusiness_model->find(array('condition' => "subbusinessId=" . $subbusiness->subbusinessId));
                    $subbusiness_info2->isPay = 1;
                    $subbusiness_info2->payType = 5;
                    $subbusiness_info2->payTime = date('Y-m-d H:i:s');
                    $subbusiness_info2->save();
                }
                if ($vericode == NULL) {
                    $this->sendCode($_SESSION['user']['phone'], $code);
                }
                $indegral_model = new integraldetails();
                $indegral_model->_userid = $userid;
                $indegral_model->reduceIntegral = $point;
                $indegral_model->happentime = date('Y-m-d H:i:s');
                $indegral_model->happenInfo = "积分支付";
                $indegral_model->save();
                $return = array('resultCode' => 200, 'resultDescription' => '支付成功', 'points' => $points - $point, 'point' => $point);
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            } else {
                $return = array('resultCode' => 401, 'resultDescription' => '支付失败');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            }
        } elseif ($type == 3) {//线下支付
            $business_info->status = 0;
            $business_info->paytype = 0;
            if ($vericode == NULL) {
                while (true) {
                    $code = $this->genCode(9);
                    if (!$business_model->find("verificationCode=$code")) {
                        break;
                    }
                }
                $business_info->verificationCode = $code;
            }
            if ($business_info->save()) {
                foreach ($subbusiness_info as $subbusiness) {
                    $subbusiness_info2 = $subbusiness_model->find(array('condition' => "subbusinessId=" . $subbusiness->subbusinessId));
                    $subbusiness_info2->isPay = 0;
                    $subbusiness_info2->payType = 0;
                    $subbusiness_info2->payTime = date('Y-m-d H:i:s');
                    $subbusiness_info2->save();
                }
                if ($vericode == NULL) {
                    $this->sendCode($_SESSION['user']['phone'], $code);
                }
                $return = array('resultCode' => 200, 'resultDescription' => '支付成功');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            } else {
                $return = array('resultCode' => 401, 'resultDescription' => '支付失败');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            }
        } else {
            $return = array('resultCode' => 401, 'resultDescription' => '支付方式有误');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

    private function sendCode($mobiles, $code) {
        $code = substr($code, 0, 3) . '%20' . substr($code, 3, 3) . '%20' . substr($code, 6, 3);
        $contents = '【颇闰科技】欢迎使用颇闰产品，您的验证码为' . $code;
        $url = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user=cqutprint&pwd=112233&mobiles=$mobiles&contents=$contents&chid=0&sendtime=";
        file_get_contents($url);
    }

}
