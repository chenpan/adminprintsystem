<?php

header('content-type:application/json;charset=utf8');

class adminController extends Controller {

    public function __construct() {
        
    }

    public function actionPrintors() {
        $printor_model = printor::model();
        $list = $printor_model->findAll();
        if ($list != null) {
            $printors = array();
            foreach ($list as $printor) {
                $isoff = (time() - $printor->last_time) > 3000 ? 1 : 0;
                $status = ($printor->paper_remain < 100 || $printor->printer_status > 0 || $isoff == 1) ? 1 : 0;
                array_push($printors, array('id' => $printor->printorId, 'name' => $printor->printorName, 'status' => $status,
                    'paper_remain' => $printor->paper_remain, 'printer_status' => $printor->printer_status, 'isoff' => $isoff));
            }
            $return = array('resultCode' => 200, 'resultDescription' => '获取终端列表成功', 'printors' => $printors);
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        } else {
            $return = array('resultCode' => 402, 'resultDescription' => '终端列表为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

    public function actionPrintor() {
        $id = isset($_POST['id']) ? trim($_POST['id']) : '';
        if ($id == '' || intval($id) <= 0) {
            $return = array('resultCode' => 401, 'resultDescription' => '终端ID非法');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $id = intval($id);
        $printor_model = printor::model();
        $list = $printor_model->find("printorId=$id");
        $printor = array('paper_remain' => $list->paper_remain, 'yuan_remain' => $list->yuan_remain, 'jiao_remain' => $list->jiao_remain, 'yuan_changer' => $list->yuan_changer, 'jiao_changer' => $list->jiao_changer,
            'coin_acceptor' => $list->coin_acceptor, 'note_acceptor' => $list->note_acceptor, 'card_acceptor' => $list->card_acceptor, 'printer_status' => $list->printer_status);
        if ($list != null) {
            $return = array('resultCode' => 200, 'resultDescription' => '获取终端列表成功', 'printor' => $printor);
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        } else {
            $return = array('resultCode' => 402, 'resultDescription' => '终端列表为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

}
