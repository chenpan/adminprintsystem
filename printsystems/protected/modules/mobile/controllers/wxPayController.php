﻿<?php

class wxPayController extends Controller {
    /*
     * 微信下单
     */

    public function actionAppWXOrder() {
        $body = "手机微信支付";
        $out_trade_no = null;
        $detail = "文件打印";
        //微信支付单位为分
        $total_fee = 0;
        if (isset($_POST)) {
            $body = $_POST['body'];
            $out_trade_no = $_POST['out_trade_no'];
            $detail = $_POST['detail'];
            $total_fee = $_POST['total_fee'] * 100;
        }
        $orderURL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
        //生成随机字符串
        $str = $this->randStr(32);

        $arr = array(
            //appid
            "appid" => 'wx04da4155c18873c8',
            //商户号
            //"mch_id" => '1292670201',
            "mch_id" => '1297473901',
            //商品描述
            "body" => $body,
            //商品详情
            "detail" => $detail,
            //商品订单号
            "out_trade_no" => $out_trade_no,
            //交易类型
            "trade_type" => 'APP',
            //随机字符窜
            "nonce_str" => $str,
            "notify_url" => 'http://www.cqutprint.com/wxpayMobile/notify_url.php',
            "total_fee" => $total_fee,
            'spbill_create_ip' => $this->get_client_ip(),
        );

        //对数组签名
        $sign = $this->getSign($arr);
        $arr['sign'] = $sign;
        print_r($arr);
        $xml = $this->arr2xml($arr);
        // print_r($xml);
        $response = $this->postXmlCurl($xml, $orderURL);
        //将微信返回数据转换成数组
        $return = $this->xmlstr_to_array($response);
        print_r($return);
        if ($return['return_code'] == 'SUCCESS' && $return['result_code'] == "SUCCESS") {
            $prepayid = $return['prepay_id'];
            $return = array(
                'resultCode' => 200,
                'resultDescription' => '预下单成功',
                'data' => $this->getOrder($prepayid),
            );
        } else {
            $return = array(
                'resultCode' => 400,
                'resultDescription' => '预下单失败',
                'data' => $return,
            );
        }
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    //执行第二次签名，才能返回给客户端使用
    public function getOrder($prepayId) {
        $data["appid"] = 'wx04da4155c18873c8';
        $data["noncestr"] = $this->randStr(32);
        $data["package"] = "Sign=WXPay";
        $data["partnerid"] = "1297473901";
        $data["prepayid"] = $prepayId;
        $data["timestamp"] = time();
        $s = $this->getSign($data);
        $data["sign"] = $s;
        return $data;
    }

    /**
     *  将数组转换为xml
     *  @param array $data  要转换的数组
     *  @param bool $root   是否要根节点
     *  @return string     xml字符串
     */
    private function arr2xml($data, $root = true) {
        $str = "";
        if ($root)
            $str .= "<xml>";
        foreach ($data as $key => $val) {
            if (is_array($val)) {
                $child = arr2xml($val, false);
                $str .= "<$key>$child</$key>";
            } else {
//                $str.= "<$key><![CDATA[$val]]></$key>";
                $str.= "<$key>$val</$key>";
            }
        }
        if ($root)
            $str .= "</xml>";
        return $str;
    }

    //post https请求，CURLOPT_POSTFIELDS xml格式
    function postXmlCurl($xml, $url, $second = 30) {
        //初始化curl        
        $ch = curl_init();
        //超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        //这里设置代理，如果有的话
        //curl_setopt($ch,CURLOPT_PROXY, '8.8.8.8');
        //curl_setopt($ch,CURLOPT_PROXYPORT, 8080);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
//            echo "curl出错，错误码:$error" . "<br>";
//            echo "<a href='http://curl.haxx.se/libcurl/c/libcurl-errors.html'>错误原因查询</a></br>";
            curl_close($ch);
            return false;
        }
    }

    public function sign($arr) {

        //参数名按字典序排序
        ksort($arr);
        //key
        $key = 'UFMcDsLEgYSlDaI15vnUtZrI831ETFGs';
        //
        $strA = "";
        foreach ($arr as $k => $v) {
            if ($v != "") {
                $strA .=$k . '=' . $v . '&';
            }
        }
        $unSign = $strA . 'key=' . $key;
        // print_r(">>>>>".$unSign.">>>>");
        $sign = strtoupper(MD5($unSign));
        return $sign;
    }

    /*
      生成签名
     */

    function getSign($Obj) {
        $key = 'UFMcDsLEgYSlDaI15vnUtZrI831ETFGs';
        foreach ($Obj as $k => $v) {
            $Parameters[strtolower($k)] = $v;
        }
        //签名步骤一：按字典序排序参数
        ksort($Parameters);
        $String = $this->formatBizQueryParaMap($Parameters, false);
        print_r("   String:" . $String);

        //echo "【string】 =".$String."</br>";
        //签名步骤二：在string后加入KEY
        $String = $String . "&key=" . $key;
        //   echo "<textarea style='width: 50%; height: 150px;'>$String</textarea> <br />";
        //签名步骤三：MD5加密
        $result_ = strtoupper(md5($String));
        print_r("     签名：" . $result_);



        return $result_;
    }

    /*
      获取当前服务器的IP
     */

    function get_client_ip() {
        if ($_SERVER['REMOTE_ADDR']) {
            $cip = $_SERVER['REMOTE_ADDR'];
        } elseif (getenv("REMOTE_ADDR")) {
            $cip = getenv("REMOTE_ADDR");
        } elseif (getenv("HTTP_CLIENT_IP")) {
            $cip = getenv("HTTP_CLIENT_IP");
        } else {
            $cip = "unknown";
        }
        return $cip;
    }

    //将数组转成uri字符串
    function formatBizQueryParaMap($paraMap, $urlencode) {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if ($urlencode) {
                $v = urlencode($v);
            }
            $buff .= strtolower($k) . "=" . $v . "&";
        }
        $reqPar;
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    /**
      xml转成数组
     */
    function xmlstr_to_array($xmlstr) {
        $doc = new DOMDocument();
        $doc->loadXML($xmlstr);
        return $this->domnode_to_array($doc->documentElement);
    }

    function domnode_to_array($node) {
        $output = array();
        switch ($node->nodeType) {
            case XML_CDATA_SECTION_NODE:
            case XML_TEXT_NODE:
                $output = trim($node->textContent);
                break;
            case XML_ELEMENT_NODE:
                for ($i = 0, $m = $node->childNodes->length; $i < $m; $i++) {
                    $child = $node->childNodes->item($i);
                    $v = $this->domnode_to_array($child);
                    if (isset($child->tagName)) {
                        $t = $child->tagName;
                        if (!isset($output[$t])) {
                            $output[$t] = array();
                        }
                        $output[$t][] = $v;
                    } elseif ($v) {
                        $output = (string) $v;
                    }
                }
                if (is_array($output)) {
                    if ($node->attributes->length) {
                        $a = array();
                        foreach ($node->attributes as $attrName => $attrNode) {
                            $a[$attrName] = (string) $attrNode->value;
                        }
                        $output['@attributes'] = $a;
                    }
                    foreach ($output as $t => $v) {
                        if (is_array($v) && count($v) == 1 && $t != '@attributes') {
                            $output[$t] = $v[0];
                        }
                    }
                }
                break;
        }
        return $output;
    }

    /*
     * 生成随机字符串
     */

    public function randStr($len = 32) {
        $str = null;
        $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
        $max = strlen($strPol) - 1;

        for ($i = 0; $i < $len; $i++) {
            $str.=$strPol[rand(0, $max)]; //rand($min,$max)生成介于min和max两个数之间的一个随机整数
        }
        print_r("  随即字符串：：" . $str);
        return $str;
    }

    /**
     * 	作用：将xml转为array
     */ function xmlToArray($xml) {
        //将XML转为array        
        $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $array_data;
    }

    function convertXmlToArray($xml) {
        $xmlObj = simplexml_load_string($xml);
        $resp_array = array();
        $properties = get_object_vars($xmlObj);
        $varKeys = array_keys($properties);
        foreach ($varKeys as $vk) {
            $resp_array[$vk] = trim($xmlObj->$vk);
        }
        return $resp_array;
    }

    public function actionwxPayNotify() {
        $result_code = $_POST["result_code"];
        $err_code = $_POST["err_code"];
        $err_code_des = $_POST["err_code_des"];
        $trade_type = $_POST["trade_type"];
        $transaction_id = $_POST["transaction_id"];
        $out_trade_no = $_POST["out_trade_no"];
        $time_end = $_POST["time_end"];
        $total_fee = $_POST["total_fee"];
        
        
        $business_model = business::model();
        $business_info = $business_model->find("orderId =" . $out_trade_no . " and paidMoney =" . $total_fee);
        if (count($business_info) > 0) {
            file_put_contents('D:/data.txt', "ssssssss：：", FILE_APPEND);
            //确认用户支付成功该订单
            $business_info->status = 1;
            //微信支付
            $business_info->paytype = 7;
            
           if (!isset($business_info->verificationCode)) {//为空
                    $verificationCode = mt_rand(100000000, 999999999); //验证码
                    $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一

                    while ($verificationCodes) {
                        $verificationCode = mt_rand(100000000, 999999999);
                        $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一
                    }
                    $user = 'cqutprint'; //短信接口用户名 $user
                    $pwd = '112233'; //短信接口密码 $pwd
                    $mobiles = $user_infoss->phone; //说明：取用户输入的手号
                    $contents = "亲爱的用户，您此次打印的验证码为" . $verificationCode . ",您已支付成功，请您于终端机打印,谢谢！【颇闰科技】"; //说明：取用户输入的短信内容
                    $chid = 0; //通道ID
                    $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";

                    $business_info->verificationCode = $verificationCode;
                    $business_info->consumptionIntegral = $integration;
                    file_get_contents($sendMessage);
file_put_contents('D:/data.txt', "yifasongduanxin222", FILE_APPEND); 
                } else{
                $verificationCode = $business_info->verificationCode;
file_put_contents('D:/data.txt', "yifasongduanxin111", FILE_APPEND); 
                }
            
            $wechatpay = new wechatpay;
            $wechatpay->out_trade_no = $out_trade_no;
            $wechatpay->total_fee = $total_fee * 100;
            $wechatpay->time_end = $time_end;
            $wechatpay->result_code = $result_code;
            $wechatpay->err_code = $err_code;
            $wechatpay->err_code_des = $err_code_des;
            $wechatpay->transaction_id = $transaction_id;
            $wechatpay->trade_type = $trade_type;
            //
            $subbusiness_model = subbusiness::model();
            $subbusiness_info = $subbusiness_model->findAll("_businessId =" . $business_info->businessid);
            foreach ($subbusiness_info as $value) {
                $value->trade_no = $transaction_id;
                $value->isPay = 1;
                $value->save();
            }
           if($business_info->save()){
               return TRUE;
           }else{
               return FALSE;
           }
        } 
    }

}
