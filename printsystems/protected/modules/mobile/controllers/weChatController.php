<?php

class weChatController extends Controller
{

    public function actionLogin()
    {//登陆 用户微信唯一识别码登陆
        $openId = $_POST["openId"];
        $wechat_model = wechat::model();
        $weChat_info = $wechat_model->find(array('condition' => "weiOpenId= '$openId'"));
        if (isset($weChat_info))
        {
            $returnMessage = '{"resultCode":200,"resultDescription":"登录成功！"，"userid":"' . $weChat_info->_userId . '"}';
            echo $returnMessage;
        }
        else
        {
            $returnMessage = '{"resultCode":400,"resultDescription":"未绑定用户！"}';
            echo $returnMessage;
        }
    }

    public function actionValid()
    {//绑定用户微信唯一识别码到对应平台账号
        if (!empty($_POST['username']))
        {
            $username = $_POST['username'];
            $userpsw = md5($_POST['password']);
            $weiOpenId = $_POST['openId'];
            $user_model = user::model();
            $wechat_model = new wechat();
            $user_info = $user_model->find(array('condition' => "username= '$username' AND userpsw='$userpsw'"));
            $wechat_info = $wechat_model->find("_userId = $user_info->userid");
            if (isset($user_info))
            {
                if (isset($wechat_info))
                {
                    $returnMessage = '{"resultCode":403,"resultDescription":"已绑定用户！"}';
                    echo $returnMessage;
                }
                else
                {
                    $wechat_model->_userId = $user_info->userid;
                    $wechat_model->weiOpenId = $weiOpenId;
                    if ($wechat_model->save())
                    {
                        $returnMessage = '{"resultCode":200,"resultDescription":"信息保存成功！"}';
                        echo $returnMessage;
                    }
                    else
                    {
                        $returnMessage = '{"resultCode":400,"resultDescription":"信息保存失败！"}';
                        echo $returnMessage;
                    }
                }
            }
            else
            {
                $returnMessage = '{"resultCode":401,"resultDescription":"用户名或密码错误！"}';
                echo $returnMessage;
            }
        }
        else
        {
            $returnMessage = '{"resultCode":402,"resultDescription":"未获取到信息！"}';
            echo $returnMessage;
        }
    }

//注册发送验证码
    public function actionRegisterphone()
    {
        $registerPhone = $_POST['phone'];
        $user_model = user::model();
        $user_infos = $user_model->find(array('condition' => "phone= '$registerPhone'")); //判断手机是否注册？
        if ($user_infos)
        {
            $json = '{"data":"error"}';
            echo $json;
        }
        else
        {
            if (isset(Yii::app()->session['phoneRegistercheckma']))
            {
                unset(Yii::app()->session['phoneRegistercheckma']);
            }
            $phoneRegistercheckma = mt_rand(100000, 999999);
            Yii::app()->session['phoneRegistercheckma'] = $phoneRegistercheckma;
            $user = 'cqutprint'; //短信接口用户名 $user
            $pwd = '112233'; //短信接口密码 $pwd
            $mobiles = $registerPhone; //说明：取用户输入的手号
            $contents = "亲爱的用户，您此次注册的验证码为" . $phoneRegistercheckma . "，验证码很重要，打死都不要告诉任何人哦，谢谢！"; //说明：取用户输入的短信内容
            $chid = 0; //通道ID
            $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";
            file_get_contents($sendMessage);
            $json = "{'data':'success','phoneRegistercheckma':'$phoneRegistercheckma'}";
            $json = str_replace("'", '"', $json);
            echo $json;
        }
    }

    //注册
    public function actionRegister()
    {
        $user_model = new user();
        $record_model = new record();
        $integralDetails_model = new integralDetails();

        $registerUsername = $_POST['username']; //用户名
        $registerPassword = md5($_POST['password']); //密码
        $registerPhone = $_POST['phone']; //手机
        $registerEmail = $_POST['email']; //邮箱
        $phonecode = $_POST['yanzheng']; //手机验证码
        $openId = $_POST['openId'];
        $user_infos = $user_model->find(array('condition' => "username= '$registerUsername'")); //判断用户是否存在？ 
        if ($user_infos)
        {
            $json = '{"data":"false-exist"}';
            echo $json;
        }
        else
        {

            $user_model->username = $registerUsername;
            $user_model->userpsw = $registerPassword;
            $user_model->phone = $registerPhone;
            $user_model->email = $registerEmail;
            date_default_timezone_set('PRC');

            $user_model->registertime = date('Y-m-d H:i:s');
//                $user_model->integration = 200;

            if ($user_model->save())
            {
//                $userid = $user_model->attributes['userid'];
//                $wechat_model = new wechat();
//                $wechat_model->_userid = $userid;
//                $wechat_model->weiOpenId = $openId;
//                $wechat_model->save();
                $record_model->userid = $user_model->userid;
                $record_model->points = 200;
                $integralDetails_model->_userid = $user_model->userid;
                $integralDetails_model->addIntegral = 200;
                $integralDetails_model->reduceIntegral = 0;
                $integralDetails_model->happentime = date('Y-m-d H:i:s');
                $integralDetails_model->happenInfo = "注册送积分";
                if ($record_model->save() && $integralDetails_model->save())
                {
                    Yii::app()->session['username'] = $registerUsername;
                    unset(Yii::app()->session['phoneRegistercheckma']);
                    $json = '{"data":"success"}';
                    echo $json;
                }
            }
            else
            {
                $json = '{"data":"false-error"}';
                echo "$json";
            }
        }
    }

    public function actionFileList()
    {//文件列表
        if (!empty($_POST['openId']))
        {
            $openId = $_POST['openId'];
            $wechat_model = wechat::model();
            $wechat_info = $wechat_model->find(array('condition' => "weiOpenId='$openId'"));

            $userid = $wechat_info->_userId;

            $attachment_model = attachment::model();
            $attachment_info = $attachment_model->findAll(array('condition' => "isdelete=1 AND _userid = $userid", 'order' => 'attachmentid DESC'));

            if (count($attachment_info) != 0)
            {
                $str = '';
                foreach ($attachment_info as $K => $V)
                {
                    $filePath = Yii::app()->request->hostInfo . '/assets/userfile/' . $V->attachmentfile; //下载地址
                    $str .= "{'attachmentid':'$V->attachmentid','attachmentname':'$V->attachmentname','filenumber':'$V->filenumber','filePath':'$filePath','uploadtime':'$V->uploadtime'},";
                }
                $str = substr($str, 0, -1);
                $returnMessage = "{'resultCode':200,'resultDescription': '获取订单成功(包含所有文件)','userid':'$userid','file': ["
                        . $str
                        . "]}";
                $returnMessage = str_replace("'", '"', $returnMessage);
                echo $returnMessage;
            }
            else
            {
                $returnMessage = '{"resultCode":401,"resultDescription":"没有文件！"}';
                echo $returnMessage;
            }
        }
        else
        {
            $returnMessage = '{"resultCode":400,"resultDescription":"请重新登录！"}';
            echo $returnMessage;
        }
    }

    public function actionUpload()
    {//新上传文件
        $openId = $_POST['openId'];
        $wechat_model = wechat::model();
        $wechat_info = $wechat_model->find(array('condition' => "weiOpenId='$openId'"));

        $userid = $wechat_info->_userId;

        $user_model = user::model();
        $user_info = $user_model->find("userid = $userid");


        $targetFolder = './assets/userfile'; // Relative to the root
        $attachment_model = new attachment();

        $tempFile = $_FILES['file']['tmp_name'];
        date_default_timezone_set('PRC');
        $fileName = $userid . '-' . date('YmdHis');
        $attachment_after = substr(strrchr($_FILES['file']['name'], '.'), 1);  //文件的后缀名 docx
        $road = dirname(Yii::app()->basePath) . "\assets\userfile\\" . $fileName . "." . $attachment_after;
        $road = str_replace("\\", "/", $road);
        $targetPath = $targetFolder;
        $targetFile = rtrim($targetPath, '/') . '/' . $fileName . "." . $attachment_after;
        // Validate the file type
        $fileTypes = array('doc', 'docx', 'pdf'); // File extensions
        $fileParts = pathinfo($_FILES['file']['name']);
        if (in_array($fileParts['extension'], $fileTypes))
        {
            move_uploaded_file($tempFile, $targetFile);
            $filenumber = $this->getPages(str_replace('\\', '/', $road) . "\n");
            if ($filenumber > 0)
            {
                $attachment_model->attachmentname = $_FILES['file']['name']; //原名
                $attachment_model->attachmentfile = $fileName . "." . $attachment_after; //新名
                $attachment_model->_userid = $userid;
                $attachment_model->filetype = $attachment_after;
                date_default_timezone_set('PRC');
                $attachment_model->uploadtime = date('Y-m-d H:i:s');
                $attachment_model->filenumber = (int) $filenumber;
                $attachment_model->_storeid = $user_info->_storeid;
                $attachment_model->isdelete = 1;
                $attachment_model->ip = GetHostByName("");
                if ($attachment_model->save())
                {
                    echo "<script>alert('上传成功');window.location.href='http://weixin.cqutprint.com/index.php?r=file/AllFileList';</script>";
                }
            }
        }
        else
        {
            echo "<script>alert('文件格式不对，请重新选择文件！');window.location.href='http://weixin.cqutprint.com/index.php?r=index/indexFirst';</script>";
        }
    }

    function getPages($filePath)
    {
        $pages = 0;
        $host = "127.0.0.1";
        $port = 8090;
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO,
                array("sec" => 10, "usec" => 0));  // 发送超时2秒
        socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO,
                array("sec" => 10, "usec" => 0));  // 接收超时4秒
        socket_connect($socket, $host, $port);    //  连接  
        $num = socket_write($socket, $filePath); // 数据传送 向服务器发送消息
        if ($num > 0)
        {
            $pages = socket_read($socket, 1024, PHP_NORMAL_READ);
        }
        socket_close($socket);
        return $pages;
    }

    public function actionCreatOrder()
    {//生成订单
        $business_model = new business();

        $openId = $_POST['openId'];
//        $openId = "osW7cjnDNpur7JtblAwGgMKbco_g";
        $wechat_model = wechat::model();
        $wechat_info = $wechat_model->find(array('condition' => "weiOpenId='$openId'"));

        $userid = $wechat_info->_userId;
        $user_model = user::model();
        $user_info = $user_model->find("userid = $userid");

        $attachment_model = attachment::model();


        $attachmentidd = $_POST['attachmentidd']; //打印文件ID  1
        $printnumbers = $_POST['printnumbers']; //打印文件份数
        $printPages = $_POST['printPages']; //打印页码
//        $attachmentidd = "1,2,3"; //打印文件ID  1
//        $printnumbers = "4,5,6"; //打印文件份数
//        $printPages = "1-3,4*2-5*8-10"; //打印页码

        date_default_timezone_set('PRC');

        $orderNum = date('YmdHis') . mt_rand(100000, 999999); //生成20位订单号 20150901094439392811123
        $orderNums = $business_model->find(array('condition' => "orderId = '$orderNum'")); //判断订单号是否唯一

        while ($orderNums)
        {
            $orderNum = date('YmdHis') . mt_rand(100000, 999999);
            $orderNums = $business_model->find(array('condition' => "orderId = '$orderNum'")); //判断订单号是否唯一
        }

        $attachmentid = explode(',', $attachmentidd); //文件ID 1,2,3,4
        $printList = explode(',', $printnumbers); //每个文件打印的份数 1,2,2,2
        $filePages = explode('*', $printPages); //每个文件打印的页码  1,2,3-5*5-9,10,11*1,5,8

        $money = 0.00; //总金额
        $money_one = 0.00; //订单中单个文件所需的金额
        $page = 0; //总页数  用于打折
        $page_one = 0; //单个文件页数  用于打折

        foreach ($printList as $l => $y)
        {
            $filePage = explode(',', $filePages[$l]);
            foreach ($filePage as $z => $x)
            {
                if (is_numeric($x))
                {
                    $page += (int) $y;
//                    $money += (int) $y * $x * 0.2;
                }
                else
                {
                    $filePag = explode('-', $x);
                    $filePa = $filePag[1] - $filePag[0] + 1;
                    $page += (int) $y * $filePa;
//                    $money += (int) $y * (int) $filePa * 0.2;
                }
            }
        }
        if ($page > 0 && $page < 20)
        {
            $money = $page * 0.2;
        }
        else if ($page >= 20 && $page < 50)
        {
            $money = $page * 0.18;
        }
        else if ($page >= 50)
        {
            $money = $page * 0.15;
        }
        //存入订单
        $business_model->_userid = $userid;
        $business_model->orderId = $orderNum; //订单号
        $business_model->paidMoney = $money; //支付总金额
        $business_model->placeOrdertime = date('Y-m-d H:i:s', time());
        $business_model->_storeid = $user_info->_storeid;
        $business_model->isdelete = 0;

        $status = 0;
        $businessId = '';
        if ($business_model->save())
        {
            $businessId = $business_model->attributes['businessid'];
            foreach ($printList as $l => $y)
            {
                $filePage = explode(',', $filePages[$l]);
                foreach ($filePage as $z => $x)
                {
                    if (is_numeric($x))
                    {
//                    $money_one+= (int) $y * $x * 0.2;
                        $page_one +=(int) $y;
                    }
                    else
                    {
                        $filePag = explode('-', $x);
                        $filePa = $filePag[1] - $filePag[0] + 1;
//                    $money_one+= (int) $y * (int) $filePa * 0.2;
                        $page_one+=(int) $y * (int) $filePa;
                    }
                }

                if ($page > 0 && $page < 20)
                {
                    $money_one = $page_one * 0.2;
                }
                else if ($page >= 20 && $page < 50)
                {
                    $money_one = $page_one * 0.18;
                }
                else if ($page >= 50)
                {
                    $money_one = $page_one * 0.15;
                }

                $subbusiness_model = new subbusiness();
                //存入子订单，存储订单详细信息
                $subbusiness_model->_businessId = $business_model->businessid;
                $subbusiness_model->_attachmentId = $attachmentid[$l];
                $subbusiness_model->paidMoney = $money_one;
                $subbusiness_model->printNumbers = $y;
                $subbusiness_model->printSet = $filePages[$l]; //打印页码
                $subbusiness_model->isdelete = 0;
                $subbusiness_model->isrefund = 0;

                $money_one = 0.00;
                $page_one = 0;

                if (!$subbusiness_model->save())
                {
                    $status == 1;
                }
            }
            if ($status == 0)
            {
                $returnMessage = "{'resultCode':200,'resultDescription':'保存成功！','businessId':$businessId,'orderId':'$orderNum'}";
                echo $returnMessage;
            }
            else
            {
                $returnMessage = '{"resultCode":400,"resultDescription":"保存失败！"}';
                echo $returnMessage;
            }
        }
        else
        {
            $returnMessage = '{"resultCode":400,"resultDescription":"保存失败！"}';
            echo $returnMessage;
        }
    }

    //未支付列表
    public function actionNotpaidList()
    {
        $openId = $_POST['openId'];
//        $openId = "osW7cjloir6Ik8BMZR9scxUDpvP8";
        $wechat_model = wechat::model();
        $wechat_info = $wechat_model->find(array('condition' => "weiOpenId='$openId'"));

        $userid = $wechat_info->_userId;

        $business_model = business::model();
        $subbusiness_model = subbusiness::model();

        $Notpaid = array(); //未支付
        $Notpaids = ""; //未支付

        $business_info = $business_model->findAll(array('condition' => "_userid = '$userid' AND isdelete = 0"));

        foreach ($business_info as $k => $l)
        {
            //所有支付与否 1 全支付 0 不全支付
            $allpayed = 1;
            $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessId = '$l->businessid' AND isdelete = 0"));
            foreach ($subbusiness_info as $h => $j)
            {
                if ($j->isPay == 0)
                {//0未支付 
                    $allpayed = 0;
                }
            }
            //属于未支付列表
            if ($allpayed == 0)
            {
                $Notpaids .= $l->businessid . ",";
                array_push($Notpaid, $l->businessid);
            }
        }
        $Notpaids = substr($Notpaids, 0, strlen($Notpaids) - 1);

        if (strlen($Notpaids) == 0)
        {
            array_push($Notpaid, -1);
        }

        $Notpaidcriteria = new CDbCriteria;
        $Notpaidcriteria->compare('businessid', $Notpaid);
        $Notpaidcriteria->order = 'businessid DESC';
        $NotpaidList = $business_model->findAll($Notpaidcriteria);

        $NotpaidList_str = ""; //未支付字符串
        foreach ($NotpaidList as $K => $V)
        {
            $NotpaidList_str .= "{'businessid':'$V->businessid','orderId':'$V->orderId','paidMoney':'$V->paidMoney','placeOrdertime':'$V->placeOrdertime','verificationCode':'$V->verificationCode'},";
        }

        $NotpaidList_str = substr($NotpaidList_str, 0, -1);

        $returnMessage = "{'resultCode':200,'resultDescription': '获取订单列表成功(包含所有订单)','notPaidList': ["
                . $NotpaidList_str
                . "]}";
        $returnMessage = str_replace("'", '"', $returnMessage);
        echo $returnMessage;
    }

    //已支付未打印列表
    public function actionYetpaidList()
    {
        $openId = $_POST['openId'];
//        $openId = "osW7cjloir6Ik8BMZR9scxUDpvP8";
        $wechat_model = wechat::model();
        $wechat_info = $wechat_model->find(array('condition' => "weiOpenId='$openId'"));

        $userid = $wechat_info->_userId;

        $business_model = business::model();
        $subbusiness_model = subbusiness::model();

        $Yetpaid = array(); //已支付
        $Yetpaids = ""; //已支付

        $business_info = $business_model->findAll(array('condition' => "_userid = '$userid' AND isdelete = 0"));

        foreach ($business_info as $k => $l)
        {
            //所有支付与否 1 全支付 0 不全支付
            $allpayed = 1;
            //0未打印完成
            $allprinted = 1;
            $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessId = '$l->businessid' AND isdelete = 0"));
            foreach ($subbusiness_info as $h => $j)
            {
                if ($j->isPay == 0 || $j->isrefund == 1 || $j->isrefund == 2)
                {//0未支付 退款 或者 退款中
                    $allpayed = 0;
                }
                if ($j->status == 0 || $j->status == 2)
                {
                    $allprinted = 0;
                }
            }
            //属于已支付列表
            if ($allprinted == 0 && $allpayed == 1)
            {
                $Yetpaids .= $l->businessid . ",";
                array_push($Yetpaid, $l->businessid);
            }
        }
        $Yetpaids = substr($Yetpaids, 0, strlen($Yetpaids) - 1);

        if (strlen($Yetpaids) == 0)
        {
            array_push($Yetpaid, -1);
        }

        $Yetpaidcriteria = new CDbCriteria;
        $Yetpaidcriteria->compare('businessid', $Yetpaid);
        $Yetpaidcriteria->order = 'businessid DESC';
        $YetpaidList = $business_model->findAll($Yetpaidcriteria);
        $YetpaidList_str = ""; //已支付字符串
        foreach ($YetpaidList as $K => $V)
        {
            $YetpaidList_str .= "{'businessid':'$V->businessid','orderId':'$V->orderId','paidMoney':'$V->paidMoney','placeOrdertime':'$V->placeOrdertime','verificationCode':'$V->verificationCode'},";
        }

        $YetpaidList_str = substr($YetpaidList_str, 0, -1);

        $returnMessage = "{'resultCode':200,'resultDescription': '获取订单列表成功(包含所有订单)','yetPaidList': ["
                . $YetpaidList_str
                . "]}";
        $returnMessage = str_replace("'", '"', $returnMessage);
        echo $returnMessage;
    }

    //打印历史 包含退款 
    public function actionYetprintList()
    {
        $openId = $_POST['openId'];
        $wechat_model = wechat::model();
        $wechat_info = $wechat_model->find(array('condition' => "weiOpenId='$openId'"));
        $userid = $wechat_info->_userId;
        $business_model = business::model();
        $subbusiness_model = subbusiness::model();
        $Yetprint = array(); //已打印
        $Yetprints = ""; //已打印
        $business_info = $business_model->findAll(array('condition' => "_userid = '$userid' AND isdelete = 0"));
        foreach ($business_info as $k => $l)
        {
            //所有支付与否 1 全支付 0 不全支付
            $allpayed = 1;
            //0未打印完成
            $allprinted = 1;
            $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessId = '$l->businessid' AND isdelete = 0"));
            foreach ($subbusiness_info as $h => $j)
            {
                if ($j->isPay == 0)
                {//0未支付 
                    $allpayed = 0;
                }
                if ($j->status == 0 && $j->isrefund == 0) //只去除未打印的订单
                    $allprinted = 0;
            }
            //属于已支付列表
            if ($allprinted == 1 && $allpayed == 1)
            {
                $Yetprints .= $l->businessid . ",";
                array_push($Yetprint, $l->businessid);
            }
        }
        $Yetprints = substr($Yetprints, 0, strlen($Yetprints) - 1);

        if (strlen($Yetprints) == 0)
        {
            array_push($Yetprint, -1);
        }

        $Yetprintcriteria = new CDbCriteria;
        $Yetprintcriteria->compare('businessid', $Yetprint);
        $Yetprintcriteria->order = 'businessid DESC';
        $YetprintList = $business_model->findAll($Yetprintcriteria);
        $YetprintList_str = ""; //已打印字符串
        foreach ($YetprintList as $K => $V)
        {
            $YetprintList_str .= "{'businessid':'$V->businessid','orderId':'$V->orderId','paidMoney':'$V->paidMoney','placeOrdertime':'$V->placeOrdertime','verificationCode':'$V->verificationCode'},";
        }

        $YetprintList_str = substr($YetprintList_str, 0, -1);

        $returnMessage = "{'resultCode':200,'resultDescription': '获取订单列表成功(包含所有订单)','yetPrintList': ["
                . $YetprintList_str
                . "]}";
        $returnMessage = str_replace("'", '"', $returnMessage);
        echo $returnMessage;
    }

    public function actionOrderDetail()
    {//订单详情
//        $orderId = $_POST["orderId"];
        $businessidd = $_POST["businessid"];
        $businssid_model = business::model();
        $subbusiness_model = subbusiness::model();
        $attachment_model = attachment::model();

        $businessid_info = $businssid_model->find(array("condition" => "businessid=$businessidd AND isdelete = 0"));

        $Order_str = '';
        if (count($businessid_info) != 0)
        {
            $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId=$businessidd AND isdelete = 0"));

            if (count($subbusiness_info) != 0)
            {
                foreach ($subbusiness_info as $l => $y)
                {
                    $attachmentId = $y->_attachmentId;
                    $attachinfo = $attachment_model->find(array('condition' => "attachmentid = $attachmentId"));
                    $Order_str .= "{'attachmentId':'$attachmentId','attachmentname':'$attachinfo->attachmentname','printNumber':'$y->printNumbers','paidMoney':'$y->paidMoney','printSet':'$y->printSet','payType':'$y->payType','isPay':'$y->isPay','status':'$y->status','marchineId':'$y->marchineId','subbusinessId':'$y->subbusinessId','isrefund':'$y->isrefund'},";
                }
                $Order_str = substr($Order_str, 0, -1);
                $returnMessage = "{'resultCode':200,'resultDescription': '获取订单详情成功','orderDetail': ["
                        . $Order_str
                        . "]}";
                $returnMessage = str_replace("'", '"', $returnMessage);
                echo $returnMessage;
            }
            else
            {
                $returnMessage = '{"resultCode":401,"resultDescription":"此订单没有数据！"}';
                echo $returnMessage;
            }
        }
        else
        {
            $returnMessage = '{"resultCode":400,"resultDescription":"此订单不存在或着已被删除！"}';
            echo $returnMessage;
        }
    }

    public function actiongetPoints()
    {//获取剩余积分
        $openId = $_POST["openId"];
        $wechat_model = wechat::model();
        $wechat_info = $wechat_model->find(array('condition' => "weiOpenId='$openId'"));

        $userid = $wechat_info->_userId;
        $user_model = user::model();
        $user_info = $user_model->findByPk($userid);
        $username = $user_info->username;
        $phone = $user_info->phone;
        $email = $user_info->email;
        $record_model = record::model();
        $integration = (int) $record_model->find(array("condition" => "userid = '$userid'"))->points;

        $returnMessage = '{"resultCode":200,"resultDescription":"请求成功！","username":"' . $username . '","phone":"' . $phone . '","email":"' . $email . '","integration":"' . $integration . '"}';
        echo $returnMessage;
    }

    public function actionpayOnline()
    {//线上支付
        $orderId = $_POST["out_trade_no"]; //orderId
        $total_fee = $_POST['total_fee']; //总支付金额
        $user_model = user::model();
        $business_model = business::model();
        $subbusiness_model = subbusiness::model();
        $record_model = record::model();
        $integralDetails_model = new integralDetails();

        $business_info = $business_model->find(array('condition' => "orderId=$orderId"));
        $user_infoss = $user_model->find(array('condition' => "userid = '$business_info->_userid'"));

        $record_info = $record_model->find(array('condition' => "userid = '$business_info->_userid'"));
        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessid = $business_info->businessid"));
        foreach ($subbusiness_info as $subbus)
        {
            if ($subbus->isPay == 1)
            {
                $status = false;
            }
            else
            {
                $status = true;
            }
        }
        if ($status)
        {
            $addintegration = $total_fee * 10; //需要增加的积分
            //需要减去的积分
            $integration = ($business_info->paidMoney - $total_fee) * 100;
            if ($integration != 0)
            {
                $integralDetails_info = new integralDetails();
                $record_info->points -= $integration;
                $integralDetails_info->_userid = $business_info->_userid;
                $integralDetails_info->addIntegral = 0;
                $integralDetails_info->reduceIntegral = $integration;
                date_default_timezone_set('PRC');
                $integralDetails_info->happentime = date('Y-m-d H:i:s');
                $integralDetails_info->happenInfo = "支付订单号" . $orderId . "扣积分";
                $integralDetails_info->save();
            }

            $record_info->points +=$addintegration;
            $integralDetails_model->_userid = $business_info->_userid;
            $integralDetails_model->addIntegral = $addintegration;
            $integralDetails_model->reduceIntegral = 0;
            date_default_timezone_set('PRC');
            $integralDetails_model->happentime = date('Y-m-d H:i:s');
            $integralDetails_model->happenInfo = "支付订单号" . $orderId . "送积分";
            $integralDetails_model->save();


            foreach ($subbusiness_info as $subbus)
            {
                $subbus->isPay = 1;
                $subbus->payType = 7; //7微信支付

                date_default_timezone_set('PRC');

                $subbus->payTime = date('Y-m-d H:i:s');
                $subbus->save();
            }

            if (!isset($business_info->verificationCode))
            {//为空
                $verificationCode = mt_rand(100000, 999999); //验证码
                $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一

                while ($verificationCodes)
                {
                    $verificationCode = mt_rand(100000, 999999);
                    $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一
                }
                $user = 'cqutprint'; //短信接口用户名 $user
                $pwd = '112233'; //短信接口密码 $pwd
                $mobiles = $user_infoss->phone; //说明：取用户输入的手号
                $contents = "亲爱的用户，您此次打印的验证码为" . $verificationCode . ",您已支付成功，请您于终端机打印,谢谢！【颇闰科技】"; //说明：取用户输入的短信内容
                $chid = 0; //通道ID
                $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";

                $business_info->verificationCode = $verificationCode;
                $business_info->consumptionIntegral = $integration;
                file_get_contents($sendMessage);
            }
            else
                $verificationCode = $business_info->verificationCode;

            if ($business_info->save() && $record_info->save())
            {
                $returnMessage = '{"resultCode":200,"resultDescription":"保存成功！"}';
                echo $returnMessage;
            }
            else
            {
                $returnMessage = '{"resultCode":400,"resultDescription":"保存失败！"}';
                echo $returnMessage;
            }
        }
        else
        {
            $returnMessage = '{"resultCode":200,"resultDescription":"保存成功！"}';
            echo $returnMessage;
        }
    }

    public function actionpayPoints()
    {//积分支付
        $openId = $_POST["openId"];
        $wechat_model = wechat::model();
        $wechat_info = $wechat_model->find(array('condition' => "weiOpenId='$openId'"));
        $userid = $wechat_info->_userId;

        $points = $_POST['points']; //需要减去的积分

        $user_model = user::model();
        $business_model = business::model();
        $subbusiness_model = subbusiness::model();
        $record_model = record::model();
        $integralDetails_model = new integralDetails();

        $user_info = $user_model->find(array('condition' => "userid = '$userid'"));

        $orderId = $_POST['orderId']; //订单号
        $business_info = $business_model->find(array('condition' => "orderId = $orderId"));

        $record_info = $record_model->find(array('condition' => "userid = '$user_info->userid'"));

        $record_info->points -= $points;
//        $user_info->integration -= $integration;
        $integralDetails_model->_userid = $user_info->userid;
        $integralDetails_model->addIntegral = 0;
        $integralDetails_model->reduceIntegral = $points;
        date_default_timezone_set('PRC');

        $integralDetails_model->happentime = date('Y-m-d H:i:s');
        $integralDetails_model->happenInfo = "支付订单号" . $orderId . "扣积分";

        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessid = $business_info->businessid"));

        foreach ($subbusiness_info as $subbus)
        {
            $subbus->isPay = 1;

            $subbus->payType = 5;
            date_default_timezone_set('PRC');

            $subbus->payTime = date('Y-m-d H:i:s');

            $subbus->save();
        }

        if (!isset($business_info->verificationCode))
        {//为空
            $verificationCode = mt_rand(100000, 999999); //验证码
            $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'"));

            while ($verificationCodes)
            {
                $verificationCode = mt_rand(100000, 999999);
                $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断验证码是否唯一
            }
            $user = 'cqutprint'; //短信接口用户名 $user
            $pwd = '112233'; //短信接口密码 $pwd
            $mobiles = $user_info->phone; //说明：取用户输入的手号
            $contents = "亲爱的用户，您此次打印的验证码为" . $verificationCode . ",您已支付成功，请您于终端机打印,谢谢！【颇闰科技】"; //说明：取用户输入的短信内容
            $chid = 0; //通道ID
//
//        date_default_timezone_set('PRC');
//        $sendtime = date('Y-m-d H:i:s'); //发送时间
            $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";

            $business_info->verificationCode = $verificationCode;
            $business_info->consumptionIntegral = $integration;
            file_get_contents($sendMessage);
        }
        else
            $verificationCode = $business_info->verificationCode;

        if ($user_info->save() && $business_info->save() && $record_info->save() && $integralDetails_model->save())
        {
            $returnMessage = '{"resultCode":200,"resultDescription":"支付成功！"}';
            echo $returnMessage;
        }
        else
        {
            $returnMessage = '{"resultCode":400,"resultDescription":"支付失败！"}';
            echo $returnMessage;
        }
    }

    public function actionpayLine()
    {//线下支付
        $orderId = $_POST["orderId"];
        $openId = $_POST["openId"];
        $wechat_model = wechat::model();
        $user_model = user::model();
        $wechat_info = $wechat_model->find(array('condition' => "weiOpenId='$openId'"));
        $userid = $wechat_info->_userId;

        $user_info = $user_model->find(array('condition' => "userid='$userid'"));


        $business_model = business::model();
        $subbusiness_model = subbusiness::model();

        $business_info = $business_model->find(array('condition' => "orderId = $orderId"));

        $subbusiness_info = $subbusiness_model->findAll(array('condition' => "_businessid = $business_info->businessid"));

        foreach ($subbusiness_info as $subbus)
        {
            $subbus->payType = 0;
            $r = date_default_timezone_set('PRC');

            $subbus->payTime = date('Y-m-d H:i:s');

            $subbus->save();
        }
        if ($business_info->verificationCode != null)
        {
            $returnMessage = '{"resultCode":200,"resultDescription":"支付成功！"}';
            echo $returnMessage;
        }
        else
        {
            $verificationCode = mt_rand(100000, 999999); //验证码
            $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一

            while ($verificationCodes)
            {
                $verificationCode = mt_rand(100000, 999999);
                $verificationCodes = $business_model->find(array('condition' => "verificationCode = '$verificationCode'")); //判断订单号是否唯一
            }

            $user = 'cqutprint'; //短信接口用户名 $user
            $pwd = '112233'; //短信接口密码 $pwd
            $mobiles = $user_info->phone; //说明：取用户输入的手号
            $contents = "亲爱的用户，您此次打印的验证码为" . $verificationCode . ",您选择的支付方式是线下支付，请您于终端机支付打印,谢谢！【颇闰科技】"; //说明：取用户输入的短信内容
            $chid = 0; //通道ID
//
//        date_default_timezone_set('PRC');
//        $sendtime = date('Y-m-d H:i:s'); //发送时间
            $sendMessage = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user={$user}&pwd={$pwd}&mobiles={$mobiles}&contents={$contents}&chid={$chid}&sendtime=";

            $business_info->verificationCode = $verificationCode;
            file_get_contents($sendMessage);
            if ($business_info->save())
            {
                $returnMessage = '{"resultCode":200,"resultDescription":"支付成功！"}';
                echo $returnMessage;
            }
            else
            {
                $returnMessage = '{"resultCode":400,"resultDescription":"支付失败！"}';
                echo $returnMessage;
            }
        }
    }

}
