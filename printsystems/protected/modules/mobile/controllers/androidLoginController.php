<?php

header('content-type:application/json;charset=utf8');

class androidLoginController extends Controller {

    public function __construct() {
        date_default_timezone_set('PRC');
        ini_set('session.gc_maxlifetime', 180);
        ini_set("session.gc_divisor", 1);
        session_start();
    }

    public function actionGetSchoolList() {
        $school_model = store::model();
        $school_info = $school_model->findAll();
        $result;
        $schoolList = array();
        if (count($school_info) > 0) {
            foreach ($school_info as $v) {
                $schoolArray = array(
                    "storeid" => $v->storeid,
                    "storename" => $v->storename,
                    "addtime" => $v->addtime
                );
                array_push($schoolList, $schoolArray);
            }

            $result = array(
                'resultCode' => 200,
                'resultDescription' => '获取成功',
                'list' => $schoolList
            );
        } else {
            $result = array(
                'resultCode' => 400,
                'resultDescription' => '院校列表为空',
            );
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public function actionLogin() {
        if (isset($_SESSION['user']) && $_SESSION['user'] != null) {
            $return = array('resultCode' => 403, 'resultDescription' => '您已登陆');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $username = isset($_POST['username']) ? trim($_POST['username']) : '';
        $pwd = isset($_POST['pwd']) ? trim($_POST['pwd']) : '';

        if ($username == '' || $pwd == '') {
            $return = array('resultCode' => 401, 'resultDescription' => '用户名和密码不能为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $user_model = new user();
        $usernameR = $user_model->findAll(array('condition' => "username='$username' OR phone='$username'"));
//        print_r($usernameR);
//        exit();
        if ($usernameR) {
            for ($i = 0; $i < count($usernameR); $i++) {
                if (md5($pwd) == $usernameR[$i]->userpsw) {
                    Yii::app()->request->cookies['username'] = new CHttpCookie('username', $usernameR[$i]->username);
                    Yii::app()->request->cookies['pwd'] = new CHttpCookie('pwd', $pwd);
                    Yii::app()->request->cookies['phone'] = new CHttpCookie('phone', $usernameR[$i]->phone);

                    $_SESSION['user'] = array('userid' => $usernameR[$i]->userid, 'username' => $usernameR[$i]->username, 'phone' => $usernameR[$i]->phone);
                    $sessionID = new CHttpCookie('PHPSESSID', session_id());
                    Yii::app()->request->cookies['PHPSESSID'] = $sessionID;
                    $return = array('resultCode' => 200, 'resultDescription' => '登陆成功');
                    echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                    return;
                }
            }
        }
        $return = array('resultCode' => 402, 'resultDescription' => '用户名或密码错误');
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    public function actionRegister() {
        if (!isset($_POST) || empty($_POST)) {
            $return = array('resultCode' => 401, 'resultDescription' => '用户名或密码不能为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $user_model = new user();
        $pwd = isset($_POST['pwd']) ? trim($_POST['pwd']) : '';
        $pwd2 = isset($_POST['pwd2']) ? trim($_POST['pwd2']) : '';
        $phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
        $username = isset($_POST['username']) ? trim($_POST['username']) : '';
        $code = isset($_POST['code']) ? trim($_POST['code']) : '';
        $schoolName = isset($_POST['schoolName']) ? trim($_POST['schoolName']) : '';
        if (!isset($_SESSION['register']['phone']) || $phone == '' || $phone != $_SESSION['register']['phone']) {
            $return = array('resultCode' => 405, 'resultDescription' => '用户验证手机号不一致');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        if (!isset($_SESSION['register']['code']) || $code == '' || $code != $_SESSION['register']['code']) {
            $return = array('resultCode' => 406, 'resultDescription' => '验证码错误');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        if ($pwd != $pwd2) {
            $return = array('resultCode' => 404, 'resultDescription' => '两次密码不一致');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $returnjson = $this->checkuser($username);
        if ($returnjson != NULL) {
            echo $returnjson;
            return;
        }

        $returnjson = $this->checkphone($phone);
        if ($returnjson != NULL) {
            echo $returnjson;
            return;
        }
        if ($schoolName != "") {
            $store_model = store::model();
            $store_info = $store_model->find("storename = '" . $schoolName . "'");

            if ($store_info) {

                $user_model->_storeid = $store_info->storeid;
            }
        }
        $user_model->username = $username;
        $user_model->userpsw = md5($pwd);
        $user_model->phone = $phone;
        $user_model->registertime = date('Y-m-d H:i:s');
        if ($user_model->save()) {
            unset($_SESSION['register']);
            $userinfo = $user_model->find(array('condition' => "username='$username'"));
            $record_model = new record();
            $record_model->userid = $userinfo['userid'];
            $record_model->points = 200;
            $record_model->save();
            $indegral_model = new integraldetails();
            $indegral_model->_userid = $userinfo['userid'];
            $indegral_model->addIntegral = 200;
            $indegral_model->happentime = date('Y-m-d H:i:s');
            $indegral_model->happenInfo = '注册送积分';
            $indegral_model->save();
            $return = array('resultCode' => 200, 'resultDescription' => '注册成功', 'code' => $code);
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
    }

    public function actionfindPwd() {
        if (!isset($_POST) || empty($_POST)) {
            $return = array('resultCode' => 401, 'resultDescription' => '手机号或验证不能为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
        $code = isset($_POST['code']) ? trim($_POST['code']) : '';
        if (!isset($_SESSION['register']['phone']) || $phone == '' || $phone != $_SESSION['register']['phone']) {
            $return = array('resultCode' => 405, 'resultDescription' => '用户验证手机号不一致');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        if (!isset($_SESSION['register']['code']) || $code == '' || $code != $_SESSION['register']['code']) {
            $return = array('resultCode' => 406, 'resultDescription' => '验证码错误');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }

        $user_model = new user();
        $usernameR = $user_model->find(array('condition' => "phone='$phone'"));
        if ($usernameR) {
            $return = array('resultCode' => 200, 'resultDescription' => '验证通过', 'username' => $usernameR->username, 'phone' => $usernameR->phone);
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $return = array('resultCode' => 402, 'resultDescription' => '用户绑定手机号不一致');
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    public function actionresetPwd() {
        if (!isset($_POST) || empty($_POST)) {
            $return = array('resultCode' => 401, 'resultDescription' => '手机号或验证不能为空');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $username = isset($_POST['username']) ? trim($_POST['username']) : '';
        $phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
        $pwd = isset($_POST['pwd']) ? trim($_POST['pwd']) : '';
        $pwd2 = isset($_POST['pwd2']) ? trim($_POST['pwd2']) : '';
        if ($pwd != $pwd2) {
            $return = array('resultCode' => 404, 'resultDescription' => '两次密码不一致');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $user_model = new user();
        $usernameR = $user_model->find(array('condition' => "username='$username' AND phone='$phone'"));
        if ($usernameR) {
            $usernameR->userpsw = md5($pwd);
            if ($usernameR->save()) {
                $return = array('resultCode' => 200, 'resultDescription' => '重置成功');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
            $return = array('resultCode' => 405, 'resultDescription' => '重置失败');
            echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
            return;
        }
        $return = array('resultCode' => 402, 'resultDescription' => '用户绑定手机号不一致');
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    private function genCode($len = 6) {
        if (intval($len) <= 0) {
            return '';
        }
        $chars = '0123456789';
        $char = substr($chars, rand(0, 9), 1);
        $code = $char;
        $len2 = 1;
        while ($len2 < $len) {
            $chars2 = substr($chars, 0, intval($char)) . substr($chars, intval($char) + 1);
            $char = substr($chars2, rand(0, 8), 1);
            $code.=$char;
            $len2 = $len2 + 1;
        }
        return $code;
    }

    public function actionsendCode() {
        if (isset($_POST)) {
            $mobiles = isset($_POST['mobiles']) ? trim($_POST['mobiles']) : '';
            $type = isset($_POST['type']) ? trim($_POST['type']) : '';
        }
        $returnjson = $this->checkphone($mobiles);
        if ($type == '' && $returnjson != NULL) {
            echo $returnjson;
            return;
        } elseif ($type != '') {
            $return = json_decode($returnjson);
            if ($return->resultCode != 412) {
                $return = array('resultCode' => 421, 'resultDescription' => '手机号未注册');
                echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                return;
            }
        }

        $code = $this->genCode();
        $_SESSION['register'] = array('code' => $code, 'phone' => $mobiles, 'time' => time());
        $sessionID = new CHttpCookie('PHPSESSID', session_id());
        Yii::app()->request->cookies['PHPSESSID'] = $sessionID;
        $contents = '【颇闰科技】欢迎使用颇闰产品，您的验证码为' . $code;
        $url = "http://www.106008.com/WebAPI/SmsAPI.asmx/SendSmsExt?user=cqutprint&pwd=112233&mobiles=$mobiles&contents=$contents&chid=0&sendtime=";
        file_get_contents($url);
        $return = array('resultCode' => 200, 'resultDescription' => '验证码发送成功', 'code' => $_SESSION['register']['code'],
            'phone' => $_SESSION['register']['phone'], 'time' => $_SESSION['register']['time']);
        echo json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        return;
    }

    public function checkuser($username) {
        if (isset($_POST['username'])) {
            $username = isset($_POST['username']) ? trim($_POST['username']) : '';
        }
        if ($username == '') {
            $return = array('resultCode' => 401, 'resultDescription' => '用户名不能为空');
            return json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
        $user_model = new user();
        $user_info = $user_model->find("username = '$username'");
        if ($user_info) {
            $return = array('resultCode' => 411, 'resultDescription' => '用户名已注册');
            return json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
        return NULL;
    }

    public function checkphone($phone) {
        if (isset($_POST['phone'])) {
            $phone = isset($_POST['phone']) ? trim($_POST['phone']) : '';
        }
        if ($phone == '') {
            $return = array('resultCode' => 402, 'resultDescription' => '手机号不能为空');
            return json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
        $user_model = new user();
        $user_info = $user_model->find("phone = '$phone'");
        if ($user_info) {
            $return = array('resultCode' => 412, 'resultDescription' => '手机号已注册');
            return json_encode($return, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
        return NULL;
    }

}
