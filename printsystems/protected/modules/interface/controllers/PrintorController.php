<?php

/*
 * 终端向平台获取信息 接口
 */

class printorController extends Controller {

    public function actionSearchOrder($verificationCode = null, $marchineid = null) {//终端拿订单
        $printor_model = printor::model();
        if ($verificationCode == "" || $verificationCode == null) {
            $returnMessage = '{"resultCode":400,"resultDescription":"验证码不能为空"}';
            echo $returnMessage;
        } else {
            $verificationCode = intval($verificationCode);
            $marchineid = addslashes($marchineid);
            $printor_info = $printor_model->find("machineId = '$marchineid'");
            if (count($printor_info) != 0) {
                $business_model = business::model();
                $business_info = $business_model->find(array("condition" => "verificationCode= $verificationCode AND isdelete = 0"));
                if (count($business_info) != 0) {
                    if ($business_info->_storeid == $printor_info->_storeId) {
                        $businessid = $business_info->businessid;
                        $orderId = $business_info->orderId;
                        $subbusiness_model = subbusiness::model();
                        $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId= $businessid AND isrefund = 0")); //"_businessId = $businessid"
                        if (count($subbusiness_info) != 0) {
                            $attachment_model = attachment::model();
                            $str = '';

                            require_once './oss/samples/Common.php';
                            $bucket = "porunoss";

                            $timeout = 3600;
                            $ossClient = Common::getOssClient();

                            foreach ($subbusiness_info as $K => $V) {

                                $attachmentId = $V->_attachmentId;
                                $attachment_info = $attachment_model->findByPk($attachmentId);

                                if (count($attachment_info) != 0) {
                                    $object = $attachment_info->savepath . $attachment_info->attachmentfile . "-" . $attachment_info->attachmentname;
                                    $filePath = $ossClient->signUrl($bucket, $object, $timeout);

                                    $str .= "{'attachmentId':'$attachmentId','attachmentName':'$attachment_info->attachmentname','fileNumber':'$attachment_info->filenumber','filePath':'$filePath','isPay':'$V->isPay','payType':'$V->payType','payTime':'$V->payTime','paidMoney':'$V->paidMoney','printNumbers':'$V->printNumbers','printSet':'$V->printSet','successNumbers':'$V->successNumbers','errorPage':'$V->errorPage','printTime':'$V->printTime','trade_no':'$V->trade_no','status':'$V->status','double_sided':'$V->double_sided'},";
                                }
                            }
                            $str = substr($str, 0, -1);
                            $returnMessage = "{'resultCode':200,'resultDescription': '获取订单成功(包含所有文件)','orderId':'$orderId','file': ["
                                    . $str
                                    . "]}";
                            $returnMessage = str_replace("'", '"', $returnMessage);
                            echo $returnMessage;
                        } else {
                            $returnMessage = '{"resultCode":400,"resultDescription":"子订单不存在或已失效！"}';
                            echo $returnMessage;
                        }
                    } else {
                        $store_model = store::model();
                        $store_info = $store_model->findByPk($business_info->_storeid);
                        if (count($store_info) != 0) {
                            $returnMessage = '{"resultCode":400,"resultDescription":"此订单应在' . $store_info->storename . '打印！"}';
                            echo $returnMessage;
                        } else {
                            $returnMessage = '{"resultCode":400,"resultDescription":"此订单不应在此打印！"}';
                            echo $returnMessage;
                        }
                    }
                } else {
                    $returnMessage = '{"resultCode":400,"resultDescription":"订单不存在或已失效！"}';
                    echo $returnMessage;
                }
            } else {
                $returnMessage = '{"resultCode":400,"resultDescription":"此终端不存在！"}';
                echo $returnMessage;
            }
        }
    }

    //新版终端拿订单，后面会更改为这种
    public function actionSearchOrder_new($verificationCode = null, $marchineid = null) {//终端拿订单
        $printor_model = printor::model();
        if ($verificationCode == "" || $verificationCode == null) {
            $returnMessage = '{"resultCode":400,"resultDescription":"验证码不能为空"}';
            echo $returnMessage;
        } else {
            $verificationCode = intval($verificationCode);
            $marchineid = addslashes($marchineid);
            $printor_info = $printor_model->find("machineId = '$marchineid'");
            if (count($printor_info) != 0) {
                $business_model = business::model();
                $business_info = $business_model->find(array("condition" => "verificationCode= $verificationCode AND isdelete = 0"));

                header("Content-type: text/html; charset=utf-8");

                if (count($business_info) != 0) {
                    if ($business_info->_storeid == $printor_info->_storeId) {
                        $businessid = $business_info->businessid;
                        $orderId = $business_info->orderId;
                        $subbusiness_model = subbusiness::model();
                        $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId= $businessid AND isrefund = 0")); //"_businessId = $businessid"
                        if (count($subbusiness_info) != 0) {
                            $attachment_model = attachment::model();
                            $str = '';

                            foreach ($subbusiness_info as $K => $V) {

                                $attachmentId = $V->_attachmentId;
                                $attachment_info = $attachment_model->findByPk($attachmentId);
                                if (count($attachment_info) != 0) {
                                    $filePath = $attachment_info->savepath . $attachment_info->attachmentfile . "-" . $attachment_info->attachmentname;
                                    $str .= "{'attachmentId':'$attachmentId','attachmentName':'$attachment_info->attachmentname','fileNumber':'$attachment_info->filenumber','filePath':'$filePath','isPay':'$V->isPay','payType':'$V->payType','payTime':'$V->payTime','paidMoney':'$V->paidMoney','printNumbers':'$V->printNumbers','printSet':'$V->printSet','successNumbers':'$V->successNumbers','errorPage':'$V->errorPage','printTime':'$V->printTime','trade_no':'$V->trade_no','status':'$V->status','double_sided':'$V->double_sided'},";
                                }
                            }
                            $str = substr($str, 0, -1);
                            $returnMessage = "{'resultCode':200,'resultDescription': '获取订单成功(包含所有文件)','orderId':'$orderId','file': ["
                                    . $str
                                    . "]}";
                            $returnMessage = str_replace("'", '"', $returnMessage);
                            echo $returnMessage;
                        } else {
                            $returnMessage = '{"resultCode":400,"resultDescription":"子订单不存在或已失效！"}';
                            echo $returnMessage;
                        }
                    } else {
                        $store_model = store::model();
                        $store_info = $store_model->findByPk($business_info->_storeid);
                        if (count($store_info) != 0) {
                            $returnMessage = '{"resultCode":400,"resultDescription":"此订单应在' . $store_info->storename . '打印！"}';
                            echo $returnMessage;
                        } else {
                            $returnMessage = '{"resultCode":400,"resultDescription":"此订单不应在此打印！"}';
                            echo $returnMessage;
                        }
                    }
                } else {
                    $returnMessage = '{"resultCode":400,"resultDescription":"订单不存在或已失效！"}';
                    echo $returnMessage;
                }
            } else {
                $returnMessage = '{"resultCode":400,"resultDescription":"此终端不存在！"}';
                echo $returnMessage;
            }
        }
    }

    public function actionOperateOrder() {//线下订单处理
        //$_POST['json'] = '{"orderId":"20160525104611737023671","filestatus":[{"attachmentid":"38944","marchineId":"CQUT-PRINT02","isPay":"1","payType":"5","payTime":"20150303123040","paidMoney":"0.42","printNumbers":"1","printSet":"1-7", "successNumbers":"7", "errorPage":"0", "printTime":"20150905120930", "status":"1","trade_no":"123","double_sided":"1"},{"attachmentid":"38921","marchineId":"CQUT-PRINT02","isPay":"1","payType":"5","payTime":"20150303123040","paidMoney":"0.18","printNumbers":"1","printSet":"1-3", "successNumbers":"3", "errorPage":"1", "printTime":"20150905120930", "status":"1","trade_no":"201415210212","double_sided":"1"}]}';
        if (!empty($_POST)) {
            if (is_array($_POST['json'])) {//是数组
                $arrayCode = $_POST['json'];
            } else {
                $arrayCode = CJSON::decode($_POST['json']);
            }
            $orderId = $arrayCode['orderId'];
            $data = $arrayCode['filestatus'];

            $business_model = business::model();
            $business_info = $business_model->find("orderId = $orderId");
            $businessId = $business_info->businessid;
            $subbusiness_model = subbusiness::model();

            $attachment_model = attachment::model();
            $library_model = library::model();
            $user_model = user::model();

            $flag = 1;

            foreach ($data as $K => $V) {
                $attachmentid = $V['attachmentid'];
                $subbusiness_info = $subbusiness_model->find("_attachmentId = $attachmentid AND _businessId =$businessId");

                $attachment_info = $attachment_model->find("attachmentid = $attachmentid");
                if ($V['status'] == 1 && $business_info->_userid != $attachment_info->_userid && $subbusiness_info->status != 1) {//如果不是本人的文档，则给文档分享者加积分
                    $library_info = $library_model->find("attachmentid = $attachmentid");
                    $library_info->printcnt += 1;
                    $library_info->save();
                    $user_info = $user_model->find("userid = $attachment_info->_userid");
                    $user_info->integration += $library_info->credit;
                    $user_info->save();
                    $integralDetails_model = new integralDetails;
                    $integralDetails_model->_userid = $attachment_info->_userid;
                    $integralDetails_model->addIntegral = $library_info->credit;
                    $integralDetails_model->reduceIntegral = 0;
                    $integralDetails_model->happentime = date('Y-m-d H:i:s');
                    $integralDetails_model->happenInfo = "打印分享文档'" . $attachment_info->attachmentname . "'获得积分";
                    $integralDetails_model->save();
                }
                $subbusiness_info->marchineId = $V['marchineId'];
                $subbusiness_info->isPay = $V['isPay'];
                $subbusiness_info->payType = $V['payType'];
                $subbusiness_info->paidMoney = $V['paidMoney'];
                $subbusiness_info->printNumbers = $V['printNumbers'];
                $subbusiness_info->printSet = $V["printSet"];
                $subbusiness_info->successNumbers = $V['successNumbers'];
                $subbusiness_info->errorPage = $V['errorPage'];
                date_default_timezone_set('PRC');
                $subbusiness_info->printTime = date('Y-m-d H:i:s');
                $subbusiness_info->status = $V['status'];
                $subbusiness_info->trade_no = $V['trade_no'];
                $subbusiness_info->double_sided = $V['double_sided'];
                if (!$subbusiness_info->save()) {
                    $flag = 0;
                }
                $subbusiness_infos = $subbusiness_model->findAll("_businessId =$businessId");
                $allprint = 1; //已全部打印
                foreach ($subbusiness_infos as $k => $l) {
                    if ($l->status != 1) {
                        $allprint = 0; //未全部打印
                    }
                }
            }
            if ($allprint == 1) {
                $business_info->status = 2;
            }

            if ($business_info->save() && $flag == 1) {
                $returnMessage = '{"resultCode":200,"resultDescription":"信息保存成功！"}';
                echo $returnMessage;
            } else {
                $returnMessage = '{"resultCode":400,"resultDescription":"信息保存失败！"}';
                echo $returnMessage;
            }
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"未获取到信息！"}';
            echo $returnMessage;
        }
    }

    public function actionPrintState() {//拿终端状态
//        $_POST['json'] = '{"machineId":"CQUT-PRINT03","paper_remain":"111","yuan_remain":"123","jiao_remain":"33","jiao_changer":"1","yuan_changer":"1","coin_acceptor":"1","note_acceptor":"1","card_acceptor":"1","printer_status":"1","printer_errorcode":"1","current_version":"1.0.0.1"}';
        if (!empty($_POST)) {
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            try {
                if (isset($arrayPrintor['machineId'])) {
                    $machineId = $arrayPrintor['machineId']; //终端标识
                } else {
                    $machineId = "";
                }
                if (isset($arrayPrintor['paper_remain'])) {
                    $paper_remain = $arrayPrintor['paper_remain']; //剩余纸张
                } else {
                    $paper_remain = 0;
                }
                if (isset($arrayPrintor['printer_status'])) {
                    $printer_status = $arrayPrintor['printer_status']; //打印机状态
                } else {
                    $printer_status = 2;
                }
                if (isset($arrayPrintor['printer_errorcode'])) {
                    $printer_errorcode = $arrayPrintor['printer_errorcode']; //打印机出错码
                } else {
                    $printer_errorcode = "";
                }
                if (isset($arrayPrintor['printer_errorinfo'])) {
                    $printer_errorinfo = $arrayPrintor['printer_errorinfo']; //打印机出错信息
                } else {
                    $printer_errorinfo = "就绪";
                }
                if (isset($arrayPrintor['current_version'])) {
                    $current_version = $arrayPrintor['current_version']; //终端版本
                } else {
                    $current_version = ""; //终端版本
                }
            } catch (Exception $e) {
                $return = '{"resultCode":300,"resultDescription":"未知错误"}';
                echo $return;
            }
            $printor_model = printor::model();
            $printor_info = $printor_model->find(array('condition' => "machineId='$machineId'"));
            if ($printor_info) {
                $printor_info->machineId = $machineId;
                $printor_info->paper_remain = $paper_remain;
                $printor_info->printer_status = $printer_status;
                $printor_info->printer_errorcode = $printer_errorcode;
                $printor_info->printer_errorinfo = $printer_errorinfo;
                if ($paper_remain < 100 || ($printer_status != 0 && $printer_status != 1)) {
                    $printor_info->printer_error_time = date('Y-m-d H:i:s');
                } else {
                    $printor_info->printer_error_time = NULL;
                }
                $printor_info->version = $current_version;
                if ($printor_info->save()) {
                    $return = '{"resultCode":200,"resultDescription":"状态传输成功"}';
                    echo $return;

                    /*                     * *************如果有问题则发送短信给管理员***************************** */
                    if ($paper_remain < 100 || ($printer_status != 0 && $printer_status != 1)) {
                        if ($paper_remain < 100) {
                            $contents = "尊敬的管理员，" . $printor_info->printorName . "的纸张已不足100张，请及时添加！【颇闰科技】";
                            $message = array(
                                'description' => $contents
                            );
                        }

                        if ($printer_status != 0 && $printer_status != 1) {
                            $contents = "尊敬的管理员，" . $printor_info->printorName . "的打印机出现故障，请及时排除！【颇闰科技】";
                            $message = array(
                                'description' => $contents
                            );
                        }
                        $storeid = $printor_info->_storeId;
                        if (!empty($storeid)) {
                            $administrator_model = administrator::model();
                            $administrator_info = $administrator_model->find(array('condition' => "_storeid=$storeid"));
                            $pushChannelids = explode(',', $administrator_info['pushChannelid']);
                            foreach ($pushChannelids as $k => $pushChannelid) {
                                if (!empty($pushChannelid)) {
                                    $this->actionBiaduPush($message, $pushChannelid);
                                }
                            }
                        }
                    }
                    /*                     * ****************************************** */
                } else {
                    $return = '{"resultCode":400,"resultDescription":"状态保存失败"}';
                    echo $return;
                }
            } else {
                $return = '{"resultCode":400,"resultDescription":"没找到该终端"}';
                echo $return;
            }
        } else {
            $return = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $return;
        }
    }

    public function actionBiaduPush($message, $pushChannelid) {
        require_once 'BaiduPush/sdk.php';

// 创建SDK对象.
        $sdk = new PushSDK();

// 消息控制选项。
        $opts = array(
            'msg_type' => 0,
        );
        $optss = array(
            'msg_type' => 1,
        );
// 发送
        $rs = $sdk->pushMsgToSingleDevice($pushChannelid, $message, $opts);
        $rs = $sdk->pushMsgToSingleDevice($pushChannelid, $message, $optss);
    }

    public function actionPostBusinessInfo() {//终端直接打印业务信息
//$_POST['json'] = '{"machineId":"A","sessionId":"111","udiskId":"123","filename":"33","totalpages":"1","copies":"1","printset":"1","paytype":"1","paidmoney":"1","needpay":"1","exchange":"1","starttime":"20150907153040","endtime":"20150907153040","exit_phrase":"6","translation_status":"","print_status":"1","trade_no":"123123"}';
//        $_POST['json'] = '{"machineId": "O97KIL261","sessionId": 1464663986,"udiskId": "","filename": "D:convertedpdf294098916.pdf","totalpages": 5,"copies": 1,"printset": "5-1","is_duplex_page": 1,"paytype": 5,"paidmoney": 0.05,"needpay": 0.05,"exchange": 0.0,"starttime": "2016-05-31T11:06:26","endtime": "2016-05-31T11:08:38","exit_phrase": 5,"translation_status": -1,"print_status": 1,"trade_no": "2016053121001004507192054368"}';
//        $_POST['json'] = '{"machineId": "CQUT-PRINT16","sessionId": 1460279324,"udiskId": "","filename": "D:convertedpdf（本部）毕业实习&毕业论文计划.pdf","totalpages": 1,"copies": 1,"printset": "1-1","is_duplex_page": 1,"paytype": 3,"paidmoney": 0.2,"needpay": 0.2,"exchange": 0.0,"starttime": "2016-04-10T17:08:44","endtime": "2016-04-10T17:09:47","exit_phrase": 6,"translation_status": 1,"print_status": 4,"trade_no": "2016041021001004140239323943"}';
        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            $sessionId = $arrayPrintor['sessionId'];
            $trade_no = $arrayPrintor['trade_no'];
            $exit_phrase = $arrayPrintor['exit_phrase'];
            $totalpages = $arrayPrintor['totalpages']; //需要打印的页数
            if (isset($arrayPrintor['printedpages'])) {
                $printedpages = $arrayPrintor['printedpages']; // 实际打印的页数
            } else {
                $printedpages = 0;
            }
            $machineId = $arrayPrintor['machineId']; // 获取machineId           
            $printor_model = printor::model();
            $printor_info = $printor_model->find("machineId = '$machineId'");

            $prbusiness_model = prbusiness::model();
            $prbusiness_info = $prbusiness_model->find("sessionId = '$sessionId' AND trade_no = '$trade_no'");
            $refund_modei = refund::model();
            $refund_infi = $refund_modei->find("_sessionId = '$sessionId' AND tborderId = '$trade_no'");
//判断是否需要存入退款
            if ($exit_phrase == 5 && $printedpages < $totalpages) {
                if ((count($prbusiness_info) == 0 || (count($prbusiness_info) != 0 && $prbusiness_info->exit_phrase <= 5)) && count($refund_infi) == 0) {
                    include_once("WxPayPubHelper/WxPayPubHelper.php");
                    $wechatpay_model = wechatpay::model();
                    $wechatpay_info = $wechatpay_model->find("transaction_id = '$trade_no'");

                    $refund_model = new refund();
                    $refund_model->_sessionId = $arrayPrintor['sessionId'];
                    $refund_model->tborderId = $arrayPrintor['trade_no'];
                    if ($arrayPrintor['paytype'] == 5) {
                        $refund_model->Integral = $arrayPrintor['paidmoney'] * 100;
                        $refund_model->consumptionIntegral = $arrayPrintor['paidmoney'] * 100;
                    } else {
                        $refund_model->money = $arrayPrintor['paidmoney'];
                    }
                    $refund_model->applyTime = date('Y-m-d H:i:s');
                    if (count($printor_info) != 0)
                        $refund_model->_storeid = $printor_info->_storeId;

//终端状态值 1、现金、；2、刷卡；3、支付宝 4微信支付 5、积分
//数据库状态值 0线下支付 1线上支付宝支付 2一卡通支付 3现金投币支付 4终端扫码支付 5积分支付 6积分+支付宝支付 7终端微信
//退款方式 1 支付宝 2一卡通  3 现金  5 积分 6积分+支付宝 7微信
                    if ($arrayPrintor['paytype'] == 1) {
                        $refund_model->refundType = 3;
                        $refund_model->payType = 3;
                    } else if ($arrayPrintor['paytype'] == 2) {
                        $refund_model->refundType = 2;
                        $refund_model->payType = 2;
                    } else if ($arrayPrintor['paytype'] == 3) {
                        $refund_model->refundType = 1;
                        $refund_model->payType = 4;
                    } else if ($arrayPrintor['paytype'] == 4) {
                        $refund_model->refundType = 7;
                        $refund_model->payType = 7;

                        if (count($wechatpay_info) != 0) {

//微信退款可以自动退款
                            $out_trade_no = $wechatpay_info->out_trade_no;
                            $refund_fee = $arrayPrintor['paidmoney'] * 100;
//商户退款单号，商户自定义，此处仅作举例
                            $out_refund_no = "$out_trade_no";
//总金额需与订单号out_trade_no对应，demo中的所有订单的总金额为1分
                            $total_fee = $wechatpay_info->total_fee;

//使用退款接口
                            $refund = new Refund_pub();
//设置必填参数
//appid已填,商户无需重复填写
//mch_id已填,商户无需重复填写
//noncestr已填,商户无需重复填写
//sign已填,商户无需重复填写
                            $refund->setParameter("out_trade_no", "$out_trade_no"); //商户订单号
                            $refund->setParameter("out_refund_no", "$out_refund_no"); //商户退款单号
                            $refund->setParameter("total_fee", "$total_fee"); //总金额
                            $refund->setParameter("refund_fee", "$refund_fee"); //退款金额
                            $refund->setParameter("op_user_id", WxPayConf_pub::MCHID); //操作员
//非必填参数，商户可根据实际情况选填
//$refund->setParameter("sub_mch_id","XXXX");//子商户号 
//$refund->setParameter("device_info","XXXX");//设备号 
//$refund->setParameter("transaction_id","XXXX");//微信订单号
//调用结果
                            $refundResult = $refund->getResult();
//商户根据实际情况设置相应的处理流程,此处仅作举例
                            if ($refundResult["return_code"] == "FAIL") {
                                $refund_model->statue = 2;
                                echo "通信出错：" . $refundResult['return_msg'] . "<br>";
                            } else {
                                if ($refundResult['result_code'] == "SUCCESS") {
                                    $refund_model->statue = 1;
                                } else {
                                    $refund_model->statue = 2;
                                }
                                $refund_model->refundTime = date('Y-m-d H:i:s');
                            }
                        } else {
                            $refund_model->statue = 1;
                        }
                    } else if ($arrayPrintor['paytype'] == 5) {//积分支付自动退款
                        $refund_model->refundType = 5;
                        $refund_model->payType = 5;

                        $printpaytemp_model = printpaytemp::model();
                        $user_model = user::model();
                        $printpaytemp_info = $printpaytemp_model->find("trade_no = '$trade_no'");
                        if (count($printpaytemp_info) != 0) {
                            $user_info = $user_model->find("phone = '$printpaytemp_info->buyer_logon_id'");
                            if (count($user_info) != 0) {
                                $store_model = store::model();
                                $store_info = $store_model->findByPk($printor_info->_storeId);
                                if ($store_info->active == 0) {//不处于活动期间
                                    $user_info->integration += $arrayPrintor['paidmoney'] * 100;
                                    $integralDetails_model = new integralDetails();
                                    $integralDetails_model->_userid = $user_info->userid;
                                    $integralDetails_model->addIntegral = $arrayPrintor['paidmoney'] * 100;
                                    $integralDetails_model->reduceIntegral = 0;
                                    $integralDetails_model->happentime = date('Y-m-d H:i:s');
                                    $integralDetails_model->happenInfo = "终端订单号" . $trade_no . "退款";
                                    if ($user_info->save() && $integralDetails_model->save()) {
                                        $refund_model->statue = 1;
                                    } else {
                                        $refund_model->statue = 2;
                                    }
                                    $refund_model->_userId = $user_info->userid;
                                    $refund_model->refundTime = date('Y-m-d H:i:s');
                                }
                            }
                        }
                    }
                    $refund_model->save();
                }
                if (count($prbusiness_info) == 0) {
                    $prbusiness_model = new prbusiness();

                    foreach ($arrayPrintor as $K => $V) {
                        $prbusiness_model->$K = $V;
                    }
                    if (count($printor_info) != 0)
                        $prbusiness_model->_storeid = $printor_info->_storeId;
                    $prbusiness_model->save();
                } else if (count($prbusiness_info) != 0) {
                    if ($prbusiness_info->exit_phrase < $exit_phrase) {
                        $prbusiness_info->exit_phrase = $exit_phrase;
                        $prbusiness_info->save();
                    }
                }
            }//判断是否需要存入prbusiness表和refund是否需要删除以前保存错误的退款 
            else if ($arrayPrintor['exit_phrase'] > 5 && $arrayPrintor['exit_phrase'] <= 8) {
                if (count($refund_infi) != 0) {
                    $refund_infi->delete();
                    $refund_infi->save();
                }
                if (count($prbusiness_info) != 0) {
                    $prbusiness_info->exit_phrase = $exit_phrase;
                    $prbusiness_info->save();
                } else {
                    $prbusiness_model = new prbusiness();

                    foreach ($arrayPrintor as $K => $V) {
                        $prbusiness_model->$K = $V;
                    }
                    if (count($printor_info) != 0)
                        $prbusiness_model->_storeid = $printor_info->_storeId;
                    $prbusiness_model->save();
                }
            } else {
                $prbusiness_model = new prbusiness();

                foreach ($arrayPrintor as $K => $V) {
                    $prbusiness_model->$K = $V;
                }
                if (count($printor_info) != 0)
                    $prbusiness_model->_storeid = $printor_info->_storeId;
                $prbusiness_model->save();
            }
            $return = '{"resultCode":200,"resultDescription":"success"}';
            echo $return;
        } else {
            $return = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $return;
        }
    }

    public function actionPostCardTradeInfo() {//终端直接打印业务信息 暂停使用
        if (!empty($_POST)) {
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            $cardtrade_model = new cardtrade();
            foreach ($arrayPrintor as $K => $V) {
                $cardtrade_model->$K = $V;
            }
            if ($cardtrade_model->save()) {
                $return = '{"resultCode":200,"resultDescription":"刷卡信息传输成功"}';
                echo $return;
            } else {
                $return = '{"resultCode":400,"resultDescription":"刷卡信息传输失败"}';
                echo $return;
            }
        } else {
            $return = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $return;
        }
    }

    public function actionVersion($type = null) {//终端版本控制
        $printVersion_model = new printVersion();
        if ($type == null) {
            $printVersion_info = $printVersion_model->find(array('order' => 'versionId DESC'));
            $versionFilePash = Yii::app()->request->hostInfo . '/printVersion/' . $printVersion_info->versionFile;
            $str = "{'versionCode':'$printVersion_info->versionCode','versionFilePash':'$versionFilePash','updateTime':'$printVersion_info->updateTime'}";
            $returnMessage = str_replace("'", '"', $str);
            echo $returnMessage;
        }
    }

    public function actionPayInfo($out_trade_no = null) {//返回支付结果给终端
        $out_trade_no = addslashes($out_trade_no);
        if ($out_trade_no == null) {
            echo '{"resultCode":"400","resultMessage":"没有传递数据"}';
        } else {
            $printpaytemp_model = printpaytemp::model();
            $printpaytemp_info = $printpaytemp_model->find("out_trade_no='$out_trade_no'");

            if (count($printpaytemp_info) != 0) {
                $trade_status = $printpaytemp_info->trade_status;
                $returnMessage = "{'resultCode':'200','resultMessage':'成功','trade_status':'$trade_status'}";
                echo str_replace("'", '"', $returnMessage);
            } else {
                echo '{"resultCode":"400","resultMessage":"信息不存在"}';
            }
        }
    }

    public function actionChangeprintor($machineId) {//是否开关机
        $printor_model = printor::model();
        $machineids = addslashes($machineId);
        $printor_info = $printor_model->find(array('condition' => "machineId='$machineids'"));
        if (count($printor_info) != 0) {
            $printor_info->last_time = time();
            if ($printor_info->save()) {
                $return = '{"resultCode":200,"resultDescription":"状态传输成功"}';
                echo $return;
            } else {
                $return = '{"resultCode":400,"resultDescription":"状态保存失败"}';
                echo $return;
            }
        } else {
            $return = '{"resultCode":400,"resultDescription":"没找到该终端"}';
            echo $return;
        }
    }

//获取广告播放列表
    public function actiongetAdsFileList($marchineId = null) {
        $printor_model = printor::model();
        $advertisement_model = advertisement::model();
        $adsdelivery_model = adsdelivery::model();
        $machineIds = addslashes($marchineId);
        $printor_info = $printor_model->find("machineId = '$machineIds'");

        $str = "";
        if (count($printor_info) != 0) {
            $adsdelivery_info = $adsdelivery_model->findAll("_groupID = $printor_info->_groupID");
            if (count($adsdelivery_info) != 0) {
                require_once './oss/samples/Common.php';
                $bucket = "porunoss";
                $timeout = 3600;

                foreach ($adsdelivery_info as $K => $L) {
                    if ($L->_advertisementID != NULL) {
                        $advertisement_info = $advertisement_model->find("advertisementID = $L->_advertisementID");
                        if (count($advertisement_info) != 0) {
                            $name = $advertisement_info->advertisementName . '.' . $advertisement_info->advertisementFormat;
                            $object = $advertisement_info->advertisementUrl;
                            $ossClient = Common::getOssClient();
                            $versionFilePash = $ossClient->signUrl($bucket, $object, $timeout);

                            $str .= "{'name':'$name','downloadUrl':'$versionFilePash','times':'$L->times','md5':'$advertisement_info->FileMD5','length':'$advertisement_info->advertisementLength'},";
                        }
                    }
                }
                $str = substr($str, 0, -1);
                $returnMessage = "{'resultCode':'200','description': 'success','files': ["
                        . $str
                        . "]}";
                $returnMessage = str_replace("'", '"', $returnMessage);
                echo $returnMessage;
            } else {
                $returnMessage = '{"resultCode":400,"resultDescription":"未找到该播放策略！"}';
                echo $returnMessage;
            }
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"终端机不存在！"}';
            $returnMessage = str_replace("'", '"', $returnMessage);
            echo $returnMessage;
        }
    }

//终端机判断是否联网
    public function actionnetworking() {
        echo "success";
    }

//终端升级程序升级结果状态回传服务器
    public function actionReportUpdateResult() {
//        $_POST['json'] = '{"machineId": "CQUT-PRINTD1","resultCode": "200","resultDescription": "升级成功","clientVersion":"1.0.5.2","starttime": "2015-12-23 14:52:29","endtime": "2015-12-23 14:52:34"}';

        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            $machineId = $arrayPrintor['machineId'];
            $resultCode = $arrayPrintor['resultCode']; //升级结果状态码
            $clientVersion = $arrayPrintor['clientVersion']; //终端程序版本号(不管有无升级成功，都回传)
            $resultDescription = $arrayPrintor['resultDescription']; //描述
            $starttime = $arrayPrintor['starttime']; //
            $endtime = $arrayPrintor['endtime']; //

            $printor_model = printor::model();

            $printor_info = $printor_model->find("machineId = '$machineId'");

            if (count($printor_info) != 0) {
                $printor_info->version = $clientVersion;
                $printor_info->updatetime = date('Y-m-d H:i:s'); //更新时间
                $printor_info->updatestate = $resultCode;
                $printor_info->updatedescript = $resultDescription;
                $printor_info->starttime = $starttime;
                $printor_info->endtime = $endtime;

// 200 升级成功   201 无需升级  400 升级失败 其他未知错误 401 获取服务器版本失败
//402 下载主程序文件失败 403 下载的主程序文件MD5校验不通过 404 解压失败 405 覆盖原程序失败
                if ($printor_info->save()) {
                    $returnMessage = '{"resultCode":200,"resultDescription":"保存成功！"}';
                    echo $returnMessage;
                } else {
                    $returnMessage = '{"resultCode":402,"resultDescription":"保存失败！"}';
                    echo $returnMessage;
                }
            } else {
                $returnMessage = '{"resultCode":401,"resultDescription":"未找到该终端！"}';
                echo $returnMessage;
            }
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"未获取到信息！"}';
            echo $returnMessage;
        }
    }

// 终端获取需要的版本信息
    public function actionGetUpdateInfoById($machineId = null) {
        require_once './oss/samples/Common.php';

//        require_once './oss/src/OSS/OssClient.php';
        $printor_model = printor::model();
        $machineId = addslashes($machineId);
        $printor_info = $printor_model->find("machineId = '$machineId'");
        $str = "";
        if (count($printor_info) != 0) {
            $group_model = group::model();
            $group_info = $group_model->find("groupID = $printor_info->_groupID");
            if (count($group_info) == 0) {
                $returnMessage = '{"resultCode":402,"resultDescription":"未找到该终端所在分组！"}';
                echo $returnMessage;
            } else {
                if ($group_info->_versionId == "") {
                    $returnMessage = '{"resultCode":403,"resultDescription":"该终端未设置升级版本！"}';
                    echo $returnMessage;
                } else {
                    $printversion_model = printVersion::model();
                    $printversion_info = $printversion_model->find("versionId = $group_info->_versionId");
                    if (count($printversion_info) != 0) {
                        $bucket = "porunoss";
                        $object = $printversion_info->Filepath;
                        $timeout = 3600;
                        $ossClient = Common::getOssClient();
                        $versionFilePash = $ossClient->signUrl($bucket, $object, $timeout);

                        $str .= "{'type':'$printversion_info->type','version':'$printversion_info->versionCode','updatestate':'$printor_info->updatestate','url':'$versionFilePash','md5':'$printversion_info->FileMD5'},";
                        $str = substr($str, 0, -1);
                        $returnMessage = str_replace("'", '"', $str);
                        echo $returnMessage;
                    } else {
                        $returnMessage = '{"resultCode":401,"resultDescription":"未找到该版本信息！"}';
                        echo $returnMessage;
                    }
                }
            }
        } else {
            $returnMessage = '{"resultCode":401,"resultDescription":"未找到该终端！"}';
            echo $returnMessage;
        }
    }

//终端查询颇闰服务器后台获取当前交易是否成功
    public function actionGetTradeResult() {
//$_POST['json'] = '{"payMethod":3,"payMethodStr":"ALIPAY","outTradeNo":"CQUT-PRINTD114520512181"}';

        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }

            $payMethod = $arrayPrintor["payMethod"]; //支付方式(3:支付宝；4:微信)
            $payMethodStr = $arrayPrintor["payMethodStr"]; //支付方式描述（ALIPAY/WEIXIN）
            $outTradeNo = $arrayPrintor["outTradeNo"]; //本次交易终端生成的交易号
            if ($payMethod == 3) {
                $printpaytemp_model = printpaytemp::model();
                $printpaytemp_info = $printpaytemp_model->find("out_trade_no = '$outTradeNo'");
                if (count($printpaytemp_info) != 0) {
                    $returnMessage = '{"resultCode":200,"resultDescription":"' . $printpaytemp_info->trade_status . '","tradeNo":"' . $printpaytemp_info->trade_no . '"}';
                    echo $returnMessage;
                } else {
                    $returnMessage = '{"resultCode":400,"resultDescription":"NOT_FOUND"}';
                    echo $returnMessage;
                }
            } else if ($payMethod == 4) {
                $wechatpay_model = wechatpay::model();
                $wechatpay_info = $wechatpay_model->find("out_trade_no = '$outTradeNo'");
                if (count($wechatpay_info) != 0) {
                    $returnMessage = '{"resultCode":200,"resultDescription":"' . $wechatpay_info->result_code . '","tradeNo":"' . $wechatpay_info->transaction_id . '"}';
                    echo $returnMessage;
                } else {
                    $returnMessage = '{"resultCode":400,"resultDescription":"NOT_FOUND"}';
                    echo $returnMessage;
                }
            }
        } else {
            $returnMessage = '{"resultCode":401,"resultDescription":"未获取到信息！"}';
            echo $returnMessage;
        }
    }

//终端上传分享文件记录 
    public function actionterminalsharefiles() {
//        $_POST['json'] = '{"filename": "a.doc","truename": "CQUTLJ-PRINT12","savepath": "home/uploads/2016-02-18/","machineid": "CQUTLJ-PRINT12"}';
        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            $filename = $arrayPrintor['filename']; //文件名
            if (isset($arrayPrintor['filenumber'])) {
                $filenumber = $arrayPrintor['filenumber']; //页数
            } else {
                $filenumber = 0;
            }
            $truename = $arrayPrintor['truename']; //文件名前缀
            $savepath = $arrayPrintor['savepath']; //保存路径
            $machineid = $arrayPrintor['machineid']; //来源于哪个终端

            $terminalshare_model = new terminalshare();
            $printor_model = printor::model();
            $printor_info = $printor_model->find(array("condition" => "machineId = '$machineid'"));

            if (count($printor_info) == 0) {
                $returnMessage = '{"resultCode":401,"resultDescription":"未找到该终端"}';
                echo $returnMessage;
            } else {
                $terminalshare_model->filename = $filename;
                $terminalshare_model->truename = $truename;
                $terminalshare_model->savepath = $savepath;
                $terminalshare_model->printorId = $printor_info->printorId;
                $terminalshare_model->storeId = $printor_info->_storeId;
                $terminalshare_model->uploadtime = date('Y-m-d H:i:s', time());
                $terminalshare_model->statue = 0;
                $terminalshare_model->filenumber = $filenumber;
                if ($terminalshare_model->save()) {
                    $returnMessage = '{"resultCode":200,"resultDescription":"保存成功"}';
                    echo $returnMessage;
                } else {
                    $returnMessage = '{"resultCode":402,"resultDescription":"保存失败"}';
                    echo $returnMessage;
                }
            }
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $returnMessage;
        }
    }

    //终端获取打印价格
    public function actionGetMoney() {
//        $_POST['json'] = '{"actionType": "1","singleTotal": "5","copies": "2","paperSize": "PaperA4","isColor": "0","isDuplex": "0","storeid": "","machineid": "SICNU-SZS-02"}';
//        $_POST['json'] = '{"actionType": "1","singleTotal": "2","copies": "1","paperSize": "PaperA4","isColor": "0","isDuplex": "1","storeid": "","machineid": "CQUT-PRINTD1"}';

        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            $actionType = $arrayPrintor['actionType']; //打印类型 1打印 2扫描 3复印
            $singleTotal = $arrayPrintor['singleTotal']; //单份总页数
            $copies = $arrayPrintor['copies']; //总份数
            $paperSize = $arrayPrintor['paperSize']; //打印尺寸 PaperA3：A3  PaperA4：A4
            $isColor = $arrayPrintor['isColor']; //是否彩印：0黑白；1彩色。默认0
            $isDuplex = $arrayPrintor['isDuplex']; //是否双面：0单面，1双面。默认0
            $machineid = $arrayPrintor['machineid']; //机器id，默认为空

            $price_model = price::model();
            $printor_model = printor::model();

            $printor_info = $printor_model->find("machineId = '$machineid'");
            if (count($printor_info) != 0) {
                $storeid = $printor_info->_storeId;
                $price_info = $price_model->find("storeid = $storeid and type = 2");
                if (count($price_info) != 0) {
                    if ($actionType == "2") {//身份证扫描打印
                        $price = $price_info->scan_price * $copies;
                    } else if ($actionType == "1" && $isDuplex == "0") {//单面打印
                        $price = $price_info->one_side_A4 * $copies * $singleTotal;
                    } else if ($actionType == "1" && $isDuplex == "1") {//双面打印
                        $price = ($price_info->two_side_A4 * floor($singleTotal / 2) + $price_info->one_side_A4 * ($singleTotal % 2)) * $copies;
                    }
                    $returnMessage = '{"code":"200","msg":"请求成功","money":"' . $price . '"}';
                    echo $returnMessage;
                } else {
                    $returnMessage = '{"code":"400","msg":"没有此终端的打印策略","money":""}';
                    echo $returnMessage;
                }
            } else {
                $returnMessage = '{"code":"400","msg":"没有此终端信息","money":""}';
                echo $returnMessage;
            }
        } else {
            $returnMessage = '{"code":"400","msg":"没有数据","money":""}';
            echo $returnMessage;
        }
    }

    //新终端获取打印价格  增加了免费打印时前20页免费
    public function actionGetMoney_new() {
        //$_POST['json'] = '{"actionType": "1","singleTotal": "5","copies": "2","paperSize": "PaperA4","isColor": "0","isDuplex": "0","storeid": "","machineid": "SICNU-SZS-02"}';
//        $_POST['json'] = '{"actionType": "1","singleTotal": "2","copies": "1","paperSize": "PaperA4","isColor": "0","isDuplex": "1","storeid": "","machineid": "CQUT-PRINTD1"}';
        $active = 0;
        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            $actionType = $arrayPrintor['actionType']; //打印类型 1打印 2扫描 3复印
            $singleTotal = $arrayPrintor['singleTotal']; //单份总页数
            $copies = $arrayPrintor['copies']; //总份数
            $paperSize = $arrayPrintor['paperSize']; //打印尺寸 PaperA3：A3  PaperA4：A4
            $isColor = $arrayPrintor['isColor']; //是否彩印：0黑白；1彩色。默认0
            $isDuplex = $arrayPrintor['isDuplex']; //是否双面：0单面，1双面。默认0
            $machineid = $arrayPrintor['machineid']; //机器id，默认为空

            $price_model = price::model();
            $printor_model = printor::model();
            $printor_info = $printor_model->find("machineId = '$machineid'");
            if (count($printor_info) != 0) {
                $storeid = $printor_info->_storeId;
                $price_info = $price_model->find("storeid = $storeid and type = 2");
                if (count($price_info) != 0) {
                    $store_model = store::model();
                    $store_info = $store_model->findByPk($storeid);
                    if (count($store_info) != 0) {
                        $active = $store_info->active;
                        $activedescript = $store_info->activedescript;
                        if ($active == 0) {
                            if ($actionType == "2") {//身份证扫描打印
                                $price = $price_info->scan_price * $copies;
                            } else if ($actionType == "1" && $isDuplex == "0") {//单面打印
                                $price = $price_info->one_side_A4 * $copies * $singleTotal;
                            } else if ($actionType == "1" && $isDuplex == "1") {//双面打印
                                $price = ($price_info->two_side_A4 * floor($singleTotal / 2) + $price_info->one_side_A4 * ($singleTotal % 2)) * $copies;
                            }
                        } else {
                            if ($actionType == "2") {//身份证扫描打印
                                if ($copies <= 20) {
                                    $price = 0;
                                } else {
                                    $price = $price_info->scan_price * ($copies - 20);
                                }
                            } else if ($actionType == "1" && $isDuplex == "0") {//单面打印
                                if ($copies * $singleTotal <= 20) {
                                    $price = 0;
                                } else {
                                    $price = $price_info->one_side_A4 * ($copies * $singleTotal - 20);
                                }
                            } else if ($actionType == "1" && $isDuplex == "1") {//双面打印
                                if ($copies * $singleTotal <= 20) {
                                    $price = 0;
                                } else {
                                    $printnumber = $copies * $singleTotal - 20;
                                    $price = $price_info->two_side_A4 * floor($printnumber / 2) + $price_info->one_side_A4 * ($printnumber % 2);
                                }
                            }
                        }
                        $returnMessage = '{"code":"200","msg":"请求成功","money":"' . $price . '","active":' . $active . ',"activedescript":"' . $activedescript . '"}';
                        echo $returnMessage;
                    } else {
                        $returnMessage = '{"code":"400","msg":"没有此终端所在学校","money":"","active":' . $active . ',"activedescript":""}';
                        echo $returnMessage;
                    }
                } else {
                    $returnMessage = '{"code":"400","msg":"没有此终端的打印策略","money":"","active":' . $active . ',"activedescript":""}';
                    echo $returnMessage;
                }
            } else {
                $returnMessage = '{"code":"400","msg":"没有此终端信息","money":"","active":' . $active . ',"activedescript":""}';
                echo $returnMessage;
            }
        } else {
            $returnMessage = '{"code":"400","msg":"没有数据","money":"","active":' . $active . ',"activedescript":""}';
            echo $returnMessage;
        }
    }

    //获取运营人员的联系信息
    public function actiongetSupportInfos($machineId = null) {
        $printor_model = printor::model();
        $machineIds = addslashes($machineId);
        $printor_info = $printor_model->find("machineId = '$machineIds'");
        header("Content-type: text/html; charset=utf-8");
        if (count($printor_info) != 0) {
            $administrator_model = administrator::model();
            $administrator_info = $administrator_model->find("_storeid = $printor_info->_storeId");
            if (count($administrator_info) != 0) {
                $returnMessage = "{'resultCode':200,'description': '获取成功','address':'重庆市九龙坡区杨家坪清研理工创业谷','email':'tsd@cqporun.com','hotline':'$administrator_info->phone','complaintsline':'400-8785799','qqnumber': '2389164243'}";
                echo $returnMessage;
            } else {
                $returnMessage = '{"resultCode":400,"description":"此学校没有管理员！"}';
                echo $returnMessage;
            }
        } else {
            $returnMessage = '{"resultCode":400,"description":"此终端不存在！"}';
            echo $returnMessage;
        }
    }

//终端广告系统回传播放器状态接口
//向服务器回传当前终端播放器的状态，当前回传频率，每20秒回传一次。
    public function actionreportPlayerStatus() {
        
    }

    public function actionSearchOrder_news($verificationCode = null, $marchineid = null) {//终端拿订单
        $printor_model = printor::model();
        if ($verificationCode == "" || $verificationCode == null) {
            $returnMessage = '{"resultCode":400,"resultDescription":"验证码不能为空"}';
            echo $returnMessage;
        } else {
            $verificationCode = intval($verificationCode);
            $marchineid = addslashes($marchineid);

            $printor_info = $printor_model->find("machineId = '$marchineid'");
            if (count($printor_info) != 0) {
                $business_model = business::model();
                $business_info = $business_model->find(array("condition" => "verificationCode= $verificationCode AND isdelete = 0"));

                header("Content-type: text/html; charset=utf-8");

                if (count($business_info) != 0) {
                    if ($business_info->_storeid == $printor_info->_storeId) {
                        $businessid = $business_info->businessid;
                        $orderId = $business_info->orderId;
                        $subbusiness_model = subbusiness::model();
                        $subbusiness_info = $subbusiness_model->findAll(array("condition" => "_businessId= $businessid AND isrefund = 0")); //"_businessId = $businessid"
                        if (count($subbusiness_info) != 0) {
                            $attachment_model = attachment::model();
                            $str = '';

                            foreach ($subbusiness_info as $K => $V) {

                                $attachmentId = $V->_attachmentId;
                                $attachment_info = $attachment_model->findByPk($attachmentId);
                                if (count($attachment_info) != 0) {
                                    $filePath = $attachment_info->savepath . $attachment_info->attachmentfile . "-" . $attachment_info->attachmentname;
                                    $str .= "{'attachmentId':'$attachmentId','attachmentName':'$attachment_info->attachmentname','fileNumber':'$attachment_info->filenumber','filePath':'$filePath','isPay':'$V->isPay','payType':'$V->payType','payTime':'$V->payTime','paidMoney':'$V->paidMoney','printNumbers':'$V->printNumbers','printSet':'$V->printSet','successNumbers':'$V->successNumbers','errorPage':'$V->errorPage','printTime':'$V->printTime','trade_no':'$V->trade_no','status':'$V->status','double_sided':'$V->double_sided'},";
                                }
                            }
                            $str = substr($str, 0, -1);
                            $returnMessage = "{'resultCode':200,'resultDescription': '获取订单成功(包含所有文件)','orderId':'$orderId','file': ["
                                    . $str
                                    . "]}";
                            $returnMessage = str_replace("'", '"', $returnMessage);
                            echo $returnMessage;
                        } else {
                            $returnMessage = '{"resultCode":400,"resultDescription":"子订单不存在或已失效！"}';
                            echo $returnMessage;
                        }
                    } else {
                        $store_model = store::model();
                        $store_info = $store_model->findByPk($business_info->_storeid);
                        if (count($store_info) != 0) {
                            $returnMessage = '{"resultCode":400,"resultDescription":"此订单应在' . $store_info->storename . '打印！"}';
                            echo $returnMessage;
                        } else {
                            $returnMessage = '{"resultCode":400,"resultDescription":"此订单不应在此打印！"}';
                            echo $returnMessage;
                        }
                    }
                } else {
                    $returnMessage = '{"resultCode":400,"resultDescription":"订单不存在或已失效！"}';
                    echo $returnMessage;
                }
            } else {
                $returnMessage = '{"resultCode":400,"resultDescription":"此终端不存在！"}';
                echo $returnMessage;
            }
        }
    }

    //错误信息回传后台服务器接口
    public function actionPostErrorInfo() {
//        $_POST['json'] = '{"machineid":"CQUT-PRINTD1", "errortype": "打印中错误","errorinformaition": "打印机连接失败","occurtime": "2016-4-25 12:32:41"}';
        if (!empty($_POST)) {
            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
            if ($encode != 'UTF-8') {
                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
            }
            if (is_array($_POST['json'])) {//是数组
                $arrayPrintor = $_POST['json'];
            } else {
                $arrayPrintor = CJSON::decode($_POST['json']);
            }
            if (isset($arrayPrintor['machineid'])) {
                $machineid = $arrayPrintor['machineid']; //终端的唯一标识符
            } else {
                $machineid = "";
            }
            if (isset($arrayPrintor['errortype'])) {
                $error_type = $arrayPrintor['errortype']; //错误类型
            } else {
                $error_type = "";
            }
            if (isset($arrayPrintor['errorinformaition'])) {
                $error_informaition = $arrayPrintor['errorinformaition']; //具体的错误信息，错误信息内容可能会比较多，估计要多开辟些内存
            } else {
                $error_informaition = "";
            }
            if (isset($arrayPrintor['occurtime'])) {
                $occur_time = $arrayPrintor['occurtime']; //错误发生时间，以终端时间为准
            } else {
                $occur_time = "";
            }

            $printor_model = printor::model();
            $printor_info = $printor_model->find("machineId = '$machineid'");
            if (count($printor_info) != 0) {
                $printorerror_model = new printorerror();
                $printorerror_model->machineid = $machineid;
                $printorerror_model->_storeid = $printor_info->_storeId;
                $printorerror_model->error_type = $error_type;
                $printorerror_model->error_informaition = $error_informaition;
                $printorerror_model->occur_time = $occur_time;
                $printorerror_model->add_time = date('Y-m-d H:i:s');

                if ($printorerror_model->save()) {
                    $returnMessage = '{"resultCode":200,"resultDescription":"保存成功"}';
                    echo $returnMessage;
                } else {
                    $returnMessage = '{"resultCode":400,"resultDescription":"保存失败"}';
                    echo $returnMessage;
                }
            } else {
                $returnMessage = '{"resultCode":400,"resultDescription":"没有此终端"}';
                echo $returnMessage;
            }
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $returnMessage;
        }
    }

    //错误信息回传后台服务器接口
    public function actionPostErrorInfos() {
//        $_POST['json'] = '{"machineid":"CQUT-PRINTD1", "errortype": "打印中错误","errorinformaition": "打印机连接失败","occurtime": "2016-4-25 12:32:41"}';
        $data = file_get_contents("php://input");
//        $data = '{"machineid": "CQUT-PRINT09","errortype": "打印中错误","errorinformaition": "打印机连接失败","occurtime": "2016-6-25 12:32:41"}';
        if (!empty($data)) {
//            $encode = mb_detect_encoding($_POST['json'], array('ASCII', 'UTF-8', 'GB2312', 'GBK', 'BIG5'));
//            if ($encode != 'UTF-8') {
//                $_POST['json'] = iconv($encode, "UTF-8//IGNORE", $_POST['json']);
//            }
            if (is_array($data)) {//是数组
                $arrayPrintor = $data;
            } else {
                $arrayPrintor = CJSON::decode($data);
            }
            if (isset($arrayPrintor['machineid'])) {
                $machineid = $arrayPrintor['machineid']; //终端的唯一标识符
            } else {
                $machineid = "";
            }
            if (isset($arrayPrintor['errortype'])) {
                $error_type = $arrayPrintor['errortype']; //错误类型
            } else {
                $error_type = "";
            }
            if (isset($arrayPrintor['errorinformaition'])) {
                $error_informaition = $arrayPrintor['errorinformaition']; //具体的错误信息，错误信息内容可能会比较多，估计要多开辟些内存
            } else {
                $error_informaition = "";
            }
            if (isset($arrayPrintor['occurtime'])) {
                $occur_time = $arrayPrintor['occurtime']; //错误发生时间，以终端时间为准
            } else {
                $occur_time = "";
            }

            $printor_model = printor::model();
            $printor_info = $printor_model->find("machineId = '$machineid'");
            if (count($printor_info) != 0) {
                $printorerror_model = new printorerror();
                $printorerror_model->machineid = $machineid;
                $printorerror_model->_storeid = $printor_info->_storeId;
                $printorerror_model->error_type = $error_type;
                $printorerror_model->error_informaition = $error_informaition;
                $printorerror_model->occur_time = $occur_time;
                $printorerror_model->add_time = date('Y-m-d H:i:s');

                if ($printorerror_model->save()) {
                    $returnMessage = '{"resultCode":200,"resultDescription":"保存成功"}';
                    echo $returnMessage;
                } else {
                    $returnMessage = '{"resultCode":400,"resultDescription":"保存失败"}';
                    echo $returnMessage;
                }
            } else {
                $returnMessage = '{"resultCode":400,"resultDescription":"没有此终端"}';
                echo $returnMessage;
            }
        } else {
            $returnMessage = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $returnMessage;
        }
    }

}
