<?php

/*
 * 抓取终端信息 接口
 */

class getPrintorController extends Controller
{
    public function actionPostPrintorInfo()//拿终端状态数据
    {
        if (!empty($_POST))
        {
            if (is_array($_POST))//是数组
            {
                $arrayPrintor = $_POST;
            }
            else
            {
                $arrayPrintor = CJSON::decode($_POST);
            }
            try
            {
                $marchineid = $arrayPrintor['marchineid']; //终端标识
                $printorstate = $arrayPrintor['printorstate']; //终端状态
                $currentpaper = $arrayPrintor['currentpaper']; //剩余纸张
                $leftjiaoCoin = $arrayPrintor['leftjiaoCoin']; //找零器剩余多少一角的纸币
                $leftyuanCoin = $arrayPrintor['leftyuanCoin']; //找零器剩余一元硬币多少
                $coinMarchineStatus = $arrayPrintor['coinMarchineStatus']; //投币器状态
                $moneyMarhcineStatus = $arrayPrintor['moneyMarhcineStatus']; //纸币器目前的工作状态
                $soundWavePayStatus = $arrayPrintor['soundWavePayStatus']; //声波支付目前的工作状态
                $changeMarchineStatus = $arrayPrintor['changeMarchineStatus']; //找零器工作状态
            }
            catch (Exception $e)
            {
                $return = '{"resultCode":300,"resultDescription":"未知错误"}';
                echo $return;
            }
            $printor_model = printor::model();
            $printor_info = $printor_model->find(array('condition' => "marchineid='$marchineid'"));
            $printor_info->printorstate = $printorstate;
            $printor_info->currentpaper = $currentpaper;
            $printor_info->leftjiaoCoin = $leftjiaoCoin;
            $printor_info->leftyuanCoin = $leftyuanCoin;
            $printor_info->coinMarchineStatus = $coinMarchineStatus;
            $printor_info->moneyMarhcineStatus = $moneyMarhcineStatus;
            $printor_info->soundWavePayStatus = $soundWavePayStatus;
            if ($printor_info->save())
            {
                $return = '{"resultCode":200,"resultDescription":"传输成功"}';
                echo $return;
            }
            else
            {
                $return = '{"resultCode":400,"resultDescription":"状态保存失败"}';
                echo $return;
            }
        }
        else
        {
            $return = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $return;
        }
    }

    public function actionPostWebBusinessInfo()//更新通过网络平台打印文件信息
    {
        // $_POST =  $_POST = '{"businessid":1,"isComplete":1,"isPay":1,"attachmentid":5,"printtime":20151126123030}; 已经通过平台支付宝 支付的文件打印
        // $_POST = '{"isComplete":1,"isPay":0,"attachmentid":6,"userid":16,"paidMoney":1,"payTime":20151126123030}';//平台上传通过终端支付
        if (!empty($_POST))
        {
            if (is_array($_POST))//是数组
            {
                $arrayBusiness = $_POST;
            }
            else
            {
                $arrayBusiness = CJSON::decode($_POST);
            }
            $business_model = new business();
            $isPay = $arrayBusiness['isPay']; //1已经支付过  0 通过终端支付
            $isComplete = $arrayBusiness['isComplete'];
            if ($isComplete == 1) //打印成功
            {
                if ($isPay == 1)
                {
                    $businessid = $arrayBusiness['businessid'];//业务id
                    $attachmentid = $arrayBusiness['attachmentid'];
                    $business_info = $business_model->find(array('condition' => "businessid=$businessid"));
                    $business_info->isComplete = 1;
                    $business_info->printTime = $arrayBusiness['printtime'];//打印时间
                 //   $attachment_model = attachment::model();
                   // $attachment_info = $attachment_model->findByPk($attachmentid);
                  //  $attachment_info->state = 1;
                    if ($business_info->save())
                    {
                        $return = '{"resultCode":200,"resultDescription":"更新交易数据成功"}';
                        echo $return;
                    }
                    else
                    {
                        $return = '{"resultCode":400,"resultDescription":"更新交易数据失败"}';
                        echo $return;
                    }
                }
                else
                {
                    $userid = $arrayBusiness['userid'];
                    $attachmentid = $arrayBusiness['attachmentid'];
                    $paidMoney = $arrayBusiness['paidMoney'];
                    $payTime = $arrayBusiness['payTime'];

                    $business_model->_userid = $userid;
                    $business_model->_attachmentid = $attachmentid;
                    $business_model->payType = 0;
                    $business_model->paidMoney = $paidMoney;
                    $business_model->payTime = $payTime;
                    $business_model->isComplete = 1;

                    $attachment_model = attachment::model();
                    $attachment_info = $attachment_model->findByPk($attachmentid);
                    $attachment_info->state = 1;
                    if ($business_model->save() && $attachment_info->save())
                    {
                        $return = '{"resultCode":200,"resultDescription":"终端交易信息保存成功"}';
                        echo $return;
                    }
                    else
                    {
                        $return = '{"resultCode":400,"resultDescription":"终端交易信息保存失败"}';
                        echo $return;
                    }
                }
            }
            else
            {
                $return = '{"resultCode":400,"resultDescription":"打印失败，无法保存交易记录"}';
                echo $return;
            }
        }
        else
        {
            $return = '{"resultCode":400,"resultDescription":"没有数据"}';
            echo $return;
        }
    }

    public function actionPostBusinessInfo()//拿终端交易信息
    {
//        $_POST = '{"marchineid":"ddd","udiskId":"abd","payType":2,"printType":1,"printName":"文件1","printNumbers":1,"paidMoney":1.2,"changeMoney":1,"needPayMoney":1,"transferTime":20140205151320,"transferFinishedTime":20140206051320,"transferStatus":1,"transferPhrase":1,"paytime":20140206051320,"printtime":20140206051320,"iscomplete":1}';
//        $_POST = '{"printType":2,"ispay":1,"marchineid":"abd","attachmentid":123,"iscomplete":1}';
//        $_POST = '{"ispay":0,"marchineid":"ddd","attachmentid":123,"payType":2,"printType":2,"printNumbers":1,"paidMoney":1.2,"changeMoney":1,"needPayMoney":1,"transferTime":20140205151320,"transferFinishedTime":20140206051320,"transferStatus":1,"transferPhrase":1,"paytime":20140206051320,"iscomplete":1}';//未支付 只上传

        if (!empty($_POST))
        {
            if (is_array($_POST))//是数组
            {
                $arrayBusiness = $_POST;
            }
            else
            {
                $arrayBusiness = CJSON::decode($_POST);
            }
            $printType = $arrayBusiness['printType'];//打印方式
            $busines_model = new business();
            if ($printType==1)//终端新增订单
            {
                $busines_model = new business();
                $busines_model->_marchineid = $arrayBusiness['marchineid']; //交易终端唯一标识
                $busines_model->udiskId = $arrayBusiness['udiskId']; //U盘id
                $busines_model->payType = $arrayBusiness['payType']; //交易支付方式。（支付宝1 终端一卡通支付2 终端投币3）
                $busines_model->printType = $printType; //打印方式，1终端直接打印 2通过平台先上传
                $busines_model->printName = $arrayBusiness['printName']; //打印文件名称
                $busines_model->printNumbers = $arrayBusiness['printNumbers']; //打印份数
                $busines_model->paidMoney = $arrayBusiness['paidMoney']; //用户支付金额
                $busines_model->changeMoney = $arrayBusiness['changeMoney']; //找零金额
                $busines_model->needPayMoney = $arrayBusiness['needPayMoney']; //用户应付金额
                $busines_model->transferTime = $arrayBusiness['transferTime']; //交易开始的时间
                $busines_model->transferFinishedTime = $arrayBusiness['transferFinishedTime']; //交易完成的时间
                $busines_model->transferStatus = $arrayBusiness['transferStatus']; //交易的状态。（交易最终的状态：成功1，失败0，继续交易2，等）
                $busines_model->transferPhrase = $arrayBusiness['transferPhrase']; //本次交易所处于的阶段。（本次交易正处于的阶段：如登录1，打印2，选文件3，方便错误处理）。
                $busines_model->paytime = $arrayBusiness['paytime']; //支付时间 
                $busines_model->printtime = $arrayBusiness['printtime'];//打印时间 
                if ($arrayBusiness['iscomplete'] == 1)
                {
                    $busines_model->iscomplete = 1; //打印成功
                    if ($busines_model->save())
                    {
                        $return = '{"resultCode":200,"resultDescription":"交易信息保存成功"}';
                        echo $return;
                    }
                    else
                    {
                        $return = '{"resultCode":400,"resultDescription":"交易信息保存失败"}';
                        echo $return;
                    }
                }
                else
                {
                    $busines_model->iscomplete = 0; //打印失败
                }
               
            }
            else if ($printType == 2)//网络平台订单处理
            {
                $ispay = $arrayBusiness['ispay'];
                if($ispay == 1)//已经在平台支付过
                {
                    $attachmentid = $arrayBusiness['attachmentid'];
                    $busines_info = $busines_model->find(array('condition' => "_attachmentid = $attachmentid"));
                    //$busines_info->printType = 2; //打印方式，1终端直接打印 2通过平台先上传
                    $busines_info->_marchineid = $arrayBusiness['marchineid']; //终端唯一标识
                    if ($arrayBusiness['iscomplete'] == 1)
                    {
                        $attachment_model = attachment::model();
                        $attachment_info =$attachment_model->findByPk($attachmentid);
                        $attachment_info->state = 1;
                        $busines_info->iscomplete = 1; //打印成功
                    }
                    else
                    {
                        $busines_info->iscomplete = 0; //打印失败
                    }
                    if ($busines_info->save()&&$attachment_info->save())
                    {
                        $return = '{"resultCode":200,"resultDescription":"订单交易成功"}';
                        echo $return;
                    }
                    else
                    {
                        $return = '{"resultCode":400,"resultDescription":"交易信息保存失败"}';
                        echo $return;
                    }
                }
                else //通过终端支付
                {
                    $attachmentid = $arrayBusiness['attachmentid'];
                    $busines_model->_attachmentid = $attachmentid;//文件id
                    $busines_model->_marchineid = $arrayBusiness['marchineid']; //终端唯一标识
                    $busines_model->payType = $arrayBusiness['payType']; //交易支付方式。（支付宝1 终端一卡通支付2 终端投币3）
                    $busines_model->printNumbers = $arrayBusiness['printNumbers']; //打印份数
                    $busines_model->paidMoney = $arrayBusiness['paidMoney']; //用户支付金额
                    $busines_model->changeMoney = $arrayBusiness['changeMoney']; //找零金额
                    $busines_model->needPayMoney = $arrayBusiness['needPayMoney']; //用户应付金额
                    $busines_model->transferTime = $arrayBusiness['transferTime']; //交易开始的时间
                    $busines_model->transferFinishedTime = $arrayBusiness['transferFinishedTime']; //交易完成的时间
                    $busines_model->transferStatus = $arrayBusiness['transferStatus']; //交易的状态。（交易最终的状态：成功1，失败0，继续交易2，等）
                    $busines_model->transferPhrase = $arrayBusiness['transferPhrase']; //本次交易所处于的阶段。（本次交易正处于的阶段：如登录1，打印2，选文件3，方便错误处理）。
                    $busines_model->paytime = $arrayBusiness['paytime']; //支付时间 
                    if ($arrayBusiness['iscomplete'] == 1)
                    {
                        $attachment_model = attachment::model();
                        $attachment_info =$attachment_model->findByPk($attachmentid);
                        $attachment_info->state = 1;
                        $attachment_info->ispay = 1;
                        $busines_model->iscomplete = 1; //打印成功
                    }
                    else
                    {
                        $busines_model->iscomplete = 0; //打印失败
                    }
                    if ($busines_model->save()&&$attachment_info->save())
                    {
                        $return = '{"resultCode":200,"resultDescription":"订单交易成功"}';
                        echo $return;
                    }
                    else
                    {
                        $return = '{"resultCode":400,"resultDescription":"交易信息保存失败"}';
                        echo $return;
                    }
                }
            }
        }
        else
        {
            $return = '{"resultCode":400,"resultDescription":"无法抓取数据"}';
            echo $return;
        }
    }
}
