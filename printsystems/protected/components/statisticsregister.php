<?php

class statisticsregister {

    public function statistics_register($storeid = 0, $starttime, $endtime, $types) {
        $user_model = user::model();
        $store_model = store::model();
        if ($storeid != 0) {
            $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }
        $registerarr = array(); //返回的数组

        $dayArray = array(); //每日注册人数

        $register_total = 0; //总注册人数

        if ($types == "day") {
            $year = date("Y", strtotime($starttime));
            while (strtotime($starttime) <= strtotime($endtime)) {
                $stime = $starttime . " 00:00:00";
                $etime = $starttime . " 23:59:59";
                $i = date("d", strtotime($stime));
                $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
                if ($storeid != 0) {
                    $user_info = $user_model->findAllBySql("select * from tbl_user where registertime between :stime and :etime and _storeid in ($storeid)", array(':stime' => $stime, ':etime' => $etime));
                } else {
                    $user_info = $user_model->findAllBySql("select * from tbl_user where registertime between :stime and :etime", array(':stime' => $stime, ':etime' => $etime));
                }
                $register_total += count($user_info);
                array_push($dayArray, array('register_day' => count($user_info), 'day' => $i));
            }
        }
        if ($types == "month") {
            $year = date("Y", strtotime($starttime));
            for ($i = 1; $i <= 12; $i++) {
                if ($i != 12) {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                } else {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . $i . "-31 23:59:59";
                }
                if ($storeid != 0) {
                    $user_info = $user_model->findAllBySql("select * from tbl_user where registertime between :stime and :etime and _storeid in ($storeid)", array(':stime' => $stime, ':etime' => $etime));
                } else {
                    $user_info = $user_model->findAllBySql("select * from tbl_user where registertime between :stime and :etime", array(':stime' => $stime, ':etime' => $etime));
                }
                $register_total += count($user_info);
                array_push($dayArray, array('register_day' => count($user_info), 'day' => $i));
            }
        }
        if (isset($store_info)) {
            if (count($store_info) == 1) {
                foreach ($store_info as $k => $l) {
                    $registerarr["schoolname"] = $l->storename; //学校名称
                }
            } else {
                $registerarr["schoolname"] = "";
            }
        } else {
            $registerarr["schoolname"] = "";
        }
        $registerarr["storeid"] = $storeid; //storeid
        $registerarr["year"] = $year; //年份
        $registerarr["dayArray"] = $dayArray; //每日注册人数
        $registerarr["register_total"] = $register_total; //总注册人数
        return $registerarr;
    }

}
