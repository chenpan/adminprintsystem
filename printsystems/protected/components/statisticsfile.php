<?php

class statisticsfile {

    //本月文件上传量的各种统计 按每天的方式统计
    public function statisticsfile_day($storeid = 0, $starttime, $endtime, $types) {

        if ($storeid != 0) {
            $store_model = store::model();
            $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }

        $filearr = array();
        $dayArray_PC = array(); //webPC上传量
        $dayArray_APP = array(); //webAPP上传量
        $dayArray_WX = array(); //webWX上传量
        $dayArray_PR = array(); //终端上传量
        $dayArray_total = array(); //总上传量
        $dayArray_Web = array();
//        $totalCount_day_All = array();//总上传量

        $totalCount_dayss = 0; //web上传量共有多少
        $totalCount_dayss_PC = 0; //webPC上传量共有多少
        $totalCount_dayss_APP = 0; //webAPP上传量共有多少
        $totalCount_dayss_WX = 0; //webWX上传量共有多少
        $totalCount_dayss_PR = 0; //终端上传量共有多少
        if ($types == "day") {
            $year = date("Y", strtotime($starttime));
            while (strtotime($starttime) <= strtotime($endtime)) {

                $stime = $starttime . " 00:00:00";
                $etime = $starttime . " 23:59:59";
                $i = date("d", strtotime($stime));
                $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
                //终端每日文件上传量
//                $connection = Yii::app()->db;
                $connection2 = Yii::app()->db2;
                if ($storeid != 0) {
                    $sqls = "select attachmentid from tbl_attachment where storeid in ($storeid) and medium = 4 and uploadtime between '$stime' and '$etime'";
                } else {
                    $sqls = "select attachmentid from tbl_attachment where medium = 4 and uploadtime between '$stime' and '$etime'";
                }
                $totalCount_days_PR = count($connection2->createCommand($sqls)->query());

                $totalCount_dayss_PR += $totalCount_days_PR;
                array_push($dayArray_PR, array('totalCount_days_PR' => $totalCount_days_PR, 'day' => $i));
                //终端每日webPC上传量
                if ($storeid != 0) {
                    $sqlspc = "select attachmentid from tbl_attachment where storeid in ($storeid) and medium = 1 and uploadtime between '$stime' and '$etime'";
                } else {
                    $sqlspc = "select attachmentid from tbl_attachment where medium = 1 and uploadtime between '$stime' and '$etime'";
                }
                $totalCount_days_PC = count($connection2->createCommand($sqlspc)->query());

                $totalCount_dayss_PC += $totalCount_days_PC;
                array_push($dayArray_PC, array('totalCount_days_PC' => $totalCount_days_PC, 'day' => $i));
                //终端每日webAPP上传量
                if ($storeid != 0) {
                    $sqlsapp = "select attachmentid from tbl_attachment where storeid in ($storeid) and medium = 2 and uploadtime between '$stime' and '$etime'";
                } else {
                    $sqlsapp = "select attachmentid from tbl_attachment where medium = 2 and uploadtime between '$stime' and '$etime'";
                }
                $totalCount_days_APP = count($connection2->createCommand($sqlsapp)->query());

                $totalCount_dayss_APP += $totalCount_days_APP;
                array_push($dayArray_APP, array('totalCount_days_APP' => $totalCount_days_APP, 'day' => $i));
                //终端每日webWX上传量
                if ($storeid != 0) {
                    $sqlswx = "select attachmentid from tbl_attachment where storeid in ($storeid) and medium = 3 and uploadtime between '$stime' and '$etime'";
                } else {
                    $sqlswx = "select attachmentid from tbl_attachment where medium = 3 and uploadtime between '$stime' and '$etime'";
                }
                $totalCount_days_WX = count($connection2->createCommand($sqlswx)->query());

                $totalCount_dayss_WX += $totalCount_days_WX;
                array_push($dayArray_WX, array('totalCount_days_WX' => $totalCount_days_WX, 'day' => $i));
                $totalCount_day_All = $totalCount_days_PR + $totalCount_days_PC + $totalCount_days_APP + $totalCount_days_WX;
                $totalCount_day_Web = $totalCount_days_PC + $totalCount_days_APP + $totalCount_days_WX;
                array_push($dayArray_Web, array('totalCount_day_Web' => $totalCount_day_Web, 'day' => $i));
                array_push($dayArray_total, array('totalCount_day_All' => $totalCount_day_All, 'day' => $i));
            }
        }
        if ($types == "month") {
            $year = date("Y", strtotime($starttime));
            for ($i = 1; $i <= 12; $i++) {
                if ($i != 12) {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                } else {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . $i . "-31 23:59:59";
                }
                $connection2 = Yii::app()->db2;
                if ($storeid != 0) {
                    $sqls = "select attachmentid from tbl_attachment where storeid in ($storeid) and medium = 4 and uploadtime between '$stime' and '$etime'";
                } else {
                    $sqls = "select attachmentid from tbl_attachment where medium = 4 and uploadtime between '$stime' and '$etime'";
                }
                $totalCount_days_PR = count($connection2->createCommand($sqls)->query());

                $totalCount_dayss_PR += $totalCount_days_PR;
                array_push($dayArray_PR, array('totalCount_days_PR' => $totalCount_days_PR, 'day' => $i));
                //终端每月webPC上传量
                if ($storeid != 0) {
                    $sqlspc = "select attachmentid from tbl_attachment where storeid in ($storeid) and medium = 1 and uploadtime between '$stime' and '$etime'";
                } else {
                    $sqlspc = "select attachmentid from tbl_attachment where medium = 1 and uploadtime between '$stime' and '$etime'";
                }
                $totalCount_days_PC = count($connection2->createCommand($sqlspc)->query());

                $totalCount_dayss_PC += $totalCount_days_PC;
                array_push($dayArray_PC, array('totalCount_days_PC' => $totalCount_days_PC, 'day' => $i));
                //终端每月webAPP上传量
                if ($storeid != 0) {
                    $sqlsapp = "select attachmentid from tbl_attachment where storeid in ($storeid) and medium = 2 and uploadtime between '$stime' and '$etime'";
                } else {
                    $sqlsapp = "select attachmentid from tbl_attachment where medium = 2 and uploadtime between '$stime' and '$etime'";
                }
                $totalCount_days_APP = count($connection2->createCommand($sqlsapp)->query());

                $totalCount_dayss_APP += $totalCount_days_APP;
                array_push($dayArray_APP, array('totalCount_days_APP' => $totalCount_days_APP, 'day' => $i));
                //终端每月webWX上传量
                if ($storeid != 0) {
                    $sqlswx = "select attachmentid from tbl_attachment where storeid in ($storeid) and medium = 3 and uploadtime between '$stime' and '$etime'";
                } else {
                    $sqlswx = "select attachmentid from tbl_attachment where medium = 3 and uploadtime between '$stime' and '$etime'";
                }
                $totalCount_days_WX = count($connection2->createCommand($sqlswx)->query());

                $totalCount_dayss_WX += $totalCount_days_WX;
                array_push($dayArray_WX, array('totalCount_days_WX' => $totalCount_days_WX, 'day' => $i));
                $totalCount_day_All = $totalCount_days_PR + $totalCount_days_PC + $totalCount_days_APP + $totalCount_days_WX;
                $totalCount_day_Web = $totalCount_days_PC + $totalCount_days_APP + $totalCount_days_WX;
                array_push($dayArray_Web, array('totalCount_day_Web' => $totalCount_day_Web, 'day' => $i));
                array_push($dayArray_total, array('totalCount_day_All' => $totalCount_day_All, 'day' => $i));
            }
        }
        if (isset($store_info)) {
            if (count($store_info) == 1) {
                foreach ($store_info as $k => $l) {
                    $filearr["schoolname"] = $l->storename; //学校名称
                }
            } else {
                $filearr["schoolname"] = "";
            }
        } else {
            $filearr["schoolname"] = "";
        }
        $filearr["storeid"] = $storeid; //storeid
        $filearr["year"] = $year; //年份
        $filearr["dayArray_PR"] = $dayArray_PR;
        $filearr["dayArray_PC"] = $dayArray_PC;
        $filearr["dayArray_APP"] = $dayArray_APP;
        $filearr["dayArray_WX"] = $dayArray_WX;
        $filearr["dayArray_Web"] = $dayArray_Web;
        $filearr["dayArray_total"] = $dayArray_total;
        $filearr["totalCount_days_PR"] = $totalCount_days_PR; //每日终端文件上传量
        $filearr["totalCount_days_PC"] = $totalCount_days_PC; //每日PC文件上传量
        $filearr["totalCount_days_APP"] = $totalCount_days_APP; //每日APP文件上传量
        $filearr["totalCount_days_WX"] = $totalCount_days_WX; //每日WX文件上传量
        $filearr["totalCount_days_total"] = $totalCount_day_All; //每日总文件上传量


        $filearr["totalCount_dayss_PR"] = $totalCount_dayss_PR; //时间段内终端文件上传量
        $filearr["totalCount_dayss_PC"] = $totalCount_dayss_PC; //时间段内PC文件上传量
        $filearr["totalCount_dayss_APP"] = $totalCount_dayss_APP; //时间段内APP文件上传量
        $filearr["totalCount_dayss_WX"] = $totalCount_dayss_WX; //时间段内WX文件上传量
        $filearr["totalCount_dayssAll"] = $totalCount_dayss_PR + $totalCount_dayss_PC + $totalCount_dayss_APP + $totalCount_dayss_WX;
        $filearr["totalCount_dayssWeb"] = $totalCount_dayss_PC + $totalCount_dayss_APP + $totalCount_dayss_WX;
        return $filearr;
    }

}
