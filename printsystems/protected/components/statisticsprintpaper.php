<?php

class statisticsprintpaper {

    public function statistics_printpaper($storeid = 0, $starttime, $endtime) {
        $store_model = store::model();
        $printor_model = printor::model();
        if ($storeid != 0) {
            $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $printor_info = $printor_model->findAllBySql("select * from tbl_printor where _storeId in (" . $storeid . ")");
            } else {
                $printor_info = $printor_model->findAllBySql("select * from tbl_printor where _storeId in ($storeid) and printorId in (" . Yii::app()->session['printor_id'] . ")");
            }
        } else {
            $store_info = $store_model->findAllBySql("select * from tbl_store", array('order' => "storeid DESC"));

            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $printor_info = $printor_model->findAllBySql("select * from tbl_printor");
            } else {
                $printor_info = $printor_model->findAllBySql("select * from tbl_printor where printorId in (" . Yii::app()->session['printor_id'] . ")");
            }
        }
        $connection = Yii::app()->db;
        $connection2 = Yii::app()->db2;
        if ($storeid == 0) {
            $stt = "select businessid from tbl_business";
        } else {
            $stt = "select businessid from tbl_business where _storeid in ($storeid)";
        }
        $sttx = $connection2->createCommand($stt)->query();
        $strt = "";
        foreach ($sttx as $k => $v) {
            $strt .= $v['businessid'] . ",";
        }
        $strt = substr($strt, 0, -1);
        if (strlen($strt) == 0) {
            $strt = 0;
        }
        $printpaperarr = array(); //返回的数组

        $totalPages = 0; //此学校所有终端的打印纸张

        $dayArray_pages = array(); //学校打印纸张总量
        $stime = date('Y-m-d', strtotime($starttime)) . " 00:00:00";
        $etime = date('Y-m-d', strtotime($endtime)) . " 23:59:59";
        $year = date("Y", strtotime($starttime));
        if (count($printor_info) != 0) {
            foreach ($printor_info as $l => $y) {
                $totalPage = 0;
                $machineId = $y->machineId;
                if ($storeid != 0) {
                    $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness where _businessId in(" . $strt . ") and status != 0 and marchineId = '$machineId' and printTime between '$stime' and '$etime'";
                    $sqlp = "select * from tbl_prbusiness where _storeid in ($storeid) and exit_phrase >= 5 and machineId = '$machineId' and printset is not null and printset != '' and starttime between '$stime' and '$etime'";
                } else {
                    $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness where status != 0 and marchineId = '$machineId' and printTime between '$stime' and '$etime'";
                    $sqlp = "select * from tbl_prbusiness where exit_phrase >= 5 and machineId = '$machineId' and printset is not null and printset != '' and starttime between '$stime' and '$etime'";
                }
                foreach ($connection2->createCommand($sqls)->query() as $q => $w) {
                    if ($w["printSet"] != "") {
                        $filePage = explode(',', $w["printSet"]);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $totalPagex = 0;
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPagex +=(int) $filePa * $w["successNumbers"] + $w["errorPage"];
                                if ($w["double_sided"] == 0) {//单面
                                    $totalPage += $totalPagex;
                                } else {
                                    $totalPage += ceil($totalPagex / 2);
                                }
                            }
                        }
                    }
                }

                foreach ($connection->createCommand($sqlp)->query() as $q => $w) {
                    if ($w["exit_phrase"] == 6) {
                        if ($w["printset"] != "") {
                            $filePage = explode(',', $w["printset"]);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $totalPagex = 0;
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPagex +=(int) $filePa * $w["copies"];
                                    if ($w["is_duplex_page"] == 1) {//单面
                                        $totalPage += $totalPagex;
                                    } else {
                                        $totalPage += ceil($totalPagex / 2);
                                    }
                                }
                            }
                        }
                    } else {
                        if ($w["is_duplex_page"] == 1) {//单面
                            if ($w["printedpages"] != "") {
                                $totalPage +=$w["printedpages"];
                            } else {
                                $totalPagex = 0;
                                if ($w["printset"] != "") {
                                    $filePage = explode(',', $w["printset"]);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w["copies"];
                                            $totalPage += $totalPagex;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($w["printedpages"] != "") {
                                $totalPage +=ceil($w["printedpages"] / 2);
                            } else {
                                $totalPagex = 0;
                                if ($w["printset"] != "") {
                                    $filePage = explode(',', $w["printset"]);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w["copies"];
                                            $totalPage += ceil($totalPagex / 2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $totalPages +=$totalPage;
                array_push($dayArray_pages, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalPage' => $totalPage));
            }

            if (isset($store_info)) {
                if (count($store_info) == 1) {
                    foreach ($store_info as $k => $l) {
                        $printpaperarr["schoolname"] = $l->storename; //学校名称
                    }
                } else {
                    $printpaperarr["schoolname"] = "";
                }
            } else {
                $printpaperarr["schoolname"] = "";
            }
            $printpaperarr["storeid"] = $storeid; //storeid
            $printpaperarr["year"] = $year; //年份
            $printpaperarr["totalPages"] = $totalPages; //总打印纸张数
            $printpaperarr["dayArray_pages"] = $dayArray_pages; //总打印纸张数
            return $printpaperarr;
        } else {
            if (isset($store_info)) {
                if (count($store_info) == 1) {
                    foreach ($store_info as $k => $l) {
                        $printpaperarr["schoolname"] = $l->storename; //学校名称
                    }
                } else {
                    $printpaperarr["schoolname"] = "";
                }
            } else {
                $printpaperarr["schoolname"] = "";
            }
            array_push($dayArray_pages, array('machineId' => "", 'printorName' => " ", 'address' => " ", 'totalPage' => ""));
            $printpaperarr["storeid"] = $storeid; //storeid
            $printpaperarr["year"] = $year; //年份
            $printpaperarr["totalPages"] = ""; //总打印纸张数
            $printpaperarr["dayArray_pages"] = $dayArray_pages; //总打印纸张数
            return $printpaperarr;
        }
    }

    //选择学校显示
    public function statistics_printpaper_total($storeid = 0, $starttime, $endtime, $types) {
        $store_model = store::model();
        if ($storeid != 0) {
            $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        } else {
            $store_info = $store_model->findAllBySql("select * from tbl_store", array('order' => "storeid DESC"));
        }
        $connection = Yii::app()->db;
        $connection2 = Yii::app()->db2;
        if ($storeid == 0) {
            $stt = "select businessid from tbl_business";
        } else {
            $stt = "select businessid from tbl_business where _storeid in ($storeid)";
        }
        $strt = "";
        $sttx = $connection2->createCommand($stt)->query();
        foreach ($sttx as $k => $v) {
            $strt .= $v['businessid'] . ",";
        }
        $strt = substr($strt, 0, -1);
        if (strlen($strt) == 0) {
            $strt = 0;
        }
        $printpaperarr = array(); //返回的数组

        $totalPages = 0; //此学校所有终端的打印纸张
        $dayArray = array(); //打印的总纸张数

        if ($types == "day") {
            $year = date("Y", strtotime($starttime));
            while (strtotime($starttime) <= strtotime($endtime)) {
                $stime = $starttime . " 00:00:00";
                $etime = $starttime . " 23:59:59";
                $i = date("d", strtotime($stime));
                $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
                $totalPage = 0;
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness"
                                . " where _businessId in(" . $strt . ") and status != 0 and"
                                . " marchineId in (select machineId from db_home.tbl_printor where _storeId in ($storeid)) and printTime between '$stime' and '$etime'";
                        $sqlp = "select * from tbl_prbusiness where _storeid in ($storeid) and exit_phrase >= 5 and machineId in (select machineId from db_home.tbl_printor where _storeId in ($storeid)) and printset is not null and printset != '' and starttime between '$stime' and '$etime'";
                    } else {
                        $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness where _businessId in(" . $strt . ") and status != 0 and marchineId in (select machineId from db_home.tbl_printor where _storeId in ($storeid) and printorId in (" . Yii::app()->session['printor_id'] . ")) and printTime between '$stime' and '$etime'";
                        $sqlp = "select * from tbl_prbusiness where _storeid in ($storeid) and exit_phrase >= 5 and machineId in (select machineId from db_home.tbl_printor where _storeId in ($storeid) and printorId in (" . Yii::app()->session['printor_id'] . ")) and printset is not null and printset != '' and starttime between '$stime' and '$etime'";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness where _businessId in(" . $strt . ") and status != 0 and marchineId in (select machineId from db_home.tbl_printor where printTime between '$stime' and '$etime')"; // and marchineId in (select machineId from tbl_printor) 
                        $sqlp = "select * from tbl_prbusiness where exit_phrase >= 5  and printset is not null and printset != '' and machineId in (select machineId from db_home.tbl_printor)  and starttime between '$stime' and '$etime'"; //and machineId in (select machineId from db_home.tbl_printor) 
                    } else {
                        $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness where _businessId in(" . $strt . ") and status != 0 and marchineId in (select machineId from db_home.tbl_printor) and printTime between '$stime' and '$etime'"; // and marchineId in (select machineId from tbl_printor) 
                        $sqlp = "select * from tbl_prbusiness where exit_phrase >= 5  and printset is not null and printset != '' and machineId in (select machineId from db_home.tbl_printor where printorId in (" . Yii::app()->session['printor_id'] . "))  and starttime between '$stime' and '$etime'"; //and machineId in (select machineId from db_home.tbl_printor) 
                    }
                }
                $sqls_result = $connection2->createCommand($sqls)->query();
                foreach ($sqls_result as $q => $w) {
                    if ($w["printSet"] != "") {
                        $filePage = explode(',', $w["printSet"]);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $totalPagex = 0;
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPagex +=(int) $filePa * $w["successNumbers"] + $w["errorPage"];
                                ;
                                if ($w["double_sided"] == 0) {//单面
                                    $totalPage += $totalPagex;
                                } else {
                                    $totalPage += ceil($totalPagex / 2);
                                }
                            }
                        }
                    }
                }
                $sqlp_result = $connection->createCommand($sqlp)->query();
                foreach ($sqlp_result as $q => $w) {
                    if ($w["exit_phrase"] == 6) {
                        if ($w["printset"] != "") {
                            $filePage = explode(',', $w["printset"]);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $totalPagex = 0;
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPagex +=(int) $filePa * $w["copies"];
                                    if ($w["is_duplex_page"] == 1) {//单面
                                        $totalPage += $totalPagex;
                                    } else {
                                        $totalPage += ceil($totalPagex / 2);
                                    }
                                }
                            }
                        }
                    } else {
                        if ($w["is_duplex_page"] == 1) {//单面
                            if ($w["printedpages"] != "") {
                                $totalPage +=$w["printedpages"];
                            } else {
                                $totalPagex = 0;
                                if ($w["printset"] != "") {
                                    $filePage = explode(',', $w["printset"]);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w["copies"];
                                            $totalPage += $totalPagex;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($w["printedpages"] != "") {
                                $totalPage +=ceil($w["printedpages"] / 2);
                            } else {
                                $totalPagex = 0;
                                if ($w["printset"] != "") {
                                    $filePage = explode(',', $w["printset"]);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w["copies"];
                                            $totalPage += ceil($totalPagex / 2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $totalPages +=$totalPage;
                array_push($dayArray, array('totalPage' => $totalPage, 'day' => $i));
            }
        }
        if ($types == "month") {
            $year = date("Y", strtotime($starttime));
            for ($i = 1; $i <= 12; $i++) {
                if ($i != 12) {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                } else {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . $i . "-31 23:59:59";
                }
                $totalPage = 0;
                if ($storeid != 0) {
                    $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness where _businessId in(" . $strt . ") and status != 0 and marchineId in (select machineId from db_home.tbl_printor where _storeId in ($storeid)) and printTime between '$stime' and '$etime'";
                    $sqlp = "select * from tbl_prbusiness where _storeid in ($storeid) and exit_phrase >= 5 and machineId in (select machineId from db_home.tbl_printor where _storeId in ($storeid)) and printset is not null and printset != '' and starttime between '$stime' and '$etime'";
                } else {
                    $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness where _businessId in(" . $strt . ") and status != 0 and marchineId in (select machineId from db_home.tbl_printor) and printTime between '$stime' and '$etime'"; // and marchineId in (select machineId from tbl_printor) 
                    $sqlp = "select * from tbl_prbusiness where exit_phrase >= 5  and printset is not null and printset != '' and machineId in (select machineId from db_home.tbl_printor)  and starttime between '$stime' and '$etime'"; //and machineId in (select machineId from db_home.tbl_printor) 
                }
                $sqls_result = $connection2->createCommand($sqls)->query();
                foreach ($sqls_result as $q => $w) {
                    if ($w["printSet"] != "") {
                        $filePage = explode(',', $w["printSet"]);
                        foreach ($filePage as $z => $x) {
                            if (is_numeric($x)) {
                                $totalPage += 1;
                            } else {
                                $totalPagex = 0;
                                $filePag = explode('-', $x);
                                $filePa = $filePag[1] - $filePag[0] + 1;
                                $totalPagex +=(int) $filePa * $w["successNumbers"] + $w["errorPage"];
                                ;
                                if ($w["double_sided"] == 0) {//单面
                                    $totalPage += $totalPagex;
                                } else {
                                    $totalPage += ceil($totalPagex / 2);
                                }
                            }
                        }
                    }
                }
                $sqlp_result = $connection->createCommand($sqlp)->query();
                foreach ($sqlp_result as $q => $w) {
                    if ($w["exit_phrase"] == 6) {
                        if ($w["printset"] != "") {
                            $filePage = explode(',', $w["printset"]);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $totalPagex = 0;
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPagex +=(int) $filePa * $w["copies"];
                                    if ($w["is_duplex_page"] == 1) {//单面
                                        $totalPage += $totalPagex;
                                    } else {
                                        $totalPage += ceil($totalPagex / 2);
                                    }
                                }
                            }
                        }
                    } else {
                        if ($w["is_duplex_page"] == 1) {//单面
                            if ($w["printedpages"] != "") {
                                $totalPage +=$w["printedpages"];
                            } else {
                                $totalPagex = 0;
                                if ($w["printset"] != "") {
                                    $filePage = explode(',', $w["printset"]);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w["copies"];
                                            $totalPage += $totalPagex;
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($w["printedpages"] != "") {
                                $totalPage +=ceil($w["printedpages"] / 2);
                            } else {
                                $totalPagex = 0;
                                if ($w["printset"] != "") {
                                    $filePage = explode(',', $w["printset"]);
                                    foreach ($filePage as $z => $x) {
                                        if (is_numeric($x)) {
                                            $totalPage += 1;
                                        } else {
                                            $filePag = explode('-', $x);
                                            $filePa = $filePag[1] - $filePag[0] + 1;
                                            $totalPagex +=(int) $filePa * $w["copies"];
                                            $totalPage += ceil($totalPagex / 2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                $totalPages +=$totalPage;
                array_push($dayArray, array('totalPage' => $totalPage, 'day' => $i));
            }
        }
        if (isset($store_info)) {
            if (count($store_info) == 1) {
                foreach ($store_info as $k => $l) {
                    $printpaperarr["schoolname"] = $l->storename; //学校名称
                }
            } else {
                $printpaperarr["schoolname"] = "";
            }
        } else {
            $printpaperarr["schoolname"] = "";
        }
        $printpaperarr["storeid"] = $storeid; //storeid
        $printpaperarr["year"] = $year; //年份
        $printpaperarr["totalPages"] = $totalPages; //总打印纸张数
        $printpaperarr["dayArray"] = $dayArray; //总打印纸张数
        return $printpaperarr;
    }

    //下载EXCEL
    public function statistics_printpaper_day_total($storeid = 0, $starttime, $endtime) {
        $store_model = store::model();
        $printor_model = printor::model();

        if ($storeid != 0) {
            $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $printor_info = $printor_model->findAllBySql("select * from tbl_printor where _storeId in (" . $storeid . ")");
            } else {
                $printor_info = $printor_model->findAllBySql("select * from tbl_printor where _storeId in ($storeid) and printorId in (" . Yii::app()->session['printor_id'] . ")");
            }
        } else {
            $store_info = $store_model->findAllBySql("select * from tbl_store", array('order' => "storeid DESC"));

            if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                $printor_info = $printor_model->findAllBySql("select * from tbl_printor");
            } else {
                $printor_info = $printor_model->findAllBySql("select * from tbl_printor where printorId in (" . Yii::app()->session['printor_id'] . ")");
            }
        }
        $connection = Yii::app()->db;
        $connection2 = Yii::app()->db2;
        if ($storeid == 0) {
            $stt = "select businessid from tbl_business";
        } else {
            $stt = "select businessid from tbl_business where _storeid in ($storeid)";
        }
        $sttx = $connection2->createCommand($stt)->query();
        $strt = "";
        foreach ($sttx as $k => $v) {
            $strt .= $v['businessid'] . ",";
        }
        $strt = substr($strt, 0, -1);
        if (strlen($strt) == 0) {
            $strt = 0;
        }
        $printpaperarr = array(); //返回的数组

        $totalPages = 0; //此学校所有终端的打印纸张
        $dayArray_pages = array();
        $dayArray_printor = array();
        $year = date("Y", strtotime($starttime));
        while (strtotime($starttime) <= strtotime($endtime)) {
            $stime = $starttime . " 00:00:00";
            $etime = $starttime . " 23:59:59";
            $i = date("d", strtotime($stime));
            $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
            $totalPage = 0;
            if (count($printor_info) != 0) {
                foreach ($printor_info as $l => $y) {
                    $totalPage = 0;
                    $machineId = $y->machineId;
                    if ($storeid != 0) {
                        $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness where _businessId in(" . $strt . ") and status != 0 and marchineId = '$machineId' and printTime between '$stime' and '$etime'";
                        $sqlp = "select * from tbl_prbusiness where _storeid in ($storeid) and exit_phrase >= 5 and machineId = '$machineId' and printset is not null and printset != '' and starttime between '$stime' and '$etime'";
                    } else {
                        $sqls = "select printSet,successNumbers,double_sided,errorPage from tbl_subbusiness where status != 0 and marchineId = '$machineId' and printTime between '$stime' and '$etime'";
                        $sqlp = "select * from tbl_prbusiness where exit_phrase >= 5 and machineId = '$machineId' and printset is not null and printset != '' and starttime between '$stime' and '$etime'";
                    }
                    foreach ($connection2->createCommand($sqls)->query() as $q => $w) {
                        if ($w["printSet"] != "") {
                            $filePage = explode(',', $w["printSet"]);
                            foreach ($filePage as $z => $x) {
                                if (is_numeric($x)) {
                                    $totalPage += 1;
                                } else {
                                    $totalPagex = 0;
                                    $filePag = explode('-', $x);
                                    $filePa = $filePag[1] - $filePag[0] + 1;
                                    $totalPagex +=(int) $filePa * $w["successNumbers"] + $w["errorPage"];
                                    if ($w["double_sided"] == 0) {//单面
                                        $totalPage += $totalPagex;
                                    } else {
                                        $totalPage += ceil($totalPagex / 2);
                                    }
                                }
                            }
                        }
                    }

                    foreach ($connection->createCommand($sqlp)->query() as $q => $w) {
                        if ($w["exit_phrase"] == 6) {
                            if ($w["printset"] != "") {
                                $filePage = explode(',', $w["printset"]);
                                foreach ($filePage as $z => $x) {
                                    if (is_numeric($x)) {
                                        $totalPage += 1;
                                    } else {
                                        $totalPagex = 0;
                                        $filePag = explode('-', $x);
                                        $filePa = $filePag[1] - $filePag[0] + 1;
                                        $totalPagex +=(int) $filePa * $w["copies"];
                                        if ($w["is_duplex_page"] == 1) {//单面
                                            $totalPage += $totalPagex;
                                        } else {
                                            $totalPage += ceil($totalPagex / 2);
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($w["is_duplex_page"] == 1) {//单面
                                if ($w["printedpages"] != "") {
                                    $totalPage +=$w["printedpages"];
                                } else {
                                    $totalPagex = 0;
                                    if ($w["printset"] != "") {
                                        $filePage = explode(',', $w["printset"]);
                                        foreach ($filePage as $z => $x) {
                                            if (is_numeric($x)) {
                                                $totalPage += 1;
                                            } else {
                                                $filePag = explode('-', $x);
                                                $filePa = $filePag[1] - $filePag[0] + 1;
                                                $totalPagex +=(int) $filePa * $w["copies"];
                                                $totalPage += $totalPagex;
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($w["printedpages"] != "") {
                                    $totalPage +=ceil($w["printedpages"] / 2);
                                } else {
                                    $totalPagex = 0;
                                    if ($w["printset"] != "") {
                                        $filePage = explode(',', $w["printset"]);
                                        foreach ($filePage as $z => $x) {
                                            if (is_numeric($x)) {
                                                $totalPage += 1;
                                            } else {
                                                $filePag = explode('-', $x);
                                                $filePa = $filePag[1] - $filePag[0] + 1;
                                                $totalPagex +=(int) $filePa * $w["copies"];
                                                $totalPage += ceil($totalPagex / 2);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $totalPages +=$totalPage;
                    array_push($dayArray_printor, array('machineId' => $y->machineId, 'printorName' => $y->printorName, 'address' => $y->address, 'totalPage' => $totalPage));
                }
            }
            array_push($dayArray_pages, $dayArray_printor);
        }
        if (isset($store_info)) {
            if (count($store_info) == 1) {
                foreach ($store_info as $k => $l) {
                    $printpaperarr["schoolname"] = $l->storename; //学校名称
                }
            } else {
                $printpaperarr["schoolname"] = "";
            }
        } else {
            $printpaperarr["schoolname"] = "";
        }
        $printpaperarr["storeid"] = $storeid; //storeid
        $printpaperarr["year"] = $year; //年份
        $printpaperarr["totalPages"] = $totalPages; //总打印纸张数
        $printpaperarr["dayArray_pages"] = $dayArray_pages; //总打印纸张数
        return $printpaperarr;
    }

}
