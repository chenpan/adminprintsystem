<?php

class statisticssorder {

//本月订单量的各种统计 按每天的方式统计
    public function statsticssorder_day($storeid = 0, $starttime, $endtime, $types) {

        if ($storeid != 0) {
            $store_model = store::model();
            $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }

        $orderarr = array();

        $dayArray = array(); //web订单量
        $dayArray_PC = array(); //webPC订单量
        $dayArray_APP = array(); //webAPP订单量
        $dayArray_WX = array(); //webWX订单量
        $dayArray_refund_web = array(); //web订单退款量
        $dayArray_refundrate_web = array(); //web订单退款率
        $dayArray_refundrate_terminal = array(); //终端订单退款率
        $dayArray_refundrate_total = array(); //订单总退款率
        $dayArray_refund_total = array(); //终端总订单退款量
        $dayArray_refund_terminal = array(); //终端订单退款量
        $dayArray_pro = array(); //终端订单量
        $dayArray_total = array(); //总订单量

        $totalCount_dayss = 0; //web订单共有多少
        $totalCount_dayss_PC = 0; //webPC订单共有多少
        $totalCount_dayss_APP = 0; //webAPP订单共有多少
        $totalCount_dayss_WX = 0; //webWX订单共有多少
        $totalCount_days_refund = 0; //订单退款共有多少
        $totalCount_days_refund_web = 0; //web订单退款共有多少
        $totalCount_days_refund_terminal = 0; //终端订单退款共有多少
        $totalCount_dayspros = 0; //终端订单共有多少
        $totalCount_dattotals = 0; //总订单共有多少
//按日统计
//            $day = date("Y-m"); //2015-11
//            $fate = date("d"); //本月有多少天
//            $month = date("m"); //11月
        if ($types == "day") {


            $year = date("Y", strtotime($starttime));

            while (strtotime($starttime) <= strtotime($endtime)) {
                $stime = $starttime . " 00:00:00";
                $etime = $starttime . " 23:59:59";

                $i = date("d", strtotime($stime));

                $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));
//WEB每日订单量
                $connection = Yii::app()->db;
                $connection2 = Yii::app()->db2;
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select count(businessid) as total_orders from tbl_business where _storeid in ($storeid) and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sqls = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a._storeid in ($storeid)"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select count(businessid) as total_orders from tbl_business where placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sqls = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE"
                                . " a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqls)->query() as $k => $v) {
                    $totalCount_days = $v['total_orders'];
                }
//                $totalCount_days = count($connection2->createCommand($sqls)->query());

                $totalCount_dayss += $totalCount_days;
                array_push($dayArray, array('totalCount_days' => $totalCount_days, 'day' => $i));

//终端每日订单量

                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select count(prbusinessid) as total_orders from tbl_prbusiness where starttime between '$stime' and '$etime' AND exit_phrase >= 5 AND _storeid in ($storeid)";
                    } else {
                        $sqls = "select count(a.prbusinessid) as total_orders from tbl_prbusiness a"
                                . " join db_home.tbl_printor b on a.machineId = b.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.exit_phrase >= 5 and a._storeid in ($storeid)"
                                . " and b.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select count(prbusinessid) as total_orders from tbl_prbusiness"
                                . " where starttime between '$stime' and '$etime' AND exit_phrase >= 5";
                    } else {
                        $sqls = "select count(a.prbusinessid) as total_orders from tbl_prbusiness a"
                                . " join db_home.tbl_printor b on a.machineId = b.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.exit_phrase >= 5"
                                . " and b.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection->createCommand($sqls)->query() as $k => $v) {
                    $totalCount_dayspro = $v['total_orders'];
                }
//                $totalCount_dayspro = count($connection->createCommand($sqls)->query());

                $totalCount_dayspros += $totalCount_dayspro;
                array_push($dayArray_pro, array('totalCount_days' => $totalCount_dayspro, 'day' => $i));

//总订单量
                $totalCount_dattotals += $totalCount_days + $totalCount_dayspro;
                array_push($dayArray_total, array('totalCount_days' => $totalCount_days + $totalCount_dayspro, 'day' => $i));

//web端订单PC端订单量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_PC = "select count(businessid) as total_orders from tbl_business where _storeid in ($storeid) and medium = 1 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_PC = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a._storeid in ($storeid) and a.medium = 1"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_PC = "select count(businessid) as total_orders from tbl_business where medium = 1 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_PC = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a.medium = 1"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sql_PC)->query() as $k => $v) {
                    $totalCount_days_PC = $v['total_orders'];
                }
//                $totalCount_days_PC = count($connection2->createCommand($sql_PC)->query());

                $totalCount_dayss_PC += $totalCount_days_PC;
                array_push($dayArray_PC, array('totalCount_days_PC' => $totalCount_days_PC, 'day' => $i));

//web端订单手机APP端订单量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_APP = "select count(businessid) as total_orders from tbl_business where _storeid in ($storeid) and medium = 2 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_APP = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a._storeid in ($storeid) and a.medium = 2"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_APP = "select count(businessid) as total_orders from tbl_business where  medium = 2 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_APP = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a.medium = 2"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sql_APP)->query() as $k => $v) {
                    $totalCount_days_APP = $v['total_orders'];
                }
//                $totalCount_days_APP = count($connection2->createCommand($sql_APP)->query());

                $totalCount_dayss_APP += $totalCount_days_APP;
                array_push($dayArray_APP, array('totalCount_days_APP' => $totalCount_days_APP));

//web端订单微信端订单量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_WX = "select count(businessid) as total_orders from tbl_business where _storeid in ($storeid) and medium = 3 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_WX = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a._storeid in ($storeid) and a.medium = 3"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_WX = "select count(businessid) as total_orders from tbl_business where  medium = 3 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_WX = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a.medium = 3"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sql_WX)->query() as $k => $v) {
                    $totalCount_days_WX = $v['total_orders'];
                }
//                $totalCount_days_WX = count($connection2->createCommand($sql_WX)->query());

                $totalCount_dayss_WX += $totalCount_days_WX;
                array_push($dayArray_WX, array('totalCount_days_WX' => $totalCount_days_WX));

                $refund_model = refund::model();
//web订单退款量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlq = "select count(distinct a.businessid) as total_orders from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " where c.applyTime between '$stime' and '$etime' and c.subbusinessId is not null and a._storeid in ($storeid)";
//                        $sqlq = "select count(*) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is not null and _storeid in ($storeid)";
                    } else {
                        $sqlq = "select count(distinct a.businessid) as total_orders from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " join tbl_printor d on b.marchineId = d.machineId"
                                . " where c.applyTime between '$stime' and '$etime' and c.tborderId != ''"
                                . " and d._storeid in ($storeid) and c._storeid in ($storeid) and d.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlq = "select count(distinct a.businessid) as total_orders from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " where c.applyTime between '$stime' and '$etime' and c.subbusinessId is not null";
//                        $sqlq = "select count(*) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is not null";
                    } else {
                        $sqlq = "select count(distinct a.businessid) as total_orders from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " join tbl_printor d on b.marchineId = d.machineId"
                                . " where c.applyTime between '$stime' and '$etime' and c.tborderId != ''"
                                . " and d._storeid in ($storeid) and d.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlq)->query() as $k => $v) {
                    $totalCount_refund_web = $v['total_orders'];
                }
//                $totalCount_refund_web = count($connection2->createCommand($sqlq)->query());
                $totalCount_days_refund_web += $totalCount_refund_web;

                $totalCount_days_refund += $totalCount_refund_web;
                array_push($dayArray_refund_web, array('totalCount_refund_web' => $totalCount_refund_web, 'day' => $i));
//终端订单退款量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlw = "select count(refundId) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null and _storeid in ($storeid)";
//                        $sqlw = "select count(*) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null and _storeid in ($storeid)";
                    } else {
                        $sqlw = "select count(a.refundId) as total_orders from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlw = "select count(refundId) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null";
//                        $sqlw = "select count(*) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null";
                    } else {
                        $sqlw = "select count(a.refundId) as total_orders from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlw)->query() as $k => $v) {
                    $totalCount_refund_terminal = $v['total_orders'];
                }
//                $totalCount_refund_terminal = count($connection2->createCommand($sqlw)->query());
                $totalCount_days_refund_terminal += $totalCount_refund_terminal;

                $totalCount_days_refund += $totalCount_refund_terminal;
                array_push($dayArray_refund_terminal, array('totalCount_refund_terminal' => $totalCount_refund_terminal));

//web端退款率
                $num = 0;
                if ($totalCount_days == 0) {
                    $num = 0;
                } else {
                    $num = $totalCount_refund_web / $totalCount_days * 100;
                }
                array_push($dayArray_refundrate_web, array('dayArray_refundrate_web' => sprintf("%.2f", $num), 'day' => $i));
//终端退款率
                if ($totalCount_dayspro == 0) {
                    $num = 0;
                } else {
                    $num = $totalCount_refund_terminal / $totalCount_dayspro * 100;
                }
                array_push($dayArray_refundrate_terminal, array('dayArray_refundrate_terminal' => sprintf("%.2f", $num)));
//订单总退款率
                if (($totalCount_days + $totalCount_dayspro) == 0) {
                    $num = 0;
                } else {
                    $num = ($totalCount_refund_web + $totalCount_refund_terminal) / ($totalCount_days + $totalCount_dayspro) * 100;
                }
                array_push($dayArray_refundrate_total, array('dayArray_refundrate_total' => sprintf("%.2f", $num)));
//总退款订单量                    
                array_push($dayArray_refund_total, array('dayArray_refund_total' => $totalCount_refund_web + $totalCount_refund_terminal, 'day' => $i));
            }
        }
        if ($types == "month") {
            $year = date("Y", strtotime($starttime));
            for ($i = 1; $i <= 12; $i++) {
                if ($i != 12) {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                } else {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . $i . "-31 23:59:59";
                }
//WEB每日订单量
                $connection = Yii::app()->db;
                $connection2 = Yii::app()->db2;
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select count(businessid) as total_orders from tbl_business where _storeid in ($storeid) and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sqls = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a._storeid in ($storeid)"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select count(businessid) as total_orders from tbl_business where placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sqls = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE"
                                . " a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqls)->query() as $k => $v) {
                    $totalCount_days = $v['total_orders'];
                }
//                $totalCount_days = count($connection2->createCommand($sqls)->query());

                $totalCount_dayss += $totalCount_days;
                array_push($dayArray, array('totalCount_days' => $totalCount_days, 'day' => $i));

//终端每日订单量

                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select count(prbusinessid) as total_orders from tbl_prbusiness where starttime between '$stime' and '$etime' AND exit_phrase >= 5 AND _storeid in ($storeid)";
                    } else {
                        $sqls = "select count(a.prbusinessid) as total_orders from tbl_prbusiness a"
                                . " join db_home.tbl_printor b on a.machineId = b.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.exit_phrase >= 5 and a._storeid in ($storeid)"
                                . " and b.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqls = "select count(prbusinessid) as total_orders from tbl_prbusiness"
                                . " where starttime between '$stime' and '$etime' AND exit_phrase >= 5";
                    } else {
                        $sqls = "select count(a.prbusinessid) as total_orders from tbl_prbusiness a"
                                . " join db_home.tbl_printor b on a.machineId = b.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.exit_phrase >= 5"
                                . " and b.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection->createCommand($sqls)->query() as $k => $v) {
                    $totalCount_dayspro = $v['total_orders'];
                }
//                $totalCount_dayspro = count($connection->createCommand($sqls)->query());

                $totalCount_dayspros += $totalCount_dayspro;
                array_push($dayArray_pro, array('totalCount_days' => $totalCount_dayspro, 'day' => $i));

//总订单量
                $totalCount_dattotals += $totalCount_days + $totalCount_dayspro;
                array_push($dayArray_total, array('totalCount_days' => $totalCount_days + $totalCount_dayspro, 'day' => $i));

//web端订单PC端订单量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_PC = "select count(businessid) as total_orders from tbl_business where _storeid in ($storeid) and medium = 1 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_PC = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a._storeid in ($storeid) and a.medium = 1"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_PC = "select count(businessid) as total_orders from tbl_business where medium = 1 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_PC = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a.medium = 1"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sql_PC)->query() as $k => $v) {
                    $totalCount_days_PC = $v['total_orders'];
                }
//                $totalCount_days_PC = count($connection2->createCommand($sql_PC)->query());

                $totalCount_dayss_PC += $totalCount_days_PC;
                array_push($dayArray_PC, array('totalCount_days_PC' => $totalCount_days_PC, 'day' => $i));

//web端订单手机APP端订单量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_APP = "select count(businessid) as total_orders from tbl_business where _storeid in ($storeid) and medium = 2 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_APP = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a._storeid in ($storeid) and a.medium = 2"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_APP = "select count(businessid) as total_orders from tbl_business where  medium = 2 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_APP = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a.medium = 2"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sql_APP)->query() as $k => $v) {
                    $totalCount_days_APP = $v['total_orders'];
                }
//                $totalCount_days_APP = count($connection2->createCommand($sql_APP)->query());

                $totalCount_dayss_APP += $totalCount_days_APP;
                array_push($dayArray_APP, array('totalCount_days_APP' => $totalCount_days_APP));

//web端订单微信端订单量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_WX = "select count(businessid) as total_orders from tbl_business where _storeid in ($storeid) and medium = 3 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_WX = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a._storeid in ($storeid) and a.medium = 3"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sql_WX = "select count(businessid) as total_orders from tbl_business where  medium = 3 and placeOrdertime between '$stime' and '$etime'";
                    } else {
                        $sql_WX = "SELECT count(distinct a.businessid) as total_orders FROM tbl_business a join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_printor c on b.marchineId = c.machineId WHERE a.medium = 3"
                                . " and a.placeOrdertime between '$stime' and '$etime' and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sql_WX)->query() as $k => $v) {
                    $totalCount_days_WX = $v['total_orders'];
                }
//                $totalCount_days_WX = count($connection2->createCommand($sql_WX)->query());

                $totalCount_dayss_WX += $totalCount_days_WX;
                array_push($dayArray_WX, array('totalCount_days_WX' => $totalCount_days_WX));

                $refund_model = refund::model();
//web订单退款量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlq = "select count(distinct a.businessid) as total_orders from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " where c.applyTime between '$stime' and '$etime' and c.subbusinessId is not null and a._storeid in ($storeid)";
//                        $sqlq = "select count(*) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is not null and _storeid in ($storeid)";
                    } else {
                        $sqlq = "select count(distinct a.businessid) as total_orders from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " join tbl_printor d on b.marchineId = d.machineId"
                                . " where c.applyTime between '$stime' and '$etime' and c.tborderId != ''"
                                . " and d._storeid in ($storeid) and c._storeid in ($storeid) and d.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlq = "select count(distinct a.businessid) as total_orders from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " where c.applyTime between '$stime' and '$etime' and c.subbusinessId is not null";
//                        $sqlq = "select count(*) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is not null";
                    } else {
                        $sqlq = "select count(distinct a.businessid) as total_orders from tbl_business a"
                                . " join tbl_subbusiness b on a.businessid = b._businessId"
                                . " join tbl_refund c on b.subbusinessId = c.subbusinessId"
                                . " join tbl_printor d on b.marchineId = d.machineId"
                                . " where c.applyTime between '$stime' and '$etime' and c.tborderId != ''"
                                . " and d._storeid in ($storeid) and d.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlq)->query() as $k => $v) {
                    $totalCount_refund_web = $v['total_orders'];
                }
//                $totalCount_refund_web = count($connection2->createCommand($sqlq)->query());
                $totalCount_days_refund_web += $totalCount_refund_web;

                $totalCount_days_refund += $totalCount_refund_web;
                array_push($dayArray_refund_web, array('totalCount_refund_web' => $totalCount_refund_web, 'day' => $i));
//终端订单退款量
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlw = "select count(refundId) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null and _storeid in ($storeid)";
//                        $sqlw = "select count(*) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null and _storeid in ($storeid)";
                    } else {
                        $sqlw = "select count(a.refundId) as total_orders from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlw = "select count(refundId) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null";
//                        $sqlw = "select count(*) as total_orders from tbl_refund where applyTime between '$stime' and '$etime' and subbusinessId is null";
                    } else {
                        $sqlw = "select count(a.refundId) as total_orders from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlw)->query() as $k => $v) {
                    $totalCount_refund_terminal = $v['total_orders'];
                }
//                $totalCount_refund_terminal = count($connection2->createCommand($sqlw)->query());
                $totalCount_days_refund_terminal += $totalCount_refund_terminal;

                $totalCount_days_refund += $totalCount_refund_terminal;
                array_push($dayArray_refund_terminal, array('totalCount_refund_terminal' => $totalCount_refund_terminal));

//web端退款率
                $num = 0;
                if ($totalCount_days == 0) {
                    $num = 0;
                } else {
                    $num = $totalCount_refund_web / $totalCount_days * 100;
                }
                array_push($dayArray_refundrate_web, array('dayArray_refundrate_web' => sprintf("%.2f", $num), 'day' => $i));
//终端退款率
                if ($totalCount_dayspro == 0) {
                    $num = 0;
                } else {
                    $num = $totalCount_refund_terminal / $totalCount_dayspro * 100;
                }
                array_push($dayArray_refundrate_terminal, array('dayArray_refundrate_terminal' => sprintf("%.2f", $num)));
//订单总退款率
                if (($totalCount_days + $totalCount_dayspro) == 0) {
                    $num = 0;
                } else {
                    $num = ($totalCount_refund_web + $totalCount_refund_terminal) / ($totalCount_days + $totalCount_dayspro) * 100;
                }
                array_push($dayArray_refundrate_total, array('dayArray_refundrate_total' => sprintf("%.2f", $num)));
//总退款订单量                    
                array_push($dayArray_refund_total, array('dayArray_refund_total' => $totalCount_refund_web + $totalCount_refund_terminal, 'day' => $i));
            }
        }
        $orderarr["totalCount_dattotals"] = $totalCount_dattotals; //总订单有多少
        $orderarr["dayArray_total"] = $dayArray_total; //总订单量数组
        $orderarr["totalCount_dayspros"] = $totalCount_dayspros; //终端订单共有多少
        $orderarr["dayArray_pro"] = $dayArray_pro; //终端订单量数组
        $orderarr["totalCount_dayss"] = $totalCount_dayss; //web订单共有多少
        if (isset($store_info)) {
            if (count($store_info) == 1) {
                foreach ($store_info as $k => $l) {
                    $orderarr["schoolname"] = $l->storename; //学校名称
                }
            } else {
                $orderarr["schoolname"] = "";
            }
        } else {
            $orderarr["schoolname"] = "";
        }
        $orderarr["year"] = $year; //年份
        $orderarr["dayArray"] = $dayArray; //web订单量数组
        $orderarr["storeid"] = $storeid; //storeid
        $orderarr["dayArray_PC"] = $dayArray_PC; //webPC订单量数组
        $orderarr["dayArray_APP"] = $dayArray_APP; //webAPP订单量数组
        $orderarr["dayArray_WX"] = $dayArray_WX; //webWX订单量数组
        $orderarr["totalCount_dayss_PC"] = $totalCount_dayss_PC; //webPC订单共有多少
        $orderarr["totalCount_dayss_APP"] = $totalCount_dayss_APP; //webAPP订单共有多少
        $orderarr["totalCount_dayss_WX"] = $totalCount_dayss_WX; //webWX订单共有多少
        $orderarr["dayArray_refund_web"] = $dayArray_refund_web; //web订单退款量数组
        $orderarr["dayArray_refund_terminal"] = $dayArray_refund_terminal; //终端订单退款量数组
        $orderarr["totalCount_days_refund"] = $totalCount_days_refund; //订单退款共有多少
        $orderarr["dayArray_refund_total"] = $dayArray_refund_total; //终端总订单退款量数组
        $orderarr["totalCount_days_refund_web"] = $totalCount_days_refund_web; //web订单退款共有多少
        $orderarr["totalCount_days_refund_terminal"] = $totalCount_days_refund_terminal; //终端订单退款共有多少
        $orderarr["dayArray_refundrate_web"] = $dayArray_refundrate_web; //web订单退款率数组
        $orderarr["dayArray_refundrate_terminal"] = $dayArray_refundrate_terminal; //终端订单退款率数组
        $orderarr["dayArray_refundrate_total"] = $dayArray_refundrate_total; //订单总退款率数组
        return $orderarr;
    }

}
