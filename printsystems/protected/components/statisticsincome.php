<?php

class statisticsincome {

    public function statistics_income($storeid = 0, $starttime, $endtime, $types) {
        if ($storeid != 0) {
            $store_model = store::model();
            $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }

        //判断终端订单是否退款
        $connection = Yii::app()->db; //home
        $connection2 = Yii::app()->db2; //admin
        $sqlx = "select _sessionId from tbl_refund WHERE _sessionId is not null";
        $rowx = $connection2->createCommand($sqlx)->query();
        $str = "";
        foreach ($rowx as $k => $v) {
            $str .= $v['_sessionId'] . ",";
        }
        $str = substr($str, 0, -1);
        if (strlen($str) == 0) {
            $str = 0;
        }
        //本订单是否属于此学校
        if ($storeid == 0) {
            $stt = "select businessid from tbl_business";
        } else {
            $stt = "select businessid from tbl_business where _storeid in ($storeid)";
        }
        $sttx = $connection2->createCommand($stt)->query();
        $strt = "";
        foreach ($sttx as $k => $v) {
            $strt .= $v['businessid'] . ",";
        }
        $strt = substr($strt, 0, -1);
        if (strlen($strt) == 0) {
            $strt = 0;
        }

        $incomearr = array(); //返回的数组

        $dayArray_removerefund = array(); //除去退款每日收入金额
        $dayArray = array(); //不除去退款每日收入金额
        $dayArray_removerefund_integral = array(); //除去退款每日收入积分
        $dayArray_removerefund_money = array(); //除去退款除去积分每日收入金额
        $dayArray_integral = array(); //不除去退款每日收入积分
        $dayArray_money = array(); //不除去退款除去积分每日收入金额
        $dayArray_removerefund_money_zfb = array(); //除去退款除去积分支付宝每日收入金额
        $dayArray_removerefund_money_wx = array(); //除去退款除去积分微信每日收入金额
        $dayArray_money_zfb = array(); //不除去退款除去积分每日支付宝收入金额
        $dayArray_money_wx = array(); //不除去退款除去积分每日微信收入金额
        $darArray_refundrate_integral = array(); //每日积分退款率
        $darArray_refundrate_money = array(); //每日金额退款率
        $darArray_refundrate_total = array(); //每日收入总退款率


        $totalIncome_removerefund_dayss = 0; //除去退款总收入
        $totalIncome_dayss = 0; //不除去退款总收入
        $totalIncome_removerefund_integrals = 0; //除去退款收入总积分
        $total_removerefund_money = 0; //除去退款除去积分收入总金额
        $total_integrals = 0; //不除去退款收入总积分
        $total_moneys = 0; //不除去退款除去积分收入总金额
        $total_removerefund_money_zfbs = 0; //除去退款除去积分支付宝收入总金额
        $total_removerefund_money_wxs = 0; //除去退款除去积分微信收入总金额
        $total_money_zfbs = 0; //不除去退款除去积分支付宝收入总金额
        $total_money_wxs = 0; //不除去退款除去积分微信收入总金额
        $total_refundrate_integral = 0; //积分总退款率
        $total_refundrate_money = 0; //金额总退款率

        if ($types == "day") {
            $year = date("Y", strtotime($starttime));
            while (strtotime($starttime) <= strtotime($endtime)) {
                $stime = $starttime . " 00:00:00";
                $etime = $starttime . " 23:59:59";
                $i = date("d", strtotime($stime));
                $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));

                $totalIncome_removerefund_day = 0; //每日除去退款收入总金额
                //除去退款纯收入
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsx = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0";
                        $sqlpx = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''";
                    } else {
                        $sqlsx = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpx = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsx = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0";
                        $sqlpx = "select sum(paidmoney) as paidMoney from tbl_prbusiness where sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''";
                    } else {
                        $sqlsx = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpx = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsx)->query() as $k => $v) {
                    $totalIncome_removerefund_day += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpx)->query() as $k => $v) {
                    $totalIncome_removerefund_day += $v['paidMoney'];
                }
                $totalIncome_removerefund_dayss += sprintf("%.2f", $totalIncome_removerefund_day);
                array_push($dayArray_removerefund, array('totalIncome_removerefund_day' => sprintf("%.2f", $totalIncome_removerefund_day), 'day' => $i));


                $totalIncome_day = 0; //每日不除去退款收入总金额
                //不除去退款的总金额
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsf = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1";
                        $sqlpf = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != ''";
                    } else {
                        $sqlsf = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpf = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsf = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1";
                        $sqlpf = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != ''";
                    } else {
                        $sqlsf = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpf = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsf)->query() as $k => $v) {
                    $totalIncome_day += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpf)->query() as $k => $v) {
                    $totalIncome_day += $v['paidMoney'];
                }
                $totalIncome_dayss += sprintf("%.2f", $totalIncome_day);
                array_push($dayArray, array('totalIncome_day' => sprintf("%.2f", $totalIncome_day), 'day' => $i));

                //除去退款每日收入积分和金额
                $total_refund_integral = 0;
                $total_refund_integralp = 0;
                $total_refund_money = 0;

                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsi = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 5";
                        $sqlsm = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and (payType = 7 or payType = 1)";
                        $sqlpm = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpi = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsi = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid)) and a.payType = 5"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsm = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0 and (a.payType = 7 or a.payType = 1)"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpm = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and (a.payType = 3 or a.payType = 4) and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpi = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and a.payType = 5 and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsi = "select sum(paidMoney) as paidMoney from tbl_subbusiness where  payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 5";
                        $sqlsm = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and (payType = 7 or payType = 1)";
                        $sqlpm = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpi = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsi = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and payType = 5 and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsm = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0 and (payType = 7 or payType = 1)"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpm = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and (payType = 3 or payType = 4) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpi = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and payType = 5 and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsi)->query() as $k => $v) {
                    $total_refund_integral += $v['paidMoney'] * 100;
                }
                foreach ($connection->createCommand($sqlpi)->query() as $k => $v) {
                    $total_refund_integralp += $v['paidMoney'] * 100;
                }
                $totalIncome_removerefund_integrals += $total_refund_integral + $total_refund_integralp;
                $total_refund_integral += sprintf("%d", $total_refund_integralp);
                array_push($dayArray_removerefund_integral, array('total_refund_integral' => sprintf("%d", $total_refund_integral), 'day' => $i));

                foreach ($connection2->createCommand($sqlsm)->query() as $k => $v) {
                    $total_refund_money += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpm)->query() as $k => $v) {
                    $total_refund_money += $v['paidMoney'];
                }

                $total_removerefund_money += sprintf("%.2f", $total_refund_money);
                array_push($dayArray_removerefund_money, array('total_refund_money' => sprintf("%.2f", $total_refund_money), 'day' => $i));

                //不除去退款每日收入积分和金额
                $total_integral = 0;
                $total_integralp = 0;
                $total_money = 0;

                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsq = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and payType = 5";
                        $sqlsl = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and (payType = 7 or payType = 1)";
                        $sqlpl = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpq = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsq = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsl = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and (a.payType = 7 or a.payType = 1)"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpl = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and (a.payType = 3 or a.payType = 4)"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpq = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsq = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and payType = 5";
                        $sqlsl = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and (payType = 7 or payType = 1)";
                        $sqlpl = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpq = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsq = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsl = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and (a.payType = 7 or a.payType = 1)"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpl = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and (a.payType = 3 or a.payType = 4)"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpq = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsq)->query() as $k => $v) {
                    $total_integral += $v['paidMoney'] * 100;
                }
                foreach ($connection->createCommand($sqlpq)->query() as $k => $v) {
                    $total_integralp += $v['paidMoney'] * 100;
                }
                $total_integrals += $total_integral + $total_integralp;
                $total_integral += $total_integralp;
                array_push($dayArray_integral, array('total_integral' => sprintf("%d", $total_integral), 'day' => $i));

                foreach ($connection2->createCommand($sqlsl)->query() as $k => $v) {
                    $total_money += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpl)->query() as $k => $v) {
                    $total_money += $v['paidMoney'];
                }
                $total_moneys += sprintf("%.2f", $total_money);
                array_push($dayArray_money, array('total_money' => sprintf("%.2f", $total_money), 'day' => $i));

                //支付宝收入统计
                $total_removerefund_money_zfb = 0; //除去退款支付宝收入
                $total_money_zfb = 0; //不除去退款支付宝收入
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //不除去退款
                        $sqlsz = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and payType = 1";
                        $sqlpz = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 3";
                        //除去退款
                        $sqlsb = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 1";
                        $sqlpb = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 3";
                    } else {
                        //不除去退款
                        $sqlsz = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 1"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpz = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 3"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        //除去退款
                        $sqlsb = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 1 and isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpb = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 3"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //不除去退款
                        $sqlsz = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and payType = 1";
                        $sqlpz = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != '' and paytype = 3";
                        //除去退款
                        $sqlsb = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 1";
                        $sqlpb = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 3";
                    } else {
                        //不除去退款
                        $sqlsz = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 1"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpz = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 3"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        //除去退款
                        $sqlsb = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 1 and isrefund = 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpb = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 3"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }

                foreach ($connection2->createCommand($sqlsz)->query() as $k => $v) {
                    $total_money_zfb += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpz)->query() as $k => $v) {
                    $total_money_zfb += $v['paidMoney'];
                }

                foreach ($connection2->createCommand($sqlsb)->query() as $k => $v) {
                    $total_removerefund_money_zfb += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpb)->query() as $k => $v) {
                    $total_removerefund_money_zfb += $v['paidMoney'];
                }

                $total_removerefund_money_zfbs += sprintf("%.2f", $total_removerefund_money_zfb);
                array_push($dayArray_removerefund_money_zfb, array('total_removerefund_money_zfb' => sprintf("%.2f", $total_removerefund_money_zfb), 'day' => $i));

                $total_money_zfbs += sprintf("%.2f", $total_money_zfb);
                array_push($dayArray_money_zfb, array('total_money_zfb' => sprintf("%.2f", $total_money_zfb), 'day' => $i));


                //微信收入统计
                $total_removerefund_money_wx = 0; //除去退款微信收入
                $total_money_wx = 0; //不除去退款微信收入
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //不除去退款
                        $sqlsw = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and payType = 7";
                        $sqlpw = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 4";
                        //除去退款
                        $sqlsr = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 7";
                        $sqlpr = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 4";
                    } else {
                        //不除去退款
                        $sqlsw = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 7"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpw = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 4"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        //除去退款
                        $sqlsr = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 7 and isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpr = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 4"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //不除去退款
                        $sqlsw = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and payType = 7";
                        $sqlpw = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != '' and paytype = 4";
                        //除去退款
                        $sqlsr = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 7";
                        $sqlpr = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 4";
                    } else {
                        //不除去退款
                        $sqlsw = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 7"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpw = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 4"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        //除去退款
                        $sqlsr = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 7 and isrefund = 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpr = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 4"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsw)->query() as $k => $v) {
                    $total_money_wx += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpw)->query() as $k => $v) {
                    $total_money_wx += $v['paidMoney'];
                }

                foreach ($connection2->createCommand($sqlsr)->query() as $k => $v) {
                    $total_removerefund_money_wx += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpr)->query() as $k => $v) {
                    $total_removerefund_money_wx += $v['paidMoney'];
                }
                $total_removerefund_money_wxs += sprintf("%.2f", $total_removerefund_money_wx);
                array_push($dayArray_removerefund_money_wx, array('total_removerefund_money_wx' => sprintf("%.2f", $total_removerefund_money_wx), 'day' => $i));

                $total_money_wxs += sprintf("%.2f", $total_money_wx);
                array_push($dayArray_money_wx, array('total_money_wx' => sprintf("%.2f", $total_money_wx), 'day' => $i));

                //退款金额和积分 用以计算收入退款率
                $total_refund_integrals = 0;
                $total_refund_integralsp = 0;
                $total_refund_moneys = 0;

                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsy = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and payType = 5 and isrefund != 0";
                        $sqlsp = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and (payType = 7 or payType = 1) and isrefund != 0";
                        $sqlpp = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and sessionId in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpy = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and sessionId in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsy = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5 and a.isrefund != 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsp = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and (a.payType = 7 or a.payType = 1) and a.isrefund != 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpp = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId in ($str) and a.starttime between '$stime' and '$etime'"
                                . " and a.trade_no != '' and (a.payType = 3 or a.payType = 4)"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";


                        $sqlpy = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId in ($str) and a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsy = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and payType = 5 and isrefund != 0";
                        $sqlsp = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and (payType = 7 or payType = 1) and isrefund != 0";
                        $sqlpp = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpy = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsy = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5 and a.isrefund != 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsp = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and (a.payType = 7 or a.payType = 1) and a.isrefund != 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpp = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId in ($str) and a.starttime between '$stime' and '$etime'"
                                . " and a.trade_no != '' and (a.payType = 3 or a.payType = 4)"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";


                        $sqlpy = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId in ($str) and a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsy)->query() as $k => $v) {
                    $total_refund_integrals = $v['paidMoney'] * 100;
                }foreach ($connection->createCommand($sqlpy)->query() as $k => $v) {
                    $total_refund_integralsp = $v['paidMoney'] * 100;
                }

                $total_refundrate_integral += $total_refund_integrals + $total_refund_integralsp;
                $total_refund_integrals += $total_refund_integralsp;
                if ($total_integral != 0) {
                    array_push($darArray_refundrate_integral, array('total_refundrate_integral' => sprintf("%.2f", $total_refund_integrals / $total_integral * 100), 'day' => $i));
                } else {
                    array_push($darArray_refundrate_integral, array('total_refundrate_integral' => 0, 'day' => $i));
                }

                foreach ($connection2->createCommand($sqlsp)->query() as $k => $v) {
                    $total_refund_moneys += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpp)->query() as $k => $v) {
                    $total_refund_moneys += $v['paidMoney'];
                }
                $total_refundrate_money += sprintf("%.2f", $total_refund_moneys);
                if ($total_money != 0) {
                    array_push($darArray_refundrate_money, array('total_refundrate_money' => sprintf("%.2f", $total_refund_moneys / $total_money * 100), 'day' => $i));
                } else {
                    array_push($darArray_refundrate_money, array('total_refundrate_money' => 0, 'day' => $i));
                }
                if ($total_integral / 100 + $total_money != 0) {
                    $num = ($total_refund_integrals / 100 + $total_refund_moneys) / ($total_integral / 100 + $total_money);
                } else {
                    $num = 0;
                }
                array_push($darArray_refundrate_total, array('total_refundrate' => sprintf("%.2f", $num * 100), 'day' => $i));
            }
        }
        if ($types == "month") {
            $year = date("Y", strtotime($starttime));
            for ($i = 1; $i <= 12; $i++) {
                if ($i != 12) {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                } else {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . $i . "-31 23:59:59";
                }

                $totalIncome_removerefund_day = 0; //每日除去退款收入总金额
                //除去退款纯收入
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsx = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0";
                        $sqlpx = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''";
                    } else {
                        $sqlsx = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpx = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsx = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0";
                        $sqlpx = "select sum(paidmoney) as paidMoney from tbl_prbusiness where sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''";
                    } else {
                        $sqlsx = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpx = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsx)->query() as $k => $v) {
                    $totalIncome_removerefund_day += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpx)->query() as $k => $v) {
                    $totalIncome_removerefund_day += $v['paidMoney'];
                }
                $totalIncome_removerefund_dayss += sprintf("%.2f", $totalIncome_removerefund_day);
                array_push($dayArray_removerefund, array('totalIncome_removerefund_day' => sprintf("%.2f", $totalIncome_removerefund_day), 'day' => $i));


                $totalIncome_day = 0; //每日不除去退款收入总金额
                //不除去退款的总金额
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsf = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1";
                        $sqlpf = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != ''";
                    } else {
                        $sqlsf = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpf = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsf = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1";
                        $sqlpf = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != ''";
                    } else {
                        $sqlsf = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpf = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsf)->query() as $k => $v) {
                    $totalIncome_day += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpf)->query() as $k => $v) {
                    $totalIncome_day += $v['paidMoney'];
                }
                $totalIncome_dayss += sprintf("%.2f", $totalIncome_day);
                array_push($dayArray, array('totalIncome_day' => sprintf("%.2f", $totalIncome_day), 'day' => $i));

                //除去退款每日收入积分和金额
                $total_refund_integral = 0;
                $total_refund_integralp = 0;
                $total_refund_money = 0;

                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsi = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 5";
                        $sqlsm = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and (payType = 7 or payType = 1)";
                        $sqlpm = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpi = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsi = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid)) and a.payType = 5"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsm = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0 and (a.payType = 7 or a.payType = 1)"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpm = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and (a.payType = 3 or a.payType = 4) and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpi = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and a.payType = 5 and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsi = "select sum(paidMoney) as paidMoney from tbl_subbusiness where  payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 5";
                        $sqlsm = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and (payType = 7 or payType = 1)";
                        $sqlpm = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpi = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsi = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0"
                                . " and payType = 5 and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsm = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.isrefund = 0 and (payType = 7 or payType = 1)"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpm = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and (payType = 3 or payType = 4) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpi = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId not in (" . $str . ") and a.starttime between '$stime' and '$etime' and a.trade_no != ''"
                                . " and payType = 5 and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsi)->query() as $k => $v) {
                    $total_refund_integral += $v['paidMoney'] * 100;
                }
                foreach ($connection->createCommand($sqlpi)->query() as $k => $v) {
                    $total_refund_integralp += $v['paidMoney'] * 100;
                }
                $totalIncome_removerefund_integrals += $total_refund_integral + $total_refund_integralp;
                $total_refund_integral += sprintf("%d", $total_refund_integralp);
                array_push($dayArray_removerefund_integral, array('total_refund_integral' => sprintf("%d", $total_refund_integral), 'day' => $i));

                foreach ($connection2->createCommand($sqlsm)->query() as $k => $v) {
                    $total_refund_money += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpm)->query() as $k => $v) {
                    $total_refund_money += $v['paidMoney'];
                }

                $total_removerefund_money += sprintf("%.2f", $total_refund_money);
                array_push($dayArray_removerefund_money, array('total_refund_money' => sprintf("%.2f", $total_refund_money), 'day' => $i));

                //不除去退款每日收入积分和金额
                $total_integral = 0;
                $total_integralp = 0;
                $total_money = 0;

                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsq = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and payType = 5";
                        $sqlsl = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and (payType = 7 or payType = 1)";
                        $sqlpl = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpq = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsq = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsl = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and (a.payType = 7 or a.payType = 1)"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpl = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and (a.payType = 3 or a.payType = 4)"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpq = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsq = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and payType = 5";
                        $sqlsl = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and (payType = 7 or payType = 1)";
                        $sqlpl = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpq = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsq = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsl = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and (a.payType = 7 or a.payType = 1)"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpl = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and (a.payType = 3 or a.payType = 4)"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpq = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsq)->query() as $k => $v) {
                    $total_integral += $v['paidMoney'] * 100;
                }
                foreach ($connection->createCommand($sqlpq)->query() as $k => $v) {
                    $total_integralp += $v['paidMoney'] * 100;
                }
                $total_integrals += $total_integral + $total_integralp;
                $total_integral += $total_integralp;
                array_push($dayArray_integral, array('total_integral' => sprintf("%d", $total_integral), 'day' => $i));

                foreach ($connection2->createCommand($sqlsl)->query() as $k => $v) {
                    $total_money += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpl)->query() as $k => $v) {
                    $total_money += $v['paidMoney'];
                }
                $total_moneys += sprintf("%.2f", $total_money);
                array_push($dayArray_money, array('total_money' => sprintf("%.2f", $total_money), 'day' => $i));

                //支付宝收入统计
                $total_removerefund_money_zfb = 0; //除去退款支付宝收入
                $total_money_zfb = 0; //不除去退款支付宝收入
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //不除去退款
                        $sqlsz = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and payType = 1";
                        $sqlpz = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 3";
                        //除去退款
                        $sqlsb = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 1";
                        $sqlpb = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 3";
                    } else {
                        //不除去退款
                        $sqlsz = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 1"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpz = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 3"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        //除去退款
                        $sqlsb = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 1 and isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpb = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 3"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //不除去退款
                        $sqlsz = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and payType = 1";
                        $sqlpz = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != '' and paytype = 3";
                        //除去退款
                        $sqlsb = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 1";
                        $sqlpb = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 3";
                    } else {
                        //不除去退款
                        $sqlsz = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 1"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpz = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 3"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        //除去退款
                        $sqlsb = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 1 and isrefund = 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpb = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 3"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }

                foreach ($connection2->createCommand($sqlsz)->query() as $k => $v) {
                    $total_money_zfb += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpz)->query() as $k => $v) {
                    $total_money_zfb += $v['paidMoney'];
                }

                foreach ($connection2->createCommand($sqlsb)->query() as $k => $v) {
                    $total_removerefund_money_zfb += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpb)->query() as $k => $v) {
                    $total_removerefund_money_zfb += $v['paidMoney'];
                }

                $total_removerefund_money_zfbs += sprintf("%.2f", $total_removerefund_money_zfb);
                array_push($dayArray_removerefund_money_zfb, array('total_removerefund_money_zfb' => sprintf("%.2f", $total_removerefund_money_zfb), 'day' => $i));

                $total_money_zfbs += sprintf("%.2f", $total_money_zfb);
                array_push($dayArray_money_zfb, array('total_money_zfb' => sprintf("%.2f", $total_money_zfb), 'day' => $i));


                //微信收入统计
                $total_removerefund_money_wx = 0; //除去退款微信收入
                $total_money_wx = 0; //不除去退款微信收入
                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //不除去退款
                        $sqlsw = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and payType = 7";
                        $sqlpw = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 4";
                        //除去退款
                        $sqlsr = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 7";
                        $sqlpr = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 4";
                    } else {
                        //不除去退款
                        $sqlsw = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 7"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpw = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 4"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        //除去退款
                        $sqlsr = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 7 and isrefund = 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpr = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 4"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        //不除去退款
                        $sqlsw = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and payType = 7";
                        $sqlpw = "select sum(paidmoney) as paidMoney from tbl_prbusiness where starttime between '$stime' and '$etime' and trade_no != '' and paytype = 4";
                        //除去退款
                        $sqlsr = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and isrefund = 0 and payType = 7";
                        $sqlpr = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId not in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and paytype = 4";
                    } else {
                        //不除去退款
                        $sqlsw = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 7"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpw = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 4"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        //除去退款
                        $sqlsr = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 7 and isrefund = 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlpr = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 4"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsw)->query() as $k => $v) {
                    $total_money_wx += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpw)->query() as $k => $v) {
                    $total_money_wx += $v['paidMoney'];
                }

                foreach ($connection2->createCommand($sqlsr)->query() as $k => $v) {
                    $total_removerefund_money_wx += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpr)->query() as $k => $v) {
                    $total_removerefund_money_wx += $v['paidMoney'];
                }
                $total_removerefund_money_wxs += sprintf("%.2f", $total_removerefund_money_wx);
                array_push($dayArray_removerefund_money_wx, array('total_removerefund_money_wx' => sprintf("%.2f", $total_removerefund_money_wx), 'day' => $i));

                $total_money_wxs += sprintf("%.2f", $total_money_wx);
                array_push($dayArray_money_wx, array('total_money_wx' => sprintf("%.2f", $total_money_wx), 'day' => $i));

                //退款金额和积分 用以计算收入退款率
                $total_refund_integrals = 0;
                $total_refund_integralsp = 0;
                $total_refund_moneys = 0;

                if ($storeid != 0) {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsy = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and payType = 5 and isrefund != 0";
                        $sqlsp = "select sum(paidMoney) as paidMoney from tbl_subbusiness where _businessId in(" . $strt . ") and payTime between '$stime' and '$etime' and ispay = 1 and (payType = 7 or payType = 1) and isrefund != 0";
                        $sqlpp = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and sessionId in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpy = "select sum(paidmoney) as paidMoney from tbl_prbusiness where _storeid in ($storeid) and sessionId in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsy = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5 and a.isrefund != 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsp = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and (a.payType = 7 or a.payType = 1) and a.isrefund != 0"
                                . " and a._businessId in (select businessid from tbl_business where _storeid in ($storeid))"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpp = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId in ($str) and a.starttime between '$stime' and '$etime'"
                                . " and a.trade_no != '' and (a.payType = 3 or a.payType = 4)"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";


                        $sqlpy = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId in ($str) and a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                } else {
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                        $sqlsy = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and payType = 5 and isrefund != 0";
                        $sqlsp = "select sum(paidMoney) as paidMoney from tbl_subbusiness where payTime between '$stime' and '$etime' and ispay = 1 and (payType = 7 or payType = 1) and isrefund != 0";
                        $sqlpp = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != ''and (payType = 3 or payType = 4)";
                        $sqlpy = "select sum(paidmoney) as paidMoney from tbl_prbusiness where tbl_prbusiness.sessionId in (" . $str . ") and starttime between '$stime' and '$etime' and trade_no != '' and payType = 5";
                    } else {
                        $sqlsy = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and a.payType = 5 and a.isrefund != 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $sqlsp = "select sum(paidMoney) as paidMoney from tbl_subbusiness a"
                                . " join tbl_printor c on a.marchineId = c.machineId"
                                . " where a.payTime between '$stime' and '$etime' and a.ispay = 1 and (a.payType = 7 or a.payType = 1) and a.isrefund != 0"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";

                        $sqlpp = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId in ($str) and a.starttime between '$stime' and '$etime'"
                                . " and a.trade_no != '' and (a.payType = 3 or a.payType = 4)"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";


                        $sqlpy = "select sum(paidmoney) as paidMoney from tbl_prbusiness a"
                                . " join db_home.tbl_printor c on a.machineId = c.machineId"
                                . " where a.sessionId in ($str) and a.starttime between '$stime' and '$etime' and a.trade_no != '' and a.payType = 5"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                    }
                }
                foreach ($connection2->createCommand($sqlsy)->query() as $k => $v) {
                    $total_refund_integrals = $v['paidMoney'] * 100;
                }
                foreach ($connection->createCommand($sqlpy)->query() as $k => $v) {
                    $total_refund_integralsq = $v['paidMoney'] * 100;
                }

                $total_refundrate_integral += $total_refund_integrals + $total_refund_integralsq;
                $total_refund_integrals += $total_refund_integralsq;

                if ($total_integral != 0) {
                    array_push($darArray_refundrate_integral, array('total_refundrate_integral' => sprintf("%.2f", $total_refund_integrals / $total_integral * 100), 'day' => $i));
                } else {
                    array_push($darArray_refundrate_integral, array('total_refundrate_integral' => 0, 'day' => $i));
                }

                foreach ($connection2->createCommand($sqlsp)->query() as $k => $v) {
                    $total_refund_moneys += $v['paidMoney'];
                }
                foreach ($connection->createCommand($sqlpp)->query() as $k => $v) {
                    $total_refund_moneys += $v['paidMoney'];
                }
                $total_refundrate_money += sprintf("%.2f", $total_refund_moneys);
                if ($total_money != 0) {
                    array_push($darArray_refundrate_money, array('total_refundrate_money' => sprintf("%.2f", $total_refund_moneys / $total_money * 100), 'day' => $i));
                } else {
                    array_push($darArray_refundrate_money, array('total_refundrate_money' => 0, 'day' => $i));
                }
                if ($total_integral / 100 + $total_money != 0) {
                    $num = ($total_refund_integrals / 100 + $total_refund_moneys) / ($total_integral / 100 + $total_money);
                } else {
                    $num = 0;
                }
                array_push($darArray_refundrate_total, array('total_refundrate' => sprintf("%.2f", $num * 100), 'day' => $i));
            }
        }

        if (isset($store_info)) {
            if (count($store_info) == 1) {
                foreach ($store_info as $k => $l) {
                    $incomearr["schoolname"] = $l->storename; //学校名称
                }
            } else {
                $incomearr["schoolname"] = "";
            }
        } else {
            $incomearr["schoolname"] = "";
        }
        $incomearr["storeid"] = $storeid; //storeid
        $incomearr["year"] = $year; //年份
        $incomearr["totalIncome_removerefund_dayss"] = $totalIncome_removerefund_dayss; //除去退款总收入
        $incomearr["totalIncome_dayss"] = sprintf("%.2f", $totalIncome_dayss); //不除去退款总收入
        $incomearr["dayArray_removerefund"] = $dayArray_removerefund; //除去退款每日收入金额
        $incomearr["dayArray"] = $dayArray; //不除去退款每日收入金额
        $incomearr["dayArray_removerefund_integral"] = $dayArray_removerefund_integral; //除去退款每日收入积分数组
        $incomearr["dayArray_removerefund_money"] = $dayArray_removerefund_money; //除去退款除去积分每日收入金额数组
        $incomearr["dayArray_integral"] = $dayArray_integral; //不除去退款每日收入积分
        $incomearr["dayArray_money"] = $dayArray_money; //不除去退款除去积分每日收入金额
        $incomearr["totalIncome_removerefund_integrals"] = sprintf("%d", $totalIncome_removerefund_integrals); //除去退款收入总积分
        $incomearr["total_removerefund_money"] = sprintf("%.2f", $total_removerefund_money); //除去退款除去积分收入总金额
        $incomearr["total_integrals"] = sprintf("%d", $total_integrals); //不除去退款收入总积分
        $incomearr["total_moneys"] = sprintf("%.2f", $total_moneys); //不除去退款除去积分收入总金额

        $incomearr["dayArray_removerefund_money_zfb"] = $dayArray_removerefund_money_zfb; //除去退款除去积分支付宝每日收入金额
        $incomearr["dayArray_money_zfb"] = $dayArray_money_zfb; //不除去退款除去积分支付宝每日收入金额
        $incomearr["total_removerefund_money_zfbs"] = sprintf("%.2f", $total_removerefund_money_zfbs); //除去退款除去积分支付宝收入总金额
        $incomearr["total_money_zfbs"] = sprintf("%.2f", $total_money_zfbs); //不除去退款除去积分支付宝收入总金额

        $incomearr["dayArray_removerefund_money_wx"] = $dayArray_removerefund_money_wx; //除去退款除去积分微信每日收入金额
        $incomearr["dayArray_money_wx"] = $dayArray_money_wx; //不除去退款除去积分微信每日收入金额
        $incomearr["total_removerefund_money_wxs"] = sprintf("%.2f", $total_removerefund_money_wxs); //除去退款除去积分微信收入总金额
        $incomearr["total_money_wxs"] = sprintf("%.2f", $total_money_wxs); //不除去退款除去积分微信收入总金额

        $incomearr["darArray_refundrate_integral"] = $darArray_refundrate_integral; //每日积分退款率数组
        $incomearr["darArray_refundrate_money"] = $darArray_refundrate_money; //每日金额退款率数组
        $incomearr["darArray_refundrate_total"] = $darArray_refundrate_total; //每日收入总退款率
        if ($total_integrals != 0) {
            $incomearr["total_refundrate_integral"] = sprintf("%.2f", $total_refundrate_integral / $total_integrals * 100); //积分总退款率
        } else {
            $incomearr["total_refundrate_integral"] = 0; //积分总退款率
        }
        if ($total_moneys != 0) {
            $incomearr["total_refundrate_money"] = sprintf("%.2f", $total_refundrate_money / $total_moneys * 100); //金额总退款率
        } else {
            $incomearr["total_refundrate_money"] = 0; //金额总退款率
        }
        if ($total_integrals + $total_moneys != 0) {
            $incomearr["total_refundrate"] = sprintf("%.2f", ($total_refundrate_integral / 100 + $total_refundrate_money) / ($total_integrals / 100 + $total_moneys) * 100); //收入总退款率
        } else {
            $incomearr["total_refundrate"] = 0; //收入总退款率
        }
        return $incomearr;
    }

}
