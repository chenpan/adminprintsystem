<?php

class statisticsrefund {

    public function statistics_refund($storeid = 0, $starttime, $endtime, $types) {
        $refund_model = refund::model();
        if ($storeid != 0) {
            $store_model = store::model();
            $store_info = $store_model->findAllBySql("select * from tbl_store where storeid in (" . $storeid . ")", array('order' => "storeid DESC"));
        }

        $refundarr = array(); //返回的数组

        $dayArray = array(); //每日退款总金额
        $dayArray_integral = array(); //每日退款积分
        $dayArray_money = array(); //每日退款金额

        $refund_totals = 0; //总退款
        $refund_integral_totals = 0; //总退款积分
        $refund_money_totals = 0; //总退款金额

        if ($types == "day") {
            $year = date("Y", strtotime($starttime));
            while (strtotime($starttime) <= strtotime($endtime)) {
                $stime = $starttime . " 00:00:00";
                $etime = $starttime . " 23:59:59";
                $i = date("d", strtotime($stime));
                $starttime = date('Y-m-d', strtotime('+1 day', strtotime($starttime)));

                //积分
                $refund_integral_total = 0;
                //除去积分金额
                $refund_money_total = 0;

                if ($storeid != 0) {
                    //总金额
//                    $refund_infot = $refund_model->findAllBySql("select * from tbl_refund where _storeid in ($storeid) and applyTime between :stime and :etime and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商                
                        //积分
                        $refund_infoi = $refund_model->findAllBySql("select Integral from tbl_refund where _storeid in ($storeid) and applyTime between :stime and :etime and refundType = 5 and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                        foreach ($refund_infoi as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                        //金额
                        $refund_infom = $refund_model->findAllBySql("select money from tbl_refund where _storeid in ($storeid) and applyTime between :stime and :etime and refundType != 5 and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                        foreach ($refund_infom as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                    } else {
                        //终端订单退的钱
                        $refund_info_pm_sql = "select a.money from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_pm = $refund_model->findAllBySql($refund_info_pm_sql);
                        foreach ($refund_info_pm as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                        //线上订单退的钱
                        $refund_info_sm_sql = "select a.money from tbl_refund a join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_sm = $refund_model->findAllBySql($refund_info_sm_sql);
                        foreach ($refund_info_sm as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                        //终端订单退的积分
                        $refund_info_pp_sql = "select a.Integral from tbl_refund a"
                                . " join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_pp = $refund_model->findAllBySql($refund_info_pp_sql);
                        foreach ($refund_info_pp as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                        //线上订单退的积分
                        $refund_info_sp_sql = "select a.Integral from tbl_refund a"
                                . " join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_sp = $refund_model->findAllBySql($refund_info_sp_sql);
                        foreach ($refund_info_sp as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                    }
                } else {
                    //总金额
//                    $refund_infot = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商               
                        //积分
                        $refund_infoi = $refund_model->findAllBySql("select Integral from tbl_refund where applyTime between :stime and :etime and refundType = 5 and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                        foreach ($refund_infoi as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                        //金额
                        $refund_infom = $refund_model->findAllBySql("select money from tbl_refund where applyTime between :stime and :etime and refundType != 5 and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                        foreach ($refund_infoi as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                    } else {
                        //终端订单退的钱
                        $refund_info_pm_sql = "select a.money from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_pm = $refund_model->findAllBySql($refund_info_pm_sql);
                        foreach ($refund_info_pm as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                        //线上订单退的钱
                        $refund_info_sm_sql = "select a.money from tbl_refund a join db_home.tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_sm = $refund_model->findAllBySql($refund_info_sm_sql);
                        foreach ($refund_info_sm as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                        //终端订单退的积分
                        $refund_info_pp_sql = "select a.Integral from tbl_refund a"
                                . " join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_pp = $refund_model->findAllBySql($refund_info_pp_sql);
                        foreach ($refund_info_pp as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                        //线上订单退的积分
                        $refund_info_sp_sql = "select a.Integral from tbl_refund a"
                                . " join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_sp = $refund_model->findAllBySql($refund_info_sp_sql);
                        foreach ($refund_info_sp as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                    }
                }

                //积分加金额
//                $refund_total = 0;
//                foreach ($refund_infot as $k => $l) {
//                    $refund_total += $l->Integral / 100 + $l->money;
//                }


                $refund_integral_totals += $refund_integral_total;
                array_push($dayArray_integral, array('refund_integral_total' => $refund_integral_total, 'day' => $i));


                $refund_money_totals += $refund_money_total;
                array_push($dayArray_money, array('refund_money_total' => $refund_money_total, 'day' => $i));

                //总退款金额（积分加金额）
                $refund_totals += $refund_integral_total / 100 + $refund_money_total;
                array_push($dayArray, array('refund_total' => $refund_integral_total / 100 + $refund_money_total, 'day' => $i));
            }
        }
        if ($types == "month") {
            $year = date("Y", strtotime($starttime));
            for ($i = 1; $i <= 12; $i++) {
                if ($i != 12) {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . ($i + 1) . "-1 00:00:00";
                } else {
                    $stime = $year . "-" . $i . "-1 00:00:00";
                    $etime = $year . "-" . $i . "-31 23:59:59";
                }
                
                //积分
                $refund_integral_total = 0;
                //除去积分金额
                $refund_money_total = 0;
                if ($storeid != 0) {
                    //总金额
//                    $refund_infot = $refund_model->findAllBySql("select * from tbl_refund where _storeid in ($storeid) and applyTime between :stime and :etime and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商                
                        //积分
                        $refund_infoi = $refund_model->findAllBySql("select Integral from tbl_refund where _storeid in ($storeid) and applyTime between :stime and :etime and refundType = 5 and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                        foreach ($refund_infoi as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                        //金额
                        $refund_infom = $refund_model->findAllBySql("select money from tbl_refund where _storeid in ($storeid) and applyTime between :stime and :etime and refundType != 5 and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                        foreach ($refund_infom as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                    } else {
                        //终端订单退的钱
                        $refund_info_pm_sql = "select a.money from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_pm = $refund_model->findAllBySql($refund_info_pm_sql);
                        foreach ($refund_info_pm as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                        //线上订单退的钱
                        $refund_info_sm_sql = "select a.money from tbl_refund a join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_sm = $refund_model->findAllBySql($refund_info_sm_sql);
                        foreach ($refund_info_sm as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                        //终端订单退的积分
                        $refund_info_pp_sql = "select a.Integral from tbl_refund a"
                                . " join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_pp = $refund_model->findAllBySql($refund_info_pp_sql);
                        foreach ($refund_info_pp as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                        //线上订单退的积分
                        $refund_info_sp_sql = "select a.Integral from tbl_refund a"
                                . " join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and a._storeid in ($storeid) and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_sp = $refund_model->findAllBySql($refund_info_sp_sql);
                        foreach ($refund_info_sp as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                    }
                } else {
                    //总金额
//                    $refund_infot = $refund_model->findAllBySql("select * from tbl_refund where applyTime between :stime and :etime and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                    if (Yii::app()->session['is_agent'] == 0) {//不是代理商               
                        //积分
                        $refund_infoi = $refund_model->findAllBySql("select Integral from tbl_refund where applyTime between :stime and :etime and refundType = 5 and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                        foreach ($refund_infoi as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                        //金额
                        $refund_infom = $refund_model->findAllBySql("select money from tbl_refund where applyTime between :stime and :etime and refundType != 5 and tborderId != ''", array(':stime' => $stime, ':etime' => $etime));
                        foreach ($refund_infoi as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                    } else {
                        //终端订单退的钱
                        $refund_info_pm_sql = "select a.money from tbl_refund a join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_pm = $refund_model->findAllBySql($refund_info_pm_sql);
                        foreach ($refund_info_pm as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                        //线上订单退的钱
                        $refund_info_sm_sql = "select a.money from tbl_refund a join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId where a.applyTime between '$stime' and '$etime' and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_sm = $refund_model->findAllBySql($refund_info_sm_sql);
                        foreach ($refund_info_sm as $k => $l) {
                            $refund_integral_total += $l->money;
                        }
                        //终端订单退的积分
                        $refund_info_pp_sql = "select a.Integral from tbl_refund a"
                                . " join db_admin.tbl_prbusiness b on a._sessionId = b.sessionId"
                                . " join tbl_printor c on b.machineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_pp = $refund_model->findAllBySql($refund_info_pp_sql);
                        foreach ($refund_info_pp as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                        //线上订单退的积分
                        $refund_info_sp_sql = "select a.Integral from tbl_refund a"
                                . " join tbl_subbusiness b on a.tborderId = b.trade_no"
                                . " join tbl_printor c on b.marchineId = c.machineId"
                                . " where a.applyTime between '$stime' and '$etime' and a.refundType = 5 and a.Integral != 0 and a.tborderId != ''"
                                . " and c.printorId in (" . Yii::app()->session['printor_id'] . ")";
                        $refund_info_sp = $refund_model->findAllBySql($refund_info_sp_sql);
                        foreach ($refund_info_sp as $k => $l) {
                            $refund_integral_total += $l->Integral;
                        }
                    }
                }

                //积分加金额
//                $refund_total = 0;
//                foreach ($refund_infot as $k => $l) {
//                    $refund_total += $l->Integral / 100 + $l->money;
//                }
                $refund_integral_totals += $refund_integral_total;
                array_push($dayArray_integral, array('refund_integral_total' => $refund_integral_total, 'day' => $i));

                $refund_money_totals += $refund_money_total;
                array_push($dayArray_money, array('refund_money_total' => $refund_money_total, 'day' => $i));

                //总退款金额（积分加金额）
                $refund_totals += $refund_integral_total / 100 + $refund_money_total;
                array_push($dayArray, array('refund_total' => $refund_integral_total / 100 + $refund_money_total, 'day' => $i));
            }
        }
        if (isset($store_info)) {
            if (count($store_info) == 1) {
                foreach ($store_info as $k => $l) {
                    $refundarr["schoolname"] = $l->storename; //学校名称
                }
            } else {
                $refundarr["schoolname"] = "";
            }
        } else {
            $refundarr["schoolname"] = "";
        }
        $refundarr["storeid"] = $storeid; //storeid
        $refundarr["year"] = $year; //年份
        $refundarr["dayArray"] = $dayArray; //每日退款总金额(积分加金额)
        $refundarr["dayArray_integral"] = $dayArray_integral; //每日退款积分
        $refundarr["dayArray_money"] = $dayArray_money; //每日退款金额
        $refundarr["refund_totals"] = $refund_totals; //总退款
        $refundarr["refund_integral_totals"] = $refund_integral_totals; //总退款积分
        $refundarr["refund_money_totals"] = $refund_money_totals; //总退款金额
        return $refundarr;
    }

}
