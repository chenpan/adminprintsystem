<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            .th{
                padding-bottom: 10px;
                padding-top: 10px;
            }
            select {
                border: 1px solid #e6e6e6;
                height: 40px;
                margin-left: -1px;
                width: 164px;
                cursor: pointer;
                border-radius: 3px;
            }
            .modal-body input[type="text"] {
                color: #666;
                height: 30px;
                width: 500px;
                text-indent: 5px;
                border: 1px solid #e6e6e6;
                border-radius: 3px;
            }
            textarea {
                border: 1px solid #e6e6e6;
                height: 110px;
                width: 500px;
            }
            .Msg{
                color: #C62F2F;
            }
            #file-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <!--<script src="./platform/js/share.js" type="text/javascript"></script>-->
        <script type="text/javascript">
            $(document).ready(function() {
                var table = $('#alreadytable').dataTable({
                    "serverSide": true,
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "ajax": './index.php?r=filesAdmin/filesToserverSideAjax',
                    "stateSave": false,
                    "paginate": true,
                    "pagingType": "input",
                    "order": [[9, "desc"]],
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到相关数据",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "",
                        "processing": "正在加载中..."
                    },
                    'columns': [
                        {"data": "attachmentid", "visible": false, "orderble": false, "searchable": false},
                        {"data": "userId", "visible": false, "orderble": false, "searchable": false},
                        {"data": "isShare", "visible": false, "orderble": false, "searchable": false},
                        {"data": "id", "orderble": true, "width": "50px"},
                        {"data": "name", "orderble": true, "searchable": true},
                        {"data": "phone", "orderble": true, "searchable": true},
                        {"data": "attachment", "orderble": true, "searchable": true, "width": "250"},
                        {"data": "filenumber", "orderble": true, "searchable": true},
                        {"data": "school", "orderble": true, "searchable": true},
                        {"data": "uploadtime", "orderble": true, "searchable": true},
                        {"data": "id",
                            "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                                if (oData.isShare == 1 || oData.isShare == 2) {
                                    $(nTd).html("<a <?php echo $flagDown; ?> display='<?php echo $flagDown; ?>'href='javascript:void(0);' " +
                                            "onclick='downloads(" + oData.attachmentid + ")'><span class='label label-success' style='cursor:pointer'>下载</span></a>&nbsp")
                                            .append("<a <?php echo $flagFiles; ?> display='<?php echo $flagFiles; ?>'href='javascript:void(0);\n\
                                        'onclick='deleteFile(" + oData.attachmentid + ")'><span class='label label-success' style='cursor:pointer'>删除</span></a>&nbsp")
                                            .append("<a><span class='label label-warning' style='cursor:pointer;color:white;'>已分享</span></a>");
                                } else {
                                    $(nTd).html("<a <?php echo $flagDown; ?> display='<?php echo $flagDown; ?>'href='javascript:void(0);' " +
                                            "onclick='downloads(" + oData.attachmentid + ")'><span class='label label-success' style='cursor:pointer'>下载</span></a>&nbsp")
                                            .append("<a <?php echo $flagFiles; ?> display='<?php echo $flagFiles; ?>'href='javascript:void(0);\n\
                                        'onclick='deleteFile(" + oData.attachmentid + ")'><span class='label label-success' style='cursor:pointer'>删除</span></a>&nbsp")
                                            .append("<a 'href='javascript:void(0);' " +
                                                    "onclick='shareFile(\"" + oData.attachmentid + "\",\"" + oData.userId + "\",\"" + oData.attachment + "\")'><span class='label label-success' style='cursor:pointer;'>分&nbsp;享</span></a>");
                                }
                            }
                        }
                    ],
                    "columnDefs": [
                        {"targets": [0], "data": "attachmentid", visible: false}
                    ],
                    "drawCallback": function(settings) {
                        var api = this.api();
                        v = api.row(0).column(0).data()[0];
                        if (v == null) {
                            api.row(0).column(10).visible(false);
                        } else {
                            api.row(0).column(10).visible(true);
                        }
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#myFoot").on('click', '#save', function() {
                    var div = $(this).parents("div");
                    var attachmentid = $("#objectId").val();
                    var filename = $("#inputName").val();
                    var userId = $("#userId").val();
                    var classify1 = div.find(".classify1").val();
                    var classify2 = div.find(".classify2").val();
                    var classify3 = div.find(".classify3").val();
                    var classify = (classify3 > 0 ? classify3 : (classify2 > 0 ? classify2 : classify1));
                    var credit = parseInt(div.find("#points").val());
                    var keyword = div.find("#keyword").val();
                    var describe = div.find("#inputNote").val();
                    if (parseInt(classify) == 0) {
                        $('#selectMsg').text("请选择文库分类");
                        return;
                    } else {
                        $('#selectMsg').text("*");
                    }
                    if (parseInt(credit) < 0 || parseInt(credit) > 500) {
                        $('#pointMsg').text("积分设置范围0-500");
                        return;
                    } else {
                        $('#pointMsg').text("*");
                    }
                    if (keyword.length > 50) {
                        $('#keywordMsg').text("标签太长");
                        return;
                    } else {
                        $('#pointMsg').text("");
                    }
                    if (describe.length > 300) {
                        $('#describeMsg').text("文档说明太长");
                        return;
                    } else {
                        $('#describeMsg').text("");
                    }
                    $.post("./index.php?r=filesAdmin/shareFile", {attachmentid: attachmentid, userId: userId, filename: filename, classify: classify, credit: credit, keyword: keyword, describe: describe}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        console.log(data);
                        var code = data.code;
                        var msg = data.msg;
                        if (code == 200) {
                            $('#returnMsg').text("分享成功！");
                        } else if (code == 401) {
                            if (msg == "文件不存在") {
                                $('#returnMsg').text("文档不存在！");
                            } else if (msg == "文件已分享") {
                                $('#returnMsg').text("文件已分享！")
                            } else {
                                $('#returnMsg').text("分享出错！")
                            }
                        }
                    });
                });
            });
            /**
             * 编辑数据带出值
             */
            function shareFile(attachmentid, userId, name) {
                $("input").attr("value", "");
                $("#returnMsg").text("");
                $("#objectId").val(attachmentid);
                $("#userId").val(userId);
                $("#inputName").val(name);
                var select = $('#myBody');
                selectClassify(select, 0, 0, 0);
                select.on('change', '.classify1', function() {
                    selectClassify(select, $(this).val(), 0, 0);
                });
                select.on('change', '.classify2', function() {
                    selectClassify(select, $(this).parents("select").find(".classify1").val(), $(this).val(), 0);
                });
                select.on('change', '.classify3', function() {
                    selectClassify(select, 0, 0, $(this).val());
                });
                $("#myModal").modal("show");
                // 分享文档
                $('#points').blur(function() {
                    if ($('#points').val() == "") {
                        $('#points').val("0");
                    }
                });
            }
            function selectClassify(select, code1, code2, code3) {
                select.find(".classify1 option").remove();
                select.find(".classify2 option").remove();
                select.find(".classify3 option").remove();
                select.find(".classify1").append($("<option/>").val(0).text("请选择"));
                select.find(".classify2").append($("<option/>").val(0).text("请选择"));
                select.find(".classify3").append($("<option/>").val(0).text("请选择"));
                var code;
                code1 = parseInt(code1);
                code2 = parseInt(code2);
                code3 = parseInt(code3);
                var classdata = $('#selectData').text();
                var classify = eval('(' + classdata + ')');
                if (code3 > 0) {
                    code2 = Math.floor(code3 / 10000) * 10000;
                }
                if (code2 > 0) {
                    code1 = Math.floor(code2 / 1000000) * 1000000;
                }
                for (var i = 0; i < classify.length; i++) {
                    code = parseInt(classify[i].code);
                    var option = $("<option/>").val(code).text(classify[i].name);
                    if (code % 1000000 == 0) {
                        if (code == code1) {
                            option.prop("selected", true);
                        }
                        option.appendTo(select.find(".classify1"));
                    } else if (code % 10000 == 0) {
                        if (code1 > 0 && code >= code1 && code < (code1 + 1000000)) {
                            if (code == code2) {
                                option.prop("selected", true);
                            }
                            option.appendTo(select.find(".classify2"));
                        } else if (code1 == 0) {
                            option.appendTo(select.find(".classify2"));
                        }
                    } else {
                        if (code2 > 0) {
                            if (code >= code2 && code < (code2 + 10000)) {
                                if (code == code3) {
                                    option.prop("selected", true);
                                }
                                option.appendTo(select.find(".classify3"));
                            }
                        } else if (code2 == 0) {
                            option.appendTo(select.find(".classify3"));
                        }
                    }
                }

            }
            function downloads(attachmentid)
            {
                window.location.href = "./index.php?r=filesAdmin/download&attachmentid=" + attachmentid;
            }
            function deleteFile(attachmentid)
            {
                if ('<?php echo $flagFiles; ?>' == "") {
                    if (confirm("确定删除这个文件吗？"))
                    {
                        $.post("./index.php?r=filesAdmin/deleteFile", {attachmentid: attachmentid}, function(data) {
                            if (data.replace(/(^\s*)|(\s*$)/g, '') == "success")
                            {
                                alert("删除成功！");
                                window.location.href = './index.php?r=filesAdmin/files';
                            } else
                            {
                                alert("删除失败！");
                            }
                        });
                    }
                } else if ('<?php echo $flagFiles; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
        </script>
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>文件列表</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=filesAdmin/files">文件</a>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap">               
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="alreadytable" width="100%;" style="font-size: 14px !important;">
                                <thead>
                                    <tr class="th">
                                        <th>文件序列</th>
                                        <th>用户Id</th>
                                        <th>分享</th>
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>用户名</th>
                                        <th>电话</th>
                                        <th>文件名</th>
                                        <th>文件页数</th>
                                        <th>所属学校</th>
                                        <th>上传时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                            </table>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">x</span></button>
                                            <h4 class="modal-title" id="myModalLabel">分享文档</h4>
                                        </div>
                                        <div class="modal-body" id="myBody">
                                            <div class="form-group">
                                                <input type="hidden" id="objectId"/>
                                                <input type="hidden" id="userId"/>
                                                <label class="form-label" for="inputName">标题：</label> 
                                                <input type="text" id="inputName" readonly="readonly"/>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="inputJob">分类：</label> 
                                                <select class="classify1">
                                                </select>
                                                <select class="classify2">
                                                </select>
                                                <select class="classify3">
                                                </select>
                                                <span class="Msg" id="selectMsg">*</span>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="points">积分：</label> 
                                                <input type="text" id="points" name="points" placeHolder="请设置下载所需积分（0-500）"style="width: 331px;" />
                                                <span class="Msg"id="pointMsg">*</span>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="keyword">标签：</label> 
                                                <input type="text" id="keyword" name="keyword"  placeHolder="请为所分享文档添加标签（不超过50个字）"/>
                                                <span class="Msg"id="keywordMsg"></span>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="inputNote">说明：</label>
                                                <textarea name="inputNote" id="inputNote" cols="30" rows="4"></textarea>
                                                <span class="Msg"id="describeMsg"></span>
                                            </div>
                                            <div class="form-group">
                                                <span class="Msg" id="returnMsg"></span>
                                            </div>
                                        </div>
                                        <div class="modal-footer"id="myFoot">
                                            <button type="button" class="btn btn-primary" id="save">保存</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                        <span  id="selectData" style="visibility:hidden"><?php echo $classify; ?></span>
                    </div>
                    <!-- / END OF FOOTER -->
                </div>
                <br>
            </div>
        </div>
    </body>
</html>