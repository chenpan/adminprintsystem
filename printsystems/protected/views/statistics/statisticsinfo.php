<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
                letter-spacing: 0;
            }
            input{
                border:1px #f5f5f5 solid;
                padding: 5px 10px;
            }
            input[type=text] {
                color:#37BCE5;
            }
            #statistics-open{
                display: block;
            }
            #statistics-info{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                var date = getyestdayFormatDate();
                $("#onetime").val(date);
                $("#sure").click(function () {
                    $("#email").val().split;
//                    var re = /^1[34578]\d{9}(?:\,1[358]\d{9})*$/;//多个手机正则表达式，用逗号隔开
                    var re = /^((([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6}\;))*(([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})))$/;//多邮箱正则表达式，用逗号或分号隔开
                    if ($("#onetime").val() == "" || $("#onetime").val() == null) {
                        reback();
                        $("#onetime-info").text("请选择日期！");
                        return false;
                    } else if ($("#email").val() == "" || $("#email").val() == null || (!re.test($("#email").val())))
                    {
                        reback();
                        $("#email-info").text("请输入（正确）邮箱！");
                        return false;
                    } else {
                        if (confirm("确认发送？")) {
                            reback();
                            eidtuserform.submit();
                        }
                    }
                });
                $("#queryinfo").click(function () {
                    var date = $("#onetime").val();
                    $("#text").val("");
                    reback();
                    if ($("#onetime").val() == "" || $("#onetime").val() == null) {
                        reback();
                        $("#onetime-info").text("请选择日期！");
                        return false;
                    } else {
                        $("#text-info").text("正在生成信息，请耐心等待！");
                        console.log(date);
                        $.post("./index.php?r=statistics/statisticsinfo_oneday", {date: date}, function (datainfo) {
                            $("#text-info").text("*");
                            var data = eval("(" + datainfo + ")");
                            var code = data[0].code;
                            var msg = data[0].msg;
                            console.log(msg);
                            if (code == 200) {
                                $("#text").val(msg);
                            } else {
                                $("#text-info").text(date + "信息获取失败！");
                            }
                        });
                    }
                });
            });

            function getyestdayFormatDate() {
                var date = new Date();
                var seperator1 = "-";
                var seperator2 = ":";
                var Year = date.getFullYear();
                var month = date.getMonth() + 1;
                var strDate = date.getDate()-1;
                if (month >= 1 && month <= 9) {
                    month = "0" + month;
                }
                if (strDate >= 0 && strDate <= 9) {
                    strDate = "0" + strDate;
                }
                var currentdate = Year + seperator1 + month + seperator1 + strDate;
                return currentdate;
            }
            function reback() {
                $("#email-info,#onetime-info,#add_success").text("*");

                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>统计信息</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=statistics/statisticsinfo">统计信息</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="resetUser" class="form-group" style="text-align: center" >
                                <iframe style="display:none" name="test"></iframe>
                                <form class="form-horizontal" name="eidtuserform" id="eidtuserform" target="test" method="post">
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"><label for="onetime">选择日期：</label></div>   
                                        <div class="col-sm-3"><input style="width: 372px;height: 30px" id="onetime" class="laydate-icon" name="onetime" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})" onchange="querydata()"placeholder="请选择某日时间"></div>   
                                        <div class="col-sm-3 star" id="onetime-info">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"><label for="email">邮箱地址：</label></div>   
                                        <div class="col-sm-3"><textarea rows="2" style="width: 372px;height: 150px;max-width:372px;" id="email" name ="email" placeholder="请输入邮箱,多个请用英文分号隔开" ></textarea></div>   
                                        <div class="col-sm-3 star" id="email-info">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"><label for="text">发送内容: </label></div>
                                        <div class="col-sm-3"><textarea rows="3" style="width: 372px;height: 300px;max-width:372px;" id="text" name="text"><?php echo "$yestday_info_statists;" ?></textarea></div>
                                        <div class="col-sm-3 star" id="text-info">*</div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <div class="col-sm-5"></div>
                                        <div class="col-sm-1"> <button type="button" id="queryinfo" class="btn btn-info btn-block" style="width: 100%;outline:none;">查询</button></div>
                                        <div class="col-sm-2"> <button type="button" id="sure" class="btn btn-info btn-block" style="width: 100%;outline:none;">发送</button></div>
                                        <div class="col-sm-3" style="margin-top:5px;"><lable class="save_lable"></lable> <span id="add_success"></span></div>
                                    </div>
                                </form>
                            </div>
                        </DIV>     

                    </DIV>          
                </DIV>   
                <br>
                <!-- FOOTER -->
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </DIV> 
        </DIV>
    </BODY>
</HTML>
