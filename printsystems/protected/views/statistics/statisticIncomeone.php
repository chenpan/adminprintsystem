<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./css/outWindows/js/jquery.leanModal.min.js"></script>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin-left: 27px;
                margin-right: 27px;
                width: 97%;
            }
            .content-wrap{
                margin-left: 28px;
                margin-right: 28px;
                padding: 0px;
                border-radius: 3px;
            }
            #statistics-open{
                display: block;
            }
            #statistics-order{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#searchs").click(function() {
                    var starttime = $("#starttime").val();
                    var endtime = $("#endtime").val();
                    var types = $("input[name='time']:checked").val();
                    var starttimes = new Date(starttime.replace(/\-/g, "/"));
                    var endtimes = new Date(endtime.replace(/\-/g, "/"));

                    var date1 = starttime.split('-');
// 得到月数
                    var date2 = parseInt(date1[0]) * 12 + parseInt(date1[1]);
// 拆分年月日
                    var date3 = endtime.split('-');
// 得到月数
                    var date4 = parseInt(date3[0]) * 12 + parseInt(date3[1]);
                    var m = Math.abs(date4 - date2);
//判断是否超过一年
                    var date5 = starttimes.getFullYear();
                    var date6 = endtimes.getFullYear();
                    var n = date6 - date5;

                    if (starttime == "") {
                        alert("请选择开始时间！");
                        return false;
                    }
                    if (endtime == "") {
                        alert("请选择结束时间！");
                        return false;
                    }
                    if (starttime > endtime) {
                        alert("请选择正确的时间！");
                        return false;
                    }
                    if (m >= 1 && types == "day")
                    {
                        alert("只提供同一个月的查询！");
                        return false;
                    }
                    if (n >= 1 && types == "month")
                    {
                        alert("只提供同一年的查询！");
                        return false;
                    }

                    $.post("./index.php?r=statistics/search_statisticIncomeone", {storeid: "<?php echo $incomearr['storeid']; ?>", starttime: starttime, endtime: endtime, types: types}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        searchbymonth(data);
                    });
                });
                //查看本月
                $("#searchToday").click(function() {
                    looktoday();
                });
                looktoday();
                function looktoday() {
                    //总收入除去退款与总收入不除去退款
                    $('#zxcharts_total').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $incomearr["schoolname"]; ?> 收入统计'},
                        subtitle: {text: '年份: <?php echo $incomearr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($incomearr["dayArray"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '金额（元）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '不除去退款总收入（共 <?php echo $incomearr["totalIncome_dayss"]; ?> 元）',
                                data: [
<?php
foreach ($incomearr["dayArray"] as $k => $l) {
    echo $l["totalIncome_day"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '除去退款总收入（共 <?php echo $incomearr["totalIncome_removerefund_dayss"]; ?> 元）',
                                data: [
<?php
foreach ($incomearr["dayArray_removerefund"] as $k => $l) {
    echo $l["totalIncome_removerefund_day"] . ",";
}
?>
                                ]
                            }
                        ]
                    });

//总积分收入除去退款与总收入不除去退款
                    $('#zxcharts_integral').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $incomearr["schoolname"]; ?> 总积分收入'},
                        subtitle: {text: '年份: <?php echo $incomearr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($incomearr["dayArray_removerefund"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '积分（分）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '不除去退款积分总收入（共 <?php echo $incomearr["total_integrals"]; ?> 分）',
                                data: [
<?php
foreach ($incomearr["dayArray_integral"] as $k => $l) {
    echo $l["total_integral"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '除去退款积分总收入（共 <?php echo $incomearr["totalIncome_removerefund_integrals"]; ?> 分）',
                                data: [
<?php
foreach ($incomearr["dayArray_removerefund_integral"] as $k => $l) {
    echo $l["total_refund_integral"] . ",";
}
?>
                                ]
                            }
                        ]
                    });

                    //总收入金额除去退款和积分与总收入金额不除去退款和积分
                    $('#zxcharts_money').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $incomearr["schoolname"]; ?> 总金额收入'},
                        subtitle: {text: '年份: <?php echo $incomearr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($incomearr["dayArray_money"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '金额（元）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '不除去退款金额总收入（共 <?php echo $incomearr["total_moneys"]; ?> 元）',
                                data: [
<?php
foreach ($incomearr["dayArray_money"] as $k => $l) {
    echo $l["total_money"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '除去退款和积分总收入（共 <?php echo $incomearr["total_removerefund_money"]; ?> 元）',
                                data: [
<?php
foreach ($incomearr["dayArray_removerefund_money"] as $k => $l) {
    echo $l["total_refund_money"] . ",";
}
?>
                                ]
                            }
                        ]
                    });

                    //支付宝
                    $('#zxcharts_zfb').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $incomearr["schoolname"]; ?> 支付宝收入统计'},
                        subtitle: {text: '年份: <?php echo $incomearr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($incomearr["dayArray_removerefund_money_zfb"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '收入（元）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '不除去退款总收入（共 <?php echo $incomearr["total_money_zfbs"]; ?> 元）',
                                data: [
<?php
foreach ($incomearr["dayArray_money_zfb"] as $k => $l) {
    echo $l["total_money_zfb"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '除去退款总收入（共 <?php echo $incomearr["total_removerefund_money_zfbs"]; ?> 元）',
                                data: [
<?php
foreach ($incomearr["dayArray_removerefund_money_zfb"] as $k => $l) {
    echo $l["total_removerefund_money_zfb"] . ",";
}
?>
                                ]
                            }
                        ]
                    });

                    //微信
                    $('#zxcharts_wx').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $incomearr["schoolname"]; ?> 微信收入统计'},
                        subtitle: {text: '年份: <?php echo $incomearr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($incomearr["dayArray_removerefund_money_wx"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '收入（元）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '不除去退款总收入（共 <?php echo $incomearr["total_money_wxs"]; ?> 元）',
                                data: [
<?php
foreach ($incomearr["dayArray_money_wx"] as $k => $l) {
    echo $l["total_money_wx"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '除去退款总收入（共 <?php echo $incomearr["total_removerefund_money_wxs"]; ?> 元）',
                                data: [
<?php
foreach ($incomearr["dayArray_removerefund_money_wx"] as $k => $l) {
    echo $l["total_removerefund_money_wx"] . ",";
}
?>
                                ]
                            }
                        ]
                    });



                    //收入退款率
                    $('#zxcharts_refundrate').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $incomearr["schoolname"]; ?> 收入退款率统计'},
                        subtitle: {text: '年份: <?php echo $incomearr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($incomearr["darArray_refundrate_integral"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '百分比（%）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '积分退款率（共 <?php echo $incomearr["total_refundrate_integral"]; ?> %）',
                                data: [
<?php
foreach ($incomearr["darArray_refundrate_integral"] as $k => $l) {
    echo $l["total_refundrate_integral"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '金额退款率（共 <?php echo $incomearr["total_refundrate_money"]; ?> %）',
                                data: [
<?php
foreach ($incomearr["darArray_refundrate_money"] as $k => $l) {
    echo $l["total_refundrate_money"] . ",";
}
?>
                                ]
                            }, {
                                name: '总退款率（共 <?php echo $incomearr["total_refundrate"]; ?> %）',
                                data: [
<?php
foreach ($incomearr["darArray_refundrate_total"] as $k => $l) {
    echo $l["total_refundrate"] . ",";
}
?>
                                ],
                                color: '#F17421'
                            }
                        ]
                    });

                }
                //查看今年
                $("#searchMonth").click(function() {//$incomearr_month
                    var starttime = getNowFormatDate();
                    var endtime = getNowFormatDate();
                    var types = "month";
                    //获取当前日期
                    $.post("./index.php?r=statistics/search_statisticIncomeone", {storeid: "<?php echo $incomearr['storeid']; ?>", starttime: starttime, endtime: endtime, types: types}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        searchbymonth(data);
                    });
                });
                //按月显示查询时间段
                function searchbymonth(data) {
                    //不除去退款和除去退款总收入
                    var schoolname = data.schoolname;//学校名称
                    var year = data.year;

                    var dayArray = data.dayArray;
                    var categoriesArr = [];//表的横坐标
                    var totalIncome_day = [];//web端订单数量
                    for (var i = 0; i < dayArray.length; i++)
                    {
                        categoriesArr.push(dayArray[i]["day"]);
                        totalIncome_day.push(Number(dayArray[i]["totalIncome_day"]));
                    }

                    var totalIncome_dayss = data.totalIncome_dayss;
                    var totalIncome_removerefund_dayss = data.totalIncome_removerefund_dayss;
                    var dayArray_removerefund = data.dayArray_removerefund;

                    var totalIncome_removerefund_day = [];//终端订单数量
                    for (var i = 0; i < dayArray_removerefund.length; i++)
                    {
                        totalIncome_removerefund_day.push(Number(dayArray_removerefund[i]["totalIncome_removerefund_day"]));
                    }

                    //不除去退款和除去退款总收入
                    $('#zxcharts_total').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 收入统计'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '金额（元）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [
                            {
                                name: '不除去退款总收入（共 ' + totalIncome_dayss + ' 元）',
                                data: totalIncome_day
                            }, {
                                name: '除去退款总收入（共 ' + totalIncome_removerefund_dayss + ' 元）',
                                data: totalIncome_removerefund_day,
                                color: '#20C8B1'
                            }
                        ]
                    });


                    //总积分收入除去退款与总收入不除去退款
                    var total_integrals = data.total_integrals;
                    var dayArray_integral = data.dayArray_integral;
                    var total_integral = [];
                    for (var i = 0; i < dayArray_integral.length; i++)
                    {
                        total_integral.push(dayArray_integral[i]["total_integral"]);
                    }
                    var totalIncome_removerefund_integrals = data.totalIncome_removerefund_integrals;
                    var dayArray_removerefund_integral = data.dayArray_removerefund_integral;
                    var total_refund_integral = [];
                    for (var i = 0; i < dayArray_removerefund_integral.length; i++)
                    {
                        total_refund_integral.push(Number(dayArray_removerefund_integral[i]["total_refund_integral"]));
                    }

                    //web端订单来源
                    $('#zxcharts_integral').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 总积分收入'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '积分（分）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '不除去退款积分总收入（共 ' + total_integrals + ' 分）',
                                data: total_integral
                            }, {
                                name: '除去退款积分总收入（共 ' + totalIncome_removerefund_integrals + ' 分）',
                                data: total_refund_integral,
                                color: '#20C8B1'
                            }
                        ]
                    });
                    //总收入金额除去退款和积分与总收入金额不除去退款和积分
                    var total_moneys = data.total_moneys;
                    var dayArray_money = data.dayArray_money;
                    var total_money = [];//web订单退款量
                    for (var i = 0; i < dayArray_money.length; i++)
                    {
                        total_money.push(Number(dayArray_money[i]["total_money"]));
                    }
                    var total_removerefund_money = data.total_removerefund_money;
                    var dayArray_removerefund_money = data.dayArray_removerefund_money;
                    var total_refund_money = [];//终端订单退款量
                    for (var i = 0; i < dayArray_removerefund_money.length; i++)
                    {
                        total_refund_money.push(Number(dayArray_removerefund_money[i]["total_refund_money"]));
                    }

                    //web端退款订单，终端退款订单，总退款订单数量
                    $('#zxcharts_money').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + '总金额收入'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '金额（元）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '不除去退款金额总收入（共 ' + total_moneys + ' 元）',
                                data: total_money
                            }, {
                                name: '除去退款和积分总收入（共 ' + total_removerefund_money + ' 元）',
                                data: total_refund_money,
                                color: '#20C8B1'
                            }
                        ]
                    });
                    //支付宝收入
                    var total_removerefund_money_zfbs = data.total_removerefund_money_zfbs;
                    var total_money_zfbs = data.total_money_zfbs;
                    var dayArray_removerefund_money_zfb = data.dayArray_removerefund_money_zfb;
                    var total_removerefund_money_zfb = [];
                    for (var i = 0; i < dayArray_removerefund_money_zfb.length; i++)
                    {
                        total_removerefund_money_zfb.push(Number(dayArray_removerefund_money_zfb[i]["total_removerefund_money_zfb"]));
                    }
                    var dayArray_money_zfb = data.dayArray_money_zfb;
                    var total_money_zfb = [];
                    for (var i = 0; i < dayArray_money_zfb.length; i++)
                    {
                        total_money_zfb.push(Number(dayArray_money_zfb[i]["total_money_zfb"]));
                    }
                    //支付宝收入
                    $('#zxcharts_zfb').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 支付宝收入统计'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '金额（元）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '不除去退款总收入（共 ' + total_money_zfbs + ' 元）',
                                data: total_money_zfb
                            }
                            , {
                                name: '除去退款总收入（共 ' + total_removerefund_money_zfbs + ' 元）',
                                data: total_removerefund_money_zfb,
                                color: '#20C8B1'
                            }
                        ]
                    });
                    
                    //微信收入
                    var total_removerefund_money_wxs = data.total_removerefund_money_wxs;
                    var total_money_wxs = data.total_money_wxs;
                    var dayArray_removerefund_money_wx = data.dayArray_removerefund_money_wx
                    var total_removerefund_money_wx = [];
                    for (var i = 0; i < dayArray_removerefund_money_wx.length; i++)
                    {
                        total_removerefund_money_wx.push(Number(dayArray_removerefund_money_wx[i]["total_removerefund_money_wx"]));
                    }
                    var dayArray_money_wx = data.dayArray_money_wx;
                    var total_money_wx = [];
                    for (var i = 0; i < dayArray_money_wx.length; i++)
                    {
                        total_money_wx.push(Number(dayArray_money_wx[i]["total_money_wx"]));
                    }
                    //微信收入
                    $('#zxcharts_wx').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 微信收入统计'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '金额（元）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '不除去退款总收入（共 ' + total_money_wxs + ' 元）',
                                data: total_money_wx
                            }
                            , {
                                name: '除去退款总收入（共 ' + total_removerefund_money_wxs + ' 元）',
                                data: total_removerefund_money_wx,
                                color: '#20C8B1'
                            }
                        ]
                    });


                    //收入退款率
                    var total_refundrate_integral = data.total_refundrate_integral;
                    var darArray_refundrate_integral = data.darArray_refundrate_integral;
                    var total_refundrate_integrals = data.total_refundrate_integral;
                    var total_refundrate_integral = [];//web端退款率
                    for (var i = 0; i < darArray_refundrate_integral.length; i++)
                    {
                        total_refundrate_integral.push(Number(darArray_refundrate_integral[i]["total_refundrate_integral"]));
                    }
                    var total_refundrate_money = data.total_refundrate_money;
                    var darArray_refundrate_money = data.darArray_refundrate_money;
                    var total_refundrate_moneys = data.total_refundrate_money;
                    var total_refundrate_money = [];//终端退款率
                    for (var i = 0; i < darArray_refundrate_money.length; i++)
                    {
                        total_refundrate_money.push(Number(darArray_refundrate_money[i]["total_refundrate_money"]));
                    }
                    var total_refundrate = data.total_refundrate;

                    var darArray_refundrate_total = data.darArray_refundrate_total;
                    var total_refundrates = data.total_refundrate;
                    var total_refundrate = [];//终端退款率
                    for (var i = 0; i < darArray_refundrate_total.length; i++)
                    {
                        total_refundrate.push(Number(darArray_refundrate_total[i]["total_refundrate"]));
                    }

                    //订单当天退款率
                    $('#zxcharts_refundrate').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 收入退款率统计'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '百分比（%）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '积分退款率（共 ' + total_refundrate_integrals + ' %）',
                                data: total_refundrate_integral
                            }
                            , {
                                name: '金额退款率（共 ' + total_refundrate_moneys + ' %）',
                                data: total_refundrate_money,
                                color: '#20C8B1'
                            }, {
                                name: '总退款率（共 ' + total_refundrates + ' %）',
                                data: total_refundrate,
                                color: '#F17421'
                            }
                        ]
                    });
                }
                //下载订单excel
                $("#download").click(function() {
                    var dstarttime = $("#dstarttime").val();
                    var dendtime = $("#dendtime").val();
                    if (dstarttime == "") {
                        alert("请选择开始时间！");
                        return false;
                    }
                    if (dendtime == "") {
                        alert("请选择结束时间！");
                        return false;
                    }
                    if (dstarttime > dendtime) {
                        alert("请选择正确的时间！");
                        return false;
                    }
                    else {
                        window.location.href = "./index.php?r=statistics/downloadincomeexcel&storeid=<?php echo $storeid; ?>&dstarttime=" + dstarttime + "&dendtime=" + dendtime;
                        $("#downloadexcelsmodal").css({"display": "none"});
                        $("#lean_overlay").css({"display": "none", opacity: 0});
                    }
                });
                 if('<?php echo $flagincome; ?>' == "hidden"){
                    $("#downloadexcel").hide();
                }
                $('#downloadexcel').leanModal({top: 100, overlay: 0.45, closeButton: ".hidemodal"});
                $('#close').click(function() {
                    $("#downloadexcelsmodal").css({"display": "none"});
                    $("#lean_overlay").css({"display": "none", opacity: 0});
                });
            });
            //获取当前时间函数
            function getNowFormatDate() {
                var date = new Date();
                var seperator1 = "-";
                var seperator2 = ":";
                var month = date.getMonth() + 1;
                var strDate = date.getDate();
                if (month >= 1 && month <= 9) {
                    month = "0" + month;
                }
                if (strDate >= 0 && strDate <= 9) {
                    strDate = "0" + strDate;
                }
                var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
                        + " " + date.getHours() + seperator2 + date.getMinutes()
                        + seperator2 + date.getSeconds();
                return currentdate;
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3><?php echo $incomearr["schoolname"]; ?> 收入统计</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=statistics/school&type=income">学校列表</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">收入统计</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap" style="background-color:#FFF">
                    <div class="row" style="margin-top: 10px">          
                        <div class="col-md-12"style="text-align: center;padding-bottom: 23px">   
                            <div class="box box-info">
                                <div style="text-align:center;margin-top: 20px;color: red">***提示:按日统计只能查询同一个月***</div>
                                <div class="box-header with-border">
                                    开始时间：<input id="starttime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    结束时间：<input id="endtime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    <input type="radio" name="time" value="month" checked="checked" />按月统计
                                    <input type="radio" name="time" value="day" />按日统计
                                    <input id="searchs" type="button" class="btn btn-success" value="查询">
                                    <input id="searchToday" type="button" class="btn btn-info" value="查询本月">
                                    <input id="searchMonth" type="button" class="btn btn-info" value="查询今年">
                                    <input id="downloadexcel" href="#downloadexcelsmodal" type="button" class="btn btn-danger" value="下载收入EXCEL">
                                </div>            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts_total" style="width:100%;"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts_integral" style="width:100%;"></div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts_money" style="width:100%;"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts_zfb" style="width:100%;"></div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts_wx" style="width:100%;"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts_refundrate" style="width:100%;"></div>
                            </div>
                        </div>
                    </div>  
                </div>

                <!--  / DEVICE MANAGER -->
                <div id="downloadexcelsmodal">
                    <h1 style="font-size: 18px;font-family: Microsoft YaHei;text-align: center;margin-top: 5px;">下载订单统计EXCEL</h1>  <a class="hidemodal" href="#"></a>
                    <br>
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="dstarttime" class="col-sm-2 control-label">开始时间：</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control laydate-icon" id="dstarttime" placeHolder="请设置开始时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                            </div>
                            <label for="dendtime" class="col-sm-2 control-label">结束时间：</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control laydate-icon" id="dendtime" placeHolder="请设置结束时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                            </div>
                        </div>
                        <div class="center"id="myFoot">
                            <button type="button" class="btn btn-primary" id="download">下载</button>
                            <button type="button" class="btn btn-default" id="close">关闭</button>
                        </div>
                    </form>
                </div>



                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

