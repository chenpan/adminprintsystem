<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./css/outWindows/js/jquery.leanModal.min.js"></script>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin-left: 27px;
                margin-right: 27px;
                width: 97%;
            }
            .content-wrap{
                margin-left: 28px;
                margin-right: 28px;
                padding: 0px;
                border-radius: 3px;
            }
            #statistics-open{
                display: block;
            }
            #statistics-order{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#searchs").click(function () {
                    var starttime = $("#starttime").val();
                    var endtime = $("#endtime").val();
                    var types = $("input[name='time']:checked").val();
                    var starttimes = new Date(starttime.replace(/\-/g, "/"));
                    var endtimes = new Date(endtime.replace(/\-/g, "/"));

                    var date1 = starttime.split('-');
// 得到月数
                    var date2 = parseInt(date1[0]) * 12 + parseInt(date1[1]);
// 拆分年月日
                    var date3 = endtime.split('-');
// 得到月数
                    var date4 = parseInt(date3[0]) * 12 + parseInt(date3[1]);
                    var m = Math.abs(date4 - date2);
//判断是否超过一年
                    var date5 = starttimes.getFullYear();
                    var date6 = endtimes.getFullYear();
                    var n = date6 - date5;

                    if (starttime == "") {
                        alert("请选择开始时间！");
                        return false;
                    }
                    if (endtime == "") {
                        alert("请选择结束时间！");
                        return false;
                    }
                    if (starttime > endtime) {
                        alert("请选择正确的时间！");
                        return false;
                    }
                    if (m >= 1 && types == "day")
                    {
                        alert("只提供同一个月的查询！");
                        return false;
                    }
                    if (n >= 1 && types == "month")
                    {
                        alert("只提供同一年的查询！");
                        return false;
                    }

                    $.post("./index.php?r=statistics/search_statisticOrderone", {storeid: "<?php echo $orderarr['storeid']; ?>", starttime: starttime, endtime: endtime, types: types}, function (datainfo) {
                        var data = eval("(" + datainfo + ")");
                        searchbymonth(data);
                    });
                });
                //查看本月
                $("#searchToday").click(function () {
                    looktoday();
                });
                looktoday();
                function looktoday() {
                    //web订单数量，终端订单数量和总订单数量
                    $('#zxcharts').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $orderarr["schoolname"]; ?> 订单统计(共<?php echo $orderarr["totalCount_dayss"] + $orderarr["totalCount_dayspros"]; ?>单)'},
                        subtitle: {text: '年份: <?php echo $orderarr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($orderarr["dayArray"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '订单量（单）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [
                            {
                                name: 'WEB订单量（共 <?php echo $orderarr["totalCount_dayss"]; ?> 单）',
                                data: [
<?php
foreach ($orderarr["dayArray"] as $k => $l) {
    echo $l["totalCount_days"] . ",";
}
?>
                                ]
                            }, {
                                name: '终端订单量（共 <?php echo $orderarr["totalCount_dayspros"]; ?> 单）',
                                data: [
<?php
foreach ($orderarr["dayArray_pro"] as $k => $l) {
    echo $l["totalCount_days"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '总订单量（共 <?php echo $orderarr["totalCount_dattotals"]; ?> 单）',
                                data: [
<?php
foreach ($orderarr["dayArray_total"] as $k => $l) {
    echo $l["totalCount_days"] . ",";
}
?>
                                ],
                                color: '#F17421'
                            }

                        ]
                    });
                    //web端订单来源
                    $('#zxcharts_web').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $orderarr["schoolname"]; ?> WEB端订单统计(共<?php echo $orderarr["totalCount_dayss"]; ?>单)'},
                        subtitle: {text: '年份: <?php echo $orderarr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($orderarr["dayArray_PC"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '订单量（单）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: 'PC端订单量统计（共 <?php echo $orderarr["totalCount_dayss_PC"]; ?> 单）',
                                data: [
<?php
foreach ($orderarr["dayArray_PC"] as $k => $l) {
    echo $l["totalCount_days_PC"] . ",";
}
?>
                                ]
                            }, {
                                name: 'APP端订单量统计（共 <?php echo $orderarr["totalCount_dayss_APP"]; ?> 单）',
                                data: [
<?php
foreach ($orderarr["dayArray_APP"] as $k => $l) {
    echo $l["totalCount_days_APP"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '微信端订单量统计（共 <?php echo $orderarr["totalCount_dayss_WX"]; ?> 单）',
                                data: [
<?php
foreach ($orderarr["dayArray_WX"] as $k => $l) {
    echo $l["totalCount_days_WX"] . ",";
}
?>
                                ],
                                color: '#F17421'
                            }
                        ]
                    });
                    //web端退款订单，终端退款订单，总退款订单数量
                    $('#zxcharts_refund').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $orderarr["schoolname"]; ?> 退款订单量统计(共 <?php echo $orderarr["totalCount_days_refund"]; ?> 单)'},
                        subtitle: {text: '年份: <?php echo $orderarr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($orderarr["dayArray_refund_web"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '订单量（单）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: 'WEB端退款订单量（共 <?php echo $orderarr["totalCount_days_refund_web"]; ?> 单）',
                                data: [
<?php
foreach ($orderarr["dayArray_refund_web"] as $k => $l) {
    echo $l["totalCount_refund_web"] . ",";
}
?>
                                ]
                            }, {
                                name: '终端退款订单量（共 <?php echo $orderarr["totalCount_days_refund_terminal"]; ?> 单）',
                                data: [
<?php
foreach ($orderarr["dayArray_refund_terminal"] as $k => $l) {
    echo $l["totalCount_refund_terminal"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '退款订单总量（共 <?php echo $orderarr["totalCount_days_refund"]; ?> 单）',
                                data: [
<?php
foreach ($orderarr["dayArray_refund_total"] as $k => $l) {
    echo $l["dayArray_refund_total"] . ",";
}
?>
                                ],
                                color: '#F17421'
                            }
                        ]
                    });
                    //web端当天退款率
                    $('#zxcharts_refund_web').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $orderarr["schoolname"]; ?> 退款订单率统计(共 <?php if ($orderarr["totalCount_dattotals"] != 0) {
    echo sprintf("%.2f", $orderarr["totalCount_days_refund"] / $orderarr["totalCount_dattotals"] * 100);
} else {
    echo sprintf("%2f", 0);
}; ?> %)'},
                        subtitle: {text: '年份: <?php echo $orderarr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($orderarr["dayArray_refundrate_web"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '百分比（%）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: 'WEB端退款百分比（共 <?php if ($orderarr["totalCount_dayss"] != 0) {
    echo sprintf("%.2f", $orderarr["totalCount_days_refund_web"] / $orderarr["totalCount_dayss"] * 100);
} else {
    echo sprintf("%2f", 0);
} ?> %）',
                                data: [
<?php
foreach ($orderarr["dayArray_refundrate_web"] as $k => $l) {
    echo $l["dayArray_refundrate_web"] . ",";
}
?>
                                ]
                            }, {
                                name: '终端退款百分比（共 <?php if ($orderarr["totalCount_dayspros"] != 0) {
    echo sprintf("%.2f", $orderarr["totalCount_days_refund_terminal"] / $orderarr["totalCount_dayspros"] * 100);
} else {
    echo sprintf("%2f", 0);
} ?> %）',
                                data: [
<?php
foreach ($orderarr["dayArray_refundrate_terminal"] as $k => $l) {
    echo $l["dayArray_refundrate_terminal"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '退款总百分比（共 <?php if ($orderarr["totalCount_dattotals"] != 0) {
    echo sprintf("%.2f", $orderarr["totalCount_days_refund"] / $orderarr["totalCount_dattotals"] * 100);
} else {
    echo sprintf("%2f", 0);
} ?> %）',
                                data: [
<?php
foreach ($orderarr["dayArray_refundrate_total"] as $k => $l) {
    echo $l["dayArray_refundrate_total"] . ",";
}
?>
                                ],
                                color: '#F17421'
                            }
                        ]
                    });
                }
                //查看今年
                $("#searchMonth").click(function () {//$orderarr_month
                    var starttime = getNowFormatDate();
                    var endtime = getNowFormatDate();
                    var types = "month";
                    //获取当前日期
                    $.post("./index.php?r=statistics/search_statisticOrderone", {storeid: "<?php echo $orderarr['storeid']; ?>", starttime: starttime, endtime: endtime, types: types}, function (datainfo) {
                        var data = eval("(" + datainfo + ")");
                        searchbymonth(data);
                    });
                });
                //按月显示查询时间段
                function searchbymonth(data) {
                    //web订单数量，终端订单数量和总订单数量
                    var schoolname = data.schoolname;//学校名称
                    var totalCount_dayss = data.totalCount_dayss;
                    var totalCount_dayspros = data.totalCount_dayspros;
                    var year = data.year;
                    var dayArray = data.dayArray;
                    var categoriesArr = [];//表的横坐标
                    var webcount = [];//web端订单数量
                    for (var i = 0; i < dayArray.length; i++)
                    {
                        categoriesArr.push(dayArray[i]["day"]);
                        webcount.push(Number(dayArray[i]["totalCount_days"]));
                    }
                    var totalCount_dayspros = data.totalCount_dayspros;
                    var dayArray_pro = data.dayArray_pro;
                    var terminalcount = [];//终端订单数量
                    for (var i = 0; i < dayArray_pro.length; i++)
                    {
                        terminalcount.push(Number(dayArray_pro[i]["totalCount_days"]));
                    }
                    var totalCount_dattotals = data.totalCount_dattotals;//总订单数量
                    var dayArray_total = data.dayArray_total;
                    var totalcount = [];//总订单数量
                    for (var i = 0; i < dayArray_total.length; i++)
                    {
                        totalcount.push(Number(dayArray_total[i]["totalCount_days"]));
                    }
                    //web订单数量，终端订单数量和总订单数量
                    $('#zxcharts').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 订单统计(共' + (totalCount_dayss + totalCount_dayspros) + '单)'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '订单量（单）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [
                            {
                                name: 'WEB订单量（共 ' + totalCount_dayss + ' 单）',
                                data: webcount
                            }, {
                                name: '终端订单量（共 ' + totalCount_dayspros + ' 单）',
                                data: terminalcount,
                                color: '#20C8B1'
                            }, {
                                name: '总订单量（共 ' + totalCount_dattotals + ' 单）',
                                data: totalcount,
                                color: '#F17421'
                            }

                        ]
                    });
                    //web端订单来源
                    var totalCount_dayss_PC = data.totalCount_dayss_PC;
                    var dayArray_PC = data.dayArray_PC;
                    var PCcount = [];//PC端订单数量
                    for (var i = 0; i < dayArray_PC.length; i++)
                    {
                        PCcount.push(Number(dayArray_PC[i]["totalCount_days_PC"]));
                    }
                    var totalCount_dayss_APP = data.totalCount_dayss_APP;
                    var dayArray_APP = data.dayArray_APP;
                    var APPcount = [];//APP端订单量
                    for (var i = 0; i < dayArray_APP.length; i++)
                    {
                        APPcount.push(Number(dayArray_APP[i]["totalCount_days_APP"]));
                    }
                    var totalCount_dayss_WX = data.totalCount_dayss_WX;
                    var dayArray_WX = data.dayArray_WX;
                    var WXcount = [];//WX端订单量
                    for (var i = 0; i < dayArray_WX.length; i++)
                    {
                        WXcount.push(Number(dayArray_WX[i]["totalCount_days_WX"]));
                    }

                    //web端订单来源
                    $('#zxcharts_web').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' WEB端订单统计(共 ' + totalCount_dayss + ' 单)'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '订单量（单）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: 'PC端订单量统计（共 ' + totalCount_dayss_PC + ' 单）',
                                data: PCcount
                            }, {
                                name: 'APP端订单量统计（共 ' + totalCount_dayss_APP + ' 单）',
                                data: APPcount,
                                color: '#20C8B1'
                            }, {
                                name: '微信端订单量统计（共 ' + totalCount_dayss_WX + ' 单）',
                                data: WXcount,
                                color: '#F17421'
                            }
                        ]
                    });
                    //web端退款订单，终端退款订单，总退款订单数量
                    var totalCount_days_refund = data.totalCount_days_refund;
                    var totalCount_days_refund_web = data.totalCount_days_refund_web;
                    var dayArray_refund_web = data.dayArray_refund_web;
                    var refund_web = [];//web订单退款量
                    for (var i = 0; i < dayArray_refund_web.length; i++)
                    {
                        refund_web.push(Number(dayArray_refund_web[i]["totalCount_refund_web"]));
                    }
                    var totalCount_days_refund_terminal = data.totalCount_days_refund_terminal;
                    var dayArray_refund_terminal = data.dayArray_refund_terminal;
                    var terminal_web = [];//终端订单退款量
                    for (var i = 0; i < dayArray_refund_terminal.length; i++)
                    {
                        terminal_web.push(Number(dayArray_refund_terminal[i]["totalCount_refund_terminal"]));
                    }
                    var totalCount_days_refund = data.totalCount_days_refund;
                    var dayArray_refund_total = data.dayArray_refund_total;
                    var terminal_total = [];//总订单退款量
                    for (var i = 0; i < dayArray_refund_total.length; i++)
                    {
                        terminal_total.push(Number(dayArray_refund_total[i]["dayArray_refund_total"]));
                    }

                    //web端退款订单，终端退款订单，总退款订单数量
                    $('#zxcharts_refund').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 退款订单量统计(共 ' + totalCount_days_refund + ' 单)'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '订单量（单）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: 'WEB端退款订单量（共 ' + totalCount_days_refund_web + ' 单）',
                                data: refund_web
                            }, {
                                name: '终端退款订单量（共 ' + totalCount_days_refund_terminal + ' 单）',
                                data: terminal_web,
                                color: '#20C8B1'
                            }, {
                                name: '退款订单总量（共 ' + totalCount_days_refund + ' 单）',
                                data: terminal_total,
                                color: '#F17421'
                            }
                        ]
                    });
                    //订单当天退款率
                    var dayArray_refundrate_web = data.dayArray_refundrate_web;
                    var refundrate_web = [];//web端退款率
                    for (var i = 0; i < dayArray_refundrate_web.length; i++)
                    {
                        refundrate_web.push(Number(dayArray_refundrate_web[i]["dayArray_refundrate_web"]));
                    }
                    var dayArray_refundrate_terminal = data.dayArray_refundrate_terminal;
                    var refundrate_terminal = [];//终端退款率
                    for (var i = 0; i < dayArray_refundrate_terminal.length; i++)
                    {
                        refundrate_terminal.push(Number(dayArray_refundrate_terminal[i]["dayArray_refundrate_terminal"]));
                    }
                    var dayArray_refundrate_total = data.dayArray_refundrate_total;
                    var refundrate_total = [];//终端退款率
                    for (var i = 0; i < dayArray_refundrate_total.length; i++)
                    {
                        refundrate_total.push(Number(dayArray_refundrate_total[i]["dayArray_refundrate_total"]));
                    }
                    if (totalCount_dattotals == 0) {
                        var totalCount_dattotals_a = 0;
                    } else {
                        var totalCount_dattotals_a = (totalCount_days_refund / totalCount_dattotals * 100).toFixed(2);
                    }
                    if (totalCount_dayss == 0) {
                        var totalCount_dayss_a = 0;
                    } else {
                        var totalCount_dayss_a = (totalCount_days_refund_web / totalCount_dayss * 100).toFixed(2);
                    }
                    if (totalCount_dayspros == 0) {
                        var totalCount_dayspros_a = 0;
                    } else {
                        var totalCount_dayspros_a = (totalCount_days_refund_terminal / totalCount_dayspros * 100).toFixed(2);
                    }
                    if (totalCount_dattotals == 0) {
                        var totalCount_dattotals_a = 0;
                    } else {
                        var totalCount_dattotals_a = (totalCount_days_refund / totalCount_dattotals * 100).toFixed(2);
                    }
                    //订单当天退款率
                    $('#zxcharts_refund_web').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 退款订单率统计(共 ' + totalCount_dattotals_a + ' %)'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '百分比（%）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: 'WEB端退款百分比（共 ' + totalCount_dayss_a + ' %）',
                                data: refundrate_web
                            }
                            , {
                                name: '终端退款百分比（共 ' + totalCount_dayspros_a + ' %）',
                                data: refundrate_terminal,
                                color: '#20C8B1'
                            }, {
                                name: '退款总百分比（共 ' + totalCount_dattotals_a + ' %）',
                                data: refundrate_total,
                                color: '#F17421'
                            }
                        ]
                    });
                }
                //下载订单excel
                $("#download").click(function () {
                    var dstarttime = $("#dstarttime").val();
                    var dendtime = $("#dendtime").val();
                    if (dstarttime == "") {
                        alert("请选择开始时间！");
                        return false;
                    }
                    if (dendtime == "") {
                        alert("请选择结束时间！");
                        return false;
                    }
                    if (dstarttime > dendtime) {
                        alert("请选择正确的时间！");
                        return false;
                    } else {
                        window.location.href = "./index.php?r=statistics/downloadorderexcel&storeid=<?php echo $storeid; ?>&dstarttime=" + dstarttime + "&dendtime=" + dendtime;
                        $("#downloadorderexcelsmodal").css({"display": "none"});
                        $("#lean_overlay").css({"display": "none", opacity: 0});
                    }
                });
                $('#downloadorderexcel').leanModal({top: 100, overlay: 0.45, closeButton: ".hidemodal"});
                $('#close').click(function () {
                    $("#downloadorderexcelsmodal").css({"display": "none"});
                    $("#lean_overlay").css({"display": "none", opacity: 0});
                });
                if ('<?php echo $flagorder; ?>' == "hidden") {
                    $("#downloadorderexcel").hide();
                }
            });
            //获取当前时间函数
            function getNowFormatDate() {
                var date = new Date();
                var seperator1 = "-";
                var seperator2 = ":";
                var month = date.getMonth() + 1;
                var strDate = date.getDate();
                if (month >= 1 && month <= 9) {
                    month = "0" + month;
                }
                if (strDate >= 0 && strDate <= 9) {
                    strDate = "0" + strDate;
                }
                var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
                        + " " + date.getHours() + seperator2 + date.getMinutes()
                        + seperator2 + date.getSeconds();
                return currentdate;
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
<?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3><?php echo $orderarr["schoolname"]; ?> 订单统计</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=statistics/school&type=order">学校列表</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">订单统计</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap" style="background-color:#FFF">
                    <div class="row" style="margin-top: 10px">          
                        <div class="col-md-12"style="text-align: center;padding-bottom: 23px">   
                            <div class="box box-info">
                                <div style="text-align:center;margin-top: 20px;color: red">***提示:按日统计只能查询同一个月***</div>
                                <div class="box-header with-border">
                                    开始时间：<input id="starttime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    结束时间：<input id="endtime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    <input type="radio" name="time" value="month" checked="checked" />按月统计
                                    <input type="radio" name="time" value="day" />按日统计
                                    <input id="searchs" type="button" class="btn btn-success" value="查询">
                                    <input id="searchToday" type="button" class="btn btn-info" value="查询本月">
                                    <input id="searchMonth" type="button" class="btn btn-info" value="查询今年">
                                    <input id="downloadorderexcel" href="#downloadorderexcelsmodal" type="button" class="btn btn-danger" value="下载订单EXCEL">
                                </div>            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts" style="width:100%;"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts_web" style="width:100%;"></div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts_refund" style="width:100%;" ></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="zxcharts_refund_web" style="width:100%;"></div>
                            </div>
                        </div>
                    </div>  
                </div>

                <!--  / DEVICE MANAGER -->
                <div id="downloadorderexcelsmodal">
                    <h1 style="font-size: 18px;font-family: Microsoft YaHei;text-align: center;margin-top: 5px;">下载订单统计EXCEL</h1>  <a class="hidemodal" href="#"></a>
                    <br>
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="dstarttime" class="col-sm-2 control-label">开始时间：</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control laydate-icon" id="dstarttime" placeHolder="请设置开始时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                            </div>
                            <label for="dendtime" class="col-sm-2 control-label">结束时间：</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control laydate-icon" id="dendtime" placeHolder="请设置结束时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                            </div>
                        </div>
                        <div class="center"id="myFoot">
                            <button type="button" class="btn btn-primary" id="download">下载</button>
                            <button type="button" class="btn btn-default" id="close">关闭</button>
                        </div>
                    </form>
                </div>



                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

