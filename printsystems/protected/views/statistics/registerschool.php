<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./css/outWindows/js/jquery.leanModal.min.js"></script>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
                  #breadcrumb{
                background-color: #FFF;
                margin-left: 27px;
                margin-right: 27px;
                width: 97%;
            }
           .content-wrap{
                margin-left: 28px;
                margin-right: 28px;
                padding: 0px;
                border-radius: 3px;
            }
            #statistics-open{
                display: block;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#schooltable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#searchs").click(function() {
                    var starttime = $("#starttime").val();
                    var endtime = $("#endtime").val();
                    var types = $("input[name='time']:checked").val();
                    var starttimes = new Date(starttime.replace(/\-/g, "/"));
                    var endtimes = new Date(endtime.replace(/\-/g, "/"));

                    var date1 = starttime.split('-');
// 得到月数
                    var date2 = parseInt(date1[0]) * 12 + parseInt(date1[1]);
// 拆分年月日
                    var date3 = endtime.split('-');
// 得到月数
                    var date4 = parseInt(date3[0]) * 12 + parseInt(date3[1]);
                    var m = Math.abs(date4 - date2);
//判断是否超过一年
                    var date5 = starttimes.getFullYear();
                    var date6 = endtimes.getFullYear();
                    var n = date6 - date5;

                    if (starttime == "") {
                        alert("请选择开始时间！");
                        return false;
                    }
                    if (endtime == "") {
                        alert("请选择结束时间！");
                        return false;
                    }
                    if (starttime > endtime) {
                        alert("请选择正确的时间！");
                        return false;
                    }
                    if (m >= 1 && types == "day")
                    {
                        alert("只提供同一个月的查询！");
                        return false;
                    }
                    if (n >= 1 && types == "month")
                    {
                        alert("只提供同一年的查询！");
                        return false;
                    }

                    $.post("./index.php?r=statistics/search_statisticregisterone", {storeid: "<?php echo $registerarr['storeid']; ?>", starttime: starttime, endtime: endtime, types: types}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        searchbymonth(data);
                    });
                });
                //查看本月
                $("#searchToday").click(function() {
                    looktoday();
                });
                looktoday();
                function looktoday() {
                    //总退款除去退款与总退款不除去退款
                    $('#zxcharts_register').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $registerarr["schoolname"]; ?> 注册人数统计'},
                        subtitle: {text: '年份: <?php echo $registerarr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($registerarr["dayArray"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '注册人数（人）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '注册人数（共 <?php echo $registerarr["register_total"]; ?> 人）',
                                data: [
<?php
foreach ($registerarr["dayArray"] as $k => $l) {
    echo $l["register_day"] . ",";
}
?>
                                ]
                            }
                        ]
                    });
                }
                //查看今年
                $("#searchMonth").click(function() {//$registerarr_month
                    var starttime = getNowFormatDate();
                    var endtime = getNowFormatDate();
                    var types = "month";
                    //获取当前日期
                    $.post("./index.php?r=statistics/search_statisticregisterone", {storeid: "<?php echo $registerarr['storeid']; ?>", starttime: starttime, endtime: endtime, types: types}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        searchbymonth(data);
                    });
                });
                //按月显示查询时间段
                function searchbymonth(data) {
                    var schoolname = data.schoolname;//学校名称
                    var year = data.year;

                    var dayArray = data.dayArray;
                    var categoriesArr = [];//表的横坐标
                    var register_total = [];//注册总人数
                    var register_totals = data.register_total;
                    for (var i = 0; i < dayArray.length; i++)
                    {
                        categoriesArr.push(dayArray[i]["day"]);
                        register_total.push(Number(dayArray[i]["register_day"]));
                    }


                    //注册人数统计
                    $('#zxcharts_register').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 注册人数统计'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '注册人数（人）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [
                            {
                                name: '注册人数（共 ' + register_totals + ' 人）',
                                data: register_total
                            }
                        ]
                    });
                }
                //下载订单excel
                $("#download").click(function() {
                    var dstarttime = $("#dstarttime").val();
                    var dendtime = $("#dendtime").val();
                    if (dstarttime == "") {
                        alert("请选择开始时间！");
                        return false;
                    }
                    if (dendtime == "") {
                        alert("请选择结束时间！");
                        return false;
                    }
                    if (dstarttime > dendtime) {
                        alert("请选择正确的时间！");
                        return false;
                    }
                    else {
                        window.location.href = "./index.php?r=statistics/downloadregisterexcel&storeid=<?php echo $registerarr['storeid']; ?>&dstarttime=" + dstarttime + "&dendtime=" + dendtime;
                        $("#downloadexcelsmodal").css({"display": "none"});
                        $("#lean_overlay").css({"display": "none", opacity: 0});
                    }
                });
                $('#downloadexcel').leanModal({top: 100, overlay: 0.45, closeButton: ".hidemodal"});
                $('#close').click(function() {
                    $("#downloadexcelsmodal").css({"display": "none"});
                    $("#lean_overlay").css({"display": "none", opacity: 0});
                });
                if ('<?php echo $flagregister; ?>' == "hidden") {
                    $("#downloadexcel").hide();
                }
            });
            //获取当前时间函数
            function getNowFormatDate() {
                var date = new Date();
                var seperator1 = "-";
                var seperator2 = ":";
                var month = date.getMonth() + 1;
                var strDate = date.getDate();
                if (month >= 1 && month <= 9) {
                    month = "0" + month;
                }
                if (strDate >= 0 && strDate <= 9) {
                    strDate = "0" + strDate;
                }
                var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
                        + " " + date.getHours() + seperator2 + date.getMinutes()
                        + seperator2 + date.getSeconds();
                return currentdate;
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <h3>注册人数统计
                        </h3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">
                                注册人数统计
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap" style="background-color:#FFF;padding: 15px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="schooltable">
                                <thead>
                                    <tr class="th">
                                        <th  style="padding-left: 10px;">序列</th>
                                        <th>院校名称</th>
                                        <th>加入时间</th>
                                        <th>操作</th></tr></thead>
                                <tbody>
                                    <?php foreach ($store_info as $K => $V) { ?>
                                        <tr>
                                            <td  style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->storename; ?></td>

                                            <td> <div class="sparkbar" data-color="#00a65a" data-height="20">
                                                    <?php echo $V->addtime; ?>
                                                </div>
                                            </td>
                                            <td>
                                                <a  style="text-decoration: none; " href="./index.php?r=statistics/terninalStatistics&storeid=<?php echo $V->storeid; ?>&type=<?php echo $type; ?>">
                                                    <span class="label label-success">注册人数统计</span>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="content-wrap" style="background-color:#FFF">
                    <div class="row" style="margin-top: 10px">          
                        <div class="col-md-12"style="text-align: center;padding-bottom: 23px">   
                            <div class="box box-info">
                                <div style="text-align:center;margin-top: 20px;color: red">***提示:按日统计只能查询同一个月***</div>
                                <div class="box-header with-border">
                                    开始时间：<input id="starttime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    结束时间：<input id="endtime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    <input type="radio" name="time" value="month" checked="checked" />按月统计
                                    <input type="radio" name="time" value="day" />按日统计
                                    <input id="searchs" type="button" class="btn btn-success" value="查询">
                                    <input id="searchToday" type="button" class="btn btn-info" value="查询本月">
                                    <input id="searchMonth" type="button" class="btn btn-info" value="查询今年">
                                    <input id="downloadexcel" href="#downloadexcelsmodal" type="button" class="btn btn-danger" value="下载注册人数EXCEL">
                                </div>            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="chart-wrap">
                                <div id="zxcharts_register" style="width:100%;"></div>
                            </div>
                        </div>
                    </div>  
                </div>


                <!--  / DEVICE MANAGER -->
                <div id="downloadexcelsmodal">
                    <h1 style="font-size: 18px;font-family: Microsoft YaHei;text-align: center;margin-top: 5px;">下载注册人数统计EXCEL</h1>  <a class="hidemodal" href="#"></a>
                    <br>
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="dstarttime" class="col-sm-2 control-label">开始时间：</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control laydate-icon" id="dstarttime" placeHolder="请设置开始时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                            </div>
                            <label for="dendtime" class="col-sm-2 control-label">结束时间：</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control laydate-icon" id="dendtime" placeHolder="请设置结束时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                            </div>
                        </div>
                        <div class="center"id="myFoot">
                            <button type="button" class="btn btn-primary" id="download">下载</button>
                            <button type="button" class="btn btn-default" id="close">关闭</button>
                        </div>
                    </form>
                </div>
                <!-- FOOTER -->

                <div id="footer">
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

