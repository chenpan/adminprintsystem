<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <script src="./css/bootstrap/highcharts.js" type="text/javascript"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin-left: 27px;
                margin-right: 27px;
                width: 97%;
            }
            .content-wrap{
                background-color: #FFF;
                margin-left: 28px;
                margin-right: 28px;
                padding: 0px;
                border-radius: 3px;
                padding-top: 25px;
                padding-bottom: 25px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            #statistics-open{
                display: block;
            }
            #statistics-paper{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $('#zxcharts').highcharts({
                    chart: {type: 'line'},
                    title: {text: '<?php echo $schoolname; ?> 打印纸张数（共<?php echo $daytotalPages; ?>张）'},
                    subtitle: {text: '年份: <?php echo $year; ?> 年  <?php echo date("m"); ?> 月'},
                    xAxis: {categories:
                                [
<?php
foreach ($dayArray as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                ]}, //'一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'
                    yAxis: {title: {text: '纸张数（张）'}},
                    plotOptions: {
                        line: {dataLabels: {enabled: true}, enableMouseTracking: false}},
                    series: [{
                            name: '<?php echo $schoolname; ?>',
                            data: [
<?php
foreach ($dayArray as $k => $l) {
    echo $l["totalPage"] . ",";
}
?>
                            ]//3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8
                        }]
                });
                $("#searchToday").click(function() {
                    $('#zxcharts').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $schoolname; ?> 打印纸张数（共<?php echo $daytotalPages; ?>张）'},
                        subtitle: {text: '年份: <?php echo $year; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($dayArray as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]}, //'一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'
                        yAxis: {title: {text: '纸张数（张）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}, enableMouseTracking: false}},
                        series: [{
                                name: '<?php echo $schoolname; ?>',
                                data: [
<?php
foreach ($dayArray as $k => $l) {
    echo $l["totalPage"] . ",";
}
?>
                                ]//3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8
                            }]
                    });
                });
                $("#searchMonth").click(function() {
                    $('#zxcharts').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $schoolname; ?> 打印纸张数（共<?php echo $monthtotalPages; ?>张）'},
                        subtitle: {text: '年份: <?php echo $year; ?> 年'},
                        xAxis: {categories: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']},
                        yAxis: {title: {text: '纸张数（张）'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}, enableMouseTracking: false}},
                        series: [{
                                name: '<?php echo $schoolname; ?>',
                                data: [
<?php
foreach ($monthArray as $k => $l) {
    echo $l["totalPage"] . ",";
}
?>
                                ]//3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8
                            }]
                    });
                });
                $("#searchs").click(function() {
                    var starttime = $("#starttime").val();
                    var endtime = $("#endtime").val();
                    var time = $("input[name='time']:checked").val();

                    var starttimes = new Date(starttime.replace(/\-/g, "/"));
                    var endtimes = new Date(endtime.replace(/\-/g, "/"));

                    if (starttime == "") {
                        alert("请选择开始时间！");
                        return false;
                    }
                    if (endtime == "") {
                        alert("请选择结束时间！");
                        return false;
                    }
                    if (starttime > endtime) {
                        alert("请选择正确的时间！");
                        return false;
                    }
                    var date3 = endtimes.getTime() - starttimes.getTime();   //时间差的毫秒数
                    //计算相差的年数
                    var years = Math.floor(date3 / (12 * 30 * 24 * 3600 * 1000));
                    var days = Math.floor(date3 / (24 * 3600 * 1000));
                    if (years >= 1 && time == "month")
                    {
                        alert("暂时不提供超过一年的查询！");
                        return false;
                    }
                    if (days >= 31 && time == "day")
                    {
                        alert("暂时不提供超过一个月的查询！");
                        return false;
                    }

                    $.post("./index.php?r=statistics/search_terninalStatisticsone", {machineId: "<?php echo $machineId; ?>", starttime: starttime, endtime: endtime, time: time}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        var categoriesArr = [];
                        var seriesArr = [];
                        for (var i = 0; i < data.length - 1; i++)
                        {
                            categoriesArr.push(data[i]["num"]);
                            seriesArr.push(data[i]["totalPage"]);
                        }
                        $('#zxcharts').highcharts({
                            chart: {type: 'line'},
                            title: {text: '<?php echo $schoolname; ?> 打印纸张数（共' + data[data.length - 1] + '张）'},
//                        subtitle: {text: '年份: <?php echo $year; ?> 年'},
                            xAxis: {categories: categoriesArr
//                                        ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
                            },
                            yAxis: {title: {text: '纸张数（张）'}},
                            plotOptions: {
                                line: {dataLabels: {enabled: true}, enableMouseTracking: false}},
                            series: [{
                                    name: '<?php echo $schoolname; ?>',
                                    data:
                                            seriesArr
                                            //3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8
                                }]
                        });
                    });
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3><?php echo $schoolname; ?> 打印纸张统计</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=statistics/school&type=printpaper">学校列表</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">打印纸张统计</a>
                        </li>
                    </ul>
                </div>

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row" style="margin-top: 10px">          
                                <div class="col-md-12"style="text-align: center;">   
                                    开始时间：<input id="starttime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    结束时间：<input id="endtime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    <input type="radio" name="time" value="month" checked="checked" />按月统计
                                    <input type="radio" name="time" value="day" />按日统计
                                    <input id="searchs" type="button" class="btn btn-success" value="查询">
                                    <input id="searchToday" type="button" class="btn btn-info" value="查询本月">
                                    <input id="searchMonth" type="button" class="btn btn-info" value="查询今年">
                                </div>                                
                            </div>                           
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row" style="margin-top: 10px">          
                                <div class="col-md-12">      
                                    <div id="zxcharts" style=" height: 600px;"></div>
                                </div>
                            </div>                         
                        </div>
                    </div>
                </div>
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
            <!--  / DEVICE MANAGER -->

        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

