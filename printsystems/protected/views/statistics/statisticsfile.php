<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./css/outWindows/js/jquery.leanModal.min.js"></script>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            #statistics-open{
                display: block;
            }
            #statistics-file{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#schooltable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#searchs").click(function() {
                    var starttime = $("#starttime").val();
                    var endtime = $("#endtime").val();
                    var types = $("input[name='time']:checked").val();
                    var starttimes = new Date(starttime.replace(/\-/g, "/"));
                    var endtimes = new Date(endtime.replace(/\-/g, "/"));

                    var date1 = starttime.split('-');
// 得到月数
                    var date2 = parseInt(date1[0]) * 12 + parseInt(date1[1]);
// 拆分年月日
                    var date3 = endtime.split('-');
// 得到月数
                    var date4 = parseInt(date3[0]) * 12 + parseInt(date3[1]);
                    var m = Math.abs(date4 - date2);
//判断是否超过一年
                    var date5 = starttimes.getFullYear();
                    var date6 = endtimes.getFullYear();
                    var n = date6 - date5;

                    if (starttime == "") {
                        alert("请选择开始时间！");
                        return false;
                    }
                    if (endtime == "") {
                        alert("请选择结束时间！");
                        return false;
                    }
                    if (starttime > endtime) {
                        alert("请选择正确的时间！");
                        return false;
                    }
                    if (m >= 1 && types == "day")
                    {
                        alert("只提供同一个月的查询！");
                        return false;
                    }
                    if (n >= 1 && types == "month")
                    {
                        alert("只提供同一年的查询！");
                        return false;
                    }

                    $.post("./index.php?r=statistics/search_statisticsfile", {storeid: "<?php echo $filearr['storeid']; ?>", starttime: starttime, endtime: endtime, types: types}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        searchbymonth(data);
                    });
                });
                //查看本月
                $("#searchToday").click(function() {
                    looktoday();
                });
                looktoday();
                function looktoday() {
                    //总上传量与线上与线下
                    $('#zxcharts_total').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $filearr["schoolname"]; ?> 上传量统计'},
                        subtitle: {text: '年份: <?php echo $filearr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($filearr["dayArray_PC"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '上传量'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: '总上传量（共 <?php echo $filearr["totalCount_dayssAll"]; ?> 篇）',
                                data: [
<?php
foreach ($filearr["dayArray_total"] as $k => $l) {
    echo $l["totalCount_day_All"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '线下（共 <?php echo $filearr["totalCount_dayss_PR"]; ?> 篇）',
                                data: [
<?php
foreach ($filearr["dayArray_PR"] as $k => $l) {
    echo $l["totalCount_days_PR"] . ",";
}
?>
                                ],
                                color: '#F17421'
                            }, {
                                name: '线上（共 <?php echo $filearr["totalCount_dayssWeb"]; ?> 篇）',
                                data: [
<?php
foreach ($filearr["dayArray_Web"] as $k => $l) {
    echo $l["totalCount_day_Web"] . ",";
}
?>
                                ]
                            }
                        ]
                    });

//线上上传量
                    $('#zxcharts_web').highcharts({
                        chart: {type: 'line'},
                        title: {text: '<?php echo $filearr["schoolname"]; ?> 线上上传量'},
                        subtitle: {text: '年份: <?php echo $filearr["year"]; ?> 年  <?php echo date("m"); ?> 月'},
                        xAxis: {categories:
                                    [
<?php
foreach ($filearr["dayArray_PC"] as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                    ]},
                        yAxis: {title: {text: '份'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [{
                                name: 'PC端（共 <?php echo $filearr["totalCount_dayss_PC"]; ?> 篇）',
                                data: [
<?php
foreach ($filearr["dayArray_PC"] as $k => $l) {
    echo $l["totalCount_days_PC"] . ",";
}
?>
                                ],
                                color: '#FFCB00'
                            }, {
                                name: 'APP端（共 <?php echo $filearr["totalCount_dayss_APP"]; ?> 篇）',
                                data: [
<?php
foreach ($filearr["dayArray_APP"] as $k => $l) {
    echo $l["totalCount_days_APP"] . ",";
}
?>
                                ],
                                color: '#20C8B1'
                            }, {
                                name: '微信端（共 <?php echo $filearr["totalCount_dayss_WX"]; ?> 篇）',
                                data: [
<?php
foreach ($filearr["dayArray_WX"] as $k => $l) {
    echo $l["totalCount_days_WX"] . ",";
}
?>
                                ],
                                color: '#66B82C'
                            }
                        ]
                    });
                }
                //查看今年
                $("#searchMonth").click(function() {//$filearr_month
                    var starttime = getNowFormatDate();
                    var endtime = getNowFormatDate();
                    var types = "month";
                    //获取当前日期
                    $.post("./index.php?r=statistics/search_statisticsfile", {storeid: "<?php echo $filearr['storeid']; ?>", starttime: starttime, endtime: endtime, types: types}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        searchbymonth(data);
                    });
                });
                //按月显示查询时间段
                function searchbymonth(data) {
                    //不除去退款和除去退款总收入
                    var schoolname = data.schoolname;//学校名称
                    var year = data.year;

                    var dayArray_PR = data.dayArray_PR;
                    var dayArray_PC = data.dayArray_PC;
                    var dayArray_APP = data.dayArray_APP;
                    var dayArray_WX = data.dayArray_WX;
                    var dayArray_Web = data.dayArray_Web;
                    var dayArray_total = data.dayArray_total;

                    var categoriesArr = [];//表的横坐标
                    var totalfile_day = [];//总
                    var webfile_day = [];//线上
                    var prfile_day = [];//pr 
                    var pcfile_day = [];//pc 
                    var appfile_day = [];//app 
                    var wxfile_day = [];//wx 
                    for (var i = 0; i < dayArray_PR.length; i++)
                    {
                        categoriesArr.push(dayArray_PR[i]["day"]);
                        totalfile_day.push(Number(dayArray_total[i]["totalCount_day_All"]));
                        webfile_day.push(Number(dayArray_Web[i]["totalCount_day_Web"]));
                        prfile_day.push(Number(dayArray_PR[i]["totalCount_days_PR"]));
                        pcfile_day.push(Number(dayArray_PC[i]["totalCount_days_PC"]));
                        appfile_day.push(Number(dayArray_APP[i]["totalCount_days_APP"]));
                        wxfile_day.push(Number(dayArray_WX[i]["totalCount_days_WX"]));
                    }

                    var totalCount_dayssAll = data.totalCount_dayssAll;
                    var totalCount_dayssWeb = data.totalCount_dayssWeb;
                    var totalCount_dayss_PR = data.totalCount_dayss_PR;
                    var totalCount_dayss_PC = data.totalCount_dayss_PC;
                    var totalCount_dayss_WX = data.totalCount_dayss_WX;
                    var totalCount_dayss_APP = data.totalCount_dayss_APP;


                    $('#zxcharts_total').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 上传量统计'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '上传量'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [
                            {
                                name: '总上传量（共 ' + totalCount_dayssAll + ' 篇）',
                                data: totalfile_day
                            }, {
                                name: '线上（共 ' + totalCount_dayssWeb + ' 篇）',
                                data: webfile_day,
                                color: '#20C8B1'
                            }, {
                                name: '线下（共 ' + totalCount_dayss_PR + ' 篇）',
                                data: prfile_day,
                                color: '#F17421'
                            }
                        ]
                    });
                    $('#zxcharts_web').highcharts({
                        chart: {type: 'line'},
                        title: {text: schoolname + ' 线上上传量统计'},
                        subtitle: {text: '年份: ' + year + '年'},
                        xAxis: {categories: categoriesArr},
                        yAxis: {title: {text: '上传量'}},
                        plotOptions: {
                            line: {dataLabels: {enabled: true}}},
                        series: [
                            {
                                name: 'WEB（共 ' + totalCount_dayss_PC + ' 篇）',
                                data: pcfile_day,
                            }, {
                                name: '微信（共 ' + totalCount_dayss_WX + ' 篇）',
                                data: wxfile_day,
                                color: '#20C8B1'
                            }, {
                                name: 'APP（共 ' + totalCount_dayss_APP + ' 篇）',
                                data: appfile_day,
                                color: '#F17421'
                            }
                        ]
                    });
                }
                //下载订单excel
                $("#download").click(function() {
                    var dstarttime = $("#dstarttime").val();
                    var dendtime = $("#dendtime").val();
                    if (dstarttime == "") {
                        alert("请选择开始时间！");
                        return false;
                    }
                    if (dendtime == "") {
                        alert("请选择结束时间！");
                        return false;
                    }
                    if (dstarttime > dendtime) {
                        alert("请选择正确的时间！");
                        return false;
                    } else {
                        window.location.href = "./index.php?r=statistics/downloadfileexcel&storeid=<?php echo $filearr['storeid']; ?>&dstarttime=" + dstarttime + "&dendtime=" + dendtime;
                        $("#downloadexcelsmodal").css({"display": "none"});
                        $("#lean_overlay").css({"display": "none", opacity: 0});
                    }
                });
                $('#downloadexcel').leanModal({top: 100, overlay: 0.45, closeButton: ".hidemodal"});
                $('#close').click(function() {
                    $("#downloadexcelsmodal").css({"display": "none"});
                    $("#lean_overlay").css({"display": "none", opacity: 0});
                });
                if ('<?php echo $flagfile; ?>' == "hidden") {
                    $("#downloadexcelsmodal").hide();
                }
            });
            //获取当前时间函数
            function getNowFormatDate() {
                var date = new Date();
                var seperator1 = "-";
                var seperator2 = ":";
                var month = date.getMonth() + 1;
                var strDate = date.getDate();
                if (month >= 1 && month <= 9) {
                    month = "0" + month;
                }
                if (strDate >= 0 && strDate <= 9) {
                    strDate = "0" + strDate;
                }
                var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
                        + " " + date.getHours() + seperator2 + date.getMinutes()
                        + seperator2 + date.getSeconds();
                return currentdate;
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3><?php echo $filearr["schoolname"]; ?> 上传量统计</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>统计
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=statistics/school&type=file">学校列表</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">上传文件统计</a>
                        </li>
                    </ul>
                </div>

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row" style="margin-top: 10px">          
                                <div class="col-md-12"style="text-align: center;">   
                                    开始时间：<input id="starttime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    结束时间：<input id="endtime" class="laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    <input type="radio" name="time" value="month" checked="checked" />按月统计
                                    <input type="radio" name="time" value="day" />按日统计
                                    <input id="searchs" type="button" class="btn btn-success" value="查询">
                                    <input id="searchToday" type="button" class="btn btn-info" value="查看本月">
                                    <input id="searchMonth" type="button" class="btn btn-info" value="查看今年">
                                    <input id="downloadexcel" href="#downloadexcelsmodal" type="button" class="btn btn-danger" value="下载上传量EXCEL">
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="chart-wrap">
                                        <div id="zxcharts_total" style="width:100%;"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="chart-wrap">
                                        <div id="zxcharts_web" style="width:100%;"></div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>

                <!--  / DEVICE MANAGER -->
                <div id="downloadexcelsmodal">
                    <h1 style="font-size: 18px;font-family: Microsoft YaHei;text-align: center;margin-top: 5px;">下载上传量统计EXCEL</h1>  <a class="hidemodal" href="#"></a>
                    <br>
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="dstarttime" class="col-sm-2 control-label">开始时间：</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control laydate-icon" id="dstarttime" placeHolder="请设置开始时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                            </div>
                            <label for="dendtime" class="col-sm-2 control-label">结束时间：</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control laydate-icon" id="dendtime" placeHolder="请设置结束时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                            </div>
                        </div>
                        <div class="center"id="myFoot">
                            <button type="button" class="btn btn-primary" id="download">下载</button>
                            <button type="button" class="btn btn-default" id="close">关闭</button>
                        </div>
                    </form>
                </div>
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

