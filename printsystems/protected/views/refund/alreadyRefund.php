<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #refund-open{
                display: block;
            }
            #refund-list{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            a{
                color:#9EA7B3;
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $('#alreadytable').dataTable({
                    "serverSide": true,
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "ajax": './index.php?r=refund/alreadyRefundAjax',
                    "stateSave": false,
                    "paginate": true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    },
                    "order": [ 9, 'desc' ],
                    'columns': [
                        {"data": "sessionId", "orderble": true},
                        {"data": "name", "orderble": true, "searchable": true},
                        {"data": "orderId", "orderble": true, "searchable": true},
                        {"data": "businessId","visible": false, "orderble": true, "searchable": true},
                        {"data": "money", "orderble": true, "searchable": true, },
                        {"data": "Integral", "orderble": true, "searchable": true},
                        {"data": "payTypeName", "orderble": true},
                        {"data": "tborderId", "orderble": true, "searchable": true},
                        {"data": "applyTime", "orderble": true, "searchable": true, },
                        {"data": "refundTime", "orderble": true, "searchable": true},
                        {"data": "refundTypeName", "orderble": true},
                        {"data": "storeName", "orderble": true, "searchable": true}
                    ],
                    "columnDefs": [
                        {"targets": 3,
                            "data": "businessId",
                            "visible": false
                        }
                    ]
                });
                var table = $('#alreadytable').DataTable();
                $('#alreadytable tbody').on('click', 'td', function () {
                    var colIdx = table.cell(this).index().column;
                    if (colIdx == 2) {
                        var rowIdx = table.cell(this).index().row;
                        var value = table.column(colIdx).row(rowIdx).data()["businessId"];
                        if (value != "") {
                            window.location.href="./index.php?r=refund/orderDetail&businessid=" + value;
                        }
                    }
                    if(colIdx == 0){
                        var rowIdx = table.cell(this).index().row;
                        var orderId = table.column(colIdx).row(rowIdx).data()["sessionId"];
                        var tborderId = table.column(colIdx).row(rowIdx).data()["tborderId"];
                        if(orderId != ""){
                             window.location.href="./index.php?r=refund/prbdetails&_sessionId="+ orderId +"&trade_no="+ tborderId;
                        }
                    }
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>已退款列表</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=refund/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>退款
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">已退款列表</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table  id="alreadytable">
                                <thead>
                                    <tr class="th">
                                        <th>sessionId</th>
                                        <th>用户</th>
                                        <th>订单号</th>
                                        <th></th>
                                        <th>支付金额</th>
                                        <th>支付消耗积分</th>
                                        <th>支付方式</th>
                                        <th>订单交易号</th>
                                        <!--<th>用户支付账号</th>-->
                                        <th>申请退款时间</th>
                                        <th>实际退款时间</th>
                                        <th>退款方式</th>
                                        <th>所属学校</th>
                                        <!--<th>状态</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

