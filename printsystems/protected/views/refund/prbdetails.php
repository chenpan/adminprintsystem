<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            #refund-open{
                display: block;
            }

        </style>
<!--        <script type="text/javascript">
            $(function() {
                $('#datailetable').dataTable({
                    stateSave: true,
                    "ordering": false,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "搜索："
                    }
                });
            });
        </script>-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>

        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">

                <!-- CONTENT -->


                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>退款订单详情</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>退款
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">退款订单详情</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="datailetable">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>sessionId</th>
                                        <th>一卡通卡号</th>
                                        <th>支付宝支付交易号</th>
                                        <th>文件名</th>
                                        <th>文件页数</th>
                                        <th>打印份数</th>
                                        <th>打印设置</th>
                                        <th>实际打印页数</th>
                                        <th>支付方式</th>
                                        <th>用户支付金额</th>
                                        <th>交易开始时间</th>
                                        <th>交易结束时间</th>
                                        <th>打印终端</th>
                                        <th>退出阶段</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($prbusiness_info as $K => $V) {
                                        ?>
                                        <tr style="border:none;">
                                            <td style="padding-left: 13px;">
                                                <?php echo $K + 1; ?>
                                            </td>
                                            <td><?php echo $V->sessionId; ?></td>
                                            <td><?php echo $V->udiskId; ?></td>
                                            <td><?php echo $V->trade_no; ?></td>
                                            <td><?php echo'<div class="filename" title=' . $V->filename . '>' . $V->filename . '</div>' ?></td>
                                            <td><?php echo $V->totalpages; ?></td>
                                            <td><?php echo $V->copies; ?></td>
                                            <td><?php echo $V->printset; ?></td>
                                            <td><?php echo $V->printedpages; ?></td>
                                            <td><?php
                                                if ($V->paytype == 1)
                                                    echo "现金";
                                                else if ($V->paytype == 2)
                                                    echo "刷卡";
                                                else if ($V->paytype == 3)
                                                    echo "支付宝";
                                                ?>
                                            </td>
                                            <td><?php echo $V->paidmoney; ?></td>
                                            <td><?php echo $V->starttime; ?></td>
                                            <td><?php echo $V->endtime; ?></td>
                                            <td><?php
                                                if ($V->sessionId == "") {
                                                    $marchine_info = subbusiness::model()->find("subbusinessId = '$V->subbusinessId'");
                                                    $marchine_name = printor::model()->find("machineId = '$marchine_info->marchineId'");
                                                    if (count($marchine_name) != 0) {
                                                        echo $marchine_name->printorName;
                                                    } else {
                                                        echo "无";
                                                    }
                                                } else {
                                                    $marchine_info = prbusiness::model()->find("prbusinessid = '$V->prbusinessid'");
                                                    $marchine_name = printor::model()->find("machineId = '$marchine_info->machineId'");
                                                    echo $marchine_name->printorName;
                                                }
                                                ?></td>
                                            <td><?php echo $V->exit_phrase; ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

