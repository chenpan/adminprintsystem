<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            .checkbox {
                width: 20px;
                height: 20px;
                border: 1px #E5E5E5 solid;
                border-radius: 100%;
                position: relative;
            }
            .checkboxFour label {
                width: 14px;
                height: 14px;
                border-radius: 100px;

                -webkit-transition: all .5s ease;
                -moz-transition: all .5s ease;
                -o-transition: all .5s ease;
                -ms-transition: all .5s ease;
                transition: all .5s ease;
                cursor: pointer;
                position: static;
                top: 2px;
                left: 2px;
                z-index: 1;
                background-color:#FFFFFF;
                border: 1px #65C91B solid;
            }
            /*                        input[type=checkbox] {
                                        visibility: hidden;
                                    }*/
            #refund-open{
                display: block;
            }
            #refund-list{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            a{
                color:#9EA7B3;
            }
            .btn-dete{
                background-color: #55C6F1!important;
                color: white!important;
                border: 1px solid #55C6F1!important;
                width: 86px;
            }
            .btn-dete:hover{
                background-color:#3CBDEF!important;
                color: white!important;
                border: 1px solid #3CBDEF!important;
            }
            .btn-info{
                background-color:#65C3DF!important;
                color: white!important;
                width: 86px;
                border: 1px solid #65C3DF!important;
            }
            .btn-info:hover{
                background-color:#4CB8D8!important;
                color: white!important;
                border: 1px solid #4CB8D8!important;
            }
        </style>
        <script type="text/javascript">
            $(function () {
                window.checkedval = 0;
                $('#refundtable').dataTable({
                    "serverSide": true,
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "ajax": './index.php?r=refund/refundAjax',
                    "stateSave": false,
                    "pagingType": "input",
                    "order": [10, 'desc'],
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    },
                    columnDefs: [{
                            orderable: false, //禁用排序
                            targets: [0, 0]   //指定的列
                        }],
                    'columns': [
                        {"data": "refundId", "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<input type='checkbox' name='checkList' value='" + sData + "'>");
                            }},
                        {"data": "sessionId", "orderble": true},
                        {"data": "name", "orderble": true, "searchable": true},
                        {"data": "orderId", "orderble": true, "searchable": true},
                        {"data": "businessId", "visible": false, "orderble": true, "searchable": true},
                        {"data": "money", "orderble": true, "searchable": true},
                        {"data": "Integral", "orderble": true, "searchable": true},
                        {"data": "payTypeName", "orderble": true},
                        {"data": "tborderId", "orderble": true, "searchable": true},
                        {"data": "sellerAccounter", "orderble": true, "searchable": true},
//                        {"data": "marchine_name", "orderble": true, "searchable": true, },
                        {"data": "applyTime", "orderble": true, "searchable": true},
                        {"data": "refundTypeName", "orderble": true},
                        {"data": "storeName", "orderble": true, "searchable": true},
                        {"data": "statueName", "orderble": true, "searchable": true, }
                    ],
                    "drawCallback": function () {
                        if (window.checkedval == 1) {
                            $("input[name='checkList']").prop("checked", $("#checkAll").prop("checked"));
//                            $('#refundtable tbody tr').addClass('selected');
                        }
                    }
                });
                var table = $('#refundtable').DataTable();
                $('#refundtable tbody').on('click', 'td', function () {
                    var colIdx = table.cell(this).index().column;
                    if (colIdx == 3) {
                        var rowIdx = table.cell(this).index().row;
                        var value = table.column(colIdx).row(rowIdx).data()["businessId"];
                        if (value != "") {
                            window.location.href = "./index.php?r=refund/orderDetail&businessid=" + value;
                        }
                    }
                    if (colIdx == 1) {
                        var rowIdx = table.cell(this).index().row;
                        var orderId = table.column(colIdx).row(rowIdx).data()["sessionId"];
                        var tborderId = table.column(colIdx).row(rowIdx).data()["tborderId"];
                        if (orderId != "") {
                            window.location.href = "./index.php?r=refund/prbdetails&_sessionId=" + orderId + "&trade_no=" + tborderId;
                        }
                    }
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                //支付宝退款按钮
                $("#refund").click(function () {
                    var num = 0;
                    var refundIdd = "";
                    var $renfundtype = "";
                    for (var i = 0; i < $("#refundtable").find(":checkbox").length; i++)
                    {
                        $renfundtype = $("#refundtable").find(":checkbox").eq(i).parents('tr').find('td').eq(10).text();
                        if ($("#refundtable").find(":checkbox").eq(i).is(':checked') && $renfundtype == "支付宝")//&& $renfundtype == "支付宝"
                        {
                            refundIdd += $("#refundtable").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    refundIdd = refundIdd.substring(0, refundIdd.length - 1);
                    if (num == 0)
                    {
                        alert("请您选择支付宝退款的选项！");
                        return false;
                    }
                    window.open("./index.php?r=refund/refund_zfb&refundIdd=" + refundIdd, "_blank");
                });
                //微信退款按钮
                $("#wxrefund").click(function () {
                    var num = 0;
                    var refundIdd = "";
                    var $renfundtype = "";
                    for (var i = 0; i < $("#refundtable").find(":checkbox").length; i++)
                    {
                        $renfundtype = $("#refundtable").find(":checkbox").eq(i).parents('tr').find('td').eq(10).text();
                        if ($("#refundtable").find(":checkbox").eq(i).is(':checked') && $renfundtype == "微信")
                        {
                            refundIdd += $("#refundtable").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    refundIdd = refundIdd.substring(0, refundIdd.length - 1);
                    if (num == 0)
                    {
                        alert("请您选择微信退款的选项！");
                        return false;
                    }
                    $.post("./index.php?r=refund/refund_wx", {refundIdd: refundIdd}, function (datainfo) {
                        var data = eval("(" + datainfo + ")");
                        console.log(data);
                        alert(data.info);
                        window.location.href = "./index.php?r=refund/refund";
                    });
//                    window.open("./index.php?r=refund/refund_wx&refundIdd=" + refundIdd, "_blank");
                });
                //积分退款按钮
                $("#pointsrefund").click(function () {
                    if (confirm("确定退款？")) {
                        var num = 0;
                        var refundIdd = "";
                        var $renfundtype = "";
                        for (var i = 0; i < $("#refundtable").find(":checkbox").length; i++)
                        {
                            $renfundtype = $("#refundtable").find(":checkbox").eq(i).parents('tr').find('td').eq(10).text();
                            if ($("#refundtable").find(":checkbox").eq(i).is(':checked') && $renfundtype == "积分")
                            {
                                refundIdd += $("#refundtable").find(":checkbox").eq(i).attr('value') + ",";
                                num++;
                            }
                        }
                        refundIdd = refundIdd.substring(0, refundIdd.length - 1);
                        if (num == 0)
                        {
                            alert("请您选择积分退款的选项！");
                            return false;
                        }
                        $.post("./index.php?r=refund/refund_points", {refundIdd: refundIdd}, function (datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                alert("积分退款成功！");
                                window.location.href = "./index.php?r=refund/refund";
                            } else if (data.data == "false") {
                                alert("积分退款失败！");
                                window.location.href = "./index.php?r=refund/refund";
                            } else if (data.data == "printed"){
                                alert("订单已打印成功！");
                            }
                        });
                    }
                });

                if ('<?php echo $deleteRefund; ?>' == "false") {
                    $("#deleterefund").hide();
                }
                if ('<?php echo $zfbrefund; ?>' == "false") {
                    $("#refund").hide();
                }
                if ('<?php echo $wxrefund; ?>' == "false") {
                    $("#wxrefund").hide();
                }
                if ('<?php echo $jfrefund; ?>' == "false") {
                    $("#pointsrefund").hide();
                }
                //删除退款记录
                $("#deleterefund").click(function () {
                    var num = 0;
                    var refundIdd = "";
                    for (var i = 0; i < $("#refundtable").find(":checkbox").length; i++)
                    {
                        if ($("#refundtable").find(":checkbox").eq(i).is(':checked'))
                        {
                            refundIdd += $("#refundtable").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    refundIdd = refundIdd.substring(0, refundIdd.length - 1);
                    if ('<?php echo $deleteRefund; ?>' == "true") {
                        if (num == 0)
                        {
                            alert("请您选择需要退款的选项！");
                            return false;
                        }
                        if (confirm("确定删除？"))
                        {
                            $.post("./index.php?r=refund/deleterefund", {refundIdd: refundIdd}, function (datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "success")
                                {
                                    alert("删除成功！");
                                    window.location.href = "./index.php?r=refund/refund";
                                } else
                                    alert("删除失败！");
                            });
                        }
                    } else if ('<?php echo $deleteRefund; ?>' == "false") {
                        window.location.href = './index.php?r=nonPrivilege/index';
                    }
                });
                $("#checkAll").on("click", function () {
                    if ($("#refundtable").find(":checkbox").eq(0).is(':checked'))
                    {
                        for (var i = 0; i < $("#refundtable").find(":checkbox").length; i++)
                        {
                            $("#refundtable").find(":checkbox").eq(i).removeAttr("checked");
                            $("#refundtable").find("label").eq(i).css("background-color", "#FFFFFF");
                        }
                    } else
                    {
                        for (var i = 0; i < $("#refundtable").find(":checkbox").length; i++)
                        {
                            $("#refundtable").find(":checkbox").eq(i).prop("checked", true);
                            $("#refundtable").find("label").eq(i).css("background-color", "#65C91B");
                        }
                    }
                });
                $("#refundtable").on('click', 'div', function () {
                    alert($(this).find(":checkbox").prop());
                    if ($(this).find(":checkbox").prop("checked")) {//取消选择
                        $(this).find(":checkbox").removeAttr("checked");
                        $(this).find("label").css("background-color", "#FFFFFF");
                    } else {//选择
                        $(this).find(":checkbox").prop("checked", true);
                        $(this).find("label").css("background-color", "#65C91B");
                    }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>

        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">

                <!-- CONTENT -->


                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>退款列表</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>退款
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=refund/refund">退款列表</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="refundtable">
                                <thead>
                                    <tr class="th">
                                        <th style="width:15px">
                                            <a href="#" id='checkAll'>全选</th>                                           
                                        </th>
                                        <th>sessionId</th>
                                        <th>用户</th>
                                        <th class="orderid">订单号</th>
                                        <th></th>
                                        <th>退款金额</th>
                                        <th>退款积分</th>
                                        <th>支付方式</th>
                                        <th>订单交易号</th>
                                        <th>用户支付账号</th>
                                        <!--<th>打印终端</th>-->
                                        <th>申请退款时间</th>
                                        <th>退款方式</th>
                                        <th>所属学校</th>
                                        <th>状态</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-12 btnlist">
                            <button type="button" class="btn btn-info" id="refund">支付宝退款</button>
                            <button type="button" class="btn btn-info" id="wxrefund">微信退款</button>
                            <button type="button" class="btn btn-info" id="pointsrefund">积分退款</button>
                            <button type="button" class="btn btn-success btn-dete" id="deleterefund">删除退款</button>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

