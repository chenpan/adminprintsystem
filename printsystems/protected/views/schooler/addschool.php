<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            label{text-align: right;}
            #school-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#save").click(function() {
                    if ($("#schoolname").val() == "" || $("#schoolname").val() == null)
                    {
                        reback();
                        $("#schoolname-info").text("请输入学校名称！");
                        return false;
                    }

                    else if ($("#schoolusername").val() == "" || $("#schoolusername").val() == null)
                    {
                        reback();
                        $("#schoolusername-info").text("请输入高校管理员用户名！");
                        return false;
                    }
                    else if ($("#province").val() == 0) {
                        reback();
                        $("#province-info").text("请输入高校管理员用户名！");
                        return false;
                    }
                    else if ($("#schoolpassword").val() == "" || $("#schoolpassword").val() == null)
                    {
                        reback();
                        $("#schoolpassword-info").text("请输入高校管理员密码！");
                        return false;
                    }
                    else if ($("#phone").val() == "" || $("#schoolusername").val() == null)
                    {
                        reback();
                        $("#phone-info").text("请输入高校管理员手机号码！");
                        return false;
                    }
                    else if ($("#QQ").val() == "" || $("#schoolpassword").val() == null)
                    {
                        reback();
                        $("#QQ-info").text("请输入高校管理员QQ！");
                        return false;
                    }
                    else if ($("#schoollogo").val() == "" || $("#schoollogo").val() == null)
                    {
                        reback();
                        $("#schoollogo-info").text("请上传学校logo！");
                        return false;
                    }
                    else
                    {
                        $("#schoolname-info,#schoolusername-info,#schoolpassword-info,#phone-info,#QQ-info,#schoollogo-info").text("");
                        if (confirm("确认保存？"))
                            addschoolform.submit();
                    }
                });
            });
            function reback() {
                $("#schoolname-info,#schoolusername-info,#schoolpassword-info,#phone-info,#QQ-info,#schoollogo-info,#province").text("*");

                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>新增学校</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=schooler/School">学校</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=schooler/addschool">新增学校</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form role="form" id="addschoolform" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="schoolname">学校名称：</label></div>
                                            <div class="col-sm-3"><input type="text" placeholder="" id="schoolname" name ="schoolname" class="form-control" placeholder="请输入学校名称"></div>
                                            <div class="col-sm-3 star" id="schoolname-info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="schooldescript">学校简介：</label></div>
                                            <div class="col-sm-3"><textarea rows="6" style="width:100%" id="schooldescript" name="schooldescript"></textarea></div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="schoolusername">所属省份：</label></div>
                                            <div class="col-sm-3">
                                                <select id="province" name ="province" style=" width:100%; height:30px;line-height:30px; ">
                                                    <option value ="0">请选择省份</option>
                                                    <option value ="北京市">
                                                        北京市 </option><option value ="天津市">
                                                        天津市 </option><option value ="上海市">
                                                        上海市 </option><option value ="重庆市">
                                                        重庆市 </option><option value ="河北省">
                                                        河北省 </option><option value ="山西省">
                                                        山西省 </option><option value ="辽宁省">
                                                        辽宁省 </option><option value ="吉林省">
                                                        吉林省 </option><option value ="黑龙江省">
                                                        黑龙江省</option><option value ="江苏省">
                                                        江苏省 </option><option value ="浙江省">
                                                        浙江省 </option><option value ="安徽省">
                                                        安徽省 </option><option value ="福建省">
                                                        福建省 </option><option value ="江西省">
                                                        江西省 </option><option value ="山东省">
                                                        山东省 </option><option value ="河南省">
                                                        河南省 </option><option value ="湖北省">
                                                        湖北省 </option><option value ="湖南省">
                                                        湖南省 </option><option value ="广东省">
                                                        广东省 </option><option value ="海南省">
                                                        海南省 </option><option value ="四川省">
                                                        四川省 </option><option value ="贵州省">
                                                        贵州省 </option><option value ="云南省">
                                                        云南省 </option><option value ="陕西省">
                                                        陕西省 </option><option value ="甘肃省">
                                                        甘肃省 </option><option value ="青海省">
                                                        青海省 </option><option value ="台湾省">
                                                        台湾省 </option><option value ="广西壮族自治区">
                                                        广西壮族自治区</option><option value ="内蒙古自治区">
                                                        内蒙古自治区</option><option value ="西藏自治区">
                                                        西藏自治区</option><option value ="宁夏回族自治区">
                                                        宁夏回族自治区 </option><option value ="新疆维吾尔自治区">
                                                        新疆维吾尔自治区</option><option value ="香港特别行政区">
                                                        香港特别行政区</option><option value ="澳门特别行政区">
                                                        澳门特别行政区</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-3 star" id="province-info">*</div>
                                        </div>                                            
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="schoolusername">学校管理员帐号：</label></div>
                                            <div class="col-sm-3"> <input type="email" placeholder="" id="schoolusername" name="schoolusername" class="form-control" placeholder="请输入学校学校管理员帐号"></div>
                                            <div class="col-sm-3 star" id="schoolusername-info">*</div>
                                        </div>                                        
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="schoolpassword">学校管理员密码：</label></div>
                                            <div class="col-sm-3"><input type="password" placeholder="" id="schoolpassword" name="schoolpassword" class="form-control" placeholder="请输入学校管理员密码"></div>
                                            <div class="col-sm-3 star" id="schoolpassword-info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="phone">手机号码：</label></div>
                                            <div class="col-sm-3"><input type="email" placeholder="" id="phone" name="phone" class="form-control" placeholder="请输入手机号码"></div>
                                            <div class="col-sm-3 star" id="phone-info">*</div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="QQ">QQ：</label></div> 
                                            <div class="col-sm-3"><input type="email" placeholder="" id="QQ" name="QQ" class="form-control" placeholder="请输入QQ"></div> 
                                            <div class="col-sm-3 star" id="QQ-info">*</div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="logo">学校logo：</label></div> 
                                            <div class="col-sm-3"><input type="file" id="schoollogo" name="schoollogo" style="margin-top:7px;outline:none;"></div> 
                                            <div class="col-sm-3 star" id="schoollogo-info">*</div>
                                        </div>
                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-3"> <button class="btn btn-info" type="button" id="save" style="width: 100%;outline:none;">保存</button></div>
                                            <div class="col-sm-3" style="margin-top:5px;"> <span id="info" style="color:red;"></span></div>
                                        </div>
                                    </form>  
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

