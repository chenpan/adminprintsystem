<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #school-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }

        </style>
        <script type="text/javascript">
            $(function() {
                $('#schooltable').dataTable({
                    stateSave: true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#addschool").click(function() {
                    window.location.href = "./index.php?r=schooler/addSchool";
                });
                $("#save").click(function() {
//                    if ($("#schoolname").val() == "" || $("#schoolname").val() == null)
//                    {
//                        reback();
//                        $("#schoolname-info").text("请输入学校名称！");
//                        return false;
//                    } else if ($("#schoolusername").val() == "" || $("#schoolusername").val() == null)
//                    {
//                        reback();
//                        $("#schoolusername-info").text("请输入高校管理员用户名！");
//                        return false;
//                    } else if ($("#schoolpassword").val() == "" || $("#schoolpassword").val() == null)
//                    {
//                        reback();
//                        $("#schoolpassword-info").text("请输入高校管理员密码！");
//                        return false;
//                    } else if ($("#phone").val() == "" || $("#schoolpassword").val() == null)
//                    {
//                        reback();
//                        $("#phone-info").text("请输入高校管理员手机号！");
//                        return false;
//                    } else if ($("#QQ").val() == "" || $("#schoolpassword").val() == null)
//                    {
//                        reback();
//                        $("#QQ-info").text("请输入高校管理员QQ！");
//                        return false;
//                    }
//                    else if ($("#schoollogo").val() == "" || $("#schoolpassword").val() == null)
//                    {
//                          reback();
//                        $("#schoollogo-info").text("请输入高校Logo！");
//                        return false;
//                    }
//                    else
//                    {
                    $("#schoolname-info,#schoolusername-info,#schoolpassword-info,#phone-info,#QQ-info,#schoollogo-info").text("");
                    if (confirm("确认保存？"))
                        addschoolform.submit();
//                    }
                });
            });
            function reback() {
                $("#schoolname-info,#schoolusername-info,#schoolpassword-info,#phone-info,#QQ-info,#schoollogo-info").text("*");

                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>编辑学校</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=schooler/School">学校</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">编辑学校</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <iframe style="display:none" name="test"></iframe>
                                    <form role="form" id="addschoolform" method="post" target="test" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group" style="display:none;">
                                            <div class="col-sm-5 control-label"> <label for="schoolname">学校ID:</label></div>
                                            <div class="col-sm-3"><input type="text" placeholder="" id="schoolId" name ="schoolId" class="form-control" value="<?php echo $store_info->storeid; ?>"></div>
                                            <div class="col-sm-3 "></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="schoolname">学校名称:</label></div>
                                            <div class="col-sm-3"> <input type="text" placeholder="" id="schoolname" name ="schoolname" class="form-control" value="<?php echo $store_info->storename; ?>" placeholder="请输入学校名称"></div>
                                            <div class="col-sm-3 star" id="schoolname-info"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="schooldescript">学校简介:</label></div>
                                            <div class="col-sm-3"><textarea rows="9" style="width:100%" id="schooldescript" name="schooldescript"><?php echo $store_info->storedescript; ?></textarea></div>
                                            <div class="col-sm-3 "></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="schoolusername">学校管理员帐号:</label></div>
                                            <div class="col-sm-3"> <input type="email" placeholder="" id="schoolusername" name="schoolusername" class="form-control" value="<?php echo $administrator_info->username; ?>" placeholder="请输入学校管理员帐号"></div>
                                            <div class="col-sm-3 star" id="schoolusername-info"></div>
                                        </div>                                        
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="schoolpassword">学校管理员密码:</label></div>
                                            <div class="col-sm-3"><input type="password" placeholder="" id="schoolpassword" name="schoolpassword" class="form-control" placeholder="请输入姓名学校管理员密码"></div>
                                            <div class="col-sm-3 star" id="schoolpassword-info"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="phone">手机号码:</label></div>
                                            <div class="col-sm-3"><input type="email" placeholder="" id="phone" name="phone" class="form-control" value="<?php echo $administrator_info->phone; ?>" placeholder="请输入手机号码"></div>
                                            <div class="col-sm-3 star" id="phone-info"></div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="QQ">QQ:</label></div>
                                            <div class="col-sm-3"><input type="email" placeholder="" id="QQ" name="QQ" class="form-control" value="<?php echo $administrator_info->QQ; ?>" placeholder="请输入QQ"></div>
                                            <div class="col-sm-3 star" id="QQ-info"></div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="logo">学校logo:</label></div>
                                            <div class="col-sm-3"><input type="file" id="schoollogo" name="schoollogo" style="margin-top:7px;outline:none;"></div>
                                            <div class="col-sm-3 star" id="schoollogo-info"></div>
                                        </div>

                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-3"> <button class="btn btn-info" type="button" id="save" style="width: 100%;outline:none;">保存</button></div>
                                            <div class="col-sm-3" style="margin-top:5px;"><lable class="save_lable"></lable> <span id="info" style="color:red;"></span></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

