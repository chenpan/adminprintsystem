<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./css/outWindows/js/jquery.leanModal.min.js"></script>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;

            }
            #school-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#schooltable').dataTable({
                    stateSave: true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    },
                    "footerCallback": function(row, data, start, end, display) {
                        var api = this.api(), data;
                        // Remove the formatting to get integer data for summation
                        var intVal = function(i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };
                        // Total over all pages
                        total = api
                                .column(5)
                                .data()
                                .reduce(function(a, b) {
                                    return intVal(a) + intVal(b);
                                });
                        // Total over this page
                        pageTotal = api
                                .column(5, {page: 'current'})
                                .data()
                                .reduce(function(a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);
                        // Update footer
                        $(api.column(8).footer()).html(
                                '合计 : ' + pageTotal + ' 台 ( 共 ' + total + ' 台)'
                                );
                    }
                });
                $('.active').leanModal({top: 100, overlay: 0.45, closeButton: ".hidemodal"});
                $('#close').click(function() {
                    $("#activemodal").css({"display": "none"});
                    $("#lean_overlay").css({"display": "none", opacity: 0});
                });

                $("#save").click(function() {
                    var activedescript = $("#activedescript").val();
                    if (activedescript.replace(" ", "").length == 0) {
                        alert("请输入活动描述！");
                        return false;
                    }
                    if (activedescript.length > 100) {
                        alert("请限制字数在100字以内！");
                        return false;
                    }
                    if ('<?php echo $active; ?>' == "") {
                        if (confirm("确认 " + types + " 活动功能?")) {
                            $.post("./index.php?r=schooler/editactive", {storeid: storeids, type: types, activedescript: activedescript}, function(datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "success") {
                                    alert(types + "成功！");
                                    window.location.href = "./index.php?r=schooler/school";
                                } else
                                    alert(data.data);
                            });
                        }
                    } else if ('<?php echo $active; ?>' == "hidden") {
                        window.location.href = './index.php?r=nonPrivilege/index';
                    }
                });

                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });

                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#addschool").click(function() {
                    window.location.href = "./index.php?r=schooler/addSchool";
                });
                if ('<?php echo $addSchool; ?>' == "hidden") {
                    $("#addschool").parent().parent().parent().hide();
                }
            });
            function deleteschool(storeid, storename) {
                if ('<?php echo $deleteSchool; ?>' == "") {
                    if (confirm("确认删除 " + storename + " 院校?")) {
                        $.post("./index.php?r=schooler/deleteschool", {storeid: storeid}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=schooler/school";
                            } else
                                alert("删除失败！");
                        });
                    }
                } else if ('<?php echo $deleteSchool; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }

            function editschool(storeid) {
                if ('<?php echo $editSchool; ?>' == "") {
                    window.location.href = './index.php?r=schooler/editInfo&storeid=' + storeid;
                } else if ('<?php echo $editSchool; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }

            //全局变量 
            var storeids = 0;
            var types = "";
            function assignment(storeid, type) {
                storeids = storeid;
                types = type;
            }

            //关闭活动
            function closeactive(storeid, storename, type) {
                if ('<?php echo $active; ?>' == "") {
                    if (confirm("确认 " + type + " " + storename + " 活动功能?")) {
                        $.post("./index.php?r=schooler/editactive", {storeid: storeid, type: type, activedescript: ""}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                alert("关闭成功！");
                                $("#activemodal").css({"display": "none"});
                                $("#lean_overlay").css({"display": "none", opacity: 0});
                                window.location.href = "./index.php?r=schooler/school";
                            } else {
                                $("#activemodal").css({"display": "none"});
                                $("#lean_overlay").css({"display": "none", opacity: 0});
                                alert(data.data);
                            }
                        });
                    }
                } else if ('<?php echo $active; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
            //查看详情
            function activedetail(activedetaildescript) {
                $("#myFoot").css("display", "none");
                $("#activedescript").val(activedetaildescript);
            }

        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>学校列表</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=schooler/School">学校</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addschool" value="新增学校">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="schooltable">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>学校名称</th>
                                        <th>学校简介</th>
                                        <th>学校校徽</th>
                                        <th>所属省份</th>
                                        <th>终端数量</th>
                                        <th>手机号码</th>
                                        <th>QQ</th>
                                        <th>加入时间</th>
                                        <th>活动</th>
                                        <!--<th>活动描述</th>-->
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="10" style="text-align:right"></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($store_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->storename; ?></td>
                                            <td><?php echo $V->storedescript; ?></td>
                                            <td>
                                                <a class="fancy" href="./images/storelogo/<?php echo $V->storelogo; ?>"  data-fancybox-group="gallery">
                                                    <img style = "width:20px;height:20px" class="thumbnails" src="./images/storelogo/<?php echo $V->storelogo; ?>"/>
                                                </a>
                                            </td>
                                            <td><?php echo $V->province; ?></td>
                                            <td><?php
                                                if (Yii::app()->session['is_agent'] == 0) {//不是代理商
                                                    echo count(printor::model()->findAll("_storeId = $V->storeid"));
                                                } else {
                                                    $machine_count = "select count(*) as total_machine_count from tbl_printor a join db_admin.tbl_agent_printor b on a.printorId = b.printor_id where b.administrator_id = " . Yii::app()->session['administrator_id'] . " and a._storeId = $V->storeid";
                                                    foreach (Yii::app()->db2->createCommand($machine_count)->query() as $k => $v) {
                                                        $total_machine_count = $v['total_machine_count'];
                                                    }
                                                    echo $total_machine_count;
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                $administrator_info = administrator::model()->find("_storeid = $V->storeid");
                                                if (count($administrator_info) != 0) {
                                                    echo $administrator_info->phone;
                                                } else {
                                                    echo "无";
                                                }
                                                ?></td>
                                            <td><?php
                                                if (count($administrator_info) != 0) {
                                                    echo $administrator_info->QQ;
                                                } else {
                                                    echo "无";
                                                }
                                                ?></td>
                                            <td><?php echo $V->addtime; ?></td>
                                            <td><?php if ($V->active == 0 && $active == "") { ?>
                                                    <a href="#activemodal" class="active"  <?php echo $active; ?> onclick="assignment(<?php echo $V->storeid; ?>, '开启')"><span class="label label-default" style="background-color:#58B759;color:#FFF">开启</span></a>
                                                    <?php
                                                } else if ($V->active == 0 && $active == "hidden") {
                                                    echo "未开启";
                                                    ?>
                                                <?php } else if ($V->active == 1 && $active == "") { ?>
                                                    <a href="#" class='closeactive' <?php echo $active; ?> onclick="closeactive(<?php echo $V->storeid; ?>, '<?php echo $V->storename; ?>', '关闭')"><span class="label label-danger">关闭</span></a>
                                                    <?php
                                                } else if ($V->active == 1 && $active == "hidden") {
                                                    echo "已开启";
                                                    ?>
                                                <?php } ?>
                                            </td>
    <!--                                            <td>
                                                <a href="#activemodal" class='active' onclick="activedetail('<?php echo $V->activedescript; ?>')"><span class="label label-danger">查看</span></a>
                                            </td>-->
                                            <td>
                                                <a class="edit_btn" <?php echo $editSchool; ?> href="#" onclick="editschool(<?php echo $V->storeid; ?>)"><span class="label label-success">编辑</span></a>
                                                <!--<a href="#" class="delete_btn" onclick="deleteschool(<?php echo $V->storeid; ?>, '<?php echo $V->storename; ?>')" ><span class="label label-success">删除</span></a>-->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <div id="activemodal">
                    <h1 style="font-size: 18px;font-family: Microsoft YaHei;text-align: center;margin-top: 5px;">活动描述</h1>  <a class="hidemodal" href="#"></a>
                    <br>
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label for="activedescript" class="col-sm-2 control-label">活动描述：</label>
                            <div class="col-sm-10">
                                <textarea rows="6" style="width:100%" id="activedescript" name="activedescript" placeholder="限制100字以内"></textarea>
                            </div>
                        </div>

                        <div class="center"id="myFoot">
                            <button type="button" class="btn btn-primary" id="save">保存</button>
                            <button type="button" class="btn btn-default" id="close">关闭</button>
                        </div>
                    </form>
                </div>

                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

