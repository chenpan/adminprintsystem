<!DOCTYPE HTML>
<HTML>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            table tr:hover{
                background-color: transparent;
            }
            table tr{
                border-bottom: none;
            }
            #order-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            function downloads(attachmentid)
            {
                window.location.href = "./index.php?r=filesAdmin/download&attachmentid=" + attachmentid;
            }
            function deleteFile(attachmentid)
            {
                if (confirm("确定删除这个文件吗？"))
                {
                    $.post("./index.php?r=filesAdmin/deleteFile", {attachmentid: attachmentid}, function(data) {
                        if (data.replace(/(^\s*)|(\s*$)/g, '') == "success")
                        {
                            alert("删除成功！");
                            window.location.href = './index.php?r=filesAdmin/files';
                        }
                        else
                        {
                            alert("删除失败！");
                        }
                    });
                }
            }
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
            });
        </script>
    </head>  
    <BODY>
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>订单详情</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=business/business">订单</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">订单详情</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap" style="width:25%;margin-top: 30px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-wrapper">   
                                <div class="order-detail-main"><?php echo $businessid_info->orderId ?><span style="font-size:11px;margin-left: 10px;color:#d5d5d5">订单号</span></div><br>
                                <div class="order-detail-main"><?php
                                    if (isset($businessid_info->verificationCode))
                                        echo "<sapn>" . $businessid_info->verificationCode . "</span>";
                                    else
                                        echo "无";
                                    ?><span style="font-size:11px;margin-left: 10px;color:#d5d5d5">验证码</span></div> 
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-wrap" style="width:47%;margin-top:20px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-wrapper">   
                                <div class="menuFont order-detail-title">时间追踪</div><hr>
                                <table id="orderDetail" style="margin-left: 1px;background-color:#FFF;"> 
                                    <tr style="font-weight: bold;">
                                        <td>
                                            下单
                                        </td>
                                        <td>
                                            付款
                                        </td>
                                        <td>
                                            打印
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php echo $businessid_info->placeOrdertime; ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo subbusiness::model()->find("_businessId = $businessid_info->businessid")->payTime;
//                                            echo $businessid_info->placeOrdertime;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo subbusiness::model()->find("_businessId = $businessid_info->businessid")->printTime;
//                                            echo $businessid_info->placeOrdertime
                                            ?>
                                        </td> 
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-wrap" style="width: 75%;margin-top:20px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-wrapper"> 
                                <div class="menuFont order-detail-title">付款信息</div><hr>
                                <table id="orderDetail" style="margin-left: 1px;background-color:#FFF;">
                                    <tr style="font-weight: bold;">
                                        <td>
                                            用户名
                                        </td> 
                                        <td>
                                            手机号码
                                        </td> 
                                        <td>
                                            商家信息
                                        </td>
<!--                                        <td>
                                            支付状态
                                        </td> 
                                        <td>
                                            付款方式
                                        </td> 
                                        <td>
                                            付款账号
                                        </td> -->
                                        <td>
                                            支付订单号
                                        </td> 
                                        <td>
                                            商品总值
                                        </td> 
                                        <td>
                                            应支付金额
                                        </td> 
                                        <td>
                                            消费积分
                                        </td> 

                                    </tr>
                                    <tr>
                                        <td style="color:#16a085">
                                            <?php
                                            echo user::model()->find("userid = $businessid_info->_userid")->username;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo user::model()->find("userid = $businessid_info->_userid")->phone;
                                            ?>
                                        </td>
                                        <td>
                                            重庆颇闰科技有限公司
                                        </td>
<!--                                        <td>
                                           
                                        </td>
                                        <td>
                                            支付宝
                                        </td> 
                                        <td>
                                            13330290051
                                        </td> -->
                                        <td>
                                            <?php
                                            echo $businessid_info->trade_no;
                                            ?>
                                        </td>
                                        <td>
                                            ￥ <?php
                                            echo $businessid_info->paidMoney;
                                            ?> 元
                                        </td>
                                        <td>
                                            ￥ <?php
                                            echo $businessid_info->paidMoney;
                                            ?> 元
                                        </td>    
                                        <td>
                                            <?php
                                            echo $businessid_info->consumptionIntegral . " 点";
                                            ?>
                                        </td>    
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-wrap" style="width: 99%;margin-top: 20px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-wrapper">  
                                <div class="menuFont order-detail-title">打印信息</div><hr>
                                <table id="orderDetail" style="margin-left: 1px;background-color:#FFF;">
                                    <tr style="font-weight: bold;">
                                        <td style="width: 500px;">文件名称</td>
                                        <td>打印份数</td>
                                        <td>打印页码</td>
                                        <td>支付金额</td>
                                        <td>支付状态</td>
                                        <td>支付方式</td>
                                        <td>支付类型</td>
                                        <td>打印状态</td>
                                        <td>打印终端</td>
                                    </tr>
                                    <?php
                                    foreach ($attachmentArray as $K => $V) {
                                        $attachmentname = $V["attachmentname"];
                                        echo '<tr><td><div class="filename" style="width:360px;" title=' . $attachmentname . '>' . $V["attachmentname"] . '</div></td>'
                                        . '<td><span style="color:#16a085;">' . $V["printNumber"] . '</span> 份</td>'
                                        . '<td>第' . $V["printSet"] . '页</td>'
                                        . '<td><span style="color:#16a085;">￥' . $V["paidMoney"] . '</span> 元</td>';
                                        if ($V["isrefund"] == 0) {
                                            if ($V["isPay"] == 0)
                                                echo '<td><span class ="ispay">未支付</span></td>';
                                            else if ($V["isPay"] == 1)
                                                echo '<td class ="ispay">已支付</td>';
                                            else
                                                echo '<td class ="ispay">未知的错误</td>';
                                        }
                                        else if ($V["isrefund"] == 1)
                                            echo '<td class ="ispay">已退款</td>';
                                        else if ($V["isrefund"] == 2)
                                            echo '<td class ="ispay">退款中</td>';

                                        if ($V["payType"] == "0")
                                            echo '<td>线下支付</td>';
                                        if ($V["payType"] == "1")
                                            echo '<td>支付宝</td>';
                                        else if ($V["payType"] == "2")
                                            echo '<td>一卡通</td>';
                                        else if ($V["payType"] == "3")
                                            echo '<td>投币</td>';
                                        else if ($V["payType"] == "4")
                                            echo '<td>终端扫码</td>';
                                        else if ($V["payType"] == "5")
                                            echo '<td>积分</td>';
                                        else if ($V["payType"] == "6")
                                            echo '<td>积分+支付宝</td>';
                                        else if ($V["payType"] == "7")
                                            echo '<td>微信</td>';
                                        else if ($V["payType"] == null)
                                            echo '<td>无</td>';

                                        if ($V["double_sided"] == 0)
                                            echo '<td>单面打印</td>';
                                        else if ($V["double_sided"] == "1")
                                            echo '<td>双面打印</td>';

                                        if ($V["status"] == "0")
                                            echo '<td>未打印</td>';
                                        else if ($V["status"] == "1")
                                            echo '<td>已打印</td>';
                                        else if ($V["status"] == "2")
                                            echo '<td>打印失败</td>';

                                        $marchineid = $V["marchineId"];
                                        if ($marchineid == NULL) {
                                            echo "<td>无</td>";
                                        } else {
                                            $marchine_model = printor::model()->find(array('condition' => "machineId = '$marchineid'"));
                                            if (count($marchine_model) != 0) {
                                                echo "<td>" . $marchine_model->printorName . "</td>";
                                            } else {
                                                echo "<td>无</td>";
                                            }
                                        }

                                        echo'</tr>';
                                    }
                                    ?>
                                    </tr>


                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </DIV>
            <br>
        </DIV>
    </BODY>
</HTML>