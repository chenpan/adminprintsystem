<!DOCTYPE HTML>
<HTML>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            #order-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }

        </style>
        <script type="text/javascript">
            $(function() {
                $('#alreadytable').dataTable({
                    "serverSide": true,
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "ajax": './index.php?r=business/businessAjax',
//                    "stateSave": true,
                    "paginate": true,
                     pagingType: "input",
                    "order": [[6, "desc"]],
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有相关数据",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "",
                        "processing": "正在加载..."
                    },
                    'columns': [
                        {"data": "businessId", "visible": false, "orderble": false, "searchable": false},
                        {"data": "id"},
                        {"data": "orderId", "orderble": true, "searchable": true},
                        {"data": "name", "orderble": true, "searchable": true},
                        {"data": "school", "orderble": true, "searchable": true},
                        {"data": "type", "orderble": true, "searchable": true},
                        {"data": "placeOrdertime", "orderble": true, "searchable": true},
                        {"data": "paidMoney", "orderble": true, "searchable": true},
                        {"data": "consumptionIntegral", "orderble": true, "searchable": true},
                        {"data": "verificationCode", "orderble": true, "searchable": true},
                        {"data": "businessId",
                            "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);' " +
                                        "onclick='query(" + oData.businessId + ")'><span class='label label-success' style='cursor:pointer'>查看详情</span></a>");
                            }
                        }
                    ],
                    "columnDefs": [
                        {"targets": [0], "data": "businessId", visible: false, }
                    ],
                    "drawCallback": function(settings) {
                        var api = this.api();
                        v = api.row(0).column(0).data()[0];
                        if (v == null) {
                            api.row(0).column(7).visible(false);
                        } else {
                            api.row(0).column(7).visible(true);
                        }
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
            });
            function query(businessId)
            {
                window.location.href = "./index.php?r=business/orderDetail&businessid=" + businessId;
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head> 
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>订单列表</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=business/business">订单</a>
                        </li>
                    </ul>
                </div>  
                <DIV class="content-wrap">        
                    <DIV class="row">
                        <DIV class="col-lg-12">
                            <table id="alreadytable" width="100%">
                                <thead>
                                    <tr class="th">
                                        <th>订单序列</th>
                                        <th>序列</th>
                                        <th>订单号</th>
                                        <th>所属用户</th>
                                        <th>所属学校</th>
                                        <th>下单方式</th>
                                        <th>下单时间</th>
                                        <th>金额</th>
                                        <th>支付消耗积分</th>
                                        <th>验证码</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                            </table>
                        </DIV>
                    </DIV> 
                    <!-- FOOTER -->

                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    </div>
                    <!-- / END OF FOOTER -->
                </DIV>
                <br>
            </DIV>           
    </BODY>
</HTML>