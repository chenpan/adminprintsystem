<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
                letter-spacing: 0;
            }
            input{
                border:1px #f5f5f5 solid;
                padding: 5px 10px;
            }
            #active-points{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            input[type=text] {
                color:#37BCE5;
            }
        </style>
        <script type="text/javascript">

            $(function () {
                $('#alreadytable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#active-points").css("display", "block");
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#sure").click(function () {
                    var phone = $("#phone").val();
                    var points = $("#points").val();
                    var text = $("#text").text();
                    var re = /^1[34578]\d{9}(?:\,1[358]\d{9})*$/;
                    if ($("#phone").val() == "" || $("#phone").val() == null || (!re.test($("#phone").val())))
                    {
                        alert("请输入电话号码！");
                        return false;
                    } else if ($("#points").val() == "" || $("#points").val() == null || ($("#oldpoint").val() - $("#points").val()) < 0)
                    {
                        alert("请正确输入积分！");
                        return false;
                    } else
                    {
                        if (confirm("确认扣除？"))
                        $.post("./index.php?r=active/divpoints", {phone: phone, points: points, text: text}, function (data) {
                            var code = eval("(" + data + ")");
                            console.log(code.data);
                            if (code.data == "sucess")
                            {
                                alert("扣除成功！");
                                window.location.href = './index.php?r=active/parttimepoints';
                            } else if (code.data == "false")
                            {
                                alert("扣除失败！");
                            } else if (code.data == "nophone")
                            {
                                alert("无此手机信息！");
                            } else if (code.data == "errphone")
                            {
                                alert("手机格式不正确！");
                            }
                        });
                    }
                });
                $("#phone").blur(function () {
                    var phone = $("#phone").val();
                    var re = /^1[34578]\d{9}(?:\,1[358]\d{9})*$/;
                    if (re.test(phone)) {
                        $.post("./index.php?r=active/oldpoints", {phone: phone}, function (datainfo) {
                            var data = eval("(" + datainfo + ")");
                            var code = data[0].code;
                            var msg = data[0].msg;
                            console.log(msg);
                            if (code == 200) {
//                                alert("ok");
                                $("#oldpoint").val(msg);
                            } else if (code == 401) {
                                $("#oldpoint").text("获取失败！");
                            }
                        });
                    }
                });
                $("#points").blur(function () {
                    var oldpoints = $("#oldpoint").val();
                    var points = $("#points").val();
                    if ((oldpoints - points) < 0) {
                        alert("积分不足以用来扣除！");
                    }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>扣除积分</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=active/parttimepoints">积分</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">扣除积分</a>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="resetUser" class="form-group" style="text-align: center" >
                                <!--<iframe style="display:none" name="test"></iframe>-->
                                <form class="form-horizontal" name="parttimeform" id="parttimeform"> 
                                    <!--target="test"-->
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-5 control-label">手机号码：</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="phone" id="phone" placeholder="输入手机号码(单个手机号)"/>
                                        </div>   
                                        <div class="col-sm-3 error" id="phone1">                           
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="oldpoint" class="col-sm-5 control-label">剩余积分：</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="oldpoint" id="oldpoint" readonly="value"/>
                                        </div>   
                                        <div class="col-sm-3 error" id="oldpoint1">                           
                                        </div>
                                    </div>
                                    <div class="form-group"id="schooldiv">
                                        <label for="points" class="col-sm-5 control-label">扣除积分：</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="points" id="points" placeholder="输入要扣除的积分数"/>
                                        </div>   
                                        <div class="col-sm-3 error" id="points1">                           
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"><label for="text">发送内容:</label>    </div>
                                        <div class="col-sm-3"> <textarea rows="3" style="width: 372px;max-width:372px;max-height:200px;" id="text" name="text"></textarea></div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <div class="col-sm-offset-5 col-sm-3">
                                            <button type="button" id="sure" class="btn btn-info btn-block" style="width: 100%;outline:none;">确认</button>
                                        </div>
                                        <div id="add_success">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </DIV>     

                    </DIV>          
                </DIV>   
                <br>
                <!-- FOOTER -->
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </DIV> 
        </DIV>
    </BODY>
</HTML>