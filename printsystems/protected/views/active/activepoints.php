<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
                letter-spacing: 0;
            }
            input{
                border:1px #f5f5f5 solid;
                padding: 5px 10px;
            }
            #active-points{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            input[type=text] {
                color:#37BCE5;
            }
        </style>
        <script type="text/javascript">

            $(function() {
                $("#active-points").css("display", "block");
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#sure").click(function() {
                    var re = /^1[34578]\d{9}(?:\,1[34578]\d{9})*$/;
                    if ($("#phone").val() == "" || $("#phone").val() == null || (!re.test($("#phone").val())))
                    {
                        alert("请输入电话号码！");
                        return false;
                    } else if ($("#points").val() == "" || $("#points").val() == null || $("#points").val() == 0)
                    {
                        alert("请输入积分！");
                        return false;
                    } else
                    {
                        if (confirm("确认增加？"))
                            eidtuserform.submit();
                    }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>增加积分</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=active/activepoints">积分</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">增加积分</a>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="resetUser" class="form-group" style="text-align: center" >
                                <iframe style="display:none" name="test"></iframe>
                                <form class="form-horizontal" name="eidtuserform" id="eidtuserform" target="test" method="post">
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"><label for="phone">手机号码：</label></div>   
                                        <div class="col-sm-3"><input type="text" class="form-control" name="phone" id="phone" placeholder="输入手机号码(多个用逗号隔开)"/></div>   
                                        <div class="col-sm-1 star" id="phone">*</div>
                                    </div>
                                    <div class="form-group"id="schooldiv">
                                        <div class="col-sm-5 control-label"><label for="points">增加积分：</label></div>
                                        <div class="col-sm-3"><input type="text" class="form-control" name="points" id="points" placeholder="输入增加积分数"/></div>   
                                        <div class="col-sm-1 star" id="points">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"><label for="text">发送内容： </label></div>
                                        <div class="col-sm-3"><textarea rows="3" style="width: 372px;max-width:372px;max-height:200px;" id="text" name="text"></textarea></div>
                                        <div class="col-sm-1 star" id="text">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"><label for="text">是否为运营： </label></div>
                                        <div class="col-sm-3">
                                            <label class="radio-inline">
                                                <input type="radio" name="check" checked="checked" value="not"> 否
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="check" value="yes"> 是
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <div class="col-sm-5"></div>
                                        <div class="col-sm-3"> <button type="button" id="sure" class="btn btn-info btn-block" style="width: 100%;outline:none;">保存</button></div>
                                        <div class="col-sm-3" style="margin-top:5px;"><lable class="save_lable"></lable> <span id="add_success"></span></div>
                                    </div>
                                </form>
                            </div>
                        </DIV>     

                    </DIV>          
                </DIV>   
                <br>
                <!-- FOOTER -->
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </DIV> 
        </DIV>
    </BODY>
</HTML>