<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            .error{
                 color:red;
                margin-left: -20px;
                margin-top: 10px;
            }
              #terminal-grouping{
               background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#add_btn").click(function() {
                    var groupName = $("#groupName").val().replace(/\s+/g, "");
                    var groupDetail = $("#groupDetail").val().replace(/\s+/g, "");
                    var schoolName = $("#schoolName").val();

                    if (groupName.length == 0)
                    {
                        reback();
                        $("#groupName_error").text("请输入分组名称！");
                        return false;
                    }
                    if (schoolName == 0)
                    {
                        reback();
                        $("#schoolName_error").text("请选择一个学校！");
                        return false;
                    }
                    reback();
                    if (confirm("确定保存？"))
                    {
                        $.post("./index.php?r=printor/addgroup", {groupName: groupName, groupDetail: groupDetail, schoolName: schoolName}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "false")
                            {
                                reback();
                                $("#add_success").text("添加失败！");
                            }
                            else if (data.data == "success")
                            {
                                reback();
                                $("#add_success").text("添加成功！");
                                $("#groupName").val("");
                                $("#groupDetail").val("");
                                $("#schoolName").val("0");
                            }
                            else if (data.data == "exist")
                            {
                                reback();
                                $("#groupName_error").text("此分组名称已存在！");
                            }
                        });
                    }
                });
                   $("#terminal-open").css("display","block");
            });
            function reback() {
                $("#groupName_error").text("*");
                $("#add_success").text("");
                $("#schoolName_error").text("*");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>新增分组</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                         <li>终端
                        </li>
                         <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=printor/group">终端分组</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">新增分组</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="groupName" class="col-sm-5 control-label">分组名称：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="groupName" placeholder="请输入分组名称"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="groupName_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="schoolName" class="col-sm-5 control-label">所属学校：</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" id="schoolName">
                                            <option value="0">******请选择******</option>
                                            <?php foreach ($store_info as $k => $l) { ?>
                                                <option value="<?php echo $l->storeid; ?>"><?php echo $l->storename; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>   
                                    <div class="col-sm-3 error" id="schoolName_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="groupDetail" class="col-sm-5 control-label">分组描述：</label>
                                    <div class="col-sm-3">
                                        <textarea id="groupDetail" rows="5" style="width:100%;color:#8FBDE6;text-indent:1em"></textarea>
                                    </div>   
                                    <div class="col-sm-3 error" id="groupDetail_error">                           
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 30px;">
                                    <div class="col-sm-offset-5 col-sm-3">
                                        <button type="button" id="add_btn" class="btn btn-info btn-block" style="width: 100%;outline:none;">保存</button>
                                    </div>
                                    <div class="col-sm-3 error" id="add_success">

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->

    </body>

</html>

