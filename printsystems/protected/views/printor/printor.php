<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-information{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
            span .label-success{
                background-color:#45B6B0!important;
            }
            span .label-success:hover{
                background-color:#45B6B0!important;
            }
            .label-successs{
                background-color: white!important;
                border: 1px solid #60A73C!important;
                color: #60A73C;
            }
            .label-successs:hover{
                background-color: white!important;
                border: 1px solid #60A73C!important;
                color: #60A73C;
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $('#printortable').dataTable({
                    "serverSide": true,
                    "stateSave": false,
                    "displayLength": 10,
                    "pagingType": "input",
                    "ajax": './index.php?r=printor/printortopage',
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    },
                    'columns': [
                        {"data": "id", "orderble": true, "searchable": true},
                        {"data": "printorId", "visible": false, "orderble": false, "searchable": false},
                        {"data": "machineId", "orderble": true, "searchable": true},
                        {"data": "printorName", "orderble": true, "searchable": true},
                        {"data": "address", "orderble": true, "searchable": true},
                        {"data": "picture", "orderble": true, "searchable": true, "render": function (data, type, full, meta) {
                                return data == null ?
                                        "无" :
                                        '<a class="fancy " href="./images/terninalPictures/' + data + '" data-fancybox-group="gallery"><img style = "width:20px;height:20px" class="thumbnails" src="./images/terninalPictures/' + data + '"/></a>';
                            }},
                        {"data": "storename", "orderble": true, "searchable": true},
                        {"data": "groupName", "orderble": true, "searchable": true},
                        {"data": "printorId",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a <?php echo $editPrintor; ?> display='<?php echo $editPrintor; ?>'href='javascript:void(0);' " +
                                        "onclick='edit(" + oData.printorId + ")'><span class='label label-success' style='cursor:pointer'>编辑</span></a>&nbsp")
                                        .append("<a <?php echo $deletePrintor; ?> display='<?php echo $deletePrintor; ?>'href='javascript:void(0);\n\
                                        'onclick='deleteterninal(" + oData.printorId + ")'><span class='label label-success' style='cursor:pointer'>删除</span></a>&nbsp");
                            }
                        }
                    ]
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });

                $("#addprintor").click(function () {
                    window.location.href = "./index.php?r=printor/addprintor";
                });
                $("#terminal-open").css("display", "block");
                if ('<?php echo $addPrintor; ?>' == "hidden") {
                    $("#addprintor").parent().parent().parent().hide();
                }
            });
            function deleteterninal(printorId) {
                if ('<?php echo $deletePrintor; ?>' == "") {
                    if (confirm("确定删除？")) {
                        $.post("./index.php?r=printor/deleteterninal", {printorId: printorId}, function (datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                window.location.href = "./index.php?r=printor/printor";
                            }
                        });
                    }
                } else if ('<?php echo $deletePrintor; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
            function edit(printorId) {
                if ('<?php echo $editPrintor; ?>' == "") {
                    window.location.href = './index.php?r=printor/editprintor&printorId='+printorId;

                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>终端信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>终端
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">终端信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addprintor" value="新增终端">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table  id="printortable">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>printorID</th>
                                        <th>终端ID</th>
                                        <th>终端名</th>
                                        <th>地址</th>
                                        <th>展示图片</th>
                                        <th>所属学校</th>
                                        <th>所属组</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

