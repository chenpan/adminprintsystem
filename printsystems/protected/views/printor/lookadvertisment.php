<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-information{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            #terminal-open{
                display: block;
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#adtable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });

                $("#addads").click(function() {
                    window.location.href = "./index.php?r=advertisements/addadvertisement&groupID=<?php echo $groupID; ?>";
                });

            });
            function deleteAds(advertisementID, companyID) {

                var advertisementID = advertisementID;
                if (confirm("确认删除 ？"))
                {
                    $.post("./index.php?r=advertisements/delads", {advertisementID: advertisementID}, function(data) {
                        if (data.data == "success")
                        {
                            alert("删除成功！");
                            window.location.href = "./index.php?r=advertisements/advertisementInfo&companyID=" + companyID;
                        } else if (data.data == "false")
                        {
                            alert("删除失败！");
                        }
                    }, 'json');
                }

            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>广告信息--<?php echo group::model()->findByPk($groupID)->groupName; ?></H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>终端
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=printor/group">终端分组</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">广告管理</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addads" value="新增广告">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="adtable">
                                <thead>
                                    <tr class="th">
                                        <th  style="padding-left: 10px;">序列</th>
                                        <th>广告名称</th>
                                        <th>广告长度</th>
                                        <th>所属公司</th>
                                        <th>每日开始时间</th>
                                        <th>每日结束时间</th>
                                        <th>开始日期</th>
                                        <th>结束日期</th>
                                        <th>每日播放次数</th>
                                        <th>添加时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($advertisement_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td  style="padding-left: 10px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->advertisementName . '.' . $V->advertisementFormat; ?></td>
                                            <td><?php echo $V->advertisementLength . "秒"; ?></td>
                                            <td><?php echo company::model()->find("companyID = $V->_companyID")->companyName; ?></td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $V->advertisementID and _groupID = $groupID")->starttime;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $V->advertisementID and _groupID = $groupID")->endtime;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $V->advertisementID and _groupID = $groupID")->startDate;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $V->advertisementID and _groupID = $groupID")->endDate;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $V->advertisementID and _groupID = $groupID")->times;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $V->advertisementID and _groupID = $groupID")->addTime;
                                                ?>
                                            </td>
                                            <td>
                                                <a class="edit_btn" href="./index.php?r=advertisements/editadsdelivery&groupID=<?php echo $groupID; ?>&advertisementID=<?php echo $V->advertisementID; ?>&id=<?php echo $V->advertisementID; ?>"><span class="label label-success">编辑</span></A>
                                                <a class="delete_btn" href="#" onclick="deleteAds(<?php echo $V->advertisementID; ?>,<?php echo company::model()->find("companyID = $V->_companyID")->companyID; ?>)" ><span class="label label-success">删除</span></A>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

