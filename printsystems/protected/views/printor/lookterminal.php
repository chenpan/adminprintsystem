<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-information{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
            span .label-success{
                background-color:#45B6B0!important;
            }
            span .label-success:hover{
                background-color:#45B6B0!important;
            }
            .label-successs{
                background-color: white!important;
                border: 1px solid #60A73C!important;
                color: #60A73C;
            }
            .label-successs:hover{
                background-color: white!important;
                border: 1px solid #60A73C!important;
                color: #60A73C;
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#printortable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $('.fancy').fancybox();
                $('.fancybox-thumbs').fancybox({
                    prevEffect: 'none',
                    nextEffect: 'none',
                    closeBtn: false,
                    arrows: false,
                    nextClick: true,
                    helpers: {
                        thumbs: {
                            width: 50,
                            height: 50
                        }
                    }
                });
                $("#terminal-open").css("display", "block");
                $("#addprintor").click(function() {
                    window.location.href = "./index.php?r=printor/addprintor&groupID=<?php echo $group_info->groupID; ?>";
                });
            });
            function deleteterninal(printorId) {
                if (confirm("确定删除？")) {
                    $.post("./index.php?r=printor/deleteterninal", {printorId: printorId}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "success") {
                            window.location.href = "./index.php?r=printor/lookterminal&groupID=<?php echo $group_info->groupID; ?>";
                        }
                    });
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>终端信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>终端
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=printor/group">终端分组</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">终端信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addprintor" value="新增终端">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table  id="printortable">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>终端ID</th>
                                        <th>终端名</th>
                                        <!--<th>状态</th>-->
                                        <th>地址</th>
                                        <th>展示图片</th>
                                        <!--<th>剩余纸张</th>-->
                                        <th>所属学校</th>
                                        <th>所属组</th>
<!--                                        <th>版本号</th>
                                        <th>错误码</th>
                                        <th>打印机</th>-->
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($printor_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td style="padding-left: 13px;"><A><?php echo $K + 1; ?></A></td>
                                            <td><?php echo $V->machineId; ?></td>
                                            <td><?php echo $V->printorName; ?></td>
                                            <td>
                                                <?php
                                                echo $V->address;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($V->picture == NULL)
                                                    echo "无";
                                                else {
                                                    echo '<a class="fancy "  href="./images/terninalPictures/' . $V->picture . '" data-fancybox-group="gallery">';
                                                    echo '<img style = "width:20px;height:20px" class="thumbnails" src="./images/terninalPictures/' . $V->picture . '"/>';
                                                    echo '</a>';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($V->_storeId == "") {
                                                    echo "暂无";
                                                } else {
                                                    echo store::model()->find("storeid=$V->_storeId")->storename;
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($V->_groupID == "")
                                                    echo '暂无';
                                                else {
                                                    if ($V->_groupID == 0 || $V->_groupID == "") {
                                                        echo "暂无";
                                                    } else {
                                                        $groupName = group::model()->find("groupID = $V->_groupID")->groupName;
                                                        echo $groupName;
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a class="edit_btn" href="./index.php?r=printor/editprintor&printorId=<?php echo $V->printorId; ?>"><span class="label label-success">编辑</span></a>
                                                <span class="label label-success delete_btn" style="cursor:pointer" onclick="deleteterninal('<?php echo $V->printorId; ?>')">删除</span>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

