<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <?php echo $recommend; ?>

        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-3.0.3.min.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/prettify.css" type="text/css">

<!--<script type="text/javascript" src="./css/multiselect/js/jquery-2.0.3.min.js"></script>-->
<!--<script type="text/javascript" src="./css/multiselect/js/bootstrap-3.0.3.min.js"></script>-->
        <script type="text/javascript" src="./css/multiselect/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/prettify.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#vesionGroup').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150
                });
            });
        </script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-version{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#type").val("<?php echo $version_info->type; ?>");
                $('#vesionGroup').multiselect('select', ['<?php echo $group_groupId; ?>']);
                $("#save").click(function() {
                    if ($("#versionname").val() == "" || $("#versionname").val() == null)
                    {
                        reback();
                        $("#version-name").text("请输入版本名称！");
                        return false;
                    } else if ($("#type").val() == 0 || $("#type").val() == "") {
                        reback();
                        $("#version-Type").text("请选择类型！");
                        return false;
                    } else if ($("#vesionCode").val() == "" || $("#vesionCode").val() == null)
                    {
                        reback();
                        $("#version-num").text("请输入版本号！");
                        return false;
                    } else
                    {
                        $("#version-file,#version-name,#version-num,#version-group").text("");
                        if (confirm("确认保存？"))
                            editversionform.submit();
                    }
                });
                $("#terminal-open").css("display", "block");
            });
            function reback() {
                $("#version-file,#version-name,#version-num,#version-group").text("*");

                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>编辑版本</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=printor/printversion">终端版本</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">编辑版本</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <iframe style="display:none" name="test"></iframe>
                                    <form role="form" id="editversionform"name="editversionform"target="test"method="post"enctype="multipart/form-data" action="./index.php?r=printor/editVersions"class="form-horizontal">
                                        <input id="versionID" type="hidden" name="versionID" value="<?php echo $version_info->versionId; ?>">

                                        <!--                                        <div class="form-group">
                                                                                    <div class="col-sm-5 control-label"> <label for="versionfile">版本文件:</label>    </div>
                                                                                    <div class="col-sm-3"> <input type="file" id="versionfile" name="versionfile" style="outline:none;margin-top: 7px;">    </div>
                                                                                    <div class="col-sm-3 star" style="margin-top: 13px;" id="version-file">*</div>
                                                                                </div>-->
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="versionname">版本名称:</label>    </div>
                                            <div class="col-sm-3"><input type="text" placeholder="" id="versionname" name ="versionname" class="form-control" placeholder="请输入版本名称" value="<?php echo $version_info->versionFile; ?>">    </div>
                                            <div class="col-sm-3 star" id="version-name">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="versiondescript">版本简介:</label>    </div>
                                            <div class="col-sm-3"> <textarea rows="9" style="width:100%;color:#8FBDE6;text-indent:1em" id="versiondescript" name="versiondescript"><?php echo $version_info->versiondescript; ?></textarea>    </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="vesionType">类型:</label></div>
                                            <div class="col-sm-3">
                                                <select name="type" id="type" class="form-control">
                                                    <option  value="0" selected="selected">--请选择--</option>
                                                    <option name="update"id="update" value="update">update</option>
                                                    <option name="patch"id="patch" value="patch">patch</option>
                                                    <option name="config"id="config" value="config">config</option>
                                                    <option name="reboot "id="reboot " value="reboot">reboot</option>
                                                </select>   
                                            </div>
                                            <div class="col-sm-3 star" id="version-Type">*</div>
                                        </div>  
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="vesionCode">版本号:</label>    </div>
                                            <div class="col-sm-3"> <input type="email" placeholder="" id="vesionCode" name="vesionCode" class="form-control" placeholder="请输入版本号" value="<?php echo $version_info->versionCode; ?>">    </div>
                                            <div class="col-sm-3 star" id="version-num">*</div>
                                        </div>  
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="vesionGroup">所属分组:</label>    </div>
                                            <div class="col-sm-3"> 
                                                <select id="vesionGroup" name="vesionGroup[]" class="form-control" multiple="multiple" style="padding:0px">
                                                    <?php
                                                    foreach ($group_info as $k => $l) {
                                                        ?>
                                                        <option value="<?php echo $l->groupID; ?>"><?php echo $l->groupName; ?></option>
                                                    <?php } ?>
                                                </select>    </div>
                                            <div class="col-sm-3 star" id="version-group">*</div>
                                        </div>
                                        <div class="form-group" style="padding-top: 10px;">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-3"> 
                                                <input class="btn btn-info" type="button" id="save" value="保存" style="width:100%;"></input>
                                            </div>
                                            <div><lable class="save_lable"></lable></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>

        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

