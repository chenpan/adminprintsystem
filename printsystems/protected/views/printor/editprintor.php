<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-information{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#schooltable').dataTable({
                    stateSave: true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });

                $("#printorgroup").val("<?php echo $printor_info->_groupID; ?>");
                $("#school").val("<?php echo $printor_info->_storeId; ?>");

                $("#save").click(function() {
                    if ($("#machineId").val() == "" || $("#machineId").val() == null)
                    {
                        $("#machineId-info").text("请输入终端ID！");
                        return false;
                    } else if ($("#printorname").val() == "" || $("#printorname").val() == null)
                    {
                        $("#printorname-info").text("请输入终端名称！");
                        return false;
                    } else if ($("#printoraddress").val() == "" || $("#printoraddress").val() == null)
                    {
                        $("#printoraddress-info").text("请输入终端地址！");
                        return false;
                    } else if ($("#printorlng").val() == "" || $("#printorlng").val() == null)
                    {
                        reback();
                        $("#printorlng-info").text("请输入终端经度！");
                        return false;
                    } else if ($("#printorlat").val() == "" || $("#printorlat").val() == null)
                    {
                        reback();
                        $("#printorlat-info").text("请输入终端纬度！");
                        return false;
                    } else if ($("#printorgroup").val() == "" || $("#printorgroup").val() == 0)
                    {
                        $("#printorgroup-info").text("请选择一个分组！");
                        return false;
                    } else if ($("#school").val() == "" || $("#school").val() == 0)
                    {
                        $("#school-info").text("请选择一个学校！");
                        return false;
                    }
                    else
                    {
                        $("#machineId-info,#printorname-info,#printoraddress-info,#printorgroup-info,#school-info,#showpicture-info,#printorlng-info,#printorlat-info").text("*");
                        if (confirm("确认保存？"))
                            editprintorform.submit();
                    }
                });
                $("#terminal-open").css("display", "block");
            });
            $("#school").ready(function() {
                var schoool = document.getElementById("school");
                var index = schoool.selectedIndex; // 选中索引
                var schooltext = schoool.options[index].text; // 选中文本
                var schoolvalue = schoool.options[index].value; // 选中值

                var printorgroup = document.getElementById("printorgroup");
                printorgroup.innerHTML = "";
                $.post("./index.php?r=printor/editprintorAjax", {value: schoolvalue, text: schooltext}, function(datainfo) {
                    var data = eval("(" + datainfo + ")");
                    for (i = 0; i < data.length; i++) {
                        var groupid = data[i].groupid;
                        var groupname = data[i].groupname;
                        var opt = document.createElement("OPTION");
                        opt.value = groupid;
                        opt.text = groupname;
                        opt.selected = false;
                        printorgroup.add(opt);
                    }
                });
            })
            function onSelect() {
                var schoool = document.getElementById("school");
                var index = schoool.selectedIndex; // 选中索引
                var schooltext = schoool.options[index].text; // 选中文本
                var schoolvalue = schoool.options[index].value; // 选中值

                var printorgroup = document.getElementById("printorgroup");
                printorgroup.innerHTML = "";
                $.post("./index.php?r=printor/editprintorAjax", {value: schoolvalue, text: schooltext}, function(datainfo) {
                    var data = eval("(" + datainfo + ")");
                    for (i = 0; i < data.length; i++) {
                        var groupid = data[i].groupid;
                        var groupname = data[i].groupname;
                        var opt = document.createElement("OPTION");
                        opt.value = groupid;
                        opt.text = groupname;
                        opt.selected = false;
                        printorgroup.add(opt);
                    }
                });
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>编辑终端</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>终端
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=printor/printor">终端信息</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">编辑终端</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <iframe style="display:none" name="test"></iframe>
                                    <form role="form" id="editprintorform" name="editprintorform" method="post" target="test" enctype="multipart/form-data"  class="form-horizontal">
                                        <input type="text" id="oldmachineId" name ="oldmachineId"  style="display:none" value="<?php echo $printor_info->machineId; ?>">
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="machineId">终端ID:</label>  </div>
                                            <div class="col-sm-3"> <input type="text" id="machineId" name ="machineId" class="form-control" value="<?php echo $printor_info->machineId; ?>"  placeholder="请输入终端ID">  </div>
                                            <div class="col-sm-3 star" id="machineId-info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="printorname">终端名称:</label>  </div>
                                            <div class="col-sm-3"><input type="text" id="printorname" name ="printorname" class="form-control" value="<?php echo $printor_info->printorName; ?>"  placeholder="请输入终端名称">  </div>
                                            <div class="col-sm-3 star" id="printorname-info">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="printoraddress">终端地址:</label>  </div>
                                            <div class="col-sm-3"><input type="text" id="printoraddress" name="printoraddress" class="form-control" value="<?php echo $printor_info->address; ?>"  placeholder="请输入终端地址">  </div>
                                            <div class="col-sm-3 star" id="printoraddress-info">*</div>
                                        </div>       
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="printorlng">终端经度:</label></div>
                                            <div class="col-sm-3"><input type="text" id="printorlng" name="printorlng" class="form-control" value="<?php echo $printor_info->lng; ?>" placeholder="请输入终端经度"></div>
                                            <div class="col-sm-3 star" id="printorlng-info">*</div>
                                        </div>   
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="printorlat">终端纬度:</label></div>
                                            <div class="col-sm-3"><input type="text" id="printorlat" name="printorlat" class="form-control" value="<?php echo $printor_info->lat; ?>" placeholder="请输入终端纬度"></div>
                                            <div class="col-sm-3 star" id="printorlat-info">*</div>
                                        </div>   
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="school">所属院校:</label>  </div>
                                            <div class="col-sm-3"> <select id="school" name="school" class="form-control"  onchange="onSelect()">
                                                    <?php foreach ($store_info as $k => $l) { ?>
                                                        <option value="<?php echo $l->storeid; ?>"><?php echo $l->storename; ?></option>
                                                    <?php } ?>    
                                                </select>  </div>
                                            <div class="col-sm-3 star" id="school-info">*</div>
                                            <!--<input type="email" placeholder="" id="phone" name="phone" class="form-control">-->
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="printorgroup">终端分组:</label>  </div>
                                            <div class="col-sm-3"> <select id="printorgroup" name="printorgroup" class="form-control">
                                                    <?php foreach ($group_info as $k => $l) { ?>
                                                        <option value="<?php echo $l->groupID; ?>"><?php echo $l->groupName; ?></option>
                                                    <?php } ?>
                                                </select>  </div>
                                            <div class="col-sm-3 star" id="printorgroup-info">*</div>
                                           <!--<input type="password" placeholder="" id="schoolpassword" name="schoolpassword" class="form-control">-->
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="logo">展示图片:</label>  </div>
                                            <div class="col-sm-3"><input type="file" id="showpicture" name="showpicture" style="margin-top: 7px;outline:none;">  </div>
                                            <!--<div class="col-sm-3 star" id="showpicture-info">*</div>-->
                                        </div>

                                        <div class="form-group" style="margin-top: 30px;">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-3"> <button class="btn btn-info" type="button" id="save"  style="width: 100%;outline:none;">保存</button></div>
                                            <div class="col-sm-3" style="margin-top:5px;"><lable class="save_lable"></lable> <span id="info"></span></div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

