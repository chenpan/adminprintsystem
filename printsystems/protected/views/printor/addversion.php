<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <?php echo $recommend; ?>

        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-3.0.3.min.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/prettify.css" type="text/css">
        <link rel="stylesheet" href="./css/oss/style.css" type="text/css">

<!--<script type="text/javascript" src="./css/multiselect/js/jquery-2.0.3.min.js"></script>-->
<!--<script type="text/javascript" src="./css/multiselect/js/bootstrap-3.0.3.min.js"></script>-->
        <script type="text/javascript" src="./css/multiselect/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/prettify.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#vesionGroup').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150
                });
            });
        </script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-version{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
            .btn .caret {
                margin-left: 300px;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#terminal-open").css("display", "block");
            });
            function reback() {
                $("#version-file,#version-name,#version-num,#version-group，#version-Type").text("*");

                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>新增版本</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=printor/printversion">终端版本</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">新增版本</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form role="form" id="addversionform" method="post" enctype="multipart/form-data"  class="form-horizontal">                                        
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="versionname">版本名称:</label>    </div>
                                            <div class="col-sm-3"><input type="text" id="versionname" name ="versionname" class="form-control" placeholder="请输入版本名称">    </div>
                                            <div class="col-sm-3 star" id="version-name">*</div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="versiondescript">版本简介:</label>    </div>
                                            <div class="col-sm-3"> <textarea rows="9" style="width:100%;color:#8FBDE6;text-indent:1em" id="versiondescript" name="versiondescript"></textarea>    </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="vesionType">类型:</label></div>
                                            <div class="col-sm-3">
                                                <select name="type" id="type" class="form-control">
                                                    <option  selected="selected">--请选择--</option>
                                                    <option name="update"id="update" value="update">update</option>
                                                    <option name="patch"id="patch" value="patch">patch</option>
                                                    <option name="config"id="config" value="config">config</option>
                                                    <option name="reboot "id="reboot " value="reboot">reboot</option>
                                                </select>   
                                            </div>
                                            <div class="col-sm-3 star" id="version-Type">*</div>
                                        </div>  
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="vesionCode">版本号:</label></div>
                                            <div class="col-sm-3">
                                                <input type="text" id="vesionCode" name="vesionCode" class="form-control" placeholder="请输入版本号">
                                            </div>
                                            <div class="col-sm-3 star" id="version-num">*</div>
                                        </div>  
                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"><label for="vesionGroup">所属分组:</label></div>
                                            <div class="col-sm-3"> 
                                                <select id="vesionGroup" name="vesionGroup[]" class="form-control" multiple="multiple" style="padding:0px;text-align: left;">
                                                    <?php
                                                    foreach ($group_info as $k => $l)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $l->groupID; ?>"><?php echo $l->groupName; ?></option>
<?php } ?>
                                                </select>   
                                            </div>
                                            <div class="col-sm-3 star" id="version-group">*</div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-5 control-label"> <label for="versionfile">版本文件:</label>    </div>
                                            <div class="col-sm-3">
                                                <div id="ossfile">你的浏览器不支持flash,Silverlight或者HTML5！</div>
                                                <div id="container">
                                                    <a id="selectfiles" class="btn" style="width:100%;" href="javascript:void(0);">选择文件</a>
                                                </div>  
                                                <p>&nbsp;</p>
                                                        <!--<input type="file" id="versionfile" name="versionfile" style="outline:none;margin-top: 7px;">-->
                                            </div>
                                            <div class="col-sm-3 star" style="margin-top: 13px;" id="version-file">*<pre id="console" style="border:0px;background-color: #FFFFFF"></pre></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-5"></div>
                                            <div class="col-sm-3"> 
                                                <a id="postfiles" class="btn  btn-info " style="width:100%;" href="javascript:void(0);">保存</a>
                                                <!--<button class="btn btn-info" type="button" id="save"  style="width:100%;">保存</button>-->
                                            </div>
                                            <div class="col-sm-3 star"><span id="info"></span></div>
                                        </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>
    <script type="text/javascript" src="./css/oss/lib/plupload-2.1.2/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="./css/oss/upload_version.js"></script>
</html>

