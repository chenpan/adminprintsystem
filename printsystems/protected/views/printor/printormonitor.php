<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="./css/PictureDisplay/css/index.css" rel="stylesheet">
        <?php echo $recommend; ?>
        <script type="text/javascript" src="./css/PictureDisplay/js/jquery.fancybox.js "></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-monitoring{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            .label-successs{
                background-color: white!important;
                border: 1px solid #60A73C!important;
                color: #60A73C;
            }
            .label-successs:hover{
                background-color: white!important;
                border: 1px solid #60A73C!important;
                color: #60A73C;
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $('#printortable').dataTable({
                    "serverSide": true,
                    "stateSave": true,
                    "pagingType": "input",
                    "ajax": './index.php?r=printor/printormonitortopage',
                    "displayLength": 10,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    },
                    'columns': [
                        {"data": "id", "orderble": true, "searchable": true},
                        {"data": "machineId", "orderble": true, "searchable": true},
                        {"data": "printorName", "orderble": true, "searchable": true},
                        {"data": "last_time", "orderble": true, "searchable": true, "render": function (data, type, full, meta) {
                                return data == "关机" ?
                                        '<span style="color:red">' + data + '</span>' :
                                        data;
                            }},
                        {"data": "address", "orderble": true, "searchable": true},
                        {"data": "paper_remain", "orderble": true, "searchable": true, "render": function (data, type, full, meta) {
                                return data < 100 ?
                                        '<span style="color:red">' + data + '</span>' :
                                        data;
                            }},
                        {"data": "storename", "orderble": true, "searchable": true},
                        {"data": "groupName", "orderble": true, "searchable": true},
                        {"data": "version", "orderble": true, "searchable": true},
                        {"data": "printer_errorcode", "orderble": true, "searchable": true},
                        {"data": "printer_error_time", "orderble": true, "searchable": true},
                        {"data": "updatestate", "orderble": true, "searchable": true},
                        {"data": "updatetime", "orderble": true, "searchable": true},
                        {"data": "printer_status", "orderble": true, "searchable": true, "render": function (data, type, full, meta) {
                                return data =="运行正常" ?
                                        '<span class="label label-success label-successs">运行正常</span>' :
                                         '<span class="label label-danger">' + data +'</span>';
                            }}
                    ]
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#terminal-open").css("display", "block");
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>终端监控</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>终端
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">终端监控</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table  id="printortable">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>终端ID</th>
                                        <th>终端名</th>
                                        <th>状态</th>
                                        <th>地址</th>
                                        <!--<th>展示图片</th>-->
                                        <th>剩余纸张</th>
                                        <th>所属学校</th>
                                        <th>所属组</th>
                                        <th>版本号</th>
                                        <!--                                                <th>剩余元</th>
                                                                                        <th>剩余角</th>
                                                                                        <th>找零器角</th>
                                                                                        <th>找零器元</th>
                                                                                        <th>硬币投币器</th>
                                                                                        <th>纸币投币器</th>
                                                                                        <th>刷卡器</th>-->
                                        <th>错误码</th>   
                                        <th>错误发生时间</th>
                                        <th>升级状态</th>
                                        <th>升级时间</th>
                                        <th>打印机状态</th>
                                    </tr>
                                </thead>
                                <table>
                                </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

