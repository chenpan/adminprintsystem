<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            .error{
                color:red;
            }
            #terminal-version{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $('#printversiontable').dataTable({
                    stateSave: true,
                    pagingType:"input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#addversion").click(function() {
                    window.location.href = "./index.php?r=printor/addversion";
                });
                $("#terminal-open").css("display", "block");

                if ('<?php echo $deleteVersion; ?>' == "false") {
                    $(".delete_btn").hide();
                }
                if ('<?php echo $addVersion; ?>' == "false") {
                    $("#addversion").parent().parent().parent().hide();
                }
                if ('<?php echo $editVersion; ?>' == "false") {
                    $(".edit_btn").hide();
                }
            });
            function deleteversion(versionId, versionFile) {
                if ('<?php echo $deleteVersion; ?>' == "true") {
                    if (confirm("确认删除 " + versionFile + " 版本?"))
                    {
                        $.post("./index.php?r=printor/deleteversion", {versionId: versionId}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                alert("删除成功");
                                window.location.href = "./index.php?r=printor/printversion";
                            } else if (data.data == "false")
                            {
                                alert("删除失败！");
                            } else if (data.data == "no")
                            {
                                alert("此分组不存在！");
                            }
                            else if (data.data == "no_printor")
                            {
                                alert("请先删除此分组下的终端！");
                            }
                        });
                    }
                } else if ('<?php echo $deleteVersion; ?>' == "false") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
            function downloads(versionId) {
                window.location.href = "./index.php?r=printor/downloadprintorversion&versionId=" + versionId;
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>终端版本</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>终端
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">终端版本</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addversion" value="新增版本">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="printversiontable">
                                <thead>
                                    <tr class="th">
                                        <th  style="padding-left: 10px;">序列</th>
                                        <th>版本号</th>
                                        <th>加入时间</th>
                                        <th>版本名称</th>
                                        <th>版本简介</th>
                                        <th>投放组</th>
                                        <th>类型</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($printVesion_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td  style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->versionCode; ?></td>

                                            <td> <DIV class="sparkbar" data-color="#00a65a" data-height="20">
                                                    <?php echo $V->updateTime; ?>
                                                </DIV>
                                            </td>
                                            <td>
                                                <?php echo $V->versionFile; ?>  
                                            </td>
                                            <td>
                                                <?php echo $V->versiondescript; ?>  
                                            </td>
                                            <td>
                                                <?php
                                                $group_name = "";
                                                if ($V->_groupID != "") {
                                                    $versionGroup = explode(',', $V->_groupID); //ID 1,2,3,4

                                                    for ($i = 0; $i < count($versionGroup); $i++) {
                                                        $group_info = group::model()->find("groupID = '$versionGroup[$i]'");
                                                        if (count($group_info) != 0) {
                                                            $group_name .= $group_info->groupName . ",";
                                                        }
                                                    }
                                                    $group_name = substr($group_name, 0, -1);
                                                }
                                                echo $group_name;
                                                ?>  
                                            </td>
                                            <td><?php echo $V->type; ?>
                                            </td>
                                            <td>
        <!--                                                <a class="edit_btn" href="./index.php?r=printor/editversion&versionId=<?php echo $V->versionId; ?>"><span class="label label-success">编辑</span></a>
                                                    <a class="delete_btn" href="#" onclick="deleteversion(<?php echo $V->versionId; ?>, '<?php echo $V->versionFile; ?>')" ><span class="label label-success">删除</span></a>-->
                                                <a href="#" onclick="downloads(<?php echo $V->versionId; ?>)"><span class="label label-success">下载</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->

    </body>

</html>

