<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-information{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
            span .label-success{
                background-color:#45B6B0!important;
            }
            span .label-success:hover{
                background-color:#45B6B0!important;
            }
            .label-successs{
                background-color: white!important;
                border: 1px solid #60A73C!important;
                color: #60A73C;
            }
            .label-successs:hover{
                background-color: white!important;
                border: 1px solid #60A73C!important;
                color: #60A73C;
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $('#printorerrortable').dataTable({
                    "serverSide": true,
                    "stateSave":false,
                    "pagingType": "input",
                    "ajax": './index.php?r=printor/printorerrAjax',
                    "order": [8, 'desc'],
                    "displayLength": 100,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    },
                    'columns': [
                        {"data": "errorid", "orderble": true, "visible": false, },
                        {"data": "id", "orderble": true, "searchable": true},
                        {"data": "machineId", "orderble": true, "searchable": true},
                        {"data": "printorName", "orderble": true, "searchable": true},
                        {"data": "address", "orderble": true, "searchable": true},
                        {"data": "storename", "orderble": true, "searchable": true},
                        {"data": "error_type", "orderble": true, "searchable": true},
                        {"data": "error_informaition", "orderble": true, "searchable": true},
                        {"data": "occur_time", "orderble": true, "searchable": true},
                        {"data": "errorid",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);onclick=deleteterninal(" + oData.errorid + ")'><span class='label label-success' style='cursor:pointer'>删除</span></a>");
                            }
                        }
                    ]
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#terminal-open").css("display", "block");
                if ('<?php echo $delErrInfo; ?>' == "false") {
                    $(".delete_btn").hide();
                }
            });
            function deleteterninal(errorid) {
//                if ('<?php echo $delErrInfo; ?>' == "false") {
//                    return false;
//                }
                if (confirm("确定删除？")) {
                    $.post("./index.php?r=printor/deleteerrorinfo", {errorid: errorid}, function (datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "success") {
                            alert("删除成功");
                            window.location.href = "./index.php?r=printor/printerror";
                        }
                    });
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>错误信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>终端
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">错误信息</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table  id="printorerrortable">
                                <thead>
                                    <tr class="th">
                                        <th>错误Id</th>
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>终端ID</th>
                                        <th>终端名称</th>
                                        <th>终端地址</th>
                                        <th>所属学校</th>
                                        <th>错误类型</th>
                                        <th>错误信息</th>
                                        <th>产生时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

