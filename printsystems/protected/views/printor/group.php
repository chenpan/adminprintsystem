<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #terminal-grouping{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);  
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#grouptable').dataTable({
                    stateSave: true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#addgroup").click(function() {
                    window.location.href = "./index.php?r=printor/addgroup";
                });
                $("#terminal-open").css("display", "block");

                if ('<?php echo $addGroup; ?>' == "hidden") {
                    $("#addgroup").parent().parent().parent().hide();
                }

            });
            function deletegroup(groupID, groupName) {
                if ('<?php echo $deleteGroup; ?>' == "") {
                    if (confirm("确认删除 " + groupName + " 分组?"))
                    {
                        $.post("./index.php?r=printor/deleteGroup", {groupID: groupID}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                alert("删除成功");
                                window.location.href = "./index.php?r=printor/group";
                            } else if (data.data == "false")
                            {
                                alert("删除失败！");
                            } else if (data.data == "no")
                            {
                                alert("此分组不存在！");
                            }

                        });
                    }
                } else if ('<?php echo $deleteGroup; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>终端分组</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>终端
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">终端分组</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addgroup" value="新增分组">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="grouptable">
                                <thead>
                                    <tr class="th">
                                        <th  style="padding-left: 10px;">序列</th>
                                        <th>组名</th>
                                        <th>组描述</th>
                                        <th>所属学校</th>
                                        <th>版本号</th>
                                        <th>版本名称</th>
                                        <th>版本描述</th>
                                        <th>版本类型</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($group_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td  style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->groupName; ?></td>
                                            <td><?php echo $V->groupDetail; ?></td>
                                            <td><?php
                                                $store_info = store::model()->find("storeid = $V->_storeid");
                                                if (count($store_info) != 0) {
                                                    echo $store_info->storename;
                                                } else {
                                                    echo "无";
                                                }
                                                ?></td>
                                            <td><?php
                                                if (isset($V->_versionId)) {
                                                    $printVersion_infos = printVersion::model()->find("versionId = $V->_versionId");
                                                    if (count($printVersion_infos) != 0) {
                                                        echo $printVersion_infos->versionCode;
                                                    } else {
                                                        echo "无";
                                                    }
                                                } else {
                                                    echo '无';
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                if (isset($V->_versionId)) {
                                                    if (count($printVersion_infos) != 0) {
                                                        echo $printVersion_infos->versionFile;
                                                    } else {
                                                        echo "无";
                                                    }
                                                } else {
                                                    echo '无';
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                if (isset($V->_versionId)) {
                                                    if (count($printVersion_infos) != 0) {
                                                        echo $printVersion_infos->versiondescript;
                                                    } else {
                                                        echo "无";
                                                    }
                                                } else {
                                                    echo '无';
                                                }
                                                ?>
                                            </td>
                                            <td><?php
                                                if (isset($V->_versionId)) {
                                                    if (count($printVersion_infos) != 0) {
                                                        echo $printVersion_infos->type;
                                                    } else {
                                                        echo "无";
                                                    }
                                                } else {
                                                    echo '无';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a class="edit_btn" <?php echo $editGroup; ?> href="./index.php?r=printor/editgroup&groupID=<?php echo $V->groupID; ?>"><span class="label label-success">编辑</span></a>
                                                <a class="delete_btn" <?php echo $deleteGroup; ?> href="#" onclick="deletegroup(<?php echo $V->groupID; ?>, '<?php echo $V->groupName; ?>')" ><span class="label label-success">删除</span></a>
                                                <a class="terminal_btn" href="./index.php?r=printor/lookterminal&groupID=<?php echo $V->groupID; ?>"><span class="label label-success">终端</span></a>
                                                <a class="advertisment_btn" href="./index.php?r=printor/lookadvertisment&groupID=<?php echo $V->groupID; ?>"><span class="label label-success">广告</span></a>
                                                <a class="version_btn" href="./index.php?r=printor/lookversion&groupID=<?php echo $V->groupID; ?>"><span class="label label-success">版本</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->

    </body>

</html>

