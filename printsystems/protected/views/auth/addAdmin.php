<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-3.0.3.min.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/prettify.css" type="text/css">
        <link rel="stylesheet" href="./css/oss/style.css" type="text/css">

        <script type="text/javascript" src="./css/multiselect/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/prettify.js"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
                letter-spacing: 0;
            }
            input{
                border:1px #f5f5f5 solid;
                padding: 5px 10px;
            }
            #jurisdiction-open{
                display: block;
            }
            #permission-assignment{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            input[type=text] {
                color:#37BCE5;
            }
            .error{
                color:red;
                margin-left: -20px;
                margin-top: 10px;
            }
        </style>
        <script type="text/javascript">

            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });

                $('#schoolName').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150
                });

                $('#machinename').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150
                });

                //学校改变  如果是管理部分终端，则需要修改终端显示。
                $('#schoolName').change(function() {
                    if ($('input:radio[name="check"]:checked').val() == "yes") {
                        var store_id = $("#schoolName").val();
                        changemachinename(store_id);
                    }
                });

                //是否是管理部分终端改变
                $("input[name='check']").change(function() {
                    if ($('input:radio[name="check"]:checked').val() == "yes") {
                        $("#machinename_div").css("display", "block");
                        var store_id = $("#schoolName").val();
                        changemachinename(store_id);
                    } else {
                        $("#machinename_div").css("display", "none");
                    }
                });

                function changemachinename(store_id) {
                    $.post("./index.php?r=auth/searchmachine", {store_id: store_id}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        $('#machinename').empty();
                        for (var i = 0; i < data.length; i++) {
                            $('#machinename').append($('<option></option>').text(data[i]['printorName']).val(data[i]['printorId']));
                            $('#machinename').multiselect('rebuild')
                        }
                    });
                }

                $("#add_btn").click(function() {
                    if ($("#username").val() == "" || $("#username").val() == null) {
                        relback();
                        $("#username_error").text("请输入用户名。");
                        return false;
                    } else if ($("#password").val() == "" || $("#password").val() == null) {
                        relback();
                        $("#password_error").text("请输入密码。");
                        return false;
                    } else if ($("#password").val().length < 6) {
                        relback();
                        $("#password_error").text("密码不能少于6位。");
                        return false;
                    } else if ($("#password").val().length > 12) {
                        relback();
                        $("#password_error").text("密码不能多于12位。");
                        return false;
                    } else if ($("#phone").val() == "" || $("#phone").val() == null) {
                        relback();
                        $("#phone_error").text("请输入联系电话。");
                        return false;
                    } else if ($("#QQ").val() == "" || $("#QQ").val() == null) {
                        relback();
                        $("#QQ_error").text("请输入QQ。");
                        return false;
                    } else if ($("#role").val() == 0) {
                        relback();
                        $("#role_error").text("请选择角色。");
                        return false;
                    } else if ($("#schoolName").val() == null) {
                        relback();
                        $("#schoolName_error").text("请选择管理学校。");
                        return false;
                    } else if ($('input:radio[name="check"]:checked').val() == "yes" && $("#machinename").val() == null) {
                        relback();
                        $("#machinename_error").text("请选择管理终端。");
                        return false;
                    } else {
                        relback();
                        if (confirm("确认保存？"))
                            adminform.submit();
                    }
                });

            });
            function relback() {
                $("#username_error").text("*");
                $("#password_error").text("*");
                $("#phone_error").text("*");
                $("#QQ_error").text("*");
                $("#role_error").text("*");
                $("#schoolName_error").text("*");
                $("#machinename_error").text("*");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>添加管理员</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=auth/assignRole">权限分配</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">添加</a>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <iframe style="display:none" name="test"></iframe>
                            <form class="form-horizontal" name="adminform" target="test" method="post">
                                <div class="form-group">
                                    <label for="username" class="col-sm-5 control-label">用户名：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="username" id="username" placeholder="请输入管理者昵称"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="username_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-5 control-label">密码：</label>
                                    <div class="col-sm-3">
                                        <input type="password" class="form-control"name="password" id="password" placeholder="请输入管理者密码"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="password_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-5 control-label">电话号码：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"name="phone" id="phone" placeholder="请输入联系电话"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="phone_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="QQ" class="col-sm-5 control-label">QQ：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"name="QQ" id="QQ" placeholder="请输入QQ"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="QQ_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="role" class="col-sm-5 control-label">角色：</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" id="role" name="role">
                                            <option value="0">----请选择----</option>
                                            <?php foreach ($role_info as $k => $l) { ?>
                                                <option value="<?php echo $l->roleId; ?>"><?php echo $l->rolename; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>   
                                    <div class="col-sm-3 error" id="role_error">  
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5 control-label"><label for="schoolName">管理学校:</label>    </div>
                                    <div class="col-sm-3"> 
                                        <select id="schoolName" name="schoolName[]" class="form-control" multiple="multiple" style="padding:0px">
                                            <?php foreach ($store_info as $k => $l) { ?>
                                                <option value="<?php echo $l->storeid; ?>"><?php echo $l->storename; ?></option>
                                            <?php } ?>
                                        </select>   
                                    </div>
                                    <div class="col-sm-3 star" id="schoolName_error">*</div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5 control-label"><label>是否管理部分终端:</label></div>
                                    <div class="col-sm-3"> 
                                        <label class="radio-inline">
                                            <input type="radio" name="check"  value="yes"> 是
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="check" checked="checked" value="no"> 否
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id ="machinename_div" style="display: none">
                                    <div class="col-sm-5 control-label"><label for="machinename">管理终端:</label>    </div>
                                    <div class="col-sm-3"> 
                                        <select id="machinename" name="machinename[]" class="form-control" multiple="multiple" style="padding:0px">
                                        </select>   
                                    </div>
                                    <div class="col-sm-3 star" id="machinename_error">*</div>
                                </div>
                                <div class="form-group" style="margin-top: 30px;">
                                    <div class="col-sm-offset-5 col-sm-3">
                                        <button type="button" id="add_btn" class="btn btn-info btn-block" style="width: 100%;outline:none;">保存</button>
                                        <lable class="save_lable"></lable>
                                    </div>
                                    <div class="col-sm-3 error" id="add_success">

                                    </div>
                                </div>
                            </form>
                        </div>
                    </DIV>          
                </DIV>   
                <br>
                <!-- FOOTER -->
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </DIV> 
        </DIV>
    </BODY>
</HTML>