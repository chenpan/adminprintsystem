<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #jurisdiction-open{
                display: block;
            }
            #roles-permissions{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#role').dataTable({
                    stateSave: true,
                    pagingType: "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定注销？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
//                $("#role .label.label-success").click(function () {
//                    var id = $(this).parent("td").find("input").val();
//                    if (confirm("确定删除？"))
//                    {
//                        $.post("./index.php?r=auth/deleteRole", {id: id}, function (data) {
//                            if (data.data == "success")
//                            {
//                                alert("删除成功！");
//                                window.location.href = "./index.php?r=auth/role";
//                            } else if (data.data == "false")
//                            {
//                                alert("删除失败！");
//                                window.location.href = "./index.php?r=auth/role";
//                            }
//                        }, 'json');
//                    }
//
//                });
                $("#createRole .btn.btn-primary").click(function() {
                    alert(ok);
                    if (confirm("确定创建？")) {
                        window.location.href = "./index.php?r=auth/addRole";
                    }
                });

            });
            function deleteRole(id) {
                if ('<?php echo $flagRole; ?>' == "true") {
                    // var id = $(this).parent("td").find("input").val();
                    if (confirm("确定删除？"))
                    {
                        $.post("./index.php?r=auth/deleteRole", {id: id}, function(data) {
                            if (data.data == "success")
                            {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=auth/role";
                            } else if (data.data == "false")
                            {
                                alert("删除失败！");
                                window.location.href = "./index.php?r=auth/role";
                            }
                        }, 'json');
                    }
                } else if ('<?php echo $flagRole; ?>' == "false") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>角色与权限</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>权限</li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=auth/role">角色与权限</a>
                        </li>
                    </ul>
                </div>         
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="createRole" >
                                <A href="./index.php?r=auth/addAuth" id="createAuth"><span class="btn btn-success btn-set">创建权限</span></A>
                                <A href="./index.php?r=auth/addRole" style="margin-left:2%"><span class="btn btn-success  btn-set">创建角色</span></A>
                            </div>    
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="no-margin" id="role">
                                <thead>
                                    <tr class="th">
                                        <th class="num col-lg-3" style="padding-left: 10px;">序号</th>
                                        <th class="name col-lg-5" >角色名称</th>
                                        <th class="action col-lg-4" >操作</th>
                                    </tr></thead>
                                <tbody>
                                    <?php foreach ($role_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td class="num" style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td class="name" style="padding-left: 13px;"><?php echo $V->rolename; ?></td>
                                            <td class="action">
                                                <A  style="text-decoration:none;" href="./index.php?r=auth/editRole&roleid=<?php echo $V->roleId; ?>"> <span class="label label-success">编辑</span></A>
                                                <A style="text-decoration:none;"href="#" onclick="deleteRole(<?php echo $V->roleId; ?>)"> <span  class="label label-success" style="cursor: pointer">删除</span>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

