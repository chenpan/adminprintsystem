<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 40px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #e6e6e6  solid;
                outline:none;
            }

            .content-wrap input[type="text"]{
                background-color: white;
                color:#00B9FF;
                border-radius: 3px;
                margin-left: 10px;
                border:1px #e6e6e6  solid;
                padding: 5px 10px;
                margin-top: 10px;
            }
            .content-wrap input[type="checkbox"]{
                width: 16px;
                height:16px;
                margin-top: 9px;
            }
            #jurisdiction-open{
                display: block;
            }
            #roles-permissions{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $("#logout").click(function () {
                    if (confirm("确定取消？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#logout1").click(function () {
                    if (confirm("确定取消？"))
                    {
                        window.location.href = "./index.php?r=auth/role";
                    }
                });
                $("#save").click(function () {
                    var j=0;
                       for (var i = 0; i < $(this).parent().parent().find(":checkbox").length; i++) {
                            if ($(this).parent().parent().find(":checkbox").eq(i).is(':checked')) {
                              j++;
                            }
                        }
                    if ($("#roleid").val() == "" || $("#roleid").val() == null)
                    {
                        alert("请输入角色名称！");
                        return false;
                    } else if (j == 0) {
                        alert("请勾选权限！");
                        return false;
                } else
                {
                    if (confirm("确认添加？")) {
                        roleform.submit();
                    }
                }
            });
            $(".authchild").click(function () {
                if ($(this).parent().find("input[type='checkbox'].authchild:checked").length > 0) {
                    $(this).parent().prev("td").find(".auth").prop("checked", true);
                } else {
                    $(this).parent().prev("td").find(".auth").removeAttr("checked");
                }
            });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>创建角色</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>权限</li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=auth/role">角色与权限</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=auth/addRole">创建角色</a>
                        </li>
                    </ul>
                </div>
                <form method="post" name="roleform" action="./index.php?r=auth/saveRole">
                    <div class="content-wrap">
                        <div class="row">
                            <div class="col-lg-12">
                                角色名称: <input type="text" name="role" class="text-blue " id="roleid"></div>
                        </div>
                    </div>
                    <div class="content-wrap">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="no-margin">
                                    <tbody>
                                        <?php
                                        $item_model = item::model();
                                        foreach ($authitem_info as $auth_info) {
                                            $auth_name = $auth_info->itemName;
                                            $id = $auth_info->itemId;
                                            $item_child = $item_model->findAll("parentItem = '$id'");
                                            ?>
                                            <tr style="padding-top:20px;padding-bottom:20px;line-height:30px;">
                                                <td style="text-align:center;font-weight:bold;width: 250px;"><input style="float:left;margin-left: 90px;" name="authparent[]" id="authparent[]" class="auth" type="checkbox" value="<?php echo $auth_name; ?>"><div style="float:left;margin-left: 8px;margin-top:2px;"><?php echo $auth_name . "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp"; ?></div></td>
                                                <td style="text-align:justify;">
                                                    <?php
                                                    foreach ($item_child as $child_info) {
                                                        $itemchild = $child_info->itemName;
                                                        ?>
                                                        <input style="float:left;" id="authchild[]" name="authchild[]" class="authchild" type="checkbox" value="<?php echo $itemchild; ?>"><div style="float:left;margin-left:5px;margin-top:2px;width: 110px;"><?php echo $itemchild . ""; ?></div>
                                                    <?php } ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div style="margin-top:30px;">
                                    <input name="save" id="save" type="button" value="保存" class="btn btn-primary" style="outline:none;width: 170px;">
                                    <input name="logout" id="logout1" type="button" value="取消" class="btn btn-primary" style="outline:none;width:110px;margin-left: 12px;">
                                </div>

                            </div>

                            <!--  / DEVICE MaNaGER -->
                            <!-- FOOTER -->

                            <div id="footer">
                                <div class="devider-footer-left"></div>
                                <div class="time">
                                    <p id="spanDate">
                                    <p id="clock">
                                </div>
                                <div class="copyright">Copyright © 2014-2015
                                    <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                            </div>
                            <!-- / END OF FOOTER -->
                        </div>
                </form>  
            </div>
            <!--  END OF PaPER WRaP -->
    </body>

</html>

