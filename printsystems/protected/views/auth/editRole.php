<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top:30px;
                padding-bottom: 40px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            .th{
                padding-bottom: 10px;
                padding-top: 10px;
            }
            .content-wrap input[type=text] {
                color:#00ACED;
                background-color:white;
                border:1px #f5f5f5 solid;
                border-radius: 3px;
                width: 200px;
                height: 30px;
                text-indent: 2em;
                margin-left: 15px;
            }
            .content-wrap input[type="checkbox"]{
                width: 16px;
                height:16px;
                margin-top: 9px;
            }
            #jurisdiction-open{
                display: block;
            }
            #roles-permissions{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            /*             .checkboxFour {
                            width: 20px;
                            height: 20px;
                            border: 1px #E5E5E5 solid;
                            border-radius: 100%;
                            position: relative;
                        }
                        .checkboxFour td {
                            width: 14px;
                            height: 14px;
                            border-radius: 100px;
            
                            -webkit-transition: all .5s ease;
                            -moz-transition: all .5s ease;
                            -o-transition: all .5s ease;
                            -ms-transition: all .5s ease;
                            transition: all .5s ease;
                            cursor: pointer;
                            position: static;
                            top: 2px;
                            left: 2px;
                            z-index: 1;
                            background-color:#FFFFFF;
                            border: 1px #65C91B solid;
                        }*/
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#logout").click(function () {
                    if (confirm("确定取消？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#save").click(function () {
                    if ($(".authchild").length == 0)
                    {
                        alert("请输入权限！");
                        return false;
                    } else
                    {
                        if (confirm("确认保存？")) {
                            roleform.submit();
                        }
                    }
                });
                $("#nosave").click(function () {
                    window.location.href = "./index.php?r=auth/role";
                });
                $(".authchild").click(function () {
                    if ($(this).parent().find("input[type='checkbox'].authchild:checked").length > 0) {
                        $(this).parent().prev("td").find(".auth").prop("checked", true);
                    } else {
                        $(this).parent().prev("td").find(".auth").attr("checked", false);
                    }
                });
            });



        </script>
    </head>   
    <body class="skin-blue sidebar-mini">
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>角色编辑</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>权限</li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=auth/role">角色与权限</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">编辑角色</a>
                        </li>
                    </ul>
                </div>   
                <iframe style="display:none" name="test"></iframe>
                <form method="post" name="roleform" target="test" action="./index.php?r=auth/editupdateRole">
                    <div class="content-wrap">
                        <div class="row">
                            <div class="col-lg-12">
                                角色名称: <input type="text" name="role" class="role" id="roleid" value="<?php echo $authrole_info->rolename ?>">
                            </div>
                        </div>
                    </div>
                    <div class="content-wrap">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="no-margin" >
                                    <tbody>

                                    <input type="hidden" name="roleid" value="<?php echo $roleid; ?>">
                                    <?php
                                    foreach ($assign_info as $auth_info) {
                                        $authitem_model = item::model();
                                        $item_name = $auth_info->itemName;
                                        $id = $auth_info->itemId;
                                        $child_info = $authitem_model->findAll("parentItem='$id'");
                                        $checked = false;
                                        $i = 0;
                                        if (isset($auth_name[$id][0])) {
                                            if ($auth_name[$id][0] == $item_name) {
                                                $checked = true;
                                            }
                                        }
                                        ?>
                                        <tr>  
                                            <td colspan="11">
                                                <hr/>
                                            </td>
                                        </tr>
                                        <tr style="line-height:30px;">
                                            <td style="text-align:right;font-weight:bold;width: 250px;" class="checkboxFour td"><input  style="float:left;margin-left: 90px;" name="authparent[]" class="auth" type="checkbox" 
                                                <?php
                                                if (isset($checked)) {
                                                    if ($checked) {
                                                        ?> checked="checked"<?php } ?><?php } ?>value="<?php echo $item_name; ?>">
                                                <div style="float:left;margin-left: 8px;margin-top:2px;font-size: 20px"> <?php echo $item_name . ""; ?>
                                                </div>
                                            </td>
                                            <td style="text-align:justify;">
                                                <?php
                                                foreach ($child_info as $v) {
                                                    $i++;
                                                    $child_name = $v->itemName;
                                                    $child_id = $v->itemId;
                                                    $checked = false;
                                                    if (isset($auth_name[$id][$child_id])) {
                                                        if ($auth_name[$id][$child_id] == $child_name) {
                                                            $checked = true;
                                                        }
                                                    }
                                                    ?>
                                                    <input  style="float:left;" name="authchild[]" class="authchild" type="checkbox" 
                                                    <?php
                                                    if (isset($checked)) {
                                                        if ($checked) {
                                                            ?> checked="checked"<?php
                                                                }
                                                            }
                                                            ?>
                                                            value="<?php echo $child_name; ?>"><div style="float:left;margin-left:5px;margin-top:2px;width: 110px;font-size: 15px"><?php echo $child_name . ""; ?></div>
                                                        <?php } ?>
                                                    <?php } ?>
                                        </td>
                                    </tr> 

                                    </tbody>
                                </table>
                                <div style="margin-top:30px;text-align: center">
                                    <input name="save" id="save" type="button" value="保存" class="btn btn-primary" style="outline:none;width: 170px;">
                                    <input name="nosave" id="nosave" type="reset" value="取消" class="btn btn-success" style="outline:none;width:110px;margin-left: 12px;">
                                    <lable class="save_lable"></lable>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>  
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

