<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #jurisdiction-open{
                display: block;
            }
            #permission-assignment{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                var table = $('#alreadytable').dataTable({
                    "pagingType": "input",
                    "Processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "order": [[1, "asc"]],
                    "stateSave": false,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "",
                        "Processing": "正在加载...",
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定注销？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
//                $(".save").click(function() {
//                    var adminID = $(this).next(".saveid").val();
//                    console.log(adminID);
//                    window.location.href = "./index.php?r=auth/modify&adminID=" + adminID;
//                });
//                $(".del").click(function() {
//                    if (confirm("确定删除？")) {
//                        var adminId = $(this).next(".del").val();
//                        $.post("./index.php?r=auth/assignDel", {adminId: adminId}, function(data) {
//                            if (data.data == "success")
//                            {
//                                alert("删除成功！");
//                                window.location.href = "./index.php?r=auth/assignRole";
//                            } else if (data.data == "false")
//                            {
//                                alert("删除失败！");
//                                window.location.href = "./index.php?r=auth/assignRole";
//                            }
//                        }, 'json');
//                    }
//                });
            });
            function editrole(id){
                    window.location.href = "./index.php?r=auth/modify&adminID=" + id;
            }
            function delrole(id){
                if (confirm("确定删除？")) {
                        $.post("./index.php?r=auth/assignDel", {adminId: id}, function(data) {
                            if (data.data == "success")
                            {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=auth/assignRole";
                            } else if (data.data == "false")
                            {
                                alert("删除失败！");
                                window.location.href = "./index.php?r=auth/assignRole";
                            }
                        }, 'json');
                    }
            }
//            function auto() {
//                var left = ($("td.action").width() - $("select.form-control").outerWidth()) / 2;
//                $("select.form-control").css('margin-left', left - 10);
//            }
        </script>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>权限分配</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>权限</li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=platform/auth/role">权限分配</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <DIV id="createUser" >
                                <A href="./index.php?r=auth/addAdmin" id="createUser"><span class="btn btn-success btn-set">添加管理员</span></A>
                            </DIV>    
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="alreadytable">
                                <thead>
                                    <tr class="th">
                                        <th>序列</th>
                                        <th>用户</th>
                                        <th>角色</th>
                                        <th>学校</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($admin_info as $K => $V) {
                                        ?>
                                        <TR>
                                            <td><?php echo $K + 1; ?></td>
                                            <td>
                                                <?php echo $V->username; ?>
                                            </td>
                                            <td class="action">
                                                <div>
                                                    <?php foreach ($role_info as $k => $l) {
                                                        ?>
                                                        <?php
                                                        if ($l->roleId == $V->_roleid) {
                                                            echo $l->rolename;
                                                        }
                                                        ?>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                            <td class="action">
                                                <?php
                                                $store_model = store::model();
                                                $storeIds = explode(',', $V->_storeid); //ID 1,2,3,4
                                                $storename = "";
                                                foreach ($storeIds as $k => $l) {
                                                    $store_infos = $store_model->findByPk($l);
                                                    if (count($store_infos) != 0)
                                                        $storename .= $store_model->findByPk($l)->storename . ",";
                                                }
                                                $storename = substr($storename, 0, -1);
                                                echo $storename;
//                                                foreach ($store_info as $key => $v) {
//                                                    if ($v->storeid == $V->_storeid) {
//                                                        echo $v->storename;
//                                                    }
//                                                }
                                                ?>
                                            </td>
                                            <td class="action">
                                                <a style="text-decoration: none;cursor: pointer;" onclick="editrole(<?php echo $V->administratorid; ?>)">
                                                    <span class="label label-success">编辑</span>
                                                </a>
                                                <a style="text-decoration: none;cursor: pointer;" onclick="delrole(<?php echo $V->administratorid; ?>)">
                                                    <span class="label label-success">删除</span>
                                                </a>
                                            </td>
                                        </TR>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </DIV>
                    </DIV>   
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

