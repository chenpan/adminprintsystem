<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #advertisement-management{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            #advertisement-open{
                display: block;
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#grouptable').dataTable({
                    stateSave: true,
                    pagingType: "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
            });
            function deleteadsdelivery(advertisementID, groupID) {
                var advertisementID = advertisementID;
                if (confirm("确认删除 ？"))
                {
                    $.post("./index.php?r=advertisements/deladsdelivery", {advertisementID: advertisementID, groupID: groupID}, function(data) {
                        if (data.data == "success")
                        {
                            alert("删除成功！");
                            window.location.href = "./index.php?r=advertisements/lookgroup&advertisementID=" + advertisementID;
                        } else if (data.data == "false")
                        {
                            alert("删除失败！");
                        }
                    }, 'json');
                }

            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>所投放终端分组--<span font-weight: bold"><?php echo $advertisementname; ?></span> </H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>广告
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=advertisements/advertisementInfo">广告管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=advertisements/lookgroup&advertisementID=<?php echo $advertisementID; ?>">分组管理</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="grouptable">
                                <thead>
                                    <tr class="th">
                                        <th  style="padding-left: 10px;">序列</th>
                                        <th>组名</th>
                                        <th>组描述</th>
                                        <th>所属学校</th>
                                        <th>每日开始时间</th>
                                        <th>每日结束时间</th>
                                        <th>开始日期</th>
                                        <th>结束日期</th>
                                        <th>每日播放次数</th>
                                        <th>添加时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($group_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td  style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->groupName; ?></td>
                                            <td><?php echo $V->groupDetail; ?></td>
                                            <td><?php echo store::model()->find("storeid = $V->_storeid")->storename; ?></td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $advertisementID and _groupID = $V->groupID")->starttime;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $advertisementID and _groupID = $V->groupID")->endtime;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $advertisementID and _groupID = $V->groupID")->startDate;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $advertisementID and _groupID = $V->groupID")->endDate;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $advertisementID and _groupID = $V->groupID")->times;
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo adsdelivery::model()->find("_advertisementID = $advertisementID and _groupID = $V->groupID")->addTime;
                                                ?>
                                            </td>
                                            <td>
                                                <a class="edit_btn" href="./index.php?r=advertisements/editadsdelivery&groupID=<?php echo $V->groupID; ?>&advertisementID=<?php echo $advertisementID; ?>"><span class="label label-success">编辑</span></a>
                                                <a class="delete_btn" href="#" onclick="deleteadsdelivery(<?php echo $advertisementID; ?>,<?php echo $V->groupID; ?>)" ><span class="label label-success">删除</span></a>
                                                    <!--<a class="terminal_btn" href="./index.php?r=printor/lookterminal&groupID=<?php echo $V->groupID; ?>"><span class="label label-success">终端</span></a>-->
                                                    <!--<a class="advertisment_btn" href="./index.php?r=printor/lookadvertisment&groupID=<?php echo $V->groupID; ?>"><span class="label label-success">广告</span></a>-->
                                                    <!--<a class="version_btn" href="./index.php?r=printor/lookversion&groupID=<?php echo $V->groupID; ?>"><span class="label label-success">版本</span></a>-->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->

    </body>

</html>

