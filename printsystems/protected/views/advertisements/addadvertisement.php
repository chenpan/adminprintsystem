<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-3.0.3.min.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/prettify.css" type="text/css">
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <script type="text/javascript" src="./css/multiselect/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/prettify.js"></script>

        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            .error{
                color:red;
                margin-top:7px;
                margin-left: -20px;
            }
            .laydate-icon{
                width:100%;
            }
            /*            #advertisement-management{
                            background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
                        }*/
            #advertisement-open{
                display: block;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#adGroup').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                if (<?php echo $groupID; ?> != 0) {
                    $("#advertisement-open").css("display", "none");
                    $("#terminal-open").css("display", "block");
                }
                else {
                    $("#advertisement-open").css("display", "block");
                    $("#terminal-open").css("display", "none");
                }
                $('#adGroup').multiselect('select', [<?php echo $groupID; ?>]);
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>新增广告</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>广告
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=advertisements/advertisementInfo">广告管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">新增广告</a>
                        </li>
                    </ul>
                </div>

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <iframe style="display:none" name="test"></iframe>
                            <form class="form-horizontal" id="advertisement_form" name="advertisement_form" method="post" target="test" action="./index.php?r=advertisements/addadvertisements" enctype="multipart/form-data">
                                <!--                                <div class="form-group">
                                                                    <label for="file" class="col-sm-5 control-label">广告文件：</label>
                                                                    <div class="col-sm-3">
                                                                        <input type="file" id="file" name="file" style="margin-top: 7px;outline:none;"/>
                                                                    </div>   
                                                                    <div class="col-sm-3 error" id="file_error">
                                                                        *
                                                                    </div>
                                                                </div>-->
                                <div class="form-group">
                                    <label for="advertisementname" class="col-sm-5 control-label">广告名称：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="advertisementname" name="advertisementname" placeholder="请输入广告名称"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="advertisementname_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="advertisementLength" class="col-sm-5 control-label">广告时长：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="advertisementLength" name="advertisementLength" placeholder="请输入广告时长"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="advertisementLength_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="startdate" class="col-sm-5 control-label">开始日期：</label>
                                    <div class="col-sm-3">
                                        <input type="text" id="startdate" style="color:#79B4DC;margin-top:3px;" name="startdate" class="laydate-icon form-control" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})" >
                                    </div>   
                                    <div class="col-sm-3 error" id="startdate_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="enddate" class="col-sm-5 control-label">结束日期：</label>
                                    <div class="col-sm-3">
                                        <input type="text" id="enddate" name="enddate" style="color:#79B4DC;margin-top:3px;" class="laydate-icon form-control" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    </div>   
                                    <div class="col-sm-3 error" id="enddate_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="starttime" class="col-sm-5 control-label">每日开始时间：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="starttime" name="starttime" placeholder="时间格式:08:15:15"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="starttime_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="endtime" class="col-sm-5 control-label">每日结束时间：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="endtime" name="endtime" placeholder="时间格式:20:59:59"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="endtime_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5 control-label"><label for="vesionGroup">投放终端组:</label></div>
                                    <div class="col-sm-3"> 
                                        <select id="adGroup" name="adGroup[]" class="form-control" multiple="multiple" style="padding:0px">
                                            <?php
                                            foreach ($group_info as $k => $l) {
                                                ?>
                                                <option value="<?php echo $l->groupID; ?>"><?php echo $l->groupName; ?></option>
                                            <?php } ?>
                                        </select>   
                                    </div>
                                    <div class="col-sm-3 star" id="group_error">*</div>
                                </div>
                                <!--                                <div class="form-group">
                                                                    <label for="group" class="col-sm-5 control-label">投放终端组：</label>
                                                                    <div class="col-sm-3">
                                                                        <select class="form-control" id="group" name="group">
                                                                            <option value="0">--请选择--</option>
                                <?php
                                foreach ($group_info as $k => $l) {
                                    ?>
                                                                                                            <option value="<?php echo $l->groupID; ?>"><?php echo $l->groupName; ?></option>
                                <?php } ?>
                                                                        </select>
                                                                    </div>   
                                                                    <div class="col-sm-3 error" id="group_error">
                                                                        *
                                                                    </div>
                                                                </div>-->
                                <div class="form-group">
                                    <label for="company" class="col-sm-5 control-label">所属公司：</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" id="company" name="company">
                                            <option value="0">--请选择--</option>
                                            <?php
                                            foreach ($company_info as $k => $l) {
                                                ?>
                                                <option value="<?php echo $l->companyID; ?>"><?php echo $l->companyName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>   
                                    <div class="col-sm-3 error" id="company_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="times" class="col-sm-5 control-label">每日播放次数：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="times" name="times" placeholder="请输入每日播放次数"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="times_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5 control-label"> <label for="versionfile">版本文件:</label>    </div>
                                    <div class="col-sm-3">
                                        <div id="ossfile">你的浏览器不支持flash,Silverlight或者HTML5！</div>
                                        <div id="container">
                                            <a id="selectfiles" class="btn btn-info" style="width:100%;" href="javascript:void(0);">选择文件</a>
                                        </div>  
                                        <p>&nbsp;</p>
                                                <!--<input type="file" id="versionfile" name="versionfile" style="outline:none;margin-top: 7px;">-->
                                    </div>
                                    <div class="col-sm-3 star" style="margin-top: 13px;" id="version-file"><pre id="console" style="border:0px;background-color: #FFFFFF"></pre></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5"></div>
                                    <div class="col-sm-3"> 
                                        <a id="postfiles" class="btn btn-success" style="width:100%;" href="javascript:void(0);">保存</a>
                                        <!--<button class="btn btn-info" type="button" id="save"  style="width:100%;">保存</button>-->
                                    </div>
                                    <div class="col-sm-3 star"><span id="info"></span></div>
                                </div>
                                <!--                                <div class="form-group">
                                                                    <div class="col-sm-offset-5 col-sm-3">
                                                                        <button type="button" id="add_btn" class="btn btn-info btn-block">保存</button>
                                                                    </div>
                                                                    <div class="col-sm-3 error" id="add_success">
                                
                                                                    </div>
                                                                </div>-->
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->
    </body>
    <script type="text/javascript" src="./css/oss/lib/plupload-2.1.2/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="./css/oss/upload_ad_1.js"></script>
</html>

