<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-3.0.3.min.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/prettify.css" type="text/css">
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <script type="text/javascript" src="./css/multiselect/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/prettify.js"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            .error{
                color:red;
                margin-top:7px;
                margin-left: -20px;
            }
            .laydate-icon{
                width:100%;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });

                $('#group').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150,
                    checkAllText: "全选",
                    uncheckAllText: "全不选",
                    noneSelectedText: "没有数据",
                    selectedList: 100
                });
                $('#group').multiselect('select', [<?php echo $group; ?>]);
                $("#company").val(<?php echo $advertisement_info->_companyID; ?>);
                if (<?php echo $id; ?> != 0) {
                    $("#advertisement-open").css("display", "none");
                    $("#terminal-open").css("display", "block");
                }
                else {
                    $("#advertisement-open").css("display", "block");
                    $("#terminal-open").css("display", "none");
                }

                $("#add_btn").click(function() {
                    var advertisementName = $("#advertisementName").val();
                    var company = $("#company").val();
                    var advertisementLength = $("#advertisementLength").val().replace(/\s+/g, "");


                    if (advertisementName.length == 0)
                    {
                        reback();
                        $("#advertisementName_error").text("请输入广告名称！");
                        return false;
                    }
                    if (advertisementLength.length == 0)
                    {
                        reback();
                        $("#advertisementLength_error").text("请输入广告时长！");
                        return false;
                    }
                    if (company == 0)
                    {
                        reback();
                        $("#company_error").text("请选择一个公司！");
                        return false;
                    }
                    reback();
                    if (confirm("确定保存？"))
                    {
                        advertisement_form.submit();
                    }
                });
            });
            function reback() {
                $("#advertisementName_error").text("*");
                $("#advertisementLength_error").text("*");
                $("#company_error").text("*");


                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>编辑广告--<span style="font-weight: bold"><?php echo $advertisement_info->advertisementName . '.' . $advertisement_info->advertisementFormat; ?></span></H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>广告
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">编辑广告</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <iframe style="display:none" name="test"></iframe>
                            <form class="form-horizontal" id="advertisement_form" name="advertisement_form" target="test" method="post" action="./index.php?r=advertisements/editad" enctype="multipart/form-data">
                                <input type="hidden" class="form-control"  name="advertisementID"  value="<?php echo $advertisement_info->advertisementID; ?>"/>
                                <input type="hidden" class="form-control"  name="companyID"  value="<?php echo $advertisement_info->_companyID; ?>"/>
                                <div class="form-group">
                                    <label for="advertisementName" class="col-sm-5 control-label">广告名称：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="advertisementName" name="advertisementName" value="<?php echo $advertisement_info->advertisementName; ?>" placeholder="请输入广告时长"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="advertisementName_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="advertisementLength" class="col-sm-5 control-label">广告时长：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="advertisementLength" name="advertisementLength" value="<?php echo $advertisement_info->advertisementLength; ?>" placeholder="请输入广告时长"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="advertisementLength_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5 control-label"><label for="vesionGroup">投放终端组:</label></div>
                                    <div class="col-sm-3"> 
                                        <select id="group" name="group[]" class="form-control" multiple="multiple" style="padding:0px">
                                            <?php
                                            foreach ($group_info as $k => $l) {
                                                ?>
                                                <option value="<?php echo $l->groupID; ?>"><?php echo $l->groupName; ?></option>
                                            <?php } ?>
                                        </select>   
                                    </div>
                                    <div class="col-sm-3 error" id="version-group_error">*</div>
                                </div>
                                <div class="form-group">
                                    <label for="company" class="col-sm-5 control-label">所属公司：</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" id="company" name="company">
                                            <option value="0">--请选择--</option>
                                            <?php foreach ($company_info as $k => $l) { ?>
                                                <option value="<?php echo $l->companyID; ?>"><?php echo $l->companyName; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>   
                                    <div class="col-sm-3 error" id="company_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-3">
                                        <button type="button" id="add_btn" class="btn btn-info btn-block">保存</button>
                                    </div>
                                    <div class="col-sm-3 error" id="add_success">

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

