<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #advertisement-management{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            #advertisement-open{
                display: block;
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#adtable').dataTable({
                    stateSave: true,
                     pagingType: "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });

                $("#addads").click(function() {
                    window.location.href = "./index.php?r=advertisements/addadvertisement";
                });
                
                if ('<?php echo $addAds; ?>' == "hidden") {
                    $("#addads").parent().parent().parent().hide();
                }
                
            });
            function deleteAds(advertisementID, companyID) {
                if ('<?php echo $deleteAds; ?>' == "") {
                    var advertisementID = advertisementID;
                    if (confirm("确认删除 ？"))
                    {
                        $.post("./index.php?r=advertisements/delads", {advertisementID: advertisementID}, function(data) {
                            if (data.data == "success")
                            {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=advertisements/advertisementInfo&companyID=" + companyID;
                            } else if (data.data == "false")
                            {
                                alert("删除失败！");
                            }
                        }, 'json');
                    }
                } else if ('<?php echo $deleteAds; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
            function downloads(advertisementID) {
                window.location.href = "./index.php?r=advertisements/downloadadvertisements&advertisementID=" + advertisementID;
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>广告信息</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>广告
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">广告管理</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addads" value="新增广告">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="adtable">
                                <thead>
                                    <tr class="th">
                                        <th  style="padding-left: 10px;">序列</th>
                                        <th>广告名称</th>
                                        <th>广告时长</th>
                                        <th>所属公司</th>
                                        <th>投放分组</th>
                                        <th>加入时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($advertisement_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td  style="padding-left: 10px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->advertisementName . '.' . $V->advertisementFormat; ?></td>
                                            <td><?php echo $V->advertisementLength . "秒"; ?></td>
                                            <td>
                                                <?php
                                                $company_info = company::model()->find("companyID = $V->_companyID");
                                                if (count($company_info) != 0) {
                                                    echo $company_info->companyName;
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $adsdelivery_model = adsdelivery::model();
                                                $group_model = group::model();
                                                $adsdelivery_info = $adsdelivery_model->findAll("_advertisementID = $V->advertisementID");
                                                $groupnames = "";
                                                if (count($adsdelivery_info) != 0) {
                                                    foreach ($adsdelivery_info as $key => $value) {
                                                        $group_info = $group_model->findByPk($value->_groupID);
                                                        if (count($group_info) != 0) {
                                                            $groupnames = $groupnames . $group_info->groupName . ",";
                                                        }
                                                    }
                                                }
                                                $groupnames = substr($groupnames, 0, -1);
                                                echo $groupnames;
                                                ?>
                                            </td>
                                            <td><?php echo $V->addTime; ?></td>
                                            <td>
                                                <a class="edit_btn" <?php echo $editAds; ?> href="./index.php?r=advertisements/editAds&advertisementID=<?php echo $V->advertisementID; ?>"><span class="label label-success">编辑</span></a>
                                                <a class="delete_btn" <?php echo $deleteAds; ?> href="#" onclick="deleteAds(<?php echo $V->advertisementID; ?>,<?php echo company::model()->find("companyID = $V->_companyID")->companyID; ?>)" ><span class="label label-success">删除</span></a>
                                                <a href="#" onclick="downloads(<?php echo $V->advertisementID; ?>)"><span class="label label-success">下载</span></a>
                                                <a class="group_btn" href="./index.php?r=advertisements/lookgroup&advertisementID=<?php echo $V->advertisementID; ?>"><span class="label label-success">分组</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

