<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #company-management{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
            #advertisement-open{
                display: block;
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#companytable').dataTable({
                    stateSave: true,
                    pagingType: "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });

                $("#addcompany").click(function() {
                    window.location.href = "./index.php?r=advertisements/addcompany";
                });

                if ('<?php echo $addCompany; ?>' == "hidden") {
                    $("#addcompany").parent().parent().parent().hide();
                }
            });
            function deleteCompany(ID, name) {
                if ('<?php echo $deleteCompany; ?>' == "") {
                    if (confirm("确认删除 " + name + " 公司?"))
                    {
                        $.post("./index.php?r=advertisements/deleteCompany", {ID: ID}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                alert("删除成功");
                                window.location.href = "./index.php?r=advertisements/companyInfo";
                            } else if (data.data == "false")
                            {
                                alert("删除失败！");
                            } else if (data.data == "no")
                            {
                                alert("此公司不存在！");
                            }

                        });
                    }
                } else if ('<?php echo $deleteCompany; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>公司管理</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>广告
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=advertisements/companyInfo">公司管理</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addcompany" value="新增公司">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="companytable">
                                <thead>
                                    <tr class="th">
                                        <th style="padding-left: 10px;">序列</th>
                                        <th>公司名称</th>
                                        <th>联系人</th>
                                        <th>联系电话</th>
                                        <th>公司地址</th>
                                        <th>加入时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($company_info as $K => $V) {
                                        ?>
                                        <tr>
                                            <td  style="padding-left: 13px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->companyName; ?></td>
                                            <td><?php echo $V->linkMan; ?></td>
                                            <td><?php echo $V->companyPhone; ?></td>
                                            <td><?php echo $V->companyAddress; ?></td>
                                            <td><?php echo $V->addTime; ?></td>
                                            <td>
                                                <a class="edit_btn" <?php echo $editCompany; ?> href="./index.php?r=advertisements/editcompany&companyID=<?php echo $V->companyID; ?>"><span class="label label-success">编辑</span></a>
                                                <a class="delete_btn" <?php echo $deleteCompany; ?> href="#" onclick="deleteCompany(<?php echo $V->companyID; ?>, '<?php echo $V->companyName; ?>')" ><span class="label label-success">删除</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

