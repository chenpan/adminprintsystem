<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            .error{
                 color:red;
                margin-top:7px;
                margin-left: -20px;
            }
            #company-management{
               background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
             #advertisement-open{
                display: block;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定注销？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#add_btn").click(function() {
                    var name = $("#name").val().replace(/\s+/g, "");
                    var man = $("#man").val().replace(/\s+/g, "");
                    var phone = $("#phone").val().replace(/\s+/g, "");
                    var address = $("#address").val().replace(/\s+/g, "");


                    if (name.length == 0)
                    {
                        reback();
                        $("#name_error").text("请输入公司名称！");
                        return false;
                    }
                     if (man.length == 0)
                    {
                        reback();
                        $("#man_error").text("请输入公司联系人！");
                        return false;
                    }
                     if (phone.length == 0)
                    {
                        reback();
                        $("#phone_error").text("请输入公司联系电话！");
                        return false;
                    }
                     if (address.length == 0)
                    {
                        reback();
                        $("#address_error").text("请输入公司地址！");
                        return false;
                    }
                    reback();
                    if (confirm("确定保存？"))
                    {
                        $.post("./index.php?r=advertisements/editCompanys", {companyID:<?php echo $company_info->companyID; ?>, name: name, man: man, phone: phone, address: address}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "false")
                            {
                                reback();
                                $("#add_success").text("保存失败！");
                            }
                            else if (data.data == "success")
                            {
                                reback();
                                $("#add_success").text("保存成功！");
                            }
                            else if (data.data == "exist")
                            {
                                reback();
                                $("#name_error").text("此公司名称已存在！");
                            }
                        });
                    }
                });
            });
            function reback() {
                $("#name_error").text("*");
                $("#man_error").text("*");
                $("#phone_error").text("*");
                $("#address_error").text("*");
                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>编辑公司</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>广告
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=advertisements/companyInfo">公司管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">编辑公司</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">公司名称：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="name" value="<?php echo $company_info->companyName; ?>" placeholder="请输入公司名称"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="name_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">公司联系人：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="man" value="<?php echo $company_info->linkMan; ?>" placeholder="请输入公司联系人"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="man_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">联系电话：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="phone" value="<?php echo $company_info->companyPhone; ?>" placeholder="请输入联系电话"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="phone_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-5 control-label">公司地址：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="address" value="<?php echo $company_info->companyAddress; ?>" placeholder="请输入公司地址"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="address_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-3">
                                        <button type="button" id="add_btn" class="btn btn-info btn-block">保存</button>
                                    </div>
                                    <div class="col-sm-3 error" id="add_success">

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

