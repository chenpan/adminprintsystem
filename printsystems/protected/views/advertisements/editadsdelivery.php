<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-3.0.3.min.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="./css/multiselect/css/prettify.css" type="text/css">
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <script type="text/javascript" src="./css/multiselect/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="./css/multiselect/js/prettify.js"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            /*            #advertisement-management{
                            background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
                        }*/
            /*            #advertisement-open{
                            display: block;
                        }*/
            .error{
                color:red;
                margin-top:7px;
                margin-left: -20px;
            }
            .laydate-icon{
                width:100%;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });

                $('#group').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    maxHeight: 150,
                    checkAllText: "全选",
                    uncheckAllText: "全不选",
                    noneSelectedText: "没有数据",
                    selectedList: 100
                });
                $('#group').multiselect('select', [<?php echo "'" . $groupID . "'"; ?>]);
                $("#company").val(<?php echo $advertisement_info->_companyID; ?>);

                if (<?php echo $id; ?> != 0) {
                    $("#terminal-open").css("display", "block");
                }
                else {
                    $("#advertisement-open").css("display", "block");
                }

                $("#add_btn").click(function() {
                    var startdate = $("#startdate").val().replace(/\s+/g, "");
                    var enddate = $("#enddate").val().replace(/\s+/g, "");
                    var starttime = $("#starttime").val().replace(/\s+/g, "");
                    var endtime = $("#endtime").val().replace(/\s+/g, "");
                    var group = $("#group").val();
                    var times = $("#times").val().replace(/\s+/g, "");



                    if (startdate.length == 0)
                    {
                        reback();
                        $("#startdate_error").text("请输入开始日期！");
                        return false;
                    }
                    if (enddate.length == 0)
                    {
                        reback();
                        $("#enddate_error").text("请输入结束日期！");
                        return false;
                    }
                    if (starttime.length == 0)
                    {
                        reback();
                        $("#starttime_error").text("请输入开始时间！");
                        return false;
                    }
                    if (endtime.length == 0)
                    {
                        reback();
                        $("#endtime_error").text("请输入结束时间！");
                        return false;
                    }
                    if (group == 0)
                    {
                        reback();
                        $("#group_error").text("请选择一个分组！");
                        return false;
                    }
                    if (times.length == 0)
                    {
                        reback();
                        $("#times_error").text("请输入每日播放次数！");
                        return false;
                    }
                    reback();
                    if (confirm("确定保存？"))
                    {
                        advertisement_form.submit();
                    }
                });
            });
            function reback() {
                $("#startdate_error").text("*");
                $("#enddate_error").text("*");
                $("#starttime_error").text("*");
                $("#endtime_error").text("*");
                $("#group_error").text("*");
                $("#times_error").text("*");

                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>编辑广告播放策略--<span style="font-weight: bold"><?php echo $advertisement_info->advertisementName . '.' . $advertisement_info->advertisementFormat; ?></H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>广告
                        </li>
                        <?php if ($id == 0) { ?>
                            <li><i class="fa fa-lg fa-angle-right"></i>
                            </li>
                            <li><a href="./index.php?r=advertisements/advertisementInfo">广告管理</a>
                            </li>
                            <li><i class="fa fa-lg fa-angle-right"></i>
                            </li>
                            <li><a href="./index.php?r=advertisements/lookgroup&advertisementID=<?php echo $advertisement_info->advertisementID; ?>">分组管理</a>
                            </li>
                            <li><i class="fa fa-lg fa-angle-right"></i>
                            </li>
                        <?php } else { ?>
                            <li><i class="fa fa-lg fa-angle-right"></i>
                            </li>
                            <li>终端
                            </li>
                            <li><i class="fa fa-lg fa-angle-right"></i>
                            </li>
                            <li><a href="./index.php?r=printor/group">终端分组</a>
                            </li>
                            <li><i class="fa fa-lg fa-angle-right"></i>
                            </li>
                            <li><a href="./index.php?r=printor/lookadvertisment&groupID=<?php echo $groupID; ?>">广告管理</a>
                            </li>
                        <?php } ?>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">编辑广告播放策略</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <iframe style="display:none" name="test"></iframe>
                            <form class="form-horizontal" id="advertisement_form" name="advertisement_form" target="test" method="post" action="./index.php?r=advertisements/editadsdeliverys" enctype="multipart/form-data">
                                <input type="hidden" class="form-control"  name="advertisementID"  value="<?php echo $advertisement_info->advertisementID; ?>"/>
                                <input type="hidden" class="form-control"  name="companyID"  value="<?php echo $advertisement_info->_companyID; ?>"/>
                                <div class="form-group">
                                    <label for="startdate" class="col-sm-5 control-label">开始日期：</label>
                                    <div class="col-sm-3">
                                        <input type="text" id="startdate" style="color:#79B4DC;" name="startdate" class="form-control" value="<?php echo $adsdelivery_info->startDate; ?>" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    </div>   
                                    <div class="col-sm-3 error" id="startdate_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="enddate" class="col-sm-5 control-label">结束日期：</label>
                                    <div class="col-sm-3">
                                        <input type="text" id="enddate" name="enddate" style="color:#79B4DC;" class="form-control" value="<?php echo $adsdelivery_info->endDate; ?>" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})">
                                    </div>   
                                    <div class="col-sm-3 error" id="enddate_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="starttime" class="col-sm-5 control-label">每日开始时间：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="starttime" name="starttime" value="<?php echo $adsdelivery_info->starttime; ?>" placeholder="时间格式:08:15:15"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="starttime_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="endtime" class="col-sm-5 control-label">每日结束时间：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="endtime" name="endtime" value="<?php echo $adsdelivery_info->endtime; ?>" placeholder="时间格式:20:59:59"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="endtime_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5 control-label"><label for="vesionGroup">投放终端组:</label></div>
                                    <div class="col-sm-3"> 
                                        <select id="group" name="group[]" class="form-control" multiple="multiple" style="padding:0px">
                                            <?php
                                            foreach ($group_info as $k => $l) {
                                                ?>
                                                <option value="<?php echo $l->groupID; ?>"><?php echo $l->groupName; ?></option>
                                            <?php } ?>
                                        </select>   
                                    </div>
                                    <div class="col-sm-3 error" id="version-group_error">*</div>
                                </div>
                                <div class="form-group">
                                    <label for="times" class="col-sm-5 control-label">每日播放次数：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="times" name="times" value="<?php echo $adsdelivery_info->times; ?>" placeholder="请输入每日播放次数"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="times_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-3">
                                        <button type="button" id="add_btn" class="btn btn-info btn-block">保存</button>
                                    </div>
                                    <div class="col-sm-3 error" id="add_success">

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

