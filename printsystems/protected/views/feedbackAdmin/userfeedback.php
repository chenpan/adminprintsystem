<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            .th{
                padding-bottom: 10px;
                padding-top: 10px;
            }
            select {
                border: 1px solid #e6e6e6;
                height: 40px;
                margin-left: -1px;
                width: 164px;
                cursor: pointer;
                border-radius: 3px;
            }
            .modal-body input[type="text"] {
                color: #666;
                height: 30px;
                width: 500px;
                text-indent: 5px;
                border: 1px solid #e6e6e6;
                border-radius: 3px;
            }
            textarea {
                border: 1px solid #e6e6e6;
                height: 110px;
                width: 500px;
            }
            .Msg{
                color: #C62F2F;
            }
            #feedback-open{
                display: block;
            }
            #feedback-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            a{
                color:#9EA7B3;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#feedbacktable').dataTable({
                    "serverSide": true,
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "ajax": './index.php?r=feedbackAdmin/userfeedbackAjax',
                    "stateSave": false,
                    "paginate": true,
                    "pagingType": "input",
                    "order": [6, 'desc'],
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    },
                    'columns': [
                        {"data": "id", "orderble": true, "searchable": true},
                        {"data": "phone", "orderble": true, "searchable": true},
                        {"data": "storename", "orderble": true, "searchable": true},
                        {"data": "content", "orderble": true, "searchable": true},
                        {"data": "email", "orderble": true, "searchable": true},
                        {"data": "url", "orderble": true, "searchable": true},
                        {"data": "addtime", "orderble": true, "searchable": true},
                        {"data": "statue", "orderble": true, "searchable": true},
                        {"data": "feedbackid",
                            "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                                if (oData.statue != "已处理")
                                    $(nTd).html("<a href='javascript:void(0);' onclick='edit(" + oData.feedbackid + ")'><span class='label label-success' style='cursor:pointer'>标为已处理</span></a>");
                                else
                                    $(nTd).html(" ");
                            }
                        }
                    ]
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
            });
            function edit(feedbackid)
            {
                if (confirm("确定标为已处理？")) {
                    $.post("./index.php?r=feedbackAdmin/edit", {feedbackid: feedbackid}, function(datainfo) {
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "success") {
                            alert("保存成功");
                            window.location.href = "./index.php?r=feedbackAdmin/userfeedback";
                        }else{
                            alert("保存失败");
                        }
                    });
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>

        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">

                <!-- CONTENT -->


                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>用户反馈</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=feedbackAdmin/userfeedback">用户反馈</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="feedbacktable">
                                <thead>
                                    <tr>
                                        <th>序列</th>
                                        <th>电话</th>
                                        <th>学校</th>
                                        <th>反馈内容</th>
                                        <th>邮箱</th>
                                        <th>url</th>
                                        <th>反馈时间</th>
                                        <th>状态</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

