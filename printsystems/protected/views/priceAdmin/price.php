<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            select {
                border: 1px solid #e6e6e6;
                height: 40px;
                margin-left: -1px;
                width: 164px;
                cursor: pointer;
                border-radius: 3px;
            }
            .modal-body input[type="text"] {
                color: #666;
                height: 30px;
                width: 300px;
                text-indent: 5px;
                border: 1px solid #e6e6e6;
                border-radius: 3px;
            }
            textarea {
                border: 1px solid #e6e6e6;
                height: 110px;
                width: 500px;
            }
            .Msg{
                color: #C62F2F;
            }
            #price-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            input[type="text"]{
                color: #66afe9;
            }
            .th {
                border-bottom:10px solid #F5F5F5;
                height: 45px;
                padding-top: 10px;
            }
            .th1 {
                border-bottom:10px solid #FFFFFF;
                height: 45px;
                padding-top: 10px;
            }
            #schooldel{
                cursor:pointer;
            }
            /*            select{
                            border: 1px solid #e6e6e6 !important;
                            border-radius: 3px;
                        }
                        .col-sm-9 {
                            margin-left: -8px;
                        }
                        hr{width: 106%;}*/
        </style>
        <!--<script src="./platform/js/share.js" type="text/javascript"></script>-->
        <script type="text/javascript">
            $(function() {
                var table = $('#pricetable').dataTable({
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "pagingType": "input",
                    "paginate": true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到相关数据",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "",
                        "processing": "正在加载中..."
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#closeprice").click(function() {
                    $('#Msg').text("");
                });
                $("#myFoot").on('click', '#save', function() {
                    if ('<?php echo $flagEdit; ?>' == "") {
                        var priceId = $("#priceId").val();
                        var A4 = $("#A4").val();
                        var A42b = $("#A42b").val();
                        var A3 = $("#A3").val();
                        var scan = $("#scan").val();
                        var color = $("#color").val();
                        var copy = $("#copy").val();
                        if (A4 < 0 || A42b < 0 || A3 < 0 || scan < 0 || color < 0 || copy < 0) {
                            $('#Msg').text("价格不能为负！");
                            return;
                        } else {
                            $('#A4Msg').text("*");
                        }
                        if (confirm("确认保存？")) {
                            $.post("./index.php?r=priceAdmin/editprices", {priceId: priceId, A4: A4, A42b: A42b, A3: A3, scan: scan, color: color, copy: copy}, function(datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == 200) {
                                    alert("保存成功!");
                                    window.location.reload();
                                } else if (data.data == 401) {
                                    alert("保存失败!");
                                }
                            });
                        }
                    } else {
                        window.location.href = './index.php?r=nonPrivilege/index';
                    }
                });
            });
            function changPrice(id) {
                var priceId = id;
                var priceId = $("#" + priceId);
                var storename = priceId.next().next().text();
                var a4Id = priceId.next().next().next();
                var a4 = a4Id.text();
                var a42b = a4Id.next().text();
                var a3 = a4Id.next().next().text();
                var scan = a4Id.next().next().next().text();
                var color = a4Id.next().next().next().next().text();
                var copy = a4Id.next().next().next().next().next().text();
                var type = a4Id.next().next().next().next().next().next().text();
                $("#myModalLabel").text(storename + type);
                $("#priceId").val(id);
                $("#A4").val(a4);
                $("#A42b").val(a42b);
                $("#A3").val(a3);
                $("#scan").val(scan);
                $("#color").val(color);
                $("#copy").val(copy);
                $("#type").val(type);
                $("#myModal").modal("show");
                // 分享文档
            }
            function delSchool(id) {
                if ('<?php echo $flagDel; ?>' == "") {
                    var priceId = id;
                    if (confirm("确定删除")) {
                        $.post("./index.php?r=priceAdmin/delSchool", {priceId: priceId}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == 200) {
                                window.location.reload();
                            } else if (data.data == 401) {
                                alert("删除失败!");
                            }
                        });
                    }
                } else if ('<?php echo $flagDel; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }

            /**
             * 编辑数据带出值
             */
        </script>
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>价格列表</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=priceAdmin/price">价格</a>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap">               
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="pricetable" width="100%;" style="font-size: 14px;">
                                <thead>
                                    <tr class="th">
                                        <th style="display:none">priceId</th>
                                        <!--<th style="display:none">storeId</th>-->
                                        <th>序列</th>
                                        <th>学校</th>
                                        <th>A4单面(张)</th>
                                        <th>A4双面(张)</th>
                                        <th>A3单面(张)</th>
                                        <th>扫描(张)</th>
                                        <th>彩色(张)</th>
                                        <th>复印(张)</th>
                                        <th>类型</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($price_info as $K => $V) { ?>
                                        <tr class="th1">
                                            <th style="display:none" id="<?php echo $V->priceid; ?>"><?php echo $V->priceid; ?></th>
                                            <!--<th style="display:none" id="<?php echo $V->storeid; ?>"><?php echo $V->storeid; ?></th>-->
                                            <th style="padding-left: 13px;"><?php echo $K + 1; ?></th>
                                            <th class="storename"><?php
                                                $store_model = store::model();
                                                $store_info = $store_model->findByPk($V->storeid);
                                                if (count($store_info) != 0) {
                                                    echo $store_info->storename;
                                                } else {
                                                    echo "无";
                                                }
                                                ?></th>
                                            <th class="a4"><?php echo $V->one_side_A4; ?></th>
                                            <th class="a42b"><?php echo $V->two_side_A4; ?></th>
                                            <th class="a3"><?php echo $V->one_side_A3; ?></th>
                                            <th class="scan"><?php echo $V->scan_price; ?></th>
                                            <th class="color"><?php echo $V->color_price; ?></th>
                                            <th class="copy"><?php echo $V->copy_price; ?></th>
                                            <th class="copy"><?php
                                            if ($V->type == 1) {
                                                echo '线上';
                                            } else if ($V->type == 2) {
                                                echo '线下';
                                            };
                                                ?>

                                            </th>
                                            <th><a style="text-decoration: none;"  <?php echo $flagEdit; ?> id="priceedit" class="priceedit" href="#pricemodal" onclick="changPrice(<?php echo $V->priceid; ?>)">
                                                    <span class="label label-success">编辑</span>
                                                </a>
                                                <a style="text-decoration: none;"  <?php echo $flagDel; ?> id="schooldel" class="schooldel" onclick="delSchool(<?php echo $V->priceid; ?>)">
                                                    <span class="label label-success">删除</span>
                                                </a>
                                            </th>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">x</span></button>
                                        <h4 class="modal-title" id="myModalLabel"align="center"></h4>
                                    </div>
                                    <div class="modal-body" id="myBody" >
                                        <div class="form-group">
                                            <input type="hidden" id="priceId"/>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="A4">A4单(张)：</label> 
                                            <input type="text" id="A4" name="A4"/>
                                            <span class="Msg" id="A4Msg">*</span>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="A42b">A4双(张)：</label> 
                                            <input type="text" id="A42b" name="A42b"value=""/>
                                            <span class="Msg" id="A42bMsg">*</span>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="A3">A3单(张)：</label> 
                                            <input type="text" id="A3" name="A3"value=""/>
                                            <span class="Msg" id="A3Msg">*</span>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="scan">扫 描(张)：</label> 
                                            <input type="text" id="scan" name="scan"/>
                                            <span class="Msg" id="scanMsg">*</span>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="color">彩 印(张)：</label> 
                                            <input type="text" id="color" name="color"/>
                                            <span class="Msg" id="colorMsg">*</span>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label" for="copy">复 印(张)：</label> 
                                            <input type="text" id="copy" name="copy"/>
                                            <span class="Msg" id="copyMsg">*</span>
                                        </div>
                                    </div>
                                    <div class="modal-footer"id="myFoot"align="center">
                                        <button type="button" class="btn btn-primary"align="center" id="save">保存</button>
                                        <button type="button" class="btn btn-default" align="center"data-dismiss="modal" id="closeprice">关闭</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    </div>
                    <!-- / END OF FOOTER -->
                </div>
                <br>
            </div>
        </div>

    </body>
</html>
