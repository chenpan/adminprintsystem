<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            label{text-align: right;}
            #price-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#sure").click(function () {
                    if ($("#school").val() == "" || $("#school").val() == null)
                    {
                        reback();
                        $("#schoolname-info").text("请输入学校名称！");
                        return false;
                    } else if ($("#A4").val() == "" || $("#A4").val() == null && !jg.test($("#A4").val()))
                    {
                        reback();
                        $("#A4-info").text("请正确输入价格！");
                        return false;
                    } else if ($("#A42b").val() == "" || $("#A42b").val() == null && !jg.test($("#A42b").val()))
                    {
                        reback();
                        $("#A42b-info").text("请正确输入价格！");
                        return false;
                    } else if ($("#A3").val() == "" || $("#A3").val() == null && !jg.test($("#A3").val()))
                    {
                        reback();
                        $("#A43-info").text("请正确输入价格！");
                        return false;
                    } else if ($("#scan").val() == "" || $("#scan").val() == null && !jg.test($("#scan").val()))
                    {
                        reback();
                        $("#scan-info").text("请正确输入价格！");
                        return false;
                    } else if ($("#color").val() == "" || $("#color").val() == null && !jg.test($("#color").val()))
                    {
                        reback();
                        $("#color-info").text("请正确输入价格！");
                        return false;
                    } else if ($("#copy").val() == "" || $("#copy").val() == null && !jg.test($("#color").val()))
                    {
                        reback();
                        $("#copy-info").text("请正确输入价格！");
                        return false;
                    } else
                    {
                        $("#schoolname-info,#A4-info,#A42b-info,#scan-info,#color-info,#copy-info,#A3-info").text("");
                        if (confirm("确认保存？"))
                            addschoolform.submit();
                    }
                });
                $("#type").val(1);
            });
            function onSelect() {
                var type = document.getElementById("type");
                var index = type.selectedIndex; // 选中索引
                var typevalue = type.options[index].value; // 选中值

                var school = document.getElementById("school");
                school.innerHTML = "";
                $.post("./index.php?r=priceAdmin/addSchoolAjax", {value: typevalue}, function (datainfo) {
                    var data = eval("(" + datainfo + ")");
                    for (i = 0; i < data.length; i++) {
                        var storeId = data[i].storeId;
                        var storename = data[i].storeName;
                        var opt = document.createElement("OPTION");
                        opt.value = storeId;
                        opt.text = storename;
                        opt.selected = false;
                        school.add(opt);
                    }
                });
            }
            function reback() {
                $("#schoolname-info,#schoolusername-info,#schoolpassword-info,#phone-info,#QQ-info,#schoollogo-info,#province").text("*");

                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>添加价格策略</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=priceAdmin/price">价格</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=priceAdmin/addschool">添加价格策略</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="resetUser" class="form-group" style="text-align: center" >
                                <iframe style="display:none" name="test"></iframe>
                                <form class="form-horizontal" name="addschoolform" id="addschoolform" target="test" method="post">
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label" > <label for="type">类型：</label></div>
                                        <div class="col-sm-3"><select id="type" name="type" class="form-control" onchange="onSelect()">
                                                    <option value="1">线上</option>
                                                    <option value="2">线下</option>
                                            </select>  </div>
                                        <div class="col-sm-3 star" id="schoolname-info">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"> <label for="schoolname">学校名称：</label></div>
                                        <div class="col-sm-3"><select id="school" name="school" class="form-control">
                                                <?php foreach ($store_info as $k => $l) { ?>
                                                    <option value="<?php echo $l->storeid; ?>"><?php echo $l->storename; ?></option>
                                                <?php } ?>    
                                            </select>  </div>
                                        <div class="col-sm-3 star" id="schoolname-info">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"> <label for="A4">A4价格：</label></div>
                                        <div class="col-sm-3"><input type="text" id="A4" name ="A4" class="form-control" placeholder="输入A4价格(只输数字,单位为元)"></div>
                                        <div class="col-sm-3 star" id="A4-info">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"> <label for="A42b">A4双面：</label></div>
                                        <div class="col-sm-3"><input type="text" id="A42b" name ="A42b" class="form-control"placeholder="输入A4双面价格(只输数字,单位为元)"></div>
                                        <div class="col-sm-3 star" id="A42b-info">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"> <label for="A3">A3价格：</label></div>
                                        <div class="col-sm-3"><input type="text"  id="A3" name ="A3" class="form-control"placeholder="输入A3价格(只输数字,单位为元)"></div>
                                        <div class="col-sm-3 star" id="A3-info">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"> <label for="scan">扫描价格：</label></div>
                                        <div class="col-sm-3"><input type="text"  id="scan" name ="scan" class="form-control"placeholder="输入扫描价格(只输数字,单位为元)"></div>
                                        <div class="col-sm-3 star" id="scan-info">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"> <label for="color">彩印价格：</label></div>
                                        <div class="col-sm-3"><input type="text"  id="color" name ="color" class="form-control"placeholder="输入彩印价格(只输数字,单位为元)"></div>
                                        <div class="col-sm-3 star" id="color-info">*</div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-5 control-label"> <label for="copy">复印价格：</label></div>
                                        <div class="col-sm-3"><input type="text"  id="copy" name ="copy" class="form-control"placeholder="输入复印价格(只输数字,单位为元)"></div>
                                        <div class="col-sm-3 star" id="copy-info">*</div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <div class="col-sm-5"></div>
                                        <div class="col-sm-3"> <button type="button" id="sure" class="btn btn-info btn-block" style="width: 100%;outline:none;">保存</button></div>
                                        <div class="col-sm-3" style="margin-top:5px;"><lable class="save_lable"></lable><span id="add_success"></span></div>
                                    </div>

                                </form>  
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->
    </body>

</html>

