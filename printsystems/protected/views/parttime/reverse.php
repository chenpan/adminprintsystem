<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .modal-body input[type="text"] {
                color: #666;
                height: 30px;
                width: 300px;
                text-indent: 5px;
                border: 1px solid #e6e6e6;
                border-radius: 3px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #job-management{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);   
            }
            #job-open{
                display: block;
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#pluralitytable').dataTable({
                    stateSave: true,
                    pagingType: "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });

                //编辑状态保存
                $("#save").click(function() {
                    var integralDetailsId = $("#integralDetailsId").val();
                    var statue = $('input:radio[name=check]:checked').val();
                    var rechargedescript = $("#rechargedescript").val();
                    if (confirm("确认保存？")) {
                        $.post("./index.php?r=parttime/editsatue", {integralDetailsId: integralDetailsId, statue: statue, rechargedescript: rechargedescript}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                alert("保存成功!");
                                window.location.reload();
                            } else if (data.data == "fail") {
                                alert("保存失败!");
                            }
                        });
                    }
                });
            });
            //弹出框赋值
            function changstatue(integralDetailsId, statue, rechargedescript) {
                $("#integralDetailsId").val(integralDetailsId);
                if (statue == 1) {//未完全冲账
                    $("input[name='check'][value=section]").attr("checked", true);
                } else if (statue == 2) {//冲账完成
                    $("input[name='check'][value=all]").attr("checked", true);
                }
                $("#rechargedescript").val(rechargedescript);
                $("#reversemodal").modal("show");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>兼职冲账</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">兼职冲账</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="pluralitytable">
                                <thead>
                                    <tr class="th">
                                        <th  style="padding-left: 10px;">序号</th>
                                        <th>用户名</th>
                                        <th>手机号码</th>
                                        <th>充值积分</th>
                                        <th>充值时间</th>
                                        <th>冲账状态</th>
                                        <th>描述</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($integralDetails_info as $K => $V) {
                                        ?>
                                        <tr>

                                            <td  style="padding-left: 10px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo user::model()->findByPk($V->_userid)->username; ?></td>
                                            <td> 
                                                <?php echo user::model()->findByPk($V->_userid)->phone; ?>
                                            </td>
                                            <td><?php echo $V->addIntegral; ?></td>
                                            <td><?php echo $V->happentime; ?> </td>
                                            <td><?php
                                                if ($V->statue == 1)
                                                    echo "未冲账完";
                                                else
                                                    echo "已冲账完";
                                                ?> </td>
                                            <td><?php echo $V->rechargedescript; ?></td>
                                            <td>
                                                <a class="edit_btn" href="#" onclick="changstatue(<?php echo $V->integralDetailsId; ?>,<?php echo $V->statue; ?>, '<?php echo $V->rechargedescript; ?>')"> <span class ="label label-success">编辑</span></a>                                               
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <div class="modal fade" id="reversemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">x</span></button>
                                <h4 class="modal-title" id="myModalLabel"align="center">兼职冲账</h4>
                            </div>
                            <div class="modal-body" id="myBody" >
                                <div class="form-group">
                                    <input type="text" id="integralDetailsId" name="integralDetailsId" style="display:none"/>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">冲账状态：</label> 
                                    <label class="radio-inline">
                                        <input type="radio" name="check" value="section"> 部分
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="check" value="all"> 全部
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="form-label" for="rechargedescript">详细描述：</label> 
                                    <textarea rows="3" style="width: 450px;max-width:450px;max-height:200px;" id="rechargedescript" name="rechargedescript"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer"id="myFoot"align="center">
                                <button type="button" class="btn btn-primary"align="center" id="save">保存</button>
                                <button type="button" class="btn btn-default" align="center"data-dismiss="modal">关闭</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

