<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>

        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }

            //checkbox 
            .checkboxFour {
                width: 20px;
                height: 20px;
                border: 1px #E5E5E5 solid;
                border-radius: 100%;
                position: relative;
            }
            .checkboxFour label {
                width: 14px;
                height: 14px;
                border-radius: 100px;

                -webkit-transition: all .5s ease;
                -moz-transition: all .5s ease;
                -o-transition: all .5s ease;
                -ms-transition: all .5s ease;
                transition: all .5s ease;
                cursor: pointer;
                position: static;
                top: 2px;
                left: 2px;
                z-index: 1;
                background-color:#FFFFFF;
                border: 1px #65C91B solid;
            }
            input[type=checkbox] {
                visibility: hidden;
            }
            #job-examine{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            #job-open{
                display: block;
            }
            .btn-pass{
                background-color:#65C3DF!important;
                color: white!important;
                width: 86px;
                border: 1px solid #65C3DF!important;
            }
            .btn-pass:hover{
                 background-color:#4CB8D8!important;
                color: white!important;
                border: 1px solid #4CB8D8!important;
            }
           .btn-dete{
                background-color: #55C6F1!important;
                color: white!important;
                width: 86px;
                border: 1px solid #55C6F1!important;
            }
            .btn-dete:hover{
                background-color:#3CBDEF!important;
                color: white!important;
                border: 1px solid #3CBDEF!important;
            }
            #select_all{
                color:#9ea7b3;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#auditetable').dataTable({
                    stateSave: true,
                    pagingType: "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    },
                    columnDefs: [{
                            orderable: false, //禁用排序
                            targets: [0, 0]   //指定的列
                        }]
                });

                $("#auditetable tr td .checkboxFour").click(function() {
                    if ($(this).find(":checkbox").prop("checked")) {//取消选择
                        $(this).find(":checkbox").removeAttr("checked");
                        $(this).find("label").css("background-color", "#FFFFFF");
                    } else {//选择
                        $(this).find(":checkbox").prop("checked", true);
                        $(this).find("label").css("background-color", "#65C91B");
                    }
                });
                //通过
                $("#passSelected").click(function() {
                    var ids = "";
                    var checked = $("#auditetable input:checked");
                    checked.each(function() {
                        ids += $(this).val() + ",";
                    });


                    ids = ids.substr(0, ids.length - 1);
                    if (ids.length == 0) {
                        alert("请选择人员！");
                        return false;
                    }
                    if (confirm("确认通过？"))
                    {
                        $.post("./index.php?r=partTime/passSelected", {ids: ids}, function(data) {
                            if (data.data == "success")
                            {
                                alert("保存成功！");
                                window.location.href = "./index.php?r=partTime/audite";
                            } else if (data.data == "false")
                            {
                                alert("保存失败！");
                            }
                        }, 'json');
                    }
                });
                $("#outSelected").click(function() {
                    var ids = "";
                    var checked = $("#auditetable input:checked");
                    checked.each(function() {
                        ids += $(this).val() + ",";
                    });
                    ids = ids.substr(0, ids.length - 1);
                    if (ids.length == 0) {
                        alert("请选择人员！");
                        return false;
                    }
                    if (confirm("确认淘汰？"))
                    {
                        $.post("./index.php?r=partTime/outSelected", {ids: ids}, function(data) {
                            if (data.data == "success")
                            {
                                alert("保存成功！");
                                window.location.href = "./index.php?r=partTime/audite";
                            } else if (data.data == "false")
                            {
                                alert("保存失败！");
                            }
                        }, 'json');
                    }
                });

                //全选
                $("#select_all").click(function() {
                    if ($("#auditetable").find(":checkbox").eq(0).is(':checked'))
                    {
                        for (var i = 0; i < $("#auditetable").find(":checkbox").length; i++)
                        {
                            $("#auditetable").find(":checkbox").eq(i).removeAttr("checked");
                            $("#auditetable").find("label").eq(i).css("background-color", "#FFFFFF");
                        }
                    } else
                    {
                        for (var i = 0; i < $("#auditetable").find(":checkbox").length; i++)
                        {
                            $("#auditetable").find(":checkbox").eq(i).prop("checked", true);
                            $("#auditetable").find("label").eq(i).css("background-color", "#65C91B");
                        }
                    }
                });


                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                if ('<?php echo $parttimePass; ?>' == "false") {
                    $("#passSelected").hide();
                }
                if ('<?php echo $parttimeDeny; ?>' == "false") {
                    $("#outSelected").hide();
                }

            });

        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>审核兼职人员</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>兼职
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">审核兼职人员</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="auditetable">
                                <thead>
                                    <tr class="th">
                                        <th><a href="#" id="select_all">全选</a></th>
                                        <th>序号</th>
                                        <th>姓名</th>
                                        <th>手机号码</th>
                                        <th>兼职类型</th>
                                        <th>申请时间</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($plurality_info as $K => $V)
                                    {
                                        ?>
                                        <tr>
                                            <td>
                                                <div class="checkboxFour">
                                                    <input type='checkbox'  value= '<?php echo $V->pluralityId; ?>'>
                                                    <label for="checkboxFourInput"></label>
                                                </div>
                                            </td>

                                            <td><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->pluralityName; ?></td>
                                            <td> 
                                                <?php echo $V->phone; ?>
                                            </td>
                                            <td><?php
                                                if ($V->pluralityType == 0)
                                                {
                                                    echo "按日";
                                                }
                                                else if ($V->pluralityType == 1)
                                                {
                                                    echo "按月";
                                                }
                                                else if ($V->pluralityType == 2)
                                                {
                                                    echo "按次";
                                                }
                                                ?></td>
                                            <td><?php echo $V->applyTime; ?> </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-12 btnlist">
                            <button type="button" class="btn btn-info btn-pass" id="passSelected">通过</button>
                            <button type="button" class="btn btn-success btn-dete" id="outSelected">淘汰</button>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

