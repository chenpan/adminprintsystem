<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            #job-management{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);   
            }
            #job-open{
                display: block;
            }
            .btn-set{
                background-color:#76B8E6 !important;
                color:white!important;
            }
            .btn-set:hover{
                background-color:#56AFEC!important;
                color:white!important;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#pluralitytable').dataTable({
                    stateSave: true,
                    pagingType: "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#addPlurality").click(function() {
                    window.location.href = "./index.php?r=parttime/addPlurality";
                });

                if ('<?php echo $addParttime; ?>' == "hidden") {
                    $("#addPlurality").parent().parent().parent().hide();
                }
               
            });
            function editPlurality(pluralityId) {//
                window.location.href = "./index.php?r=partTime/editPlurality&pluralityId=" + pluralityId;
            }
            function delPlurality(pluralityId) {
                if ('<?php echo $deleteParttime; ?>' == "") {
                    if (confirm("确定删除？"))
                    {
                        $.post("./index.php?r=partTime/delPlurality", {pluralityId: pluralityId}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success")
                            {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=partTime/partTimeInfo";
                            } else if (data.data == "false")
                            {
                                alert("删除失败！");
                            }
                        });
                    }
                } else if ('<?php echo $deleteParttime; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>兼职管理</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">兼职管理</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addPlurality" value="新增兼职人员">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="pluralitytable">
                                <thead>
                                    <tr class="th">
                                        <th  style="padding-left: 10px;">序号</th>
                                        <th>姓名</th>
                                        <th>手机号码</th>
                                        <th>兼职类型</th>
                                        <th>所属学校</th>
                                        <th>入职时间</th>
                                        <th>离职时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($plurality_info as $K => $V) {
                                        ?>
                                        <tr>

                                            <td  style="padding-left: 10px;"><?php echo $K + 1; ?></td>
                                            <td><?php echo $V->pluralityName; ?></td>
                                            <td> 
                                                <?php echo $V->phone; ?>
                                            </td>
                                            <td><?php
                                                if ($V->pluralityType == 0) {
                                                    echo "按日";
                                                } else if ($V->pluralityType == 1) {
                                                    echo "按月";
                                                } else if ($V->pluralityType == 2) {
                                                    echo "按次";
                                                }
                                                ?></td>
                                            <td><?php echo store::model()->find("storeid = $V->_storeid")->storename; ?> </td>
                                            <td><?php echo $V->addTime; ?> </td>
                                            <td><?php echo $V->leaveTime == "0000-00-00" ? "" : $V->leaveTime; ?></td>
                                            <td>
                                                <a class="edit_btn" <?php echo $editParttime; ?> href="./index.php?r=partTime/editPlurality&pluralityId=<?php echo $V->pluralityId; ?>"><span class="label label-success">编辑</span></a>
                                                <a class="delete_btn" <?php echo $deleteParttime; ?> href="#" id="<?php echo $V->pluralityId; ?>" onclick="delPlurality(<?php echo $V->pluralityId; ?>)"><span class="label label-success">删除</span></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

