<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <script src="./css/laydate/laydate.js" type="text/javascript"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            .error{
                color:red;
                margin-left: -20px;
                margin-top: 10px;
            }
            #job-management{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            #job-open{
                display: block;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#pluralitytable').dataTable({
                    stateSave: true,
                    pagingType: "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#addPlurality").click(function() {
                    var addName = $("#addName").val().replace(/\s+/g, "");
                    var addPhone = $("#addPhone").val().replace(/\s+/g, "");
                    var addType = $("#addType").val();
                    var school = $("#school").val();
                    var addTime = $("#addTime").val();
                    if (addName.length == 0) {
                        reback();
                        $("#addName_error").text("请输入姓名！");
                        return false;
                    } else if (addPhone.length == 0) {
                        reback();
                        $("#addPhone_error").text("请输入手机号码！");
                        return false;
                    } else if (!/^(13[0-9]|14[0-9]|15[0-9]|17[0-9]|18[0-9])\d{8}$/i.test(addPhone)) {
                        reback();
                        $("#addPhone_error").text("手机号码格式不对！");
                        return false;
                    } else if (school == "0") {
                        reback();
                        $("#school_error").text("请选择所属学校！");
                        return false;
                    } else if (addType == "s") {
                        reback();
                        $("#addType_error").text("请选择兼职类型！");
                        return false;
                    } else {
                        reback();
                        if (confirm("确认保存？"))
                        {
                            reback();
                            $.post("./index.php?r=partTime/addPlurality", {addName: addName, addPhone: addPhone, addType: addType, school: school,addTime:addTime}, function(datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "false")
                                {
                                    reback();
                                    $("#add_success").text("添加失败！");
                                }
                                else if (data.data == "success")
                                {
                                    reback();
                                    $("#add_success").text("添加成功！");
                                    $("#addName").val("");
                                    $("#addPhone").val("");
                                    $("#addType").val("s");
                                    $("#school").val("0");
                                }
                            });
                        }
                    }
                });
            });
            function reback() {
                $("#addName_error").text("*");
                $("#addPhone_error").text("*");
                $("#addType_error").text("*");

                $("#add_success").text("");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>新增兼职人员</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>兼职
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=parttime/partTimeInfo">兼职管理</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">添加兼职人员</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal" id="plurality_form" name="plurality_form" method="post" action="./index.php?r=partTime/addPlurality" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="addName" class="col-sm-5 control-label">姓名：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="addName" name="addName" placeholder="请输入姓名"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="addName_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="addPhone" class="col-sm-5 control-label">手机号：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="addPhone" name="addPhone" placeholder="请输入手机号"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="addPhone_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5 control-label"><label for="school">所属学校:</label></div>
                                    <div class="col-sm-3"> 
                                        <select id="school" name="school" class="form-control">
                                            <option value="0">******请选择******</option>
                                            <?php foreach ($store_info as $k => $l) {
                                                ?>
                                                <option value="<?php echo $l->storeid; ?>"><?php echo $l->storename; ?></option>
                                            <?php } ?>    
                                        </select></div>
                                    <div class="col-sm-3 star" id="school_error">*</div>
                                </div> 
                                <div class="form-group">
                                    <label for="addType" class="col-sm-5 control-label">兼职类型：</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" id="addType" name="addType">
                                            <option value="s">******请选择******</option>
                                            <option value ="0">按日</option>
                                            <option value ="1">按月</option>
                                            <option value="2">按次</option>
                                        </select>
                                    </div>   
                                    <div class="col-sm-3 error" id="addType_error">
                                        *
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label for="addTime" class="col-sm-5 control-label">入职时间：</label>
                                    <div class="col-sm-3">
                                        <input type="text" style="color:#79B4DC;width: 100%;margin-top:4px;text-indent:1.3em;height: 26px;"id="addTime" name="addTime"class = "laydate-icon" onclick="laydate({istime: true, format: 'YYYY-MM-DD'})"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="addPhone_error">

                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 30px;">
                                    <div class="col-sm-offset-5 col-sm-3">
                                        <button type="button" id="addPlurality" class="btn btn-info btn-block"  style="width: 100%;outline:none;">保存</button>
                                    </div>
                                    <div class="col-sm-3 error" id="add_success">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

