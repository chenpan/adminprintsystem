<!DOCTYPE html>
<html>
    <head>
        <META content="IE=11.0000" http-equiv="X-UA-Compatible">
        <META charset="UTF-8">    
        <META name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"> 
        <TITLE>重庆颇闰科技-后台管理系统</TITLE>   
        <!-- Le styles -->
        <?php echo $recommend; ?>
        <script src="./css/bootstrap/highcharts.js" type="text/javascript"></script>

        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            #breadcrumb {
                width: 96.5%;
                margin-left:auto;
                margin-right: auto;
            }
            #index-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            table tr:hover{
                background-color: transparent;
            }
            table tr{
                border-bottom: 1px #F1F1F1 solid;
            }
            .content-wrap {
                padding: 0 30px;
                border-radius: 3px;
                margin-bottom:20px; 
            }
            .speed{
                margin-top: 30px;
            }
        </style>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
        <!-- END OF RIGHT SLIDER CONTENT-->
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                var optionIncome = ({
                    chart: {type: 'line',
                        renderTo: 'incomecharts'},
                    title: {text: '近七天收入统计(共<?php echo $totalIncome_dayss; ?>元)'},
                    xAxis: {categories:
                                [
<?php
foreach ($dayArray as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                ]},
                    yAxis: {title: {text: '收入量（元）'}},
                    plotOptions: {
                        line: {dataLabels: {enabled: true}}},
                    series: [{
                            name: '近七天收入统计',
                            data: [
<?php
foreach ($dayArray as $k => $l) {
    echo $l["totalIncome_day"] . ",";
}
?>
                            ]
                        },
                        {
                            name: '近七天收入(积分)统计',
                            data: [
<?php
foreach ($dayArray as $k => $l) {
    echo $l["totalIncome_points_day"] . ",";
}
?>
                            ],
                            color: '#20C8B1'
                        }
                    ]
                });
                var optionsOrder = ({
                    chart: {type: 'line',
                        renderTo: 'ordercharts'},
                    title: {text: '近七天订单统计(共<?php echo $totalCount_dayss; ?>单)'},
                    xAxis: {categories:
                                [
<?php
foreach ($dayOrder as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                ]},
                    yAxis: {title: {text: '订单量（元）'}},
                    plotOptions: {
                        line: {dataLabels: {enabled: true}}},
                    series: [{
                            name: '近七天web端订单统计',
                            data: [
<?php
foreach ($dayOrder as $k => $l) {
    echo $l["totalCount_days_web"] . ",";
}
?>
                            ],
                            color: '#20C8B1'
                        }, {
                            name: '近七天终端订单统计',
                            data: [
<?php
foreach ($dayOrder as $k => $l) {
    echo $l["totalCount_days_terminal"] . ",";
}
?>
                            ]
                        },
                        {
                            name: '近七天终端退款订单统计',
                            data: [
<?php
foreach ($dayOrder as $k => $l) {
    echo $l["totalCount_refund_terminal"] . ",";
}
?>
                            ],
                            color: '#FF4040'
                        },
                        {
                            name: '近七天WEB端退款订单统计',
                            data: [
<?php
foreach ($dayOrder as $k => $l) {
    echo $l["totalCount_refund_web"] . ",";
}
?>
                            ],
                            color: '#FFB5C5'
                        }

                    ]
                });
                var optionsRegister = ({
                    chart: {type: 'line',
                        renderTo: 'registercharts'
                    },
                    title: {text: '近七天注册人数统计(共<?php echo $regCount_dayss; ?>人)'},
                    xAxis: {categories:
                                [
<?php
foreach ($dayReg as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                ]},
                    yAxis: {title: {text: '注册人数'}},
                    plotOptions: {
                        line: {dataLabels: {enabled: true}}},
                    series: [{
                            name: '近七天注册人数统计',
                            data: [
<?php
foreach ($dayReg as $k => $l) {
    echo $l["regCount_days"] . ",";
}
?>
                            ]
                        }]
                });
                var optionsRefund = ({
                    chart: {type: 'line',
                        renderTo: 'refundcharts'
                    },
                    title: {text: '近七天退款统计(共<?php echo $totalrefund_dayss; ?>元)'},
                    xAxis: {
                        categories:
                                [
<?php
foreach ($dayRefund as $k => $l) {
    echo "'" . $l['day'], "'" . ",";
}
?>
                                ]
                    },
                    yAxis: {title: {text: '退款统计'}},
                    plotOptions: {
                        line: {dataLabels: {enabled: true}}},
                    series: [{
                            name: '近七天退款统计',
                            data: [
<?php
foreach ($dayRefund as $k => $l) {
    echo $l["totalrefund_days"] . ",";
}
?>
                            ]
                        }]
                });
                var optionsrefundrate_web = ({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        renderTo: 'refundratechartsweb_count'
                    },
                    title: {text: 'WEB端订单昨日退款率'},
                    tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
                    plotOptions: {
                        pie: {allowPointSelect: true, cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                            }
                        }
                    }, series: [{
                            type: 'pie',
                            name: '所占百分比',
                            data: [
                                {name: '退款率', y: <?php echo $refundrateCount_web; ?>, color: '#FF4040'},
                                {name: '未退款率', y: <?php echo $refundrateCount_web_total - $refundrateCount_web; ?>, sliced: true, selected: true, color: '#66CDAA'}
                            ]
                        }]
                });
                var optionsrefundrate_ternminal = ({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        renderTo: 'refundratechartsterminal_count'
                    },
                    title: {text: '终端订单昨日退款率'},
                    tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
                    plotOptions: {
                        pie: {allowPointSelect: true, cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                            }
                        }
                    }, series: [{
                            type: 'pie',
                            name: '所占百分比',
                            data: [
//                                            ['退款率', <?php echo $refundrateCount_terminal; ?>, color:'#FF4040'],
                                {name: '退款率', y: <?php echo $refundrateCount_terminal; ?>, color: '#FF4040'},
                                {name: '未退款率', y: <?php echo $refundrateCount_terminal_total - $refundrateCount_terminal; ?>, sliced: true, selected: true, color: '#66CDAA'}
                            ]
                        }]
                });
                var optionsrefundrate_total = ({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        renderTo: 'refundratechartstotal_count'
                    },
                    title: {text: '订单昨日总退款率'},
                    tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'},
                    plotOptions: {
                        pie: {allowPointSelect: true, cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                connectorColor: '#000000',
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                            }
                        }
                    }, series: [{
                            type: 'pie',
                            name: '所占百分比',
                            data: [
//                                ['退款率', <?php echo $refundrateCount_total; ?>, color:'#FF4040'],
                                {name: '退款率', y: <?php echo $refundrateCount_total; ?>, color: '#FF4040'},
                                {name: '未退款率', y: <?php echo $refundrateCount_total_total; ?>, sliced: true, selected: true, color: '#66CDAA'},
                            ]
                        }]
                });
                incomechart = new Highcharts.Chart(optionIncome);
                orderchart = new Highcharts.Chart(optionsOrder);
                registerchart = new Highcharts.Chart(optionsRegister);
                refundchart = new Highcharts.Chart(optionsRefund);
                refundrate_web = new Highcharts.Chart(optionsrefundrate_web);
                refundrate_ternminal = new Highcharts.Chart(optionsrefundrate_ternminal);
                refundrate_total = new Highcharts.Chart(optionsrefundrate_total);
//                var time = [];
//                var datarefund = [];
//                var dataregister = [];
//                var dataorder = [];
//                var dataincome = [];
//                var datapoint = [];
//                var datarefund_terminal = [];
//                var datarefund_web = [];
//                var totalCount_days_terminal = [];
//                var totalrefund;
//                var totalregister;
//                var totalorder;
//                var totalincome;
//                setInterval(function() {
//                    $.ajax({
//                        url: './index.php?r=admin/getData', //?elevator_id='+data.id,
//                        type: 'post',
//                        dataType: 'json',
//                        success: function(data) {
//                            var jsonData = eval(data);
//                            $.each(jsonData, function(i, day) {
//                                time[i] = day.day;
//                                datarefund[i] = day.totalrefund_days;
//                                totalrefund = day.totalrefund_dayss;
//                                dataregister[i] = day.regCount_days;
//                                totalregister = day.regCount_dayss;
//                                dataorder[i] = day.totalCount_days;
//                                totalCount_days_terminal[i] = day.totalCount_days_terminal;
//                                datarefund_terminal[i] = day.totalCount_refund_terminal;
//                                datarefund_web[i] = day.totalCount_refund_web;
//                                totalorder = day.totalCount_dayss;
//                                dataincome[i] = day.totalIncome_day;
//                                datapoint[i] = day.totalIncome_points_day;
//                                totalincome = day.totalIncome_dayss;
//                            });
//                            optionIncome.title.text = "近七天收入统计（共" + totalincome + "元)";
//                            optionIncome.xAxis.categories = time;
//                            optionIncome.series[0].data = dataincome;
//                            optionIncome.series[1].data = datapoint;
//                            incomechart = new Highcharts.Chart(optionIncome);
//                            optionsOrder.title.text = "近七天订单统计（共" + totalorder + "单)";
//                            optionsOrder.xAxis.categories = time;
//                            optionsOrder.series[0].data = dataorder;
//                            optionsOrder.series[1].data = totalCount_days_terminal;
//                            optionsOrder.series[2].data = datarefund_terminal;
//                            optionsOrder.series[3].data = datarefund_web;
//                            registerchart = new Highcharts.Chart(optionsOrder);
//                            optionsRegister.title.text = "近七天注册人数统计（共" + totalregister + "人)";
//                            optionsRegister.xAxis.categories = time;
//                            optionsRegister.series[0].data = dataregister;
//                            registerchart = new Highcharts.Chart(optionsRegister);
//                            optionsRefund.xAxis.categories = time;
//                            optionsRefund.series[0].data = datarefund;
//                            optionsRefund.title.text = "近七天退款统计(共" + totalrefund + "元)";
//                            refundchart = new Highcharts.Chart(optionsRefund);
//                        }
//                    });
//                }, 1000 * 3);
            });
        </script>
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" id="allscream">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>重庆颇闰科技-后台管理平台<SMALL>Version 1.0</SMALL></H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                    </ul>
                </div>
                <!-- END OF BREADCRUMB -->
                <!--  DEVICE MANAGER -->
                <div class="content-wrap" style="margin-bottom:0px;">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="profit" id="profitClose">
                                <div class="headline">
                                    <h3>
                                        <span>
                                            <i class="maki-commerical-building"></i>&#160;&#160;覆盖校园(单位:个)</span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span class="pull-left"><i class="clock-position"></i></span>
                                    <?php echo $store_all; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="revenue" id="revenueClose">
                                <div class="headline">
                                    <h3>
                                        <span>
                                            <i class="fontawesome-group"></i>&#160;&#160;会员量(单位:人)</span>
                                    </h3>
                                </div>
                                <div class="value">
                                    <span class="pull-left"><i class="gauge-position"></i></span>
                                    <?php echo $user_all; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="order" id="orderClose">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="entypo-skype-circled"></i>&#160;&#160;在外总积分(单位:分)</span>
                                    </h3>
                                </div>
                                <div class="value">
                                    </span><?php echo $totalPoints; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="member" id="memberClose">
                                <div class="headline ">
                                    <h3>
                                        <span>
                                            <i class="maki-beer"></i>
                                            &#160;&#160;终端总数量(单位:台)
                                        </span>
                                    </h3>
                                </div>
                                <div class="value">
                                    </span><?php echo $total_machine_count; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="member">
                                <div class="headline">
                                    <h3>
                                        <span style="background-color:#8085E9">
                                            <i class="entypo-switch"></i>
                                            &#160;&#160;转化率
                                        </span>
                                    </h3>
                                </div>
                                <div class="value" style="color:#8085E9">
                                    </span><?php echo $conversion; ?><b>%</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->

                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="incomecharts" style="width:100%;"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="ordercharts" style="width:100%;"></div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="registercharts" style="width:100%;" ></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="chart-wrap">
                                <div id="refundcharts" style="width:100%;"></div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="chart-wrap">
                                <div id="refundratechartsweb_count" style="width:100%;" ></div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="chart-wrap">
                                <div id="refundratechartsterminal_count" style="width:100%;" ></div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="chart-wrap">
                                <div id="refundratechartstotal_count" style="width:100%;"></div>
                            </div>
                        </div>
                    </div>  
                </div>
                <!-- /END OF CONTENT -->




                <!-- FOOTER -->
                <div class="footer-space"></div>
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.
                    </div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->
    </body>
</html>

