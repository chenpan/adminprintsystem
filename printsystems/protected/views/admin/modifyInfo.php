<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            .error{
                color:red;
                margin-top: 7px;
                margin-left: -20px;
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#save").click(function() {
                    var password = $("#password").val().replace(/\s+/g, "");
                    if (password.length == 0) {
                        reback();
                        $("#password_error").text("密码不能为空！");
                        return false;
                    } 
                    else {
                        reback();
                        if (confirm("确认保存？"))
                        {
                            $.post("./index.php?r=admin/Modify", {password: password}, function(datainfo) {
                                var data = eval("(" + datainfo + ")");
                                if (data.data == "false")
                                {
                                    reback();
                                    $("#save_success").text("保存失败！");
                                }
                                else if (data.data == "success")
                                {
                                    reback();
                                    $("#save_success").text("保存成功！");
                                }
                            });
                        }
                    }
                });
            });
            function reback() {
                $("#password_error").text("*");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>个人中心</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">个人中心</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-horizontal" id="plurality_form" name="plurality_form" method="post" action="./index.php?r=partTime/addPlurality" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="username" class="col-sm-5 control-label">用户名：</label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" id="username" name="username" value="<?php echo Yii::app()->session['adminuser']; ?>" disabled="true"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="username_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-sm-5 control-label">密码：</label>
                                    <div class="col-sm-2">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="请输入密码"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="password_error">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-5 col-sm-3">
                                        <button type="button" id="save" class="btn btn-info btn-block" style="width: 240px;outline:none;margin-top:10px;">保存</button>
                                    </div>
                                    <div class="col-sm-3 error" id="save_success">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

