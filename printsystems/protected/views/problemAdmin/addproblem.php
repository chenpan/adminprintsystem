<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link rel="stylesheet" href="./css/oss/style.css" type="text/css">
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
             #problem-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3); 
            }
            .error{
                color:red;
                margin-left: -20px;
                margin-top: 10px;
            }
            .btn .caret {
                margin-left: 300px;
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#add_btn").click(function () {


                    if ($("#ossfile:has(div)").length != 0) {
                        $("#postfiles").trigger("click");
//                        addproblemform.submit();
                        console.log("添加了文件的");
                    } else {
                        if ($("#name").val() == null || $("#name").val() == "")
                        {
                            reback();
                            $("#nameMsg").text("请输入问题名称！");
                            return false;
                        } else if ($("#type").val() == 0)
                        {
                            reback();
                            $("#typeMsg").text("请选择问题类型！");
                            return false;
                        } else if ($("#solution").val() == null || $("#solution").val() == "")
                        {
                            reback();
                            $("#solutionMsg").text("请输入解决方案！");
                            return false;
                        } else if (confirm("确认增加？")) {
                            reback();
                            addproblemform.submit();
                        }
                    }

                });
//                $("#terminal-open").css("display", "block");
            });
            function reback() {
                $("#nameMsg").text("*");
                $("#typeMsg").text("*");
                $("#info").text("");
                $("#solutionMsg").text("*");
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PaPER WRaP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREaDCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>新增问题</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>问题管理
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=printor/group">新增问题</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <iframe style="display:none" name="test"></iframe>
                            <form class="form-horizontal" name="addproblemform" id="addproblemform" target="test" method="post">
                                <div class="form-group">
                                    <input type="hidden" id="problemId"/>
                                    <label for="name" class="col-sm-5 control-label">问题名称：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="请输入问题名称"/>
                                    </div>   
                                    <div class="col-sm-3 error" id="nameMsg">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="descrp" class="col-sm-5 control-label">问题描述：</label>
                                    <div class="col-sm-3">
                                        <textarea id="descrp" name="descrp" rows="3" style="width:100%;color:#8FBDE6;text-indent:1em"></textarea>
                                    </div>   
                                </div>
                                <div class="form-group">
                                    <label for="type" class="col-sm-5 control-label">问题类型：</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" id="type" name="type">
                                            <option value="0">+++++++++请选择++++++++++</option> 
                                            <?php foreach ($problemtype_info as $k => $v) { ?>
                                                <option value="<?php echo $v->problemtype_id; ?>"><?php echo $v->problemtype_name; ?></option> 
                                            <?php } ?>
                                        </select>
                                    </div>   
                                    <div class="col-sm-3 error" id="typeMsg">
                                        *
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="solution" class="col-sm-5 control-label">解决方案：</label>
                                    <div class="col-sm-3">
                                        <textarea id="solution" name="solution" rows="5" style="width:100%;color:#8FBDE6;text-indent:1em"></textarea>
                                    </div>   
                                    <div class="col-sm-3 error" id="solutionMsg">*</div>
                                </div>
                                <div class="form-group">
                                    <label for="author" class="col-sm-5 control-label">作者：</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="author" name="author"/>
                                    </div>   
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-5 control-label"> <label for="attachmentfile">版本文件:</label>    </div>
                                    <div class="col-sm-3">
                                        <div id="ossfile">你的浏览器不支持flash,Silverlight或者HTML5！</div>
                                        <div id="container">
                                            <a id="selectfiles" class="btn" style="width:100%;" href="javascript:void(0);">选择文件</a>
                                        </div>  
                                        <p>&nbsp;</p>
                                                <!--<input type="file" id="versionfile" name="versionfile" style="outline:none;margin-top: 7px;">-->
                                    </div>
                                    <!--<div class="col-sm-3 star" style="margin-top: 13px;" id="attachmentfile-file">*<pre id="console" style="border:0px;background-color: #FFFFFF"></pre></div>-->
                                </div>
                                <div class="form-group" style="margin-top: 30px;">
                                    <div class="col-sm-offset-5 col-sm-3">
                                        <a id="postfiles" class="btn  btn-info "style="width:20%; display: none" href="javascript:void(0);">保存</a>
                                        <button type="button" id="add_btn" class="btn btn-info btn-block" style="width: 100%;outline:none;">保存</button>
                                        <!--<span class="Msg" id="info"></span>-->
                                    </div>
                                    <div class="col-sm-3 error"><span class="Msg" id="save_info"></span>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MaNaGER -->
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. all rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PaPER WRaP -->

    </body>
    <script type="text/javascript" src="./css/oss/lib/plupload-2.1.2/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="./css/oss/upload_problem.js"></script>
</html>

