<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
                border-radius: 3px;
            }
            .menulist{
                margin-top: 25px;
            }
            #alreadytable_wrapper section{
                border:1px #f5f5f5  solid;
                outline:none;
            }
            table tr:hover{
                background-color: transparent;
            }
            table tr{
                border-bottom: none;
            }
            #problem-open{
                display: block;
            }

        </style>
        <script type="text/javascript">
            $(function () {
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#download").click(function () {
                    window.location.href = "./index.php?r=problemAdmin/download&id=" + <?php echo $problem_info->problemid; ?>;
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">
                <!-- CONTENT -->
                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>问题详情</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li>
                            <i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li>
                            <i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>问题
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li> <a href="#">问题详情</a>
                        </li>
                    </ul>
                </div>
                <!--  / DEVICE MANAGER -->
                <div class="content-wrap" style="width:100%;margin-top:20px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-wrapper">   
                                <div class="menuFont order-detail-title"></div><hr>
                                <table id="orderDetail" style="margin-left: 1px;background-color:#FFF;"> 
                                    <tr style="font-weight: bold;">
                                        <td>
                                            问题名称
                                        </td>
                                        <td>
                                            问题类型
                                        </td>
                                        <td>
                                            作者
                                        </td>
                                        <td>
                                            问题描述
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="color:#16a085">
                                            <?php
                                            echo $problem_info->problem_name;
                                            ?>
                                        </td>
                                        <td style="color:#16a085">
                                            <?php
                                            echo $typename;
                                            ?>
                                        </td>
                                        <td style="color:#16a085">
                                            <?php
                                            echo $problem_info->author;
                                            ?>
                                        </td>
                                        <td style="color:#16a085">
                                            <?php
                                            echo $problem_info->problem_describe;
                                            ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-wrap" style="width: 100%;margin-top:20px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-wrapper" style="width:100%;"> 
                                <div class="menuFont order-detail-title">解决方案</div><hr>
                                <div class="col-sm-11"><textarea type="text" id="solution" name="solution" style="width:93%;height:200px;"value=""><?php
                                        echo $problem_info->solution;
                                        ?> 
                                    </textarea></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content-wrap" style="width: 100%;margin-top: 20px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="content-wrapper">  
                                <div class="menuFont order-detail-title">附件</div><hr>
                                <table id="orderDetail" style="margin-left: 1px;background-color:#FFF;">
                                    <tr style="font-weight: bold;">
                                        <td style="width: 250px;">附件名称：</td>
                                        <td style="width: 250px;"><?php
                                            echo $problem_info->attachment_name;
                                            ?> </td>
                                        <td style="width: 250px;"><a id="download"><span class='label label-success' style='cursor:pointer'>下载</span></a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
        </div>
        <!--  END OF PAPER WRAP -->

    </body>

</html>

