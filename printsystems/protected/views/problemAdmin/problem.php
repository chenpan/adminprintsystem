<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link rel="stylesheet" href="./css/oss/style.css" type="text/css">
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            select {
                border: 1px solid #e6e6e6;
                height: 40px;
                margin-left: -1px;
                width: 164px;
                cursor: pointer;
                border-radius: 3px;
            }
            .modal-body input[type="text"] {
                color: #666;
                height: 30px;
                width: 300px;
                text-indent: 5px;
                border: 1px solid #e6e6e6;
                border-radius: 3px;
            }
            textarea {
                border: 1px solid #e6e6e6;
                height: 110px;
                width: 500px;
            }
            .Msg{
                color: #C62F2F;
            }
            #problem-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            input[type="text"]{
                color: #66afe9;
            }
            .th {
                border-bottom:10px solid #F5F5F5;
                height: 45px;
                padding-top: 10px;
            }
            .th1 {
                border-bottom:10px solid #FFFFFF;
                height: 45px;
                padding-top: 10px;
            }
            #schooldel{
                cursor:pointer;
            }
            .fileupdate{
                float: left;
            }
            /*            select{
                            border: 1px solid #e6e6e6 !important;
                            border-radius: 3px;
                        }
                        .col-sm-9 {
                            margin-left: -8px;
                        }
                        hr{width: 106%;}*/
        </style>
        <!--<script src="./platform/js/share.js" type="text/javascript"></script>-->
        <script type="text/javascript">
            $(function () {
                var table = $('#problemtable').dataTable({
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "pagingType": "input",
                    "paginate": true,
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到相关数据",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "",
                        "processing": "正在加载中..."
                    }
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#closeprice").click(function () {
                    $('#Msg').text("");
                });
                //编辑保存
                $("#myFoot").on('click', '#save', function () {
                    reback();
                    if ($("#ossfile:has(div)").length != 0) {
                        $("#postfiles").trigger("click");
                        console.log("添加了文件的");
                    } else {
                        var id = $("#problemId").val();
                        var name = $("#name").val();
                        var descrp = $("#descrp").val();
                        var type = $("#type").val();
                        var solution = $("#solution").val();
                        var author = $("#author").val();
                        if ($("#name").val() == null || $("#name").val() == "") {
                            reback();
                            $("#nameMsg").text("请输入问题名称！");
                            return false;
                        } else if ($("#type").val() == null || $("#type").val() == "") {
                            reback();
                            $("#typeMsg").text("请输入问题类型！");
                            return false;
                        } else if ($("#solution").val() == null || $("#solution").val() == "") {
                            reback();
                            $("#solutionMsg").text("请输入解决方案！");
                            return false;
                        } else if (confirm("确认保存？")) {
                            $.post("./index.php?r=problemAdmin/editproblem_nofile", {id: id, name: name, descrp: descrp, type: type, solution: solution, author: author}, function (data) {
                                if (data.data == "success")
                                {
                                    reback();
                                    $("#info").text("保存成功！");
                                    window.location.href = "./index.php?r=problemAdmin/problem";
                                } else if (data.data == "false")
                                {
                                    alert("保存失败！");
                                    window.location.href = "./index.php?r=problemAdmin/problem";
                                }
                            }, 'json');
                        }
                    }
                });
                $("#addproblem").click(function () {
                    window.location.href = "./index.php?r=problemAdmin/addprblem";
                });
            });
            //问题编辑
            function editproblem(id) {
                var problemId = $("#" + id);
                var problemname = problemId.nextAll().eq(1).text().trim();
                var probledescrp = problemId.nextAll().eq(2).text().trim();
//                console.log(problemId.nextAll().eq(3).attr("class"));
                var probletype = problemId.nextAll().eq(3).attr("class").trim();
                var solution = problemId.nextAll().eq(4).text().trim();
                var author = problemId.nextAll().eq(5).text().trim();


                $("#myModalLabel").text(problemname);
                $("#problemId").val(id);
                $("#name").val(problemname);
                $("#descrp").val(probledescrp);
                $("#type").val(probletype);
                $("#solution").val(solution);
                $("#author").val(author);
                $("#myModal").modal("show");
                // 分享文档
            }
            //问题详情
            function probleminfo(id) {
                var problemId = id;
                window.location.href = "./index.php?r=problemAdmin/probleminfo&Id=" + problemId;
            }
            function delproblem(id) {
                var problemId = id;
                if (confirm("确定删除")) {
                    $.post("./index.php?r=problemAdmin/delproblem", {problemId: problemId}, function (datainfo) {
                        console.log("ok");
                        var data = eval("(" + datainfo + ")");
                        if (data.data == "success") {
                            window.location.reload();
                        } else if (data.data == "false") {
                            alert("删除失败!");
                        }
                    });
                }
            }
            function reback() {
                $(".Msg").text("*");
            }

            /**
             * 编辑数据带出值
             */
        </script>
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>问题管理</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=problemAdmin/problem">问题</a>
                        </li>
                    </ul>
                </div>  
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <input type="button" class="btn btn-success btn-set" id="addproblem" value="新增问题">
                        </div>
                    </div>
                </div>
                <div class="content-wrap">               
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="problemtable" width="100%;" style="font-size: 14px;">
                                <thead>
                                    <tr class="th">
                                        <th style="display:none">priceId</th>
                                        <!--<th style="display:none">storeId</th>-->
                                        <th>序列</th>
                                        <th>问题名称</th>
                                        <th>问题描述</th>
                                        <th>问题类型</th>
                                        <th style="display:none">解决方案</th>
                                        <th>作者</th>
                                        <th>添加时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($problem_info as $key => $v) { ?>
                                        <tr>
                                            <td style="display:none" id="<?php echo $v->problemid; ?>"></td>
                                            <td class="problemid"><?php echo $key + 1; ?></td>
                                            <td class="name"><a style="text-decoration: none;cursor:pointer;" 
                                                                onclick="probleminfo(<?php echo $v->problemid; ?>)">
                                                    <span><?php echo $v->problem_name; ?></span>
                                                </a></td>
                                            <td><?php echo $v->problem_describe; ?></td>
                                            <td class="<?php echo $v->problem_type; ?>"><?php
                                                $typeId = $v->problem_type;
                                                echo $problemtype_mode->find("problemtype_id =$typeId")->problemtype_name;
                                                ?></td>
                                            <td style="display:none"><?php echo $v->solution; ?></td>
                                            <td><?php echo $v->author; ?></td>
                                            <td><?php echo $v->addtime; ?></td>
                                            <td><a style="text-decoration: none;" id="priceedit" class="priceedit"href="#myModal" 
                                                   onclick="editproblem(<?php echo $v->problemid; ?>)">
                                                    <span class="label label-success">编辑</span>
                                                </a>
                                                <a style="text-decoration: none;"  id="delproblem" class="delproblem" onclick="delproblem(<?php echo $v->problemid; ?>)">
                                                    <span class="label label-success">删除</span>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">x</span></button>
                                        <h4 class="modal-title" id="myModalLabel"align="center"></h4>
                                    </div>
                                    <div class="modal-body" id="myBody" >
                                        <form role="form" id="editproblemform" name="editproblemform"method="post" enctype="multipart/form-data"  class="form-horizontal" style="margin-left:4px">
                                            <div class="form-group">
                                                <input type="hidden" id="problemId"/>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2  control-label" for="name">问题名称：</label>
                                                <div class="col-sm-6"><input class="form-control" type="text" id="name" name="name"/></div>
                                                <div class="col-sm-3"><span class="Msg" id="nameMsg">*</span></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2  control-label" for="descrp">问题描述：</label> 
                                                <div class="col-sm-6"><input class="form-control" type="text" id="descrp" name="descrp"value=""/></div>
                                                <div class="col-sm-3"><span class="Msg" id="descrpMsg" ></span></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2  control-label" for="type">问题类型：</label>
                                                <div class="col-sm-6"><select id="type" class="form-control type" name="type" style="width:300px">
                                                        <?php foreach ($problemtype_info as $v) { ?>
                                                            <option  value="<?php echo $v->problemtype_id; ?>"><?php echo $v->problemtype_name; ?></option> 
                                                        <?php } ?>
                                                    </select></div>
                                                <div class="col-sm-3"><span class="Msg" id="typeMsg">*</span></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2  control-label" for="solution">解决方案：</label> 
                                                <div class="col-sm-6"><textarea type="text" id="solution" name="solution" style="width:300px"value=""></textarea></div>
                                                <div class="col-sm-3"><span class="Msg" id="solutionMsg">*</span></div>
                                            </div>                                       
                                            <div class="form-group">
                                                <label class="col-sm-2  control-label" for="author">作者：</label> 
                                                <div class="col-sm-6"><input class="form-control" type="text" id="author" name="author"/></div>
                                                <div class="col-sm-3"><span class="Msg" ></span></div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2  control-label" for="attachmentfile">附件 :</label>
                                                <div class="col-sm-6"><div class="fileupdate">
                                                        <div id="ossfile">你的浏览器不支持flash,Silverlight或者HTML5！</div>
                                                        <div id="container">
                                                            <a id="selectfiles" class="btn" style="width:300px;" href="javascript:void(0);">选择文件</a>
                                                        </div>  
                                                        <p>&nbsp;</p>
                                                    </div></div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer"id="myFoot"align="center">
                                        <a id="postfiles" class="btn  btn-info "style="width:20%; display: none" href="javascript:void(0);">保存</a>
                                        <button class="btn  btn-info "style="width:20%;" href="javascript:void(0);"id="save">保存</button>
                                        <button type="button" class="btn btn-default" align="center"data-dismiss="modal" id="closeprice">关闭</button>
                                        <span class="Msg" id="save_info"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    </div>
                    <!-- / END OF FOOTER -->
                </div>
                <br>
            </div>
        </div>

    </body>
    <script type="text/javascript" src="./css/oss/lib/plupload-2.1.2/js/plupload.full.min.js"></script>
    <script type="text/javascript" src="./css/oss/upload_problem.js"></script>
</html>
