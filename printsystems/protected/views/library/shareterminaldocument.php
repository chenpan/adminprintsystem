<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <link href="./css/outWindows/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./css/outWindows/js/jquery.leanModal.min.js"></script>
        <!--<script type="text/javascript" src="./css/pagination/input.js"></script>-->
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            .btnlist{
                text-align: center;
            }
            .checkbox {
                width: 20px;
                height: 20px;
                border: 1px #E5E5E5 solid;
                border-radius: 100%;
                position: relative;
            }
            .checkboxFour label {
                width: 14px;
                height: 14px;
                border-radius: 100px;

                -webkit-transition: all .5s ease;
                -moz-transition: all .5s ease;
                -o-transition: all .5s ease;
                -ms-transition: all .5s ease;
                transition: all .5s ease;
                cursor: pointer;
                position: static;
                top: 2px;
                left: 2px;
                z-index: 1;
                background-color:#FFFFFF;
                border: 1px #65C91B solid;
            }
            /*                        input[type=checkbox] {
                                        visibility: hidden;
                                    }*/
            #share-open{
                display: block;
            }
            #terminal-share{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            a{
                color:#9EA7B3;
            }
            .btn-dete{
                background-color: #55C6F1!important;
                color: white!important;
                border: 1px solid #55C6F1!important;
                width: 86px;
            }
            .btn-dete:hover{
                background-color:#3CBDEF!important;
                color: white!important;
                border: 1px solid #3CBDEF!important;
            }
            .btn-info{
                width: 86px;  
            }
            /*            select{
                            border: 1px solid #e6e6e6 !important;
                            border-radius: 3px;
                        }
                        .col-sm-9 {
                            margin-left: -8px;
                        }
                        hr{width: 106%;}*/
        </style>
        <script type="text/javascript">
            $(function () {
                $("#sharetable").dataTable({
                    "serverSide": true,
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "ajax": './index.php?r=library/shareterminaldocumentAjax',
                    "stateSave": false,
                    "paginate": true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    },
                    columnDefs: [{
                            orderable: false, //禁用排序
                            targets: [0, 0]   //指定的列
                        }],
                    'columns': [
                        {"data": "shareId", "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<input type='checkbox' name='checkList' value='" + sData + "'>");
                            }},
                        {"data": "filename", "orderble": true},
                        {"data": "storeName", "orderble": true, "searchable": true},
                        {"data": "printorName", "orderble": true, "searchable": true},
                        {"data": "uploadtime", "orderble": true, "searchable": true},
                        {"data": "shareId",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a 'href='javascript:void(0);' " +
                                        "onclick='downloads(" + oData.shareId + ")'><span class='label label-success' style='cursor:pointer'>下载</span></a>&nbsp");
                            }
                        }
                    ]
                });
                $("#shareselect_all").on("click", function () {
                    if (isSelectAll() == false)
                    {
                        for (var i = 0; i < $("#sharetable").find(":checkbox").length; i++)
                        {
                            $("#sharetable").find(":checkbox").eq(i).removeAttr("checked");
                            $("#sharetable").find("label").eq(i).css("background-color", "#FFFFFF");
                        }

                    } else if (isSelectAll() == true)
                    {
                        for (var i = 0; i < $("#sharetable").find(":checkbox").length; i++)
                        {
                            $("#sharetable").find(":checkbox").eq(i).prop("checked", true);
                            $("#sharetable").find("label").eq(i).css("background-color", "#65C91B");
                        }
                    }
                });
                $('#close').click(function () {
                    $("#sharesmodal").css({"display": "none"});
                    $("#lean_overlay").css({"display": "none", opacity: 0});
                    history.go(0);
                });
                $('#share').leanModal({top: 100, overlay: 0.45, closeButton: ".hidemodal"});
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#share").click(function () {
                    var select = $('#myBody');
                    selectClassify(select, 0, 0, 0);
                    select.on('change', '.classify1', function () {
                        selectClassify(select, $(this).val(), 0, 0);
                    });
                    select.on('change', '.classify2', function () {
                        selectClassify(select, $(this).parents("select").find(".classify1").val(), $(this).val(), 0);
                    });
                    select.on('change', '.classify3', function () {
                        selectClassify(select, 0, 0, $(this).val());
                    });
                });
                //分享保存按钮
                $("#save").click(function () {
                    var num = 0;
                    var shareIdd = "";
                    for (var i = 0; i < $("#sharetable").find(":checkbox").length; i++)
                    {
                        if ($("#sharetable").find(":checkbox").eq(i).is(':checked'))
                        {
                            shareIdd += $("#sharetable").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    shareIdd = shareIdd.substring(0, shareIdd.length - 1);
                    if (num == 0)
                    {
                        alert("请您选择需要分享的选项！");
                        return false;
                    } else {
                        var classify1 = $(".classify1").val();
                        var classify2 = $(".classify2").val();
                        var classify3 = $(".classify3").val();
                        var classify = (classify3 > 0 ? classify3 : (classify2 > 0 ? classify2 : classify1));
                        var credit = parseInt($("#credit").val());
                        var keyword = $("#keyword").val();
                        var describe = $("#describe").val();
                        if (parseInt(classify) == 0) {
                            $('#selectMsg').text("X");
                            return false;
                        } else {
                            $('#selectMsg').text("*");
                        }
                        if (parseInt(credit) < 0 || parseInt(credit) > 500) {
                            $('#pointMsg').text("X");
                            return false;
                        } else {
                            $('#pointMsg').text("*");
                        }
                        if (keyword.length > 50) {
                            $('#keywordMsg').text("X");
                            return false;
                        } else {
                            $('#pointMsg').text("");
                        }
                        if (describe.length > 300) {
                            $('#describeMsg').text("X");
                            return false;
                        } else {
                            $('#describeMsg').text("");
                        }
                        if (confirm("确认保存？")) {
                            $.post("./index.php?r=library/shareterminal", {shareIdd: shareIdd, classify: classify, credit: credit, keyword: keyword, describe: describe}, function (datainfo) {
                                var data = eval("(" + datainfo + ")");
                                var code = data.code;
                                if (code == 200) {
                                    $('#returnMsg').text("分享成功！");
                                } else if (code == 400) {
                                    $('#returnMsg').text("分享失败！");
                                }
                            });
                        }
                    }
                });
                //删除按钮
                $("#delete").click(function () {
                    var num = 0;
                    var shareIdd = "";
                    for (var i = 0; i < $("#sharetable").find(":checkbox").length; i++)
                    {
                        if ($("#sharetable").find(":checkbox").eq(i).is(':checked'))
                        {
                            shareIdd += $("#sharetable").find(":checkbox").eq(i).attr('value') + ",";
                            num++;
                        }
                    }
                    shareIdd = shareIdd.substring(0, shareIdd.length - 1);
                    if (num == 0)
                    {
                        alert("请您选择需要删除的选项！");
                        return false;
                    }
                    if (confirm("确认删除？"))
                    {
                        $.post("./index.php?r=library/deleteshares", {shareIdd: shareIdd}, function (data) {
                            if (data.data == "success")
                            {
                                alert("删除成功！");
                                window.location.href = "./index.php?r=library/shareterminaldocument";
                            } else if (data.data == "false")
                            {
                                alert("删除失败！");
                            }
                        }, 'json');
                    }
                });
                function selectClassify(select, code1, code2, code3) {
                    select.find(".classify1 option").remove();
                    select.find(".classify2 option").remove();
                    select.find(".classify3 option").remove();
                    select.find(".classify1").append($("<option/>").val(0).text("请选择"));
                    select.find(".classify2").append($("<option/>").val(0).text("请选择"));
                    select.find(".classify3").append($("<option/>").val(0).text("请选择"));
                    var code;
                    code1 = parseInt(code1);
                    code2 = parseInt(code2);
                    code3 = parseInt(code3);
                    var classdata = $('#selectData').text();
                    var classify = eval('(' + classdata + ')');
                    if (code3 > 0) {
                        code2 = Math.floor(code3 / 10000) * 10000;
                    }
                    if (code2 > 0) {
                        code1 = Math.floor(code2 / 1000000) * 1000000;
                    }
                    for (var i = 0; i < classify.length; i++) {
                        code = parseInt(classify[i].code);
                        var option = $("<option/>").val(code).text(classify[i].name);
                        if (code % 1000000 == 0) {
                            if (code == code1) {
                                option.prop("selected", true);
                            }
                            option.appendTo(select.find(".classify1"));
                        } else if (code % 10000 == 0) {
                            if (code1 > 0 && code >= code1 && code < (code1 + 1000000)) {
                                if (code == code2) {
                                    option.prop("selected", true);
                                }
                                option.appendTo(select.find(".classify2"));
                            } else if (code1 == 0) {
                                option.appendTo(select.find(".classify2"));
                            }
                        } else {
                            if (code2 > 0) {
                                if (code >= code2 && code < (code2 + 10000)) {
                                    if (code == code3) {
                                        option.prop("selected", true);
                                    }
                                    option.appendTo(select.find(".classify3"));
                                }
                            } else if (code2 == 0) {
                                option.appendTo(select.find(".classify3"));
                            }
                        }
                    }

                }
            });
            // 判断当前是否为全选状态  
            function isSelectAll() {
                var isSelected = true;
                for (var i = 0; i < $("#sharetable").find(":checkbox").length; i++) {
                    if ($("#sharetable").find(":checkbox")[i].checked != true) {
                        isSelected = false;
                    }
                }
                return isSelected;
            }
            function downloads(shareid) {
                window.location.href = "./index.php?r=library/download&shareid=" + shareid;
            }

        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>

        <!--  PAPER WRAP -->
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr">

                <!-- CONTENT -->


                <!-- BREADCRUMB -->
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>分享终端文件</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li>文档
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=library/shareterminaldocument">分享终端文档</a>
                        </li>
                    </ul>
                </div>
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="sharetable" width="100%">
                                <thead>
                                    <tr class="th">
                                        <th style="width:15px">
                                            <a href="#" id='shareselect_all'>全选</th>                                           
                                        </th>
                                        <th>文件名</th>
                                        <th>所属学校</th>
                                        <th>上传终端</th>
                                        <th>上传时间</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-12 btnlist">
                            <button type="button" class="btn btn-info" id="share" href="#sharesmodal">分享</button>
                            <button type="button" class="btn btn-success btn-dete" id="delete">删除</button>
                        </div>
                    </div>
                </div>
                <!--  / DEVICE MANAGER -->

                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    <span  id="selectData" style="visibility:hidden"><?php echo $classify; ?></span>
                </div>
                <!-- / END OF FOOTER -->
            </div>            
        </div>
        <!--  END OF PAPER WRAP -->
        <div id="sharesmodal">
            <h1 style="font-size: 18px;font-family: Microsoft YaHei;text-align: center;margin-top: 5px;">终端文档分享</h1>  <a class="hidemodal" href="#"></a>
            <br>
            <form class="form-horizontal">
                <!--                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">标题：</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="inputName" placeHolder="请设置标题">
                                    </div>
                                    <span style="color:red" id="selectMsg">*</span>
                                </div>-->
                <div id="myBody" class="form-group">
                    <label class="col-sm-2 control-label">分类：</label> 
                    <select class="classify1 col-sm-3" style="margin-left:2%;height: 27px">
                    </select>
                    <select class="classify2 col-sm-3" style="margin-left:1%;height: 27px">
                    </select>
                    <select class="classify3 col-sm-3" style="margin-left:1%;height: 27px">
                    </select>
                    <span style="color:red;margin-left: 1%;font-weight: bold" id="selectMsg">*</span>
                </div>
                <div class="form-group">
                    <label for="credit" class="col-sm-2 control-label">积分：</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="credit" placeHolder="请设置下载所需积分(0-500)" style="width: 528px;">
                    </div>
                    <span class="Msg"id="pointMsg" style="color:red;font-weight: bold" class="col-sm-1"></span>
                </div>
                <div class="form-group">
                    <label for="keyword" class="col-sm-2 control-label">标签：</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" style="color:red;width: 528px;" id="keyword" placeHolder="请为所分享文档添加标签(不超过50个字)">
                    </div>
                    <span class="Msg" style="color:red;font-weight: bold" id="keywordMsg"></span>
                </div>
                <div class="form-group">
                    <label for="describe" class="col-sm-2 control-label">说明：</label>
                    <div class="col-sm-9">
                        <textarea placeHolder="请为分享文档做一个简短说明" id="describe" style="width: 109%;height:70px;resize:none;"></textarea>
                    </div>
                    <span class="Msg" style="color:red;font-weight: bold" id="describeMsg"></span>
                </div>
                <div class="center"id="myFoot">
                    <button type="button" class="btn btn-primary" id="save">保存</button>
                    <button type="button" class="btn btn-default" id="close">关闭</button>
                    <span class="Msg" style="color:red;font-weight: bold" id="returnMsg"></span>
                </div>
            </form>
        </div>
    </body>
</html>

