<!DOCTYPE HTML>
<HTML>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
<!--        <script type="text/javascript">
            var myFlexPaper = './platform/js/FlexPaper_2.3.6/';
        </script>-->
        <link rel="stylesheet" type="text/css" href="./platform/css/preview.css"/>
        <script type="text/javascript" src="./platform/js/FlexPaper_2.3.6/flexpaper.js"></script>
        <script type="text/javascript" src="./platform/js/FlexPaper_2.3.6/flexpaper_handlers.js"></script>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }
            #breadcrumb{
                background-color: #FFF;
                margin: 11px;
                width: 99%;
            }
            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 10px;
                padding-bottom: 10px;
            }
            .menulist{
                margin-top: 25px;
            }
            #share-open{
                display: block;
            }
            #auditdocument{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            #swf-viewer-swrap{
                border-color: #F5F5F5;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                window.auditswf = "";
                window.auditext = "";
                preview(window.auditswf, window.auditext);
                $('#auditlibtable').dataTable({
                    "serverSide": true,
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "ajax": './index.php?r=library/auditLibrarySerPage',
                    "stateSave": true,
                    "paginate": true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有相关数据",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "",
                        "processing": "正在加载..."
                    },
                    'columns': [
                        {"data": "attachmentid", "visible": false, "orderble": false, "searchable": false},
                        {"data": "id", "visible": false, "orderble": false, "searchable": false},
                        {"data": "filename", "orderble": true, "searchable": true},
                        {"data": "name", "orderble": true, "searchable": true, },
                        {"data": "credit", "visible": false, "orderble": true, "searchable": true},
                        {"data": "sharetime", "orderble": true, "searchable": true},
                        {"data": "storename", "orderble": true, "searchable": true},
                        {"data": "attachmentid",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a 'href='javascript:void(0);' " +
                                        "onclick='accept(" + oData.attachmentid + ")'><span class='label label-success' style='cursor:pointer'>通过</span></a>&nbsp")
                                        .append("<a 'href='javascript:void(0);\n\
                                        'onclick='del(" + oData.attachmentid + ")'><span class='label label-success' style='cursor:pointer'>删除</span></a>&nbsp");
                            }
                        }
                    ]
                });
                var table = $('#auditlibtable').DataTable();
                $('#auditlibtable tbody').on('click', 'tr', function () {
                    var rowIdx = table.row(this).index();
                    var attachmentid = table.column(0).row(rowIdx).data()["attachmentid"];
                    $.post("./index.php?r=library/auditLibpreview", {attachmentid: attachmentid}, function (data) {
                        var code = eval("(" + data + ")");
                        console.log(code);
                        window.auditswf = code.swf;
                        window.auditext = code.ext;
                        preview(window.auditswf, window.auditext);
                    });
                });

                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
            });
            function preview(swf, ext) {
                if (swf != '') {
                    if (ext == 'swf') {
                        $('#swf-viewer').FlexPaperViewer({
                            config: {
                                SWFFile: swf,
                                Scale: 1,
                                ZoomTransition: 'easeOut',
                                ZoomTime: 0.5,
                                ZoomInterval: 0.2,
                                FitPageOnLoad: false,
                                FitWidthOnLoad: true,
                                FullScreenAsMaxWindow: false,
                                ProgressiveLoading: false,
                                MinZoomSize: 1,
                                MaxZoomSize: 5,
                                SearchMatchAll: false,
                                InitViewMode: 'Portrait',
                                RenderingOrder: 'flash',
                                StartAtPage: '',
                                ViewModeToolsVisible: true,
                                ZoomToolsVisible: true,
                                NavToolsVisible: true,
                                CursorToolsVisible: true,
                                SearchToolsVisible: true,
                                WMode: 'window',
                                localeChain: 'zh_CN'
                            }
                        });
                    } else {
                        var img = $("<img/>", {
                            width: "100%",
                            height: "auto",
                            src: swf
                        });
                        $('#swf-viewer').html(img);
                        $('#swf-viewer').css("overflow", "auto");
                    }
                } else {
                    var empty = $("<div/>", {class: "empty", text: "预览失败"});
                    $('#swf-viewer').html(empty);
                }
            }
            //通过分享
            function accept(attachmentid) {
                if (confirm("确定通过这个文件吗？"))
                {
                    $.post("./index.php?r=library/acceptdelShare", {attachmentid: attachmentid}, function (data) {
                        var data = eval("(" + data + ")");
                        if (data.code == 200)
                        {
                            alert("审核通过！");
                            window.location.href = './index.php?r=library/auditdocument';
                        } else
                        {
                            alert(data.msg);
                        }
                    });
                }
            }
            //删除文档
            function del(attachmentid) {
                if (confirm("确定删除这个文件吗？"))
                {
                    $.post("./index.php?r=library/deleteLib", {attachmentid: attachmentid}, function (data) {
                        var code = eval("(" + data + ")");
                        console.log(code.data);
                        if (code.data == "success")
                        {
                            alert("删除成功！");
                            window.location.href = './index.php?r=library/auditdocument';
                        } else
                        {
                            alert("删除失败！");
                        }
                    });
                }
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head> 
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>审核文档</H3>
                    </div>
                    <ul class="pull-right dis-left">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=library/auditdocument">审核文档</a>
                        </li>
                    </ul>
                </div>  
                <div class="content-wrap">        
                    <div class="row">
                        <div class="col-lg-7">
                            <table id="auditlibtable" width="100%">
                                <thead>
                                    <tr class="th">
                                        <!--<th style="padding-left: 10px;"><A href="#" id="select_all">全选</A></th>-->
                                        <th>id</th>
                                        <th>id</th>
                                        <th>文件名</th>
                                        <th>用户</th>
                                        <th>di</th>
                                        <th>分享时间</th>
                                        <th>学校</th>
                                        <th>操作</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="col-lg-5" style="height:750px;">
                            <div id="swf-viewer-swrap">
                                <div id="swf-viewer"></div>                    
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FOOTER -->

                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright ? 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </div>
            <br>
        </div>           
    </BODY>
</HTML>