<!DOCTYPE html>
<html>
    <meta content="IE=11.0000" http-equiv="X-Ua-Compatible">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
    <title>后台管理系统</title>
    <link rel="shortcut icon" href="./platform/ico/favicon.ico">
    <script src="./platform/js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <style>
        body{
            background: #ebebeb;
            font-family: "Helvetica Neue","Hiragino Sans GB","Microsoft YaHei","\9ED1\4F53",arial,sans-serif;
            color: #222;
            font-size: 12px;
        }
        *{padding: 0px;margin: 0px;}
        .top_div{
            background: #008ead;
            width: 100%;
            height: 400px;
        }
        .ipt{
            border: 1px solid #d3d3d3;
            padding: 10px 10px;
            width: 260px;
            text-indent: 2em;
            border-radius: 4px;
            padding-left: 35px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s
        }
        .ipt:focus{
            border-color: #66afe9;
            outline: 0;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgba(102,175,233,.6)
        }
        .u_logo{
            background: url("./platform/img/login/username.png") no-repeat;
            padding: 10px 10px;
            position: absolute;
            top: 43px;
            left: 75px;

        }
        .p_logo{
            background: url("./platform/img/login/password.png") no-repeat;
            padding: 10px 10px;
            position: absolute;
            top: 12px;
            left: 75px;
        }
        a{
            text-decoration: none;
        }
        .tou{
            background: url("./platform/img/login/tou.png") no-repeat;
            width: 97px;
            height: 92px;
            position: absolute;
            top: -87px;
            left: 140px;
        }
        .left_hand{
            background: url("./platform/img/login/left_hand.png") no-repeat;
            width: 32px;
            height: 37px;
            position: absolute;
            top: -38px;
            left: 150px;
        }
        .right_hand{
            background: url("./platform/img/login/right_hand.png") no-repeat;
            width: 32px;
            height: 37px;
            position: absolute;
            top: -38px;
            right: -64px;
        }
        .initial_left_hand{
            background: url("./platform/img/login/hand.png") no-repeat;
            width: 30px;
            height: 20px;
            position: absolute;
            top: -12px;
            left: 100px;
        }
        .initial_right_hand{
            background: url("./platform/img/login/hand.png") no-repeat;
            width: 30px;
            height: 20px;
            position: absolute;
            top: -12px;
            right: -112px;
        }
        .left_handing{
            background: url("./platform/img/login/left-handing.png") no-repeat;
            width: 30px;
            height: 20px;
            position: absolute;
            top: -24px;
            left: 139px;
        }
        .right_handinging{
            background: url("./platform/img/login/right_handing.png") no-repeat;
            width: 30px;
            height: 20px;
            position: absolute;
            top: -21px;
            left: 210px;
        }
        .vcode{
            margin-left: 56px;
            margin-top: 13px;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            //得到焦点
            $("#password").focus(function() {
                $("#left_hand").animate({
                    left: "150",
                    top: " -38"
                }, {step: function() {
                        if (parseInt($("#left_hand").css("left")) > 140) {
                            $("#left_hand").attr("class", "left_hand");
                        }
                    }}, 2000);
                $("#right_hand").animate({
                    right: "-64",
                    top: "-38px"
                }, {step: function() {
                        if (parseInt($("#right_hand").css("right")) > -70) {
                            $("#right_hand").attr("class", "right_hand");
                        }
                    }}, 2000);
            });
            //失去焦点
            $("#password").blur(function() {
                $("#left_hand").attr("class", "initial_left_hand");
                $("#left_hand").attr("style", "left:100px;top:-12px;");
                $("#right_hand").attr("class", "initial_right_hand");
                $("#right_hand").attr("style", "right:-112px;top:-12px");
            });


            $("#login").click(function() {
                if ($("#username").val() == "" || $("#username").val() == null)
                {
                    reback();
                    $("#info").text("请输入用户名！");
                    return false;
                }
                else if ($("#password").val() == "" || $("#password").val() == null)
                {
                    reback();
                    $("#info").text("请输入密码！");
                    return false;
                }
                else
                {
                    reback();
                    loginform.submit();
                }
            });
            $('#username').bind('input propertychange', function() {
                if ($("#username").val() == "admin") {
                    $("#p_verificationcode").css("display", "block");
                    $("#div").css("height", "300px");
                } else {
                    $("#p_verificationcode").css("display", "none");
                    $("#div").css("height", "245px");
                }
            });
            $("#sendverificationcode").click(function() {
                $.post("./index.php?r=default/sendverificationcode", function(datainfo) {
                    time();
                });
            });
            if ($("#username").val() == "admin") {
                $("#p_verificationcode").css("display", "block");
                $("#div").css("height", "300px");
            }
            $("#sendverificationcode").attr("disabled", false);
            var wait = 60;
            function time() {
                if (wait == 0) {
                    $("#sendverificationcode").attr("disabled", false);
                    $("#sendverificationcode").attr("value", "发送验证码");
                    wait = 60;
                } else {
                    $("#sendverificationcode").attr("value", wait + "秒后再次发送");
                    $("#sendverificationcode").attr("disabled", true);
                    wait--;
                    setTimeout(function() {
                        time();
                    }, 1000)
                }
            }
        });
        function reback() {
            $("#info").text("");
        }
    </script>
    <body>
        <div class="top_div"></div>
        <div id = "div" style="background: rgb(255, 255, 255); margin: -127px auto auto; border: 1px solid rgb(231, 231, 231); border-image: none; width: 400px; height: 245px; text-align: center;">
            <div style="width: 165px; height: 96px; position: absolute;">
                <div class="tou"></div>
                <div class="initial_left_hand" id="left_hand"></div>
                <div class="initial_right_hand" id="right_hand"></div>
            </div>
            <form id="loginform" method="post">
                <p style="padding: 30px 0px 10px; position: relative;">
                    <span class="u_logo"></span>
                    <input class="ipt" type="text" id="username" name="username" placeholder="请输入用户名" value=""> 
                </p>
                <p style="position: relative;"><span class="p_logo"></span>      
                    <input class="ipt" id="password" type="password" name="password" placeholder="请输入密码" value="">   
                </p>
                <p id="p_verificationcode" style="position: relative;padding-top: 10px;display: none"> 
                    <input class="ipt" style="width:160px;padding-left: 10px" id="verificationcode" type="text" name="verificationcode" placeholder="请输入手机验证码"> 
                    <input id="sendverificationcode" type="button" style="height:38px;width:120px" value="发送验证码">
                </p>
                <div class="vcode">
                    <script async type="text/javascript" src="http://api.geetest.com/get.php?gt=562c1e19b5080bcd4893989beaf9db9b"></script>
                </div>
                <div style="height: 50px; line-height: 50px; margin-top: 30px; border-top-color: rgb(231, 231, 231); border-top-width: 1px; border-top-style: solid;">
                    <p style="margin: 0px 35px 20px 45px;">
                        <span style="float: left;color:red" id="info">

                        </span> 
                        <span style="float: right;">
                            <a id="login" style="background: rgb(0, 142, 173); padding: 7px 10px; border-radius: 4px; border: 1px solid rgb(26, 117, 152); border-image: none; color: rgb(255, 255, 255); font-weight: bold;" href="#">登 录</a> 
                        </span>        
                    </p>
                </div>
            </form>
        </div>
    </body>
</html>
