<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
                letter-spacing: 0;
            }
            #user-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function() {
                $('#alreadytable').dataTable({
                    "serverSide": true,
                    "processing": false, //datatable获取数据时候是否显示正在处理提示信息。
                    "ajax": './index.php?r=userManager/InfoToServerSideAjax',
                    "stateSave": false,
                    "paginate": true,
                    "pagingType": "input",
                    "order": [[5, "desc"]],
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有相关数据",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": "",
                        "Processing": "正在加载..."
                    },
                    'columns': [
                        {"data": "userid", "visible": false, "orderble": false, "searchable": false},
                        {"data": "id", "orderble": true, "searchable": true, },
                        {"data": "username", "orderble": true, "searchable": true},
                        {"data": "phone", "orderble": true, "searchable": true},
                        {"data": "email", "orderble": true, "searchable": true, },
                        {"data": "registertime", "orderble": true, "searchable": true},
                        {"data": "schoolname", "orderble": true, "searchable": true},
                        {"data": "record_info", "orderble": true, "searchable": true},
                        {"data": "userid",
                            "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);' " +
                                        "onclick='recordInfo(" + oData.userid + ")'><span class='label label-success' style='cursor:pointer'>查看</span></a>");
                            }
                        },
                        {"data": "username",
                            "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);' " +
                                        "onclick='fileInfo(\"" + oData.username + "\",\"" + oData.phone + "\")'><span class='label label-success' style='cursor:pointer'>查看</span></a>");
                            }
                        },
                        {"data": "username",
                            "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a href='javascript:void(0);' " +
                                        "onclick='businessInfo(\"" + oData.username + "\",\"" + oData.phone + "\")'><span class='label label-success' style='cursor:pointer'>查看</span></a>");
                            }
                        },
                        {"data": "userid",
                            "fnCreatedCell": function(nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<a <?php echo $deleteUser; ?> href='javascript:void(0);' " +
                                        "onclick='editInfo(" + oData.userid + ")'><span class='label label-success' style='cursor:pointer'>编辑</span></a>&nbsp")
                                        .append("<a <?php echo $deleteUser; ?> href='javascript:void(0);' onclick='deleteUsers(" + oData.userid + ")'><span class='label label-success' style='cursor:pointer'>删除</span></a>");
                            }
                        }
                    ],
                    "drawCallback": function(settings) {
                        var api = this.api();
                        v = api.row(0).column(0).data()[0];
                        if (v == null) {
                            api.row(0).column(7).visible(false);
                            api.row(0).column(8).visible(false);
                            api.row(0).column(9).visible(false);
                            api.row(0).column(10).visible(false);
                        } else {
                            api.row(0).column(7).visible(true);
                            api.row(0).column(8).visible(true);
                            api.row(0).column(9).visible(true);
                            api.row(0).column(10).visible(true);

                        }
                    }
                });

                $("#logout").click(function() {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
            });
            function deleteUsers(userid) {
                if ('<?php echo $deleteUser; ?>' == "") {
                    if (confirm("确定删除？")) {
                        $.post("./index.php?r=userManager/deleteUsers", {userid: userid}, function(datainfo) {
                            var data = eval("(" + datainfo + ")");
                            if (data.data == "success") {
                                alert("删除成功");
                                window.location.href = "./index.php?r=userManager/userInfo";
                            }
                        });
                    }
                } else if ('<?php echo $deleteUser; ?>' == "hidden") {
                    window.location.href = './index.php?r=nonPrivilege/index';
                }
            }
            function recordInfo(userid)
            {
                window.location.href = "./index.php?r=userManager/userstoreDetail&userid=" + userid;
            }
            function fileInfo(username,phone)
            {
                window.location.href = "./index.php?r=userManager/searchFile&userName=" + username + "&phone=" + phone;
            }
            function businessInfo(username,phone)
            {
                window.location.href = "./index.php?r=userManager/searchBusiness&userName=" + username + "&phone=" + phone;
            }
            function editInfo(userid)
            {
                window.location.href = "./index.php?r=userManager/editUsers&userid=" + userid;
            }
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>用户列表</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=userManager/userInfo">用户</a>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="alreadytable" width="100%">
                                <thead>
                                    <tr class="th">
                                        <th>用户序列</th>
                                        <th>序列</th>
                                        <th>用户名</th>
                                        <th>电话号码</th>
                                        <th>邮箱</th>
                                        <th>注册时间</th>
                                        <th>所属学校</th>
                                        <th>剩余积分</th>
                                        <th>积分流水</th>
                                        <TH>文件列表</TH>
                                        <TH>订单列表</TH>
                                        <TH>操作</TH>
                                    </TR>
                                </thead>
                            </table>
                        </DIV>
                    </DIV>     
                    <!-- FOOTER -->

                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    </div>
                    <!-- / END OF FOOTER -->
                </DIV>  
                <br>
            </DIV>

    </BODY>
</HTML>