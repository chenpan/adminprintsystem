<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
                letter-spacing: 0;
            }
            #user-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
//            function deleteUsers(userid) {
//                if (confirm("确定删除？")) {
//                    $.post("./index.php?r=userManager/deleteUsers", {userid: userid}, function(datainfo) {
//                        var data = eval("(" + datainfo + ")");
//                        if (data.data == "success")
//                            window.location.href = "./index.php?r=userManager/userInfo";
//                    });
//                }
//            }
            $(function () {
                $('#alreadytable').dataTable({
                    stateSave: false,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>用户订单列表</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=userManager/userInfo">用户</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">订单列表</a>
                        </li>
                    </ul>
                </div>   

                <div class="content-wrap">                                       
                    <div class="row">
                        <div class="col-lg-12">
                            <table id="alreadytable">
                                <THEAD>
                                    <TR tr="th">
                                        <TH style="padding-left: 10px;">序列</TH>
                                        <TH>订单号</TH>
                                        <TH>所属用户</TH>
                                        <TH>下单时间</TH>
                                        <TH>金额</TH>
                                        <TH>支付消耗积分</TH>
                                        <TH>操作</TH>
                                    </TR></THEAD>
                                <TBODY>
                                    <?php if ($business_info != "") { ?>
                                        <?php foreach ($business_info as $K => $V) { ?>
                                            <TR>
                                                <TD style="padding-left: 13px;"><?php echo $K + 1; ?></TD>
                                                <TD><?php echo $V->orderId; ?></TD>
                                                <TD><?php echo $userName; ?></TD>
                                                <TD><?php echo $V->placeOrdertime; ?></TD>
                                                <TD><?php echo "￥ " . $V->paidMoney . " 元"; ?></TD>
                                                <TD><?php echo $V->consumptionIntegral . " 点"; ?></TD>
                                                <TD>
                                                    <A href="./index.php?r=userManager/orderDetail&businessid=<?php echo base64_encode($V->businessid); ?>" ><SPAN class="label label-success">查看详情</SPAN></A>
                                                </TD>
                                            </TR>
                                        <?php } ?>
                                    <?php }else { ?>
                                                <TR>
                                                    <TD style="padding-left: 13px;"></TD>
                                                    <TD> </TD>
                                                    <TD> </TD>
                                                    <TD> </TD>
                                                    <TD> </TD>
                                                    <TD> </TD>
                                                    <TD> </TD>
                                                </TR>
                                            <?php } ?>  
                                </TBODY>
                            </TABLE>
                        </DIV>
                    </DIV> 
                    <!-- FOOTER -->
                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    </div>
                    <!-- / END OF FOOTER -->
                </DIV>
                <br>
            </DIV>

    </BODY>
</HTML>