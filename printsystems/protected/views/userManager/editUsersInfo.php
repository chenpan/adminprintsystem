<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
                letter-spacing: 0;
            }
            input{
                border:1px #f5f5f5 solid;
                padding: 5px 10px;
            }
            #user-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
            input[type=text] {
                color:#37BCE5;
            }
        </style>
        <script type="text/javascript">

            $(function () {
                $('#alreadytable').dataTable({
                    stateSave: true,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
                $("#logout").click(function () {
                    if (confirm("确定退出？"))
                    {
                        window.location.href = "./index.php?r=admin/Logout";
                    }
                });
                $("#sure").click(function () {
                    if ($("#username").val() == "" || $("#username").val() == null)
                    {
                        alert("请输入用户名！");
                        return false;
                    } else if ($("#password").val() == "" || $("#password").val() == null)
                    {
                        alert("请输入密码！");
                        return false;
                    } else if ($("#password").val().length < 6)
                    {
                        alert("密码不能少于6位！");
                        return false;
                    } else if ($("#password").val().length > 12)
                    {
                        alert("密码不能多于12位！");
                        return false;
                    } else if ($("#phone").val() == "" || $("#phone").val() == null)
                    {
                        alert("请输入电话号码！");
                        return false;
                    } else if (!/^(13[0-9]|14[0-9]|15[0-9]|18[0-9])\d{8}$/i.test($("#phone").val()))
                    {
                        alert("电话号码格式不对！");
                        return false;
                    } else
                    {
                        if (confirm("确认保存？"))
                            eidtuserform.submit();
                    }
                });
            });
        </script>
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>用户信息编辑</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=userManager/userInfo">用户</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">编辑操作</a>
                        </li>
                    </ul>
                </div>      
                <div class="content-wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="resetUser" class="form-group" style="text-align: center" >
                                <iframe style="display:none" name="test"></iframe>
                                <form class="form-horizontal" name="eidtuserform" id="eidtuserform" target="test" method="post">
                                    <div class="form-group">
                                        <label for="username" class="col-sm-5 control-label">用户：</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="username" id="username" value="<?php echo "$user_info->username"; ?>"/>
                                        </div>   
                                        <div class="col-sm-3 error" id="username_error">
                                            *
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-sm-5 control-label">密码：</label>
                                        <div class="col-sm-3">
                                            <input type="password" class="form-control"name="password" id="password" placeholder="请输入管理者密码"/>
                                        </div>   
                                        <div class="col-sm-3 error" id="password_error">
                                            *
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="role" class="col-sm-5 control-label">电话：</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="phone" id="phone" value="<?php echo "$user_info->phone"; ?>"/>
                                        </div>   
                                        <div class="col-sm-3 error" id="school">                           
                                        </div>
                                    </div>
                                    <div class="form-group"id="schooldiv">
                                        <label for="school" class="col-sm-5 control-label">所属学校：</label>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="email" id="email" value="<?php echo "$user_info->email"; ?>"/>
                                        </div>   
                                        <div class="col-sm-3 error" id="school">                           
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-top: 30px;">
                                        <div class="col-sm-offset-5 col-sm-3">
                                            <button type="button" id="sure" class="btn btn-info btn-block" style="width: 100%;outline:none;">保存</button>
                                        </div>
                                        <div class="col-sm-3 error" id="add_success">
                                        </div>
                                    </div>
                                </form>
                                <!--                            <?php $form = $this->beginWidget('CActiveForm', array('htmlOptions' => array('id' => 'changeform', 'target' => 'test'))); ?>
                                                                <fieldset>用户名： <?php echo $form->textField($user_info, 'username', array('type' => "text", 'value' => "$user_info->username", 'readonly' => "readonly", 'id' => "username", 'placeholder' => '用户名...', 'class' => 'name')); ?>
                                                                </fieldset>
                                                                <br>
                                                                <fieldset>
                                                                    密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码：<?php echo $form->PasswordField($user_info, 'userpsw', array('type' => "password", 'value' => "", 'id' => "userpsw", 'placeholder' => '密码...')); ?>
                                                                </fieldset>
                                                                <br>
                                                                <fieldset>
                                                                    电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话：<?php echo $form->textField($user_info, 'phone', array('type' => "text", 'value' => "$user_info->phone", 'id' => "phone", 'placeholder' => '电话号码...')); ?>
                                                                </fieldset>
                                                                <br>
                                                                <fieldset>
                                                                    邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱：<?php echo $form->textField($user_info, 'email', array('type' => "text", 'value' => "$user_info->email", 'id' => "email", 'placeholder' => '邮箱...')); ?>
                                                                </fieldset>
                                                                <br>
                                                                <button id="sure" class="btn btn-primary" type="button" style="width: 250px;">确定</button>
                                <?php $form = $this->endWidget(); ?>
                                                                <lable class="save_lable"></lable>-->
                            </div>
                        </DIV>     

                    </DIV>          
                </DIV>   
                <br>
                <!-- FOOTER -->
                <div id="footer">
                    <div class="devider-footer-left"></div>
                    <div class="time">
                        <p id="spanDate">
                        <p id="clock">
                    </div>
                    <div class="copyright">Copyright © 2014-2015
                        <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                </div>
                <!-- / END OF FOOTER -->
            </DIV> 
        </DIV>
    </BODY>
</HTML>