<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>重庆颇闰科技-后台管理系统</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php echo $recommend; ?>
        <style type="text/css">
            canvas#canvas4 {
                position: relative;
                top: 20px;
            }
            input[type="search"]{
                padding-top:5px;
                padding-bottom: 5px;
            }

            .content-wrap{
                background-color: #FFF;
                margin: 10px;
                padding-top: 20px;
                padding-bottom: 20px;
            }
            .menulist{
                margin-top: 25px;
            }
            table{
                letter-spacing: 0;
            }
            #user-open{
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.3);
            }
        </style>
        <script type="text/javascript">
            $(function () {
                $('#alreadytable').dataTable({
                    stateSave: false,
                    "pagingType": "input",
                    "language": {
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "zeroRecords": "没有找到记录",
                        "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "无记录",
                        "infoFiltered": "(从 _MAX_ 条记录过滤)",
                        "search": ""
                    }
                });
            });
            $(function () {
                $("#logout").click(function () {
                    if (confirm("确定注销？"))
                    {
                        window.location.href = "./index.php?r=schooler/Logout";
                    }
                });
                $("#searchOrderno").click(function () {
                    var userName = $("#userName").val();

                    window.location.href = "./index.php?r=userManager/searchUser&userName=" + userName;

                });
            });
        </script>  
    </head>
    <body> 
        <?php echo $leftContent; ?>
        <div class="wrap-fluid" style="margin-left:250px">
            <div class="container-fluid paper-wrap bevel tlbr"> 
                <div id="breadcrumb">
                    <div class="pull-left dis-left">
                        <H3>用户积分流水</H3>
                    </div>
                    <ul class="pull-right dis-left menulist">
                        <li>
                            <span class="entypo-home"></span>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=admin/index">首页</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="./index.php?r=userManager/userInfo">用户</a>
                        </li>
                        <li><i class="fa fa-lg fa-angle-right"></i>
                        </li>
                        <li><a href="#">积分流水</a>
                        </li>
                    </ul>
                </div>  
                <div class="content-wrap"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <table  id="alreadytable">
                                <THEAD>
                                    <TR class="th">
                                        <TH style="padding-left: 10px;">序列</TH>
                                        <TH>用户名</TH>
                                        <TH>增加积分</TH>
                                        <TH>扣除积分</TH>
                                        <TH>发生时间</TH>
                                        <TH>发生事件</TH>
                                    </TR></THEAD>
                                <TBODY>
                                    <?php foreach ($integralDetails_infos as $K => $V) { ?>
                                        <TR>
                                            <TD style="padding-left: 13px;"><?php echo $K + 1; ?></TD>
                                            <TD>
                                                <?php
                                                $user_models = user::model();
                                                $user_infox = $user_models->find(array('condition' => "userid=$V->_userid"));
                                                if (isset($user_infox))
                                                    echo $user_infox->username;
                                                else
                                                    echo "";
                                                ?>
                                            </TD>
                                            <TD><?php echo $V->addIntegral; ?></TD>
                                            <TD><?php echo $V->reduceIntegral; ?></TD>
                                            <TD><?php echo $V->happentime; ?></TD></TD>
                                            <TD><?php echo $V->happenInfo; ?></TD>
                                        </TR>
                                    <?php } ?>
                                </TBODY>
                            </TABLE>
                        </DIV>
                    </DIV> 
                    <!-- FOOTER -->

                    <div id="footer">
                        <div class="devider-footer-left"></div>
                        <div class="time">
                            <p id="spanDate">
                            <p id="clock">
                        </div>
                        <div class="copyright">Copyright © 2014-2015
                            <span class="entypo-heart"></span><a href="http://www.cqutprint.com/">重庆颇闰科技</a>. All rights reserved.</div>
                    </div>
                    <!-- / END OF FOOTER -->
                </DIV> 
                <br>
            </DIV>

    </BODY>
</HTML>