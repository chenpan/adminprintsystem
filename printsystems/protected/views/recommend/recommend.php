<!-- Le styles -->
<link rel="shortcut icon" href="./platform/ico/favicon.ico">
<link rel="stylesheet" href="./platform/css/style.css">
<link rel="stylesheet" href="./platform/css/loader-style.css">
<link rel="stylesheet" href="./platform/css/bootstrap.css">
<link href="./platform/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END OF RIGHT SLIDER CONTENT-->


<script type="text/javascript" src="./platform/js/jquery.js"></script>
<!--<script src="./platform/js/progress-bar/src/jquery.velocity.min.js"></script>-->
<!--<script src="./platform/js/progress-bar/number-pb.js"></script>-->

<!--<script type="text/javascript" src="./platform/js/preloader.js"></script>-->
<script type="text/javascript" src="./platform/js/bootstrap.js"></script>
<script type="text/javascript" src="./platform/js/app.js"></script>
<script type="text/javascript" src="./platform/js/load.js"></script>
<script type="text/javascript" src="./platform/js/main.js"></script>

<!--<script src="./platform/js/jhere-custom.js"></script>-->

<!--<script type="text/javascript" src="./platform/js/jquery-1.9.1.min.js"></script>-->
<script src="./platform/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./platform/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>

<script src="./css/bootstrap/highcharts.js" type="text/javascript"></script>
  <script type="text/javascript" src="./css/pagination/input.js"></script>
