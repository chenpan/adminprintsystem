<!-- TOP NAVBAR -->
<nav role="navigation" class="navbar navbar-static-top">
    <div class="container-fluid" style="margin-top: 20px">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="entypo-menu"></span>
            </button>
            <div id="logo-mobile" class="visible-xs">
                <h1>WEB管理<span>v1.2</span></h1>
            </div>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">

                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i style="font-size:20px;" class="icon-conversation"></i><div class="noft-red">23</div></a>


                    <ul style="margin: 11px 0 0 9px;" role="menu" class="dropdown-menu dropdown-wrap">
                        <li>
                            <a href="#">
                                <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/men/1.jpg">大方<b>刚刚</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/women/1.jpg">张超<b>3分钟前</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/men/2.jpg">吕泽松<b>2小时前</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/men/3.jpg">李雯<b>1天前</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <img alt="" class="img-msg img-circle" src="http://api.randomuser.me/portraits/thumb/men/4.jpg">王刚<b>2个月前</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div>查看全部信息</div>
                        </li>
                    </ul>
                </li>
                <li>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i style="font-size:19px;" class="icon-warning tooltitle"></i><div class="noft-green">5</div></a>
                    <ul style="margin: 12px 0 0 0;min-width: 340px;" role="menu" class="dropdown-menu dropdown-wrap">
                        <li>
                            <a href="#">
                                <span style="background:#DF2135" class="noft-icon fontawesome-file"></span>终端机一，缺纸 <b>刚刚</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <span style="background:#AB6DB0" class="noft-icon entypo-signal"></span>梅轩终端机一，未联网  <b>15分钟前</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <span style="background:#FFA200" class="noft-icon fontawesome-print"></span>竹轩终端机一，打印失败<b>2小时前</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <span style="background:#86C440" class="noft-icon fontawesome-remove"></span>兰轩终端机一，未开机  <b>1天前</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <span style="background:#0DB8DF" class="noft-icon fontawesome-circle"></span>菊轩终端机一，缺墨  <b>3个月前</b>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div style="min-width: 340px;">查看全部通知</div>
                        </li>
                    </ul>
                </li>
<!--                        <li><a href="#"><i data-toggle="tooltip" data-placement="bottom" title="Help" style="font-size:20px;" class="icon-help tooltitle"></i></a>
                </li>-->

            </ul>
            <div id="nt-title-container" class="navbar-left running-text visible-lg" style="width: 950px">
                <ul class="date-top">
                    <li class="entypo-calendar" style="margin-right:5px"></li>
                    <li id="Date"></li>
                </ul>
                <ul id="digital-clock" class="digital">
                    <li class="entypo-clock" style="margin-right:5px"></li>
                    <li class="hour"></li>
                    <li>:</li>
                    <li class="min"></li>
                    <li>:</li>
                    <li class="sec"></li>
                    <li class="meridiem"></li>
                </ul>
                <!--TITLE -->
                <div id="paper-top">
                    <div class="devider-vertical visible-lg"></div>
                    <div class="tittle-middle-header">
                        <div class="alert" id="wel-time">
                            <button type="button" class="close" data-dismiss="alert" style="margin-right: 10px;">×</button>
                            &nbsp;
                            <span class="tittle-alert entypo-info-circled"></span>
                            &nbsp;&nbsp;欢迎 <span style="font-weight: bold;"><?php echo $username; ?></span>&nbsp;&nbsp;&nbsp;<span id="login-time">您本次登录时间为 <?php echo urldecode($logintime); ?><span>
                                    </div>
                                    </div>
                                    </div>
                                    <!--/ TITLE -->
                                    </div>

                                    <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                                        <li>
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                <img alt="" class="admin-pic img-circle" src="http://api.randomuser.me/portraits/thumb/men/2.jpg">
                                                Hi, <?php echo $username; ?> <b class="caret"></b>
                                            </a>
                                            <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                                                <li>
                                                    <a href="./index.php?r=admin/modify">
                                                        <span class="entypo-user"></span>&#160;&#160;个人中心</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">
                                                        <span class="entypo-lifebuoy"></span>&#160;&#160;求助</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#" id="logout">
                                                        <span class="entypo-leaf"></span>&#160;&#160;退出</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="dropdown-toggle" href="./index.php?r=admin/downloadApp">
                                                APP下载
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" title="更换背景">
                                                <span class="icon-gear"></span>&nbsp;&nbsp;&nbsp;</a>
                                            <ul role="menu" class="dropdown-setting dropdown-menu">
                                                <li class="theme-bg">
                                                    <div id="button-bg"></div>
                                                    <div id="button-bg2"></div>
                                                    <div id="button-bg3"></div>
                                                    <div id="button-bg5"></div>
                                                    <div id="button-bg6"></div>
                                                    <div id="button-bg7"></div>
                                                    <div id="button-bg8"></div>
                                                    <div id="button-bg9"></div>
                                                    <div id="button-bg10"></div>
                                                    <div id="button-bg11"></div>
                                                    <div id="button-bg12"></div>
                                                    <div id="button-bg13"></div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                    </div>
                                    <!-- /.navbar-collapse -->
                                    </div>
                                    <!-- /.container-fluid -->
                                    </nav>
                                    <!-- /END OF TOP NAVBAR -->
                                    <!-- SIDE MENU -->
                                    <div id="skin-select" style="top:48px;">
                                        <div id="logo">
                                            <h1>颇闰科技</h1>
                                        </div>

                                        <a id="toggle">
                                            <span class="entypo-menu"></span>
                                        </a>
                                        <div class="dark">
                                            <form action="#">
                                                <span>
                                                    <input type="text" name="search" value="" class="search rounded id_search" placeholder="搜索..." autofocus="">
                                                </span>
                                            </form>
                                        </div>

                                        <div class="search-hover">
                                            <form id="demo-2">
                                                <input type="search" placeholder="搜索..." class="id_search">
                                            </form>
                                        </div>
                                        <div class="skin-part" style="">
                                            <div id="tree-wrap">
                                                <div class="side-bar">
                                                    <ul class="topnav menu-left-nest top-white" style="margin:10px">
                                                        <!--                                                        <li>
                                                                                                                    <a href="#" style="border-left:0px solid!important;" class="title-menu-left">
                                                        
                                                                                                                        <span class="design-kit"></span>
                                                                                                                        <i data-toggle="tooltip" class="fontawesome-cog pull-right config-wrap"></i>
                                                        
                                                                                                                    </a>
                                                                                                                </li>-->
                                                        <li id="index-open">
                                                            <a class="tooltip-tip ajax-load" href="./index.php?r=admin/index" title="首页">
                                                                <i class="fontawesome-home"  style="margin-left:3px;"></i>
                                                                <span>首页</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <ul style="margin:10px" class="topnav menu-left-nest top-white" <?php
                                                    if ($leftmenu['学校'] == 0 && $leftmenu['终端'] == 0 && $leftmenu['广告'] == 0 && $leftmenu['兼职'] == 0) {
                                                        echo 'style="display:none"';
                                                    }
                                                    ?>>
                                                        <!--                                                        <li>
                                                                                                                    <a href="#" style="border-left:0px solid!important;" class="title-menu-left">
                                                                                                                        <span class="widget-menu"></span>
                                                                                                                        <i data-toggle="tooltip" class="pull-right config-wrap fontawesome-cog "></i>
                                                        
                                                                                                                    </a>
                                                                                                                </li>-->

                                                        <li>
                                                            <a id="school-open"<?php
                                                            if ($leftmenu['学校'] == 0) {
//                                                            echo 'style="background-color:red"';
                                                                echo 'style="display:none"';
                                                            }
                                                            ?>class="tooltip-tip ajax-load" href="./index.php?r=schooler/school" title="学校">
                                                                <i class="maki-school"></i>
                                                                <span>学校</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a  <?php
                                                            if ($leftmenu['终端'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="#" title="终端">
                                                                <i class="maki-beer" style="margin-left:3px;"></i>
                                                                <span  style="margin-left:3px;">终端</span>                                   
                                                            </a>
                                                            <ul id="terminal-open">
                                                                <li>
                                                                    <a  id="terminal-information"<?php
                                                                    if ($leftmenu['终端信息'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=printor/printor" title="终端信息"><i class="entypo-flow-cascade"></i><span>终端信息</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="terminal-monitoring"<?php
                                                                    if ($leftmenu['终端监控'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=printor/printormonitor" title="终端监控"><i class="entypo-flow-cascade"></i><span>终端监控</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="terminal-grouping"<?php
                                                                    if ($leftmenu['终端分组'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=printor/group" title="终端分组"><i class="entypo-flow-cascade"></i><span>终端分组</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="terminal-version"<?php
                                                                    if ($leftmenu['终端版本'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=printor/printversion" title="终端版本"><i class="entypo-flow-cascade"></i><span>终端版本</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="terminal-version"<?php
                                                                    if ($leftmenu['错误信息'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=printor/printerror" title="错误信息"><i class="entypo-flow-cascade"></i><span>错误信息</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <a <?php
                                                            if ($leftmenu['广告'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="#" title="广告">
                                                                <i class="entypo-video"></i>
                                                                <span>广告</span>
                                                            </a>
                                                            <ul id="advertisement-open">
                                                                <li>
                                                                    <a  id="company-management"<?php
                                                                    if ($leftmenu['公司管理'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=advertisements/companyInfo" title="公司管理"><i class="entypo-flow-cascade"></i><span>公司管理</span></a>
                                                                </li>
                                                                <li>
                                                                    <a  id="advertisement-management"<?php
                                                                    if ($leftmenu['广告管理'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=advertisements/advertisementInfo" title="广告管理"><i class="entypo-flow-cascade"></i><span>广告管理</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <a <?php
                                                            if ($leftmenu['兼职'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="#" title="兼职">
                                                                <i class="entypo-user-add"></i>
                                                                <span style="margin-left:2px;">兼职</span>
                                                            </a>
                                                            <ul  id="job-open">
                                                                <li>
                                                                    <a id="job-management"<?php
                                                                    if ($leftmenu['兼职管理'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=parttime/partTimeInfo" title="兼职管理"><i class="entypo-flow-cascade"></i><span>兼职管理</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="job-examine"<?php
                                                                    if ($leftmenu['兼职审核'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=parttime/audite" title="兼职审核"><i class="entypo-flow-cascade"></i><span>兼职审核</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="job-reverse"<?php
                                                                    if ($leftmenu['兼职冲账'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=parttime/reverse" title="兼职冲账"><i class="entypo-flow-cascade"></i><span>兼职冲账</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>

                                                    <ul style="margin:10px" class="topnav menu-left-nest top-white"  <?php
                                                    if ($leftmenu['文件'] == 0 && $leftmenu['订单'] == 0 && $leftmenu['用户'] == 0 && $leftmenu['价格'] == 0) {
                                                        echo 'style="display:none"';
                                                    }
                                                    ?>>
                                                        <li>
                                                            <a id="price-open"<?php
                                                            if ($leftmenu['价格'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="./index.php?r=priceAdmin/price" title="价格">
                                                                <i class="entypo-hourglass"  style="margin-left:3px;"></i>
                                                                <span style="margin-left:6px;">价格</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a id="file-open"<?php
                                                            if ($leftmenu['文件'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="./index.php?r=filesAdmin/files" title="文件">
                                                                <i class="entypo-doc-text-inv"  style="margin-left:3px;"></i>
                                                                <span style="margin-left:4px;">文件</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a id="order-open" <?php
                                                            if ($leftmenu['订单'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="./index.php?r=business/business" title="订单">
                                                                <i class="fontawesome-list-alt"></i>
                                                                <span style="margin-left:3px;">订单</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a id="user-open"<?php
                                                            if ($leftmenu['用户'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="./index.php?r=userManager/userInfo" title="用户">
                                                                <i class="entypo-users"></i>
                                                                <span>用户</span>
                                                            </a>
                                                        </li>
                                                    </ul>

                                                    <ul style="margin:10px" id="menu-showhide" class="topnav menu-left-nest top-white" <?php
                                                    if ($leftmenu['退款'] == 0 && $leftmenu['统计'] == 0) {
                                                        echo 'style="display:none"';
                                                    }
                                                    ?>>
                                                        <li>
                                                            <a <?php
                                                            if ($leftmenu['退款'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip" href="#" title="退款">
                                                                <i class="maki-credit-card"></i>
                                                                <span style="margin-left:3px;">退款</span>
                                                            </a>
                                                            <ul  id="refund-open">
                                                                <li>
                                                                    <a id="refund-list"<?php
                                                                    if ($leftmenu['退款列表'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=refund/refund" title="退款列表"><i class="entypo-flow-cascade"></i><span>退款列表</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="refund-overlist"<?php
                                                                    if ($leftmenu['已退款列表'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=refund/alreadyRefund" title="已退款列表"><i class="entypo-flow-cascade"></i><span>已退款列表</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <a <?php
                                                            if ($leftmenu['文库'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip" href="#" title="文库">
                                                                <i class="entypo-database"></i>
                                                                <span style="margin-left:7px;"> 文库</span>
                                                            </a>
                                                            <ul  id="share-open">
                                                                <li>
                                                                    <a id="deldocument"<?php
                                                                    if ($leftmenu['删除文档'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=library/deldocument" title="删除文档"><i class="entypo-flow-cascade"></i><span>删除文档</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="auditdocument"<?php
                                                                    if ($leftmenu['审核文档'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=library/auditdocument" title="审核文档"><i class="entypo-flow-cascade"></i><span>审核文档</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="terminal-share"<?php
                                                                    if ($leftmenu['分享终端文档'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=library/shareterminaldocument" title="分享终端文档"><i class="entypo-flow-cascade"></i><span>分享终端文档</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <a <?php
                                                            if ($leftmenu['统计'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip" href="#" title="统计">
                                                                <i class="icon-graph-pie"></i>
                                                                <span style="margin-left:6px;">统计</span>
                                                            </a>
                                                            <ul id="statistics-open">
                                                                <li>
                                                                    <a id="statistics-order"<?php
                                                                    if ($leftmenu['订单统计'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=statistics/school&type=order" title="订单统计"><i class="entypo-flow-cascade"></i><span>订单统计</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="statistics-income"<?php
                                                                    if ($leftmenu['收入统计'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=statistics/school&type=income" title="收入统计"><i class="entypo-flow-cascade"></i><span>收入统计</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="statistics-refund"<?php
                                                                    if ($leftmenu['退款统计'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=statistics/school&type=refund" title="退款统计"><i class="entypo-flow-cascade"></i><span>退款统计</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="statistics-user"<?php
                                                                    if ($leftmenu['注册人数统计'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=statistics/school&type=register" title="注册人数统计"><i class="entypo-flow-cascade"></i><span>注册人数统计</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="statistics-paper"<?php
                                                                    if ($leftmenu['打印纸张统计'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=statistics/school&type=printpaper" title="打印纸张统计"><i class="entypo-flow-cascade"></i><span>打印纸张统计</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="statistics-file"<?php
                                                                    if ($leftmenu['上传文件统计'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=statistics/school&type=file" title="上传文件统计"><i class="entypo-flow-cascade"></i><span>上传文件统计</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="statistics-info"<?php
                                                                    if ($leftmenu['统计信息'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=statistics/statisticsinfo" title="统计信息"><i class="entypo-flow-cascade"></i><span>统计信息</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                    <ul style="margin:10px" class="topnav menu-left-nest top-white" <?php
                                                    if ($leftmenu['积分'] == 0) {
                                                        echo 'style="display:none"';
                                                    }
                                                    ?>>
                                                        <li>
                                                            <a id="active-open" <?php
                                                            if ($leftmenu['积分'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="#" title="积分">
                                                                <i class="fontawesome-leaf"></i>
                                                                <span style="margin-left:3px;">积分</span>
                                                            </a>
                                                            <ul id="active-points">
                                                                <li>
                                                                    <a id="activepoints-permissions"<?php
                                                                    if ($leftmenu['增加积分'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=active/activepoints" title="增加积分"><i class="entypo-flow-cascade"></i><span>增加积分</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="parttimepoints-permissions"<?php
                                                                    if ($leftmenu['扣除积分'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=active/parttimepoints" title="扣除积分"><i class="entypo-flow-cascade"></i><span>扣除积分</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <a id="feedback-open" <?php
                                                            if ($leftmenu['反馈'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="./index.php?r=feedbackAdmin/userfeedback" title="反馈">
                                                                <i class="entypo-github"></i>
                                                                <span style="margin-left:3px;">反馈</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a id="problem-open" <?php
                                                            if ($leftmenu['问题'] == 0) {
                                                                echo 'style="display:none;"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="./index.php?r=problemAdmin/problem" title="问题">
                                                                <i class="entypo-info-circled"></i>
                                                                <span style="margin-left:3px;">问题</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a id="message-open" <?php
                                                            if ($leftmenu['短信'] == 0) {
                                                                echo 'style="display:none;"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="./index.php?r=message/sendmessage" title="短信">
                                                                <i class="entypo-chat"></i>
                                                                <span style="margin-left:3px;">短信</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <ul style="margin:10px" class="topnav menu-left-nest top-white" <?php
                                                    if ($leftmenu['权限'] == 0) {
                                                        echo 'style="display:none"';
                                                    }
                                                    ?>>
                                                        <!--                                                        <li>
                                                                                                                    <a href="#" style="border-left:0px solid!important;" class="title-menu-left">
                                                        
                                                                                                                        <span class="design-kit"></span>
                                                                                                                        <i data-toggle="tooltip" class="fontawesome-cog pull-right config-wrap"></i>
                                                        
                                                                                                                    </a>
                                                                                                                </li>-->
                                                        <li>
                                                            <a <?php
                                                            if ($leftmenu['权限'] == 0) {
                                                                echo 'style="display:none"';
                                                            }
                                                            ?> class="tooltip-tip ajax-load" href="#" title="权限">
                                                                <i class="fontawesome-eye-close"></i>
                                                                <span style="margin-left:3px;">权限</span>
                                                            </a>
                                                            <ul id="jurisdiction-open">
                                                                <li>
                                                                    <a id="roles-permissions"<?php
                                                                    if ($leftmenu['角色与权限'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=auth/role" title="角色与权限"><i class="entypo-flow-cascade"></i><span>角色与权限</span></a>
                                                                </li>
                                                                <li>
                                                                    <a id="permission-assignment"<?php
                                                                    if ($leftmenu['权限分配'] == 0) {
                                                                        echo 'style="display:none"';
                                                                    }
                                                                    ?> class="tooltip-tip2 ajax-load" href="./index.php?r=auth/assignRole" title="权限分配"><i class="entypo-flow-cascade"></i><span>权限分配</span></a>
                                                                </li>
                                                            </ul>


                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END OF SIDE MENU -->
