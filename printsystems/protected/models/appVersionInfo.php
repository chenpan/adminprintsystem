<?php

class appVersionInfo extends MyActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return '{{appVersionInfo}}';
    }

}
