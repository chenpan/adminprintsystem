<?php

// This is the database connection configuration.
return array(
    'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/testdrive.db',
    // uncomment the following lines to use a MySQL database
//	'connectionString' => 'mysql:host=rds355y2nh13a6a5f420.mysql.rds.aliyuncs.com:3306;dbname=db_admin',
//	'emulatePrepare' => true,
//	'username' => 'admin_rw',
//	'password' => 'Kc-ADwoAsCv3J4',
//	'charset' => 'utf8',
//	'tablePrefix'=>'tbl_',
    'connectionString' => 'mysql:host=localhost;dbname=db_admin',
    'emulatePrepare' => true,
    'username' => 'background',
    'password' => 'background',
    'charset' => 'utf8',
    'tablePrefix' => 'tbl_',
//    'connectionString' => 'mysql:host=localhost;dbname=printsystem',
//    'emulatePrepare' => true,
//    'username' => 'print',
//    'password' => 'print',
//    'charset' => 'utf8',
//    'tablePrefix' => 'tbl_',
);
