<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => '重庆颇闰科技-后台管理系统',
    'defaultController' => 'default',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
// uncomment the following to enable the Gii tool

        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'print',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
        'platform',
        'interface',
        'pay',
        'mobile',
    ),
    // application components
    'components' => array(
        'session' => array(
            'timeout' => 3600,
        ),
        'user' => array(
// enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),
         */

// database settings are configured in database.php
        'db' => require(dirname(__FILE__) . '/database.php'),
        'db2' => array(
//            'class' => 'CDbConnection',
//            'connectionString' => 'mysql:host=rds355y2nh13a6a5f420.mysql.rds.aliyuncs.com:3306;dbname=db_home',
//            'emulatePrepare' => true,
//            'username' => 'admin_rw',
//            'password' => 'Kc-ADwoAsCv3J4',
//            'charset' => 'utf8',
//            'tablePrefix' => 'tbl_',
            'class' => 'CDbConnection',
            'connectionString' => 'mysql:host=localhost;dbname=db_home',
            'emulatePrepare' => true,
            'username' => 'home',
            'password' => 'home',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ),
        'errorHandler' => array(
// use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
// using Yii::app()->params['paramName']
    'params' => array(
// this is used in contact page
        'adminEmail' => 'webmaster@example.com',
    ),
);
