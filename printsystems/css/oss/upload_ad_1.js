
accessid = ''
accesskey = ''
host = ''
policyBase64 = ''
signature = ''
callbackbody = ''
filename = ''
key = ''
expire = 0
now = timestamp = Date.parse(new Date()) / 1000;
advertisementLength = ''
startdate = ''
enddate = ''
starttime = ''
endtime = ''
group = ''
company = ''
times = ''
advertisementname = ''

function send_request()
{
    var xmlhttp = null;
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (xmlhttp != null)
    {
        phpUrl = './css/oss/php/get_ad.php'
        xmlhttp.open("GET", phpUrl, false);
        xmlhttp.send(null);
        return xmlhttp.responseText
    }
    else
    {
        alert("Your browser does not support XMLHTTP.");
    }
}

function get_signature()
{
    //可以判断当前expire是否超过了当前时间,如果超过了当前时间,就重新取一下.3s 做为缓冲
    now = timestamp = Date.parse(new Date()) / 1000;
    console.log('get_signature ...');
    console.log('expire:' + expire.toString());
    console.log('now:', +now.toString())
    if (expire < now + 3)
    {
        console.log('get new sign')
        body = send_request()
        var obj = eval("(" + body + ")");
        host = obj['host']
        policyBase64 = obj['policy']
        accessid = obj['accessid']
        signature = obj['signature']
        filename = obj['filename']
        expire = parseInt(obj['expire'])
        callbackbody = obj['callback']
        key = obj['dir']
        advertisementlength = $("#advertisementLength").val()
        startdate = $("#startdate").val()
        enddate = $("#enddate").val()
        starttime = $("#starttime").val()
        endtime = $("#endtime").val()
//        group = $("#group").val()
        company = $("#company").val()
        times = $("#times").val()
        advertisementname = $("#advertisementname").val()
        group = ''
        var groups = $("#adGroup").val()
        for (var i = 0; i < groups.length; i++) {
            group += groups[i] + ","
        }
        group = group.substring(0, group.length - 1)

        return true;
    }
    return false;
}

function set_upload_param(up)
{
    var ret = get_signature()
    if (ret == true)
    {
        new_multipart_params = {
            'key': key + filename + '${filename}', //修改这里的文件名
            'policy': policyBase64,
            'OSSAccessKeyId': accessid,
            'success_action_status': '200', //让服务端返回200,不然，默认会返回204
            'callback': callbackbody,
            'signature': signature,
            'x:advertisementlength': advertisementlength,
            'x:startdate': startdate,
            'x:enddate': enddate,
            'x:starttime': starttime,
            'x:endtime': endtime,
            'x:group': group,
            'x:company': company,
            'x:times': times,
            'x:advertisementname': advertisementname
        };

        up.setOption({
            'url': host,
            'multipart_params': new_multipart_params
        });

        console.log('reset uploader')
        //uploader.start();
    }
}

var uploader = new plupload.Uploader({
    runtimes: 'html5,flash,silverlight,html4',
    browse_button: 'selectfiles',
    container: document.getElementById('container'),
    flash_swf_url: 'lib/plupload-2.1.2/js/Moxie.swf',
    silverlight_xap_url: 'lib/plupload-2.1.2/js/Moxie.xap',
    url: 'http://oss.aliyuncs.com',
//    filters: {
//        mime_types: [
//            {title: "我的文档", extensions: "PDF,DOC,DOCX,WPS"}
//        ],
//        max_file_size: '30mb', //最大只能上传400kb的文件
//        prevent_duplicates: true //不允许选取重复文件
//    },
    multi_selection: false,
    max_retries: 1,
    init: {
        //初始化
        PostInit: function() {
            document.getElementById('ossfile').innerHTML = '';
            document.getElementById('postfiles').onclick = function() {
                var advertisementname = $("#advertisementname").val().replace(/\s+/g, "");
                var startdate = $("#startdate").val().replace(/\s+/g, "");
                var enddate = $("#enddate").val().replace(/\s+/g, "");
                var starttime = $("#starttime").val().replace(/\s+/g, "");
                var endtime = $("#endtime").val().replace(/\s+/g, "");
                var group = $("#adGroup").val();
                var company = $("#company").val();
                var times = $("#times").val().replace(/\s+/g, "");
                var advertisementLength = $("#advertisementLength").val().replace(/\s+/g, "");
                if (advertisementname.length == 0)
                {
                    reback();
                    $("#advertisementname_error").text("请输入广告名称！");
                    return false;
                }
                if (advertisementLength.length == 0)
                {
                    reback();
                    $("#advertisementLength_error").text("请输入广告时长！");
                    return false;
                }
                if (startdate.length == 0)
                {
                    reback();
                    $("#startdate_error").text("请选择开始日期！");
                    return false;
                }
                if (enddate.length == 0)
                {
                    reback();
                    $("#enddate_error").text("请选择结束日期！");
                    return false;
                }
                if (starttime.length == 0)
                {
                    reback();
                    $("#starttime_error").text("请输入开始时间！");
                    return false;
                }
                if (endtime.length == 0)
                {
                    reback();
                    $("#endtime_error").text("请输入结束时间！");
                    return false;
                }
                if (group == null)
                {
                    reback();
                    $("#group_error").text("请选择一个分组！");
                    return false;
                }
                if (company == 0)
                {
                    reback();
                    $("#company_error").text("请选择一个公司！");
                    return false;
                }
                if (times.length == 0)
                {
                    reback();
                    $("#times_error").text("请输入每日播放次数！");
                    return false;
                }
                reback();
                if (confirm("确认保存?")) {

                    set_upload_param(uploader);
                    uploader.start();
                    return false;
                }
            };
        },
        //文件添加后
        FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
                document.getElementById('ossfile').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ')<b></b>'
                        + '<div class="progress"><div class="progress-bar" style="width: 0%"></div></div>'
                        + '</div>';
            });
        },
        //长传进度
        UploadProgress: function(up, file) {
            var d = document.getElementById(file.id);
            d.getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";

            var prog = d.getElementsByTagName('div')[0];
            var progBar = prog.getElementsByTagName('div')[0]
            progBar.style.width = 4 * file.percent + 'px';
            progBar.setAttribute('aria-valuenow', file.percent);
        },
        //上传完成
        FileUploaded: function(up, file, info) {
            console.log('uploaded')
            console.log(info.status)
            set_upload_param(up);
            if (info.status == 200)
            {
                document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '上传成功';

                document.getElementById("info").innerHTML = '保存成功';
                window.history.back(-1);
            }
            else
            {
                document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = info.response;
                document.getElementById("info").innerHTML = '保存失败';
            }
        },
        Error: function(up, err) {
            set_upload_param(up);
            document.getElementById('console').appendChild(document.createTextNode("\nError xml:" + err.response));
        }
    }
});
function reback() {
    $("#name_error").text("*");
    $("#file_error").text("*");
    $("#startdate_error").text("*");
    $("#enddate_error").text("*");
    $("#starttime_error").text("*");
    $("#endtime_error").text("*");
    $("#group_error").text("*");
    $("#times_error").text("*");
    $("#advertisementLength_error").text("*");
    $("#add_success").text("");
    $("#advertisementname_error").text("*");
}

uploader.init();
