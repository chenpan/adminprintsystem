
accessid = ''
accesskey = ''
host = ''
policyBase64 = ''
signature = ''
callbackbody = ''
filename = ''
key = ''
expire = 0
now = timestamp = Date.parse(new Date()) / 1000;
id = ''
name = ''
descrp = ''
type = ''
solution = ''
author = ''

function send_request()
{
    var xmlhttp = null;
    if (window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    } else if (window.ActiveXObject)
    {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (xmlhttp != null)
    {
        phpUrl = './css/oss/php/get_problem.php'
        xmlhttp.open("GET", phpUrl, false);
        xmlhttp.send(null);
        return xmlhttp.responseText
    } else
    {
        alert("Your browser does not support XMLHTTP.");
    }
}

function get_signature()
{
    //可以判断当前expire是否超过了当前时间,如果超过了当前时间,就重新取一下.3s 做为缓冲
    now = timestamp = Date.parse(new Date()) / 1000;
    console.log('get_signature ...');
    console.log('expire:' + expire.toString());
    console.log('now:', +now.toString())
    if (expire < now + 3)
    {
        console.log('get new sign')
        body = send_request()
        var obj = eval("(" + body + ")");
        host = obj['host']
        policyBase64 = obj['policy']
        accessid = obj['accessid']
        signature = obj['signature']
        filename = obj['filename']
        expire = parseInt(obj['expire'])
        callbackbody = obj['callback']
        key = obj['dir']
        id = $("#problemId").val()
        name = $("#name").val()
        descrp = $("#descrp").val()
        type = $("#type").val()
        solution = $("#solution").val()
        author = $("#author").val()
        console.log(id+name+descrp+solution)

        return true;
    }
    return false;
}

function set_upload_param(up)
{
    var ret = get_signature()
    if (ret == true)
    {
        new_multipart_params = {
            'key': key + filename + '${filename}', //修改这里的文件名
            'policy': policyBase64,
            'OSSAccessKeyId': accessid,
            'success_action_status': '200', //让服务端返回200,不然，默认会返回204
            'callback': callbackbody,
            'signature': signature,
            'x:id': id,
            'x:name': name,
            'x:descrp': descrp,
            'x:type': type,
            'x:solution': solution,
            'x:author': author,
        };

        up.setOption({
            'url': host,
            'multipart_params': new_multipart_params
        });

        console.log('reset uploader')
        //uploader.start();
    }
}

var uploader = new plupload.Uploader({
    runtimes: 'html5,flash,silverlight,html4',
    browse_button: 'selectfiles',
    container: document.getElementById('container'),
    flash_swf_url: 'lib/plupload-2.1.2/js/Moxie.swf',
    silverlight_xap_url: 'lib/plupload-2.1.2/js/Moxie.xap',
    url: 'http://oss.aliyuncs.com',
    multi_selection: false,
    max_retries: 1,
    init: {
        //初始化
        PostInit: function () {
            document.getElementById('ossfile').innerHTML = '';
            document.getElementById('postfiles').onclick = function () {
                if ($("#name").val() == null || $("#name").val() == "")
                {
                    reback();
                    $("#nameMsg").text("请输入问题名称！");
                    return false;
                }
                if ($("#type").val() == 0)
                {
                    reback();
                    $("#typeMsg").text("请选择问题类型！");
                    return false;
                }
                if ($("#solution").val() == null || $("#solution").val() == "")
                {
                    reback();
                    $("#solutionMsg").text("请输入解决方案！");
                    return false;
                } else
                {
                    $("#nameMsg,#typeMsg,#solutionMsg").text("");
                    set_upload_param(uploader);
                    uploader.start();
                    return false;
                }

            };
        },
        //文件添加后
        FilesAdded: function (up, files) {
            plupload.each(files, function (file) {
                document.getElementById('ossfile').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ')<b></b>'
                        + '<div class="progress"><div class="progress-bar" style="width: 0%"></div></div>'
                        + '</div>';
            });
        },
        //长传进度
        UploadProgress: function (up, file) {
            var d = document.getElementById(file.id);
            d.getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";

            var prog = d.getElementsByTagName('div')[0];
            var progBar = prog.getElementsByTagName('div')[0]
            progBar.style.width = 4 * file.percent + 'px';
            progBar.setAttribute('aria-valuenow', file.percent);
        },
        //上传完成
        FileUploaded: function (up, file, info) {
            console.log('uploaded');
            console.log(info.status);
            set_upload_param(up);
            if (info.status == 200)
            {
                document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '上传成功';

                document.getElementById("save_info").innerHTML = '保存成功';
                window.location.href = "./index.php?r=problemAdmin/problem";
            } else
            {
                document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = info.response;
                document.getElementById("save_info").innerHTML = '保存失败';
            }
        },
        Error: function (up, err) {
            set_upload_param(up);
            document.getElementById('console').appendChild(document.createTextNode("\nError xml:" + err.response));
        }
    }
});

uploader.init();
