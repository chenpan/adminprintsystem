<?php

function request_post($url = '', $post_data = array()) { //post Curl
    if (empty($url) || empty($post_data)) {
        return false;
    }
    $o = "";
    foreach ($post_data as $k => $v) {
        $o.= "$k=" . urlencode($v) . "&";
    }
    $post_data = substr($o, 0, -1);
    $postUrl = $url;
    $curlPost = $post_data;
    $ch = curl_init(); //初始化curl
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在  
    curl_setopt($ch, CURLOPT_URL, $postUrl); //抓取指定网页
    curl_setopt($ch, CURLOPT_HEADER, 0); //设置header
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //要求结果为字符串且输出到屏幕上
    curl_setopt($ch, CURLOPT_POST, 1); //post提交方式
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
    $data = curl_exec($ch); //运行curl
    curl_close($ch);
    return $data;
}

require_once("alipay.config.php");
require_once("lib/alipay_notify.class.php");

//计算得出通知验证结果
$alipayNotify = new AlipayNotify($alipay_config);
$verify_result = $alipayNotify->verifyNotify();

//if ($verify_result) {//验证成功
$trade_no = $_POST['trade_no']; //支付宝交易号
$out_trade_no = $_POST['out_trade_no'];  //商户订单号
$total_amount = $_POST['total_amount'];    //交易金额
$buyer_logon_id = $_POST['buyer_logon_id']; //买家帐号
$trade_status = $_POST['trade_status']; //交易状态
$buyer_pay_amount = $_POST['buyer_pay_amount']; //交易状态


if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
    $post_data = array('type' => 'success', 'out_trade_no' => $out_trade_no, 'trade_no' => $trade_no, 'total_amount' => $total_amount, 'buyer_logon_id' => $buyer_logon_id, 'trade_status' => $trade_status, "buyer_pay_amount" => $buyer_pay_amount);
    $url = "http://admin.cqutprint.com/index.php?r=alipayPrint/PayInfo";
    $data = request_post($url, $post_data);
}
echo 'success';
//} else {
//  echo "fail";
//}
?>